# -*- coding: utf-8 -*-

""" Script for running pre-commit checks: unit tests, git status, and pylint.
Must be executed from asset/ directory

"""

__date__ = 'Nov 21, 2013'
__author__ = 'Mauro Schilman'

import subprocess

if __name__ == '__main__':

    print 'Running tests....'
    subprocess.call('python -m unittest discover -s ../ -p *_test.py', shell=True)

    raw_input('Press enter to continue')
    print 'Git Status:'
    files = []
    git_status = subprocess.check_output('git status --porcelain', shell=True)
    print git_status
    for line in git_status.splitlines():
        tokens = line.strip().rsplit()
        if len(tokens) == 2:
            code, filename = tokens
        else:
            code, _, _, filename = tokens
        if code not in ['??', '!!', ' '] and '.py' in filename:
            files.append(filename)
    for f in files:
        raw_input('Press enter to continue')
        print 'Running pylint on ../%s' % f
        subprocess.call('pylint -f colorized --max-line-length=120 ../%s' % f, shell=True)

    raw_input('Press enter to continue')
    print 'Git Diff:'
    subprocess.call('git diff HEAD | colordiff', shell=True)
    ans = raw_input('Commit (y/N)? ')
    if ans and ans.lower() in ['y', 'yes']:
        subprocess.call('git commit', shell=True)
