#!/bin/bash

free=`free -mt | grep Total | awk '{print $4}'`

if [ $free -lt 256 ]; then
    ps -eo %mem,pid,user,args >/tmp/processes.txt
    echo 'Warning, free memory is '$free'mb' | mutt  -s "Server alert" diegolis@gmail.com -a /tmp/processes.txt

fi
