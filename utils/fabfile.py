# -*- coding: iso-8859-15 -*-
from fabric.api import run, env, task, settings, cd, sudo, put
from fabric.contrib.files import exists
from contextlib import contextmanager
from fabric.contrib.console import confirm
import os, time, sys
import json

#========================
#  VARIABLES GLOBALES
#========================

COMMON_PATH_REPO = '/home/infoad/adkiller' #TODO
TARGET = {
    'maquina': 'user@host:22', #TODO
}

env.user = 'infoad' #TODO
env.group = 'infoad' #TODO
env.password = '4h8a9s874k' #TODO
env.colorize_errors = True
env.skip_bad_hosts = True


@task
def git_update(branch='master'):
    """
    Clonar el repo o actualizarlo.
    """

    REPO = {
        'directorio': COMMON_PATH_REPO, # TODO
        'remote': 'https://infoxel:qwertyui@bitbucket.org/diegolis/adkiller.git' # TODO
    }

    if not is_installed('git'):
        sudo('apt-get install git-core')

    if not exists(REPO['directorio']):
        with cd_contextmanager(COMMON_PATH_REPO):
            sudo('git clone %s' % REPO['remote'])

    with cd_contextmanager(REPOS[repo]['direc']):
        sudo('git fetch --all')
        sudo('git checkout {branch}'.format(branch=branch))
        sudo('git pull')


@task
def full_deploy():
    pass


@task
def prueba_conexion():
    """Solamente "my_run('whoami')"."""
    run('whoami')

@task
def what_ubuntu():
    """cat /etc/issue"""
    result = run('cat /etc/issue')
    print result[7:12]
    return result[7:12]


@task
def desde_cero():
    # repositorio
    git_update() # la primera vez, clona e instala git si es necesario

    sudo('apt-get install python-virtualenv')

    # virtualenv
    sudo('virtualenv ADKILLER --no-sige-packages')
    run('source ADKILLER/bin/activate')
    run('pip install -r requirements.txt')

    # TODO: Qué es esto?
    with cd_contextmanager('/usr/local/lib/python2.7/dist-packages/autocomplete_light'):
        sudo('mv tests tests2')

    sudo('apt-get install ffmpeg libavcodec-extra-53')
    with cd_contextmanager('adkiller'): # TODO no entiendo donde estría esta carpeta
        sudo('cp local_settings.py.template local_settings.py')


    mensaje_edicion_local_settings = "Ahora debe editar 'local_settings.py' y rellenar"
    mensaje_edicion_local_settings += "los placeholders con los datos que correspondan."

    pedir_confirmacion(mensaje_edicion_local_settings)

    # wkhtmltopdf
    run('wget http://wkhtmltopdf.googlecode.com/files/wkhtmltopdf-0.10.0_rc2-static-i386.tar.bz2')
    run('tar xvjf wkhtmltopdf-0.10.0_rc2-static-i386.tar.bz2')
    run('mv wkhtmltopdf-i386 /usr/local/bin/wkhtmltopdf')
    run('chmod +x /usr/local/bin/wkhtmltopdf')

    # postgres # TODO no estoy seguro de que se corran estos comandos
    sudo('apt-get install postgresql')
    run("""sudo -u postgres psql -c 'CREATE USER adbout WITH PASSWORD "adbout";'""")
    run("sudo -u postgres psql -c 'CREATE DATABASE adbout OWNER adbout;'")
    run("sudo -u postgres psql -c 'ALTER USER adbout CREATEDB;'")

    # levantar backup #TODO: resolver la existencia de tal carpeta Drobpox y su contenido primero
    #run('pg_restore --username=adbout --host=localhost --schema=public --verbose --clean --dbname=adbout ~/Dropbox/adKiller/database/db.tar')

    sudo('apt-get install libpq-dev')
    run('pip install psycopg2') 

    mensaje_settings_db = """ En este momento debe editar local_settings.py (Sección DATABASES):

    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'adbout',       # Or path to database file if using sqlite3.
        'USER': 'adbout',       # Not used with sqlite3.
        'PASSWORD': 'adbout',   # Not used with sqlite3.
        'HOST': 'localhost',    # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',             # Set to empty string for default. Not used with sqlite3.
    }
    """
    pedir_confirmacion(mensaje_settings_db)

    # Collectstatics
    with cd_contextmanager(COMMON_PATH_REPO):
        sudo('python manage.py collectstatic')

    # Crontab
    sudo('cat '+ COMMON_PATH_REPO +'crontab.txt | crontab -')
    

    # PGPOOL 2

    sudo('apt-get install pgpool2')

    mensaje_configuracion_pgpool = """
    Ahora debe editar el archivo /etc/pgpool.conf

        port = 5432
        connection_life_time = 90
        client_idle_limit = 90
        backend_hostname0 = 'localhost'
        backend_port0 = 5432
        backend_weight0 = 1

    """
    pedir_confirmacion(mensaje_configuracion_pgpool)

    mensaje_configuracion_postresql = """
    Ahora debe editar el archivo /etc/postgresql/9.1/main/postgresql.conf

        port = 5432

    """
    pedir_confirmacion(mensaje_configuracion_postresql)

    sudo('service postgresql stop')
    sudo('service postgresql start')
    sudo('service pgpool2 restart')

    # Configuración en servidores:

    #Servidor de adkiller:
    #Llamo a 'manage.py migrar lineatiempo' desde el crontab
    #Usuario: infoad
    #contraseña: 4h8a9s874k

    #ip: 174.37.209.218

    #Servidor simpson:
    #En ~/projects/adkiller/ hay un script thumbs que genera los thumbs y otro que genera flvs

    #Llamarlos desde crontab cada 15 minutos

    # Configuración de la base de datos

    #Si hay problemas de codificación, ver http://airbladesoftware.com/notes/fixing-mysql-illegal-mix-of-collations

    # ssh
    #Genero una clave Publica en adbout: ssh-keygen -t rsa

    #Copio la clave Publica desde adbout: .ssh/id_rsa.pub

    #Entro a infoad: cd /home/tools

    #Pego la clave Publica en infoad: sudo mcedit .ssh/authorized_keys
    #Entrar al PhpMyadmin

    #User: root pass: qwertyui
    #Ejemplo con puntos

    #http://chart.googleapis.com/chart?chs=440x220&amp;cht=lxy&amp;chco=3072F3,FF0000&amp;chd=t:10,20,40,80,90,95,99%7C20,30,40,50,60,70,80%7C-1%7C5,10,22,35,85&amp;chdl=Ponies%7CUnicorns&amp;chdlp=b&amp;chls=2,4,1%7C1&amp;chma=5,5,5,25&amp;chm=p,FF00BB,1,-1,15
    #Levantar infoAD en Apache

    #sudo('apt-get install apache2')

    #crear sites-enabled
    #crear sites-available
    #copiar httpd.conf
    #copiar wsgi.py
    #Instalar WSGI
    #sudo('apt-get install apache2-dev')

    #editar /etc/hosts

    #https://code.google.com/p/modwsgi/wiki/QuickInstallationGuide


    


#===================================================================
# FUNCIONES AUXILIARES
#===================================================================

@contextmanager
def cd_contextmanager(path):
    """Cd seguro"""
    with cd(path):
        yield


def is_installed(package):
    with settings(warn_only=True):
        result = run('which %s' % package)
        return result.succeeded


def pedir_confirmacion(mensaje):
    print
    print "================"
    print "    ATENCIÓN"
    print
    while True:
        if confirm(mensaje, default=False):
             return


