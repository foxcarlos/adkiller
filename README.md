# Infoad

## Instalación en Linux

1. Recomendamos instalar virtualenv y virtualenvwrapper:
    - sudo pip install virtualenv
    - sudo pip install virtualenvwrapper
    - agregar al .bashrc:
        * source /usr/local/bin/virtualenvwrapper.sh
        * export WORKON_HOME=~/Envs
    - mkvirtualenv infoad
    - Ver http://virtualenvwrapper.readthedocs.org/en/latest/tips.html#creating-project-work-directories

1. Dependencias:
    - sudo apt-get install libpq-dev python-dev
    - sudo apt-get install postgresql
    - sudo apt-get install postgresql-server-dev-9.3
    - sudo apt-get install python2.7-dev

1. Clonar el repo:
    - sudo apt-get install git
    - (si no están creadas las claves privadas) ssh-keygen
    - configurar claves en git
    - git clone <this_repo> infoad
    - workon infoad
    - pip install -r requirements.txt

1. No queremos que corra los test de autocomplete_light, porque fallan:    
    - buscar la carpeta autocomplete_light
    - cd esa carpeta
    - mv tests tests2

1. Local settings:
    - cd adkiller
    - cp local_settings.py.template local_settings.py
    - Editar local_settings.py y rellenar los placeholders con los datos que correspondan

1. Si se agrega alguna librería nueva, hacer:
    - pip freeze > requirements.txt



## Instalación en Windows

1. Instalar Python, pip y virtualenv:
    - Instalar Python 2.7
    - Descargar https://raw.githubusercontent.com/pypa/pip/master/contrib/get-pip.py
    - python.exe C:\Path\to\get-pip.py
    - Agregar C:\Python27\Scripts a la variable de entorno PATH
    - pip.exe install virtualenv


1. Clonar el repo:
    - sudo apt-get install git
    - (si no están creadas las claves privadas) ssh-keygen
    - configurar claves en git
    - git clone <this_repo> infoad
    - (dentro del virtualenv)
    - pip install -r requirements.txt

1. Local settings:
    - cd adkiller
    - copy local_settings.py.template local_settings.py
    - Editar local_settings.py y rellenar los placeholders con los datos que correspondan
    - Agregar MEDIAMAP_ROOT = ''


# Levantar base de datos (PostgreSQL)

## Crear usuario y db 
* sudo -u postgres psql 
* postgres=# CREATE USER infoad WITH PASSWORD 'infoad'; 
* postgres=# CREATE DATABASE infoad OWNER infoad; 
* postgres=# ALTER USER infoad CREATEDB; 
*  \q
* Agregar al /etc/postgresql/VERSION/main/pg_hba.conf
  
```
#!bash

local all all trust
host all all 127.0.0.1/32 trust
```
* sudo service postgresql restart
* sudo useradd infoad
* sudo passwd infoad
* * Para utilizar una base con datos precargados, instalar Dropbox y pedir a Diego invitación a las carpetas compartidas del proyecto. Luego ejecutar:
  
    - psql -U infoad -W infoad -h localhost < ~/Dropbox/adKiller/database/infoad.sql
    - python manage.py syncdb
    - python manage.py migrate

1. Para empezar con una base de datos en blanco:
  
    - python manage.py syncdb (no crear usuario)
    - python manage.py migrate
    - python manage.py shell
         - from django.contrib.auth.models import User
         - User.objects.create_superuser("root", "r@r.com", "r")

1. - python manage.py collectstatic
   - python manage.py runserver

 
# Deploy

1. Instalar:
    - sudo apt-get install supervisor nginx
    - sudo pip install gunicorn
2. Archivos de configuración:
    - /etc/supervisor/supervisord.conf
    - /etc/nginx/sites-enabled/infoad
    - adkiller/wsgi.py (gunicorn)
    - gunicorn_start.bash
    - pip install setproctitle




# Otros

## Si hay problemas de codificación, ver:

    http://airbladesoftware.com/notes/fixing-mysql-illegal-mix-of-collations
    
## PGPOOL 2

    - http://www.pgpool.net/mediawiki/index.php/Main_Page

    apt-get install pgpool2   
      
    nano /etc/pgpool.conf   
    port = 5432   
    connection_life_time = 90   
    client_idle_limit = 90   
    backend_hostname0 = 'localhost'   
    backend_port0 = 5432   
    backend_weight0 = 1   
      
    nano /etc/postgresql/9.3/main/postgresql.conf   
    port = 5432   
      
    service postgresql stop   
    service postgresql start   
    service pgpool2 restart


# Librerias

- Twitter Bootstrap: responsive CSS and HTML framework
- Font Awesome: icons
- RequireJS: modular javascript loading
- CanJS: javascript framework
- git clone https://github.com/andymckay/lazy_paginator



## Ejemplo con puntos
http://chart.googleapis.com/chart?chs=440x220&cht=lxy&chco=3072F3,FF0000&chd=t:10,20,40,80,90,95,99%7C20,30,40,50,60,70,80%7C-1%7C5,10,22,35,85&chdl=Ponies%7CUnicorns&chdlp=b&chls=2,4,1%7C1&chma=5,5,5,25&chm=p,FF00BB,1,-1,15






## Instalar wkhtmlToPDF (command line utility para generar PDFs)
sudo apt-get install xvfb

Desde https://code.google.com/p/wkhtmltopdf/downloads/list
descargar wkhtmltopdf-0.11.0_rc1-static-i386.tar.bz2

Desde una consola, vamos hasta la carpeta donde guardamos el archivo anterior,
y hacemos:

tar xvjf wkhtmltopdf-0.11.0_rc1-static-i386.tar.bz2

sudo chown root:root wkhtmltopdf-i386 

sudo cp wkhtmltopdf-i386 /usr/local/bin/wkhtmltopdf



# Mediamap:

copiar local_config.js.template en local_config.js y setear los valores