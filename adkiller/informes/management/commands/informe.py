# -*- coding:  iso-8859-1 -*-

import codecs
from adkiller.app.models import Emision
from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.models import User
from adkiller.app.models import Industria
import datetime
from adkiller.filtros.utils import opciones, get_dict_filtros, FILTERS
from adkiller.filtros.models import Perfil, OpcionFiltro, Filtro, Periodo

from adkiller.informes.exportpdf import *
from adkiller.app.models import Producto

import os


def enviar_resumen(lapso):
    usuarios = User.objects.all()
    hoy = datetime.date.today()
    desde = hoy - datetime.timedelta(days=lapso)
    for usuario in usuarios:
        resumen(usuario, desde, hoy, lapso)
#    to =  ["illbelemaxi@gmail.com"]
#    resumen = ""
#    if to:
#        titulo = 'Resumen de adbout'
#        email = EmailMultiAlternatives(titulo,resumen, to[0] , to=to)
#        email.content_subtype = "html"
#        email.send()


def resultados_to_tabla(resultados, columnas, titulo, encabezados):
    if not resultados:
        return ""
    res = "<br>"

    if titulo in FILTERS:
        titulo = FILTERS[titulo].plural.encode("utf-8")

    style = 'padding: 5px 10px 5px 30px;margin: 0;font-size: 14px;' +\
    'border-top: 1px solid gainsboro;border-bottom: 1px solid #AFAFAF;' +\
    'position: relative;font-family: FontAwesome;font-weight: normal;' +\
    'font-style: normal;text-decoration: inherit;width: 499px;'

    res = "<div id='filtro' style='%s'> " % style + titulo + "</div>"
    res += "<span><table border='1' >"
    for encabezado in encabezados:
        res += "<th>" + encabezado + "</th>"

    for resultado in resultados:
        res += "<tr>"
        for columna in columnas:
            if columna == "subio":
                if resultado["subio"]:
                    res += "<td> <img src='http://adbout.com/static/design/up.gif' > " + str(resultado["antes"]) +"%</td>"
                else:
                    res += "<td><img src='http://adbout.com/static/design/down.gif' > " + str(resultado["antes"]) +"%</td>"
            else:
                res += "<td>" + str(resultado[columna] )

                if columna == "porcentaje":
                    res +="%"
                res += "</td>"
        res += "</tr>"
    res += "</table></span></div>"
    return res


def resumen(usuario, desde, hasta, lapso):
    perfil = usuario.get_profile()
    filtro = perfil.get_filtro(key="industria")
    options = filtro.opciones.filter(inicial=True)
    periodo = Periodo.objects.filter(perfil=perfil)[0]

    temp_desde = periodo.fecha_desde
    temp_hasta = periodo.fecha_hasta

    periodo.fecha_desde = desde
    periodo.fecha_hasta = hasta
    periodo.save()
    body = ""

    if True:
        dict = get_dict_filtros(perfil, append_id=True,initial=False)
        emisiones = Emision.objects.filter(fecha__gte=periodo.fecha_desde,fecha__lte=periodo.fecha_hasta)
        graph_unit = "fecha"
        dict['fecha__lte'] = periodo.fecha_hasta
        dict['fecha__gte'] = periodo.fecha_desde
        respuestas = opciones(dict,emisiones,periodo,graph_unit,usuario,"cantidad")
        path = "archivo.html"
        archivo = codecs.open(path, encoding='utf-8',mode="w+")


        encabezado = '<head><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head>'
        archivo.write(encabezado)
        ## Aca hay uno solo
        fechas = desde.strftime("%d/%m/%Y") + " al "+  hasta.strftime("%d/%m/%Y")

        archivo.write(fechas)
        body += fechas
        titulo = "<h3> <div align='center'>  Cantidad de Emisiones"
        for opcion in options:
            industria = Industria.objects.get(id=opcion.opcion_id)
            titulo += " " + str(industria)
        titulo += "</div></h3>"

        body += titulo
        archivo.write(titulo)

        grafico_content = "<div align='center' >"
        grafico_content += grafico_src(respuestas[0],"Venta Directa")
        grafico_content += "</div>"
        archivo.write(grafico_content)
        body += grafico_content

        encabezados = ["Nombre", "Cantidad", "Porcentaje", "Comparativo"]

        columnas = ["item",'absoluto_acotado',"porcentaje","subio"]

        cantidad = "cantidad"

        tabla = obtener_tabla("producto","Productos",cantidad,columnas,encabezados,perfil)
        body += tabla
        archivo.write(tabla)

#       marcas = resultados_to_tabla(resultados,columnas,"Marcas",encabezados)

        medios = obtener_tabla("medio","Medios",cantidad,columnas,encabezados,perfil)
        body += medios
        archivo.write(medios)

        programas = obtener_tabla("programa","Programas",cantidad,columnas,encabezados,perfil)
        body += programas
        archivo.write(programas)

# Documentacion https://developers.google.com/chart/image/docs/chart_params
#http://chart.apis.google.com/chart?chs=440x220&cht=lxy&chco=3072F3,FF0000&chd=t:10,20,40,80,90,95,99|0,30,40,50,60,70,80|-1|0,0,0,10,22,35,85&chdl=Ponies|Unicorns&chls=2,4,1|1,0,100,10&chxt=y,x&chds=a : Eje de las y automatico

# chxt=x,y es para poner el eje de las x y de las y's

# http://psychopyko.com/tutorial/how-to-use-google-charts/

#https://chart.googleapis.com/chart?cht=lc&chco=FF0000,00FF00,0000FF&chs=200x125&chd=s:FOETHECat,lkjtf3asv,KATYPSNXJ&chxt=x,y&chxl=0:|Oct|Nov|Dec|1:||20K||60K||100K

#http://chart.apis.google.com/chart?
#cht=lc&chs=450x330&chd=t:7,18,11,26,22,11,14&
#chxr=1,0,30&chds=0,30&
#chco=4d89f9&
#chxt=x,y&chxl=0:|Mon|Tue|Wed|Thu|Fri|Sat|Sun&
#chls=3,1,0&
#chm=d,4d89f9,0,0,12,0|d,4d89f9,0,1,22,0|d,4d89f9,0,2,12,0|d,4d89f9,0,3,12,0|d,4d89f9,0,4,12,0|d,4d89f9,0,5,12,0|d,4d89f9,0,6,12,0
#Interlineado &chg=0,6.67,5,5

#http://chart.apis.google.com/chart?chs=440x220&cht=lxy&chco=3072F3,FF0000&chd=t:10,20,40,80,90,95,99|0,30,40,50,60,70,80|-1|0,0,0,10,22,35,85&chdl=Ponies|Unicorns&chls=2,4,1|1&chxr=0,0,100,10|1,0,100,10&chxt=y,x&chof=png

    periodo.fecha_desde = temp_desde
    periodo.fecha_hasta = temp_hasta
    periodo.save()

#        resultados, has_more, has_less = obtener_opciones(perfil,"cantidad",filtro,0,50)
#        for resultado in resultados:

# Ver si surgio un nuevo producto reccorer todos productos y ver si no tienen una emision
# en fechas anteriores

# Comparativo con la semana anterior

# Comparativo general de emisiones ...


def obtener_cantidades_html(perfil):
    html = ""
    unit,sum_total,cantidad_anuncios,cantidad_segundos,cantidad_inversion = cantidades_filtradas(perfil)
    html += "<table border=5>"

    html += "<td>%s Anuncios</td><td>%s Inversion</td><td>%s Segundos</td>" % (str(cantidad_anuncios), str(cantidad_inversion),str(cantidad_segundos))

    html += "</table>"

    return html


class Command(BaseCommand):
    """ Recibe la cantidad desde donde tiene que tomar datos hasta la fecha_actual"""
    args = '<dias>'
    def handle(self, *args, **options):
        to = ["pablocelayes@gmail.com"]

        dias = int(args[0])

        hoy = datetime.date.today()
        desde = hoy - datetime.timedelta(days=dias)
        hasta_vs = hoy - datetime.timedelta(days=dias+1)
        inicial = desde - datetime.timedelta(days=dias+1)

        productos_nuevos = Producto.objects.filter(creado__gte=desde)

        # Tomo los ultimos 4 producots que tengan thumb visible
        productos = []
        for producto in productos_nuevos:
            aux = producto.thumb()
            if aux:
                thumb = aux.encode("utf-8")
                productos.append((producto,thumb))
        productos = productos[:10]

        usuarios = User.objects.all()
        for usuario in usuarios:
            perfil = Perfil.objects.get(user=usuario,activo=True)
            if verificar_permisos_cliente(perfil,dias):
                tipo_cliente = obtener_tipo_de_cliente(Perfil.objects.get(user=usuario,activo=True))
                # Genero el contenido

                # Esto genera una Busqueda en un usuario
                nuevo = perfil.clonar()
                nuevo.activo = False
                nuevo.save()

                Periodo(perfil=nuevo,fecha_desde=desde,fecha_hasta=hoy).save()

                subject = "Novedades Adbout"

                inputfile,body,x,y = obtener_html(nuevo)
                Periodo(perfil=nuevo,fecha_desde=inicial,fecha_hasta=desde).save()

                body = subject_mail(nuevo,dias,hoy,desde,hasta_vs,inicial,productos)

#                file = open("/home/maxi/Escritorio/file.html","w+")
#                file.write(body)
#                file.close()

                msg = EmailMultiAlternatives(subject,body, to[0], to)
                msg.content_subtype = "html"

                nuevo.delete()
                outputfile = inputfile.replace(".html",".pdf")
                
                #Convertir a PDF
                os.system(' xvfb-run -a -s "-screen 0 640x480x16" wkhtmltopdf  --page-width 210 --page-height 297 --margin-right 0 --margin-top 0 --margin-bottom 0 --margin-left 0 %s %s' % (inputfile, outputfile))

                #Abrir PDF
                pdf = open(outputfile, 'r')
                content = pdf.read()
                pdf.close()

                msg.attach('ticket.pdf',content,'application/pdf')
                msg.send()

        # Obtengo el PDF y lo adjunto
        #
        #        msg = EmailMultiAlternatives(subject, text_content, to[0], to)
        #        msg.attach_alternative(html_content, "text/html")
        #        msg.send()

def subject_mail(perfil,dias,hoy,desde,hasta_vs,inicial,productos):
    unidad,suma_total,anuncios,segundos,inversion = cantidades_filtradas(perfil)
    templ = get_template("email_resumen.html")
    ctx = {}

    ctx["productos"] = productos
    ctx["hasta"] = hoy.strftime("%d/%m/%Y")
    ctx["desde"] = desde.strftime("%d/%m/%Y")
    ctx["hasta_vs"] = hasta_vs.strftime("%d/%m/%Y")
    ctx["inicial"] = inicial.strftime("%d/%m/%Y")
    ctx['dias'] = dias + 1
    ctx['nombre'] = perfil.user.username
    ctx["anuncios"] = anuncios
    ctx["segundos"] = segundos
    ctx["superficie"] = inversion
    html = templ.render(Context(ctx)).encode("utf-8")

    return html


def obtener_tipo_de_cliente(perfil):
    """ Devuelve que cliente es segun sus filtros iniciales"""
    opciones = OpcionFiltro.objects.filter(filtro__perfil=perfil)
    respuesta = "Total"
    if opciones:
        for opt in opciones:
            respuesta = opt.filtro.tipo.name

    return respuesta


def verificar_permisos_cliente(perfil, dias):
    """ Devuelve un bool que dice si el cliente puede o no ver los ultimos dias"""
    puede = True
    periodo = Periodo.objects.get(perfil=perfil)
    if periodo.init_fecha_hasta:
        puede = periodo.init_fecha_hasta >= hoy
    if periodo.init_fecha_desde:
        puede = puede and periodo.init_fecha_desde <= hoy - datetime.timedelta(days=dias)
    return puede
