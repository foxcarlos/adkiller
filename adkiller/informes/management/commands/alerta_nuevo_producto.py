    # -*- coding:  utf-8 -*-

# Python Imports
import datetime

# Django Imports
from django.contrib.contenttypes.models import ContentType
from django.template.loader import get_template
from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives, EmailMessage,\
            get_connection
from django.template import Context
#from django.core.urlresolvers import reverse

# Project Imports
from adkiller.settings import MAIL_ALERTAS_CONF, SITE_URL
from adkiller.app.models import Producto, Emision
from adkiller.filtros.models import Perfil, Filtro
from adkiller.filtros.utils import obtener_opciones

""" 
	Alerta de nueva campaña
"""
class Command(BaseCommand):
    def handle(self, *args, **options):

    	mails_alertas = []

        hace_dos_dias = datetime.date.today() - datetime.timedelta(days=2)
    	# Busco las busquedas que tengan activada la alerta de nueva campaña
        busquedas = Perfil.objects.filter(alerta_producto=True)

        productos_nuevos = Producto.objects.filter(nuevo=True)

        # crear clip del patron
        # for producto in productos_nuevos:
            # for emi in producto.emisiones.all():
                # seg = Segmento.from_ad(emi)
                # status = seg.make_clip(...)
                # while not clip exists:
                    # wait
                # if status == ...:
                    # break

        for busqueda in busquedas:
            print busqueda
            # Busco todos los productos dentro del alcance de la busqueda
            filtro = busqueda.get_or_crear_filtro(key='producto')
            opciones = obtener_opciones(busqueda, busqueda.get_unit(),filtro,0,0,False)
            if opciones == ([], False, False):
                # esto devuelve cuando no hay resultados
                id_productos = []
            else:
                id_productos = [p['producto__id'] for p in opciones]
        	# Busco los productos nuevos dentro del alcance
            nuevos = []
            productos_nuevos_scope = Producto.objects.filter(nuevo=True).filter(id__in=id_productos).exclude(emisiones__fecha__lt=hace_dos_dias)
            for p in productos_nuevos_scope:
                emisiones_48_hs = Emision.objects.filter(fecha__gte=hace_dos_dias).filter(producto=p).order_by('fecha')
                if emisiones_48_hs:
                    nuevos.append(p)

            # Nombre de usuario
            nombre_usuario = busqueda.user.first_name
            if not nombre_usuario:
                nombre_usuario = busqueda.user.username

        	# Si hay nuevos productos enviar email 
            if nuevos:
                subject = 'Alerta InfoAD - %s (%d)' % (busqueda.nombre, len(nuevos)) 

                template = get_template("email_alerta_nuevo_producto.html")

                ctx = {'url_productos':nuevos,'SITE_URL':SITE_URL,
                       'nombre_usuario':nombre_usuario,'busqueda':busqueda.nombre,'id_perfil':busqueda.id}

                msg_body = template.render(Context(ctx)).encode("utf-8")
                to_addr = busqueda.user.email
                from_addr = "alertas@infoad.com.ar"
                recipients = [to_addr] 
                msg = EmailMessage(subject, msg_body, from_addr, recipients)
                msg.content_subtype = "html"
                mails_alertas.append(msg)

        mail_connection = get_connection(**MAIL_ALERTAS_CONF)
        mail_connection.open()
        mail_connection.send_messages(mails_alertas)
        mail_connection.close()


        # Al enviar el mail los pongo en nuevo == False
        if productos_nuevos:
            productos_nuevos.update(nuevo=False)
