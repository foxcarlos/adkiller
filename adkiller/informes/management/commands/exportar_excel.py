# -*- coding:  iso-8859-1 -*-

import codecs
from adkiller.app.models import Emision
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from adkiller.filtros.models import Perfil

from adkiller.app.utils import generar_csv

import os



class Command(BaseCommand):
    """ Exporta el excel que esta viendo un usuario """
    args = '<usr>'
    def handle(self, *args, **options):
        perfil = Perfil.objects.get(user__username=args[0], activo=True)
        generar_csv(perfil, True, filename="tabla.csv")