# -*- coding:  utf-8 -*-
# Python Imports
#from smtplib import SMTP
import datetime

# Django Imports
from django.template.loader import get_template
from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives, EmailMessage,\
            get_connection
from django.template import Context
from django.core.urlresolvers import reverse

# Project Imports
from adkiller.informes.models import *
from adkiller.settings import MAIL_ALERTAS_CONF, SITE_URL

""" 
    Este comando revisa todas las alertas, verifica si cambiaron de estado
    y envía notificaciones. 

    Si el periodo de la alerta es de un día, se envía notificación
    siempre que la condición se siga cumpliendo. Y también cuando se deja
    de cumplir.
"""
class Command(BaseCommand):
    def handle(self, *args, **options):
        alertas = Alerta.objects.all()
        mails_alertas = []
        for alerta in alertas:
            periodo_principal = alerta.perfil.get_periodo_or_create()
            lapso = periodo_principal.get_lapso_display()

            # FIXME: no deberían haber alertas sin lapso
            if lapso:
                respuesta = alerta.chequear()
            else:
                continue
           
            if respuesta['cambio'] or respuesta['sucedio'] and lapso == 'dia':
                def get_url_busqueda(site_url, user_id, busqueda_id):
                    return site_url + '/filtros/mostrar_busqueda/' + str(busqueda_id)

                es_maximo = SUPERO_EL_MAXIMO in respuesta['alertas']
                es_minimo = BAJO_DEL_MINIMO in respuesta['alertas']
                referencia = respuesta['referencia']                
                valor = respuesta['valor']                
                subject = "Alerta InfoAD: %s" % alerta.perfil.nombre
                template = get_template("email_alerta.html")
                unidad = alerta.perfil.unidad                
                url_busqueda = get_url_busqueda(SITE_URL, alerta.perfil.user.id, alerta.perfil.id)

                nombre_usuario = alerta.perfil.user.first_name
                if not nombre_usuario:
                    nombre_usuario = alerta.perfil.user.username


                ctx = {'alerta': alerta, 'sucedio': respuesta['sucedio'],
                        'nombre_usuario': nombre_usuario,
                        'valor_alcanzado': render_valor(valor, unidad),
                        'magnitud': render_magnitud(unidad),
                        'periodo': render_periodo(lapso), 'lapso': lapso,
                        'es_maximo': es_maximo, 'es_minimo': es_minimo,
                        'valor_de_referencia': render_valor(alerta.valor, unidad),
                        'url_busqueda': url_busqueda,'SITE_URL':SITE_URL
                        }

                msg_body = template.render(Context(ctx)).encode("utf-8")
                to_addr = alerta.perfil.user.email
                from_addr = "alertas@infoad.com.ar"
                recipients = [to_addr] 
                msg = EmailMessage(subject, msg_body, from_addr, recipients)
                msg.content_subtype = "html"
                mails_alertas.append(msg)
        mail_connection = get_connection(**MAIL_ALERTAS_CONF)
        mail_connection.open()
        mail_connection.send_messages(mails_alertas)
        mail_connection.close()


def render_valor(valor, unidad):
    if valor is None:
        return None
    valor = str(valor)
    valor = valor.replace("$", "").replace(".", "")
    valor = format(int(valor), ',').replace(',', '.')
    if unidad == "cantidad":
        res = valor + " anuncios"
    elif unidad == "volumen":
        res = valor + u" segs/cm²"
    elif unidad == "inversion":
        res = "$" + valor
    else:
        res = ""
    return res


def render_periodo(lapso):
    if lapso == "semana":
        ret = u"la última " + lapso
    elif lapso in ["dia", "mes", u"ano"]:
        ret = u"el último " + lapso
    else:
        ret = "unknown lapso"
    return ret


def render_magnitud(magnitud):
    if magnitud == "inversion":
        ret = u"una inversión"
    elif magnitud == "cantidad":
        ret = "una cantidad"
    elif magnitud == "volumen":
        ret = "un volumen"
    else:
        ret = ""
    return ret

