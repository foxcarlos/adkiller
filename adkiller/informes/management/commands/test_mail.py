# -*- coding:  utf-8 -*-
from django.core.mail import EmailMultiAlternatives, get_connection, send_mail,\
							EmailMessage

# FIXME: traer de settings
# Mail de alertas
MAIL_ALERTAS_CONF = {
    'host': 'smtp.gmail.com', 
    'port': 587, 
    'username': 'alertas@infoad.com.ar', 
    'password': 'gr4ndcl4ss3', 
    'user_tls': True
}

from_addr, to = 'alertas@mail.infoad.com.ar', 'pablocelayes@gmail.com'

alertas_connection = get_connection(**MAIL_ALERTAS_CONF) 
# Manually open the connection
alertas_connection.open()

errores_connection = get_connection()

# Construct an error e-mail
email1 = EmailMessage('Todo mal', 'Che, se pudrió todo!', from_addr,
                          [to], connection=errores_connection)
# Send the email
email1.send()

# Construct two alert emails
email2 = EmailMessage('Vos fijate', 'Eh, loco, te alerto que pasó tal cosa', from_addr,
                          [to])
email3 = EmailMessage('Vos fijate', 'Otra alerta más!', from_addr,
                          [to])

# Send the two emails in a single call
alertas_connection.send_messages([email2, email3])

# We need to manually close the connections.
alertas_connection.close()
errores_connection.close()
