from django.core.management.base import BaseCommand
from django.core.mail import EmailMultiAlternatives
from adkiller.filtros.models import Perfil


class Command(BaseCommand):

	def handle(self, *args, **options):
		perfiles = Perfil.objects.filter(user__is_active=True, user__is_staff=False).select_related().distinct('user')
		l = []
		for p in perfiles:
		    if p.filtros_to_dict(True) == {}:
		    	l.append(p)

		subject = "alertas@infoad.com.ar" 
		from_email = "Usuarios con alcance completo"
		html = '<ul>'
		for x in l:
		    html += '<li>'+ x.user.username + '</li>'
		html += '</ul>'
		html_content = html
		msg = EmailMultiAlternatives(subject, html_content, from_email, ["froldan@infoad.com.ar", "diegolis@infoad.com.ar"])
		msg.attach_alternative(html_content, "text/html")
		msg.send()