# -*- coding: utf-8 -*-

# Python Imports

# Django Imports
from django.db.models import Count, Sum
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.contrib.contenttypes.models import ContentType
from django.db import connection

# Project Imports
from adkiller.app.models import Emision, Producto, Ciudad
from adkiller.filtros.models import Periodo, Filtro
from adkiller.filtros.utils import get_dict_filtros, FILTERS, format_con_comas,\
    get_Q_from_dict, obtener_opciones, reparar_consulta, cantidades_filtradas,\
    get_qs_emisiones_filtradas, get_agg_for_magnitude, get_resultados_from_queryset
from adkiller.filtros.views import obtener_filtros

SOURCE_PDF = settings.SOURCE_PDF
STATIC_URL = settings.STATIC_URL


def grafico_src(diccionario, referencia, desde, hasta):
    """Recibe una lista de diccionarios de la forma ["item"],["valor"]"""
    src = "http://chart.googleapis.com/chart?chs=980x144&cht=lxy&" +\
                                        "chco=3072F3,FF0000&chls=6,6,0&chd=t:"
    src += ",".join([str(r) for r in range(0, len(diccionario))])
    src += "|"

    valores = []

    eje_x = []
    for elem in diccionario:
        valores.append(elem["valor"])
        eje_x.append(elem["item"])
    src += ",".join([str(r[0]) for r in valores])

    if referencia:
        src += "&chdl=" + referencia
    src += "&chdlp=t"

    # 
    src += "&chds=a"

    # dos leyendas del eje X y una del eje Y
    src += "&chxt=x,x,y"

    src += "&chxs=0,676767,11.5,1,l,676767"

    # labels del eje X (primera fila)
    src += "&chxl=0:|"
    if desde == hasta:
        src += "|".join(eje_x)
        src += "|"
    else:
        for elem in eje_x:
            src += elem.strftime("%d") + "|"
            
    # labels del eje X (segunda fila)
    src += "1:|" + desde.strftime("%d-%m-%Y") + "|" + hasta.strftime("%d-%m-%Y")

    # posicion de los labels (primera fila)
    src += "&chxp=0"
    #if desde == hasta:
    #    src += "|".join(eje_x)
    #    src += "|"
    #else:
    for elem in range(len(eje_x)):
        src += "," + str(elem) + ".5"

    # posicion de los labels (segunda fila)
    src += "|1,0,%d" % (len(eje_x))
    
    # colores
    src += "&chma=0,40,10,10&chm=p,FF00BB,1,-1,15"

    # Marcadores
    # https://developers.google.com/chart/image/docs/chart_params#gcharts_line_markers
    src += "&chm=" + "|".join(["o,3072F3,0,%d,8,0" % i for i in
                                                        range(len(valores))])

    return src


def opciones(perfil, periodo, graph_unit, user, unidad):
    """
        devuelve un results 
    """
    # FIXME: ver qué hace, distinguir de filtros/utils.py.obtener_opciones
    unit = True
    nueva = False
    
    dict_filtros = get_dict_filtros(perfil)
    Q_principal = get_Q_from_dict(dict_filtros, perfil, lookup="")
    
    if nueva:
        qs_emisiones_filtradas = Emision.objects.all().filter(Q_principal)
        lista = qs_emisiones_filtradas.values(graph_unit).order_by(graph_unit).distinct()
        if unidad == "inversion":
            lista = aplicar_descuentos(lista)
        else:
            agg = get_agg_for_magnitude(unidad)
            
            lista = lista.annotate(valor=agg)
    else:
        lista = Emision.objects.all().filter(Q_principal)
        # Aca hace falta agregar Descuentos en Tarifas, para que se apliquen
        #  los descuentos en el grafico si la unidad es "inversion"
        lista = lista.values(graph_unit).order_by(graph_unit).distinct()

        if unidad == "cantidad":
            lista = lista.annotate(valor=Count(graph_unit))
        elif unidad == "volumen":
            lista = lista.annotate(valor=Sum('duracion'))
        elif unidad == "inversion":
            lista = lista.annotate(valor=Sum('valor'))


    if not periodo:
        perfil = Perfil.objects.get(user=user,activo=True)
        periodo = Periodo.objects.filter(perfil=perfil)[0]

    estado = periodo.fecha_desde == periodo.fecha_hasta
    lapso = periodo.get_lapso_display()
    if lapso or estado:
        if estado or lapso == "dia":
            unit = False
            output_dict= []
            cont = 0
            for x in range(24):
                desde = datetime.time(x, 0)
                lista2 = lista.filter(hora__gte=desde)
                if x < 23:
                    hasta = datetime.time(x+1, 0)
                    lista2 = lista2.filter(hora__lt=hasta)
                y = len(lista2)
                cont += y
                fecha = periodo.fecha_desde
                if type(fecha) == str:
                    fecha = datetime.datetime.strptime(fecha, "%Y-%m-%d")
                output_dict.append({'item': str(x),'year':fecha.year,'mes':fecha.month,'dia':fecha.day, 'valor': [y]})
            return (output_dict, unit)

    output_dict = []
    for r in lista:
        if graph_unit == "fecha":
            x = r[graph_unit]
        else:
            x = str(periodo.fecha_desde) + " " + str(r[graph_unit])
        output_dict.append({'item': x, 'valor': [r['valor']]})

    return (output_dict, unit)


def obtener_grafico(perfil):
    periodo = Periodo.objects.filter(perfil=perfil)[0]

    graph_unit = "fecha"
    if periodo.fecha_desde == periodo.fecha_hasta:
        graph_unit = "hora"

    respuestas, unidad = opciones(perfil, periodo, graph_unit, perfil.user,
                            perfil.get_unit())
    grafico_content = ""
    grafico_content += grafico_src(respuestas, "", periodo.fecha_desde,
                                                        periodo.fecha_hasta)
    return grafico_content


def obtener_filtros_html(perfil):
    elegidos = obtener_filtros(False, perfil, False, False)
    return elegidos


def obtener_html(perfil):
    ctx = {}
    templ = get_template("pdf.html")

    filename = SOURCE_PDF + perfil.user.username + ".html"
    ar = open(filename, "w+")
    ctx["grafico"] = obtener_grafico(perfil)
    filtros = Filtro.objects.filter(perfil=perfil)

    ctx['settings'] = settings
    ctx['perfil'] = perfil
    ctx["filtros"] = obtener_filtros_html(perfil)

    periodo = perfil.periodos.all()[0]

    if perfil.nombre == u"Búsqueda":
        ctx["nombre_informe"] = "Informe ejecutivo"
    else:
        ctx["nombre_informe"] = perfil.nombre
    ciudad = filtros.filter(tipo__name="ciudad")
    if ciudad:
        ciudad = ciudad[0]
        if len(ciudad.opciones.all()) > 1:
            ctx["ciudades"] = ",".join([Ciudad.objects.get(id=c.opcion_id).
                                        nombre for c in ciudad.opciones.all()])
        elif len(ciudad.opciones.all()) == 1:
            ctx["ciudad"] = Ciudad.objects.get(id=ciudad.opciones.all()[0].
                                                                    opcion_id)

    ctx["fecha_desde"] = periodo.fecha_desde
    ctx["fecha_hasta"] = periodo.fecha_hasta

    unit, sum_total, cantidad_anuncios, cantidad_segundos, cantidad_inversion =\
                                                cantidades_filtradas(perfil)

    ctx["unidad"] = unit
    ctx["cantidad"] = cantidad_anuncios
    ctx["volumen"] = cantidad_segundos
    ctx["inversion"] = cantidad_inversion

    ctx["torta_marca"] = grafico_torta(perfil, "marca", "Marcas")
    torta = grafico_torta(perfil, "producto", "Productos")
    ctx["torta_producto"] = torta
    ctx["torta_sector"] = grafico_torta(perfil, "soporte", "Soportes")

    ctx["elegidos"] = []

    for filtro in filtros:
        if str(filtro.tipo) not in  ["producto", "agencia"]:
            encabezados = ["Nombre", "Cantidad", "Porcentaje", "Comparativo"]
            columnas = ["item", 'absoluto_acotado', "porcentaje", "subio"]
            ctx["elegidos"].append((obtener_tabla(str(filtro.tipo),
                str(filtro.tipo), perfil.get_unit(), columnas, encabezados,
                perfil, filtro.cantidadActual),
                FILTERS[str(filtro.tipo)].plural))
        elif str(filtro.tipo) == "producto":
            ctx["productos"] = display_productos(filtro, perfil,
                                                filtro.cantidadActual)

    ctx["elegidos_count_half"] = len(ctx["elegidos"]) / 2

    ctx["STATIC_URL"] = STATIC_URL
    html = templ.render(Context(ctx)).encode("utf-8")
    ar.write(html)
    ar.close()

    from django.template.loader import render_to_string
    from django.utils.html import strip_tags

    html_content = render_to_string("pdf.html", Context(ctx))
    text_content = strip_tags(html_content)

    return (filename, html, html_content, text_content)


def grafico_torta(perfil, modelo, titulo):
    filtro = perfil.get_filtro(modelo)
    if filtro:
        resultados, has_more, has_less = obtener_opciones(perfil,
                                        perfil.get_unit(), filtro, 0, 10)
        eje_x = []
        eje_y = []
        # reemplazo los espacios por un +, si no se corta la url
        for elem in resultados[:5]:
            eje_x.append(elem['item'].replace(" ", "+"))
            eje_y.append(elem['valor'])
        return obtener_url_grafico_torta(eje_x, eje_y, 600, 399, titulo)


def obtener_url_grafico_torta(eje_x, eje_y, ancho, alto, titulo):
    """ Funcion que toma una lista de nombres que iran en el eje x,
    una lista de valores asociados a esa referenica, el ancho,
    alto del grafico que se quiere obtener y el titulo del mismo,
    la maxima calidad de la imagen permitida es 300.000 pixeles  """

    src = "http://chart.apis.google.com/chart?cht=p&"
    src += "chxs=0,000000,14,0.5&chxt=x&"
    colores = ["6bacff", "FD5b5b", "5EFB6E", "FFA300", "ff61c6", "53E700"]
    src += "chco=" + ",".join(colores)
    src += "&chs=%sx%s&chd=t:" % (str(ancho), str(alto))
    auxiliar = [str(valor) for valor in eje_y]
    valores = ",".join(auxiliar)
    src += valores

    titulos = "|".join([nombre for nombre in eje_x])
    src += "&chdl="
    src += titulos
    src += "&chdls=000000,22"
    src += "&chtt=%s&chts=000000,30" % (titulo)
    src += '&chdlp=b&chma=|0,30'
    return src


def obtener_tabla(modelo, titulo, unidad, columnas, encabezados, perfil, cantidad):
    # Obtiene la lista items asociados a un filtro
    filtro = perfil.get_filtro(modelo)
    if filtro:
        resultados, has_more, has_less = obtener_opciones(perfil, unidad,
                                                        filtro, 0, cantidad)
        return resultados


def display_productos(filtro, perfil, cantidad):
    # Obtiene la lista de (productos,thumbs) asociados a un filtro para el html
    unidad = perfil.get_unit()
    resultados, has_more, has_less = obtener_opciones(perfil, unidad, filtro, 0,
                                                        cantidad)
    productos = []
    for elem in resultados:
        producto = Producto.objects.get(id=elem["id"])

        thumb = ""
        if producto.thumb():
            thumb = producto.thumb().encode("utf-8")
        productos.append((elem, thumb))
    return productos

