# Create your views here.

from exportpdf import obtener_html
from adkiller.filtros.models import Perfil
import os
from django.http import HttpResponse


def export_pdf(request):
    #Grabar HTML
    perfil = Perfil.objects.get(user=request.user, activo=True)

    inputfile, body, x, y = obtener_html(perfil)
    outputfile = inputfile.replace(".html", ".pdf")
    # outputfile = inputfile.split("/")[:-1] + "_%s.pdf" %
    # str(random.randint(1, 100000))
    #Convertir a PDF

    os.system(' xvfb-run -a -s "-screen 0 640x480x16" wkhtmltopdf --page-width 210 --page-height 297 --margin-right 0 --margin-top 0 --margin-bottom 0 --margin-left 0 %s %s' % (inputfile, outputfile))

    #Abrir PDF
    pdf = open(outputfile, 'r')
    content = pdf.read()
    pdf.close()

    os.remove(outputfile)
    os.remove(inputfile)

    response = HttpResponse(content, mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=%s' % "informe.pdf"

    return response
