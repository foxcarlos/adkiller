# -*- coding: utf-8 -*-
# Python Imports
from datetime import timedelta

# Django Imports
from django.db import models

# Project Imports
from adkiller.filtros.models import Perfil
from adkiller.filtros.models import Periodo
from adkiller.filtros.utils import cantidades_filtradas

TIPO_ALERTA_CHOICES = (
        ('VA', 'Valor Absoluto'),
        ('Va', 'Variaci?n'),
)

COMPARADOR_CHOICES = (
        ('>', 'Mayor'),
        ('<', 'Menor'),
        ('=', 'Igual'),
)

SUPERO_EL_MAXIMO = 1
BAJO_DEL_MINIMO = 0


class Alerta(models.Model):
    perfil = models.ForeignKey(Perfil, related_name="alertas")
    comparador = models.CharField(max_length=2, choices=COMPARADOR_CHOICES, null=True)
    valor = models.IntegerField(null=True)
    tipo = models.CharField(max_length=2, choices=TIPO_ALERTA_CHOICES, default="VA", null=True)
    activa = models.BooleanField(default=False)

    def setear(self, busqueda, tipo, comparador, valor, lapso):
        # inicialización de Perfil
        self.perfil = busqueda
        self.tipo = tipo
        self.comparador = comparador
        self.valor = valor        

        periodo_actual = self.perfil.get_periodo_or_create()
        periodo_actual.lapso = lapso
        periodo_actual.save()

        if tipo == 'Va':
            # creo un periodo más, para el periodo anterior
            periodo_anterior = Periodo.objects.create(perfil=self.perfil)

        self.save()

    def actualizar_periodos(self):
        periodos = Periodo.objects.filter(perfil=self.perfil)

        periodos[0].actualizar_fechas()
        periodos[0].save()

        if len(periodos) < 2:
            periodo_anterior = Periodo.objects.create(perfil=self.perfil)
        else:
            periodo_anterior = periodos[1]
        
        desde, hasta = periodos[0].get_fechas_periodo_anterior()
        periodo_anterior.fecha_desde, periodo_anterior.fecha_hasta = desde, hasta
        periodo_anterior.save()

    def valor_alcanzado(self, perfil, opcion_comparada=None):
        self.actualizar_periodos()

        unit, sum_total, cant_anuncios, cant_segundos, cant_inversion =\
                        cantidades_filtradas(perfil, opcion_comparada=opcion_comparada)

        if perfil.unidad == "cantidad":
            comparativo = cant_anuncios
        elif perfil.unidad == "volumen":
            comparativo = cant_segundos
        elif perfil.unidad == "inversion":
            comparativo = cant_inversion

        for i in range(0,2):
            if type(comparativo[i]) != float:
                comparativo[i] = float(comparativo[i].replace(".", "").replace("$", ""))

        return comparativo

    def comparar(self, valor, referencia):
        sucedio = False        
        res = []
        if self.comparador == "<" and valor < referencia:
            sucedio = True
            res.append(BAJO_DEL_MINIMO)
        if self.comparador == ">" and valor > referencia:
            sucedio = True
            res.append(SUPERO_EL_MAXIMO)
        # logica para que sepa si hubo un cambio o no
        cambio = sucedio != self.activa
        self.activa = sucedio
        self.save()

        return {'cambio': cambio, 'sucedio': sucedio, 'referencia': referencia, 'valor': valor, 'alertas': res}

    def chequear(self):
        comp = None
        try:
            comp = self.alertacomparativa
        except:
            pass

        if not comp:
            comp = self.alertacomportamiento

        if comp:
            return comp.chequear()



class AlertaComportamiento(Alerta):

    def chequear(self):
        # FIXME: implementar y debuggear manejo de alertas de variación
        if self.tipo == 'Va':
            return {'cambio': False, 'sucedio': False, 'valor': 0, 'alertas': 0}
        
        comparativo = self.valor_alcanzado(self.perfil)

        if self.tipo == "VA":
            ## Valor absoluto
            alcanzado = comparativo[0]
        else:
            ## Variacion
            anterior, actual = comparativo[0], comparativo[1]
            if float(actual):
                variacion = float(float(anterior) - float(actual)) /\
                                                        float(actual)
            else:
                if float(anterior):
                    variacion = 1.0
                else:
                    variacion = 0.0
            if variacion < 0:
                variacion = -variacion
            alcanzado = variacion

        return self.comparar(alcanzado, self.valor)



class AlertaComparativa(Alerta):
    serie_a = models.IntegerField(null=True)
    serie_b = models.IntegerField(null=True)

    def setear(self, busqueda, tipo, comparador, valor, lapso, serie_a=None, serie_b=None):
        # inicialización de Perfil
        self.serie_a = serie_a
        self.serie_b = serie_b
        super(AlertaComparativa, self).setear(busqueda, tipo, comparador, valor, lapso)


    def chequear(self):
        valor_a = self.valor_alcanzado(self.perfil, opcion_comparada=self.serie_a)[0]
        valor_b = self.valor_alcanzado(self.perfil, opcion_comparada=self.serie_b)[0]
        return self.comparar(valor_a, valor_b * (100 + self.valor) / 100)

