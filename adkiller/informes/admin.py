from django.contrib import admin

from models import Alerta


class AlertaAdmin(admin.ModelAdmin):
    list_display = ['perfil', 'comparador', 'valor', 'activa', 'tipo']

admin.site.register(Alerta, AlertaAdmin)
