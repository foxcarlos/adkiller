# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

# Python imports
from datetime import datetime, timedelta, date
import simplejson
import mock

# Django imports
from django.core.urlresolvers import reverse

# Project imports
from adkiller.app.tests import InfoadTestCase, FakeDate
from adkiller.app.views import set_unit, compare_filter, home
from adkiller.filtros.models import Filtro, Perfil, OpcionFiltro, Periodo
from adkiller.app.models import Rubro, Marca, Sector, Industria, \
        Producto, Ciudad, Medio, Programa, Emision, Soporte, Anunciante, \
        Horario
from adkiller.filtros.views import add_filter, get_ordered_filters, \
        set_ordered_filters, get_filters, remove_filter, graph, \
        get_options, lookup_filtro_general, more_options, less_options, \
        open_filter, crear_busqueda
from models import Alerta


@mock.patch('adkiller.filtros.views.date', FakeDate)
@mock.patch('adkiller.filtros.models.date', FakeDate)
class AlertasTest(InfoadTestCase):
    """ Tests para manejo y creación de alertas
    """
    def crear_emision(self, producto, horario, fecha, duracion, precio_minuto):
        params = {
             "producto": producto,
             "horario": horario,
             "fecha": fecha,
             "valor": duracion*precio_minuto,
             "duracion": duracion,
             "hora": "12:01",
             "orden": 1,
            }

        return Emision.objects.create(**params)

    def setUp(self):
        InfoadTestCase.setUp(self)

        # Piso el today
        FakeDate.today = classmethod(lambda cls: date(2012, 12, 31))

        # Creo filtros para el perfil de usuario
        self.usuario.get_profile().crear_filtros()
                
        # Creo varias emisiones, desparramadas sobre todo el año 2012
        # para poder testear sobre distintos periodos

        # Los precios por minuto crecen con los meses
        # El minuto del 12 cuesta el doble que el del 10
        # Las publicidades de Coca-Cola duran el doble que las de Chizitos                
        for mes in range(1, 13):
            ppm = mes
            if mes in [1, 7, 12]:
                # estos meses pongo publicidad todos los días
                for dia in range(1, 32):
                    fecha = datetime(2012, mes, dia)
                    self.crear_emision(self.cocacolalife, self.horario_del_10, fecha,
                                            duracion=2, precio_minuto=ppm)
                    self.crear_emision(self.chizitos, self.horario_del_10, fecha,
                                            duracion=1, precio_minuto=ppm)
                    self.crear_emision(self.cocacolalife, self.horario_del_12, fecha,
                                            duracion=2, precio_minuto=2 * ppm)
                    self.crear_emision(self.chizitos, self.horario_del_12, fecha,
                                            duracion=1, precio_minuto=2 * ppm)
            else:
                for semana in range(1, 5):
                    fecha = datetime(2012, mes, 7 * (semana - 1) + 1)
                    # desparramo las emisiones en la semana
                    diff = timedelta(days=2)
                    self.crear_emision(self.cocacolalife, self.horario_del_10, fecha,
                                            duracion=2, precio_minuto=ppm)
                    fecha += diff
                    self.crear_emision(self.chizitos, self.horario_del_10, fecha,
                                            duracion=1, precio_minuto=ppm)
                    fecha += diff
                    self.crear_emision(self.cocacolalife, self.horario_del_12, fecha,
                                            duracion=2, precio_minuto=2 * ppm)
                    fecha += diff
                    self.crear_emision(self.chizitos, self.horario_del_12, fecha,
                                            duracion=1, precio_minuto=2 * ppm)

        # Me logueo como usuario y voy a la homepage
        self.client.login(username="non_staff", password="yy")
        self.client.get(reverse(home))

        # Creo tres alertas diferentes
        self.alertas = {}
        # -> creo alerta (inversion diaria menor que 10)
        params = {'nombre': 'cantidad diaria < 4', 'alertar': 'on',
                'variable': 'inversion', 'comparador': '<', 
                'variable_valor': '10', 'periodo': 'dia'}
        self.client.get(reverse(crear_busqueda), params)

        # -> creo otra alerta (inversion semanal mayor que 100)
        params = {'nombre': 'inversion semanal > 100', 'alertar': 'on',
                'variable': 'inversion', 'comparador': '>', 
                'variable_valor': '100', 'periodo': 'semana'}
        self.client.get(reverse(crear_busqueda), params)

        # -> creo una alerta con filtros
        # filtro publicidades de chizitos en el 10
        # y alerto si la inversión del último mes fue > $40
        data = {}
        data['filter'] = "marca"
        data['value'] = str(self.pepsico.id)
        self.client.get(reverse(add_filter), data)

        data['filter'] = "medio"
        data['value'] = str(self.canal10.id)
        self.client.get(reverse(add_filter), data)

        params = {'nombre': 'chizitos en el 10', 'alertar': 'on',
                'variable': 'inversion', 'comparador': '>', 
                'variable_valor': '30', 'periodo': 'mes'}
        self.client.get(reverse(crear_busqueda), params)


        # -> creo alertas comparativas
        data = {}
        data['filter'] = "marca"
        data['value'] = str(self.cocacolalife.id)
        self.client.get(reverse(add_filter), data)
        self.client.get(reverse(compare_filter), {'filter': 'marca'})


        params = {'nombre': 'coca vs pepsi en canal 10 cantidad', 'alertar': 'on',
                'variable': 'cantidad', 'comparador': '>', 
                'variable_valor': '0', 'periodo': 'mes',
                'serie_a': self.cocacolalife.id, 
                'serie_b': self.pepsico.id, 
                }
        self.client.get(reverse(crear_busqueda), params)


        params = {'nombre': 'coca vs pepsi en canal 10 segundos', 'alertar': 'on',
                'variable': 'volumen', 'comparador': '>', 
                'variable_valor': '30', 'periodo': 'mes',
                'serie_a': self.cocacolalife.id, 
                'serie_b': self.pepsico.id, 
                }
        self.client.get(reverse(crear_busqueda), params)


        for alerta in Alerta.objects.all():
            self.alertas[alerta.perfil.nombre] = alerta

    def test_alerta_se_activa(self):
    	# seteo hoy a fin de año, la inversion es alta
        FakeDate.today = classmethod(lambda cls: date(2012, 10, 1))

    	# chequeo chizitos en el 10 más de 30 por mes
        alerta = self.alertas['chizitos en el 10']
        data = alerta.chequear()

    	# debe darme que sucede por primera vez
        self.assertTrue(data['sucedio'])
        self.assertTrue(data['cambio'])

    def test_alerta_se_desactiva(self):
        # seteo hoy en un mes ralo
        FakeDate.today = classmethod(lambda cls: date(2012, 2, 2))
        
        # chequeo la alerta 'cantidad diaria < 4'
        alerta = self.alertas['cantidad diaria < 4']
        data = alerta.chequear()

        # (se activa)
        self.assertTrue(data['sucedio'])
        self.assertTrue(data['cambio'])

        # seteo hoy en un mes cargado
        FakeDate.today = classmethod(lambda cls: date(2012, 7, 2))

        # vuelvo a chequear la alerta
        data = alerta.chequear()

        # (dejó de suceder)
        self.assertFalse(data['sucedio'])
        self.assertTrue(data['cambio'])

    def test_alerta_sin_filtros(self):
        # seteo hoy a principio de año, cuando la inversion es baja
        FakeDate.today = classmethod(lambda cls: date(2012, 1, 10))

        # chequeo cantidad diaria < 4
        alerta = self.alertas['cantidad diaria < 4']
        data = alerta.chequear()

        # debe darme que sucede por primera vez
        self.assertTrue(data['sucedio'])
        self.assertTrue(data['cambio'])
    

    def test_comparativa(self):
        # seteo hoy a principio de año, cuando la inversion es baja
        FakeDate.today = classmethod(lambda cls: date(2012, 1, 10))

        # chequeo cantidad
        alerta = self.alertas['coca vs pepsi en canal 10 cantidad']
        data = alerta.chequear()
        # debe darme que sucede por primera vez
        self.assertFalse(data['sucedio'])

        # chequeo volumen
        alerta = self.alertas['coca vs pepsi en canal 10 segundos']
        data = alerta.chequear()
        # debe darme que sucede por primera vez
        self.assertTrue(data['sucedio'])


    # FIXME    
    # def test_alerta_diaria(self):
    # 	# creo una alerta (volumen diario mayor que 5)
    #     params = {'nombre': 'volumen diario > 5', 'alertar': 'on',
    #         'variable': 'volumen', 'comparador': 'mayor', 
    #         'variable_valor': '5', 'periodo': 'dia'}
    #     response = self.client.get(reverse(crear_busqueda), params)
    #     result_data = simplejson.loads(response.content)
    #     alerta = Alerta.objects.get(id=result_data['alerta_id'])

    # 	# hago pasar dos días, cada día creo una nueva emisión de duracion 10
    #     # 1er dia : comienza a suceder
    #     FakeDate.today = classmethod(lambda cls: date(2013, 1, 1))

    #     data = alerta.chequear()
    #     self.assertTrue(data['sucedio'])
    #     self.assertTrue(data['cambio'])

    # 	# 2do dia: sigue sucediendo, no reporta cambio
    #     FakeDate.today = classmethod(lambda cls: date(2013, 1, 2))

    #     data = alerta.chequear()
    #     self.assertTrue(data['sucedio'])
    #     self.assertFalse(data['cambio'])

    # 	# hago pasar un día más, sin emisión nueva
    #     FakeDate.today = classmethod(lambda cls: date(2013, 1, 3))

    #     # chequeo que deje de suceder y reporte cambio
    #     data = alerta.chequear()
    #     self.assertFalse(data['sucedio'])
    #     self.assertTrue(data['cambio'])

    # def test_formato_del_mail(self):
    # 	# creo una alerta (inversion diaria mayor que 5)

    # 	# creo otra alerta (anuncios semanales menores que 20)

    # 	# activo ambas y verifico que se genere el mail
    # 	# y contenga el aviso apropiado


    # 	# desactivo ambas y verifico que se genere
    # 	# el mail avisando que se desactivan

    # 	# siguen inactivas un día más,
    # 	# no se manda ningún mail
    # 	pass

    # def test_cantidad_mayor_que_ultimo_mes(self):
    # 	# creo una alerta (cantidad mayor q 15 en el último mes)

    # 	# empieza a valer, verificar que se envíe el mail

    # 	# sigue valiendo, no debería enviarse nada

    # 	# deja de valer, debe mandarse mail


    # 	pass

    # def test_inversion_menor_que_ultima_semana(self):
    # 	# creo una alerta (inversión menor q 200 en la última semana)

    # 	# empieza a valer, verificar que se envíe el mail

    # 	# sigue valiendo, no debería enviarse nada

    # 	# deja de valer, debe mandarse mail

    # 	pass