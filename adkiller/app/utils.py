# -*- coding: utf-8 -*-

# Python Imports
from re import compile
import datetime
import csv
import unicodedata
from libs.texttable import Texttable
from itertools import izip_longest

# Django Imports
from django.http import HttpResponseRedirect
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import User
from django.conf import settings
from django.http import HttpResponse

# Project Imports
from adkiller.filtros.models import Perfil
from adkiller.filtros.utils import get_qs_emisiones_filtradas
from adkiller.usuarios.models import add_log


EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
LOGIN = 5  # Represents the login action in LogEntry


if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]


class ColumnTable:
    def __init__(self, title, size, header_align, content_align):
        self.title = title
        self.size = size
        self.header_align = header_align
        self.content_align = content_align


class TableGenerator:
    def __init__(self):
        self.columns = []
        self.between_columns = " "
        self.char_header_separator = "-"

    def add_column(self, title, size, header_align, content_align="l"):
        self.columns.append(ColumnTable(title,
                size,
                header_align,
                content_align
            )
        )

    def align(self, text, size, align):
        t = text[:size]
        if align == "c":
            return t.center(size)
        elif align == "l":
            return t.ljust(size)
        elif column.align == "r":
            return t.rjust(size)

    def newline(self):
        return "\r\n"

    def header(self):
        s = ""
        for column in self.columns:
            s += self.between_columns
            s += self.align(column.title, column.size, column.header_align)
        return s + self.newline()

    def header_separator(self):
        s = ""
        for column in self.columns:
            s += self.between_columns
            s += self.char_header_separator * column.size
        return s + self.newline()

    def row(self, data):
        s = ""
        for i in range(len(data)):
            column = self.columns[i]
            s += self.between_columns
            s += unicode(self.align(data[i], column.size, column.content_align))
        return s + self.newline()


class LoginRequiredMiddleware:
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """
    def process_request(self, request):
        assert hasattr(request, 'user'), "The Login Required middleware\
 requires authentication middleware to be installed. Edit your\
 MIDDLEWARE_CLASSES setting to insert\
 'django.contrib.auth.middlware.AuthenticationMiddleware'. If that doesn't\
 work, ensure your TEMPLATE_CONTEXT_PROCESSORS setting includes\
 'django.core.context_processors.auth'."
        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in EXEMPT_URLS):
                response_url = settings.LOGIN_URL
                if request.path != '/accounts/logout/login/':
                    response_url += '?next=%s' % request.path

                return HttpResponseRedirect(response_url)


def eliminar_tildes(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if
                    unicodedata.category(c) != 'Mn'))



"""TITULOS = ['marca', 'producto', 'cod medio', 'medio', 'programa', 'fecha', 'dia', 'orden',
    'duracion', 'alto', 'ancho', 'superficie', 'valor', 'observaciones',
    'color', 'origen', 'hora', 'ciudad', 'inicio', 'fin', 'score',
    'duracion_adtrack', 'id_tipo_publicidad', 'id_estilo_publicitario',
    'ids_valores_transmitidos', 'ids_recursos_utilizados', 'patron_file']
"""

TITULOS = [
    'Industria', 'Sector', 'Marca', 'Anunciante', 'Producto',
    'Duración', 'Medio', 'Programa', 'Ciudad', 'Soporte', 'Fecha', 'Hora',
    'Valor bruto', 'Observaciones', 'Id Producto', 'Link Spot',
    'Tipo Publicidad', 'Código', 'Link Tanda', 'Inicio Programa', 'Fin Programa', 'Tipo Programa'
]

def generar_csv(perfil, externo, titulos=TITULOS, filename=""):
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'

    valores = []
    recursos = []

    lista = get_qs_emisiones_filtradas(perfil)[0]

    lista = lista.filter(horario__programa__isnull=False,
                            horario__medio__isnull=False)
    lista = lista.select_related("producto__marca__sector__industria",
        "producto__marca__anunciante", "horario__medio__soporte",
        "horario__medio__ciudad")

    # csv.register_dialect('excel', delimiter="\t", escapechar='\\',
    # doublequote=False, quotechar="\"", lineterminator="\r\n")
    # dialect = csv.get_dialect('excel')

    if filename:
        writer = csv.writer(open(filename, "w+"), delimiter='\t',
                                                    dialect="excel")
        writer.writerow(titulos)
    else:
        if externo:
            # Linea especial para el excel
            writer = csv.writer(response, delimiter='\t', dialect="excel")
        else:
            response.write(u'\ufeff'.encode('utf8'))
            writer = csv.writer(response, delimiter=';', dialect="excel")
    #    writer.writerow(codecs.BOM_UTF16_LE)
        writer.writerow(titulos)
    for cantidad, item in enumerate(lista):
        # LOG
        if cantidad % 1000 == 0:
            print cantidad, datetime.datetime.now()

        obs = item.observaciones
        if obs:
            obs = obs.encode("utf-8")
        else:
            obs = ""
        observaciones = obs

        alto = ""
        ancho = ""
        color = ""
        superficie = 0
        inicio = 0
        fin = 0
        score = 0
        duracion_adtrack = 0

        try:
            audiovisual = item.audiovisual
        except:
            audiovisual = None
        if audiovisual:
            inicio = audiovisual.inicio
            fin = audiovisual.fin
            score = audiovisual.score
            duracion_adtrack = audiovisual.duracion_adtrack

        if not audiovisual:
            try:
                grafica = item.grafica
            except:
                grafica = None
            if grafica:
                alto = grafica.alto
                ancho = grafica.ancho
                color = grafica.color
                superficie = alto * ancho

        dia = item.fecha.strftime("%Y-%m-%d")
        hora = item.hora_real().strftime("%H:%M:%S")

        if not externo:
            sector = item.producto.marca.sector
            if sector:
                industria = sector.industria
            else:
                industria = None
            medio = item.horario.medio
            if medio.ciudad_id:
                ciudad = medio.ciudad
            else:
                ciudad = None

            try:
                link_spot = item.producto.patrones.all()[0].video()
            except:
                link_spot = u""
            patron = item.producto.patron_principal()
            if patron:
                filetype = patron.filetype()
            else:
                filetype = ""
            id_producto = item.producto.id
            link_spot = "http://%s/app/ver_video/?path=/app/video_producto/%d&filetype=%s" % (settings.SITE_URL, id_producto, filetype)
            link_tanda = "http://%s/app/ver_video/?path=/app/video_origen/%d&filetype=%s" % (settings.SITE_URL, item.get_fuente().id, filetype)

            if item.tipo_publicidad:
                tipo_publicidad = item.tipo_publicidad.descripcion.encode("utf-8")
            else:
                tipo_publicidad = ""

            cod_medio = medio.identificador_directv
            if not cod_medio:
                cod_medio = medio.identificador

            codigo_directv = item.get_codigo_directv()

            inicio_programa = item.horario.horaInicio
            fin_programa = item.horario.horaFin
            tipo_programa = item.horario.programa.tags

            row = [to_string(industria), to_string(sector),
                to_string(item.producto.marca),
                to_string(item.producto.marca.anunciante),
                to_string(item.producto), item.duracion, to_string(medio),
                to_string(item.horario.programa), to_string(ciudad),
                to_string(item.horario.medio.soporte), dia, hora,
                #item.orden,
                "%d" % item.tarifa_real(perfil.user), observaciones,
                id_producto,
                link_spot, tipo_publicidad, codigo_directv, link_tanda,
                inicio_programa, fin_programa, tipo_programa,
                ]
            if perfil.user.is_staff:
                row.append(cod_medio)
                row.append(score)
                row.append(item.get_mediamap_coords())
        else:
            valores = u",".join([str(v.id) for v in item.producto.valores.all()])
            valores = valores.encode("utf8")
            recursos = u",".join([str(v.id) for v in item.producto.recursos.all()])
            recursos = recursos.encode("utf8")

            tipo_publicidad = 0
            if item.producto.tipo_publicidad:
                tipo_publicidad = item.producto.tipo_publicidad.id
            estilo_publicidad = 0
            if item.producto.estilo_publicidad:
                estilo_publicidad = item.producto.estilo_publicidad
            row = []
            row = [to_string(item.producto.marca), to_string(item.producto),
                to_string(item.horario.medio), to_string(item.horario.programa),
                dia + " " + hora, dia, item.orden, item.duracion,
                alto, ancho, superficie, item.valor, observaciones, color,
                item.origen, item.hora, to_string(item.horario.medio.ciudad),
                inicio, fin, score, duracion_adtrack, tipo_publicidad,
                estilo_publicidad, valores, recursos, filename]
        writer.writerow(row)

    add_log(perfil.user, Perfil, perfil, 6, "Exportó a Excel")   

    if not filename:
        return response
    else:
        return "Archivo Creado"


def generar_dtv(perfil):
    """
    Genera un reporte de emisiones con el formato DTV
    :param perfil:
    :return string:
    """

    medio = "DTSA"
    fecha = datetime.date(2015, 07, 04)
    charset = "windows-1252"

    response = HttpResponse(content_type='plain/text; charset=windows-1252')
    response['Content-Disposition'] = 'attachment; filename="%s%02d%02d%02d.txt"' % (medio, fecha.month, fecha.day, fecha.year % 2000)
    response['Set-Cookie'] = 'fileDownload=true; path=/'

    table = TableGenerator()
    table.add_column("ON-AIR", 8, "c")
    table.add_column("DATE/TIME", 11, "c")
    table.add_column("ID", 32, "c")
    table.add_column("S", 2, "c")
    table.add_column("TITLE", 32, "c")
    table.add_column("DURATION", 11, "c")
    table.add_column("STATUS", 7, "c")
    table.add_column("DEVICE", 11, "c")
    table.add_column("RECONCILE", 9, "c")
    table.add_column("TYPE", 7, "c")
    table.add_column("SEC", 4, "c")

    # Get data list
    lista = get_qs_emisiones_filtradas(perfil)[0]
    lista = lista.filter(horario__programa__isnull=False,
                            horario__medio__isnull=False)

    #ejemplo: ["01/27/15", "06:11:05.16", "CUEIN", "1", "Cue Tone In", "00:00:00.05", "", "GPI:1", "1000003", "PT", "sGPI"]

    response.write(table.header())
    response.write(table.header_separator())
    for item in lista:
        codigo = item.get_codigo_directv()
        if not codigo:
            codigo = " "
        if item.get_codigo_directv():
            textos = "%s %s" % (item.producto.marca.nombre, item.producto.nombre)
            response.write(
                table.row(
                    [str(item.fecha.strftime("%m/%d/%y")), str(item.hora), str(codigo), " ", textos, "0" + str(datetime.timedelta(seconds=item.duracion)), " ", " ", " ", " ", " "]
                )
            )

    #response.write(table.draw())
    add_log(perfil.user, Perfil, perfil, 6, "Exportó a ASRUN DTV")
    return response

def generar_starcom(perfil):
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'


    lista = get_qs_emisiones_filtradas(perfil)[0]
    lista = lista.filter(horario__programa__isnull=False,
                            horario__medio__isnull=False)
    lista = lista.select_related("producto__marca__sector__industria",
        "producto__marca__anunciante", "horario__medio__soporte",
        "horario__medio__ciudad")


    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter='\t', dialect="excel")
    #titulos = ['Vehículo', 'Fecha', 'Hora', 'Producto / Tema', 'Dur.Av', 'Mat.', 'Pos.', 'Av Bloq', 'Programa', 'Anunciante', 'Agen Curs.', 'Segmento', 'Tipo Compra', 'Inversión', 'Falla']
    #writer.writerow(titulos)

    for cantidad, item in enumerate(lista):
        # LOG
        if cantidad % 1000 == 0:
            print cantidad, datetime.datetime.now()

        vehiculo = to_string(item.horario.medio)
        fecha = item.fecha.strftime("%d/%m/%Y")
        hora = item.hora.strftime("%H:%M:%S")
        producto = "%s / %s" % (to_string(item.producto.marca), to_string(item.producto))
        duracion = item.duracion
        if item.tipo_publicidad.nombre == "PNT":
            mat = "Mencion Barrida"
            tipo_compra = u"En Artístico".encode("utf-8")
        else:
            mat = "Film"
            tipo_compra = "Corte Comercial"

        pos = item.orden
        if pos == 0:
            pos = 1
        bloq = item.cantidad_avisos_tanda()
        if bloq == 0:
            bloq = 1

        programa = to_string(item.horario.programa)
        anunciante = to_string(item.producto.marca.anunciante)
        agente = ""
        segmento = ""
        inversion = item.valor
        falla = item.observaciones

        row = [vehiculo, fecha, hora, producto, duracion, mat, pos, bloq, programa, anunciante, agente, segmento, tipo_compra, inversion, falla]
        writer.writerow(row)

    add_log(perfil.user, Perfil, perfil, 6, "Exportó a Excel Starcom")

    return response


def generar_olx(perfil):
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'


    lista = get_qs_emisiones_filtradas(perfil)[0]
    lista = lista.filter(horario__programa__isnull=False,
                            horario__medio__isnull=False)
    lista = lista.select_related("producto__marca__sector__industria",
        "producto__marca__anunciante", "horario__medio__soporte",
        "horario__medio__ciudad")


    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter='\t', dialect="excel")
    titulos = ["Id Country_Monitor", "Country_Monitor", "Origin_Monitor", "Date_Monitor", "Week_Monitor", "Media_Monitor", "Show_Monitor", "Channel_Monitor", "Version_Monitor", "Time_Monitor", "Duration_Monitor", "Ads_Quantity_Monitor", "Position_Monitor", "GRPs_Monitor", "Investment_Monitor"]

    writer.writerow(titulos)

    for cantidad, item in enumerate(lista):
        # LOG
        if cantidad % 1000 == 0:
            print cantidad, datetime.datetime.now()

        medio = item.horario.medio
        fecha = item.fecha.strftime("%d/%m/%Y")
        hora = item.hora.strftime("%H:%M:%S")
        producto = "%s / %s" % (to_string(item.producto.marca), to_string(item.producto))
        duracion = item.duracion
        programa = to_string(item.horario.programa)
        inversion = item.valor
        pais = "Argentina"
        if medio.ciudad.nombre in ["Uruguay", "Paraguay"]:
            pais = medio.ciudad.nombre
        if "radio" in medio.nombre.lower():
            soporte = "Radio"
        else:
            soporte = "TV"

        row = ["", pais, "", fecha, "", soporte, programa, to_string(medio), producto, hora, duracion, "", "", "", inversion]
        writer.writerow(row)

    add_log(perfil.user, Perfil, perfil, 6, "Exportó a Excel OLX")

    return response


def generar_ibope(perfil):
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'


    lista = get_qs_emisiones_filtradas(perfil)[0]
    lista = lista.filter(horario__programa__isnull=False,
                            horario__medio__isnull=False)
    lista = lista.select_related("producto__marca__sector__industria",
        "producto__marca__anunciante", "horario__medio__soporte",
        "horario__medio__ciudad")

    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter='\t', dialect="excel")
    #titulos = ['CodCiudad', 'CodMedio', 'Hora', 'Duración', 'Cod0', 'Valor', 'Fecha', 'Ciudad', 'Medio', 'Programa', 'OtroCod0']
    #writer.writerow(titulos)

    for cantidad, item in enumerate(lista):
        medio = item.horario.medio
        fecha = item.fecha.strftime("%d/%m/%Y")
        hora = item.hora.strftime("%H:%M:%S")
        duracion = item.duracion
        programa = to_string(item.horario.programa)
        inversion = item.valor

        row = ['110', medio.codigo_ibope, "", hora, duracion, '0', inversion, fecha, to_string(medio.ciudad), to_string(medio), programa, '0']
        writer.writerow(row)

    add_log(perfil.user, Perfil, perfil, 6, "Exportó a Excel Ibope")

    return response


def to_string(obj):
    if obj:
        return obj.__unicode__().encode('utf8')
    else:
        return ""


class LogUserActivityMiddleWare(object):
    """Used to log when a user iteracts with the site but only once a day"""
    def process_request(self, request):

        log = LogEntry.objects.filter(user_id=request.user.id,
            action_flag=LOGIN).order_by("-action_time")
        if not log or log[0].action_time.date() < datetime.date.today():
            if request.user.id:
                self.create_log(request.user)

    def create_log(self, user):
        LogEntry.objects.log_action(
            user_id=user.id,
            content_type_id=ContentType.objects.get_for_model(User).pk,
            object_id=user.id,
            object_repr=unicode(user),
            action_flag=LOGIN,
            change_message="Login")


class Lapso:
    def __init__(self, inicio, fin, manana=False):
        self.inicio = datetime.datetime.today()
        self.inicio = self.inicio.replace(hour=inicio.hour)
        self.inicio = self.inicio.replace(minute=inicio.minute)
        self.inicio = self.inicio.replace(second=inicio.second)
        self.inicio = self.inicio.replace(microsecond=inicio.microsecond)
        if manana:
            self.inicio += datetime.timedelta(days=1)

        self.fin = datetime.datetime.today()
        self.fin = self.fin.replace(hour=fin.hour)
        self.fin = self.fin.replace(minute=fin.minute)
        self.fin = self.fin.replace(second=fin.second)
        self.fin = self.fin.replace(microsecond=fin.microsecond)
        if manana:
            self.fin += datetime.timedelta(days=1)

        self.abarca_dias_distintos = False

        if self.fin < self.inicio:
            self.fin = self.fin + datetime.timedelta(days=1)
            self.abarca_dias_distintos = True

        self.duracion = self.fin - self.inicio

    def imprime(self):
        print "inicio:   ", self.inicio.time()
        print "fin:      ", self.fin.time()
        print "duracion: ", self.duracion

    def copia_con_un_dia_mas(self):
        inicio = self.inicio
        fin = self.fin
        copia = Lapso(inicio, fin, manana=True)
        return copia

    def solapa(self, otro_lapso, chequea_dias_distintos=True):
        # si yo abarco dias distintos
        if chequea_dias_distintos and self.abarca_dias_distintos and not otro_lapso.abarca_dias_distintos:
            # hago una copia del otro sumándole un día
            copia_otro = otro_lapso.copia_con_un_dia_mas()
            return self.solapa(copia_otro, chequea_dias_distintos = False) or self.solapa(otro_lapso, chequea_dias_distintos = False)

        elif chequea_dias_distintos and otro_lapso.abarca_dias_distintos and not self.abarca_dias_distintos:
            # hago una copia de mi mismo sumandome un dia mas
            copia_self = self.copia_con_un_dia_mas()
            copia_2 = copia_self.solapa(otro_lapso, chequea_dias_distintos=False)
            return copia_self.solapa(otro_lapso, chequea_dias_distintos = False) or self.solapa(otro_lapso, chequea_dias_distintos = False)
        # si los dos cruzan el dia o ninguno cruza el dia
        else:
            # si yo empiezo antes o junto
            if self.inicio <= otro_lapso.inicio:
                # me fijo si termino después de que empiece el otro (o junto)
                return self.fin >= otro_lapso.inicio
            # si yo empiezo después
            else:
                # me fijo si el otro termina después que yo empiece (o junto)
                return otro_lapso.fin >= self.inicio


def save_file_to_disk(f, path):
    destination = open(path, 'wb+')
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def get_fecha_hora_from_thumb_url(thumb_url):
    """
    http://media.welo.tv/prog_thumbs/9/20151201/1950/298.jpg
    ->
    datetime(2015, 12, 01, 19, 54, 58)
    """
    data = thumb_url.split('/')
    fecha, hora, segundos = data[-3:]
    segundos = int(segundos.split('.')[0])
    fecha_hora = datetime.datetime.strptime(fecha + hora, '%Y%m%d%H%M')
    return fecha_hora + datetime.timedelta(seconds=segundos)
