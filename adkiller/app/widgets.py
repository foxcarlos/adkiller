from django.conf import settings
from django.contrib.admin.widgets import FilteredSelectMultiple


class AJAXFilteredSelectMultiple(FilteredSelectMultiple):
    """Filter anunciantes with AJAX calls"""

    class Media:
        js = (settings.STATIC_URL + "admin/js/core.js",
              settings.STATIC_URL + "admin/js/SelectBox.js",
              settings.STATIC_URL + "admin/js/SelectFilter2.js",
              settings.STATIC_URL + "js/ajax_anunciantes_list.js")