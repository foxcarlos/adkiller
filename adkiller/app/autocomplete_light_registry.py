# -*- coding: utf-8 -*-
import unicodedata
import autocomplete_light
import re
from models import Rubro,Industria,Sector,Anunciante,TipoPublicidad,EstiloPublicidad,ValorPublicidad,RecursoPublicidad
from models import RecursoPublicidad,Agencia,Marca  ,Emision,Programa,Producto,Medio,Ciudad,Soporte,AudioVisual
from django.db.models import Q
from django.contrib.auth.models import User




class NombreAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('nombre',)
    autocomplete_js_attributes={'placeholder': 'Ingrese valor ..',}
    
    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        choices = self.choices.all()
        if q:
            choices = choices.filter(nombre__aicontains=q)
        return self.order_choices(choices)


class SectorAutocomplete(NombreAutocomplete):
    pass

class ProgramaAutocomplete(NombreAutocomplete):
    pass

class MedioAutocomplete(NombreAutocomplete):
    pass

class CiudadAutocomplete(NombreAutocomplete):
    pass

class SoporteAutocomplete(NombreAutocomplete):
    search_fields=('nombre',)
    autocomplete_js_attributes={'placeholder': 'Ingrese valor ..',}
    
    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        choices = self.choices.all()
        if q:
            choices = choices.filter(nombre__aicontains=q, padre__isnull=True)
        return self.order_choices(choices)



class AnuncianteAutocomplete(NombreAutocomplete):
    pass

class RubroAutocomplete(NombreAutocomplete):
    pass

class IndustriaAutocomplete(NombreAutocomplete):
    pass

autocomplete_light.register(Sector, SectorAutocomplete)
autocomplete_light.register(Programa, ProgramaAutocomplete)
autocomplete_light.register(Medio, MedioAutocomplete)
autocomplete_light.register(Ciudad, CiudadAutocomplete)
autocomplete_light.register(Soporte, SoporteAutocomplete)
autocomplete_light.register(Anunciante, AnuncianteAutocomplete)
autocomplete_light.register(Rubro, RubroAutocomplete)
autocomplete_light.register(Industria, IndustriaAutocomplete)




autocomplete_light.register(TipoPublicidad, search_fields=('descripcion',),
    autocomplete_js_attributes={'placeholder': 'Tipo de Publicidad ..'}) 
    
autocomplete_light.register(EstiloPublicidad, search_fields=('descripcion',),
    autocomplete_js_attributes={'placeholder': 'Estilo de Publicidad ..'}) 
    
autocomplete_light.register(ValorPublicidad, search_fields=('descripcion',),
    autocomplete_js_attributes={'placeholder': 'Otro Valor ..'})
    
autocomplete_light.register(RecursoPublicidad, search_fields=('descripcion',),
    autocomplete_js_attributes={'placeholder': 'Otro recurso ..'}) 
    
autocomplete_light.register(Agencia, search_fields=('nombre',),
    autocomplete_js_attributes={'placeholder': 'Agencia ..'}) 


class MarcaAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('nombre',)
    autocomplete_js_attributes={'minimum_characters': 0,'placeholder': 'Marca ..','max_values': 260,}

    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        choices = self.choices.all()
        if q:
            secciones = Marca.objects.filter(nombre__aiexact=q)
            choices = choices.filter(nombre__aicontains=q)
            choices = list(secciones)   + list(choices)
        return self.order_choices(choices)

autocomplete_light.register(Marca,MarcaAutocomplete)



class EmisionAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('id',)
    autocomplete_js_attributes={'placeholder': 'Emision ..'}
autocomplete_light.register(Emision,EmisionAutocomplete)


class ProgramaAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('nombre',)
    autocomplete_js_attributes={'minimum_characters': 0,'placeholder': 'Programa ..','max_values': 260,}

    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        medio_id = self.request.GET.get('medio_id', None)
        choices = self.choices.all()
        if q:
            choices = choices.filter(nombre__aicontains=q)
        if medio_id:
            choices = choices.filter(horarios__medio_id=medio_id, horarios__enElAire=True, horarios__tarifa__isnull=False).distinct()
        return self.order_choices(choices)
        

autocomplete_light.register(Programa, ProgramaAutocomplete)        

class ProductoAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('nombre',)
    autocomplete_js_attributes={'minimum_characters': 1,'placeholder': 'Producto ..','max_values': 260,}
    def choices_for_request(self):
        q = eliminar_tildes(unicode(self.request.GET.get('q', ''))).replace('\\','').replace('(','\(').replace(')','\)')
        marca_id = self.request.GET.get('marca_id', None)
        choices = self.choices.all()
        if q:
            choices = choices.filter(nombre__aicontains=q)
        if marca_id:
            choices = choices.filter(marca_id=marca_id)

        return self.order_choices(choices)

autocomplete_light.register(Producto, ProductoAutocomplete)



def eliminar_tildes(string):
    return ''.join((c for c in unicodedata.normalize('NFD', string) if unicodedata.category(c) != 'Mn'))
    