from django import template
import time


register = template.Library()

@register.simple_tag
def random_tag():
    return int(time.time())
