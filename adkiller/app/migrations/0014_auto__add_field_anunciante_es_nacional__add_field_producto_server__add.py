# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Anunciante.es_nacional'
        db.add_column('app_anunciante', 'es_nacional',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Producto.server'
        db.add_column('app_producto', 'server',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Servidor'], null=True, blank=True),
                      keep_default=False)

        # Adding field 'Producto.id_en_simpson'
        db.add_column('app_producto', 'id_en_simpson',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Anunciante.es_nacional'
        db.delete_column('app_anunciante', 'es_nacional')

        # Deleting field 'Producto.server'
        db.delete_column('app_producto', 'server_id')

        # Deleting field 'Producto.id_en_simpson'
        db.delete_column('app_producto', 'id_en_simpson')


    models = {
        'app.agencia': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Agencia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.anunciante': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Anunciante'},
            'es_nacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.audiovisual': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'AudioVisual', '_ormbases': ['app.Emision']},
            'duracion_adtrack': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'emision_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app.Emision']", 'unique': 'True', 'primary_key': 'True'}),
            'fin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'inicio': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'})
        },
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.emision': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'Emision'},
            'duracion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 3, 6, 0, 0)', 'db_index': 'True'}),
            'hora': ('django.db.models.fields.TimeField', [], {'default': "u'00:00'"}),
            'horario': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Horario']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'migracion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Migracion']"}),
            'migrado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'to': "orm['app.Producto']"}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Programa']"}),
            'seg': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valor': ('django.db.models.fields.IntegerField', [], {})
        },
        'app.estilopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EstiloPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.grafica': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'Grafica', '_ormbases': ['app.Emision']},
            'alto': ('django.db.models.fields.IntegerField', [], {}),
            'ancho': ('django.db.models.fields.IntegerField', [], {}),
            'color': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'emision_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app.Emision']", 'unique': 'True', 'primary_key': 'True'})
        },
        'app.horario': {
            'Meta': {'object_name': 'Horario'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'dias': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '13', 'null': 'True'}),
            'enElAire': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fechaAlta': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'horaFin': ('django.db.models.fields.TimeField', [], {'null': 'True'}),
            'horaInicio': ('django.db.models.fields.TimeField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Medio']"}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'programas'", 'to': "orm['app.Programa']"}),
            'tarifa': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'app.industria': {
            'Meta': {'object_name': 'Industria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'rubro': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'industrias'", 'to': "orm['app.Rubro']"})
        },
        'app.marca': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'sector'),)", 'object_name': 'Marca'},
            'anunciante': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'marcas'", 'null': 'True', 'to': "orm['app.Anunciante']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'marcas'", 'to': "orm['app.Sector']"})
        },
        'app.medio': {
            'Meta': {'unique_together': "((u'nombre', u'ciudad'),)", 'object_name': 'Medio'},
            'canal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'dvr': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'formato': ('django.db.models.fields.CharField', [], {'default': "u'A'", 'max_length': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'incremento_color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.migracion': {
            'Meta': {'object_name': 'Migracion'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'server': ('django.db.models.fields.IntegerField', [], {}),
            'servidor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'servidor'", 'to': "orm['app.Servidor']"})
        },
        'app.producto': {
            'Meta': {'unique_together': "((u'nombre', u'marca'),)", 'object_name': 'Producto'},
            'agencia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.Agencia']"}),
            'estilo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.EstiloPublicidad']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_en_simpson': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'marca': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'productos'", 'to': "orm['app.Marca']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'nombre_del_patron': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'recursos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.RecursoPublicidad']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Servidor']", 'null': 'True', 'blank': 'True'}),
            'tipo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.TipoPublicidad']"}),
            'valores': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.ValorPublicidad']"})
        },
        'app.programa': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'medio'),)", 'object_name': 'Programa'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'programas'", 'to': "orm['app.Medio']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'app.recursopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'RecursoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.rubro': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Rubro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.secciongrafica': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'SeccionGrafica', '_ormbases': ['app.Programa']},
            'alto': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ancho': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'cantColum': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'programa_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app.Programa']", 'unique': 'True', 'primary_key': 'True'})
        },
        'app.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industria': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'sectores'", 'to': "orm['app.Industria']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.tipopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'TipoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.valorpublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'ValorPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['app']