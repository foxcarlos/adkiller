# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Rubro'
        db.create_table('app_rubro', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
        ))
        db.send_create_signal('app', ['Rubro'])

        # Adding model 'Industria'
        db.create_table('app_industria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('rubro', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'industrias', to=orm['app.Rubro'])),
        ))
        db.send_create_signal('app', ['Industria'])

        # Adding model 'Sector'
        db.create_table('app_sector', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('industria', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'sectores', to=orm['app.Industria'])),
        ))
        db.send_create_signal('app', ['Sector'])

        # Adding model 'Marca'
        db.create_table('app_marca', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('anunciante', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'marcas', null=True, to=orm['app.Anunciante'])),
            ('sector', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'marcas', to=orm['app.Sector'])),
        ))
        db.send_create_signal('app', ['Marca'])

        # Adding unique constraint on 'Marca', fields ['nombre', 'sector']
        db.create_unique('app_marca', ['nombre', 'sector_id'])

        # Adding model 'Anunciante'
        db.create_table('app_anunciante', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
        ))
        db.send_create_signal('app', ['Anunciante'])

        # Adding model 'Producto'
        db.create_table('app_producto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('marca', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'productos', to=orm['app.Marca'])),
            ('agencia', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'productos', null=True, to=orm['app.Agencia'])),
            ('observaciones', self.gf('django.db.models.fields.CharField')(max_length=500, blank=True)),
            ('tipo_publicidad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'productos', null=True, to=orm['app.TipoPublicidad'])),
            ('estilo_publicidad', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'productos', null=True, to=orm['app.EstiloPublicidad'])),
            ('nombre_del_patron', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal('app', ['Producto'])

        # Adding unique constraint on 'Producto', fields ['nombre', 'marca']
        db.create_unique('app_producto', ['nombre', 'marca_id'])

        # Adding M2M table for field valores on 'Producto'
        db.create_table('app_producto_valores', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('producto', models.ForeignKey(orm['app.producto'], null=False)),
            ('valorpublicidad', models.ForeignKey(orm['app.valorpublicidad'], null=False))
        ))
        db.create_unique('app_producto_valores', ['producto_id', 'valorpublicidad_id'])

        # Adding M2M table for field recursos on 'Producto'
        db.create_table('app_producto_recursos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('producto', models.ForeignKey(orm['app.producto'], null=False)),
            ('recursopublicidad', models.ForeignKey(orm['app.recursopublicidad'], null=False))
        ))
        db.create_unique('app_producto_recursos', ['producto_id', 'recursopublicidad_id'])

        # Adding model 'TipoPublicidad'
        db.create_table('app_tipopublicidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('app', ['TipoPublicidad'])

        # Adding model 'EstiloPublicidad'
        db.create_table('app_estilopublicidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('app', ['EstiloPublicidad'])

        # Adding model 'ValorPublicidad'
        db.create_table('app_valorpublicidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('app', ['ValorPublicidad'])

        # Adding model 'RecursoPublicidad'
        db.create_table('app_recursopublicidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.IntegerField')(unique=True)),
            ('descripcion', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('app', ['RecursoPublicidad'])

        # Adding model 'Agencia'
        db.create_table('app_agencia', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal('app', ['Agencia'])

        # Adding model 'Ciudad'
        db.create_table('app_ciudad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal('app', ['Ciudad'])

        # Adding model 'Medio'
        db.create_table('app_medio', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ciudad', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'medios', to=orm['app.Ciudad'])),
            ('soporte', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'medios', to=orm['app.Soporte'])),
        ))
        db.send_create_signal('app', ['Medio'])

        # Adding unique constraint on 'Medio', fields ['nombre', 'ciudad']
        db.create_unique('app_medio', ['nombre', 'ciudad_id'])

        # Adding model 'Soporte'
        db.create_table('app_soporte', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
        ))
        db.send_create_signal('app', ['Soporte'])

        # Adding model 'Programa'
        db.create_table('app_programa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('medio', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'programas', to=orm['app.Medio'])),
        ))
        db.send_create_signal('app', ['Programa'])

        # Adding unique constraint on 'Programa', fields ['nombre', 'medio']
        db.create_unique('app_programa', ['nombre', 'medio_id'])

        # Adding model 'Emision'
        db.create_table('app_emision', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('migracion', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'emisiones', null=True, to=orm['app.Migracion'])),
            ('producto', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'emisiones', to=orm['app.Producto'])),
            ('programa', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'emisiones', to=orm['app.Programa'])),
            ('fecha', self.gf('django.db.models.fields.DateField')(db_index=True)),
            ('hora', self.gf('django.db.models.fields.TimeField')()),
            ('formato', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('orden', self.gf('django.db.models.fields.IntegerField')()),
            ('duracion', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('alto', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('ancho', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('superficie', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('seg', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('valor', self.gf('django.db.models.fields.IntegerField')()),
            ('origen', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
            ('inicio', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('fin', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('score', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('duracion_adtrack', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
        ))
        db.send_create_signal('app', ['Emision'])

        # Adding unique constraint on 'Emision', fields ['producto', 'programa', 'fecha', 'hora', 'orden']
        db.create_unique('app_emision', ['producto_id', 'programa_id', 'fecha', 'hora', 'orden'])

        # Adding model 'Migracion'
        db.create_table('app_migracion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('hora', self.gf('django.db.models.fields.TimeField')()),
            ('server', self.gf('django.db.models.fields.IntegerField')()),
            ('servidor', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'servidor', to=orm['app.Servidor'])),
            ('completa', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('app', ['Migracion'])

        # Adding model 'Servidor'
        db.create_table('app_servidor', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('nro', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('automatico', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('fecha', self.gf('django.db.models.fields.DateField')(db_index=True, null=True, blank=True)),
            ('puerto_mysql', self.gf('django.db.models.fields.PositiveIntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('app', ['Servidor'])


    def backwards(self, orm):
        # Removing unique constraint on 'Emision', fields ['producto', 'programa', 'fecha', 'hora', 'orden']
        db.delete_unique('app_emision', ['producto_id', 'programa_id', 'fecha', 'hora', 'orden'])

        # Removing unique constraint on 'Programa', fields ['nombre', 'medio']
        db.delete_unique('app_programa', ['nombre', 'medio_id'])

        # Removing unique constraint on 'Medio', fields ['nombre', 'ciudad']
        db.delete_unique('app_medio', ['nombre', 'ciudad_id'])

        # Removing unique constraint on 'Producto', fields ['nombre', 'marca']
        db.delete_unique('app_producto', ['nombre', 'marca_id'])

        # Removing unique constraint on 'Marca', fields ['nombre', 'sector']
        db.delete_unique('app_marca', ['nombre', 'sector_id'])

        # Deleting model 'Rubro'
        db.delete_table('app_rubro')

        # Deleting model 'Industria'
        db.delete_table('app_industria')

        # Deleting model 'Sector'
        db.delete_table('app_sector')

        # Deleting model 'Marca'
        db.delete_table('app_marca')

        # Deleting model 'Anunciante'
        db.delete_table('app_anunciante')

        # Deleting model 'Producto'
        db.delete_table('app_producto')

        # Removing M2M table for field valores on 'Producto'
        db.delete_table('app_producto_valores')

        # Removing M2M table for field recursos on 'Producto'
        db.delete_table('app_producto_recursos')

        # Deleting model 'TipoPublicidad'
        db.delete_table('app_tipopublicidad')

        # Deleting model 'EstiloPublicidad'
        db.delete_table('app_estilopublicidad')

        # Deleting model 'ValorPublicidad'
        db.delete_table('app_valorpublicidad')

        # Deleting model 'RecursoPublicidad'
        db.delete_table('app_recursopublicidad')

        # Deleting model 'Agencia'
        db.delete_table('app_agencia')

        # Deleting model 'Ciudad'
        db.delete_table('app_ciudad')

        # Deleting model 'Medio'
        db.delete_table('app_medio')

        # Deleting model 'Soporte'
        db.delete_table('app_soporte')

        # Deleting model 'Programa'
        db.delete_table('app_programa')

        # Deleting model 'Emision'
        db.delete_table('app_emision')

        # Deleting model 'Migracion'
        db.delete_table('app_migracion')

        # Deleting model 'Servidor'
        db.delete_table('app_servidor')


    models = {
        'app.agencia': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Agencia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.anunciante': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Anunciante'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.emision': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'unique_together': "((u'producto', u'programa', u'fecha', u'hora', u'orden'),)", 'object_name': 'Emision'},
            'alto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'ancho': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'duracion': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'duracion_adtrack': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'fin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'formato': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inicio': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'migracion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Migracion']"}),
            'orden': ('django.db.models.fields.IntegerField', [], {}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'to': "orm['app.Producto']"}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'to': "orm['app.Programa']"}),
            'score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'seg': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'superficie': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'valor': ('django.db.models.fields.IntegerField', [], {})
        },
        'app.estilopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EstiloPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.industria': {
            'Meta': {'object_name': 'Industria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'rubro': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'industrias'", 'to': "orm['app.Rubro']"})
        },
        'app.marca': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'sector'),)", 'object_name': 'Marca'},
            'anunciante': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'marcas'", 'null': 'True', 'to': "orm['app.Anunciante']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'marcas'", 'to': "orm['app.Sector']"})
        },
        'app.medio': {
            'Meta': {'unique_together': "((u'nombre', u'ciudad'),)", 'object_name': 'Medio'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.migracion': {
            'Meta': {'object_name': 'Migracion'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'server': ('django.db.models.fields.IntegerField', [], {}),
            'servidor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'servidor'", 'to': "orm['app.Servidor']"})
        },
        'app.producto': {
            'Meta': {'unique_together': "((u'nombre', u'marca'),)", 'object_name': 'Producto'},
            'agencia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.Agencia']"}),
            'estilo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.EstiloPublicidad']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marca': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'productos'", 'to': "orm['app.Marca']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_del_patron': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'recursos': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'productos'", 'symmetrical': 'False', 'to': "orm['app.RecursoPublicidad']"}),
            'tipo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.TipoPublicidad']"}),
            'valores': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'productos'", 'symmetrical': 'False', 'to': "orm['app.ValorPublicidad']"})
        },
        'app.programa': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'medio'),)", 'object_name': 'Programa'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'programas'", 'to': "orm['app.Medio']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'app.recursopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'RecursoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.rubro': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Rubro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industria': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'sectores'", 'to': "orm['app.Industria']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.tipopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'TipoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.valorpublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'ValorPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        }
    }

    complete_apps = ['app']