# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AudioVisual'
        db.create_table('app_audiovisual', (
            ('emision_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app.Emision'], unique=True, primary_key=True)),
            ('inicio', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('fin', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('score', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
            ('duracion_adtrack', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True)),
        ))
        db.send_create_signal('app', ['AudioVisual'])

        # Adding model 'Grafica'
        db.create_table('app_grafica', (
            ('emision_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['app.Emision'], unique=True, primary_key=True)),
            ('alto', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('ancho', self.gf('django.db.models.fields.DecimalField')(max_digits=10, decimal_places=4)),
            ('color', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('app', ['Grafica'])

        # Deleting field 'Emision.alto'
        db.delete_column('app_emision', 'alto')

        # Deleting field 'Emision.score'
        db.delete_column('app_emision', 'score')

        # Deleting field 'Emision.ancho'
        db.delete_column('app_emision', 'ancho')

        # Deleting field 'Emision.duracion_adtrack'
        db.delete_column('app_emision', 'duracion_adtrack')

        # Deleting field 'Emision.inicio'
        db.delete_column('app_emision', 'inicio')

        # Deleting field 'Emision.superficie'
        db.delete_column('app_emision', 'superficie')

        # Deleting field 'Emision.fin'
        db.delete_column('app_emision', 'fin')

        # Adding field 'Emision.observaciones'
        db.add_column('app_emision', 'observaciones',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)


        # Changing field 'Emision.formato'
        db.alter_column('app_emision', 'formato', self.gf('django.db.models.fields.CharField')(max_length=1))

        # Changing field 'Emision.seg'
        db.alter_column('app_emision', 'seg', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4))

    def backwards(self, orm):
        # Deleting model 'AudioVisual'
        db.delete_table('app_audiovisual')

        # Deleting model 'Grafica'
        db.delete_table('app_grafica')

        # Adding field 'Emision.alto'
        db.add_column('app_emision', 'alto',
                      self.gf('django.db.models.fields.DecimalField')(default='', max_digits=10, decimal_places=4),
                      keep_default=False)

        # Adding field 'Emision.score'
        db.add_column('app_emision', 'score',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True),
                      keep_default=False)

        # Adding field 'Emision.ancho'
        db.add_column('app_emision', 'ancho',
                      self.gf('django.db.models.fields.DecimalField')(default=3, max_digits=10, decimal_places=4),
                      keep_default=False)

        # Adding field 'Emision.duracion_adtrack'
        db.add_column('app_emision', 'duracion_adtrack',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True),
                      keep_default=False)

        # Adding field 'Emision.inicio'
        db.add_column('app_emision', 'inicio',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Emision.superficie'
        raise RuntimeError("Cannot reverse this migration. 'Emision.superficie' and its values cannot be restored.")
        # Adding field 'Emision.fin'
        db.add_column('app_emision', 'fin',
                      self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=10, decimal_places=4, blank=True),
                      keep_default=False)

        # Deleting field 'Emision.observaciones'
        db.delete_column('app_emision', 'observaciones')


        # Changing field 'Emision.formato'
        db.alter_column('app_emision', 'formato', self.gf('django.db.models.fields.CharField')(max_length=5))

        # User chose to not deal with backwards NULL issues for 'Emision.seg'
        raise RuntimeError("Cannot reverse this migration. 'Emision.seg' and its values cannot be restored.")

    models = {
        'app.agencia': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Agencia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.anunciante': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Anunciante'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.audiovisual': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'AudioVisual', '_ormbases': ['app.Emision']},
            'duracion_adtrack': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'emision_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app.Emision']", 'unique': 'True', 'primary_key': 'True'}),
            'fin': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'inicio': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'score': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'})
        },
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.emision': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'unique_together': "((u'producto', u'programa', u'fecha', u'hora', u'orden'),)", 'object_name': 'Emision'},
            'duracion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'formato': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'migracion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Migracion']"}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {}),
            'origen': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'to': "orm['app.Producto']"}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'to': "orm['app.Programa']"}),
            'seg': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '4', 'blank': 'True'}),
            'valor': ('django.db.models.fields.IntegerField', [], {})
        },
        'app.estilopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EstiloPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.grafica': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'Grafica', '_ormbases': ['app.Emision']},
            'alto': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'ancho': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '4'}),
            'color': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'emision_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['app.Emision']", 'unique': 'True', 'primary_key': 'True'})
        },
        'app.industria': {
            'Meta': {'object_name': 'Industria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'rubro': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'industrias'", 'to': "orm['app.Rubro']"})
        },
        'app.marca': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'sector'),)", 'object_name': 'Marca'},
            'anunciante': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'marcas'", 'null': 'True', 'to': "orm['app.Anunciante']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'marcas'", 'to': "orm['app.Sector']"})
        },
        'app.medio': {
            'Meta': {'unique_together': "((u'nombre', u'ciudad'),)", 'object_name': 'Medio'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.migracion': {
            'Meta': {'object_name': 'Migracion'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'server': ('django.db.models.fields.IntegerField', [], {}),
            'servidor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'servidor'", 'to': "orm['app.Servidor']"})
        },
        'app.producto': {
            'Meta': {'unique_together': "((u'nombre', u'marca'),)", 'object_name': 'Producto'},
            'agencia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.Agencia']"}),
            'estilo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.EstiloPublicidad']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marca': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'productos'", 'to': "orm['app.Marca']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre_del_patron': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'recursos': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'productos'", 'symmetrical': 'False', 'to': "orm['app.RecursoPublicidad']"}),
            'tipo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.TipoPublicidad']"}),
            'valores': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'productos'", 'symmetrical': 'False', 'to': "orm['app.ValorPublicidad']"})
        },
        'app.programa': {
            'Meta': {'ordering': "[u'nombre']", 'unique_together': "((u'nombre', u'medio'),)", 'object_name': 'Programa'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'programas'", 'to': "orm['app.Medio']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'app.recursopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'RecursoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.rubro': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Rubro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.sector': {
            'Meta': {'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industria': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'sectores'", 'to': "orm['app.Industria']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.tipopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'TipoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.valorpublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'ValorPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        }
    }

    complete_apps = ['app']