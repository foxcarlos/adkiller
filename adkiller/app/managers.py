#coding=utf-8

import re

from django.db import models

list_of_similar_chars = [u"AaÁáÄäÀàÂâ", u"EeÉéÈèËëÊê", u"IiÍíÏïÌìÎî",
                         u"OoÓóÖöÒòÔô", u"UuÚúÜüÙùÛû", u"NnÑñ", u"CcÇç"]

def or_regex(list_classes):
    patterns = []
    for class_ in list_classes:
        pat = list(class_)
        pat = u'|'.join(pat)
        pat = u'(' + pat + u')'
        patterns.append(pat)
    return patterns

def replace_similar_chars_for_pattern(patterns, text):
    for pattern in patterns:
        text = re.sub(pattern, pattern, text)
    return text


class SimpleAccentFreeManager(models.Manager):

    def unnaccented_multiple_word_filter(self, search_fields, text, qs=None):
        if qs is None:
            qs = self.all()
        patterns = or_regex(list_of_similar_chars)
        for word in text.split():
            conditions = models.Q()
            escaped_word = replace_similar_chars_for_pattern(patterns, word)
            for search_field in search_fields:
                conditions |= models.Q(**{'{lf}__iregex'.format(lf=search_field): escaped_word})
            qs = qs.filter(conditions)

        return qs

    def unnaccented_filter(self, search_field, text):
        patterns = or_regex(list_of_similar_chars)
        escaped_string = replace_similar_chars_for_pattern(patterns, text)
        return self.filter(
            **{'{lf}__regex'.format(lf=search_field): escaped_string}
        )


class AccentFreeQuerySet(models.query.QuerySet):

    def __init__(self, model=None, query=None, using=None):
        super(AccentFreeQuerySet, self).__init__(model, query, using)
        self.prepare_query_string_replacements()

    def prepare_query_string_replacements(self):
        iequivalent_classes = list_of_similar_chars[:]
        equivalent_classes = []
        for eq_chars in iequivalent_classes:
            equivalent_classes.append(eq_chars[0::2]) #uppercases
            equivalent_classes.append(eq_chars[1::2]) #lowercases

        self.patterns = or_regex(equivalent_classes)
        self.ipatterns = or_regex(iequivalent_classes)

    def regex_escape(self, text, patterns):
        return replace_similar_chars_for_pattern(patterns, text)

    def _filter_or_exclude(self, negate, *args, **kwargs):
        kwargs = self._filter_or_exclude_aexact(negate, *args, **kwargs)
        kwargs = self._filter_or_exclude_acontains(negate, *args, **kwargs)
        kwargs = self._filter_or_exclude_aiexact(negate, *args, **kwargs)
        kwargs = self._filter_or_exclude_aicontains(negate, *args, **kwargs)
        return super(AccentFreeQuerySet, self)._filter_or_exclude(negate, *args, **kwargs)

    def _filter_or_exclude_aexact(self, negate, *args, **kwargs):
        cust_lookups = filter(lambda s: s[0].endswith('__aexact'), kwargs.items())
        for lookup in cust_lookups:
            kwargs.pop(lookup[0])
            lookup_prefix = lookup[0].rsplit('__',1)[0]
            kwargs.update(
                {
                    lookup_prefix + '__regex':
                        '^' + self.regex_escape(lookup[1], self.patterns) + '$'
                }
            )
        q_lookups = filter(lambda s: type(s) == models.Q, args)
        args = [obj for obj in args if type(obj) != models.Q]

        return kwargs

    def _filter_or_exclude_acontains(self, negate, *args, **kwargs):
        cust_lookups = filter(lambda s: s[0].endswith('__acontains'), kwargs.items())
        for lookup in cust_lookups:
            kwargs.pop(lookup[0])
            lookup_prefix = lookup[0].rsplit('__',1)[0]
            kwargs.update(
                {
                    lookup_prefix + '__regex':
                        self.regex_escape(lookup[1], self.patterns)
                }
            )
        return kwargs

    def _filter_or_exclude_aiexact(self, negate, *args, **kwargs):
        cust_lookups = filter(lambda s: s[0].endswith('__aiexact'), kwargs.items())
        for lookup in cust_lookups:
            kwargs.pop(lookup[0])
            lookup_prefix = lookup[0].rsplit('__',1)[0]
            kwargs.update(
                {
                    lookup_prefix + '__iregex':
                        '^' + self.regex_escape(lookup[1], self.ipatterns) + '$'
                }
            )
        return kwargs

    def _filter_or_exclude_aicontains(self, negate, *args, **kwargs):
        cust_lookups = filter(lambda s: s[0].endswith('__aicontains'), kwargs.items())
        for lookup in cust_lookups:
            kwargs.pop(lookup[0])
            lookup_prefix = lookup[0].rsplit('__',1)[0]
            kwargs.update(
                {
                    lookup_prefix + '__iregex':
                        self.regex_escape(lookup[1], self.ipatterns)
                }
            )
        return kwargs

