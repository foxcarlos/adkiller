# -*- coding: utf-8 -*-
#from django.conf import settings
from django.db.models import Q
from django.db.models.sql.constants import LOOKUP_SEP
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from models import *
from tastypie.cache import SimpleCache, NoCache
from tastypie import fields
import csv
import StringIO
from tastypie.serializers import Serializer
from tastypie.authentication import SessionAuthentication, MultiAuthentication, BasicAuthentication
from tastypie.authorization import DjangoAuthorization
import simplejson
from django.core.serializers import json

class CSVSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'csv']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'csv': 'text/csv',
    }

    def to_csv(self, data, options=None):
        options = options or {}
        data = self.to_simple(data, options)
        data = data['objects']
        raw_data = ""  #StringIO.StringIO()
        for item in data:  #data.items():
            for v in data:  #.items()[0]:
                raw_data += "\t%s" % v
            #writer = csv.DictWriter(raw_data, item, extrasaction='ignore')
            #writer.writer.writerow(item)
            raw_data += "\n"
        return raw_data

    def from_csv(self, content):
        raw_data = StringIO.StringIO(content)
        data = []
        # Untested, so this might not work exactly right.
        for item in csv.DictReader(raw_data):
            data.append(item)
        return data

    def to_json(self, data, options=None):
        options = options or {}

        data = self.to_simple(data, options)

        # Add in the current time.
	import time
        data['requested_time'] = time.time()

        return simplejson.dumps(data, cls=json.DjangoJSONEncoder,ensure_ascii=True, sort_keys=True, encoding = 'utf-8')

    def from_json(self, content):
        data = simplejson.loads(content)

        if 'requested_time' in data:
            # Log the request here...
            pass

        return data


class MyResource(ModelResource):
    class Meta:
        cache = NoCache()  #SimpleCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()


class SoporteResource(MyResource):

    padre = fields.ForeignKey("self", 'padre')

    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Soporte.objects.filter(padre__isnull=True)
        resource_name = 'soporte'
        filtering = {
            'nombre': ALL,
            'id': ALL,
            'padre': ALL_WITH_RELATIONS,
        }



class CiudadResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())

        queryset = Ciudad.objects.all()
        resource_name = 'ciudad'
        filtering = {
            'nombre': ALL,
        }


class EtiquetaProgramaResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())

        queryset = EtiquetaPrograma.objects.all()
        resource_name = 'etiquetaprograma'
        filtering = {
            'nombre': ALL,
        }


class EtiquetaMedioResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())

        queryset = EtiquetaMedio.objects.all()
        resource_name = 'etiquetamedio'
        filtering = {
            'nombre': ALL,
        }


class EtiquetaProductoResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())

        queryset = EtiquetaProducto.objects.all()
        resource_name = 'etiquetaproducto'
        filtering = {
            'nombre': ALL,
        }


class MedioResource(MyResource):
    soporte = fields.ForeignKey(SoporteResource, 'soporte')
    ciudad = fields.ForeignKey(CiudadResource, 'ciudad')
    id_ciudad = fields.IntegerField(readonly=True)
    etiquetas = fields.ManyToManyField(EtiquetaMedioResource,'etiquetas')

    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Medio.objects.all()
        resource_name = 'medio'
        filtering = {
            'id': ALL,
            'nombre': ALL,
            'publicar': ALL,
            'ciudad': ALL_WITH_RELATIONS,
            'soporte': ALL_WITH_RELATIONS,
            'etiquetas': ALL_WITH_RELATIONS,
        }


    def dehydrate_id_ciudad(self, bundle):
        return bundle.obj.ciudad_id


class ProgramaResource(MyResource):

    etiquetas = fields.ManyToManyField(EtiquetaProgramaResource,'etiquetas')

    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Programa.objects.all()
        resource_name = 'programa'
        filtering = {
            'id': ALL,
            'nombre': ALL,
            'etiquetas': ALL_WITH_RELATIONS, 
        }


class HorarioResource(MyResource):
    programa = fields.ForeignKey(ProgramaResource, 'programa')
    medio = fields.ForeignKey(MedioResource, 'medio')
    id_programa = fields.IntegerField(readonly=True)
    nombre_programa = fields.CharField(readonly=True)
    ident_programa = fields.CharField(readonly=True)
    id_medio = fields.IntegerField(readonly=True)

    class Meta:
        cache = NoCache() #SimpleCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Horario.objects.all()
        resource_name = 'horario'
        filtering = {
            'id': ALL,
            'medio': ALL_WITH_RELATIONS,
            'programa': ALL_WITH_RELATIONS,
            'enElAire': ALL,
            'completa': ALL,
        }

    def dehydrate_id_programa(self, bundle):
        return bundle.obj.programa_id

    def dehydrate_nombre_programa(self, bundle):
        return bundle.obj.programa.nombre

    def dehydrate_ident_programa(self, bundle):
        return bundle.obj.programa.identificador

    def dehydrate_id_medio(self, bundle):
        return bundle.obj.medio_id


class AnuncianteResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Anunciante.objects.all()
        resource_name = 'anunciante'
        filtering = {
            'id': ALL,
            'nombre': ALL,
        }


class TipoResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = TipoPublicidad.objects.all()
        resource_name = 'tipo_publicidad'

class EstiloResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = EstiloPublicidad.objects.all()
        resource_name = 'estilo_publicidad'


class ValorPublicidadResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = ValorPublicidad.objects.all()
        resource_name = 'valor'

class RecursosPublicidadResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = RecursoPublicidad.objects.all()
        resource_name = 'recursos'

class RubroResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Rubro.objects.all()
        resource_name = 'rubro'


class IndustriaResource(MyResource):
    rubro = fields.ForeignKey(Rubro, 'rubro')
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Industria.objects.all()
        resource_name = 'industria'
        filtering = {
            'nombre': ALL,
            'rubro': ALL_WITH_RELATIONS,
        }


class SectorResource(MyResource):
    industria = fields.ForeignKey(IndustriaResource, 'industria')
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Sector.objects.all()
        resource_name = 'sector'
        filtering = {
            'id': ALL,
            'nombre': ALL,
            'industria': ALL_WITH_RELATIONS,
        }


class MarcaResource(MyResource):
    anunciante = fields.ForeignKey(AnuncianteResource, 'anunciante')
    sector = fields.ForeignKey(SectorResource, 'sector')

    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Marca.objects.all()
        resource_name = 'marca'
        filtering = {
            'id': ALL,
            'nombre': ALL,
            'anunciante': ALL_WITH_RELATIONS,
            'sector': ALL_WITH_RELATIONS,
        }


class AgenciaResource(MyResource):
    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Agencia.objects.all()
        resource_name = 'agencia'


class ProductoResource(MyResource):
    marca = fields.ForeignKey(MarcaResource, 'marca')
    estilo_publicidad = fields.ForeignKey(EstiloResource, 'estilo_publicidad')
    tipo_publicidad = fields.ForeignKey(TipoResource, 'tipo_publicidad')
    recursos = fields.ManyToManyField(RecursosPublicidadResource,'recursos')
    otras_marcas = fields.ManyToManyField(MarcaResource,'otras_marcas')
    etiquetas = fields.ManyToManyField(EtiquetaProductoResource,'etiquetas')

    class Meta:
        cache = NoCache() #(timeout=60*60*24)   # 1 dia
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Producto.objects.all()
        resource_name = 'producto'
        filtering = {
            'nombre': ALL,
            'marca': ALL_WITH_RELATIONS,
            'otras_marcas': ALL_WITH_RELATIONS,
            'estilo_publicidad': ALL_WITH_RELATIONS,
            'tipo_publicidad': ALL_WITH_RELATIONS,
            'valores' :ALL_WITH_RELATIONS,
            'recursos' :ALL_WITH_RELATIONS,
            'otras_marcas' :ALL_WITH_RELATIONS,
            'etiquetas': ALL_WITH_RELATIONS,
        }


class EmisionResource(MyResource):
    producto = fields.ForeignKey(ProductoResource, 'producto', null=True)
    programa = fields.CharField(readonly=True)
    soporte = fields.CharField(readonly=True)
    horario = fields.ForeignKey(HorarioResource, 'horario',null=True)
    tipo_publicidad = fields.ForeignKey(TipoResource, 'tipo_publicidad',null=True)
    marca = fields.CharField(readonly=True)
    medio = fields.CharField(readonly=True)
    video = fields.CharField(readonly=True)
    thumb = fields.CharField(readonly=True)
    orden_cantidad = fields.CharField(readonly=True)

    filetype = fields.CharField(readonly=True)



    class Meta:
        cache = NoCache()  #SimpleCache() #(timeout=60*60*2)   # 2 horas
        serializer = CSVSerializer()
        authentication = MultiAuthentication(SessionAuthentication(), BasicAuthentication())
        authorization = DjangoAuthorization()

        queryset = Emision.objects.all()
        resource_name = 'emision'
        filtering = {
            'producto': ALL_WITH_RELATIONS,
            'tipo_publicidad': ALL_WITH_RELATIONS,
            'horario': ALL_WITH_RELATIONS,
            'fecha': ['exact', 'lt', 'lte', 'gte', 'gt'],
            'hora': ['exact', 'lt', 'lte', 'gte', 'gt'],

        }
        #ordering = ['fecha', 'hora']

    def dehydrate_valor(self, bundle):
        return int(bundle.obj.tarifa_real(bundle.request.user))

    def dehydrate_hora(self, bundle):
        return bundle.obj.hora_real().strftime("%H:%M:%S")

    def dehydrate_video(self, bundle):
        if bundle.obj.origen:
            return "/app/video_emision/%s" % bundle.obj.id

    def dehydrate_filetype(self, bundle):
        if bundle.obj.segmentos.count():
            return "iframe"
        else:
            return bundle.obj.origen[-3:]

    def dehydrate_thumb(self, bundle):
        return bundle.obj.thumb()

    def dehydrate_orden_cantidad(self, bundle):
        # TODO: por ahora pongo "-" porque es incorrecto el valor que informamos ahora
        return "-"
        if (bundle.obj.orden is None) or (bundle.obj.cantidad_avisos_tanda() is None):
            return "1/1"
        else:
            return "%s/%s" % (bundle.obj.orden, bundle.obj.cantidad_avisos_tanda())

    def dehydrate_medio(self, bundle):
        if bundle.obj.horario:
            if bundle.obj.horario.medio:
                return bundle.obj.horario.medio.__unicode__()
        return ""

    def dehydrate_fecha(self, bundle):
        return bundle.obj.fecha.strftime("%d/%m/%Y")

    def dehydrate_programa(self, bundle):
        if bundle.obj.horario:
            if bundle.obj.horario.programa:
                return bundle.obj.horario.programa.__unicode__()
        return ""

    def dehydrate_soporte(self, bundle):
        if bundle.obj.horario:
            if bundle.obj.horario.medio:
                return bundle.obj.horario.medio.soporte.__unicode__()
        return ""

    def dehydrate_marca(self, bundle):
        if bundle.obj.producto:
            return bundle.obj.producto.marca.__unicode__()

    def dehydrate_producto(self, bundle):
        if bundle.obj.producto:
            return bundle.obj.producto.nombre

#    def dehydrate_marca_producto(self, bundle):
#        return bundle.obj.producto.marca.nombre + " - " + bundle.obj.producto.nombre

    def build_filters(self, filters=None):
        """
        Con los filtros que aparezcan dos veces, hacer un Q object que sea el
        OR de las opciones.
        """
        if filters is None:
            filters = {}
        # XXX: Logica copiada de Resource.build_filters
        if getattr(self._meta, 'queryset', None) is not None:
            # Get the possible query terms from the current QuerySet.
            if hasattr(self._meta.queryset.query.query_terms, 'keys'):
                # Django 1.4 & below compatibility.
                query_terms = self._meta.queryset.query.query_terms.keys()
            else:
                # Django 1.5+.
                query_terms = self._meta.queryset.query.query_terms
        else:
            if hasattr(QUERY_TERMS, 'keys'):
                # Django 1.4 & below compatibility.
                query_terms = QUERY_TERMS.keys()
            else:
                # Django 1.5+.
                query_terms = QUERY_TERMS

        single_filters = {}
        multiple_filters = []
        for filter_expr, values in filters.lists():
            # Si hay un solo value, la logica es la de siempre, por lo que
            # lo ponemos en single_filters para luego pasarselo al
            # build_filters original
            if len(values) <= 1:
                single_filters[filter_expr] = filters[filter_expr]
                continue

            # Si no, hacemos la misma logica que el build_filters original pero
            # creando objectos Q
            q_objects = []
            for value in values:
                filter_bits = filter_expr.split(LOOKUP_SEP)
                field_name = filter_bits.pop(0)
                filter_type = 'exact'

                if not field_name in self.fields:
                    # It's not a field we know about. Move along citizen.
                    continue

                if len(filter_bits) and filter_bits[-1] in query_terms:
                    filter_type = filter_bits.pop()

                lookup_bits = self.check_filtering(field_name, filter_type, filter_bits)
                value = self.filter_value_to_python(value, field_name, filters, filter_expr, filter_type)

                db_field_name = LOOKUP_SEP.join(lookup_bits)
                qs_filter = "%s%s%s" % (db_field_name, LOOKUP_SEP, filter_type)

                q_object = Q(**{qs_filter: value})
                q_objects.append(q_object)

            ored_q_object = reduce(lambda q1, q2: q1 | q2, q_objects, Q())
            multiple_filters.append(ored_q_object)

        # Ahora, single_filters tiene los filtros que hay una sola opción
        # y le hacemos el procesamiento original
        qs_filters = super(EmisionResource, self)\
                          .build_filters(filters=single_filters)


        # FIXME: Aca pasamos los multiple_filters a apply_filters muy feamente
        qs_filters['MULTIPLE_FILTERS'] = multiple_filters

        return qs_filters

    def apply_filters(self, request, applicable_filters):
        """
        An ORM-specific implementation of ``apply_filters``.

        The default simply applies the ``applicable_filters`` as ``**kwargs``,
        but should make it possible to do more advanced things.
        """
        multiple_filters = applicable_filters.pop('MULTIPLE_FILTERS')

        filters = {}
        for lookup in applicable_filters.keys():
                filters[lookup] = applicable_filters[lookup]

        return self.get_object_list(request)\
                   .filter(*multiple_filters, **filters)

