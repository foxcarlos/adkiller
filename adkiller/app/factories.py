import factory
from factory import post_generation
from datetime import date, time
from django.contrib.auth.models import User

from models import Horario, Medio, Programa, Ciudad, Soporte


class HorarioFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Horario
    medio = factory.SubFactory('adkiller.app.factories.MedioFactory')
    programa = factory.SubFactory('adkiller.app.factories.ProgramaFactory')
    fechaAlta = date(2000, 01, 01)
    horaInicio = time(0, 0, 0)
    horaFin = time(23, 59, 59)
    dias = '1,1,1,1,1,1,1'


class MedioFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Medio

    ciudad = factory.SubFactory('adkiller.app.factories.CiudadFactory')
    soporte = factory.SubFactory('adkiller.app.factories.SoporteFactory')
    nombre = factory.Sequence(lambda count: 'nombre{0}'.format(count))


class ProgramaFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Programa
    nombre = factory.Sequence(lambda count: 'nombre{0}'.format(count))


class CiudadFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Ciudad
    nombre = factory.Sequence(lambda count: 'nombre{0}'.format(count))


class SoporteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Soporte
    nombre = factory.Sequence(lambda count: 'nombre{0}'.format(count))


class UserFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = User

    username = factory.Sequence(lambda count: 'username{0}'.format(count))
    first_name = factory.Sequence(lambda count: 'first_name{0}'.format(count))
    last_name = factory.Sequence(lambda count: 'last_name{0}'.format(count))
    email = factory.Sequence(lambda count: 'user{0}@example.com'.format(count))
    password = 'defaultpassword'

    @factory.post_generation
    def post(self, create, extracted, **kwargs):
        if create:
            self.set_password(self.password)
            self.save()
