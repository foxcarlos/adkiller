(function($){
	var initLayout = function() {
		var hash = window.location.hash.replace('#', '');
		var currentTab = $('ul.navigationTabs a')
							.bind('click', showTab)
							.filter('a[rel=' + hash + ']');
		if (currentTab.size() == 0) {
			currentTab = $('ul.navigationTabs a:first');
		}
		showTab.apply(currentTab.get(0));

		$('#clearSelection').bind('click', function(){
			$('#date3').DatePickerClear();
			return false;
		});
		
		
  		var fdesde = '12-12-2009'
		var fhasta = fdesde

		

        function crear_calendario(fecha_desde,fecha_hasta,calendario_id){
		        $(calendario_id).DatePicker({
			        flat: true,
			        date: [fecha_desde,fecha_hasta],
			        current: fecha_desde,
			        calendars: 1,
			        format:'d/m/Y',
			        mode: 'range',
			        starts: 1,
		            onChange: function(formated) {
		                if (calendario_id.search("4")> 1){
                     		$("#visoractual").val(formated[0]+" - "+formated[1]);
                     		$('.normalSelect').val("Otro período");
 		                    $('#viendoperiodo').val("");
		                }else{
                     		$("#visorcomparador").val(formated[0]+" - "+formated[1]);
                     		$('.compararSelect').val("Otro período");
		                }

                        if ($(".compararSelect").val().search("anterior") > -1){
                            setear_comparador();        
                        }
		                
		                
		                
			        }
		        });
        }

		
		function formato_fechas(desde,hasta){
		    var res = "";
		    res = res + desde.getDate() + "/"+(desde.getMonth()+1)+"/"+desde.getFullYear();
		    res = res + " - ";
		    res = res + hasta.getDate() + "/"+(hasta.getMonth()+1)+"/"+hasta.getFullYear();
		    return res;
		}

    $('.normalSelect').change(function() {
            var hoy = new Date();
                console.log($(this).val());
                if ($(this).val().search("semana") > -1){
                    hoy.setDate(hoy.getDate() -7);
                    var w = new Date(hoy);
                    $('div #date4').html("");
                    crear_calendario(hoy,new Date(),"#date4");
                    var res =formato_fechas(hoy,new Date())
                    $('#viendoperiodo').val("semana");
                    $('#visoractual').val(res);
                    if ($(".compararSelect").val().search("anterior") > -1){
                        setear_comparador();        
                    }
                }else if ($(this).val().search("mes") > -1){
                    var w = new Date(hoy);
                    hoy.setDate(hoy.getDate() -30);
                    $('div #date4').html("");
                    crear_calendario(hoy,new Date(),"#date4");
                    var res =formato_fechas(hoy,new Date())
                    $('#viendoperiodo').val("mes");
                    $('#visoractual').val(res);                    
                    if ($(".compararSelect").val().search("anterior") > -1){
                        setear_comparador();        
                    }
                }else if ($(this).val().search("año") > -1){
                    hoy.setDate(hoy.getDate() -365);
                    var w = new Date(hoy);
                    $('div #date4').html("")
                    crear_calendario(hoy,new Date(),"#date4");

                    var res =formato_fechas(hoy,new Date())                    
                    $('#viendoperiodo').val("ano");
                    $('#visoractual').val(res);
                    if ($(".compararSelect").val().search("anterior") > -1){
                        setear_comparador();        
                    }
                }else if ($(this).val().search("Día anterior") > -1){
                    var ayer = new Date(hoy);
                    ayer.setDate(hoy.getDate() - 1);

                    $('div #date4').html("")
                    crear_calendario(ayer, ayer, "#date4");
                    $('#viendoperiodo').val("dia");
                   var res =formato_fechas(ayer, ayer)
                    $('#visoractual').val(res);
                    if ($(".compararSelect").val().search("anterior") > -1){
                        setear_comparador();        
                    }
                }else{
                    $('#viendoperiodo').val("");
                }
            });
		



function setear_comparador (){
                        
        var periodo_anterior = $("#visoractual").val();
        
        var desde = periodo_anterior.split(" - ")[0];
        var hasta = periodo_anterior.split(" - ")[1];
        var dia = desde.split("/")[0];
        var mes = desde.split("/")[1];
        var year = desde.split("/")[2];

        var anterior = new Date(year,mes-1,dia);
        var nuevo = anterior.getDate() + "/"+(anterior.getMonth()+1)+"/"+anterior.getFullYear();
        

        var hasta = periodo_anterior.split(" - ")[1];
        var dia = hasta.split("/")[0];
        var mes = hasta.split("/")[1];
        var year = hasta.split("/")[2];
        var from = new Date(year,mes-1,dia);
        var novo = from.getDate() + "/"+(from.getMonth()+1)+"/"+from.getFullYear();
        /*var diffDays = from.getDate() - anterior.getDate(); */
        
        var timeDiff = Math.abs(from.getTime() - anterior.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      
        

        
        if (diffDays == 0){
         /*nuevo calendario del dia hasta -1*/
         anterior.setDate(anterior.getDate() -1);
         $('div #date3').html("");
         crear_calendario(anterior,anterior,"#date3");
         
         $("#visorcomparador").val(formato_fechas(anterior,anterior));
         
        }else{
         /*nuevo calendario del dia hasta -1*/
        anterior.setDate(anterior.getDate() -1);
         $('div #date3').html("");
         var aux = new Date(anterior.getFullYear(),anterior.getMonth(),anterior.getDate());
         if (diffDays > 0){
         aux.setDate(aux.getDate() -diffDays);
         }else{
         aux.setDate(aux.getDate() + diffDays);
         }
         crear_calendario(aux,anterior,"#date3");
         $("#visorcomparador").val(formato_fechas(aux,anterior));
        
        }


}

    $('.compararSelect').change(function() {
            var hoy = new Date();
                if ($(this).val().search("anterior") > -1){
                    setear_comparador();        
                }
            });

		
		
		
		            /*Desbloquear comparar fecha*/
        $('._CheckCalendar').click(function() {
        
             if ($("._CheckCalendar")[0].checked){
                      $('.OverCalendar').hide() ; //css("display","none");
             }else{
                  $('#overcalendario').css("display","block");

             }
        });
		
		

        function fechas_ajax(){
         $.getJSON("/app/fechas/", {}, function(data){
         var fechas = data.split(" - ");
     		crear_calendario(fechas[0],fechas[1],"#date4");
     		crear_calendario(fechas[0],fechas[1],"#date3");
     		$("#visoractual").val(fechas[0]+" - "+fechas[1]);
     		$("#visorcomparador").val(fechas[0]+" - "+fechas[1]);
          });
        
        }

        fechas_ajax();
		

		
	};
	
	
	

	
	
	var showTab = function(e) {
		var tabIndex = $('ul.navigationTabs a')
							.removeClass('active')
							.index(this);
		$(this)
			.addClass('active')
			.blur();
		$('div.tab')
			.hide()
				.eq(tabIndex)
				.show();
	};
	
	EYE.register(initLayout, 'init');
})(jQuery)
