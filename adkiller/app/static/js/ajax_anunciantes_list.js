;(function($){
    
    $(document).ready(function(e) {

        $(".selector input").live('keyup', function(e) {
            
            $selector = $(this).parents('.selector');
           
            $input = $(this);
            var querystring = $input.val();

            var ignoreKeys = function (keyCode) {
                // tab: 9, enter: 13, up: 38, down: 40, left: 37, right: 39, esc: 27
                return [9, 13, 38, 40, 37, 39, 27].indexOf(keyCode) == -1 ? false : true;
            }

            if (!ignoreKeys(e.which) && querystring) {
                $.ajax ({
                    type: "GET",
                    url: "/app/get_json_anunciantes/",
                    data: {query: $.trim(querystring)},
                    cache: false,
                    success: function(json) {
                        if (json) {

                            // don't display selected options again
                            var list_from = $selector.find(".selector-available option").map(function() {
                                return parseInt($(this).val());
                            });
                            var list_to = $selector.find(".selector-chosen option").map(function() {
                                return parseInt($(this).val());
                            });

                            for (var anunciante in json) {
                                if ($.inArray(json[anunciante].id, list_from) == -1 && $.inArray(json[anunciante].id, list_to) == -1) {
                                    $selector.find(".selector-available select").prepend("<option value='"+json[anunciante].id+"'>"+json[anunciante].name+"</option>");
                                }
                            }
                            SelectBox.init($selector.find(".selector-available select").attr('id'));
                            SelectBox.init($selector.find(".selector-chosen select").attr('id'));
                        }
                    }
                });
            }

            if(!querystring) {
                $selector.find(".selector-available option").remove(); // clear current select options
            }
        });
    });

}(django.jQuery));