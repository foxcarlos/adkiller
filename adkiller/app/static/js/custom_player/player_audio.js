function player_audio(player_id,source, width){
	var height = width * 3/4;
	var container = document.getElementById(player_id);
	$(container).css({"width":width,"height":height,"position":"relative"});

	var array_player_images = [];
	var audio = document.createElement("AUDIO");
		audio.id='my_player_audio';
		audio.className = 'player_audio';
  		audio.controls=true;

    var previous_image;
    var interval_audio;

	$.ajax({
		type:"GET",
		dataType: "json",
		url: source,
		success: complete_get_data
	});
		
	function complete_get_data(data){
		audio.src = data.audio.url;
		init_player(data.images);
	}	

	function init_player(array_images_url){
		for(var i = 0;i< array_images_url.length;i++){
			var img = new Image();
			img.className='player-audio-images';
			img.src = array_images_url[i];
			array_player_images.push(img);
			container.appendChild(img)
		}
		container.appendChild(audio)
		play_audio()
	}

	function play_audio(){
	    audio.play();
	}

	function move_audio(){
		current_time_audio = Math.floor(audio.currentTime);
		$(array_player_images[previous_image]).css({"opacity":0});
    	$(array_player_images[current_time_audio]).css({"opacity":1});
    	previous_image = current_time_audio;
	}

	audio.addEventListener("play", play_audio);
	audio.addEventListener("seeked", move_audio);
	audio.addEventListener("timeupdate", move_audio);
}