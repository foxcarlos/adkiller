var colores = ["#069", "#FD5b5b", "#0DBA64", "#FC7317", "#B521EB", "#53E700", 
					"cyan", "yellow", "blue", "white", "orange", "lightblue"];

var nombresMeses = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];

var nombresMesesCorto = [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ];


function drawMain(graph_data_array, comparison_type, scale_unit, scale_value) {
	// Opciones generales de visualización
	var visualization_options = {
        title: '',
        width: '100%',
        height: 180,
        pointSize: 10,
        hAxis: {
            title: "",
            gridlines: { color: '#fff', },
            textStyle: { align: "right", },
        },
        vAxis: {
            title: "",
            textPosition: "in",
            gridlines: { count: 3, }
        },
        legendTextStyle: { color: '#666666' },
        colors: colores,
        lineWidth: 4,
        chartArea: {
            'left': 6,
            'width': '100%',
            'height': '80%'
        },
        legend: 'top'
    };
    
	// Preparar los datos, crear objetos Date, Datetime o TimeofDay
	if (comparison_type == 'periodos') {
		var data = google.visualization.arrayToDataTable(graph_data_array)
	} else {
		var data = new google.visualization.DataTable();	
		tipo = graph_data_array[0][0];
		header = graph_data_array[0];
		data.addColumn(tipo, tipo);

		for (var j = 1; j < header.length; j++) {
	    	data.addColumn('number', header[j]);
	    	data.addColumn({type:'string', role:'tooltip'});
		}

		for (var i = 1; i < graph_data_array.length; i++) {
			var row = graph_data_array[i];
			var newRow = new Array();
			var start = parse_date_or_datetime(row[0]);
			newRow.push(start);
			for(j=1; j<row.length; j++){
				var magnitud = row[j];
				var tipo_magnitud  = graph_data_array[0][j];
				newRow.push(magnitud);
				var tooltip = subperiodo_tooltip(start, scale_unit, scale_value) + "\n" + tipo_magnitud + ": " + magnitud;
				newRow.push(tooltip);	
			}
	    	data.addRow(newRow);
		}
	}

	// Create and draw the visualization.
	var ac = new google.visualization.LineChart(document.getElementById('main_chart'));
	
	if (comparison_type == 'periodos') {		
		ac.draw(data, visualization_options);
	} else if (comparison_type == 'opciones_filtro') {
		// elimina la leyenda
        visualization_options['chartArea']['top'] = 0
		ac.draw(data, visualization_options);
	} else if (comparison_type == 'simple'){
        // elimina la leyenda
        visualization_options['chartArea']['top'] = 0
		ac.draw(data, visualization_options);
	}

	// Agrego listeners en los puntitos del gráfico,
	// que redirijan a búsquedas filtradas por periodos más pequeños
	// siempre que no estemos en el grado más fino (horas)

	if (scale_unit != 'hora'){
		google.visualization.events.addListener(ac, 'select', function () {
			// grab a few details before redirecting
			var selection = ac.getSelection();
			var x = selection[0].row;
			var desde = data.getValue(x, 0);

			var fechas = subperiodo_request(desde, scale_unit, scale_value);
			$.get("/app/add_filter/", {filter: 'fechas', value: fechas}, function(options) {
				//recargar_todo_ajax();
				location.reload();
			});
		});
	}

}

function get_tooltips(graph_data_array, col, scale_unit, scale_value){
	// returns an array with the tooltips for the points of the graph
	var tooltips_column = new Array();
	
	var magnitudes = graph_data_array[1];
	for(i=1; i < graph_data_array.length; i++){
		var row = graph_data_array[i];

		tooltips_column[i] = subperiodo_tooltip(start, scale_unit, scale_value) + "\n" + tipo_magnitud + ": " + magnitud;
	}

	return tooltips_column;
}

function getRightEndpoint(datetime, nHours){
	var end = new Date(datetime.getTime());
	end.setTime(end.getTime() +  (((nHours*60) - 1) *60*1000));
	return end;
}

function getLastDay(date, nDays){
	var lastday = new Date();
	var nMilliseconds = (nDays - 1 )* 24 * 60 * 60 * 1000;
	lastday.setTime(date.getTime() + nMilliseconds);
	console.log(lastday);
	return lastday;
}

function subperiodo_tooltip(desde, scale_unit, scale_value) {
	var res = "";
	var day = desde.getDate();
	var hasta = "";

	switch(scale_unit){
		case 'hora':
			var nHoras = scale_value;
			hasta = getRightEndpoint(desde, nHoras);
			res = format_datetime(desde) + " - " + format_time(hasta);
			break;
		case 'dia':
			var nDias = scale_value;
			res = format_date(desde);
			if (scale_value > 1){
				hasta = getLastDay(desde, nDias);
				res = res + " - " + format_date(hasta);
			}
			break;		
		case 'semana':
			var nDias = scale_value*7;
			hasta = getLastDay(desde, nDias);
			res = format_date(desde) + " - " + format_date(hasta);
			break;
		case 'mes':
			// asumimos que la única posibilidad es 1 mes
			var mes = desde.getMonth();
			res = nombresMesesCorto[mes] + "/" + desde.getFullYear();
			break;
	}

	return res;
}


function subperiodo_request(desde, scale_unit, scale_value) {
	// solo se usa con dia, semana o mes, nunca con hora pues no admite "zoom in"
	var res = "";
	var year = desde.getFullYear();
	var month = desde.getMonth();
	var day = desde.getDate();

	res = res + day + "/" + (month + 1) + "/" + year;
	res = res + " - ";

	var hasta = "";
	if(scale_unit == 'mes'){
		// asumimos que la única posibilidad es 1 mes
		hasta += getDaysInMonth(month, year) + "/" + (month + 1) + "/" + year;
	} else {
		// seria semana o dias
		var nDias = scale_value;
		if (scale_unit == 'semana') {
			nDias = nDias*7;
		}
		hasta = getLastDay(desde, nDias);
	}

	res = res + format_date(hasta);

	return res;
}

function getDaysInMonth(m, y) {
   return /8|3|5|10/.test(--m)?30:m==1?(!(y%4)&&y%100)||!(y%400)?29:28:31;
}


function formato_fechas(desde, hasta) {
	var res = "";
	res = res + desde.getDate() + "/" + (desde.getMonth() + 1) + "/" + desde.getFullYear();
	res = res + " - ";
	res = res + hasta.getDate() + "/" + (hasta.getMonth() + 1) + "/" + hasta.getFullYear();
	return res;
}

function format_date(date){
	return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
}

function format2digits(n){
	return ("0" + n).slice(-2);
}

function format_time(datetime){
	return datetime.getHours() + ":" + format2digits(datetime.getMinutes());
}

function format_datetime(datetime){
	return format_date(datetime) + " " + format_time(datetime);	
}

function parse_date_or_datetime(date_string){
	var has_time = false;
	if (date_string.indexOf(":") > -1) {
		has_time = true;
	}

	if (!has_time) {
		var dateParts = date_string.split("/");
		var xType = 'date';
		return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);							
	} else {
		xType = 'datetime' 			
		var parts = date_string.split(' ');
		dateParts = parts[0].split('/');
		timeParts = parts[1].split(':');
		return new Date(dateParts[2], dateParts[1] - 1, dateParts[0], timeParts[0], timeParts[1], 0);
	}

}

function drawPie(data_ori, div) {
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Item');
	data.addColumn('number', 'Valor');
	total = 0;
	total_porc = 0;
	$.each(data_ori, function (i, obj) {
		total_porc = total_porc + parseFloat(obj.porcentaje);
		total = total + parseFloat(obj.valor);
		if (obj) {
			data.addRows([   
					[obj.item, obj.valor]
				]);
		};
	});
	if (total_porc < 100) {
		data.addRows([   
				['Otros', total / total_porc * (100 - total_porc)]
			]);
	}
	// Create and draw the visualization.
	var ac = new google.visualization.PieChart(document.getElementById(div));
	ac.draw(data, {
			title: '',
			fontSize: '8px',
			width: '100%',
			height: '100%',
			legend: {
				position: 'left'
			},
			chartArea: {
				left: 2,
				top: 2,
				width: "60%",
				height: "100%"
			}
		});
}
