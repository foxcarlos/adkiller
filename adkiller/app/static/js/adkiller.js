/*
define(function(require, exports, module) {
    require('/static/js/bootstrap.min.js');
    require('/static/js/jquery-ui-1.9.1.custom.min.js');
    require('/static/js/jquery.fancybox.pack.js');
*/
$(document).ready(function(){
require(
    ['waypoints', '/static/js/bootstrap.min.js', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js','/static/js/jquery.fancybox.pack.js', '/static/js/blockui.js', '/static/js/fileDownload.js'],
    function(){

    function print_time() {
        var currentdate = new Date();
        var datetime = currentdate.getHours() + ":"  + currentdate.getMinutes() + ":" + currentdate.getSeconds();
    }


    $(function(){

        /*var loaderTable = $('.OverLoad');*/
        var loaderTable = $('.Loader');
        var loaderFiltros = $('#sidebar .indicator');
        var loaderProducto = $('#producto .indicator');
        /* --- Primera Carga --- */

        /* Siempre */
        autocompletar();
         filtros_top();
        filtros_elegidos_ajax();

        setear_graficos_ajax();

        set_cantidades_ajax();

        filtros_ajax(true);


        ciudades_ajax();
        fechas_ajax();
        cargar_tabla_ajax();


        $('.fancybox-image').live('click', function(e) { window.open($(this).attr('src')) });

        busquedas_ajax();

        otras_marcas_value_ajax();

        function get_filters_and_save(){
            $.getJSON("/app/get_filters/", {top: 'False', format: "json"},function(data){
                    get_filters_saved = data;
                }
            );
        };

        get_filters_and_save();



        /* busquedas_ajax(); */
        //recarga todos los filtros
        function recargar_todo_ajax(){
            get_filters_and_save();

            borrar_grafico_ajax();
            setear_graficos_ajax();

            filtros_elegidos_ajax();
            busquedas_ajax();
            otras_marcas_value_ajax();
            fechas_ajax();
            vaciar_tabla_ajax();
            cargar_tabla_ajax();
            vaciar_eleccion_ajax();
            vaciar_unidad_ajax();
            borrar_videos_ajax()
            set_cantidades_ajax();
            $('#time-range').find(".active").removeClass("active");
            filtros_top();
            vaciar_filtros_ajax();
            filtros_ajax(false);

            loaderTable.fadeOut();
            vaciar_busquedas();


        }
        //esto es para la ventana modal de las busquedas
        $("#savesearch").draggable({
            handle: ".modal-header",
            containment: 'body',
        });

        $('.modal-header').css('cursor', 'pointer');
        //esto trae las fechas para llenar el selector del navbar superior
        function fechas_ajax(){
            $.getJSON("/app/fechas/", {}, function(data){
                $("#fechas_encabezado").text(data)
            });
        }
        //vacia las busquedas
        function vaciar_busquedas(){
            /* $.blockUI(); */
            $('.busquedas').find("li").remove();
            /* $.unblockUI; */
        }
        //vacia los filtros
        function vaciar_filtros_ajax(){
            /* $.blockUI(); */
            $('.filters-block').find("ul").remove();
            $('#right-filter-options .filters-block').find('ul').remove();
            /* $.unblockUI;*/
        }
        //vacia kla tabla de contenidos
        function vaciar_tabla_ajax(){
           $('table.grilla').find("tr:gt(0)").remove();
        }
        //borra los filtros seleccionados
        function vaciar_eleccion_ajax(){
            $('.applied-filters').find("span").remove();
            $('.applied-filters').find("a").remove();
        }
        //saca los videos
        function borrar_videos_ajax(){
            $('#producto .producto-container').find("div").remove();
            $('#producto-control-row').remove();
        }
        //saca el gracfico
        function borrar_grafico_ajax(){
            /* $('#main_chart').remove();*/
        }
        //setea el grafico de lineas
        function setear_graficos_ajax(){
            if (window.google !== undefined ) {
                $.getJSON("/app/graph", function(data){
                    drawMain(data[0], data[1], data[2], data[3]);
                }, cache=false);
            }
        }
        //setea los graficos de tortas
        var setear_graficos_pie_ajax = function () {
            if (window.google !== undefined ) {
                $.getJSON("/app/options/?data=soporte&limit=3",function(data){
                    drawPie(data[0], 'soporte_chart');
                });

                $.getJSON("/app/options/?data=medio&limit=3",function(data){
                    drawPie(data[0], 'medio_chart');
                });

                $.getJSON("/app/options/?data=sector&limit=3",function(data){
                    drawPie(data[0], 'sector_chart');
                });
            }
        }

        function urlExists(url, callback){
            callback(true);
            $.ajax({
                async: true,
                type: 'GET',
                url: url,
                crossDomain: true,
                success: function(){
                    callback(true)
                },
                error: function (){
                    callback(false)
                }
            });
        }
        //busca la miniatura del video de una tanda
        function Buscar_thumb_tanda(id, image, esGrafica){
            urlExists(image, function(exists){
                if (exists){
                    $("#tanda" + id).attr("src", image);
                    if(esGrafica === true) {
                        $("#tanda" + id).parents('a').addClass('video-thumb overlay fancybox').removeAttr('onClick').attr("href", image);
                    }
                }
            });
        }
        //Busca la miniatura del video de un spot
        function Buscar_thumb_spots(id, image){
            $.get(image, {}, function(data) {
                urlExists(data, function(exists){
                    if (exists){
                        $("#spots" + id).attr("style", "background-image:url('"+data+"');");
                        /*$("#spots" + id).attr("src", data);*/
                    }
                });
            });
        }

        /* --- Tabla Dinámica --- */
        function get_tabla(url, vaciar) {

            //loaderTable.fadeIn();

            $.getJSON(url, {}, function(data) {

                var $table = $('table.grilla tbody');
                var arr = ["origen", "marca", "producto", "duracion", "medio", "programa", "fecha", "hora", "valor"];

                next_url = data.meta.next;

                if (vaciar) {
                    $table.empty();
                }
                //rellena la tabla de emisiones
                $.each(data.objects, function(index, obj) {

                    var items = "";

                       // por cada una de las keys definidas arriba
                       $.each(arr, function(index, key) {
                            if (key == "origen") {
                                var val = thumb_y_video(obj['id'], obj['thumb'], obj['video'], obj['filetype'],
                                                        70, true, obj['soporte']);
                                var esGrafica = (obj['soporte'] === 'Diario' || obj['soporte'] === 'Revista');
                                Buscar_thumb_tanda(obj['id'], obj['thumb'], esGrafica);

                            } else {
                                var val = obj[key];
                            }


                            if (key == "marca" || key == "medio" || key == "fecha"){
                                items = items + '<td>' + val ;
                            }else{
                                if (key == "producto" || key== "programa" || key == "hora"){
                                    items = items + "<br><span>";
                                    if (val){
                                        if (key == "producto" && IS_STAFF) {
                                            if (obj['soporte'] == "Diario" || obj['soporte'] == "Revista") {
                                                val = "<a target='_blank' href='/admin/app/grafica/" + obj['id'] + "/'>" + val + "</a>";
                                            } else {
                                                val = "<a target='_blank' href='/admin/app/audiovisual/" + obj['id'] + "/'>" + val + "</a>";
                                            }
                                        };
                                        items = items + val ;
                                    }
                                    items = items + '</span></td>';
                                }else{
                                    if (key == "valor"){
                                        items = items + '<td><b> $ ' + val + '</b></td>';
                                    }else{
                                        items = items + '<td>' + val + '</td>';
                                    }

                                }
                            }

                        });

                    // console.log(items);
                    $table.append('<tr>'+items+'</tr>');
                });

                //loaderTable.fadeOut();

            }).promise().done( [suscribeWaypoints, setear_graficos_pie_ajax] );
        };

        function cargar_tabla_ajax(){
           $.get("/app/url_list_emisiones/", {}, function(url) {
                get_tabla(url, true);
            });

        };



        /* --- Populating Select Secundario --- */
        $('.filters .select_filter').change(function(){
            $('.filters .select_subfilter').empty().append('<option value="">Seleccione Filtro</option>');
            var selected_item = $(this).val();
             $.getJSON("/app/options/", {data: selected_item, order: 'nombre', limit:20}, function(options) {
                if (options) {
                    $.each(options, function(index, items) {
                        $('.filters .select_subfilter').append('<option value="' + items.id + '">' + items.item + '</option>');
                    });
                };
            });
        });

        /* --- Disparando búsqueda de filtro --- */
        $('.filters .select_subfilter').change(function(){
            var filter = $('.filters .select_filter').val();
            var value = $(this).val();

            if (filter && value) {
                $.get("/app/add_filter/", {filter: filter, value: value}, function(options) {
                    $("#reset").fadeIn();
                    recargar_todo_ajax();
                });
            }
        });


        /* --- Filtro Ciudad --- */
        function ciudades_ajax(){
             $.getJSON("/app/get_ciudades/", function(options) {
                if (options.length) {
                    $('#ciudad-dropdown2 .dropdown-menu').append('<li><a class="change-filter" href="/#" data-filtro="ciudad" data-value="all" >Todas</a></li>');
                    $.each(options, function(index, option){

                        var ciudadOptions =  '<a class="change-filter" href="#" data-filtro="ciudad" data-value="' + option['id'] + '">' + option['item'] + '</a>';
                        $('#ciudad-dropdown2 .dropdown-menu').append('<li>'+ciudadOptions+'</li>');

                    });
                } else {
                    $('#ciudad-dropdown2 .dropdown-menu').append('<li>No hay opciones para este filtro</li>');
                }

            });
        }


        /* --- Filtros elegidos TOP --- */
        function filtros_top(){
            $.getJSON("/app/get_filters/", {top: 'True', format: "json"}, function(data) {
                if(data) {
                    if(data['periodo']) $('#time-range a.'+data['periodo']).addClass('active');
                    $('#ciudad-dropdown2 .dropdown-toggle').text("Todas");

                    if(data['ciudad']) {
                        if (data['ciudad'].length > 1){
                            $('#ciudad-dropdown2 .dropdown-toggle').text("Varias");
                        }
                        else{
                            $('#ciudad-dropdown2 .dropdown-toggle').text(data['ciudad'][0][0]);
                        }
                    }
                    $('#ciudad-dropdown2 .dropdown-toggle').prepend('<span class="icon world">').append('<span class="icon down">');
                }
            });
        }

        function has_elems(data) {
            var length = 0;
            for(var prop in data){
                if(data.hasOwnProperty(prop))
                    length++;
            }
            return length > 0;
        }

        $('#notice').change(function(){
            muestraOpcionesAlerta();
        });

        function filtros_elegidos_ajax(){

            /* --- Filtros elegidos (tags) --- */
            $.getJSON("/app/get_filters/", {top: 'False', format: "json"}, function(data) {
                $('span.labelAlertas').html("Alerta de Comportamiento");
                if (has_elems(data)) {
                    $.each( data, function(filter, item) {
                        if (item.length > 1){
                            for (var x in item){

                                $('.applied-filters').append('<a href="/app/remove_filter/?filter='+filter+'&id='+item[x][1]+'" class="filter-tag remove-filter" style="background:'+colores[x]+';"><i class="icon-remove"></i>'+item[x][0]+'</a>');
                            }

                            if (item[0][2]){
                                $('.applied-filters').append('<a href="/app/compare_filter/?filter='+filter+'" title="Comparar ' + filter + 's" class="actions">vs</a>');


                            } else {
                                $('.applied-filters').append('<a href="/app/compare_filter/?filter='+filter+'" title="Juntar ' + filter + 's" class="actions graph-active">vs</a>');
                                $('span.labelAlertas').html("Alerta de Comparación");
                            }

                        } else {
                            $('.applied-filters').append('<a href="/app/remove_filter/?filter='+filter+'&id='+item[0][1]+'" class="filter-tag remove-filter"><i class="icon-remove"></i>'+item[0][0]+'</a>');
                        }
                    });
                    $('#subnav').show();
                }

            });
        }



        // esto despliega dentro del modal las opciones de comparación o de comportamiento
        function muestraOpcionesAlerta(){
            if(!$('#notice').is(':checked')) {
               $('#notice-data').hide();
               $('#comportamiento').hide();
               $('#comparacion').hide();
               return;
            }

            data = get_filters_saved;
            if(!has_elems(data)){
               $('#notice-data').show();
               $('#comportamiento').show();
               $('#comparacion').hide();
            } else {
                llena_select(data,'nombre0','nombre1');
            }

        }
        // con este llena los selects
        function llena_select(data,cont1,cont2){
            $.each(data, function(filter, item){
                    if(item[0][2]){
                            $('#notice-data').show();
                            $('#comportamiento').show();
                            $('#comparacion').hide();
                    }else{
                            $('#notice-data').show();
                            $('#comportamiento').hide();
                            $('#comparacion').show();


                            var op0;
                            for(var x in item){
                                op0 += ' <option value="' + item[x][1] + '">'+ item[x][0] +'</option>';
                            }

                            var array = item.reverse();
                            var op1;
                            for(var j in array){
                                op1 += ' <option value="' + array[j][1] + '">'+ array[j][0] +'</option>';
                            }

                            $('#'+cont1).html('');
                            $('#'+cont2).html('');
                            $('#'+cont1).append(op0);
                            $('#'+cont2).append(op1);
                            return false;
                    }
            });
        }

        //cambia el contenido del select si al cambiar un select se selecciona la misma opción que en el otro
        $('#nombre0').on('change', function(){
            data = get_filters_saved;
            llena_select(data,'nombre1','nombre0');

        });
        $('#nombre1').on('change', function(){
            data = get_filters_saved;
            llena_select(data,'nombre1','nombre0');

        });





        function buscarAlertas(){
            $.getJSON("/filtros/get_alerta/", {perfil: id}, function(data) {
                if (has_elems(data)) {
                    $('#notice').attr('checked', true);
                    $('#notice-data').show();
                    // Setear la alerta
                    id = data[0][0];
                    comparador = data[0][1];
                    valor = data[0][2];
                    tipo = data[0][3];
                    $('#comparador option[value=' + comparador + ']').attr('selected','selected');
                    $('#savesearch input[name=variable_valor]').val(valor);
                    $('#variable option[value='+ data[0][4] +']').attr('selected','selected');
                    if (data[0][5] == 'S') {
                        periodo = 'semana'
                    } else if (data[0][5] = 'M') {
                        periodo = 'mes'
                    } else {
                        periodo = 'dia'
                    }
                    $('#periodo option[value='+ periodo +']').attr('selected','selected');

                }


            });
        }


         //function busquedas_ajax() {

            /* --- Filtros elegidos (tags) --- */
            /*$.getJSON("/filtros/get_busquedas/", {}, function(data) {
                if (has_elems(data)) {
                    for (x in data){

                        // Set active busqueda
                        var markup = (data[x][2]) ? '<li class="active">' : '<li>';
                        // Busqueda
                        markup += '<a href="/filtros/cargar_busqueda/'+data[x][0]+'/" class="busqueda">'+ data[x][1] + '</a>';

                        // Accioness
                        markup += '<a class="actions editar-busqueda" id="'+data[x][0]+'" href="#savesearch" data-toggle="modal" title="Editar Búsqueda"><i class="icon-pencil"></i></a>';
                        markup += '<a class="actions borrar-busqueda" id="'+data[x][0]+'" href="#" title="Eliminar Búsqueda"><i class="icon-remove"></i></a>';

                        markup += '</li>';
                        $('.busquedas').append(markup);
                    }
                    // Moviendo arriba a la busqueda activa
                    if ($('.busquedas li.active').length == 0) {
                        $('.busquedas').prepend('<li class="no-active">Seleccionar búsqueda guardada</li>');
                    } else {
                        $('.busquedas li.active').prependTo('.busquedas');
                        $('.busquedas li.active').attr('title','Búsquedas guardadas');
                    }
                    $('.busquedas li:first').addClass('first');

                    // Ocultar flecha toggle si hay una sola busqueda
                    if ($('.busquedas li').size() == 1) {
                        $('.busquedas li.first').addClass('unique');
                    }
                }
            });
        }*/











        function busquedas_ajax() {

            /* --- Filtros elegidos (tags) --- */
            $.getJSON("/filtros/get_busquedas/", {}, function(data) {
                if (has_elems(data)) {
                    for (x in data){

                        // Set active busqueda
                        var markup = (data[x][2]) ? '<li class="active contAlerts contBell">' : '<li class="contAlerts contBell">';
                        // Busqueda

                        if(data[x][1].length > 20){
                            markup += '<a href="/filtros/cargar_busqueda/'+data[x][0]+'/" title="'+data[x][1]+'" class="busquedaIndividual largo">'+ data[x][1] + '</a>';

                        }else{
                            markup += '<a href="/filtros/cargar_busqueda/'+data[x][0]+'/" class="busquedaIndividual">'+ data[x][1] + '</a>';
                        }
                        //markup += '<a href="/filtros/cargar_busqueda/'+data[x][0]+'/" title="'+data[x][1]+'" class="busqueda contTooltip">'+ data[x][1] + '</a>';
                        //pregunta si el valor de busqueda activa es mayor a 0......
                        if(data[x][3] > 0){
                            //... si es asi pone un icono de campana verde.....
                            markup += '<img class="campana" src="/static/img/bell.png"/>';
                        }
                        else{
                            //... si no es asi pone un icono de campana gris claro
                            markup += '<img class="campana-gray" src="/static/img/bell-gray.png"/>';
                        }

                        // Accioness
                        markup += '<div class="contieneAcciones"><a class="actions editar-busqueda alertIcons" id="'+data[x][0]+'" href="#savesearch" data-toggle="modal" ><i class="icon-pencil"></i></a><a class="actions borrar-busqueda alertIcons" id="'+data[x][0]+'" href="#" ><i class="icon-remove"></i></a></div>';


                        markup += '</li>';
                        $('.busquedas').append(markup);
                    }
                    // Moviendo arriba a la busqueda activa
                    if ($('.busquedas li.active').length == 0) {

                    } else {
                        $('.busquedas li.active').prependTo('.busquedas');
                    }
                    $('.busquedas li:first').addClass('first');

                    // Ocultar flecha toggle si hay una sola busqueda
                    if ($('.busquedas li').size() == 1) {
                        $('.busquedas li.first').addClass('unique');
                    }
                }

                $('.largo').limitChar(20,true);
                $('.first').children('a.busquedaIndividual.largo').limitChar(25,true);

                 $('#busquedasAlertas').find('li').hover(function(){
                    var dato = $(this).children('div.contieneAcciones');
                    dato.show().fadeIn('slow');
                 },function(){
                    var dato = $(this).children('div.contieneAcciones');
                    dato.hide().fadeOut('slow');
                 });

                //guarda en variable la cantidad de busquedas que hay generadas
                var cantidad = $('.busquedas li.active').length;
                var cantidad2 = $('.busquedas li').size();
                //pregunta si son mayor o igual que 5 busquedas,....
                if(cantidad > 5 || cantidad2 > 5 ){
                        //...mustra el boton mas y
                        $('#btnMas').css({'display':'block'});
                        //en el click de este boton....
                        $('#btnMas').on('click', function(){
                            //musetra todas las alertas generadas,.....
                            $('#busquedasAlertas').css({'overflow':'inherit'});
                            //oculta el btnMas....
                            $('#btnMas').css({'display':'none'});
                            //y muetra el btnMenos
                            $('#btnMenos').css({'display':'block'});
                            //en el click de este boton....
                            $('#btnMenos').on('click',function(){
                                //....oculta el exedente de busquedas hasta mostrar solo 5
                                $('#busquedasAlertas').css({'overflow':'hidden'});
                                //oculta este mismo boton
                                $('#btnMenos').css({'display':'none'});
                                //muestro el btnMas
                                $('#btnMas').css({'display':'block'});
                            });
                        });
                // si la cantidad es menor o igual a 5 oculta los dos botones
                }else if(cantidad <= 5 || cantidad2 <=5){
                    $('#btnMas').css({'display':'none'});
                    $('#btnMenos').css({'display':'none'});
                }
            });
        }



        /* Cargar Busqueda */
        $('a.busqueda').live('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            loaderTable.fadeIn();
            $.ajax({
                url: $(this).attr("href"),
                success: function(data){
                    recargar_todo_ajax();
                },
            });
        });

        /*$('.busquedas li:not(.first)').live('click', function(){
            $(this).find('.busqueda').trigger('click');
            $(this).siblings().slideToggle();
        });

        /* Desplegar otras busquedas */
        /*$('.busquedas li:first').live('click', function() {
            $(this).siblings().slideToggle();
            $(this).toggleClass('desplegado');
        });

        /* Eliminar Busqueda */
        $('.borrar-busqueda').live('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            if (!confirm('¿Quiere eliminar esta búsqueda?')) return;
            $.get('/filtros/eliminar_busqueda/', {id: $(this).attr('id')}, function(data){
                recargar_todo_ajax();
            });
        });

        /* Editar Busqueda*/
        $('.editar-busqueda').live('click', function(e){
            e.preventDefault();
            e.stopPropagation();
            // Cargamos la busqueda
            id = $(this).attr('id');
            loaderTable.fadeIn();
            $.ajax({
                url: '/filtros/cargar_busqueda/' + id + '/',
                success: function(data){
                    recargar_todo_ajax();
                },
            });
            // Titulo del modal y explicacion
            $('#savesearch h3').html('Editar Búsqueda');
            //  $('#savesearch .modal-header p').html('Texto explicativo');

            var alerta = false;
            // Busco los datos de la busqueda
            $.getJSON("/filtros/get_busqueda/", {id: id}, function(data) {
                if (has_elems(data)) {
                    // Nombre de la busqueda
                    $('#savesearch #namesearch').val(data[0][1]);
                    // Alerta de producto
                    $('#notice-producto').attr('checked', data[0][3]);
                }
            });



        });
        //con esto filtro los filtros para que los que esten en el rightsidebar no tengan la parte de estadistica
        function tiene_estadistica(nombreFiltro) {
            return nombreFiltro != 'etiquetaprograma' && nombreFiltro != 'etiquetamedio' && nombreFiltro != "etiquetaproducto";
        }
        //llena un filtro simple
        function llenarFiltro(filtro, agrupar, desagrupar){
            // FIXME: que tiene_estadistica sea un atributo que venga del server
            //var contenedor = $('#filter-options');
            var nombreFiltro = filtro['name'];
            if(tiene_estadistica(nombreFiltro)){
                //agrega al contenedor un div con el nombre del filtro
                $('#filter-options').append('<div id="'+ nombreFiltro +'" class="filters-block"></div>');
                //pregunta por medios para agregar el icono de edición de descuentos
                if (nombreFiltro == 'medio') {
                    descuentoButton = '<a href="#" class="save-descuento">Guardar</a><a title="Editar Descuentos" href="#" class="descuento-button"><i class="icon-pencil"></i></a>';
                } else {
                    //sino deja el espacio de ese boton en blanco
                    descuentoButton = '';
                }
                // agregar el titulo del filtro
                var titulo = '<h4 class="filter-title collapse"><a data-filtro="'+ nombreFiltro +'" href="#" class="collapse-btn">'+filtro['plural']+'</a><span class="loader"></span><i class="fa fa-chevron-down"></i>';
                titulo = titulo + descuentoButton + '</h4>';
                if (agrupar)
                    //pone el icono del mas
                    titulo = titulo + '<img id="btn'+nombreFiltro+'" src="/static/img/agrupar.png" class="iconMas" title="Agrupar" />';
                if (desagrupar)
                    //pone el icono del menos
                    titulo = titulo + '<img id="btn'+nombreFiltro+'" src="/static/img/desagrupar.png" class="iconMas" title="Desagrupar" ></i>';
                $('#'+nombreFiltro).append(titulo);


            }else{
                //agrega al contenedor un div con el nombre del filtro
                $('#right-filter-options').append('<div id="'+ nombreFiltro +'" class="filters-block contEtiquetas"></div>');
                //corta el nombre "Etiqueta de xxxxx" y se queda solo con el nombre "xxxxx" correspondiente
                var alt = filtro['plural'].split(' ');
                //agrega todo al contenedor colapsable
                $('#'+nombreFiltro).append('<h4 class="filter-title collapse tituloEtiqueta"><a data-filtro="'+ nombreFiltro +'" href="#" title="'+nombreFiltro+'" class="collapse-btn btnEtiqueta">'+alt[2]+'</a><span class="loader loaderEtiqueta"></span></h4>');
            }

        }
        //para mostrar filtros de acuerdo al perfil del usuario
        function getNivelFiltro(filtro1, filtro2){
            // ve en que nivel esta seteada la base de datos para mostrar
            $.getJSON("/filtros/get_nivel_filtro", {'filtro': filtro1},function(data){
                //lo guarda por mas que lo traiga del mismo lugar
                setNivelFiltro(filtro1, filtro2, parseInt(data));
            });
        }
        //guarda en la base de datos la ultima seleción de filtro
        function setNivelFiltro(filtro1, filtro2, nivel){
            //carga los botones de acuerdo al nivel que esta guardado en la base de datos
            //muestra u oculta de acuerdo el parametro "nivel"
            if (nivel == 1) {
                $('#' + filtro1).css({'display':'block'});
                $('#' + filtro2).css({'display':'none'});
            } else {
                $('#' + filtro2).css({'display':'block'});
                $('#' + filtro1).css({'display':'none'});
            };
            //guarda en la base el ultimo nivel seleccionado
            $.getJSON("/filtros/set_nivel_filtro", {'filtro': filtro1, 'nivel': nivel}, function(data){
            });
        }
        /* --- Todos los filtros (sidebar) --- get_ordered_filters */
        /* crear_encabezados indica si tiene que crear o no los encabezados */
        function filtros_ajax(crear_encabezados) {
            $.getJSON("/app/get_ordered_filters/", {format: "json"}, function(data) {
                var lista = $('#filter-options');
                //llena filtros dobles
                function llenarFiltroDoble (filtro1, filtro2){

                    //carga el filtro guardado en la base de datos
                    getNivelFiltro(filtro1['name'], filtro2['name']);
                    // llenar los dos filtros
                    llenarFiltro(filtro1, true, false);
                    llenarFiltro(filtro2, false, true);

                    var btn1 = "btn" + filtro1['name'];
                    var btn2 = "btn" + filtro2['name'];
                   //botones para alternar entre la vista marcas y anunciantes
                    // eventos de los botones
                    // es el boton que dice agrupar
                    $('#' + btn1).click(function(e){
                        e.preventDefault();
                        muestraAmbasOpciones(filtro1['name'],filtro2['name']);
                        //guarda en la base de datos el nivel del filtro en 1
                        setNivelFiltro(filtro1['name'], filtro2['name'], 2);

                    });
                    // es el boton que dice desagrupar
                    $('#' + btn2).click(function(e){
                        e.preventDefault();
                        muestraAmbasOpciones(filtro2['name'],filtro1['name']);
                        //guarda en la base de datos el nivel del filtro en 2
                        setNivelFiltro(filtro1['name'], filtro2['name'], 1);
                    });
                }
                //recorre toda la informacion de los filtros
                $.each( data, function(index, item) {
                    /* crear_encabezados indica si tiene que crear o no los encabezados */
                    if (crear_encabezados) {
                        //pregunta que filtro es, si es marca o medio carga el filtro doble
                        if(item['name'] == 'marca'){
                            var item2 = {};
                            item2['name'] = "anunciante";
                            item2['plural'] = "anunciantes";
                            llenarFiltroDoble(item, item2);
                        } else if(item['name'] == 'medio' ){
                            var item2 = {};
                            item2['name'] = "grupomedio";
                            item2['plural'] = "grupos de medios";
                            llenarFiltroDoble(item, item2);
                        } else
                        //si no es ninguno de los dos  carga el filtro individual
                            llenarFiltro(item, false, false);
                    } else {
                        //filterBlock = $('#'+item['name']);
                    }

                    //con esto permite cargar las opciones de anunciantes una vez que se eligio una opción
                    if(item['name'] == 'marca'){
                                $('#anunciante').append('<ul style="display:'+ item['opened']+';"></ul>');

                    }else if(item['name'] == 'medio'){
                                $('#grupomedio').append('<ul style="display:'+ item['opened']+';"></ul>');
                    }

                    if(!tiene_estadistica(item['name']))
                        $('#' + item['name']).append('<ul style="display:'+ item['opened']+';"><div class="leftColumn"></div><div class="rightColumn"></div></ul>');
                    else
                  //para el resto de los filtros
                       $('#'+item['name']).append('<ul style="display: '+ item['opened']+';"></ul>');

                });


                // con esto expande el contenedor de las opciones siempre y cuando tenga opciones para mostrar
               $.each( data, function(index, item) {
                    if (item['opened'] == 'block') {
                        call_filtro_content(item['name'], false);
                    }
                });

               if (crear_encabezados) {
                    //carga otras etiquetas
                    $('#right-filter-options').append('<div id="otras" class="filters-block contEtiquetas"><h4 class="filter-title collapse tituloEtiqueta"><a data-filtro="otras" href="#" class="collapse-btn btnEtiqueta">Otras</a><span class="loader loaderEtiqueta"></span></h4></div>');
                }
                $('#otras').append('<ul style="display:block;"></ul>');



            });
        }



        function muestraAmbasOpciones(dato1, dato2){
            if($('#filter-options #'+dato1+' ul').is(':visible') ){
                $('#filter-options #'+dato2+' ul').css({'display':'block'});
            }else {
                $('#filter-options #'+dato2+' ul').css({'display':'none'});
            }
        }




        function otras_marcas_value_ajax() {
            $.getJSON("/app/otras_marcas_value/", {}, function(data) {
                if (data == true) {
                    $("#inputSecundarias").attr('checked','checked');
                } else {
                    $('#inputSecundarias').removeAttr('checked');
                }
                hideShowOtrasMarcas();
            });
        }

        $('#inputSecundarias').live('click',function(e){
            onClickOtrasMarcas();
        });

        $('#btnSecundarias').on('click',function(){
            if($('#inputSecundarias').is(':checked')){
                $("#inputSecundarias").attr('checked',false);

            }else{
                $('#inputSecundarias').attr('checked',true);
            }
            onClickOtrasMarcas();
        });
        $('#marcadorOtrasMarcas').on('click',function(){
            if($('#inputSecundarias').is(':checked')){
                $("#inputSecundarias").attr('checked',false);

            }else{
                $('#inputSecundarias').attr('checked',true);
            }
            onClickOtrasMarcas();
        });

        function onClickOtrasMarcas() {
            if($("#inputSecundarias").is(':checked')) {
                value = true;
            } else {
                value = false;
            }

            $.ajax({
                url: '/app/set_otras_marcas/',
                type: 'GET',
                data: {value: value},
                success: function(data){
                    recargar_todo_ajax();
                },
            });

        }

        //muestro u oculto otras marcas en funcion del check box
        function hideShowOtrasMarcas(){
            if($('#inputSecundarias').is(':checked')){
                $('#otramarca').css({'display':'block'});
                $('#marcadorOtrasMarcas').css({'display':'block'});
            }else{
                $('#otramarca').css({'display':'none'});
                $('#marcadorOtrasMarcas').css({'display':'none'});
            }
        }


        function append_options_to_filter(filter, options) {
            if(tiene_estadistica(filter)){
                // Agregando items al filtro
                $('#' + filter + ' ul').append(options);
            } else {
                // agrego en dos columnas
                op = options;
                var contenedor="#etiquetaprograma";  //"#" + filter

                var paridad_previa = 0;
                for (var i=0; i < op.length; i++) {

                    paridad_previa=$(contenedor + " .leftColumn").children().length - $(contenedor + " .rightColumn").children().length;
                    // paridad_previa puede ser 1 o 0. Si paridad_previa es 1, hay uno mas a la izquierda, tiene que empezar por la derecha.
                    if ((paridad_previa) % 2 == 0){
                        $(contenedor + " .leftColumn").append(op[i]);
                    }else{
                        $(contenedor + " .rightColumn").append(op[i]);
                    }
                }
                // borro estadistica
                $(contenedor + " .estadistica").hide();
            }
        }



        /* --- Agregando un filtro a la sesion --- */
        var buscando = {};
        //carga el contenido de opciones debajo de cada filtro
        function call_filtro_content(item, asociado) {
            if ( buscando[item] !== undefined && buscando[item]) return;

            buscando[item] = true;

            if (!asociado) {
                if(item == 'marca'){
                    call_filtro_content("anunciante", true);
                } else if(item == 'medio'){
                    call_filtro_content("grupomedio", true);
                } if(item == 'anunciante'){
                    call_filtro_content("marca", true);
                } else if(item == 'grupomedio'){
                    call_filtro_content("medio", true);
                }
            }

            if (tiene_estadistica(item))
                var filtroContent = $('#'+item + " ul");
            else
                var filtroContent = $('#'+item + " ul .leftColumn");
            if (!filtroContent.is(':empty')) return;


            // Bloque del filtro para seleccionar elementos hijos
            var filterBlock = $('#'+item);


            // Loader de carga
            $('span.loader', filterBlock).show();

            $.getJSON("/app/options/", {data: item}, function(result) {
                options = result[0];
                has_more = result[1];
                has_less = result[2];


                if (options.length) {
                    append_options_to_filter(item, opciones(item,options));

                    /* --- Slider para descuentos --- */
                    if (filterBlock.attr('id') == 'medio') {
                        call_sliders();
                    }

                    // Botones + o - del filtro
                    var controlRow =  '<li class="control-row" >';

                    //para mostrar mas opciones
                    if (has_more){
                        controlRow += '<a class="options-more" data-filtro="'+item+'" href="/app/options/more" title="Más opciones"><i class="fa fa-plus-circle"></i></a>';
                    }

                    //para mostrar menos opciones
                    if (has_less){
                        controlRow += '<a id="x'+item+'" class="options-less" data-filtro="'+item+'" href="/app/options/less" title="Menos opciones"><i class="fa fa-minus-circle"></i></a>';
                    }else{
                        controlRow += '<a id="x'+item+'" class="options-less" data-filtro="'+item+'" href="/app/options/less" style="display:none;" title="Menos opciones"><i class="fa fa-minus-circle"></i></a>';
                    }
                    if(options.length <= 5){
                        $('.options-less').hide();
                    }


                    controlRow += '<a class="options-export" data-filtro="'+item+'" href="/app/options_csv/?data=' + item + '" title="Exportar a Excel"><i class="fa fa-download"></i></a>';


                    controlRow += '</li>';

                    if(has_more){
                        $('ul', filterBlock).append(controlRow);
                    }else{
                        $('ul', filterBlock);
                    }


                } else {
                    if (item == "agencia"){
                        filterBlock.hide();
                    }
                }


            }).promise().done(function(){ // Esta función se dipara cuando terminó de recibir todos los items del filtro
                // Hide el loader
                $('span.loader', filterBlock).hide();

                // Expando la lista de filtros
                $('ul', filterBlock).slideDown();

                // Vuelvo a habilitar el boton para hacer collapse del filtro
                $('h4.filter-title', filterBlock).addClass('collapse');

                buscando[item] = false;
            });
        }

        /* --- Sliders ajax --- */
        function call_sliders() {
            $('span.slider').slider({
                range: "max",
                min: 0,
                max: 100,
                step: 1,
                create: function () {
                    $(this).slider( "option", "value", $(this).prev().val() );
                },
                slide: function (event, ui) {
                    // Cuando se usa el slider actualizamos el input
                    $(this).prev('input.slider-value').val(ui.value);
                }
            });
        }

        /* --- Mostrar Slider Descuentos --- */
        $('a.descuento-button').live('click', function(e){

            // Evitar accion predeterminada del click y que no collapse el filtro
            e.stopPropagation();
            e.preventDefault();

            var $this = $(this),
                parent = $(this).parent(),
                lista =  parent.siblings('ul'),
                sliders = $('div.slider-wrapper'),
                guardarShow = $('a.save-descuento').text('Guardar').removeAttr('disable');

            // Si se hace click en el lapiz pero no esta cargado el filtro
            if (lista.is(':empty')) {
                slideFilterBlock(parent);
            }
            // Si está cargado el filtro pero está oculto o colapsado
            else if (lista.is(':hidden')) {
                lista.slideDown();
            }
            // Else: el filtro esta cargado y visible, entonces mostramos los sliders de descuentos
            else {
                sliders.toggle();
                guardarShow.toggle();
            }

            // Buscamos los descuentos solo cuando se hace click en editar descuentos
            if ($('a.save-descuento').is(':visible')) {
                get_descuentos();
            }



        });

        /* --- Traer descuentos ajax --- */
        function get_descuentos() {
            $.getJSON('/app/get_descuentos/', {}, function(descuentos){

                // Por cada descuento buscamos su respectivo input
                $.each(descuentos, function(i, descuento){

                    if ($('input.slider-value[data-value=' + descuento.medio_id + ']').length) {

                        var input = $('input.slider-value[data-value='+descuento.medio_id+']');

                        // NOTAR: Los sliders muestran el porcentaje a pagar, no el descuento
                        // Seteamos el valor desde el server al input
                        input.val(100 - descuento.porcentaje);
                        // Actualizamos el slider del input
                        input.next('span.slider').slider('value', parseInt(100 - descuento.porcentaje));

                    };

                });
            });
        }

        /* --- Guardar Descuentos --- */
        $('a.save-descuento').live('click', function(e){

            // Evitar accion predeterminada del click y que no collapse el filtro
            e.stopPropagation();
            e.preventDefault();

            var output = [],
                $this = $(this);

            $('input.slider-value').each(function(i, v){
                var $this = $(this),
                itemId = $this.data('value'),
                value = $this.val();

                if (value <= 100) {
                    // Los sliders tienen el porcentaje de la tarifa a pagar,
                    // el descuento es el complemento de eso
                    output.push({'medio_id': itemId, 'porcentaje': 100 - value});
                }
            });

            if (output != '') {

                $.getJSON('/app/set_descuentos/', {descuentos: JSON.stringify(output)}).promise().done(function(){
                    $this.attr('disable', 'disable').text('Tarifas actualizadas').delay(3000).fadeOut();
                    $('div.slider-wrapper').delay(3000).fadeOut();
                });
            }

        });


        /* --- Filtros Collapse --- */
        $('.filter-title .collapse-btn').live('click',function(e){ e.preventDefault(); });

        $('.collapse').live('click',function(e){
            slideFilterBlock($(this));

        });

        /* --- Funcion para reutilizar en edicion de descuentos --- */
        function slideFilterBlock (el) {
            var filtroContent = el.siblings('ul'),
                dato = el.children('a').attr('data-filtro');

            if (filtroContent.is(':empty')) { // Si la lista del filtro está vacía

                // Deshabilito el evento de click para evitar multiples llamadas
                el.removeClass('collapse');

                // Funcion para cargar items del filtro
                call_filtro_content(dato, false);

            } else {
                filtroContent.find('.slider-wrapper').hide();
                el.find('.save-descuento').hide();
                filtroContent.slideToggle();

            }

            if (dato != 'anunciante' && dato != 'marca')
                $.get('/app/open_filter/', {data: dato});
        }
        /* --- Traer Opciones de Filtro --- */
        /* FIXME: Los productos debería traerlos después de cargar la unidad */
        function opciones(item, options) {
            var items = [],
                html;

            $.each(options, function (index2, option) {
                if(item == 'etiquetaprograma' || item == 'etiquetamedio' || item == 'etiquetaproducto'){

                    resumenetiqueta = option['item'];
                    html =  '<li class="clearfix">';
                    html += '<div class="nombre">';

                    if(resumenetiqueta.length > 11){
                        html += '<a class="data-filter contTooltipEtiqueta" title="'+resumenetiqueta+'"  href="#" data-filtro="' + item + '" data-value="' + option['id'] + '" title="'+option['item']+'">' + resumenetiqueta + '</a>';
                    }else{
                        html += '<a class="data-filter contTooltip" href="#" data-filtro="' + item + '" data-value="' + option['id'] + '">' + resumenetiqueta + '</a>';
                    }

                    html += '</div>';

                }else if (item == "producto") {
                    html = '<div class="producto clearfix">' + thumb_y_video(option['id'], '/app/thumb_producto/' + option['id'],
                                                                        '/app/video_producto/' + option['id'], option['filetype'],
                                                                        150, false);
                    html += '<a class="data-filter" href="#" data-filtro="producto" data-value="' + option['id'] +'">';
                    html += '<div class="nombre">' + option['item'] + '</div>';
                    html += '<div class="producto-marca" style="margin-top:3px;">' + option['marca'] + '</div>';

                    resumen = option['anunciante'].substring(0,25);

                    if (option['anunciante'].length > 25){
                        resumen = resumen + "...";
                    }

                    resumen = option['anunciante'];
                    html += '<div class="producto-anunciante" >' + resumen +  '</div>';

                    /* No estamos cargando la Agencia '/' + option['agencia'] +*/
                    /* html += '<div class="producto-valor" style="margin-top:3px; font-size=5px;"><b>' + option['absoluto_acotado']  + '</b> ' +$("#unit a")[0].text  + '</div>'; */

                    var unidat = "Segundos";

                    if ($('#divisor').find(".TabResActive").attr("id") == "inversion"){
                        unidat = "Inversión bruta"
                    } else{

                        if ($('#divisor').find(".TabResActive").attr("id") == "cantidad"){
                            unidat = "Anuncios"
                        }
                    }

                    html += '<div class="producto-valor" style="margin-top:3px; font-size=5px;"><b>' + option['absoluto_acotado'] + '</b> ' ;
                    html +=  " " + unidat + " ";

                    // Subió o Bajó Productos
                    var variacion_porcentual = option['variacion_porcentual'];
                    html += '<span style="font-size: 10px;">';

                    var int_variacion_porcentual = parseInt(variacion_porcentual);
                    if (int_variacion_porcentual && int_variacion_porcentual != 0) {
                        if (int_variacion_porcentual > 0) {
                            html += '<img src="/static/design/up.gif" style="vertical-align: baseline"; />';
                        } else {
                            html += '<img src="/static/design/down.gif" style="vertical-align: baseline"; />';
                        }
                    } else {
                        html += '';
                    }

                    html += variacion_porcentual +'% </span>';

                    html +=  '</div>';
                    html += '</a>';
                    // Cuando es video o audio permitimos bajar el archivo
                    //if (option['filetype'] == 'flv' || option['filetype'] == 'mp3') {
                    //    html += '<a download="' + option['marca'] + '-' + option['item'] + '.' + option['filetype'] + '" href="/app/video_producto/'+ option['id'] + '"><i title="Descargar archivo" style="float:right; margin-top:8px;" class="icon-download-alt icon-large"></i></a>';
                    //}

                    html += '</div>';
                    Buscar_thumb_spots(option['id'],'/app/thumb_producto/'+option['id']);

                } else {
                    var filter_behaviour_class = "data-filter"
                    // FIXME: ver bien esto
                    // if (item == 'ciudad') {
                    //     var filter_behaviour_class = "change-filter";

                    resumen = option['item'];
                    /* --- Resize Filtros --- */
                    var espacioDisponible = $('#sidebar').width() - 160;



                    html =  '<li class="clearfix">';
                    html += '<div class="nombre">';



                    //COMPORTAMINETO DEL TOOLTIP EN BASE AL TAMAÑO DEL CONTENEDOR


                    //calculo un ancho en base a una medida fija (194px)
                    var cont = (espacioDisponible*100) / 194 ;
                    //multiplico por 3 la cantidad de caracteres para saber el ancho del texto
                    // y lo comparo con el ancho que calcule antes

                    if((resumen.length * 3) > cont){
                        //si el ancho que ocupan los caracteres + los espacios en blanco supera el ancho 'con'
                        //mantengo el title para que funcione el tooltip
                        html += '<a class="' + filter_behaviour_class + ' contTooltip" style="width:'+espacioDisponible+'px;" href="#" data-filtro="' + item + '" data-value="' + option['id'] + '" title="'+option['item']+'">' + resumen + '</a>';
                    }else{
                        //si el ancho que ocupan los caracteres + los espacios en blanco NO supera el ancho 'con'
                        //quito el title para que no funcione el tooltip
                        html += '<a class="' + filter_behaviour_class + ' contTooltip" style="width:'+espacioDisponible+'px;" href="#" data-filtro="' + item + '" data-value="' + option['id'] + '">' + resumen + '</a>';
                    }

                    //FIN COMPORTAMINETO DEL TOOLTIP EN BASE AL TAMAÑO DEL CONTENEDOR


                    html += '</div>';

                    html += '<div class="estadistica">';


                    // Slider de descuentos (HTML)
                    if (item == "medio") {
                        html += '<div class="slider-wrapper">';
                        html += '<input max="100" value="100" class="slider-value" id="input-slider-'+ index2 +'" data-value="' + option['id'] + '" />% tarifa ';
                        html += '<span id="slider-'+ index2 +'" class="slider"></span></div>';

                    }

                    html += '<span class="absoluto">'+option['absoluto_acotado']+'</span>';

                    html += '<div style="float:left; width:25%; margin:3px 2% 0 1%; background:#DDD; border-radius:3px;">';
                    html += '<span class="barra" style="width: '+option['porcentaje_relativo']*100/100+'%;">';
                    html += '</span></div>';

                    var auxiliar = option['porcentaje'];
                    auxiliar = auxiliar.replace(".0","");

                    if (auxiliar.search("-") >= 0 ){
                        html += '<span class="porcentaje">'+auxiliar+'</span>';
                    } else {
                        html += '<span class="porcentaje">'+auxiliar+'%</span>';
                    }

                    if (option['comparando'] == true) {
                        // Subió o Bajó Filtros
                        var variacion_porcentual = option['variacion_porcentual'];
                        html += '<span style="font-size: 10px;">';

                        var int_variacion_porcentual = parseInt(variacion_porcentual);

                        if (int_variacion_porcentual && int_variacion_porcentual != 0) {
                            if (int_variacion_porcentual > 0) {
                                html += '<img src="/static/design/up.gif" style="vertical-align: baseline"; />';
                            } else {
                                html += '<img src="/static/design/down.gif" style="vertical-align: baseline"; />';
                            }
                        } else {
                            html += '';
                        }
                        html += variacion_porcentual +'% </span>';

                    }
                    html += '</div>';
                    html += '</li>';

                };

                items.push(html);
            });

            return items;

        }

        $('a.contTooltipEtiqueta').limitChar(11,true);

        /* --- Mas Opciones de Productos --- */
        $('#producto-control-row a.options-more').live("click", function(e){
            e.preventDefault();

            if ( $(this).attr('disable') !== 'disable' ) {

                offset = $('.producto-container').children().size();

                var jqxhr = $.ajax({
                    url: '/app/options/more/',
                    type: 'GET',
                    data: {data: 'producto', limit: 3, offset: offset},
                    dataType: "json",
                    beforeSend: function() {
                        $('#producto-control-row .options-more').attr('disable', 'disable');
                    }
                });

                jqxhr.promise().done(function(data) {
                    $('.producto-container').append(opciones('producto', data[0]));
                    $('#producto-control-row .options-more').removeAttr('disable');
                });

            }
            setTimeout(function(){
                $('#producto-control-row a.options-less').show();
            },2000);

        });



        $('#producto-control-row .options-less').live('click',function(e){
            e.preventDefault();
            $.ajax({
                url: '/app/options/less/',
                type: 'GET',
                data: {data: 'producto'},
                dataType: "json",
                success: function(data){
                    $('.producto-container .producto').slice(-3).remove();
                }
            });
            if($('.producto-container').length <= 6){
                $('.options-less').hide();
            }
        });


        $('._fechas').live('click',function(e){
            e.preventDefault();

            var actual = $("#visoractual").val();
            var comparando = $("._CheckCalendar")[0].checked;
            var comparar = $("#visorcomparador").val();

            if (comparando){


                $('.BlockSelectDate').toggle();
                loaderTable.fadeIn();

                var  desde          =     actual.split(" - ")[0];
                var  hasta          =     actual.split(" - ")[1];
                var  comparar_desde =     comparar.split(" - ")[0];
                var  comparar_hasta =     comparar.split(" - ")[1];

                $.get('/app/comparar_fechas/', {from:desde,to:hasta , from2: comparar_desde,to2:comparar_hasta}, function(){
                    recargar_todo_ajax();
                });

            } else {
                $('.BlockSelectDate').toggle();
                loaderTable.fadeIn();


                var periodo = $("#viendoperiodo").val();

                if (periodo.length == 0){
                    $.get('/app/add_filter/', {filter: 'fechas', value: actual}, function(){
                        $("#reset").fadeIn();
                        recargar_todo_ajax();
                    });
                }else{
                  $.get('/app/add_filter/', {filter: 'periodo', value: periodo}, function(){
                        $("#reset").fadeIn();
                        recargar_todo_ajax();
                    });
                }
            }
        });

        /* --- Agregando un filtro a la sesion --- */
        $('.data-filter').live('click',function(e){
            e.preventDefault();
            loaderTable.fadeIn();

            var value = $(this).attr('data-value');
            var filter = $(this).attr('data-filtro');
            $.ajax({
                // FIXME: ver bien esto
                // url: '/app/add_filter/',
                url: '/app/change_filter/',
                type: 'GET',
                data: {filter: filter, value: value},
                success: function(data){
                    $("#reset").fadeIn();
                recargar_todo_ajax();

                },
            });
        });

        /* --- Cambiando un filtro de la sesion --- */
        $('.change-filter').live('click',function(e){
            e.preventDefault();
            loaderTable.fadeIn();

            var value = $(this).attr('data-value');
            var filter = $(this).attr('data-filtro');

            $.ajax({
                url: '/app/change_filter/',
                type: 'GET',
                data: {filter: filter, value: value},
                success: function(data){
                    $("#reset").fadeIn();
                recargar_todo_ajax();

                },
            });
        });

        /* --- Quitar Filtro --- */
        $('a.remove-filter').live('click', function(e){
            e.preventDefault();
            loaderTable.fadeIn();
            $.ajax({
                url: $(this).attr("href"),
                success: function(data){
                    recargar_todo_ajax();
                },
            });
        });

        $('a.remove-search').live('click', function(e){
            e.preventDefault();
            loaderTable.fadeIn();

            $.ajax({
                url: $(this).attr("href"),
                success: function(data){
                    recargar_todo_ajax();
                },
            });
        });

        /* --- Resetear Filtros --- */
        $('.reset-filters').click(function(e){
            e.preventDefault();
            loaderTable.fadeIn();

            $.ajax({
                url: $(this).attr("href"),
                success: function(data){
                    recargar_todo_ajax();
                },
            });
        });
        /* carga en variables los selectores de cada sidebar*/
        var left = $('#sidebar .options-more');
        var right = $('#rightSidebar .options-more');

        /*----    FUNCION MAS OPCIONES -------*/
        function masOpciones(boton){
            var procesando = false;
            boton.live('click',function(e){

                e.preventDefault();
                e.stopPropagation();

                if (procesando) {
                    return;
                }

                var $this = $(this),
                    link = $this.parent('li'),
                    tipo = $this.attr('data-filtro'),
                    limit = (tipo == 'producto') ? 3 : 5,
                    context = $('#'+tipo);

                // Prevenir multiples envios
                procesando = true;

                    // Fix position loader cuando se edita descuentos
                    if (tipo == 'medio' && context.find('a.save-descuento').is(':visible')) {
                        $('.loader', context).css('right', '110px');
                    }

                    $('.loader', context).show();

                    if (tipo != 'producto'){
                        offset = context.children("ul").find('li').size() - 1;
                    } else {
                        offset = context.children('ul').find('li').size();
                    }

                    var jqxhr = $.ajax({
                        url: '/app/options/more/',
                        type: 'GET',
                        data: {data: tipo, limit: limit, offset: offset},
                        dataType: "json"
                    });

                    jqxhr.promise().done(function(data){
                        $('.loader', context).hide();
                        link.detach();
                        append_options_to_filter(tipo, opciones(tipo, data[0]));


                        context.children('ul').append(link);
                        document.getElementById('x'+tipo).style.display = "inline-block";
                        procesando = false;


                        // Mostrar nuevos sliders si el boton de editar está activo en medios
                        if (tipo == 'medio') {

                            $('.loader', context).css('right', '50px');

                            call_sliders(); // Nuevos Sliders
                            get_descuentos(); // Seteamos valores de nuevos sliders

                            if (context.find('a.save-descuento').is(':visible')) {
                                context.find('div.slider-wrapper').css('display', 'block');
                            }
                        }
                    });

            });

        }/*fin mas opciones*/
        /*llama a la funcion para cada sidebar*/
        masOpciones(left);
        masOpciones(right);
        //codigo para menos opciones en el sidebar left
        $('#sidebar .options-less').live('click',function(e){
            e.preventDefault();
            var link = $(this).parent('li');
            var tipo = $(this).attr('data-filtro');
            var limit = (tipo == 'producto') ? 3 : 5;

            $.ajax({
                url: '/app/options/less/',
                type: 'GET',
                data: {data: tipo},
                dataType: "json",
                success: function(data){
                    link.prevAll().slice(0,limit).remove();

                },
            });

            if($(this).closest('ul').length <= 5){
                $(this).hide();
            }
        });
        //codigo para menos opciones en el rightSidebar
        $('#rightSidebar .options-less').live('click',function(e){
            e.preventDefault();
            var link = $(this).parent('li');
            var tipo = $(this).attr('data-filtro');
            var limit = (tipo == 'producto') ? 3 : 5;

            $.ajax({
                url: '/app/options/less/',
                type: 'GET',
                data: {data: tipo},
                dataType: "json",
                success: function(data){
                    link.prevAll().slice(0,limit).remove();
                },
            });

        });

        function lessButton(context) {
            var count = context.siblings().size();
            if(count <= 5) {
                $('.options-less').hide();
            } else {
                $('.options-less').show();
            }
        }

        function thumb_y_video(id, image, video, filetype, width, es_tanda, soporte) {
            var url_image = "",
                val;
            // Si la imagen no existe, hardcodear url de un thumb genérico según el tipo
            //
            var title = 'Ver';
            if (filetype == 'mp3') {
                title = 'Escuchar';
            }

            if (es_tanda){
                url_image = '<img id="tanda'+ id +'" src="' + image
                            + '" alt="' + title +'" title="'+title+'" width="' + width + '" />';

            } else {
                url_image = '<div class="video-thumb" id="spots' + id + '"';
                url_image = url_image + " style=\"background-image:url('" + image + "');\" ";
                url_image = url_image + ' width="' + width + '"></div>';
            }

            if (video) {
                val = '<a class="video-thumb overlay fancybox.iframe" href="/app/ver_video/?path=' +
                video + '&filetype=' + filetype  + '">' +  url_image + '</a>';
            } else {
                val = '<a target="_blank" onclick="';
                val = val + "alert('Video no disponible'); return false;";
                val = val + '" href="' + image + '">' + url_image + '</a>';
            }


            return val;
        };


        /* --- Selector de Unidades --- */
        function set_cantidades_ajax(){
            $.getJSON('/app/unit_selector/',function(unit){
                /*
                $('#unit').prepend('<span class="unit-value">'+unit.value+'</span>');
                var textActive = $('#unit .dropdown-menu a#'+unit['item']).text();
                $('#unit .dropdown-toggle').text(textActive);
                */

                if (unit) {
                    if (unit.item == "cantidad"){
                        $("#cantidad").addClass("TabResActive");
                    } else {
                        if (unit.item == "volumen"){
                            $("#volumen").addClass("TabResActive");
                        } else {
                            $("#inversion").addClass("TabResActive");
                        }
                    }

                    if (unit.cantidad_inversion.length == 1){
                        if (unit.cantidad_inversion[0] == "$0")
                            $('#inversion').hide()
                        else
                            $('#inversion').show()


                        if (unit.cantidad_segundos[0] == "0")
                            $('#volumen').hide()
                        else
                            $('#volumen').show()

                            $('#cantidad_inversion').text(unit.cantidad_inversion[0]);
                            $('#cantidad_anuncios').text(unit.cantidad_anuncios[0]);
                            $('#cantidad_segundos').text(unit.cantidad_segundos[0]);
                            $('#vs_anuncios').text("");
                            $('#vs_inversion').text("");
                            $('#vs_seg').text("");

                    } else {
                        $('#cantidad_inversion').text(unit.cantidad_inversion[0]);
                        $('#cantidad_anuncios').text(unit.cantidad_anuncios[0]);
                        $('#cantidad_segundos').text(unit.cantidad_segundos[0]);

                        $('#vs_anuncios').text(" vs "+unit.cantidad_anuncios[1]);
                        $('#vs_inversion').text(" vs "+unit.cantidad_inversion[1]);
                        $('#vs_seg').text(" vs "+unit.cantidad_segundos[1]);
                    }

                } else {
                    $('#cantidad_inversion').text("0");
                    $('#cantidad_anuncios').text("0");
                    $('#cantidad_segundos').text("0");
                }

                show_productos();
            });
        };






        function show_productos() {
            /* Productos */
            $.getJSON("/app/options/", {data: 'producto', limit: 6}, function(results) {
                var listaProducto = $('#producto .producto-container');

                // Borrar botones + y -
                $('#producto-control-row').remove();

                options = results[0];
                has_more = results[1];
                if (options.length) {
                    listaProducto.append(opciones('producto', options));

                    var controlRow =  '<div id="producto-control-row"><span style="margin-left: -13px;">';
                        controlRow += '<a class="options-more" data-filtro="producto" href="#" title="Más campañas"><i class="fa fa-plus-circle"></i></a>';
                        controlRow += '<a class="options-less" data-filtro="producto" href="#" title="Menos campañas"><i class="fa fa-minus-circle"></i></a>';
                        controlRow += '</span></div>';

                    listaProducto.parent().after(controlRow);
                }

            });

        };



        $('#divisor a.TabRes').live('click',function(e){

            e.preventDefault();

            var $this = $(this),
                jqxhr = null,
                val = $(this).attr('id');

            if ( $this.attr('disable') !== 'disable' ) {

                loaderTable.fadeIn();

                if (jqxhr) {
                    jqxhr.abort();
                }

                var jqxhr = $.ajax({
                    dataType: "json",
                    url: '/app/set_unit/',
                    data: {unit:val},
                    beforeSend: function() {
                        $this.attr('disable', 'disable');
                    }
                });

                jqxhr.promise().done(function(){

                    /* vaciar_unidad_ajax(); */

                    borrar_videos_ajax()
                    set_cantidades_ajax();
                    borrar_grafico_ajax();
                    setear_graficos_ajax();
                    vaciar_filtros_ajax();
                    filtros_ajax(false);
                    $('#divisor').find(".TabResActive").removeClass("TabResActive");
                    $("#"+val).addClass("TabResActive");
                    loaderTable.fadeOut();

                    $this.removeAttr('disable');

                });
            }

        });

        /* --- Filtros de periodo --- */
        $('#time-range a').click(function(e){
            e.preventDefault();
            loaderTable.fadeIn();
            var href = $(this).attr('href');
            $.get(href, function(){
                recargar_todo_ajax();
            });
        });

        /* --- Reset filtros --- */
        $('#reset').click(function(e){
            if($('#inputSecundarias').is(':checked')){
                $('#inputSecundarias').attr('checked', false);
                e.preventDefault();
                var href = $(this).attr('href');
                $.get(href, function(){
                    onClickOtrasMarcas();
                });

            }
            else{
                e.preventDefault();
                var href = $(this).attr('href');
                $.get(href, function(){
                    //$("#reset").fadeOut();
                    recargar_todo_ajax();
                });
            }




        });

        /* --- Reordenar Productos --- */
        function reorderGrid () {
            $('#producto ul li:nth-child(5n)').addClass('no-margin');
        }

        /* --- Sidebar Width --- */
        function sidebarWidth() {
            if ($(window).width() <= '1280') {
                $('body').removeClass().addClass('narrow-1280');
            }
            if ($(window).width() <= '980') {
                $('body').removeClass().addClass('narrow-980');
            }
            if ($(window).width() <= '800') {
                $('body').removeClass().addClass('narrow-800');
            }
        }

        /* --- Ocultar/Mostrar Pies --- */
        $('#pie-toggle').click(function(e){
            e.preventDefault();
            var text = $(this).text();
            var mostrar = false;

            $('#pie_charts').slideToggle();
            if (text == "Ver gráficos de torta") {
                $(this).text("Ocultar gráficos de torta");
                mostrar = true;
            } else {
                $(this).text("Ver gráficos de torta");
                mostrar = false;
            }
            $.get('/filtros/mostrar_tortas/', {"visible": mostrar});
        });


        //cierra el menu de opciones del usuario en el click de cada opcion, menos en la de salir con la que se edirecciona a logout
        $('.cierraLink').click(function(e){
            e.preventDefault();
            $('.OverCerrarSession').hide();
        });


        /* --- Draggable --- */
        $("#filter-options").sortable({
            update : function (event, ui) {
                var orden = $(this).sortable('toArray').toString();
                var dobles=[
                    ['medio', 'grupomedio'],
                    ['marca', 'anunciante']
                ]
                for (var j=0; j<dobles.length;j++) {
                    for (var i=0; i<2; i++) {
                        if ($(ui.item[0]).attr('id') == dobles[j][i]) {
                            $("#" + dobles[j][1-i]).detach().insertAfter($("#" + dobles[j][i]));
                        }
                    }

                }
                $.get('/app/set_ordered_filters/', {data: orden});
            }
        });

        $("#filter-options").disableSelection();

        /* --- Overlay --- */
          $(".overlay").fancybox({
            width: 490,
            minHeight: 340,
            maxHeight: 420,
            scrolling: 'none',
            openEffect  : 'none',
            closeEffect : 'none'
        });

        function vaciar_unidad_ajax(){
            /* $('.unit-value').remove(); */
        }

        /* --- Buscador/Autocomple --- */
        function autocompletar(){
            $("#autocomplete input").autocomplete({
                minLength: 3,
                source: function(request, response) {
                    $.getJSON("/app/lookup/filtro_general/", { texto: request.term }, response);
                },
                select: function( event, ui ) {
                    $.get("/app/add_filter/", {filter: ui.item.data, value: ui.item.id}, function(options) {
                        $("#autocomplete input").val("");
                        recargar_todo_ajax();
                    });
                }
            }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                if (typeof item.data === "undefined") {
                    var tipo =  '';
                } else {
                    var tipo = item.data;
                }
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a>" + item.value + '<span class="tipo-opcion">' + tipo + "</span></a>" )
                    .appendTo( ul );
            };
        };

        /* Mostrar ocultar selector de fecha */
        $('._OpenCalendar').click(function() {

            if ($('.dropdown-menu').is(':visible'))  $('[data-toggle="dropdown"]').parent().removeClass('open');
            if ($('.OverCerrarSession').is(':visible')) $('.OverCerrarSession').hide();

            $('.BlockSelectDate').toggle();
        });
        $('._CancelCalendar').click(function(e) {
            e.preventDefault();
            $('.BlockSelectDate').css({"display":"none"});
        });

        /* ---- Cerrar Calendario y User Dropdown ---- */
        $('body').click(function(e){
          if($(e.target).closest('.BlockSelectDate').length === 0){
            $('.BlockSelectDate').hide();
          }
          if($(e.target).closest('.OverCerrarSession').length === 0){
            $('.OverCerrarSession').hide();
          }
        });
        $('._OpenCalendar, ._ShowSessionUser').click(function(e){
            e.stopPropagation();
        });


        /* Mostrar ocultar session */
        $('._ShowSessionUser').click(function() {
            $('.OverCerrarSession').toggle();

            if ($('.dropdown-menu').is(':visible'))  $('[data-toggle="dropdown"]').parent().removeClass('open');
            if ($('.BlockSelectDate').is(':visible')) $('.BlockSelectDate').hide();
        });


        $('[data-toggle="dropdown"]').click(function(){
            if ($('.OverCerrarSession').is(':visible'))  $('.OverCerrarSession').hide();
            if ($('.BlockSelectDate').is(':visible')) $('.BlockSelectDate').hide();
        });
        //muestra las opciones al hacer click en el check de comportamineto
        $('#notice').click(function(){
            //$('#notice-data').toggle();
            $('#variable option[value=""]').attr('selected','selected');
            $('#comparador option[value=""]').attr('selected','selected');
            $('#periodo option[value=""]').attr('selected','selected');
            //$('#savesearch input[name="variable_valor"]').val('');

        });
        //muestra las opciones al hacer click en el check de comparacion
        $('#comparacion').click(function(){
            //$('#comparacion-data').toggle();
            $('#variable option[value=""]').attr('selected','selected');
            $('#comparador option[value=""]').attr('selected','selected');
            $('#periodo option[value=""]').attr('selected','selected');
            //$('#savesearch input[name="variable_valor"]').val('');

        });
        //cierra el modal
        $('#cerrarmodal').click(function(){
            $('#namesearch').val("");
            $('#notice').attr('checked', false);
            $('#notice-data').toggle();
            $('#comparacion-data').toggle();
            $("#savesearch").modal('hide');
            $('#notice-data').hide();
            $('comparacion-data').hide();
            recargar_todo_ajax();
        });


        // guarda los datos de la busqueda configurada
        $('#salvarbusqueda').click(function(e) {
            e.preventDefault();

            if ($('#namesearch').val() == '') {
                $('#savesearch .alert').show().find('.msg').html('Escriba un nombre para la búsqueda');
                return false;
            }
            //avisos de error al rellenar la alerta comparacion
            if ($('#comparacion').is(':visible')) {
                if ($('#comparacion select').val() == '') {
                    $('#savesearch .alert').show().find('.msg').html('Complete todos los parámetros de la alerta de Comparación');
                    return false;
                }

                if ($("#periodoCompara option:selected").val()=='') {
                    $('#savesearch .alert').show().find('.msg').html('Debe ingresar un periodo a comparar');
                    return false;
                } else if ($("#variableCompara option:selected").val()=='') {
                    $('#savesearch .alert').show().find('.msg').html('Debe ingresar una variable a comparar');
                    return false;
                }
                var nom0 = $('#nombre0').attr('value');
                var nom1 = $('#nombre1').attr('value');
                if(nom0 == nom1){
                    $('#savesearch .alert').show().find('.msg').html('Atención!! Las opciones a comparar son iguales!!');
                    return false;
                }
            }
            //avisos de error al rellenar la alerta comportamiento
             if ($('#comportamiento').is(':visible')) {
                if ($('#comportamiento input').val() == '') {
                    $('#savesearch .alert').show().find('.msg').html('Complete todos los parámetros de la alerta de Comportamiento');
                    return false;
                }

                if ($("#periodo option:selected").val()=='') {
                    $('#savesearch .alert').show().find('.msg').html('Debe ingresar un periodo por comportamiento');
                    return false;
                } else if ($("#variable option:selected").val()=='') {
                    $('#savesearch .alert').show().find('.msg').html('Debe ingresar una variable por comportamiento');
                    return false;
                }
            }


            if ($('#comportamiento').is(':hidden') && $('#comparacion').is(':hidden')) {
                datos = {'nombre' : $('#namesearch').val(), 'alertar_producto': $('#notice-producto').is(':checked')};
            } else {
                if ($('#notice-producto').is(':checked')) {
                    $('#notice-producto').val(true);
                } else {
                    $('#notice-producto').val(false);
                }
                datos = $( "#savesearch form :input[value!='']" ).serialize();
            }



                $.get("/filtros/crear_busqueda/",datos, function(data){
                $('#savesearch .alert').hide();
                $('#namesearch').val("");
                $('#notice').attr('checked', false);
                $('#notice-producto').attr('checked', false);
                //$('#comparacion').attr('checked', false);
                $('#notice-data').toggle();
                //$('#comparacion-data').toggle();
                if (data['respuesta'] == "OK"){
                    $("#savesearch").modal('hide');
                    $('#notice-data').hide();
                    //$('#comparacion').hide();
                    recargar_todo_ajax();
                } else {
                    alert(data['respuesta']);
                }

            });


        });

        $('#btn-close-msg').click(function() {
            $('#savesearch .alert').hide();
        });
        /* --- Reset filtros --- */
        $('#cambiarpassword').click(function(e){
            e.preventDefault();

            $(".OverCerrarSession").css("display","none");
            var old = $("#old_password").val();
            var pass_1 = $("#password1").val();
            var pass_2 = $("#password2").val();

            $.get("/usuarios/change_password",{old: old,pass_1:pass_1,pass_2:pass_2}, function(data){
                if (data['respuesta'] == ""){
                    alert("Cambiaste tu password");
                    $("#myModal").css("display","none");
                }else{
                    alert(data['respuesta']);
                }

            });
        });



        /* Export to Excel */
        var export_excel = $("#export-excel");
        var export_dtv = $("#export-dtv");
        var export_modal = $("#excel-modal");
        var call = "";

        export_excel.click(function(e) {
            e.preventDefault();
            $.blockUI({
                message: export_modal
            });

            call = $.fileDownload($(this).prop('href'))
                .always(function() {
                    $.unblockUI();
                });
        });

        export_dtv.click(function(e) {
            e.preventDefault();
            $.blockUI({
                message: export_modal
            });

            call = $.fileDownload($(this).prop('href'))
                .always(function() {
                    $.unblockUI();
                });
        });

        export_modal.find("button").click(function(e) {
            e.preventDefault();

            if(call) {
                call.abort();
            }

            $.unblockUI();
        });

        // selecciona el valor que se envia al input de acuerdo a lo seleccionado en el select
        $('#comparadorCompara').on('change', function(){
            if($('#comparadorCompara option:selected').text() == '+ 20%'){
                $('#valorCompara').val(20);
            }else if($('#comparadorCompara option:selected').text() == '+ 40%'){
                $('#valorCompara').val(40);
            }else if($('#comparadorCompara option:selected').text() == '+ 60%'){
                $('#valorCompara').val(60);
            }else if($('#comparadorCompara option:selected').text() == '+ 80%'){
                $('#valorCompara').val(80);
            }else if($('#comparadorCompara option:selected').text() == '- 20%'){
                $('#valorCompara').val(20);
            }else if($('#comparadorCompara option:selected').text() == '- 40%'){
                $('#valorCompara').val(40);
            }else if($('#comparadorCompara option:selected').text() == '- 60%'){
                $('#valorCompara').val(60);
            }else if($('#comparadorCompara option:selected').text() == '- 80%'){
                $('#valorCompara').val(80);
            }
        });




        // Scroll Infinito
        var suscribeWaypoints = function() {

            loaderTable.fadeOut();

            $.waypoints('destroy');

            $('.grilla').waypoint(function(direction) {

                if (direction === 'down') {

                    if (typeof(next_url) != "undefined" & next_url != null) {
                        loaderTable.show();
                        get_tabla(next_url, false);
                    }
                }

            }, {offset: 'bottom-in-view'});
        }


    });
    // END jQuery

});
// END Require JS
});
