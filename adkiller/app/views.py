# -*- coding: utf-8 -*- # pylint: disable=too-many-lines
# pylint: disable=too-many-lines, function-redefined, too-many-locals
# pylint: disable=maybe-no-member, redefined-outer-name, bare-except
# pylint: disable=unused-variable, too-many-branches, line-too-long

"""
Views for app app
"""

# Python Imports
from datetime import date
from datetime import datetime
from datetime import timedelta
import csv
import json
import simplejson
import urllib2
from requests import ConnectionError

# Django Imports
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.template import RequestContext


# Project Imports
from adkiller.app.forms import AnunciantesAJAXForm
from adkiller.app.forms import GetProgramaAJAXForm
from adkiller.app.forms import TiempoProcesoForm
from adkiller.app.models import Anunciante
from adkiller.app.models import AudioVisual
from adkiller.app.models import Descuento
from adkiller.app.models import Emision
from adkiller.app.models import Grafica
from adkiller.app.models import Horario
from adkiller.app.models import Marca
from adkiller.app.models import Medio
from adkiller.app.models import Origen
from adkiller.app.models import Patron
from adkiller.app.models import Producto
from adkiller.app.models import Programa
from adkiller.app.models import Rol
from adkiller.app.models import Servidor
from adkiller.media_map.models import Segmento, TipoSegmento
from adkiller.app.utils import generar_csv
from adkiller.app.utils import generar_dtv
from adkiller.app.utils import generar_ibope
from adkiller.app.utils import generar_olx
from adkiller.app.utils import generar_starcom
from adkiller.filtros.forms import ClonarForm
from adkiller.filtros.models import Periodo, Perfil
from adkiller.filtros.utils import cantidades_filtradas
from adkiller.filtros.utils import clonar_emisiones
from adkiller.filtros.utils import get_dict_filtros
from adkiller.usuarios.forms import PackageForm


def sin_permisos(_):
    """Devolver mensaje informativo"""
    return HttpResponse("Usted no tiene permisos para ver esta información")

def obtener_fechas(request):
    """ Obtiene el periodo de fechas elegido por el usuario,
        (se usa en el calendario)
    """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    periodo = Periodo.objects.filter(perfil=perfil)
    if not periodo:
        periodo = Periodo.objects.create(perfil=perfil, lapso="M")
        periodo.fecha_desde = (date.today() - timedelta(days=15)).\
                                                        strftime("%Y-%m-%d")
        periodo.fecha_hasta = date.today().strftime("%Y-%m-%d")
        periodo.save()
    else:
        periodo = periodo[0]
    if periodo.get_lapso_display():
        periodo.actualizar_fechas()

    if periodo.fecha_desde and periodo.fecha_hasta:
        data = periodo.fecha_desde.strftime("%d/%m/%Y") + " - " +\
                                periodo.fecha_hasta.strftime("%d/%m/%Y")
    else:
        data = "01/01/2016 - 01/01/2016"
    data = json.dumps(data, cls=DjangoJSONEncoder)
    response = HttpResponse(data, mimetype='application/json')
    response["Pragma"] = "no-cache"
    response["Cache-Control"] = "no-cache"
    response["Expires"] = "0"
    return response


def set_unit(request):
    """ Vista para cambiar la unidad, en que se filtran los datos
    Seleccionar la unidad de comparación """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    if request.GET:
        perfil.set_unit(request.GET['unit'])
    return HttpResponse("")


def set_descuentos(request):
    """
        Vista para aplicar descuentos a una lista de medios
    """
    if request.GET:
        data = request.GET['descuentos']
        data = simplejson.loads(data)
        for item in data:
            medio = Medio.objects.get(id=item['medio_id'])
            descuento = Descuento.objects.get(user=request.user, medio=medio)
            descuento.descuento = item['porcentaje']
            descuento.save()
    return HttpResponse("")


def get_descuentos(request):
    """
        Obtiene todos los medios con descuentos distintos de 0
        para el usuario actual, cada uno con su descuento

        si se le pasa un medio_id en el request,
        trae sólo el descuento para ese medio
    """
    if request.GET and 'medio_id' in request.GET:
        medio = Medio.objects.get(id=request.GET['medio_id'])
        descuentos = Descuento.objects.filter(user=request.user, medio=medio)
    else:
        descuentos = Descuento.objects.filter(user=request.user,
                                              descuento__gt=0)

    response = [{'medio_id': descuento.medio.id,
                 'porcentaje': descuento.descuento} for descuento in descuentos]

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def unit_selector(request):
    """ Obtiene una [cantidad_emisiones],[inversion],[segundos],
    es una lista porque cuando se compara
    por periodo tiene largo 2, para agregarle el valor del periodo anterior"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    unit, sum_total, cantidad_anuncios, cantidad_segundos, cantidad_inversion =\
            cantidades_filtradas(perfil)
    res = {'item': unit, 'value': sum_total,
           'cantidad_anuncios': cantidad_anuncios,
           'cantidad_segundos': cantidad_segundos,
           'cantidad_inversion': cantidad_inversion}
    data = json.dumps(res, cls=DjangoJSONEncoder)

    response = HttpResponse(data, mimetype='application/json')
    response["Pragma"] = "no-cache"
    response["Cache-Control"] = "no-cache"
    response["Expires"] = "0"
    return response


def url_list_emisiones(request):
    """ Url de la primera pagina de la tabla """
    url = '/api/emisiones/emision/?format=json'  # &order_by=-fecha'
    perfil = Perfil.objects.get(user=request.user, activo=True)

    fdict = get_dict_filtros(perfil)

    for key, value in fdict.items():
        if type(value) == list:
            for v in value:
                url += '&%s=%s' % (key, v)
        else:
            url += '&%s=%s' % (key, value)
    return HttpResponse(url)

# pylint: disable=fixme
# FIXME: debería estar en informes?
# FIXME: no repite la funcionalidad de export_csv?
# pylint: enable=fixme
def export_emisiones(request):
    """ Generar un CSV con los datos de la tabla, segun los filtros elegidos """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_csv(perfil, False)

def export_dtv(request):
    """Exportar para DTV?"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_dtv(perfil)

def export_starcom(request):
    """Exportar para starcom?"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_starcom(perfil)

def export_olx(request):
    """Exportar para OLX?"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_olx(perfil)

def export_ibope(request):
    """Exportar para ibope?"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_ibope(perfil)

@staff_member_required
def export_csv(request):
    """ Generar un CSV con los datos de la tabla, segun los filtros elegidos """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return generar_csv(perfil, True)


def exportar_programas(_):
    """ Generar un CSV con los datos de la tabla, segun los filtros elegidos """

    lista = Medio.objects.all()
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response.write(u'\ufeff'.encode('utf8'))

    #csv.register_dialect('excel', delimiter="\t", escapechar='\\',
    # doublequote=False, quotechar="\"", lineterminator="\r\n")
    #dialect = csv.get_dialect('excel')
    writer = csv.writer(response, delimiter='\t', dialect="excel")
    # writer.writerow(codecs.BOM_UTF16_LE)
    writer.writerow([
        'programa', 'programa_id', 'medio', 'medio_id', 'dias',
        'fechaAlta', 'Hora Inicio', 'Hora Fin', 'ciudad', 'ciudad_id', 'tarifa',
        'identificador', 'canal', 'dvr', 'ident_programa', 'server',
        'server_grabacion'])
    for item in lista:
        row = [u'Programacion Regular', 1, item, item.id,
               '1,1,1,1,1,1,1', '2000-01-01', '00:00:00', '23:59:59',
               item.ciudad, item.ciudad.id, 0,
               item.identificador, item.canal, item.dvr,
               'PRRE', item.server, item.server_grabacion]
        writer.writerow(row)
    return response


def exportar_marcas(request):
    """ Generar un CSV con los datos de la tabla, segun los filtros elegidos """
    if 'marca_id' in request.GET:
        marca_id = int(request.GET["marca_id"])
        lista = Marca.objects.filter(id=marca_id)
    else:
        lista = Marca.objects.filter(
            Q(nombre="Pauta Propia") |
            Q(sector__isnull=False,
              productos__emisiones__audiovisual__isnull=False,
              productos__emisiones__fecha__gt=date.today() - timedelta(days=30)
             )
        ).distinct()

    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response.write(u'\ufeff'.encode('utf8'))

    #csv.register_dialect('excel', delimiter="\t", escapechar='\\',
    # doublequote=False, quotechar="\"", lineterminator="\r\n")
    # dialect = csv.get_dialect('excel')
    writer = csv.writer(response, delimiter='\t', dialect='excel')
    writer.writerow([
        'marca_id', 'marca', 'anunciante', 'anunciante_id',
        'sector', 'sector_id', 'industria', 'industria_id', 'rubro',
        'rubro_id', 'aliasmarca'])
    for item in lista:
        anunciante = item.anunciante
        if not anunciante:
            anunciante = ""
            anunciante_id = 0
        else:
            anunciante_id = anunciante.id
        alias = "|".join([a.nombre.encode("utf8") for a in item.alias.all()])
        sector = item.sector
        if sector:
            industria = str(item.sector.industria)
            industria_id = item.sector.industria.id
            rubro = str(item.sector.industria.rubro)
            rubro_id = str(item.sector.industria.rubro.id)
            sector_id = sector.id
            sector = str(sector)
        else:
            industria = ""
            industria_id = 0
            rubro = ""
            rubro_id = 0
            sector_id = 0
            sector = ""

        row = [
            item.id, str(item), str(anunciante), anunciante_id, sector,
            sector_id, industria, industria_id, rubro, rubro_id, alias
        ]
        writer.writerow(row)
    return response


def compare_filter(request):
    """ Vista para ponerse a comparar filtros del mismo tipo"""
    if request.POST:
        key = request.POST['filter']
    else:
        key = request.GET['filter']
    perfil = Perfil.objects.get(user=request.user, activo=True)

    if perfil.comparado is not None:
        perfil.comparado = None
    else:
        perfil.comparado = ContentType.objects.get(name=key)

    perfil.save()
    return HttpResponseRedirect(reverse(home))


def home(request):
    """ Pantalla principal """

    isDTV = False

    # chequear si tiene los permisos adecuados
    # if request.user.is_staff and not request.user.is_superuser and
    # not request.user.groups.filter(name="Adbout"):
    #    return HttpResponseRedirect("/admin/")
    if not request.user.is_authenticated():
        return redirect('/accounts/login/?next=%s' % request.path)

    if not Perfil.objects.filter(user=request.user):
        perfil = Perfil(user=request.user, activo=True)
        perfil.save()
        perfil.inicializar()

    perfil = Perfil.objects.get(user=request.user, activo=True)
    periodo = perfil.get_periodo_or_create()

    if request.user.is_staff:
        if request.method == 'POST':
            if "package" in request.POST:
                package_form = PackageForm(request.POST, prefix="package")
                if package_form.is_valid():
                    option = package_form.cleaned_data['accion']
                    username = package_form.cleaned_data['user']
                    user_profile, _ = Perfil.objects.get_or_create(user=username, activo=True)

                    if option == 'Cargar':
                        # Cargar filtros del usuario en root
                        user_profile.copiar_periodo_y_filtros_a(perfil)
                        package_form = PackageForm()
                        return HttpResponseRedirect(reverse(home))
                    elif option == 'Guardar':
                        # Guarda filtros activos en user_profile
                        perfil.copiar_periodo_y_filtros_a(user_profile, True)
                        package_form = PackageForm()
                        return HttpResponseRedirect(reverse(home))

            elif "clone" in request.POST:
                clonar_form = ClonarForm(request.POST, prefix="clone")
                if clonar_form.is_valid():
                    medio = clonar_form.cleaned_data['medio']
                    date = clonar_form.cleaned_data['date']
                    clonar_emisiones(perfil, date, medio)
                    return HttpResponseRedirect(reverse(home))

        if request.method == 'GET':
            package_form = PackageForm(prefix="package")
            clone_form = ClonarForm(prefix="clone")

    if periodo.fecha_desde:
        desde = periodo.fecha_desde.strftime("%d/%m/%Y")
    if periodo.fecha_hasta:
        hasta = periodo.fecha_hasta.strftime("%d/%m/%Y")

    filtro = perfil.get_filtro(key="producto")
    if filtro:
        filtro.cantidadActual = 4
        filtro.save()
    return render_to_response('tabla.html',
                              locals(),
                              context_instance=RequestContext(request, isDTV))

def ver_video(request):
    """Mostrar
    - el video de una emisión o de un producto
    - el audio de una emisión o de un producto
    - el video o audio de un origen (tanda, en el concepto tradicional)
    - gráfica?
    """

    # por ahora dejo esto como está, funciona para gráfica
    filetype = request.GET.get('filetype', '')
    path = request.GET.get('path')
    if 'video_origen' in path:
        try:
            pk = int(request.GET['path'].split('/')[3])
            return redirect('/app/video_30fps/?t=origen&id={}'.format(pk))
        except IndexError:
            pass
    if filetype == '.jpg':
        try:
            pk = int(request.GET['path'].split('/')[3])
            patron = Patron.objects.filter(producto__pk=pk)[0]
            id_en_simpson = patron.id_en_simpson
        except IndexError:
            id_en_simpson = ''

    context = {
        'path': path,
        'filetype': filetype,
        'id_en_simpson': '',
        'tipo': 's'
    }
    if 'video_emision' in path:
        context['tipo'] = 'e'

    return render_to_response('video.html', context, context_instance=RequestContext(request))

def video_emision(_, emision):
    """Url de la tanda"""

    emision = Emision.objects.get(id=emision)
    if emision.segmentos.count():
        url = emision.segmentos.all()[0].lazy_url()
    else:
        url = emision.get_fuente().get_url()

    if url:
        return HttpResponseRedirect(url)
    else:
        return HttpResponse("")

def video_origen(_, origen):
    """ Url de la tanda  """
    origen = Origen.objects.get(id=origen)
    url = origen.get_url()
    if url:
        return HttpResponseRedirect(url)
    else:
        return HttpResponse("")

def thumb_emision(_, emision):
    """ Url de la tanda """
    emision = Emision.objects.get(id=emision)
    url = emision.thumb()
    if url:
        return HttpResponseRedirect(url)
    else:
        return HttpResponse("")


def video_producto(_, producto):
    """ Video de un spot """
    producto = Producto.objects.get(id=producto)
    url = producto.video()

    #return HttpResponse(url, content_type="text/plain; charset=utf-8")
    if url:
        return HttpResponseRedirect(url)
    else:
        return HttpResponse("")


def info_producto(request):
    """..."""

    pk = request.GET['id']
    tipo = request.GET['tipo']

    if tipo == 'Producto':
        producto = Producto.objects.get(id=pk)

        if producto.marca.anunciante:
            anunciante = producto.marca.anunciante.nombre
        else:
            anunciante = ''

        response = {'nombre':producto.nombre, 'marca':producto.marca.nombre,
                    'anunciante': anunciante}
    else:
        emision = Emision.objects.get(id=pk)

        if emision.producto.marca.anunciante:
            anunciante = emision.producto.marca.anunciante.nombre
        else:
            anunciante = ''

        response = {
            'nombre':emision.producto.nombre,
            'marca':emision.producto.marca.nombre,
            'anunciante': anunciante
        }

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def thumb_producto(_, producto):
    """ Thumb de un spot """
    producto = Producto.objects.get(id=producto)
    direc = producto.thumb()
    return HttpResponse(unicode(direc))


def comparar_fechas(request):
    """..."""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    Periodos = Periodo.objects.filter(perfil=perfil)

    from_1 = request.GET['from']
    from_1 = datetime.strptime(from_1, "%d/%m/%Y")

    to_1 = request.GET['to']
    to_1 = datetime.strptime(to_1, "%d/%m/%Y")

    from_2 = request.GET['from2']
    from_2 = datetime.strptime(from_2, "%d/%m/%Y")

    to_2 = request.GET['to2']
    to_2 = datetime.strptime(to_2, "%d/%m/%Y")

    periodo_1 = Periodos[0]
    periodo_1.fecha_desde = from_1
    periodo_1.fecha_hasta = to_1
    if len(Periodos) == 2:
        periodo_2 = Periodos[1]
    else:
        periodo_2 = Periodo(perfil=perfil)
    periodo_2.fecha_desde = from_2
    periodo_2.fecha_hasta = to_2
    periodo_2.init_fecha_desde = periodo_1.init_fecha_desde
    periodo_2.init_fecha_hasta = periodo_1.init_fecha_hasta

    periodo_1.lapso = None
    periodo_2.lapso = None

    periodo_1.save()
    periodo_2.save()

    return HttpResponseRedirect(reverse(home))


def get_programa(request):
    """Dado un medio, fecha y hora devuelve el programa asociado"""
    medio = programa = None
    response = {}
    form = GetProgramaAJAXForm(request.GET)

    if form.is_valid():
        print "FORM VALIDO"
        try:
            medio = Medio.objects.get(id=form.cleaned_data['medio'])
        except Medio.DoesNotExist:
            pass

        if medio:
            print "HAY MEDIO"
            fecha = form.cleaned_data['fecha']
            hora = form.cleaned_data['hora']
            programa = medio.get_programa_from_horario(hora, fecha)

        if programa:
            print "HAY PROGRAMA"
            response = {'programa': programa.id}
    data = json.dumps(response, cls=DjangoJSONEncoder)

    return HttpResponse(data, mimetype='application/json')


def get_duracion(request):
    """ Actualiza la duracion de un producto
        segun la ultima emision del mismo"""
    producto = Producto.objects.get(id=request.GET['producto'])
    res = {'duracion': producto.ultima_duracion()}
    data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_tarifa(request):
    """ Funcion auxiliar para grafica,
        que va obteniendo cuanto cuesta la emision"""
    if 'seccion' in request.GET and 'medio' in request.GET and \
            'color' in request.GET and 'ancho' in request.GET and \
            'alto' in request.GET and 'pagina' in request.GET and \
            'contra_tapa' in request.GET:
        seccion = Programa.objects.get(id=request.GET['seccion'])
        medio = Medio.objects.get(id=request.GET['medio'])
        color = request.GET['color']
        ancho = request.GET['ancho']
        alto = request.GET['alto']
        pagina = request.GET['pagina']
        contra_tapa = request.GET['contra_tapa']
    else:
        seccion = "null"
        medio = "null"

    if seccion == "null" or medio == "null":
        return HttpResponse("", mimetype='application/json')

    g = Grafica(color=color, contra_tapa=contra_tapa)
    if alto and alto.isdigit():
        g.alto = int(alto)
    if ancho and ancho.isdigit():
        g.ancho = int(ancho)
    if pagina:
        try:
            g.orden = int(pagina)
        except:
            g.orden = None
    g.color = color == "true"
    g.set_horario(programa=seccion, medio=medio)
    esta = g.horario is not None
    if esta:
        g.calcular_tarifa()

    res = {'tarifa': g.valor, 'esta': esta}
    data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def actualizar_patron(_):
    """nop"""
    pass


def estado_servers(_):
    """..."""
    servers = Servidor.objects.all()
    return render_to_response('estado_servers.html', {'servers': servers})


def estado_server(_, server):
    """..."""
    servers = Servidor.objects.all()
    try:
        serv = Servidor.objects.get(nombre=server)
    except:
        return render_to_response('estado_servers.html', {'servers': servers})
    response = urllib2.urlopen("http://" + serv.url_completa() +
                               "/estado_tandas.json")
    resp = response.read()
    data = json.loads(resp)
    return render_to_response('estado_server.html', {'canales': data,
                                                     'dias': data[1]["dias"],
                                                     'servers': servers})


def get_origen(request):
    """ Vista auxiliar que segun medio,fecha y hora,
    devuelve el nombre asociado al archivo"""
    fecha = None
    hora = None
    medio_id = None

    # parse fecha
    if 'fecha' in request.GET:
        try:
            fecha = datetime.strptime(request.GET['fecha'], "%d/%m/%Y")
        except:
            fecha = None
        format_hora = None
        if request.GET['hora'].count(":") == 1:
            format_hora = "%H:%M"
        elif request.GET['hora'].count(":") == 2:
            format_hora = "%H:%M:%S"
        if format_hora:
            try:
                hora = datetime.strptime(request.GET['hora'], format_hora)
            except:
                hora = None

    if 'medio' in request.GET:
        medio_id = request.GET['medio']

    # en caso de que hayan enviado todos los datos
    if medio_id and medio_id != "null" and hora and fecha:
        medio = Medio.objects.get(id=medio_id)
        programa = medio.get_programa_from_horario(hora, fecha)
        if programa:
            try:
                emision = Emision(fecha=fecha, hora=hora)
                # emision = horario.emisiones.get(fecha=fecha, hora=hora)
                emision.set_horario(medio)
                emision.set_origen()
                # si estamos entrando desde /admin/app/origen/
                if request.GET.get('origen_id') and\
                    request.GET.get('origen_id') != 'null':
                    return HttpResponse(
                        Origen.objects.get(
                            pk=request.GET.get('origen_id')
                        ).get_url()
                    )
                return HttpResponse(emision.get_fuente().get_url())
            except (Horario.DoesNotExist, Emision.DoesNotExist):
                pass

    return HttpResponse("")


def get_server(request):
    """Vista auxiliar que segun medio, devuelve el nombre del servidor"""
    medio_id = request.GET['medio']
    if medio_id != "null":
        medio = Medio.objects.get(id=medio_id)
    return HttpResponse(medio.server.nombre)


def audiovisuales_por_origen(request):
    """..."""
    if 'filename' in request.GET:
        e = AudioVisual.objects.filter(origen=request.GET['filename'])
        json_resp = []
        for aud in e:
            json_resp.append({'producto': aud.producto.nombre,
                              'inicio': aud.inicio})
        return HttpResponse(simplejson.dumps(json_resp),
                            mimetype='application/json')
    return HttpResponse("False")

def tanda_completa(request):
    """..."""
    if 'filename' in request.GET:
        incompletas = AudioVisual.objects.filter(
            origen=request.GET['filename'],
            migracion__isnull=False,
            tanda_completa=False
        )
        registros = AudioVisual.objects.filter(
            origen=request.GET['filename'],
            migracion__isnull=False
        )
        if not incompletas and registros:
            return HttpResponse("True")
        else:
            return HttpResponse("False")
    else:
        return HttpResponse("")

def set_tanda_incompleta(request):
    """recibo un origen y lo pongo como incompleto"""
    if 'filename' in request.GET:
        emisiones_tanda = AudioVisual.objects.filter(
            origen=request.GET['filename'],
            tanda_completa=True
        )
        for e in emisiones_tanda:
            e.tanda_completa = False
            e.save()
        # chequeo
        result = "False"
        emisiones_tanda = AudioVisual.objects.filter(
            origen=request.GET['filename'],
            tanda_completa=True
        )
        #no deberia haber
        if not emisiones_tanda:
            result = "True"
        return HttpResponse(result)

def datos_tanda(request):
    """recibo un origen y devuelvo las emisiones"""
    if 'filename' in request.GET:
        emisiones_tanda = AudioVisual.objects.filter(
            origen=request.GET['filename']
        )
        respuesta = []
        for e in emisiones_tanda:
            respuesta.append({
                'inicio': str(e.inicio),
                'marca': e.producto.marca.nombre,
                'producto': e.producto.nombre,
                'completa': e.tanda_completa,
            })
        return HttpResponse(respuesta)

def ultima_emision(request):
    """..."""
    respuesta = {}
    # los parámetros del GET son m=marca&p=producto
    if not 'm' in request.GET or not 'p' in request.GET:
        respuesta['todo_bien'] = False
        respuesta['mensaje'] = "Faltan datos para realizar la consulta"
    else:
        nombre_marca = request.GET['m']
        nombre_producto = request.GET['p']
        q_prod = (Q(producto__nombre=nombre_producto) |
                  Q(producto__alias__nombre=nombre_producto))

        # ojo: hay una view que se llama ultima emisión...
        ultima_emision = AudioVisual.objects.filter(
            producto__marca__nombre=nombre_marca
        ).filter(q_prod).order_by('-fecha')
        if ultima_emision:
            respuesta['todo_bien'] = True
            respuesta['ultima_emision'] = str(ultima_emision[0].fecha)
        else:
            respuesta['todo_bien'] = False
            msg = "No encontre emisiones de '%s', '%s'" % (nombre_marca,
                                                           nombre_producto)
            respuesta['mensaje'] = msg
    respuesta = json.dumps(respuesta)
    return HttpResponse(respuesta, mimetype="application/json")

# esta es la segunda vez que se define una función con este nombre!
def ultima_emision(request):
    """..."""
    respuesta = {}
    if not 'm' or not 'p' in request.GET:
        respuesta['todo_bien'] = False
        respuesta['mensaje'] = "Faltan datos para realizar la consulta"
    else:
        nombre_marca = request.GET['m']
        nombre_producto = request.GET['p']

        # esta variable se llama igual que la función...
        ultima_emision = AudioVisual.objects.filter(
            producto__nombre=nombre_producto,
            producto__marca__nombre=nombre_marca
        ).order_by('-fecha')
        if ultima_emision:
            respuesta['todo_bien'] = True
            respuesta['ultima_emision'] = str(ultima_emision[0].fecha)
        else:
            respuesta['todo_bien'] = False
            msg = "No encontre emisiones de '%s', '%s'" % (nombre_marca,
                                                           nombre_producto)
            respuesta['mensaje'] = msg
    respuesta = json.dumps(respuesta)
    return HttpResponse(respuesta, mimetype="application/json")


def tiene_publicidad(request):
    """..."""
    e = AudioVisual.objects.all()
    if 'fecha' in request.GET:
        e = e.filter(fecha=request.GET['fecha'])
    if 'hora' in request.GET:
        e = e.filter(hora=request.GET['hora'])
    if 'inicio' in request.GET:
        inicio = float(request.GET['inicio'])
        lower_bound = inicio - 1
        upper_bound = inicio + 1
        e = e.filter(inicio__gt=lower_bound, inicio__lt=upper_bound)
    if 'filename' in request.GET:
        e = e.filter(origen=request.GET['filename'])
    if e:
        return HttpResponse("True")
    else:
        return HttpResponse("False")


def get_json_anunciantes(request):
    """
    Used to retrieve and populate the anunciantes
    FilteredSelectMultiple widget with ajax in the admin
    """
    form = AnunciantesAJAXForm(request.GET)
    if form.is_valid():
        anunciantes = Anunciante.objects.filter(
            nombre__icontains=form.cleaned_data['query']
        )[:15]
        p = [{"name": anunciante.nombre, "id": anunciante.id}
             for anunciante in anunciantes]
        response = json.dumps(p)
        return HttpResponse(response, mimetype="application/json")

@staff_member_required
def control_remoto_decos(_):
    """Ofrecer interfaz para alterar el estado de los decodificadores"""
    context = {}
    return render_to_response("control_remoto_decos.html", context)


@staff_member_required
def trampolin(_):
    """..."""
    context = {
        'servers': Servidor.objects.filter(
            roles__nombre__isnull=False
        ).distinct().order_by('nombre'),
        'corte': Rol.objects.get(nombre='Corte'),
        'deteccion': Rol.objects.get(nombre='Detección'),
        'grabacion': Rol.objects.get(nombre='Grabación')
    }
    return render_to_response("trampolin.html", context)

@staff_member_required
def control_de_calidad(request):
    """..."""
    import random
    if request.method == 'POST':
        cantidad_pedida = int(request.POST['cantidad'])
        servidor_string = request.POST['servidor']
        fecha = request.POST['fecha']
        tandas = AudioVisual.objects.filter(
            migracion__servidor__nombre=servidor_string,
            fecha=fecha, tanda_completa=True
        ).order_by("origen").values('origen').distinct()
        tandas = list(tandas)
        random.shuffle(tandas)
        disponibles = len(tandas)
        cantidad_mostrada = cantidad_pedida
        if disponibles < cantidad_pedida:
            cantidad_mostrada = disponibles
        tandas = tandas[:cantidad_mostrada]
        # busco los productos de cada tanda
        for tanda in tandas:
            emisiones = list(
                AudioVisual.objects.filter(
                    origen=tanda['origen']
                ).order_by('orden')
            )
            tanda['emisiones'] = emisiones
        error = ""
        if not tandas:
            error = "No hay tandas que mostrar"
        server = Servidor.objects.filter(nombre=servidor_string)[0]
        url_server = server.url_completa()
        url_tandas = url_server + '/multimedia/ar/CapturaDVRs/'
        context = RequestContext(request, {
            'cantidad': cantidad_pedida,
            'cantidad_mostrada': cantidad_mostrada,
            'disponibles': disponibles,
            'servidor': servidor_string,
            'servidores': ['Barney', 'Bart', 'Lisa'],
            'fecha': fecha,
            'tandas': tandas,
            'url_tandas': url_tandas,
            'error': error
        })
    else:
        context = RequestContext(request, {
            'cantidad': 10,
            'servidor': 'Barney',
            'servidores': ['Barney', 'Bart', 'Lisa'],
            'fecha': datetime.strftime(datetime.today(), '%Y-%m-%d'),
            'emisiones': None
        })
    return render_to_response("control_de_calidad.html", context)


@staff_member_required
def tiempos_de_proceso(request):
    """..."""

    tiempos = []
    medios = []
    dias = []

    if request.method == 'POST':
        form = TiempoProcesoForm(request.POST)
        if form.is_valid():

            # obtengo la lista de medios
            medios = Medio.objects.all()
            if form.cleaned_data['servidor']:
                medios = medios.filter(server=form.cleaned_data['servidor'])

            # obtengo la lista de dias
            import calendar
            dias = range(1,
                         calendar.monthrange(form.cleaned_data['ano'],
                                             form.cleaned_data['mes'])[1] + 1)

            # armo un arreglo bidimensional de tiempos
            for m in medios:
                row = [(True, m.nombre)]
                for d in dias:
                    f = date(form.cleaned_data['ano'],
                             form.cleaned_data['mes'], d)
                    t = None
                    if m.cantidad_tandas_no_procesadas(f):
                        fecha = date(form.cleaned_data['ano'],
                                     form.cleaned_data['mes'], d)
                        if fecha >= date.today():
                            row.append("")
                        else:
                            row.append((False, (date.today() - fecha).days - 1))
                    else:
                        t = m.tiempo_de_proceso(f)
                        if t:
                            row.append((True, t.days - 1))
                        else:
                            #assert m.cantidad_tandas(f) == 0
                            row.append("")
                tiempos.append(row)

    else:
        form = TiempoProcesoForm()

    return render_to_response("reporte_tiempos_de_proceso.html",
                              locals(),
                              context_instance=RequestContext(request))


@staff_member_required
def tandas_no_procesadas(request, medio_id, fecha):
    """..."""
    medio = Medio.objects.get(id=medio_id)
    dia = date(fecha)
    tandas = medio.tandas_no_procesadas(dia)
    return render_to_response("reporte_tandas_incompletas.html",
                              locals(),
                              context_instance=RequestContext(request))


@staff_member_required
def horarios(request):
    """..."""
    def total_minutes(hora):
        "recibo una hora y devuelvo la cantidad de minutos"
        if hora == None:
            return 0
        return hora.hour * 60 + hora.minute

    todos_los_medios = list(
        Medio.objects.filter(
            soporte__nombre__startswith='T'
        )
    ) # Television, TV Aire, etc
    todos_los_medios.extend(
        list(
            Medio.objects.filter(
                soporte__nombre__icontains='Radio'
            )
        )
    )
    if request.method == 'POST':
        error = ''
        medio = unicode(request.POST['medio'])
        fecha = request.POST['fecha']
        try:
            _ = datetime.strptime(fecha, '%Y-%m-%d')
        except ValueError:
            error = ('"{}": La fecha ingresada no '
                     'tiene un formato válido'.format(fecha))
        if not error:
            # buscar los horarios de ese medio vigentes a esa fecha
            horarios = list(
                Horario.objects.filter(
                    fechaAlta__lte=fecha, medio__nombre=medio
                ).order_by('-fechaAlta')
            )

            def no_solapa_a_ninguno(candidato, horarios_del_dia):
                """..."""
                for h in horarios_del_dia.itervalues():
                    if candidato.solapa_en_horario(h['hora_inicio'], h['hora_fin']):
                        return False
                return True

            # ordenarlos por dia:
            def horarios_dia(dia_num):
                """..."""
                dia = {}
                for h in horarios:
                    if h.emitido_dia(dia_num):
                        if no_solapa_a_ninguno(h, dia):
                            nombre_programa = h.programa.nombre
                            if len(nombre_programa) > 40:
                                nombre_programa = nombre_programa[:37] + '...'
                            datos_horario = {
                                'nombre': nombre_programa,
                                'hora_inicio': h.horaInicio,
                                'hora_fin': h.horaFin,
                                'fecha_alta': h.fechaAlta,
                                'tarifa': '$ ' + str(h.tarifa) if h.tarifa is not None else 'S/D',
                                'link': '/admin/app/horario/' + str(h.id),
                            }
                            dia[total_minutes(h.horaInicio)] = datos_horario
                #convrtirlo en lista
                dia_lista = []
                for horario in sorted(dia.keys()):
                    dia_lista.append(dia[horario])

                return dia_lista

            dias_y_horarios = []
            dias_y_horarios.append(dict(horarios=horarios_dia(0), nombre='Domingo'))
            dias_y_horarios.append(dict(horarios=horarios_dia(1), nombre='Lunes'))
            dias_y_horarios.append(dict(horarios=horarios_dia(2), nombre='Martes'))
            dias_y_horarios.append(dict(horarios=horarios_dia(3), nombre='Miércoles'))
            dias_y_horarios.append(dict(horarios=horarios_dia(4), nombre='Jueves'))
            dias_y_horarios.append(dict(horarios=horarios_dia(5), nombre='Viernes'))
            dias_y_horarios.append(dict(horarios=horarios_dia(6), nombre='Sábado'))
            if not dias_y_horarios:
                error = "No hay horarios cargados para este medio."

            context = RequestContext(request, {'medio': medio,
                                               'medios': todos_los_medios,
                                               'dias_y_horarios': dias_y_horarios,
                                               'error': error,
                                               'fecha': fecha})
        else:
            # si hay un error
            context = RequestContext(request, {'medios': todos_los_medios,
                                               'medio': medio,
                                               'error': error,
                                               'fecha': fecha})
    else:
        # si no me pasaron un pedido por POST
        context = RequestContext(request, {'medios': todos_los_medios,
                                           'error': 'Seleccione un medio para filtrar'})
    return render_to_response("horarios.html", context)

def video_30fps(request):
    """View para mostrar video en 30 fps"""

    object_id = request.GET['id']
    object_type = request.GET['t'] # spot, emision, origen
    fallback_url = ""

    if object_type == 'spot':
        fallback_url = '/app/video_producto/{}'.format(object_id)
        context = RequestContext(request, {'error': 0,
                                           #'emision_nombre' : emision.producto.nombre ,
                                           #'emision_marca':emision.producto.marca.nombre ,
                                           'result': {'url': fallback_url},
                                           'try_create': False,
                                           'fallback_url': fallback_url,
                                          })

    elif object_type == 'origen':
        try:
            origen = Origen.objects.get(id=object_id)
        except ObjectDoesNotExist:
            context = RequestContext(request, {'error': 1})
        else:
            fallback_url = '/app/video_origen/{}'.format(object_id)
            if "radio" in origen.horario.medio.soporte.nombre.lower():
                context = RequestContext(request, {'error': 0,
                                                   'result': {'url': fallback_url},
                                                   'try_create': False,
                                                   'fallback_url': fallback_url,
                                                  })

            else: # es de tele
                # este segmento no se graba en la BBDD
                segmento = origen.get_segmento()
                try:
                    result = segmento.make_clip(tpa=False)
                except (ConnectionError, AttributeError, simplejson.JSONDecodeError):
                    # Si segmento es None, tenemos AttributeError
                    # Si no se pudo conectar a Welo, tenemos ConnectionError
                    # Si Welo falló, tenemos JSONDecodeError
                    context = RequestContext(request, {'error': 1,
                                                       'fallback_url': fallback_url})
                else:
                    if not 'status_url' in result:
                        error = 1
                    else:
                        error = 0
                    context = RequestContext(request, {'error': error,
                                                       'result': result,
                                                       'try_create': True,
                                                       'fallback_url': fallback_url,
                                                      })

    else: # Emisión
        try:
            emision = Emision.objects.get(id=object_id)
        except ObjectDoesNotExist:
            context = RequestContext(request, {'error': 1})
        else:
            fallback_url = '/app/video_emision/{}'.format(object_id)
            if "radio" in emision.horario.medio.soporte.nombre.lower():
                context = RequestContext(request, {'error': 0,
                                                   'emision_nombre' : emision.producto.nombre,
                                                   'emision_marca':emision.producto.marca.nombre,
                                                   'result': {'url': fallback_url},
                                                   'try_create': False,
                                                   'fallback_url': fallback_url,
                                                  })

            else:
                segmento = emision.get_tanda_or_segmento()
                if not segmento:
                    # crear uno de la duración de la emisión
                    hora_real = emision.hora_real()
                    duracion = emision.duracion
                    medio = emision.horario.medio

                    if hora_real is not None and duracion > 0 and medio is not None:
                        segmento = Segmento.objects.create(
                            desde=emision.hora_real(),
                            hasta=emision.hora_real() + timedelta(seconds=emision.duracion),
                            medio=emision.horario.medio,
                            descripcion='Segmento creada por view video_30fps',
                            tipo=TipoSegmento.objects.get(nombre='Publicidad')
                        )

                try:
                    result = segmento.make_clip(tpa=False)
                except (ConnectionError, AttributeError, simplejson.JSONDecodeError):
                    # Si segmento es None, tenemos AttributeError
                    # Si no se pudo conectar a Welo, tenemos ConnectionError)
                    # Si welo fallo, tenemos JSONDecodeError
                    context = RequestContext(request, {'error': 1, 'fallback_url': fallback_url})
                else:
                    if not 'status_url' in result:
                        error = 1
                    else:
                        error = 0
                    context = RequestContext(request, {'error': error,
                                                       'emision_nombre' : emision.producto.nombre,
                                                       'emision_marca': emision.producto.marca.nombre,
                                                       'result': result,
                                                       'try_create': True,
                                                       'fallback_url': fallback_url,
                                                      })


    return render_to_response("video_30fps.html", context)
