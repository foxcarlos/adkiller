# -*- coding: utf-8 -*-
# Python imports
# FIXME: esto tiene sentido acá?
import autocomplete_light
autocomplete_light.autodiscover()
import pdb
# Django imports
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Project imports
from adkiller.app.management.commands.migrar import * # FIXME: refactorizar por imports explicitos
from adkiller.filtros.views import *
from adkiller.app.models import Anunciante, Emision, Programa, Descuento
from views import *  # FIXME: refactorizar por imports explicitos
from forms import *  # FIXME: refactorizar por imports explicitos

from datetime import datetime, date, timedelta, time
import mock

from utils import Lapso

WHITESPACE_PATTERN = re.compile(r'\s+')


def remove_whitespace(string):
    """Removes all whitespace and line breaks in string"""
    return re.sub(WHITESPACE_PATTERN, '', string)


class FakeDate(date):
  "A fake replacement for date that can be mocked for testing."
  def __new__(cls, *args, **kwargs):
      return date.__new__(date, *args, **kwargs)


class InfoadTestCase(TestCase):
    """
        Clase de la que heredan todos los tests de la aplicación.
        Define un setup completo y abarcativo
    """
    def setUp(self):
        # Crear usuario root
        self.root = User.objects.create_user("staff", 'homero@simpson.net', 'xx')
        self.root.is_staff = True
        self.root.save()

        # FIXME: perfil no se debería crear automáticamente al loguearse?
        # creo un perfil para root y seteo fechas
        perfil_root = Perfil.objects.create(user=self.root, activo=True)
        perfil_root.crear_filtros()
        perfil_root.setear_cantidades()

        Periodo.objects.create(perfil=perfil_root,
                    init_fecha_desde=datetime(1982, 9, 21),
                    init_fecha_hasta=datetime(2082, 9, 21),
                    # fecha_desde=datetime(1982, 9, 21),
                    # fecha_hasta=datetime(2082, 9, 21)
                )
        perfil_root.save()

        # Crear usuario no staff
        self.usuario = User.objects.create_user("non_staff", 'bart@simpson.net', 'yy')
        self.usuario.is_staff = False
        self.usuario.save()

        # -> creo un perfil para cliente,
        # y sólo le permito ver cosas de 2012
        perfil_cliente = Perfil.objects.create(user=self.usuario, activo=True)
        perfil_cliente.crear_filtros()
        perfil_cliente.setear_cantidades()

        Periodo.objects.create(perfil=perfil_cliente,
                    init_fecha_desde=datetime(2012, 1, 1),
                    init_fecha_hasta=datetime(2012, 12, 31),
                    fecha_desde=datetime(2011, 12, 31),
                    fecha_hasta=datetime(2013, 1, 1)
                )
        perfil_cliente.save()

        # Creo dos marcas con un producto cada una
        consumo = Rubro.objects.create(nombre="consumo")
        alimentos = Industria.objects.create(rubro=consumo, nombre="alimentos")
        self.sector = Sector.objects.create(industria=alimentos, nombre="chatarra")

        self.cocacola = Marca.objects.create(sector=self.sector, nombre="Coca Cola")
        self.cocacolalife = Producto.objects.create(marca=self.cocacola, nombre="Coca Cola Life")

        self.pepsico = Marca.objects.create(sector=self.sector, nombre="Pepsico Snacks")
        self.chizitos = Producto.objects.create(marca=self.pepsico, nombre="chizitos")

        # Creo dos medios, y un horario/programa para cada uno          
        self.cordoba = Ciudad.objects.create(nombre="Córdoba")
        tv_aire = Soporte.objects.create(nombre="tv_aire")

        self.canal10 = Medio.objects.create(nombre="canal 10", ciudad=self.cordoba, soporte=tv_aire)
        cronica_mediodia = Programa.objects.create(nombre="cronica_mediodia")
        self.canal12 = Medio.objects.create(nombre="canal 12", ciudad=self.cordoba, soporte=tv_aire)
        noticiero_12 = Programa.objects.create(nombre="noticiero_12")
        
        params = {
            "medio": self.canal10,
            "programa": cronica_mediodia,
            "fechaAlta": datetime(2010, 01, 01),
            "horaInicio": "12:00",
            "horaFin": "13:00",
            "dias": "1,1,1,1,1,0,0",
        }
        self.horario_del_10 = Horario.objects.create(**params)

        params.update({'medio': self.canal12, 'programa': noticiero_12})
        self.horario_del_12 = Horario.objects.create(**params)

        # Definimos un programa, producto y horario por defecto
        self.programa = cronica_mediodia
        self.horario = self.horario_del_10
        self.producto = self.chizitos

    def crear_emision(self, fecha, hora="12:01", orden=1, duracion=1, valor=1,
                        horario=None, producto=None):
        if not producto:
            producto = self.producto
        if not horario:
            horario = self.horario
        params = {
             "producto": producto,
             "horario": horario,
             "fecha": fecha,
             "hora": hora,
             "orden": orden,
             "duracion": duracion,
             "valor": valor
            }
        return Emision.objects.create(**params)


class MedioTestCase(InfoadTestCase):
    """
    Suite de test para el model Medio
    """
    def setUp(self):
      self.medio = Medio()


class ProductoTest(InfoadTestCase):

    def test_info_producto(self):
      """Veo que me devuelva correctamente nombre de producto, marca y 
        anunciante."""

      # me logueo como usuario
      self.client.login(username="non_staff", password="yy")

     
      anunciante = Anunciante.objects.create(nombre="anunciante")
      marca = Marca.objects.create(sector=self.sector, nombre="marca", anunciante=anunciante)
      producto = Producto.objects.create(marca=marca, nombre="producto")

      pk = producto.id
      

      response = self.client.get(reverse(info_producto), {'id': pk ,'tipo': 'Producto'})
      data_producto = simplejson.loads(response.content)

      self.assertEqual(data_producto['nombre'], 'Producto')
      self.assertEqual(data_producto['marca'], 'Marca')
      self.assertEqual(data_producto['anunciante'], 'anunciante')

    def test_info_producto_sin_anunciante(self):
      """Veo que me devuelva correctamente nombre de producto y marca aunque
        no tenga anunciante."""

      # me logueo como usuario
      self.client.login(username="non_staff", password="yy")
      
      marca = Marca.objects.create(sector=self.sector, nombre="marca")
      producto = Producto.objects.create(marca=marca, nombre="producto")

      pk = producto.id
      response = self.client.get(reverse(info_producto), {'id': pk, 'tipo': 'Producto'})
      data_producto = simplejson.loads(response.content)
      self.assertEqual(data_producto['nombre'], 'Producto')
      self.assertEqual(data_producto['marca'], 'Marca')
      self.assertEqual(data_producto['anunciante'], '')


class EmisionTest(InfoadTestCase):


    def test_cantidad_avisos_por_tanta(self):

        # Creo emisiones para esos origenes
        fecha = datetime(2012, 07, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='12:01', duracion=1,
                                orden=1, valor=1, origen="Archivo1")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.chizitos, hora='12:15', duracion=1,
                                orden=2, valor=1, origen="Archivo1")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='12:25', duracion=1,
                                orden=3, valor=1, origen="Archivo1")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.chizitos, hora='12:35', duracion=1,
                                orden=4, valor=1, origen="Archivo1")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='12:50', duracion=1,
                                orden=5, valor=1, origen="Archivo1")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='15:50', duracion=1,
                                orden=1, valor=1, origen="Archivo2")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.chizitos, hora='15:55', duracion=1,
                                orden=2, valor=1, origen="Archivo2")
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='15:59', duracion=1,
                                orden=3, valor=1, origen="Archivo2")

        emision = Emision.objects.filter(origen="Archivo1")[0]
        emision2 = Emision.objects.filter(origen="Archivo2")[0]
        self.assertEqual(emision.cantidad_avisos_tanda(), 5)
        self.assertEqual(emision2.cantidad_avisos_tanda(), 3)

# FIXME: arreglar posts al home para q usen packages
class FechasYperiodosTest(InfoadTestCase):
    """
        Tests de manejo de fechas, periodos
        restricciones iniciales y actualización de periodo actual
    """
    # ----> OK!
    def test_seteo_inicial_ok(self):
        # las fechas fuera de límite de non-staff deberían estar corregidas
        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
        self.assertEqual(periodo.init_fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")
        self.assertEqual(periodo.init_fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

        # fecha_desde y fecha_hasta de root deberían estar seteados por defecto
        # a los valores de los init
        perfil = self.root.get_profile()
        periodo = perfil.get_periodo_or_create()

        self.assertEqual(periodo.fecha_desde, periodo.init_fecha_desde)
        self.assertEqual(periodo.fecha_hasta, periodo.init_fecha_hasta)

    # ----> OK!
    def test_setear_ok(self):
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "fecha_desde"
        data['value'] = "01/03/2012"
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)

        data['filter'] = "fecha_hasta"
        data['value'] = "01/04/2012"
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/03/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "01/04/2012")

    # ----> OK!
    def test_exceder_fecha_desde(self):
        """ non_staff intenta sobrepasar la fecha desde asignada"""
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "fecha_desde"
        data['value'] = "01/11/2010"

        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()

        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")

    # ----> OK!
    def test_exceder_fecha_hasta(self):
        """ non_staff intenta sobrepasar la fecha hasta asignada"""
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "fecha_hasta"
        data['value'] = "01/11/2020"

        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    # ----> OK!
    def test_validar_periodo(self):
        perfil_cliente = Perfil.objects.create(user=self.usuario, activo=True)
        periodo = Periodo.objects.create(perfil=perfil_cliente,
                    init_fecha_desde=datetime(2012, 1, 1),
                    init_fecha_hasta=datetime(2012, 12, 31),
                    fecha_desde=datetime(2011, 12, 15),
                    fecha_hasta=datetime(2013, 1, 15)
                )
        periodo.validar()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    # ----> FAIL FIXME
    # ----> roto el test, la funcionalidad anda bien
    #def test_obtener_fechas_ok(self):
    #    user_login = self.client.login(username="non_staff", password="yy")
    #    self.client.get(reverse(home))
    #    response = self.client.get(reverse(obtener_fechas))
    #    perfil = self.usuario.get_profile()
    #    periodo = perfil.get_periodo_or_create()
    #    print "laps"
    #    print periodo.get_lapso_display()
    #    print response.content
    #    desde, hasta = response.content.split(" - ")
    #    desde = desde.replace('"', "")
    #    hasta = hasta.replace('"', "")
    #    self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), desde)
    #    self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), hasta)
   

# ----> Pendientes
   # def test_cambiar_periodo_no_staff(self):
   #      """ me logueo, siendo staff
   #          creo un usuario y verifico que se cree
   #          Verifico que ande el cambiar periodo cundo no soy staff y tengo asignado
   #          un periodo.fecha desde y un periodo.fecha hasta
   #      """
   #      self.user.is_staff = True
   #      self.user.save()
   #      login = self.client.login(username="adkiller", password="worms")
   #      self.assertTrue(login)
   #      response = self.client.get("/accounts/register/")
   #      form = {}
   #      form["username"]  = "prueba"
   #      form["password"]  = "1212"
   #      form["passwordValidate"]  = "1212"
   #      form["email"]  = "a@a.com"

   #      response = self.client.post("/accounts/register/",form)

   #      self.assertEqual(User.objects.all().count(),2)
   #      self.assertNotEqual(User.objects.get(username="prueba"),None)
   #      self.assertRedirects(response,"/accounts/register_ok/"+str(User.objects.get(username="prueba").id)+"/")
   #      self.otro  = User.objects.create_user("malvado", 'hola@simpson.net', "malvado",)
   #      self.malvado = Client()
   #      self.malvado.login(username="malvado", password="malvado")
   #      self.assertTrue(login)
   #      response = self.malvado.get(reverse('home'))
   #      self.assertEqual(response.status_code, 200)
   #      self.malvado.logout()
   #      self.assertEqual(Perfil.objects.all().count(),2)
   #      perfil_malvado = Perfil.objects.get(user=self.otro)

   #      data = {}
   #      data["accion"] = "Guardar"
   #      data["username"] = "malvado"
   #      self.client.post(reverse('home'),data)
   #      self.assertEqual(OpcionFiltro.objects.all().count(),0)

   #      self.malvado.login(username="malvado", password="malvado")
   #      data = {}
   #      data["filter"] = "periodo"
   #      data["value"] = "dia"
   #      response = self.malvado.get(reverse(add_filter),data)
   #      hoy = date.today()
   #      self.assertEqual(Periodo.objects.get(perfil=perfil_malvado).fecha_hasta,hoy)

   #  def test_cambiar_periodo_no_staff_sin_restriccion_de_tiempo(self):
   #      """ logueo, siendo staff, creo un usuario, verifico que se cree el usuario
   #          Verifico que ande el cambiar periodo condo no soy staff y no tengo asignado
   #          un periodo.fecha desde y un periodo.fecha hasta"""
   #      self.user.is_staff = True
   #      self.user.save()
   #      login = self.client.login(username="adkiller", password="worms")
   #      self.assertTrue(login)
   #      response = self.client.get("/accounts/register/")
   #      form = {}
   #      form["username"]  = "prueba"
   #      form["password"]  = "1212"
   #      form["passwordValidate"]  = "1212"
   #      form["email"]  = "a@a.com"

   #      response = self.client.post("/accounts/register/",form)

   #      self.assertEqual(User.objects.all().count(),2)
   #      self.assertNotEqual(User.objects.get(username="prueba"),None)
   #      self.assertRedirects(response,"/accounts/register_ok/"+str(User.objects.get(username="prueba").id)+"/")
   #      self.otro  = User.objects.create_user("malvado", 'hola@simpson.net', "malvado",)
   #      self.malvado = Client()
   #      self.malvado.login(username="malvado", password="malvado")
   #      self.assertTrue(login)
   #      response = self.malvado.get(reverse('home'))
   #      self.assertEqual(response.status_code, 200)
   #      self.malvado.logout()
   #      self.assertEqual(Perfil.objects.all().count(),2)
   #      perfil_malvado = Perfil.objects.get(user=self.otro)

   #      data = {}
   #      data["accion"] = "Guardar"
   #      data["username"] = "malvado"
   #      self.client.post(reverse('home'),data)
   #      self.assertEqual(OpcionFiltro.objects.all().count(),0)

   #      periodo = Periodo.objects.get(perfil=perfil_malvado)
   #      periodo.init_fecha_hasta = None
   #      periodo.init_fecha_desde = None
   #      periodo.save()


   #      self.malvado.login(username="malvado", password="malvado")
   #      data = {}
   #      data["filter"] = "periodo"
   #      data["value"] = "dia"
   #      response = self.malvado.get(reverse(add_filter),data)
   #      hoy = date.today()
   #      self.assertEqual(Periodo.objects.get(perfil=perfil_malvado).fecha_hasta,hoy)

    

    # ----> FAIL
    # ----> roto el test, la funcionalidad anda bien
    # def test_cargar_ok(self):
    #     """ root elige desde 2012-11-01 al 2012-11-30, setea el valor
    #         non_staff tiene esos datos como iniciales,
    #         root carga el periodo de non_staff y puede modificarlo
    #     """
    #     # Me logueo como root
    #     self.client.login(username="staff", password="xx")
        
    #     data = {}
    #     data['filter'] = "fecha_desde"
    #     data['value'] = "01/11/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     data['filter'] = "fecha_hasta"
    #     data['value'] = "30/11/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     data = {}
    #     data["package-accion"] = "Cargar"
    #     data["package-user"] = self.usuario.id
    #     data['package'] = "Enviar"
    #     self.client.post(reverse(home), data)
    #     response = self.client.post(reverse(home), data)

    #     periodo = self.root.get_profile().get_periodo_or_create()
    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.init_fecha_desde, None)
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")
    #     self.assertEqual(periodo.init_fecha_hasta, None)

    #     data = {}
    #     data['filter'] = "fecha_desde"
    #     data['value'] = "01/11/2011"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     perfil = self.root.get_profile()
    #     periodo = perfil.get_periodo_or_create()

    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2013")

    # def test_year_bad(self):
    #     data = {}
    #     data['filter'] = "fecha_desde"
    #     data['value'] = "01/01/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     data = {}
    #     data['filter'] = "fecha_hasta"
    #     data['value'] = "31/12/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     perfil_root = self.root.get_profile()
    #     periodo = perfil_root.get_periodo_or_create()
    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    #     data = {}
    #     data["accion"] = "Guardar"
    #     data["username"] = "non_staff"
    #     self.client.post(reverse(home), data)

    #     perfil_usuario = self.usuario.get_profile()
    #     periodo = perfil_usuario.get_periodo_or_create()
    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.init_fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")
    #     self.assertEqual(periodo.init_fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    #     data = {}
    #     data['filter'] = "periodo"
    #     data['value'] = "ano"
    #     self.client.login(username="non_staff", password="yy")
    #     response = self.client.get(reverse(add_filter), data)

    #     hoy = date.today()
    #     ayer = hoy - timedelta(days=365)
        
    #     periodo = perfil_usuario.get_periodo_or_create()
    #     self.assertNotEqual(periodo.fecha_desde, ayer)
    #     self.assertNotEqual(periodo.fecha_hasta, hoy)

    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.init_fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")
    #     self.assertEqual(periodo.init_fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    # def test_selector_unidad_ok(self):
    #     response = self.client.get(reverse(unit_selector))
    #     data = {}
    #     data['unit'] = "volumen"
    #     response = self.client.get(reverse(set_unit), data)
    #     response = self.client.get(reverse(unit_selector))

    #     data = {}
    #     data['unit'] = "inversion"
    #     response = self.client.get(reverse(set_unit), data)
    #     response = self.client.get(reverse(unit_selector))

    #     rubro = Rubro(nombre="rubro")
    #     rubro.save()
    #     industria = Industria(nombre="industria", rubro=rubro)
    #     industria.save()
    #     sector = Sector(nombre="sector", industria=industria)
    #     sector.save()
    #     marca = Marca(nombre="id2", sector=sector)
    #     marca.save()
    #     marca_2 = Marca(nombre="marca2", sector=sector)
    #     marca_2.save()

    #     p = Producto(marca=Marca.objects.get(nombre="id2"))
    #     p.save()

    #     producto_2 = Producto(marca=Marca.objects.get(nombre="marca2"))
    #     producto_2.save()

    #     Soporte(nombre="soporte").save()
    #     Ciudad(nombre="soporte").save()
    #     medio = Medio(soporte=Soporte.objects.get(),
    #                     ciudad=Ciudad.objects.get())
    #     medio.save()
    #     programa = Programa(nombre="programa", medio=medio)
    #     programa.save()

    #     hoy = date.today()
    #     dictio = {
    #      "producto": p,
    #      "programa": programa,
    #      "fecha": hoy,
    #      "hora": "12: 01",
    #      "orden": 1,
    #      "duracion": 1,
    #      "alto": 1,
    #      "ancho": 1,
    #      "superficie": 0,
    #      "seg": 10010,
    #      "valor": 10010
    #     }

    #     Emision(**dictio).save()

    #     dictio["duracion"] = 10
    #     dictio["orden"] = 2
    #     Emision(**dictio).save()

    #     dictio["alto"] = 10
    #     dictio["orden"] = 3
    #     Emision(**dictio).save()

    #     dictio["orden"] = 5
    #     dictio["seg"] = 10
    #     Emision(**dictio).save()

    #     dictio["orden"] = 6
    #     dictio["valor"] = 10
    #     Emision(**dictio).save()

    #     dictio["producto"] = producto_2
    #     Emision(**dictio).save()

    #     data = {}
    #     data['filter'] = "fecha_hasta"
    #     data['value'] = "31/12/2015"
    #     response = self.client.get(reverse(add_filter), data)

    #     response = self.client.get(reverse(unit_selector))
    #     self.assertEqual(Emision.objects.all().count(), 6)
    #     self.client.get(reverse(url_list_emisiones))

    #     data = {}
    #     data['filter'] = "programa"
    #     data['value'] = "1"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.client.get(reverse(url_list_emisiones))

    # def test_export_emisiones(self):
    #     response = self.client.get(reverse(unit_selector))
    #     data = {}
    #     data['unit'] = "volumen"
    #     response = self.client.get(reverse(set_unit), data)
    #     response = self.client.get(reverse(unit_selector))
    #     data = {}
    #     data['unit'] = "inversion"
    #     response = self.client.get(reverse(set_unit), data)
    #     response = self.client.get(reverse(unit_selector))

    #     rubro = Rubro(nombre="rubro")
    #     rubro.save()
    #     industria = Industria(nombre="industria", rubro=rubro)
    #     industria.save()
    #     sector = Sector(nombre="sector", industria=industria)
    #     sector.save()
    #     marca = Marca(nombre="id2", sector=sector)
    #     marca.save()
    #     marca_2 = Marca(nombre="marca2", sector=sector)
    #     marca_2.save()

    #     p = Producto(marca=Marca.objects.get(nombre="id2"))
    #     p.save()

    #     producto_2 = Producto(marca=Marca.objects.get(nombre="marca2"))
    #     producto_2.save()

    #     Soporte(nombre="soporte").save()
    #     Ciudad(nombre="soporte").save()
    #     medio = Medio(soporte=Soporte.objects.get(),
    #                             ciudad=Ciudad.objects.get())
    #     medio.save()
    #     programa = Programa(nombre="programa", medio=medio)
    #     programa.save()

    #     hoy = date.today()
    #     kwargs = {
    #      "producto": p,
    #      "programa": programa,
    #      "fecha": hoy,
    #      "hora": "12: 01",
    #      "orden": 1,
    #      "duracion": 1,
    #      "alto": 1,
    #      "ancho": 1,
    #      "superficie": 0,
    #      "seg": 10010,
    #      "valor": 10010
    #     }

    #     Emision(**kwargs).save()

    #     kwargs["duracion"] = 10
    #     kwargs["orden"] = 2
    #     Emision(**kwargs).save()

    #     kwargs["alto"] = 10
    #     kwargs["orden"] = 3
    #     Emision(**kwargs).save()

    #     kwargs["orden"] = 5
    #     kwargs["seg"] = 10
    #     Emision(**kwargs).save()

    #     kwargs["orden"] = 6
    #     kwargs["valor"] = 10
    #     Emision(**kwargs).save()

    #     kwargs["producto"] = producto_2
    #     Emision(**kwargs).save()

    #     data = {}
    #     data['filter'] = "fecha_hasta"
    #     data['value'] = "31/12/2015"
    #     response = self.client.get(reverse(add_filter), data)

    #     response = self.client.get(reverse(unit_selector))
    #     self.assertEqual(Emision.objects.all().count(), 6)

    #     self.client.get(reverse(url_list_emisiones))

    #     data = {}
    #     data['filter'] = "programa"
    #     data['value'] = "1"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.client.get(reverse(url_list_emisiones))
    #     self.client.get(reverse(export_emisiones))


@mock.patch('adkiller.filtros.views.date', FakeDate)
@mock.patch('adkiller.filtros.models.date', FakeDate)
class LapsosTest(InfoadTestCase):
    """
        Testeamos el manejo de lapsos: periodos que varían
        dinámicamente del estilo "último día" o "último año"
    """

    # def test_dia_ok(self):

    # def test_lapso_dia(self):

    def test_lapso_semana(self):
        FakeDate.today = classmethod(lambda cls: date(2012, 11, 30))
        
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "periodo"
        data['value'] = "semana"
        self.client.get(reverse(add_filter), data)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "24/11/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "30/11/2012")

    # def test_lapso_mes(self):

    # def test_lapso_anio(self):

    def test_exceder_dia(self):
        """
            non_staff intenta sobrepasar la fecha_desde y fecha_hasta asignadas
            haciendo click en 'Hoy'
        """
        FakeDate.today = classmethod(lambda cls: date(2013, 1, 1))
        
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "periodo"
        data['value'] = "dia"
        self.client.get(reverse(add_filter), data)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "31/12/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    def test_exceder_semana_desde(self):
        """
            non_staff intenta sobrepasar la fecha_desde asignada
            haciendo click en 'última semana'
        """
        FakeDate.today = classmethod(lambda cls: date(2012, 1, 5))
        
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "periodo"
        data['value'] = "semana"
        self.client.get(reverse(add_filter), data)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "05/01/2012")

    def test_exceder_semana_hasta(self):
        """
            non_staff intenta sobrepasar la fecha_hasta asignada
            haciendo click en 'última semana'
        """
        FakeDate.today = classmethod(lambda cls: date(2013, 1, 5))
        
        self.client.login(username="non_staff", password="yy")

        data = {}
        data['filter'] = "periodo"
        data['value'] = "semana"
        self.client.get(reverse(add_filter), data)

        perfil = self.usuario.get_profile()
        periodo = perfil.get_periodo_or_create()
        self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "30/12/2012")
        self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/12/2012")

    def test_exceder_mes(self):
        """
            non_staff intenta sobrepasar la fecha_desde y fecha_hasta asignadas
            haciendo click en 'último mes'
        """
        pass

    def test_exceder_anio(self):
        """
            non_staff intenta sobrepasar la fecha_desde y fecha_hasta asignadas
            haciendo click en 'último año'
        """
        pass

    # FIXME: hacer andar acciones home
    # FIXME: revisar, ver qué hace
    # def test_mes_ok(self):
    #     data = {}
    #     data['filter'] = "fecha_desde"
    #     data['value'] = "01/01/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     data['filter'] = "fecha_hasta"
    #     data['value'] = "31/01/2012"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(response.status_code, 200)

    #     perfil = self.root.get_profile()
    #     periodo = perfil.get_periodo_or_create()
    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/01/2012")

    #     data = {}
    #     data["accion"] = "Guardar"
    #     data["username"] = "non_staff"
    #     self.client.post(reverse(home), data)

    #     perfil = self.usuario.get_profile()
    #     periodo = perfil.get_periodo_or_create()
    #     self.assertEqual(periodo.fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.init_fecha_desde.strftime("%d/%m/%Y"), "01/01/2012")
    #     self.assertEqual(periodo.fecha_hasta.strftime("%d/%m/%Y"), "31/01/2012")
    #     self.assertEqual(periodo.init_fecha_hasta.strftime("%d/%m/%Y"), "31/01/2012")

    #     data = {}
    #     data['filter'] = "periodo"
    #     data['value'] = "mes"
    #     self.client.login(username="non_staff", password="yy")
    #     response = self.client.get(reverse(add_filter), data)

    #     hoy = date.today()
    #     hace_un_mes = hoy - timedelta(days=30)
    #     perfil = self.usuario.get_profile()
    #     periodo = perfil.get_periodo_or_create()
    #     self.assertNotEqual(periodo.fecha_desde, hace_un_mes)
    #     self.assertNotEqual(periodo.fecha_hasta, hoy)


class UnitSelectorViewTest(InfoadTestCase):
    def setUp(self):
        InfoadTestCase.setUp(self)

        # Emisiones en canal 12
        self.crear_emision('2013-06-14', valor=1, duracion=1, horario=self.horario_del_12)
        self.crear_emision('2013-06-15', valor=2, duracion=2, horario=self.horario_del_12)

        # Emisiones en Canal 10
        self.crear_emision('2013-07-15', valor=3, duracion=3, horario=self.horario_del_10)
        self.crear_emision('2013-07-16', valor=5, duracion=4, horario=self.horario_del_10)

        # Me logueo como staff y voy a la home
        self.client.login(username="staff", password="xx")
        self.client.get(reverse(home))

    def test_selector_unidad_comparacion(self):
        """ Creo 2 emisiones en junio/2013 y 2 en julio/2013
            Luego hago una comparación entre esos dos meses
            y verifico que inversión, volumen y cantidad de anuncios
            den las sumas correspondientes
        """
        perfil = self.root.get_profile()
        periodos = Periodo.objects.filter(perfil=perfil)
        for p in periodos:
            p.delete()

        response = self.client.get(reverse(set_unit), {'unit': 'volumen'})
        init_fecha_desde = date(1982,9,21)
        init_fecha_hasta = date(2082,9,21)

        fecha_desde=date(2013,6,1)
        fecha_hasta=date(2013,6,30)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        fecha_desde=date(2013,7,1)
        fecha_hasta=date(2013,7,31)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        response = self.client.get(reverse(unit_selector))

        expected_json = u"""{"cantidad_segundos": ["3", "7"],
                            "item": "volumen",
                            "cantidad_anuncios": ["2", "2"],
                            "cantidad_inversion": ["$3", "$8"],
                            "value": "3"}"""

        self.assertEqual(remove_whitespace(expected_json),
            remove_whitespace(response.content))

    def test_selector_unidad_con_descuento(self):
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        periodos = Periodo.objects.filter(perfil=perfil)
        for p in periodos:
            p.delete()

        # tenemos un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal10)
        descuento.descuento = 50
        descuento.save()

        # setear un periodo que incluya a las 4 emisiones
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,6,1)
        fecha_hasta=datetime(2013,7,30)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        response = self.client.get(reverse(unit_selector))

        expected_json = u"""{"cantidad_segundos": ["10"],
                            "item": "inversion",
                            "cantidad_anuncios": ["4"],
                            "cantidad_inversion": ["$7"],
                            "value": "$7"}"""

        self.assertEqual(remove_whitespace(expected_json),
            remove_whitespace(response.content))


# FIXME: todos los tests están hechos con login como non_staff
# -----> OK!
class DescuentosTest(InfoadTestCase):
    """ Tests para el manejo de descuentos por usuario y medio,
        - en las opciones de la barra lateral
        - en el gráfico
        - en el unit_selector (testeado en UnitSelectorViewTest)
    """
    def setUp(self):
        InfoadTestCase.setUp(self)

        # creo emisiones, una para cada producto en cada medio
        fecha = datetime(2013, 07, 1)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.cocacolalife, hora='15:00', duracion=1,
                                orden=1, valor=10)

        fecha = datetime(2013, 07, 2)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=10)

        fecha = datetime(2013, 07, 3)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
                            producto=self.cocacolalife, hora='16:00', duracion=1,
                                orden=1, valor=10)

        fecha = datetime(2013, 07, 4)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=10)
        
        self.client.login(username="staff", password="xx")
        self.client.get(reverse(home))

    def test_descuentos_en_un_solo_medio(self):
        """
            Testeo que los descuentos aplicados a un medio funcionen
            en las opciones de la izquierda
        """
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # setear un periodo que incluya a las 4 emisiones
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,7,1)
        fecha_hasta=datetime(2013,7,4)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        # tenemos un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal10)
        descuento.descuento = 50
        descuento.save()

        # Busco opciones para el filtro "soporte"
        # Debería tirar en "TV aire" una inversión total de $35
        data = {}
        data['data'] = "soporte"
        response = self.client.get(reverse(get_options), data)

        # El descuento debería aplicarse sólo para emisiones
        # en canal 10, o sea que dos d las cuatro emisiones bajan a $5 c/u
        # y el total queda en $10 + $10 + $5 + $5 = $35
        expected_value = '"valor": 30.0'

        self.assertContains(response, expected_value)

    def test_descuentos_en_variacion_periodos(self):
        """ Al hacer una comparación entre periodos, chequeo que
            la variación porcentual que se muestra tenga en cuenta los descuentos
        """
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # voy a tener un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal12)
        descuento.descuento = 50
        descuento.save()

        # seteo una comparación entre periodos
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,7,1)
        fecha_hasta=datetime(2013,7,2)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        fecha_desde=datetime(2013,7,3)
        fecha_hasta=datetime(2013,7,4)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        # Busco opciones para el filtro "soporte"
        # Debería tirar en "TV aire"
        #   * una inversión de $10 en el primer periodo
        #       (por los descuentos en el 10 queda $5 + $5)
        #   * y de $20 en el segundo (sin descuentos es $10 + $10)
        data = {}
        data['data'] = "soporte"
        response = self.client.get(reverse(get_options), data)

        # la inversión en el primer periodo es de 10 (50% desc en el 10)
        # aumenta a 20 en el segundo periodo
        # la variacion procentual es del 100%
        expected_value = '"variacion_porcentual": "100"'

        self.assertContains(response, expected_value)

    def test_en_porcentaje(self):
        """
            el total esta siendo calculado bien, con o sin descuentos
        """
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # voy a tener un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal12)
        descuento.descuento = 50
        descuento.save()

        # seteo una comparación entre periodos
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,7,1)
        fecha_hasta=datetime(2013,7,4)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        # Busco opciones para el filtro "soporte"
        # Debería tirar en "TV aire"
        #   * una inversión de $10 en el primer periodo
        #       (por los descuentos en el 10 queda $5 + $5)
        #   * y de $20 en el segundo (sin descuentos es $10 + $10)
        data = {}
        data['data'] = "medio"
        response = self.client.get(reverse(get_options), data)

        # la inversión en el primer periodo es de 10 (50% desc en el 10)
        # aumenta a 20 en el segundo periodo
        # la variacion procentual es del 100%
        # porcentaje para canal 12
        expected_value = '"porcentaje": "33.3"'
        self.assertContains(response, expected_value)

        # porcentaje para canal 10
        expected_value = '"porcentaje": "66.7"'
        self.assertContains(response, expected_value)

    def test_descuentos_en_grafico_simple(self):
        """
        """
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # tenemos un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal10)
        descuento.descuento = 50
        descuento.save()

        # setear un periodo que incluya a las 4 emisiones
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,7,1)
        fecha_hasta=datetime(2013,7,4)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        response = self.client.get('/app/graph/')

        expected_json = """
                            [[["date", "inversion"],
                            ["01/07/2013", 5],
                            ["02/07/2013", 5],
                            ["03/07/2013", 10],
                            ["04/07/2013", 10]], "simple","dia",1]
                        """
        self.assertEqual(remove_whitespace(expected_json),
                         remove_whitespace(response.content))

    # FIXME
    # def test_descuentos_en_grafico_comparado(self):
    #     """
    #     """
    #     perfil = self.root.get_profile()
    #     perfil.set_unit("inversion")
    #     for p in Periodo.objects.filter(perfil=perfil):
    #         p.delete()

    #     ct_medio = ContentType.objects.get(name='medio')
    #     filtro_medio = Filtro.objects.create(perfil=perfil, tipo=ct_medio)

    #     # creo algunas emisiones más
    #     fecha = datetime(2013, 07, 5)
    #     Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
    #                         producto=self.cocacolalife, hora='16:00', duracion=1,
    #                             orden=1, valor=30)

    #     fecha = datetime(2013, 07, 5)
    #     Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
    #                         producto=self.chizitos, hora='16:00', duracion=1,
    #                             orden=1, valor=20)

    #     # setear comparación
    #     data = {}
    #     data['filter'] = "medio"
    #     data['value'] = self.canal10.id
    #     self.client.get(reverse(change_filter), data)

    #     data['value'] = self.canal12.id
    #     self.client.get(reverse(add_filter), data)

    #     # tenemos un descuento del 50% en canal 10
    #     descuento = Descuento.objects.get(user=self.root, medio=self.canal10)
    #     descuento.descuento = 50
    #     descuento.save()

    #     # setear un periodo que incluya a las 6 emisiones
    #     init_fecha_desde=datetime(1982, 9, 21)
    #     init_fecha_hasta=datetime(2082, 9, 21)

    #     fecha_desde=datetime(2013,7,1)
    #     fecha_hasta=datetime(2013,7,5)
    #     Periodo.objects.create(perfil=perfil,
    #         fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
    #         init_fecha_desde=init_fecha_desde,
    #         init_fecha_hasta=init_fecha_hasta)

    #     data = {}
    #     data['filter'] = "medio"
    #     response = self.client.get(reverse(compare_filter), data)

    #     response = self.client.get('/app/graph/')
    #     expected_json = """
    #                     [
    #                         [["date", "Canal 10", "Canal 12"],
    #                         ["01/07/2013", 5, 0],
    #                         ["02/07/2013", 5, 0],
    #                         ["03/07/2013", 0, 10],
    #                         ["04/07/2013", 0, 10],
    #                         ["05/07/2013", 10, 30]
    #                     ],
    #                     "opciones_filtro"]
    #                     """
    #     self.assertEqual(remove_whitespace(expected_json),
    #                      remove_whitespace(response.content))

    #     # le pongo un descuento también al 12
    #     descuento = Descuento.objects.get(user=self.root, medio=self.canal12)
    #     descuento.descuento = 30
    #     descuento.save()

    #     response = self.client.get('/app/graph/')
    #     expected_json = """
    #                     [
    #                         [["date", "Canal 10", "Canal 12"],
    #                         ["01/07/2013", 5, 0],
    #                         ["02/07/2013", 5, 0],
    #                         ["03/07/2013", 0, 7],
    #                         ["04/07/2013", 0, 7],
    #                         ["05/07/2013", 10, 21]
    #                     ],
    #                     "opciones_filtro"]
    #                     """
    #     self.assertEqual(remove_whitespace(expected_json),
    #                      remove_whitespace(response.content))

    def test_descuentos_en_unit_selector(self):
        """
            Testeo que los descuentos aplicados a un medio funcionen
            en el selector de unidad
        """
        perfil = self.root.get_profile()
        perfil.set_unit("inversion")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # setear un periodo que incluya a las 4 emisiones
        init_fecha_desde=datetime(1982,9,21)
        init_fecha_hasta=datetime(2082,9,21)

        fecha_desde=datetime(2013,7,1)
        fecha_hasta=datetime(2013,7,4)
        Periodo.objects.create(perfil=perfil,
            fecha_desde=fecha_desde, fecha_hasta=fecha_hasta,
            init_fecha_desde=init_fecha_desde,
            init_fecha_hasta=init_fecha_hasta)

        # tenemos un descuento del 50% en canal 10
        descuento = Descuento.objects.get(user=self.root, medio=self.canal10)
        descuento.descuento = 50
        descuento.save()

        # El descuento debería aplicarse sólo para emisiones
        # en canal 10, o sea que dos d las cuatro emisiones bajan a $5 c/u
        # y el total queda en $10 + $10 + $5 + $5 = $35
        perfil.set_unit("inversion")
        response = self.client.get(reverse(unit_selector))
        expected_value = '"value": "30.0"'


# Arreglar!

@mock.patch('adkiller.filtros.views.date', FakeDate)
@mock.patch('adkiller.filtros.models.date', FakeDate)
class GraphDataGenerationTest(InfoadTestCase):
    """Tests para la generación de data para el gráfico
        (que luego es parseada y renderizada por la Google Visualization API)
    """
    # FIXME: escribir setUp común
    def setUp(self):
        InfoadTestCase.setUp(self)

        # creo emisiones
        fecha = datetime(2013, 07, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=15)

        fecha = datetime(2013, 07, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=5)

        fecha = datetime(2013, 07, 02)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=5)

        fecha = datetime(2013, 07, 03)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=5)

        # crear más emisiones 
        fecha = datetime(2013, 8, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=1)

        Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=3)

        fecha = datetime(2013, 8, 02)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=1)

        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='16:00', duracion=1,
                                orden=1, valor=1)

        Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=2)

        fecha = datetime(2013, 8, 03)
        Emision.objects.create(fecha=fecha, horario=self.horario_del_10,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=3)

        Emision.objects.create(fecha=fecha, horario=self.horario_del_12,
                            producto=self.chizitos, hora='15:00', duracion=1,
                                orden=1, valor=1)

        # Me logueo como root
        self.client.login(username="non_staff", password="yy")

        # Cambio las fechas init para q tome los datos creados en 2013
        # (Los creamos en distinto año que los datos de InfoadTestcase
        # para que no se pisen)
        perfil_cliente = self.usuario.get_profile()
        periodo = perfil_cliente.get_periodo_or_create()
        periodo.init_fecha_desde = datetime(2013, 1, 1)
        periodo.init_fecha_hasta = datetime(2013, 12, 31)
        periodo.save()
        perfil_cliente.save()

    def test_grafico_simple_un_dia(self):
        """ Test base para el grafico con el periodo == dia ,
            verifico que cambie el grafico cuando cambio
            la unidad en la que lo estoy viendo
        """
        perfil = self.usuario.get_profile()

        # setear el periodo (comprende dos emisiones el 1/7 y una para el 2/7 y el 3/7)
        periodo = perfil.get_periodo_or_create()
        periodo.fecha_desde=datetime(2013,8,2)
        periodo.fecha_hasta=datetime(2013,8,2)
        periodo.save()

        response = self.client.get(reverse(graph))
        data = simplejson.loads(response.content)

        # remuevo el header
        rows = data[0][1:]
        self.assertEqual(len(rows),17)
        self.assertEqual(rows[15][1],2)
        self.assertEqual(rows[16][1],1)

        data = {}
        data['unit'] = "volumen"
        response = self.client.get(reverse(set_unit),data)
        response = self.client.get(reverse(graph))
        data = simplejson.loads(response.content)

        rows = data[0][1:]
        self.assertEqual(len(rows),17)
        self.assertEqual(rows[15][1],2)
        self.assertEqual(rows[16][1],1)

        data = {}
        data['unit'] = "inversion"
        response = self.client.get(reverse(set_unit),data)
        response = self.client.get(reverse(graph))
        data = simplejson.loads(response.content)
        print data
        rows = data[0][1:]
        self.assertEqual(len(rows),17)
        self.assertEqual(rows[15][1],3)
        self.assertEqual(rows[16][1],1)

    def test_grafico_simple_varios_dias(self):
        """
        """
        # crear un perfil para el usuario actual
        perfil = self.usuario.get_profile()
        perfil.set_unit("inversion")

        # setear el periodo (comprende dos emisiones el 1/7 y una para el 2/7 y el 3/7)
        periodo = perfil.get_periodo_or_create()
        periodo.fecha_desde=datetime(2013,7,1)
        periodo.fecha_hasta=datetime(2013,7,3)
        periodo.save()

        response = self.client.get('/app/graph/')
        
        expected_json = """
                            [[["date", "inversion"],
                            ["01/07/2013", 20],
                            ["02/07/2013", 5],
                            ["03/07/2013", 5]], "simple",
                            "dia",1]
                        """
        self.assertEqual(remove_whitespace(expected_json),
                         remove_whitespace(response.content))

    def test_grafico_simple_una_semana(self):
        """ Test base para el grafico con el periodo == semana ,
            verifico que cambie el grafico cuando cambio
            la unidad en la que lo estoy viendo
        """
        pass
        # FIXME: 
        # - completar de forma similar a test_grafico_simple_un_dia
        # - agregar mock de fechas para q el ultimo mes tome los datos del testcase

        # data = {}
        # data['filter'] = "periodo"
        # data['value'] = "semana"
        # crear un perfil para el usuario actual

    def test_grafico_simple_tres_semanas(self):
        pass

    def test_grafico_simple_un_mes(self):
        """ Test base para el grafico con el periodo == mes ,
            verifico que cambie el grafico cuando cambio
            la unidad en la que lo estoy viendo
        """
        pass
        # FIXME: 
        # - completar de forma similar a test_grafico_simple_un_dia
        # - agregar mock de fechas para q el ultimo mes tome los datos del testcase

        # data = {}
        # data['filter'] = "periodo"
        # data['value'] = "mes"

    def test_grafico_simple_cuatro_meses(self):
        pass

    def test_grafico_comparando_dos_opciones(self):
        # cargar el perfil de usuario
        perfil = self.usuario.get_profile()
        perfil.set_unit("inversion")
        ct_medio = ContentType.objects.get(name='medio')
        filtro_medio = Filtro.objects.create(perfil=perfil, tipo=ct_medio)

        # setear el periodo
        periodo = perfil.get_periodo_or_create()
        periodo.fecha_desde=datetime(2013,8,1)
        periodo.fecha_hasta=datetime(2013,8,3)
        periodo.save()

        # setear comparación
        data = {}
        data['filter'] = "medio"
        data['value'] = self.canal10.id
        self.client.get(reverse(add_filter), data)

        data['value'] = self.canal12.id
        self.client.get(reverse(add_filter), data)

        data = {}
        data['filter'] = "medio"
        response = self.client.get(reverse(compare_filter),data)

        response = self.client.get('/app/graph/')

        expected_json = """
                        [
                            [["date", "canal 10", "canal 12"],
                            ["01/08/2013", 1, 3],
                            ["02/08/2013", 2, 2],
                            ["03/08/2013", 3, 1]],
                            "opciones_filtro",
                            "dia",1]
                        """
        self.assertEqual(remove_whitespace(expected_json),
                         remove_whitespace(response.content))

    def test_grafico_comparando_tres_opciones(self):
        pass

    def test_grafico_comparando_dos_periodos(self):
      pass

    def test_grafico_rango(self):
        #agrego una fecha ficticia
        FakeDate.today = classmethod(lambda cls: date(2013, 12, 31))

        # Creo filtros para el perfil de usuario
        self.usuario.get_profile().crear_filtros()
        
        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")
        
        data = {}

        # Por mes
        data['filter'] = "periodo"
        data['value'] = "mes"

        self.client.get(reverse(add_filter), data)
        self.assertEqual(Periodo.objects.filter(perfil=self.usuario.get_profile())[0].lapso, 'M')

        response = self.client.get(reverse(graph))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(data_busquedas[2], 'dia')

        # Por dia
        data['filter'] = "periodo"
        data['value'] = "dia"

        self.client.get(reverse(add_filter), data)

        response = self.client.get(reverse(graph))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(data_busquedas[2], 'hora')

        # Por año
        data['filter'] = "periodo"
        data['value'] = "A"

        self.client.get(reverse(add_filter), data)

        response = self.client.get(reverse(graph))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(data_busquedas[2], 'mes')

        # FIXME: No funciona
        # Por semana
        #data['filter'] = "periodo"
        #data['value'] = "semana"

        #self.client.get(reverse(add_filter), data)

        #response = self.client.get(reverse(graph))
        #data_busquedas = simplejson.loads(response.content)
        #self.assertEqual(data_busquedas[2], 'dia')

        # Menos de 4 meses
        data['filter'] = "fecha_desde"
        data['value'] = "01/05/2013"
        self.client.get(reverse(add_filter), data)
        data['filter'] = "fecha_hasta"
        data['value'] = "01/07/2013"
        self.client.get(reverse(add_filter), data)

        response = self.client.get(reverse(graph))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(data_busquedas[2], 'dia')

        # FIXME: no funciona
        # Mas de 4 meses
        data['filter'] = "fecha_desde"
        data['value'] = "01/05/2013"
        self.client.get(reverse(add_filter), data)
        data['filter'] = "fecha_hasta"
        data['value'] = "01/10/2013"
        self.client.get(reverse(add_filter), data)

        response = self.client.get(reverse(graph))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(data_busquedas[2], 'semana')



class PermisosTest(InfoadTestCase):
    """
         Tests para el manejo de permisos según el usuario sea staff o no-staff
         y según sus filtros iniciales
    """
    def setUp(self):
        InfoadTestCase.setUp(self)
        # Me logueo como root
        self.client.login(username="staff", password="xx")

    def test_add_filter(self):
        self.client.logout()
        self.client.login(username="non_staff", password="yy")
        self.client.get(reverse(home))

        data = {}
        data['filter'] = "marca"
        data['value'] = "1"
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(OpcionFiltro.objects.all().count(), 1)
        #Cambio de response code 200 a 302 (Fernando)
        self.assertEqual(response.status_code, 302)

    def test_guardar_permiso(self):
        """ 
            * staff le asigna un filtro a non_staff
            * non_staff tiene ese filtro
            * non_staff no puede borrar ese filtro
        """
        self.client.logout()
        # staff le asigna un filtro a non_staff
        self.client.login(username="staff", password="xx")
        self.client.get(reverse(home))
 
        data = {'filter': 'sector', 'value': '1'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))
        id = self.usuario.id
        data = {'package-accion': 'Guardar', 'package-user': id,'package': 'Enviar'}
        response = self.client.post(reverse(home), data)
        self.assertEqual(response.status_code, 302)
        self.client.logout()

        # entro como non_staff y veo que tengo ese filtro como inicial
        self.client.login(username="non_staff", password="yy")

        response = self.client.get(reverse(home))
        self.assertEqual(response.status_code, 200)
        perfil = self.usuario.get_profile()
        filtro = perfil.get_filtro(key='sector')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)

        self.assertEqual(len(opciones), 1)
        self.assertTrue(opciones[0].inicial)

        # intento borrar ese filtro asignado, no deberia poder eliminarlo
        data = {'filter':'sector','id':'1'}
        response = self.client.get(reverse(remove_filter),data)

        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        
        #verifico que no se ha borrado el filtro asignado
        self.assertEqual(len(opciones), 1)
        self.assertTrue(opciones[0].inicial)

        ##### PRUEBA CON RESET
        
        #agrego un filtro dentro del sector
        data = {'filter': 'marca', 'value': '1'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))
        #agrego otro filtro dentro del sector
        data = {'filter': 'marca', 'value': '2'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))

        #reset de filtros
        response = self.client.get(reverse(reset_filters))
        self.assertEqual(response.status_code, 200)

        #verifico que los filtros por marca no esten asignados
        filtro = perfil.get_filtro(key='marca')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        self.assertEqual(len(opciones), 0)
        
        #verifico que el filtro inicial este asignado    
        filtro = perfil.get_filtro(key='sector')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        self.assertEqual(len(opciones), 1)
        self.assertTrue(opciones[0].inicial)

    def test_cargar_permiso_out_of_range(self):
        """
            * non_staff tiene un filtro
            * non_staff agrega un filtro fuera del rango asignado
            * non_staff no deberia poder agregar un filtro fuera de rango
        """
        self.client.login(username="staff", password="xx")
        self.client.get(reverse(home))

        #creo un filtro para non_staff
        data = {'filter': 'marca', 'value': '1'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))
        
        id = self.usuario.id
        data = {'package-accion': 'Guardar', 'package-user': id,'package': 'Enviar'}
        response = self.client.post(reverse(home), data)
        self.assertEqual(response.status_code, 302)

        #verifico que non_staff tenga un filtro
        perfil = self.usuario.get_profile() 
        filtro = perfil.get_filtro(key='marca')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        
        self.assertEqual(len(opciones),1)
        
        self.client.logout()
        #login como non_staff
        user_login = self.client.login(username="non_staff", password="yy")
        self.client.get(reverse(home))
        #trato de agregar un filtro fuera del actual 
        data = {'filter': 'marca', 'value': '2'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))
       
        #verifico que non_staff tenga un filtro
        perfil = self.usuario.get_profile() 
        filtro = perfil.get_filtro(key='marca')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        self.assertEqual(len(opciones),1, "Ojo que esto todavia no esta corregido")
        #FIXME Corregir el filtro fuera de rango

    def test_asignar_no_staff(self):
        """ 
            non_staff intenta asignarse filtros
            non_staff no tiene filtros en principio
            non_staff tiene acceso a toda la plataforma
        """
        #verifico que non_staff no tenga filtros
        perfil = self.usuario.get_profile() 
        filtro = perfil.get_filtro(key='marca')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        
        self.assertEqual(len(opciones),0)

        user_login = self.client.login(username="non_staff", password="yy")
        self.client.get(reverse(home))
 
        data = {'filter': 'marca', 'value': '1'}
        response = self.client.get(reverse(add_filter), data)
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse(home))
       
        #verifico que non_staff tenga filtros
        perfil = self.usuario.get_profile() 
        filtro = perfil.get_filtro(key='marca')
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        self.assertEqual(len(opciones),1)


# class GetProgramaViewTest(InfoadTestCase):
#     """Tests para la vista app.views.get_programa"""
#     def setUp(self):
#         InfoadTestCase.setUp(self)
#         # Me logueo como root
#         self.client.login(username="staff", password="xx")

#     def test_programa_que_existe(self):
#         """Deberia devolver el programa correspondiente al horario,
#             medio y fecha pasados"""
#         # en esta fecha el programa ya esta dado de alta
#         # y se transmite a esa hora en ese medio
#         response = self.client.get('/app/get_programa/', {'hora': "12:30",
#                             'fecha': "01/08/2012", 'medio': self.medio.pk})

#         expected_output = '{"programa": %s}' % self.programa.pk
#         self.assertEqual(response.content, expected_output)

#     def test_programa_que_no_existe(self):
#         """Deberia devolver vacio porque no existe un programa
#            para el horario, medio y fecha pasados"""
#         response = self.client.get('/app/get_programa/', {'hora': "11:00",
#                             'fecha': "01/08/2012", 'medio': self.medio.pk})
#         self.assertEqual(response.content, '{}')

#     def test_medio_que_no_existe(self):
#         """Deberia devolver vacio porque no existe el medio"""
#         id_medio_inexistente = self.medio.pk + 1
#         response = self.client.get('/app/get_programa/', {'hora': "11:00",
#                         'fecha': "01/08/2012", 'medio': id_medio_inexistente})
#         self.assertEqual(response.content, '{}')

#     def test_formulario_invalido(self):
#         """Deberia devolver vacio si el formulario es invalido"""
#         response = self.client.get('/app/get_programa/', {'hora': "1100",
#                                    'fecha': "01082012", 'medio': "breakthis"})
#         self.assertEqual(response.content, '{}')


# class GetJSONAnunciantesTest(InfoadTestCase):
#     """

#     """
#     def test_view_returns_200(self):
#         resp = self.client.get(reverse('get_json_anunciantes', query='blah'))
#         self.assertEqual(200, resp.status_code)

#     def test_view_returns_json(self):
#         resp = self.client.get(reverse('get_json_anunciantes', query='blah'))
#         self.assertEqual('application/json', resp.mimetype)

#     def test_view_returns_anunciantes(self):
#         """Should return json object with matched Anunciante.nombre"""
#         Anunciante.objects.create(nombre='blah1', id=1)
#         Anunciante.objects.create(nombre='blah2', id=2)
#         Anunciante.objects.create(nombre='should not appear', id=3)
#         resp = self.client.get(reverse('get_json_anunciantes', query='blah'))
#         expected_json = \
#                 '[{"nombre": "blah1", "id": 1}, {"nombre": "blah2", "id": 2}]'
#         self.assertEqual(expected_json, resp.content)

# class MigrateTest(InfoadTestCase):
#     """

#     """
#     def test_migrate(self):
#         base = "adkiller/app/features/"
#         lineas = ["server1.xls", "server2.xls", "server15.xls"]
#         servers = [3, 9, 15]
#         archivos = []
#         for elem in lineas:
#             archivos.append(open(base + elem))

#         contador = 0
#         for i in range(3):
#             self.assertEqual(Migracion.objects.all().count(), contador)
#             parse_csv(archivos[i], servers[i])
#             contador += 1
#         self.assertEqual(Migracion.objects.all().count(), 3)

#         self.assertNotEqual(Marca.objects.all().count, 0)

#         CIUDADES = ["Buenos Aires", "Chaco", "Corrientes", "Mendoza",
#             "Buenos Aires Cable", "Córdoba"]

#         ciudades = Ciudad.objects.all()
#         for elem in ciudades:
#             assert str(elem) in CIUDADES, str(elem) + ": no es una ciudad"
#         self.assertNotEqual(Sector.objects.all().count(), 0)
#         self.assertNotEqual(Medio.objects.all().count(), 0)
#         self.assertNotEqual(Rubro.objects.all().count(), 0)

#         soportes = Soporte.objects.all()
#         for soporte in soportes:
#             assert str(soporte) in ["Diario", "Televisión", "Radio"],\
#                                         str(soporte) + ": no es un soporte"

# class AudioVisualInlineFormTest(InfoadTestCase):
#     """

#     """
#     def test_form_no_instance(self):
#         """Form should not have an empty queryset for anunciantes field
#         when instantiated without an initial instance"""
#         form_data = {'instance': None}
#         form = AudioVisualInlineForm(data=form_data)
#         self.assertEqual(form.fields['anunciantes'].queryset,
#                                         Anunciante.objects.none())


#class TestTraerOrigenes(InfoadTestCase):
#    """
#    """
#    def test_regex(self):
#        from adkiller.management.commands.traer_origenes import ARCHVIOS_REGEX
#        test_input = '<a href="test.flv"></a> bla' \
#                     '<a href="test.mp3"></a><a href="test.h264"></a>' \
#                     'bla <a href="test.mp4"></a>' \
#                     'bla <a href="test.bla"></a>'
#        expected_output = ['test.flv', 'test.mp3', 'test.h264', 'test.mp4']
#        self.assertEqual(expected_output, ARCHVIOS_REGEX.findall(test_input))

class TestLapso(TestCase):

    def test_lapsos_iguales_en_el_mismo_dia(self):
       l1 = Lapso(time(12,0), time(12, 59, 59))
       l2 = Lapso(time(12,0), time(12, 59, 59))
       self.assertTrue(l1.solapa(l2))
       self.assertTrue(l2.solapa(l1))

    def test_solapados_a_antes_que_b(self):
       a = Lapso(time(12,0), time(13, 59, 59))
       b = Lapso(time(13,0), time(15, 59, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_solapados_b_antes_que_a(self):
       a = Lapso(time(13,0), time(15, 59, 59))
       b = Lapso(time(12,0), time(13, 59, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_solapados_a_dentro_de_b(self):
       a = Lapso(time(12,0), time(12, 29, 59))
       b = Lapso(time(11,0), time(12, 59, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_solapados_b_dentro_de_a(self):
       a = Lapso(time(11,0), time(12, 59, 59))
       b = Lapso(time(12,0), time(12, 29, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_NO_solapados_a_antes_que_b(self):
       a = Lapso(time(9,0), time(9, 29, 59))
       b = Lapso(time(11,0), time(12, 59, 59))
       self.assertFalse(a.solapa(b))
       self.assertFalse(b.solapa(a))

    def test_NO_solapados_b_antes_que_a(self):
       a = Lapso(time(11,0), time(12, 59, 59))
       b = Lapso(time(9,0), time(9, 29, 59))
       self.assertFalse(a.solapa(b))
       self.assertFalse(b.solapa(a))

    def test_distinto_dia_solapa_empieaza_antes(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(22,0), time(23, 29, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_distinto_dia_solapa_adentro(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(23,30), time(23, 44, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_distinto_dia_solapa_adentro_dia_siguiente(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(00,30), time(00, 44, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_distinto_dia_solapa_termina_despues_dia_siguiente(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(00,30), time(01, 29, 59))
       self.assertTrue(a.solapa(b))
       self.assertTrue(b.solapa(a))

    def test_distinto_dia_no_solapa_antes(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(20,30), time(21, 29, 59))
       self.assertFalse(a.solapa(b))
       self.assertFalse(b.solapa(a))

    def test_distinto_dia_no_solapa_antes(self):
       a = Lapso(time(23,0), time(00, 59, 59))
       b = Lapso(time(02,30), time(03, 29, 59))
       self.assertFalse(a.solapa(b))
       self.assertFalse(b.solapa(a))


from django.test import Client

class TestViewUltimaEmision(TestCase):
    def setUp(self):
        self.client = Client()
        tipo = TipoPublicidad(nombre=1, descripcion='Comercial')
        tipo.save()
        rubro = Rubro(nombre="rubro")
        rubro.save()
        industria = Industria(nombre="industria", rubro=rubro)
        industria.save()
        sector = Sector(nombre="sector", industria=industria)
        sector.save()
        self.marca = Marca(nombre="id2", sector=sector)
        self.marca.save()

        self.p = Producto(nombre='nombre', marca=Marca.objects.get(nombre="id2"))
        self.p.save()

        self.audiovisual = AudioVisual(fecha='2014-02-14', producto=self.p)
        self.audiovisual.save()

    def test_sin_parametro(self):
        response = self.client.get('/app/ultima_emision')
        content = json.loads(response.content)
        self.assertFalse(content['todo_bien'])

    def test_un_parametro_p(self):
        response = self.client.get('/app/ultima_emision/p=hjkk')
        content = json.loads(response.content)
        self.assertFalse(content['todo_bien'])

    def test_un_parametro_m(self):
        response = self.client.get('/app/ultima_emision/m=hjkk')
        content = json.loads(response.content)
        self.assertFalse(content['todo_bien'])

    def test_emision_sin_datos(self):
        response = self.client.get('/app/ultima_emision/p=hjkk&m=jasdf')
        content = json.loads(response.content)
        self.assertFalse(content['todo_bien'])

    def test_ultima_emision(self):
        response = self.client.get('/app/ultima_emision', {'m': self.marca.nombre, 'p': self.p.nombre})
        self.assertEqual(response.status_code, 200)
        content = json.loads(response.content)
        self.assertEqual(content['ultima_emision'], self.audiovisual.fecha)

from django.core.management import call_command
class TestCrearMp4(TestCase):
    def setUp(self):
        tipo = TipoPublicidad(nombre=1, descripcion='Comercial')
        tipo.save()
        rubro = Rubro(nombre="rubro")
        rubro.save()
        industria = Industria(nombre="industria", rubro=rubro)
        industria.save()
        sector = Sector(nombre="sector", industria=industria)
        sector.save()
        self.marca = Marca(nombre="id2", sector=sector)
        self.marca.save()


        # patron 2 con emisiones recientes
        self.producto2 = Producto(nombre='producto2', marca=Marca.objects.get(nombre="id2"))
        self.producto2.save()
        self.patron2 = Patron(producto=Producto.objects.get(nombre='producto2'), server=Servidor.objects.create(nombre="Barney",nro='1'))
        self.patron2.save()
        self.av_nuevo = AudioVisual(fecha=datetime.today(), producto=self.producto2)
        self.av_nuevo.save()


    def test_seleccion_correcta(self):
        argumentos = ['test']
        call_command('cortar_spot_mp4', *argumentos)
        patron2 = Patron.objects.get(producto=Producto.objects.get(nombre='producto2'))
        self.assertEqual(patron2.mp4_filename, 'Creado')

class TestCargarHorariosDTV(TestCase):

    def setUp(self):
        # programas y horarios
        self.prreg = Programa.objects.create(nombre='Programación Regular', identificador='PRRE')
        fechaAlta = date(2000, 01, 01)
        horaInicio = time(0, 0, 0)
        horaFin = time(23, 59, 59)
        dias = '1,1,1,1,1,1,1'
        # medio
        ciudad = Ciudad.objects.create(nombre='Cordoba')
        soporte = Soporte.objects.create()
        self.medio_DTV = Medio.objects.create(nombre = "medio test DTV",
                            identificador = 'MTDTV', identificador_directv = 'MTDTV',
                            ciudad = ciudad, soporte=soporte, dvr='test', canal='1')
        self.PRRE = Horario.objects.filter(programa__nombre='Programación Regular')[0]
        self.los_simpson = Programa.objects.create(nombre='Los Simpson', identificador='SPS')
        self.los_simpson_domingo = Horario.objects.create(medio=self.medio_DTV, programa=self.los_simpson,
                                        dias='1,0,0,0,0,0,0', horaInicio='21:00:00', horaFin='21:29:59',
                                        fechaAlta=date(2010, 10, 10))
        # archivos
        from django.conf import settings
        self.directorio_archivos = os.path.join(settings.PROJECT_PATH, 'adkiller/app/fixtures/horarios/')
        self.archivo_canal13 = self.directorio_archivos + '/canal13_20140612.dat'
        self.archivo_canal13_con_AR132 = self.directorio_archivos + '/canal132_20140612.dat'
        self.archivo_con_errores = self.directorio_archivos + '/archivo_con_errores.dat'
        self.archivo_migrar = self.directorio_archivos + '/Migrar_2014-06-27_05.dat'
        self.programacion_canal_13 = [
            'AR13|Hogar shopping club|2014-06-12 06:00:00|2014-06-12 06:30:00',
            'AR13|Agro Síntesis|2014-06-12 06:30:00|2014-06-12 06:45:00',
            'AR13|Tiempo del tiempo|2014-06-12 06:45:00|2014-06-12 07:00:00',
            'AR13|Arriba Argentinos|2014-06-12 07:00:00|2014-06-12 09:30:00',
            'AR13|Panam y Circo|2014-06-12 09:30:00|2014-06-12 10:30:00',
            'AR13|Piñón en familia|2014-06-12 10:30:00|2014-06-12 11:30:00',
            'AR13|Zorro|2014-06-12 11:30:00|2014-06-12 13:00:00',
            'AR13|El Noticiero del Trece|2014-06-12 13:00:00|2014-06-12 14:30:00',
            'AR13|El Diario de Mariana|2014-06-12 14:30:00|2014-06-12 16:45:00',
            'AR13|Éste es el Show|2014-06-12 16:45:00|2014-06-12 18:45:00',
            'AR13|A todo o nada|2014-06-12 18:45:00|2014-06-12 20:00:00',
            'AR13|Telenoche|2014-06-12 20:00:00|2014-06-12 21:00:00',
            'AR13|Mis amigos de siempre|2014-06-12 21:00:00|2014-06-12 21:45:00',
            'AR13|Guapas|2014-06-12 21:45:00|2014-06-12 22:30:00',
            'AR13|Showmatch|2014-06-12 22:30:00|2014-06-13 00:15:00',
            'AR13|En síntesis|2014-06-13 00:15:00|2014-06-13 00:45:00',
            'AR13|Hogar shopping club|2014-06-13 00:45:00|2014-06-13 05:30:00',
            'AR13|Hogar shopping club|2014-06-13 05:30:00|2014-06-13 05:59:59'
        ]
        # audiovisuales
        tipo = TipoPublicidad(nombre=1, descripcion='Comercial')
        tipo.save()
        rubro = Rubro(nombre="rubro")
        rubro.save()
        industria = Industria(nombre="industria", rubro=rubro)
        industria.save()
        sector = Sector(nombre="sector", industria=industria)
        sector.save()
        self.marca = Marca(nombre="id2", sector=sector)
        self.marca.save()
        self.producto = Producto(nombre='producto', marca=Marca.objects.get(nombre="id2"))
        self.producto.save()
        self.patron = Patron(producto=Producto.objects.get(nombre='producto'))
        self.patron.save()


    def str_to_time(self, hora):
        hora = datetime.strptime(hora, '%H:%M:%S')
        hora = time(hora.hour, hora.minute, hora.second)
        return hora

    def test_medio_creado_tiene_prre(self):
        horarios = Horario.objects.filter(medio=self.medio_DTV)
        self.assertEqual(len(horarios), 2) # los simpson + prre
        horarios = horarios.filter(programa__nombre__icontains='regular')
        self.assertEqual(horarios[0].programa, self.prreg)
        self.assertFalse(horarios[0].enElAire)

    def test_dia_to_string(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        dias = [ 'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
        for i in range(7):
            self.assertEqual(dias[i], CHDTV.dia_to_string(i))

    def test_genera_dia_string(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        ds = [ '1,0,0,0,0,0,0', '0,1,0,0,0,0,0', '0,0,1,0,0,0,0', '0,0,0,1,0,0,0',
               '0,0,0,0,1,0,0', '0,0,0,0,0,1,0', '0,0,0,0,0,0,1']
        for i in range(7):
            self.assertEqual(ds[i], CHDTV.genera_dia_string(i))

    def test_obtener_programacion_1(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        programacion = CHDTV.obtener_programacion('AR13', self.archivo_canal13)
        #import pdb;pdb.set_trace()
        
        self.assertEqual(len(programacion),len(self.programacion_canal_13))
        for indice in range(len(programacion)):
            c, p, i, f = programacion[indice].split('|')
            cc, pp, ii, ff = self.programacion_canal_13[indice].split('|')
            self.assertEqual(c, cc)
            self.assertEqual(i, ii)
            self.assertEqual(f, ff)

    def test_obtener_programacion_2(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        programacion = CHDTV.obtener_programacion('AR13', self.archivo_canal13_con_AR132)
        self.assertEqual(len(programacion),len(self.programacion_canal_13))
        for indice in range(len(programacion)):
            c, p, i, f = programacion[indice].split('|')
            cc, pp, ii, ff = self.programacion_canal_13[indice].split('|')
            self.assertEqual(c, cc)
            self.assertEqual(i, ii)
            self.assertEqual(f, ff)

    def test_obtener_programacion_canales_con_errores1(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        print self.archivo_canal13
        self.assertFalse(CHDTV.canal_con_errores('AR13', self.archivo_canal13))

    def test_obtener_programacion_canales_con_errores2(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        self.assertFalse(CHDTV.canal_con_errores('AR13', self.archivo_canal13_con_AR132))

    def test_obtener_programacion_canales_con_errores3(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        self.assertTrue(CHDTV.canal_con_errores('AR132', self.archivo_con_errores))

    def test_unificar_si_se_puede_1(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        # le paso un horario para crear cuando ya se emite otro dia: debe unificar
        canal = self.medio_DTV
        dias = ['1,0,0,0,0,0,0', '1,1,0,0,0,0,0', '1,1,1,0,0,0,0', '1,1,1,1,0,0,0',
                '1,1,1,1,1,0,0', '1,1,1,1,1,1,0', '1,1,1,1,1,1,1']
        for i in range(1, 7):
            dia, titulo_esp, inicio_arg_time, fin_arg_time = i, 'Los Simpson', '21:00:00', '21:29:59'
            resultado = CHDTV.unificar_si_se_puede(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, self.los_simpson_domingo.fechaAlta)
            self.assertEqual(resultado, self.los_simpson_domingo)
            self.assertEqual(resultado.dias, dias[i])

    def test_unificar_si_se_puede_2(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        # le paso un horario para crear y no debería poder unificar
        canal = self.medio_DTV
        dia, titulo_esp, inicio_arg_time, fin_arg_time = 0, 'Los Simpson', '20:00:00', '21:29:59'
        resultado = CHDTV.unificar_si_se_puede(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, self.los_simpson_domingo.fechaAlta)
        self.assertNotEqual(resultado, self.los_simpson_domingo)
        self.assertFalse(resultado)

    def test_cargar_horario_si_necesario(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        from adkiller.app.utils import Lapso
        # Para canal 13, primero limpio la base de datos
        Horario.objects.all().delete()
        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV, enElAire=True)
        self.assertEqual(len(horarios_actuales), 0)
        # creo un horario de los simpsons los lunes de 20 a 21
        los_simpson_lunes = Horario.objects.create(medio=self.medio_DTV, programa=self.los_simpson,
                                        dias='0,1,0,0,0,0,0', horaInicio='21:00:00', horaFin='21:29:59',
                                        fechaAlta='2010-10-10')
        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV, enElAire=True)
        self.assertEqual(len(horarios_actuales), 1)
        # "migro" un horario que solapa
        dia = 1
        inicio_arg_time = self.str_to_time('20:00:00')
        fin_arg_time = self.str_to_time('21:29:59')
        lapso_DTV = Lapso(inicio_arg_time, fin_arg_time)
        titulo_esp = 'El Mago de Oz'
        canal = self.medio_DTV
        fecha_archivo_dtv = '2014-06-27'
        CHDTV.cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha_archivo_dtv)

        los_simpson_lunes = Horario.objects.filter(programa=self.los_simpson)[0]

        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV)
        self.assertEqual(horarios_actuales[0].programa.nombre, titulo_esp)
        # ahora "migro" un horario que coincide (es el mismo), no haría falta migrarlo
        CHDTV.cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha_archivo_dtv)

        horarios_actuales_2 = Horario.objects.filter(medio=self.medio_DTV)
        for i in range(len(horarios_actuales_2)):
            self.assertEqual(horarios_actuales[i], horarios_actuales_2[i])


    def test_cambiar_emisiones_de_horario(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        from adkiller.app.utils import Lapso
        # Para canal 13, primero limpio la base de datos
        Horario.objects.all().delete()
        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV)
        # creo un horario de los simpsons los lunes de 20 a 21
        los_simpson_lunes = Horario.objects.create(medio=self.medio_DTV, programa=self.los_simpson,
                                        dias='0,1,0,0,0,0,0', horaInicio='21:00:00', horaFin='21:29:59',
                                        fechaAlta='2010-10-10')
        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV)

        # asigno una emision de ese horario
        self.av = AudioVisual(fecha=datetime(2014, 06, 27), hora='21:01:30', producto=self.producto, )
        self.av.horario = horarios_actuales[0]
        self.av.save()
        audiovisuales = AudioVisual.objects.filter(horario=horarios_actuales[0])
        self.assertEqual(len(audiovisuales), 1)
        self.assertEqual(audiovisuales[0], self.av)
        # "migro" un horario que solapa
        dia = 1
        inicio_arg_time = self.str_to_time('20:00:00')
        fin_arg_time = self.str_to_time('21:29:59')
        lapso_DTV = Lapso(inicio_arg_time, fin_arg_time)
        titulo_esp = 'El Mago de Oz'
        canal = self.medio_DTV
        fecha_archivo_dtv = '2014-06-27'
        CHDTV.cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha_archivo_dtv)
        # me fijo que ese horario haya tomado la emisión
        horarios_actuales_2 = Horario.objects.filter(medio=self.medio_DTV)
        audiovisuales = AudioVisual.objects.filter(horario=horarios_actuales_2[0])
        self.assertEqual(len(audiovisuales), 1)
        self.assertEqual(audiovisuales[0].horario, horarios_actuales_2[0])

    def test_migracion_completa_tomar_emisiones(self):
        from adkiller.app.management.commands import cargar_horarios_DTV as CHDTV
        from adkiller.app.utils import Lapso
        # primero elimino las emisiones
        AudioVisual.objects.all().delete()
        audiovisuales_actuales = AudioVisual.objects.all()
        self.assertEqual(len(audiovisuales_actuales), 0)
        # Para canal 13, primero limpio la base de datos
        Horario.objects.all().delete()
        horarios_actuales = Horario.objects.filter(medio=self.medio_DTV)
        # lleno el dia de emisiones
        emision1 = AudioVisual(fecha=datetime(2014, 06, 27), hora='00:01:30', producto=self.producto)
        emision1.horario = self.PRRE
        emision1.save()
        emision2 = AudioVisual(fecha=datetime(2014, 06, 27), hora='00:02:30', producto=self.producto)
        emision2.horario = self.PRRE
        emision2.save()
        emision3 = AudioVisual(fecha=datetime(2014, 06, 27), hora='19:15:28', producto=self.producto)
        emision3.horario = self.PRRE
        emision3.save()
        emision4 = AudioVisual(fecha=datetime(2014, 06, 27), hora='12:02:30', producto=self.producto)
        emision4.horario = self.PRRE
        emision4.save()
        audiovisuales_actuales = AudioVisual.objects.all()
        self.assertEqual(len(audiovisuales_actuales), 4)

        # migro un archivo de este dia
        CHDTV.migrar(self.archivo_migrar, Medio(identificador_directv = "IDTV"), self.directorio_archivos)
	#cambios, agregue el medio en la llamada a migrar (Fernando)


        # me fijo que No haya emisiones con PRRE
        emisiones_PRRE = AudioVisual.objects.filter(fecha='2014-06-27', horario__programa__nombre__contains='Regular')
        self.assertEqual(len(emisiones_PRRE), 0)


from factories import HorarioFactory, UserFactory

class HorarioTestCase(TestCase):

    def setUp(self):
        self.horario = HorarioFactory(horaInicio=time(12, 00, 00),
                                      horaFin = time(12, 59, 59))
        self.horario_con_cambio_de_dia = HorarioFactory(
            horaInicio=time(23, 30, 00),
            horaFin = time(00, 29, 59))
        self.password = 'custompassword'
        self.user = UserFactory(password=self.password)


    def test_solapa_en_horario_inicio(self):
        # inicio igual a horario de inicio
        inicio = time(12, 00, 00)
        fin = time(13, 59, 59)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))
        # inicio menor a horario de inicio
        inicio = time(11, 59, 59)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))

    def test_solapa_en_horario_fin(self):
        # inicio igual a horario de inicio
        inicio = time(11, 00, 00)
        fin = time(12, 00, 00)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))
        # fin mayor a horario de inicio
        fin = time(12, 00, 01)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))

    def test_solapa_en_horario_fin_y_inicio(self):
        # inicion y fin igual a horario de inicio y fin
        inicio = time(12, 00, 00)
        fin = time(12, 59, 59)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))
        # inicio mayor a horario de inicio y fin menor a horario de fin
        inicio = time(12, 20, 00)
        fin = time(12, 40, 00)
        self.assertTrue(self.horario.solapa_en_horario(inicio, fin))

    def test_solapa_en_horario_despues_de_doce_fin(self):
        # inicion y fin igual a horario de inicio y fin
        inicio = time(23, 00, 00)
        fin = time(23, 45, 00)
        self.assertTrue(self.horario_con_cambio_de_dia.solapa_en_horario(inicio, fin))

    def test_solapa_en_horario_despues_de_doce_inicio(self):
        # inicion y fin igual a horario de inicio y fin
        inicio = time(00, 15, 00)
        fin = time(01, 00, 00)
        self.assertTrue(self.horario_con_cambio_de_dia.solapa_en_horario(inicio, fin))


    def test_solapa_en_horario_despues_de_doce_incio_despues(self):
        # inicion y fin igual a horario de inicio y fin
        inicio = time(22, 00, 00)
        fin = time(23, 59, 59)
        self.assertTrue(self.horario_con_cambio_de_dia.solapa_en_horario(inicio, fin))


    def test_solapa_en_horario_despues_de_doce_incio_antes(self):
        # inicion y fin igual a horario de inicio y fin
        inicio = time(23, 45, 00)
        fin = time(00, 59, 59)
        self.assertTrue(self.horario_con_cambio_de_dia.solapa_en_horario(inicio, fin))

