# -*- coding: utf-8 -*- # pylint: disable=too-many-lines
# pylint: disable=line-too-long, old-style-class, missing-docstring
"""
models for app app
"""
# Python Imports
from __future__ import unicode_literals
from datetime import date
from datetime import datetime
from datetime import time
from datetime import timedelta
from simplejson import JSONDecodeError
import httplib2
import json
import os
import pytz
import re
import requests
import urllib

# Django Imports
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives, EmailMessage
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Context
from django.template.loader import get_template

# Project Imports
from adkiller.app.managers import AccentFreeQuerySet
from adkiller.app.exceptions import ErrorAlBorrarPatron
from adkiller.local_settings import PAIS


CHOICES_TIPO_EMISION = (('P', 'Publicidad'), ('C', 'Contenido'))
CHOICES_TIPO_ORIGEN = (('P', 'Publicidad'), ('C', 'Contenido'), ('F', 'Fuente'))

# donde estan guardados los audiovisuales en cada
# simpson segun sean de contenido o publicidad
CARPETA_ARCHIVOS_INFOMERCIAL = 'Infomerciales'
CARPETA_ARCHIVOS_PUBLICIDAD = 'CapturaDVRs'
CARPETA_ARCHIVOS_CONTENIDO = 'ArchivosFLV'
CARPETA_ARCHIVOS_FUENTE = 'ArchivosH264'


# al tomar la tarifa de un horario a partir de horarios anteriores, lo ideal
# sería que coincidieran perfectamente horaInicio y horaFin. Si no fuera así,
# cuál es la tolerancia que tenemos para considerar que la tarifa precisa
# supervisión humana?
TOLERANCIA_TARIFA_DUDOSA = 60 * 10 # 10 minutos

class AccentManager(models.Manager):
    def get_query_set(self):
        return AccentFreeQuerySet(self.model)


class Rubro(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    #nombre_ingles = models.CharField(max_length=150, unique=True,
    #                                    null=True, blank=True)

    objects = AccentManager()

    def __unicode__(self):
        return self.nombre

    def unificar(self, otro):
        otro.industrias.update(rubro=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Rubros"
        ordering = ["nombre"]

    def get_absolute_url(self):
        return reverse('non_admin:widget_update', args=(self.pk,))


class Industria(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    rubro = models.ForeignKey('Rubro', related_name='industrias')
    #nombre_ingles = models.CharField(max_length=150, unique=True,
    #                                    null=True, blank=True)

    objects = AccentManager()

    def __unicode__(self):
        if self.nombre:
            return self.nombre.title()
        else:
            return ""

    def unificar(self, otro):
        otro.sectores.update(industria=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Industrias"
        ordering = ["nombre"]


class Sector(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    industria = models.ForeignKey('Industria', related_name='sectores')
    #nombre_ingles = models.CharField(max_length=150, unique=True,
    #                                    null=True, blank=True)

    objects = AccentManager()

    def __unicode__(self):
        if self.nombre:
            return self.nombre.title()
        else:
            return ""

    def unificar(self, otro):
        otro.marcas.update(sector=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Sectores"
        ordering = ["nombre"]


class Marca(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    anunciante = models.ForeignKey('Anunciante', related_name='marcas', null=True, blank=True)
    sector = models.ForeignKey('Sector', related_name='marcas', null=True, blank=True)
    brandtrack_entrenado = models.BooleanField(default=False, blank=True,
                                    help_text='El entrenamiento de Brandtrack '
                                              'está aprobado. Sólo los eventos que '
                                              'tengan todas su marcas aprobadas '
                                              'serán analizados automaticamente.'
                                              )

    objects = AccentManager()

    def __unicode__(self):
        return self.nombre

    def activo(self, solo_audiovisuales=False):
        if solo_audiovisuales:
            return hay_emisiones_activas(producto__marca=self,
                                         audiovisual__isnull=False)
        else:
            return hay_emisiones_activas(producto__marca=self)
    activo.boolean = True

    def unificar(self, otro):
        if not self.alias.filter(nombre=otro.nombre):
            self.alias.create(nombre=otro.nombre)
        if not self.anunciante:
            self.anunciante = otro.anunciante
            self.save()
        for producto in otro.productos.all():
            otros = Producto.objects.filter(marca=self, nombre=producto.nombre)
            if otros:
                otros[0].unificar(producto)
            else:
                producto.marca = self
                producto.save()
        otro.alias.update(marca=self)
        otro.segmentos.update(producto=self)
        otro.delete()

    def datos_para_unificar(self):
        return "%s - %s" % (self.sector, self.anunciante)

    def __init__(self, *args, **kw):
        super(Marca, self).__init__(*args, **kw)
        self._old_nombre = self.nombre

    def save(self, *args, **kwargs):
        if self.nombre != self._old_nombre:
            alias = self._old_nombre
        else:
            alias = None
        super(Marca, self).save(*args, **kwargs)
        if alias:
            self.alias.create(nombre=alias)

    @staticmethod
    def autocomplete_search_fields():
        return ("nombre__icontains",)

    class Meta:
        verbose_name_plural = "Marcas"
        ordering = ["nombre"]


class Anunciante(models.Model):
    nombre = models.CharField(max_length=150, unique=True, )
    help_text = "Si está en más de una provincia, es nacional"
    es_nacional = models.BooleanField(default=False, help_text=help_text)
    usuarios = models.ManyToManyField(User, null=True, blank=True)

    objects = AccentManager()

    def __unicode__(self):
        if self.nombre == "anunciante 1":
            return "Anunciante Local"
        elif self.nombre == "TEVECOMPRAS 2001 SRL":
            return "TeVeCompras 2001 SRL"
        return self.nombre

    def activo(self):
        return hay_emisiones_activas(producto__marca__anunciante=self)
    activo.boolean = True

    def unificar(self, otro):
        otro.marcas.update(anunciante=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Anunciantes"
        ordering = ["nombre"]


class Patron(models.Model):
    producto = models.ForeignKey("Producto", related_name="patrones")
    filename = models.CharField(max_length=255, null=True, blank=True)
    mp4_filename = models.CharField(max_length=255, null=True, blank=True)
    server = models.ForeignKey('Servidor', null=True, blank=True)
    id_en_simpson = models.IntegerField(null=True, blank=True)


    def desentrenar(self):
        """Eliminar patrón del server de detección"""
        url = 'http://{}/API/baja_patron.php'.format(self.server.url)
        data = {'id_publicidad': self.id_en_simpson}
        request = requests.post(url, data)
        try:
            response = request.json()
        except (requests.ConnectionError, AttributeError, JSONDecodeError):
            raise ErrorAlBorrarPatron('Error al conectar a {}'.format(self.server.nombre))
        else:
            if not response['result']:
                raise ErrorAlBorrarPatron(response.get('msg', ''))


    def soporte(self):
        soporte = ""
        emisiones = Emision.objects.filter(producto=self.producto)
        if emisiones:
            emision = emisiones[0]
        else:
            emision = None
        if emision and emision.horario and emision.horario.medio and emision.horario.medio.soporte:
            soporte = emision.horario.medio.soporte.nombre
        return soporte


    def thumb(self):
        if PAIS == 'Chile':
            soporte = self.soporte()
            if soporte == 'Prensa' or soporte == 'Diario' or soporte == 'Revista':
                return "http://www.megatime.cl/mmia/ADS/%s.jpg" %  self.id_en_simpson
            elif soporte == 'Vía Pública' or soporte == 'Metro':
                return "http://www.megatime.cl/mmia/Anuncios/%s.jpg" %  self.id_en_simpson
            elif soporte == 'Televisión' or soporte == 'Tv Aire' or soporte == 'Tv Paga':
                return "http://www.megatime.cl/mmia/Spots/imgs/%s.jpg" %  self.id_en_simpson

        else:
            if self.server.url_completa():
                if self.mp4_filename:
                    return "http://" + self.server.url_completa() + "/multimedia/ar/thumbsSpots/" + self.mp4_filename + ".jpg"
                elif self.filename:
                    return "http://" + self.server.url_completa() + "/multimedia/ar/thumbsSpots/" + self.filename + ".jpg"


    def inventa_filename(self):
        if self.id_en_simpson:
            nombre = 'pub_%d' % self.id_en_simpson
        else:
            nombre = 'pub__%d' % self.id
        soportes = Soporte.objects.filter(nombre=self.soporte())
        if soportes:
            soporte = soportes[0]
            extension = soporte.filename_extension()
        else:
            extension = 'mp4'
        return '.'.join([nombre, extension])


    def video(self):
        if PAIS == 'Chile':
            soporte = self.soporte()
            if soporte == 'Radio':
                return "http://www.megatime.cl/mmia/Frases/%s.mp3" %  self.id_en_simpson
            elif soporte == 'Televisión' or soporte == 'Tv Paga' or soporte == 'Tv Aire':
                return "http://www.megatime.cl/mmia/Spots/%s.f4v" %  self.id_en_simpson
            elif soporte == 'Prensa' or soporte == 'Revista' or soporte == 'Diario':
                return "http://www.megatime.cl/mmia/ADS/%s.jpg" %  self.id_en_simpson
            elif soporte == 'Televisión' or soporte == 'Tv Aire' or soporte == 'Tv Paga':
                return "http://www.megatime.cl/mmia/tvf4v/imgs/%s.jpg" %  self.id_en_simpson

            #if self.filename and self.filename[-3:] in ["wmv", "f4v"]:
            #    return "http://www.megatime.cl/mmia/tvf4v/%s.f4v" %  self.id_en_simpson
        else:
            server = self.server
            if self.filename and "mp3" in self.filename:
                server = Servidor.objects.get(nombre="Martin Nuevo")

            if not self.mp4_filename:
                return "http://" + server.url_completa() + \
                        "/multimedia/ar/Spots/" + str(self.filename)
            else:
                return "http://" + server.url_completa() + \
                        "/multimedia/ar/SpotsMP4/" + self.mp4_filename

    def crear_mp4(self):
        headers = {
            "Content-type": "application/x-www-form-urlencoded",
           "Accept": "text/html,application/xhtml+xml," +
                            "application/xml;q=0.9,*/*;q=0.8"
        }

        def llamado_a_simpsons(origen, inicio=None, duracion=None, nom_salida=None):
            # chequeos basicos
            if not origen:
                print "No tengo nombre de archivo, no puedo ser convertido a mp4"
                resp = {'status': "No tengo nombre de archivo"}
                content = {'result': False, 'errores': 'Ni siquiera me conecto'}
                return (resp, content)

            params = {'input_file': origen}
            if inicio is not None:
                params['dec_seg_desde'] = 10 * inicio # paso a decimas
            if duracion is not None:
                params['dec_seg_duracion'] = duracion * 10 #paso a decimas
            if nom_salida is not None:
                params['nombre_salida'] = nom_salida

            params = urllib.urlencode(params)
            h = httplib2.Http()
            url = "http://%s:%s" % (self.server.url, self.server.puerto_http)
            url += "/API/crear_video_patron.php?" + params
            print url
            try:
                resp, content = h.request(url, "GET", headers=headers)
                content = json.loads(content)
            except:
                resp = {'status': "No se pudo conectar al simpson"}
                content = {'result': False, 'errores': 'entre al except'}

            return (resp, content)


        nombre_salida = self.inventa_filename()

        if nombre_salida:
            # buscar emisiones de este patron
            emisiones = AudioVisual.objects.filter(
                    score__gt=0,
#                     migracion__servidor=self.server,
                    producto=self.producto,
                    fecha__gt=date.today() - timedelta(days=30)).order_by('-fecha', '-orden')

            print "Filtrando las que tienen servidor distinto"
            emisiones = filter(lambda em: em.servidor() == self.server, emisiones)

            print "Quedaron %d emisiones" % len(emisiones)
            print len(emisiones), "emisiones disponibles"

            print "Ordenandolas segun DVR"
            # las DVR, según orden de predilección
            dvr_ranking = [
                    'DirectvMulti',
                    'Cordoba',
                    'Cablevision',
                    'SanJuan',
                    'Mendoza',
                    'Chaco',
                    'Salta',
                    'Neuquen',
                    ]

            def sort_dvr_key(emision):
                try:
                    ranking = dvr_ranking.index(emision.medio().dvr)
                except:
                    ranking = 10000 # un numero arbitrariamente grande
                return ranking

            emisiones.sort(key=sort_dvr_key)


            if len(emisiones) > 20:
                print "Probaremos con las 20 primeras..."
                emisiones = emisiones[:20]

            for emision in emisiones:
                print "\nTratando de convertir", emision.origen
                _, content = llamado_a_simpsons(
                        emision.origen,
                        emision.inicio,
                        emision.duracion,
                        nombre_salida
                )

                if content['result']:
                    # ya se logró el objetivo
                    print "EXITO!"
                    self.mp4_filename = nombre_salida
                    self.save()
                    return True
                else:
                    print content['errores']

            # si aún no lo logro
            if not self.mp4_filename:
                # convertir flv a mp4
                print "Fracaso con todas las tandas. Convirtiendo flv a mp4"
                _, content = llamado_a_simpsons(self.filename, nom_salida=nombre_salida)
                if content['result']:
                    print "EXITO"
                    self.mp4_filename = nombre_salida
                    self.save()
                    return True
                else:
                    print "Tampoco pude"
        else:
            print "No tiene filename ni id_en_simpson"

    def filetype(self):
        soporte = self.soporte()
        res = None
        if self.mp4_filename:
            res = "mp4"
        elif self.filename:
            if soporte == 'Radio':
                res = "mp3"
            elif soporte == 'Revista' or soporte == 'Diario' or soporte == 'Prensa':
                res = ".jpg"
            else:
                res = self.filename[-3:]
        else:
            if soporte == 'Radio':
                res = "mp3"
        if res == "wmv" or res == "f4v":
            res = "mp4"
        return res

    def copiar_en_simpsons(self):
        if self.id_en_simpson:
            path_archivo = "%sHashes%s/%s.dat" % (settings.MEDIA_ROOT,
                                    self.server.nombre, self.id_en_simpson)

            if os.path.isfile(path_archivo):
                print "Archivo ya transferido"
            else:
                print "Trayendo archivo"
                pattern = "sshpass -p '123456'  scp -P %s infoxel@%s:" +\
                "/home/tools/adtrack/cpp/hashes/%s.dat %sHashes%s/%s.dat 2>" +\
                " %sagregar_patron1.log"
                sentencia = pattern % (self.server.puerto_ssh, self.server.url,
                    self.id_en_simpson, settings.MEDIA_ROOT, self.server.nombre,
                    self.id_en_simpson, settings.MEDIA_ROOT)

                res = 0
                print sentencia
                res = os.system(sentencia)

                if int(res) == 0 and os.path.isfile(path_archivo):
                    print "Archivo Transferido"
                else:
                    print "Archivo no transferido"
                    return

            for server in Servidor.objects.filter(url__isnull=False,
                                                recibir_patrones=True):
                print server
                av = self.producto.emisiones.filter(audiovisual__isnull=False)
                if av and not self.producto.patrones.filter(server=server):
                    av = av[0].audiovisual
                    # definir url y headers
                    url = "http://%s:%s" % (server.url, server.puerto_http)
                    headers = {
                        "Content-type": "application/x-www-form-urlencoded",
                        "Accept": "text/html,application/xhtml+xml," +
                                        "application/xml;q=0.9,*/*;q=0.8"
                    }

                    # post para crear registro en
                    # tabla publicidad en el otro simpson

                    params = {
                        'producto': self.producto.nombre.encode("utf-8"),
                        'id_marca': self.producto.marca.id,
                        'vida_util': 7,
                        'duracion_adtrack': av.duracion_adtrack,
                    }
                    params = urllib.urlencode(params)
                    print params
                    h = httplib2.Http()
                    try:
                        resp, content = h.request(url +
                        "/adtrack/cargar_publicidad.php", "POST", body=params,
                                headers=headers)
                    except:
                        resp = {'status': "No se pudo conectar al simpson"}
                        content = ""

                    print resp, content

                    # si pudo crear el registro en tabla publicidad,
                    # copiar el .dat
                    if resp['status'] == "200":
                        new_id = int(content)
                        pattern = "sshpass -p '123456' scp -P %s %sHashes%s/" +\
                        "%s.dat infoxel@%s:/home/tools/adtrack/cpp/hashes/" +\
                            "%s.dat 2> %sagregar_patron2.log"
                        sentencia = pattern % (server.puerto_ssh,
                            settings.MEDIA_ROOT, self.server.nombre,
                            self.id_en_simpson, server.url, new_id,
                            settings.MEDIA_ROOT
                            )
                        print sentencia
                        res = os.system(sentencia)
                        if int(res) == 0:
                            # si pudo crear el registro y copiar el .dat,
                            # actualizar la base de datos para reflejar
                            # estos cambios
                            self.producto.patrones.create(server=server,
                                                    id_en_simpson=new_id)

    class Meta:
        verbose_name_plural = "Patrones"
        unique_together = ['producto', 'server']
        ordering = ["server__nombre"]


class Producto(models.Model):
    nombre = models.CharField(max_length=120, db_index=True)
    marca = models.ForeignKey('Marca', related_name='productos')
    otras_marcas = models.ManyToManyField('Marca', related_name='otros_productos', null=True, blank=True)
    agencia = models.ForeignKey('Agencia', related_name='productos', null=True, blank=True)
    observaciones = models.CharField(max_length=500, blank=True)
    tipo_publicidad = models.ForeignKey('TipoPublicidad', related_name='productos', null=True, blank=True)
    estilo_publicidad = models.ForeignKey('EstiloPublicidad', related_name='productos', null=True, blank=True)
    valores = models.ManyToManyField('ValorPublicidad', related_name='productos', null=True, blank=True)
    recursos = models.ManyToManyField('RecursoPublicidad', related_name='productos', null=True, blank=True)
    duracion = models.IntegerField(null=True, blank=True)
    categorizacion_pendiente = models.BooleanField(default=True)

    creado = models.DateTimeField(auto_now_add=True, null=True)
    modificado = models.DateTimeField(auto_now=True, null=True)

    # no se deberian usar mas, ahora esta Patron
    nombre_del_patron = models.CharField(max_length=255, null=True, blank=True)
    server = models.ForeignKey('Servidor', null=True, blank=True)
    id_en_simpson = models.IntegerField(null=True, blank=True)
    etiquetas = models.ManyToManyField('EtiquetaProducto', null=True, blank=True, related_name="productos")
    nuevo = models.BooleanField(default=True)
    codigo_directv = models.CharField(max_length=100, null=True, blank=True, help_text="Puede colocar varios códigos separados por coma")

    objects = AccentManager()

    def get_models(self):
        """Devolver models"""
        segmentos = self.segmentos.filter(boxes__usado_para_entrenar=True)
        result = []
        for segmento in segmentos:
            for b in segmento.boxes.filter(usado_para_entrenar=True):
                result.extend(b.get_models())
        return list(set(result))

    def tiene_codigo_directv(self, ident):
        codigo_dtv = self.codigo_directv
        if codigo_dtv:
            lista = codigo_dtv.split(",")
            lista = [c.strip() for c in lista]
        else:
            lista = []
        return ident in lista

    def activo(self):
        return hay_emisiones_activas(producto=self)
    activo.boolean = True

    def servers(self):
        return ",".join([p.server.nombre for p in self.patrones.all()])

    def patron_principal(self):
        # buscamos patrones que tengan id_en_simpson
        patrones = self.patrones.filter(Q(mp4_filename__isnull=False)&Q(mp4_filename__gt=""))
        if not patrones:
            patrones = self.patrones.filter(id_en_simpson__isnull=False)
        # si no hay
        if not patrones:
            if self.nombre_del_patron and self.id_en_simpson:
                if self.server:
                    server = self.server
                else:
                    emi = self.emisiones.all()
                    if emi and emi[0].migracion:
                        server = emi[0].migracion.servidor
                    else:
                        server = None
                if server:
                    p, created = Patron.objects.get_or_create(producto=self,
                                filename=self.nombre_del_patron, server=server,
                                            id_en_simpson=self.id_en_simpson)
                    return p
                # tiene id_en_simpson, tiene nombre_del_patron, pero no pudimos ubicar el server
                else:
                    if self.patrones.all():
                        return self.patrones.all()[0]
            # no tiene nombre_del_patron o no tiene id_en_simpson
            else:
                if self.patrones.all():
                    return self.patrones.all()[0]
        else:
            # si no hay patrones con id_en_simpson
            edna = patrones.filter(server__nombre="Edna")
            if edna:
                return edna[0]
            else:
                return patrones[0]

    def thumb(self):
        """ thumb del spot """
        emisiones = Emision.objects.filter(producto=self)
        if emisiones:
            emision = emisiones[0]
        else:
            # TODO: revisar esta decisión
            return None

        if PAIS == 'Chile': # VER DE HACER REFACTOR DESPUES
            thumb = None
            p = self.patron_principal()
            if p:
                thumb = p.thumb()
            if not thumb:
                if emision.formato() == "A":
                    thumb = "/static/design/thumb_tv_big.jpg"
                    return thumb
                elif emision.formato() == "R":
                    thumb = "/static/design/thumb_radio_big.jpg"
                elif emision.formato() == "G":
                    thumb = "/static/design/thumb_diario_big.jpg"
            return thumb
        else:
            if emision.formato() == "A":
                # TODO: ver qué hace esto (estaba arriba antes)
                thumb = None
                p = self.patron_principal()
                if p:
                    thumb = p.thumb()
                if not thumb:
                    thumb = "/static/design/thumb_tv_big.jpg"

                return thumb
            elif emision.formato() == "R":
                return "/static/design/thumb_radio_big.jpg"
            elif emision.formato() == "G":
                return "/static/design/thumb_diario_big.jpg"
                # FIXME: traemos thumb del aviso?

    def video(self):
        """ video del spot """
        p = self.patron_principal()
        if p:
            return p.video()

    def datos_alerta_nuevo_producto(self):
        """ Devuelve las ciudades y soportes en donde
        el producto fue emitido y la fecha de creacion """

        soportes = list(set([e.horario.medio.soporte.nombre for e in self.emisiones.all() if e.horario]))
        ciudades = list(set([e.horario.medio.ciudad.nombre for e in self.emisiones.all() if e.horario]))

        g = ['Diario', 'Revista']
        if len(soportes) > 1:
            for soporte in soportes:
                if soporte in g:
                    soportes.remove(soporte)

        soportes = "-".join(soportes)
        ciudades = "-".join(ciudades)

        emision = Emision.objects.filter(producto=self).order_by('hora').order_by('-fecha')[0]
        creado = str(emision.fecha.strftime("%d/%m")) + ' - ' + str(emision.hora.strftime("%H:%M")) + 'hs'

        return ciudades + ' - ' + soportes + ' - ' + creado

    def __unicode__(self):
        return "%s" % self.nombre

    def copiar_en_simpsons_old(self):
        patron = self.patron_principal()
        if patron and patron.id_en_simpson and self.activo():
            linux_user = settings.MEDIA_ROOT.split("/")[2]
            path_archivo_old = "%spublicidad_%s.flv" % (settings.MEDIA_ROOT,
                patron.id_en_simpson)
            path_archivo = "%s%s_%s.flv" % (settings.MEDIA_ROOT,
                patron.server.nombre, patron.id_en_simpson)

            if os.path.isfile(path_archivo):
                print "Archivo ya transferido"
            else:
                print "Trayendo archivo"
                pattern = "sudo -u %s rsync -avz infoxel@%s:/var/www/" +\
                    "multimedia/ar/Spots/publicidad_%s.flv -e 'ssh -p %s' %s  2>" +\
                    " %sagregar_patron.log"
                sentencia = pattern % (linux_user, patron.server.url,
                    patron.id_en_simpson, patron.server.puerto_ssh,
                    settings.MEDIA_ROOT, settings.MEDIA_ROOT)
                res = 0
                print sentencia
                res = os.system(sentencia)

                if int(res) == 0 and os.path.isfile(path_archivo_old):
                    "Archivo Transferido"
                else:
                    print "Archivo no transferido"
                    return

                os.system("sudo -u %s cp %s %s" % (linux_user, path_archivo_old,
                                                path_archivo))

            for server in Servidor.objects.filter(url__isnull=False):
                if not self.patrones.filter(server=server):
                    sentencia = "sudo -u %s rsync -avz %s -e 'ssh -p %s'" +\
                    " infoxel@%s:/home/tools/adtrack/cpp/ads/  2> " +\
                    "%sagregar_patron.log " % (linux_user, path_archivo,
                            server.puerto_ssh, server.url, settings.MEDIA_ROOT)
                    print sentencia
                    os.system(sentencia)

                    url = "http://%s:%s" % (server.url, server.puerto_http)
                    headers = {"Content-type": "application/x-www-form-urlencoded",
                        "Accept": "text/html,application/xhtml+xml," +
                        "application/xml;q=0.9,*/*;q=0.8"}

                    # post para crear patrón en el simpson
                    params = {
                        'pfusr': 10,
                        'accion': 'agregarPatron',
                        'format': 'xml',
                        'producto': self.nombre.encode("utf-8"),
                        'id_anunciante': "2",  # anunciante local
                        'id_marca': self.marca.id,
                        'id_sector': "1",
                        'id_agencia': "1",
                        'id_tipo_publicidad': "1",
                        'id_estilo_publicitario': "1",
                        'ids_valores_transmitidos': "1,",
                        'ids_recursos_utilizados': "1,",
                        'vida_util': 7,
                        'filename': "%s_%s_x_flv" % (patron.server.nombre,
                                                        patron.id_en_simpson),
                        'observaciones': '',
                    }
                    params = urllib.urlencode(params)
                    print params
                    h = httplib2.Http()
                    try:
                        resp, content = h.request(url + "/adtrack/control/" +
                        "lineatiempo_adtrack.service.php", "POST", body=params,
                            headers=headers)
                    except:
                        resp = {'status': "No se pudo conectar al simpson"}
                        content = ""
                    if resp['status'] == "200":
                        self.patrones.create(server=server)
                    print resp, content

    def unificar(self, otro):
        if not self.alias.filter(nombre=otro.nombre):
            self.alias.create(nombre=otro.nombre)
        if not self.agencia:
            self.agencia = otro.agencia
        if not self.nombre_del_patron:
            self.nombre_del_patron = otro.nombre_del_patron
        if not self.server:
            self.server = otro.server
        if not self.id_en_simpson:
            self.id_en_simpson = otro.id_en_simpson
        otro.emisiones.update(producto=self)
        for p in otro.patrones.all():
            if not self.patrones.filter(server=p.server):
                p.producto = self
            else:
                p.delete()
        #otro.patrones.update(producto=self)
        otro.alias.update(producto=self)
        otro.segmentos.update(producto=self)
        otro.delete()

    def datos_para_unificar(self):
        return "%s" % self.marca

    def __init__(self, *args, **kw):
        super(Producto, self).__init__(*args, **kw)
        self._old_nombre = self.nombre
        try:
            self._old_marca = self.marca
        except models.exceptions.ObjectDoesNotExist:
            self._old_marca = None
        self._old_duracion = self.duracion
        self._old_tipo = self.tipo_publicidad

    def save(self, *args, **kwargs):
        if self.nombre != self._old_nombre or self.marca != self._old_marca:
            alias_prod = self._old_nombre
            alias_marca = self._old_marca
        else:
            alias_prod = None
            alias_marca = None
        super(Producto, self).save(*args, **kwargs)
        if alias_prod or alias_marca:
            if not self.alias.filter(nombre=alias_prod, marca=alias_marca):
                self.alias.create(nombre=alias_prod, marca=alias_marca)
        if self._old_duracion != self.duracion or self._old_tipo != self.tipo_publicidad:
            # si cambio duracion o tipo_publicidad
            for e in AudioVisual.objects.filter(producto=self, fecha__gt=date.today() - timedelta(days=60)):
                if self._old_duracion != self.duracion or self._old_tipo != self.tipo_publicidad:
                    e.set_duracion()
                if self._old_tipo != self.tipo_publicidad:
                    e.tipo_publicidad = self.tipo_publicidad
                e.save()

    def ultima_duracion(self):
        e = self.emisiones.all()
        if e:
            return e[len(e) - 1].duracion

    @staticmethod
    def autocomplete_search_fields():
        return ("nombre__icontains", "marca__nombre__icontains",)

    def obtener_duracion_de_nombre(self):
        duracion = re.search("\((\d+)\)", self.nombre)
        if duracion:
            # re.match instance to int
            duracion = int(duracion.groups(0)[0])
            if duracion != self.duracion:
                self.duracion = duracion
                self.save()
            return duracion


    class Meta:
        verbose_name_plural = "Productos"
        unique_together = (('nombre', 'marca'), )
        ordering = ["marca", "nombre"]


class TipoPublicidad(models.Model):
    nombre = models.IntegerField(unique=True)
    descripcion = models.CharField(max_length=200, blank=True, null=True)
    es_publicidad = models.BooleanField(default=True)

    def __unicode__(self):
        if self.descripcion:
            return self.descripcion
        return str(self.nombre)

    class Meta:
        verbose_name_plural = "Tipo de publicidad"
        ordering = ["nombre"]


class EstiloPublicidad(models.Model):
    nombre = models.IntegerField(unique=True)
    descripcion = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):

        if self.descripcion:
            return self.descripcion
        return str(self.nombre)

    class Meta:
        verbose_name_plural = "Estilos"
        ordering = ["nombre"]


class ValorPublicidad(models.Model):
    nombre = models.IntegerField(unique=True)
    descripcion = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):
        if self.descripcion:
            return self.descripcion
        return str(self.nombre)

    class Meta:
        verbose_name_plural = "Valores"
        ordering = ["nombre"]


class RecursoPublicidad(models.Model):
    nombre = models.IntegerField(unique=True)
    descripcion = models.CharField(max_length=200, blank=True, null=True)

    def __unicode__(self):

        if self.descripcion:
            return self.descripcion
        return str(self.nombre)

    class Meta:
        verbose_name_plural = "Recursos"
        ordering = ["nombre"]


class Agencia(models.Model):
    nombre = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Agencias"
        ordering = ["nombre"]


class Ciudad(models.Model):
    nombre = models.CharField(max_length=100, unique=True)

    objects = AccentManager()

    def __unicode__(self):
        return self.nombre

    def unificar(self, otro):
        for medio in otro.medios.all():
            otros = Medio.objects.filter(ciudad=self, nombre=medio.nombre)
            if otros:
                otros[0].unificar(medio)
            else:
                medio.ciudad = self
                medio.save()

        otro.medios.update(ciudad=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Ciudades"
        ordering = ["nombre"]


class GrupoMedio(models.Model):
    nombre = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.nombre

    objects = AccentManager()


class Medio(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    identificador = models.CharField(max_length=100, null=True, blank=True)
    identificador_anterior = models.CharField(max_length=100, null=True, blank=True)
    identificador_directv = models.CharField(max_length=100, null=True, blank=True)
    canal_directv = models.CharField(max_length=100, null=True, blank=True)
    ciudad = models.ForeignKey('Ciudad', related_name='medios')
    soporte = models.ForeignKey('Soporte', related_name='medios')
    incremento_color = models.IntegerField(null=True, blank=True)
    dvr = models.CharField(max_length=100, null=True, blank=True)
    canal = models.IntegerField(null=True, blank=True)
    encargado = models.ForeignKey(User, null=True, blank=True)

    grupo = models.ForeignKey(GrupoMedio, null=True, blank=True)

    server = models.ForeignKey('Servidor', null=True, blank=True, related_name="medios_deteccion")
    server_grabacion = models.ForeignKey('Servidor', null=True, blank=True, related_name="medios_grabacion")
    publicar = models.BooleanField(default=True, db_index=True)
    grabando = models.BooleanField(default=True)
    grabando_en_cero = models.BooleanField(default=False)
    desfasando = models.BooleanField(default=False)

    codigo_ibope = models.CharField(max_length=100, null=True, blank=True)
    etiquetas = models.ManyToManyField('EtiquetaMedio', null=True, blank=True, related_name="medios")

    mediadelivery_id = models.IntegerField(null=True, blank=True)

    ratings_id = models.IntegerField(null=True, blank=True, unique=True)

    busque_tandas_hasta = models.DateTimeField(null=True, blank=True, editable=False)

    objects = AccentManager()


    def update_in_welo(self):
        import requests
        WELO = "http://welo.tv/infoad_data"
        datos = dict(name=self.nombre, device=self.dvr , slot = self.canal , delivery_id = self.mediadelivery_id)
        response = requests.post(WELO,data = datos)
        json_response = json.loads(response.content)

        if json_response['created']:
            self.mediadelivery_id = json_response['delivery_id']
            self.save()
        #new_di = response.text
#        if new_di != "" and new_di != "channel_updated":
#            try:
#                self.mediadelivery_id = new_di
#                self.save()
#            except Exception:
#                pass

    def get_duracion_tandas_inventadas(self):
        """Devolver un timedelta que indica el promedio de
        tiempo que duran las tandas de este medio"""
        seconds = {
            'Glitz [ARG]': 8 * 60
        }
        try:
            duracion = seconds[self.nombre]
        except KeyError:
            raise NotImplementedError
        else:
            return timedelta(seconds=duracion)

    def get_last_thumb_datetime(self, checked=False):
        """Devolver la fecha y hora del último thumb de este medio.
        Puede devolver None si hay problemas para encontrar la información"""
        # si no tiene media_delivery id no tiene thumbs en el storage
        result = None
        if self.mediadelivery_id:
            if checked:
                # thumbs cuya existencia ya fue chequeada
                url_template = 'http://media.welo.tv/prog_thumbs/{}/last_checked.txt'
            else:
                url_template = 'http://media.welo.tv/prog_thumbs/{}/last.txt'
            r = requests.get(url_template.format(self.mediadelivery_id))
            #print r.status_code
            if r.status_code == 404:
                #si pidio la pagina y no existe significa que ese canal no tiene thumbs en storage
                return "Sin thumbs"
            else:
                # ahora si data es vacio esto da error y retorna Alerta
                # y si no es vacio y efectivametne hay algo funciona bien
                # data es 9/20151130/1620/234.jpg
                data = r.text
                try:
                    _, dia, hora_min, seg = data.split('/')
                except:
                    return "ALERTA"
                seg = seg.split('.')[0]
                result = datetime.strptime(dia + hora_min, '%Y%m%d%H%M')
                result += timedelta(seconds=int(seg))
        return result

    def get_patrones_inicio_y_fin(self, solo_activos=False):
        """devolver brandmodels usado para encontrar inicio
        y fin de espacio publicitario"""
        inicio = set()
        fin = set()
        segmentos = self.segmentos.filter(tipo__nombre='Tanda',
                                          boxes__usado_para_entrenar=True).distinct()
        for segmento in segmentos:
            mitad = segmento.desde + (segmento.hasta - segmento.desde) / 2
            for box in segmento.boxes.filter(usado_para_entrenar=True):
                if box.model.definitivo or not solo_activos:
                    if box.datetime > mitad:
                        fin.add(box.model)
                    else:
                        inicio.add(box.model)
        return list(inicio), list(fin)


    def __unicode__(self):
        if self.nombre:
            medio = self.nombre
            medio = re.sub(r'\bMgm\b', 'MGM', medio)
            medio = re.sub(r'\bSa\b', 'SA', medio)
            return medio
        else:
            return ""

    def verificar_panel_actualizado(self):
        """"
        -1 significa que hubo algun error de comunicacion
        0 que el panel esta sincronizado
        1 que el panel esta atrasado
        """
        timezone = pytz.timezone("America/Argentina/Buenos_Aires")
        hora_actual = datetime.now(tz=timezone).replace(tzinfo=None)
        hora_panel = self.get_last_thumb_datetime()
        try:
            retraso = hora_actual - hora_panel
            if retraso.seconds >= 60:
                return 1
            else:
                return 0
        except Exception:
            return -1

    def get_retraso(self):
        timezone = pytz.timezone("America/Argentina/Buenos_Aires")
        hora_actual = datetime.now(tz=timezone).replace(tzinfo=None)
        hora_panel = self.get_last_thumb_datetime()
        try:
            retraso = hora_actual - hora_panel
            if retraso.seconds >= 60:
                return 1 , retraso.seconds/60
            else:
                return 0 , retraso.seconds/60
        except Exception:
            return -1 , -1

    def unificar(self, otro):
        """
        Si no existe un alias con el nombre de ´otro´ lo crea
        si no se tiene un identificador, ciudad, soporte, incremento_color,
        dvr o canal se le asigna el correspodiente atributo de ´otro´,
        se reasignan los horarios de ´otro´ al medio actual, se
        reasignan los alis de `otro` al medio actual por ultimo se
        borra otro
        """
        if not self.alias.filter(nombre=otro.nombre):
            self.alias.create(nombre=otro.nombre)
        if not self.identificador:
            self.identificador = otro.identificador
        if not self.ciudad:
            self.ciudad = otro.ciudad
        if not self.soporte:
            self.soporte = otro.soporte
        if not self.incremento_color:
            self.incremento_color = otro.incremento_color
        if not self.dvr:
            self.dvr = otro.dvr
        if not self.canal:
            self.canal = otro.canal
        otro.medios.update(medio=self)
        otro.alias.update(medio=self)
        otro.delete()

    def __init__(self, *args, **kw):
        super(Medio, self).__init__(*args, **kw)
        self._old_nombre = self.nombre

    def save(self, *args, **kwargs):
        medio_viejo = None
        if self.nombre != self._old_nombre and self._old_nombre and self.id:
            self.alias.create(nombre=self._old_nombre)
        try:
            medio_viejo = Medio.objects.get(id = self.id)
        except Exception:
            pass
        if medio_viejo != None:
            if self.dvr != medio_viejo.dvr or self.canal != medio_viejo.canal:
                self.update_in_welo()
                pass
        if not medio_viejo:
            self.update_in_welo()
        super(Medio, self).save(*args, **kwargs)
        self.check_programacion_regular()
        #self.update_in_welo()
        #if self.mediadelivery_id == None and "radio"  in self.nombre.lower():
        #    self.mediadelivery_id = 10000 + self.id
        #    super(Medio, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Medios"
        #unique_together = (('nombre', 'ciudad'),)
        ordering = ["nombre"]

    def datos_para_unificar(self):
        return "%s" % self.soporte

    def get_programa_from_horario(self, hora, fecha):
        """
        Dado un horario (feacha, hora) devuelve un programa
        que se emita en ese horario (fecha y hora)

        """
        programa = None
        av = AudioVisual(fecha=fecha, hora=hora)
        av.set_horario(medio=self)
        if av.horario:
            programa = av.horario.programa
        return programa

    def alerta_grabacion(self):
        """
        Si el medio tiene un encargado y este posee un email,
        se envia envia un email al mismo
        """
        if self.encargado and self.encargado.email:
            to = [self.encargado.email]
            if self.grabando:
                mensaje = "El canal %s se está grabando en AVI" % self.nombre
            else:
                mensaje = "El canal %s NO se está grabando en AVI" % self.nombre
            email = EmailMessage('Grabacion en AVI', mensaje , to=to)
            email.send()

    def tiempo_de_proceso(self, fecha):
        avs = AudioVisual.objects.filter(horario__medio=self, fecha=fecha, tanda_completa=True, migracion__isnull=False).order_by('-migracion__fecha')
        if avs:
            return avs[0].migracion.fecha - fecha

    def cantidad_tandas(self, fecha):
        tandas = AudioVisual.objects.filter(horario__medio=self, fecha=fecha)
        return len(tandas)

    def cantidad_tandas_no_procesadas(self, fecha):
        incompletas = AudioVisual.objects.filter(horario__medio=self, fecha=fecha, tanda_completa=False)
        return len(incompletas)

    def tandas_no_procesadas(self, fecha):
        incompletas = AudioVisual.objects.filter(horario__medio=self, fecha=fecha, tanda_completa=False)
        return incompletas

    def check_programacion_regular(self):
        # si no tiene un "Programacion Regular" que abarque todo el
        # día, lo creamos.
        primer_horario = Horario.objects.filter(
            medio=self,
            fechaAlta__isnull=False
            ).order_by('fechaAlta')
        crear = False
        if not primer_horario:
            crear = True
        else:
            primer_horario=primer_horario[0]
        if not crear and primer_horario.programa.nombre != 'Programación Regular':
            crear = True
        if not crear and primer_horario.horaInicio and primer_horario.horaInicio > time(0,0):
            crear = True
        if not crear and primer_horario.horaFin and primer_horario.horaFin < time(23, 59, 59):
            crear = True
        if crear:
            programa, _ = Programa.objects.get_or_create(nombre='Programación Regular')
            fechaAlta = date(2000, 01, 01)
            horaInicio = time(0, 0, 0)
            horaFin = time(23, 59, 59)
            dias = '1,1,1,1,1,1,1'
            Horario.objects.create(
                medio=self,
                programa=programa,
                fechaAlta=fechaAlta,
                horaInicio=horaInicio,
                horaFin=horaFin,
                dias=dias,
                enElAire=False)

    def asignar_emisiones_a_horario_correspondiente(self, desde_fecha=None, hasta_fecha=None, cambiar_tarifas=True):
        avs = AudioVisual.objects.filter(horario__medio=self)
        if desde_fecha:
            avs = avs.filter(fecha__gte=desde_fecha)
        if hasta_fecha:
            avs = avs.filter(fecha__lte=hasta_fecha)
        print len(avs)
        for av in avs:
            h = av.horario
            av.set_horario(medio=h.medio)
            # si cambió
            if h != av.horario:
                if cambiar_tarifas:
                    av.calcular_tarifa()
                av.save()

    def es_para_playtown(self):
        return self.nombre in ["Magazine [ARG]", "Volver [ARG]", "América 2 [ARG]", "Canal 9 [ARG]", "América 2 SAT [ARG]", "Canal 9 SAT [ARG]", "Quiero! [LAT]", "Canal 13 SAT [ARG]"]


    def horarios_de_una_fecha(self, fecha):
        horarios = list(Horario.objects.filter(fechaAlta__lte=fecha, medio__nombre=self).order_by('-fechaAlta'))
        dia_num = (fecha.weekday() + 1) % 7

        def no_solapa_a_ninguno(candidato, otros):
            for h in otros:
                if candidato.solapa_en_horario(h.horaInicio, h.horaFin):
                    return False
            return True

        lista = []
        for h in horarios:
            if h.emitido_dia(dia_num):
                if no_solapa_a_ninguno(h, lista):
                    lista.append(h)

        return lista


#@receiver(post_save, sender=Medio)
#def clip_segmentos_nuevos(sender, instance, created, **kwargs):
#    instance.update_in_welo()

class Soporte(models.Model):
    nombre = models.CharField(max_length=100, unique=True)
    padre = models.ForeignKey("Soporte", null=True, blank=True, related_name="hijos")

    objects = AccentManager()

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Soportes"
        ordering = ["nombre"]

    def filename_extension(self):
        if self.nombre.startswith('TV') or self.nombre.startswith('Tele'):
            return 'mp4'
        if self.nombre in ['Radio']:
            return 'mp3'
        if self.nombre in ['Diario', 'Revista']:
            return 'jpg'
        return ''


class Horario(models.Model):
    tarifa = models.IntegerField(null=True, blank=True)

    # para horarios migrados de DTV que toman la tarifa de un
    # horario anterior, y se espera validación humana
    tarifaDudosa = models.BooleanField(default=False)

    enElAire = models.BooleanField(default=True)

    fechaAlta = models.DateField(null=True, db_index=True)
    vigenciaHasta = models.DateField(null=True, db_index=True)
    programa = models.ForeignKey('Programa', related_name='horarios')
    medio = models.ForeignKey('Medio', related_name='medios')
    completa = models.BooleanField(default=True)

    # para audiovisuales
    horaInicio = models.TimeField(null=True, blank=True)
    horaFin = models.TimeField(null=True, blank=True)
    dias = models.CommaSeparatedIntegerField(max_length=13, null=True,
                                            blank=True)

    # para gráfica
    pagina_desde = models.IntegerField(null=True, blank=True)
    pagina_hasta = models.IntegerField(null=True, blank=True,
                                    help_text="Vacío = Página Desde")
    paginas = models.CharField(max_length=1, null=True, blank=True,
        choices=(('I', 'Impares'), ('P', 'Pares'), ('C', 'Contratapa')))
    alto = models.PositiveIntegerField(null=True, blank=True)
    ancho = models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True)
    cantColum = models.PositiveIntegerField(null=True, blank=True)

    def tarifa_por_defecto(self):
        if not "DIRECTV" in self.medio.nombre.upper() and not "TELEVENTA" in self.programa.nombre.upper():
            return 50
        else:
            return 0

    def emitido_dia(self, dia):
        """
        si self.dias == None devuelvo false!!!
        dia: va del 0 (domingo) al 6 (sábado)
        Devuelvo true o false
        """
        try:
            return self.dias and self.dias.split(',')[dia] == '1'
        except:
            print self.dias, dia
            raise

    def emitido_dia_de_semana(self):
        return self.emitido_dia(1) or self.emitido_dia(2) or self.emitido_dia(3) or self.emitido_dia(4) or self.emitido_dia(5)

    def emitido_fin_de_semana(self):
        return self.emitido_dia(0) or self.emitido_dia(6)

    def dias_incluidos_en(self, otro):
        if self.emitido_dia_de_semana()  and not otro.emitido_dia_de_semana():
            return False
        if self.emitido_fin_de_semana()  and not otro.emitido_fin_de_semana():
            return False
        return True

    def solapa_en_horario(self, inicio, fin):
        if inicio is None or fin is None or self.horaInicio is None or self.horaFin is None:
            # si faltan datos no se puede comparar
            return False
        def str_to_time(hora):
            if not isinstance(hora, time):
                hora = datetime.strptime(hora, '%H:%M:%S')
                hora = time(hora.hour, hora.minute, hora.second)
            return hora

        inicio = str_to_time(inicio)
        fin = str_to_time(fin)
        # si no cruza las 00:00hs
        if self.horaFin > self.horaInicio:
            mi_inicio_esta_entre_inicio_y_fin = self.horaInicio >= inicio and self.horaInicio <= fin
            mi_fin_esta_entre_inicio_y_fin = self.horaFin >= inicio and self.horaFin <= fin
            empiezo_antes_y_termino_despues = self.horaInicio <= inicio and self.horaFin >= fin
            return mi_inicio_esta_entre_inicio_y_fin or mi_fin_esta_entre_inicio_y_fin or empiezo_antes_y_termino_despues
        else:
            mi_inicio_esta_entre_inicio_y_fin = self.horaInicio >= inicio and self.horaInicio <= fin
            mi_fin_esta_entre_inicio_y_fin = self.horaFin >= inicio and self.horaFin <= fin
            empiezo_durante = self.horaInicio <= fin
            el_otro_empieza_dentro_mio = self.horaInicio <= inicio
            return mi_inicio_esta_entre_inicio_y_fin or mi_fin_esta_entre_inicio_y_fin or empiezo_durante or el_otro_empieza_dentro_mio

    def tomar_emisiones_de_otro_horario(self, horario_viejo):
         emisiones = AudioVisual.objects.filter(
                          horario=horario_viejo,
                          hora__gte=self.horaInicio,
                          hora__lte=self.horaFin)
         for emision in emisiones:
             emision.horario = self
             emision.calcular_tarifa()
             emision.save()

    def actualizar_emisiones(self, fecha):
        print "actualizando emisiones"
        emisiones = AudioVisual.objects.filter(
                          horario__medio=self.medio,
                          fecha=fecha,
                          hora__gte=self.horaInicio,
                          hora__lte=self.horaFin)

        for emision in emisiones:
            print '.',
            emision.horario = self
            emision.calcular_tarifa()
            emision.save()
        print

    def agregar_dia(self, nuevo_dia):
        # original: '0,0,0,0,0,0,1' (Sábado)
        # nuevo_dia: 5 (Viernes)
        # modificado:  '0,0,0,0,0,1,1' (Viernes, y Sábado)
        # recordar que 0 = Domingo, 1 = Lunes, ..., 6 = Sábado
        assert nuevo_dia in range(7)
        if not self.dias:
            self.dias = '0,0,0,0,0,0,0'
        result = self.dias.split(',')
        result[nuevo_dia] = '1'
        result = ','.join(result)
        self.dias = result
        self.save()

    def estas_confirmado(self, fecha):
        if not self.medio.identificador_directv:
            # todo horario que no sea de DTV esta confirmado a priori
            return True
        else:
            # si es DTV, nos fijamos si hubo una migración de horarios posterior a esa fecha
            return len(Migracion.objects.filter(servidor__nombre="DTV", fecha__gte=fecha)) > 0


    def duracion(self):
        now = date.today()
        if self.horaInicio == None and self.horaFin == None:
            return
        if self.horaInicio < self.horaFin:
            diferencia = datetime.combine(now, self.horaFin) - datetime.combine(now, self.horaInicio)
            diferencia = diferencia + datetime.combine(now, time(0, 0, 0))
        else:
            diferencia = datetime.combine(now, time(23, 59, 59)) - datetime.combine(now, self.horaInicio)
            diferencia = diferencia + datetime.combine(now, self.horaFin)
        return diferencia.time()

    def diferencia_duracion(self, horario):
        now = date.today()
        mi_duracion = self.duracion()
        otra_duracion = horario.duracion()
        if mi_duracion == None or otra_duracion == None:
            return
        if mi_duracion > otra_duracion:
            diferencia = datetime.combine(now, mi_duracion) - datetime.combine(now, otra_duracion)
        else:
            diferencia = datetime.combine(now, otra_duracion) - datetime.combine(now, mi_duracion)
        diferencia = diferencia + datetime.combine(now, time(0, 0, 0))
        return diferencia.time()

    def diferencia_horario_incio(self, horario):
        now = date.today()
        if self.horaInicio == None or horario.horaInicio == None:
            return
        if self.horaInicio > horario.horaInicio:
            diferencia = datetime.combine(now, self.horaInicio) - datetime.combine(now, horario.horaInicio)
        else:
            diferencia = datetime.combine(now, horario.horaInicio) - datetime.combine(now, self.horaInicio)
        diferencia = diferencia + datetime.combine(now, time(0, 0, 0))
        return diferencia.time()


    def actualizar_tarifa(self, desde_fecha=None, usar_por_defecto=True, solo_por_programa=False):
        # si no tengo algunos datos minimos, no puedo colocar tarifa
        if self.fechaAlta == None or self.horaInicio == None or self.horaFin == None:
            return

        # filtro los horarios anteriores de ese mismo medio, con tarifa positiva
        lookup_filter = {
            'medio': self.medio,
            'fechaAlta__lt': self.fechaAlta,
            'tarifa__gt': 0,
            'horaInicio__isnull': False,
            'horaFin__isnull': False
            }

        # todavia no se si es dudosa
        self.tarifaDudosa = False

        # intento buscar un horario anterior del mismo programa, que pase en los mismos dias
        anteriores = Horario.objects.filter(**lookup_filter)
        anteriores = anteriores.filter(programa=self.programa)
        anteriores = anteriores.exclude(pk=self.pk)
        if desde_fecha:
            anteriores = anteriores.filter(fechaAlta__gte=desde_fecha)
        anteriores = anteriores.order_by("-fechaAlta")

        candidato = None
        for anterior in anteriores:
            if self.dias_incluidos_en(anterior):
                candidato = anterior
                break


        anteriores = Horario.objects.filter(**lookup_filter)
        anteriores = anteriores.exclude(Q(dias=None) | Q(dias="")).exclude(Q(horaInicio__isnull=True)).exclude(Q(horaFin__isnull=True))
        anteriores = anteriores.exclude(pk=self.pk)
        if desde_fecha:
            anteriores = anteriores.filter(fechaAlta__gte=desde_fecha)

        def offset(other):
            """Devolver la suma de segundos de desfasaje
            entre los comienzos y finales de self y other"""
            # necesito datetime, tengo time
            self_inicio = datetime.combine(datetime.today(), self.horaInicio)
            self_fin = datetime.combine(datetime.today(), self.horaFin)
            other_inicio = datetime.combine(datetime.today(), other.horaInicio)
            other_fin = datetime.combine(datetime.today(), other.horaFin)

            # calcular diferencias
            offset_inicio = (self_inicio - other_inicio).total_seconds()
            offset_fin = (self_fin - other_fin).total_seconds()

            # devolver la suma
            return abs(offset_inicio) + abs(offset_fin)

        anteriores = sorted(anteriores, key=offset)

        candidato2 = None
        for anterior in anteriores:
            if self.dias_incluidos_en(anterior) and self.solapa_en_horario(anterior.horaInicio, anterior.horaFin):
                candidato2 = anterior
                break

        if candidato:
            self.tarifa = candidato.tarifa
        elif candidato2 and not solo_por_programa:
            #print '\t\ttomando tarifa de "{}" (de {} a {})'.format(candidato.programa.nombre,
            #                                                         candidato.horaInicio,
            #                                                         candidato.horaFin)
            self.tarifa = candidato2.tarifa

            if offset(candidato2) > TOLERANCIA_TARIFA_DUDOSA and candidato2.programa.nombre != self.programa.nombre and self.medio.soporte.nombre == "TV Aire Capital":
                #print '\t\tTarifa dudosa'
                self.tarifaDudosa = True

        # si la tarifa es 0, que traiga la por defecto
        if (self.tarifa == 0 or self.tarifa == None):
            self.tarifa = self.tarifa_por_defecto()

        return self.tarifa

    def tomar_emisiones(self, cambiar_tarifas=True):
        qs = AudioVisual.objects.filter(
                fecha__gte=self.fechaAlta,
                horario__medio=self.medio)
        if self.vigenciaHasta:
            qs = qs.filter(fecha__lte=self.vigenciaHasta)

        for av in qs:
            h = av.horario
            av.set_horario(medio=h.medio)
            # si cambió
            if h != av.horario:
                if cambiar_tarifas:
                    av.calcular_tarifa()
                av.save()



    def save(self, *args, **kwargs):
        # TODO: ver que pasa cuando estas modificando un horario, no creando
        flag_tomar_emisiones = False
        if not self.id:
            # si es la creacion
            flag_tomar_emisiones = True

        super(Horario, self).save(*args, **kwargs)

        if flag_tomar_emisiones:
            self.actualizar_tarifa()
            self.tomar_emisiones()
        else:
            # Si la tarifa cambio procedo a actualizar las tarifas de las emisiones
            if self._tarifa != self.tarifa:
                for emision in self.emisiones.filter(producto__isnull=False):
                    # tratamos de ver si es grafica o audiovisual
                    for field in ['grafica', 'audiovisual']:
                        try:
                            children = getattr(emision, field)
                        except models.exceptions.ObjectDoesNotExist:
                            children = None
                        # si childern es distinto de None cortamos el loop
                        if children is not None:
                            break
                    # si al terminar el loop children es None, es porque no tiene
                    # ni grafica ni audiovisual
                    if children is None:
                        children = emision
                    children.calcular_tarifa()
                    children.save()


    def __init__(self, *args, **kwargs):
        super(Horario, self).__init__(*args, **kwargs)
        self._tarifa = self.tarifa


    def __unicode__(self):
        return "%s - %s" % (self.medio.identificador,
                            self.programa.identificador)

    class Meta:
        verbose_name_plural = "Horarios"
        ordering = ["-fechaAlta"]


class Programa(models.Model):
    nombre = models.CharField(max_length=250, unique=True)
    identificador = models.CharField(max_length=100, null=True, blank=True)
    etiquetas = models.ManyToManyField('EtiquetaPrograma', null=True, blank=True, related_name="programas")
    tags = models.CharField(max_length=250, null=True, blank=True)
    objects = AccentManager()

    def __unicode__(self):
        if self.nombre:
            return self.nombre
        else:
            return ""

    def unificar(self, otro):
        if not self.alias.filter(nombre=otro.nombre):
            self.alias.create(nombre=otro.nombre)
        if not self.identificador:
            self.identificador = otro.identificador
            self.save()
        otro.horarios.update(programa=self)
        otro.delete()

    class Meta:
        verbose_name_plural = "Programas"
        ordering = ["nombre"]


class Origen(models.Model):
    tipo = models.CharField(max_length=1, choices=CHOICES_TIPO_ORIGEN,
                            default="F")
    fecha = models.DateField(db_index=True,
                                default=lambda: date.today())
    hora = models.TimeField(default="00:00")
    horario = models.ForeignKey('Horario', related_name='origenes', null=True)
    user = models.ForeignKey(User, related_name="origenes", null=True,
                            blank=True)

    archivo = models.CharField(max_length=128, unique=True, null=True)

    # Por las dudas el medio cambie de servidor y se pierda alguna referencia...
    servidor_original = models.ForeignKey("Servidor", related_name="+")

    def __unicode__(self):
        return unicode(self.archivo)

    def get_segmento(self):
        from adkiller.media_map.models import Segmento
        if self.fecha and self.hora:
            # contexto: buscar desde este punto,
            # emisiones 2 minutos para atrás y 10 minutos para adelante
            inicio = datetime.combine(self.fecha, self.hora) - timedelta(minutes=2)
            fin = datetime.combine(self.fecha, self.hora) + timedelta(minutes=10)
            emisiones = AudioVisual.objects.filter(
                horario__medio=self.horario.medio
            ).filter(
                Q(fecha__gte=inicio.date(), hora__gt=inicio.time()) &
                Q(fecha__lte=fin.date(), hora__lt=fin.time())
            ).order_by('fecha', 'hora')
            if emisiones:
                primera = emisiones[0]
                ultima = list(emisiones)[-1]
                inicio = min(datetime.combine(self.fecha, self.hora),
                             datetime.combine(primera.fecha, primera.hora) - timedelta(seconds=10))
                fin = datetime.combine(ultima.fecha, ultima.hora) + timedelta(seconds=ultima.duracion + 10)

            return Segmento(
                medio=self.horario.medio,
                desde=inicio,
                hasta=fin,
            )

    def set_horario(self, medio, programa=None, create_if_not_exists=False):
        e = Emision()
        e.fecha = self.fecha
        e.hora = self.hora
        e.set_horario(programa=programa, medio=medio)
        self.horario = e.horario

    def has_etiqueta(self, etiqueta):
        return etiqueta.lower() in [et.nombre.lower() for et in self.horario.programa.etiquetas.all()]

    def get_url(self):
        url = "http://%s/adtrack/ver_video.php?carpeta=%s&archivo=%s" % (
            self.servidor_original.url_completa(),
            self.carpeta,
            self.archivo
        )
        return url #url.replace("?", "%3F").replace("=", "%3D").replace("&", "%26")

    def send_mails(self):
        for e in self.emisiones.all():
            try:
                a = e.audiovisual
            except:
                a = None
            if a:
                a.send_mails()

    @property
    def carpeta(self):
        """
        En que carpeta esta este origen en el simpson.
        Depende del tipo (contenido, fuente o publicidad)
        """
        if self.tipo == 'P':
            if self.servidor_original.roles.filter(nombre="Corte"):
                return CARPETA_ARCHIVOS_INFOMERCIAL
            else:
                return CARPETA_ARCHIVOS_PUBLICIDAD
        elif self.tipo == 'C':
            return CARPETA_ARCHIVOS_CONTENIDO
        elif self.tipo == 'F':
            return CARPETA_ARCHIVOS_FUENTE

    class Meta:
        verbose_name_plural = "Orígenes"


class Emision(models.Model):
    tipo = models.CharField(max_length=1, choices=CHOICES_TIPO_EMISION,
                            default="P")

    migracion = models.ForeignKey('Migracion', null=True, blank=True,
                                related_name="emisiones")
    producto = models.ForeignKey('Producto', related_name='emisiones',
                                null=True)
    fecha = models.DateField(db_index=True,
                                default=lambda: date.today())
    hora = models.TimeField(default="00:00")
    orden = models.IntegerField(verbose_name="Número de Página", null=True,
                            blank=True, default=0)
    duracion = models.IntegerField(verbose_name="Duración", null=True,
                                    blank=True)
    valor = models.IntegerField(verbose_name="Tarifa", default=0)

    origen = models.CharField(max_length=200, blank=True, db_index=True)
        #Gráfica: Imagen (pdf); TV: video

    titulo = models.CharField(verbose_name="Título", max_length=255,
                            blank=True)  # se usa para contenido

    observaciones = models.CharField(max_length=500, blank=True, null=True)

    usuario = models.ForeignKey(User, null=True, blank=True)
    horario = models.ForeignKey('Horario', related_name='emisiones',
                                null=True)
    tipo_publicidad = models.ForeignKey('TipoPublicidad', verbose_name="tipo",
                            related_name='emisiones', null=True, blank=True)

    anunciantes = models.ManyToManyField(Anunciante, null=True, blank=True)

    fuente = models.ForeignKey(Origen, null=True, blank=True,
                                related_name="emisiones")
    archivo = models.CharField(max_length=200, null=True, blank=True)

    codigo_directv = models.CharField(max_length=30, null=True, blank=True)

    confirmado = models.BooleanField(default=True)

    def servidor(self):
        """Devolver servidor de esta emisión"""

        servidor = None
        try:
            servidor = self.audiovisual.servidor()
        except ObjectDoesNotExist:
            pass

        try:
            servidor = self.grafica.servidor()
        except ObjectDoesNotExist:
            pass

        if servidor is None:
            if self.migracion and self.migracion.servidor.roles.filter(nombre="Corte"):
                servidor = self.migracion.servidor

            elif self.horario and self.horario.medio.server:
                servidor = self.horario.medio.server
            else:
                servidor = Servidor.objects.filter(nombre='Edna')
                if servidor:
                    servidor = servidor[0]
                else:
                    servidor = Servidor.objects.all()[0]

        if servidor.nombre == "Maggie":
            servidor = Servidor.objects.get(nombre="Edna")

        return servidor

    def get_mediamap_coords(self):
        dt = self.hora_real() + timedelta(hours=3)
        return "http://infoad.tv/static/mediamap/#?interval=4&datetime=%s&channel=%d" % (dt.isoformat(), self.horario.medio.id)

    def get_codigo_directv(self):
        if self.codigo_directv:
            return self.codigo_directv
        elif self.producto and self.producto.codigo_directv:
            return self.producto.codigo_directv.split(',')[0]

    def tiene_codigo_directv(self, ident):
        return self.producto and self.producto.tiene_codigo_directv(ident)

    def save(self, *args, **kwargs):
        if not self.horario and self.fuente and self.fuente.horario:
            self.horario = self.fuente.horario
        super(Emision, self).save(*args, **kwargs)

    def get_dia(self):
        return self.fecha.weekday()

    def calcular_tarifa(self):
        # redefined in children
        pass

    def pagina_correcta(self, horario):
        # redefined in grafica
        return True

    def tarifa_real(self, user):
        if self.horario:
            descuentos = Descuento.objects.filter(medio=self.horario.medio,
                                                user=user)
            valor = self.valor
            if descuentos:
                valor = valor - (valor * descuentos[0].descuento) / 100.0
            return valor
        else:
            return self.valor

    def set_origen(self):
        if self.horario and self.fecha and self.horario.medio and\
        self.horario.programa:
            extension = 'mp4'
            if self.horario.medio.soporte.nombre == 'Radio':
                extension = 'mp3'
            #prog = self.horario.programa.identificador
            #if not prog:
            prog = "PRRE"
            self.origen = "%s-%s-%s-%s.%s" % (self.horario.medio.identificador,
                                              self.fecha.strftime("%d%m%y"),
                                              prog,
                                              self.hora.strftime("%H%M"), extension)

    # Defino el metodo hora para poder llamarlo desde emision y
    # redefinirlo para audiovisuales
    def hora_real(self):
        try:
            if self.audiovisual:
                if self.audiovisual.inicio > 0:
                    combinacion = datetime.combine(self.fecha, self.hora)
                    inicio = int(self.audiovisual.inicio)
                    suma = combinacion + timedelta(seconds=inicio)
                    return suma
        except:
            pass
        hora = self.hora
        if isinstance(self.hora, basestring):
            hora = datetime.strptime(hora, "%H:%M")
        try:
            dt = datetime.combine(self.fecha, hora)
        except:
            dt = hora
        return dt

    def formato(self):
        formato = "A"
        nombre = self.horario.medio.soporte.nombre
        if self.horario:
            if ("Diario" in nombre or "Revista" in nombre):
                formato = "G"
            if ("Radio" in nombre):
                formato = "R"
        return formato

    def thumb(self):
        """ Obtiene el Thumb de la emision, si es grafica, radio o
        tele sin video devuelve uno generico; via publica funciona """

        if "mp3" in self.origen:
            return "/static/design/thumb_radio.jpg"

        if "jpg" in self.origen:
            return "/static/design/thumb_diario.jpg"

        if self.segmentos.count():
            return self.segmentos.all()[0].main_thumb_url()
        elif self.origen:
            url = "http://" + self.servidor().url_completa() +\
                    "/multimedia/ar/Thumbs/" + self.origen + ".jpg"
            return url
        else:
            if self.formato() == "A":
                return "/static/design/thumb_tv.jpg"
            elif self.formato() == "R":
                return "/static/design/thumb_radio.jpg"
            elif self.formato() == "G":
                medio = self.horario.medio.identificador
                fecha = self.fecha.strftime("%d%m%y")
                seccion = self.horario.programa.identificador
                pag = self.orden
                if medio and fecha and seccion and pag:
                    url_thumb = "http://{url}/multimedia/ar/Grafica/{medio}" +\
                                    "-{fecha}-{seccion}-{pag}.JPG"
                    return url_thumb.format(url=self.servidor().url_completa(),
                                            medio=medio, fecha=fecha,
                                            seccion=seccion, pag=pag)
                else:
                    return "/static/design/thumb_diario.jpg"

        def __unicode__(self):
            if self.titulo:
                return self.titulo
            else:
                quien = self.migracion
                if not quien and self.usuario:
                    if self.usuario.first_name:
                        quien = "%s - %s" % (self.usuario.first_name,
                                                self.usuario.last_name)
                    else:
                        quien = self.usuario
            return "%s en %s (%s) - %s" % (self.producto, self.horario,
                                            self.hora_real(), quien)

    # Metodo para mostrar en el admin
    def medio(self):
        return self.horario.medio

    # Metodo para mostrar en el admin
    def programa(self):
        return self.horario.programa

    def cantidad_avisos_tanda(self):
        """
            Devuelve la cantidad de avisos que posee la tanda
        """
        return Emision.objects.filter(origen=self.origen).order_by('-orden')[0].orden

    def get_fuente(self):
        """Obtener archivo que contiene a esta emisión"""

        # TODO: refactorizar esto con Diego
        # esto es TEMPORARIO porque puede haber emisiones que no estan
        # asociadas a un origen. TODAS deberian tener origen.
        # Esta funcion crea u obtiene el fuente correspondiente a esta emision
        # hay horarios repetidos (misma fecha, hora, medio pero distinto id),
        # por eso hay que usar las dos consultas!!

        origen_old = self.origen
        self.set_origen()
        if origen_old and self.origen != origen_old:
            self.origen = origen_old
            try:
                origen = Origen.objects.get(archivo=self.origen)
            except ObjectDoesNotExist:
                # crearlo
                origen = Origen.objects.create(
                    fecha=self.fecha,
                    hora=self.hora,
                    horario=self.horario,
                    tipo=self.tipo,
                    servidor_original=self.servidor(),
                    archivo=self.origen
                )

            self.fuente = origen
            self.save()

        if not self.fuente:
            # esto no se ejecuta jamás
            fuente = Origen.objects.filter(archivo=self.origen)
            if fuente:
                self.fuente = fuente[0]
            else:
                fuente = Origen.objects.filter(
                    fecha=self.fecha,
                    hora=self.hora,
                    horario=self.horario,
                    tipo=self.tipo,
                    servidor_original=self.servidor()
                )
                if fuente and False:
                    self.fuente = fuente[0]
                else:
                    fuente = Origen.objects.filter(archivo=self.origen)
                    if fuente:
                        self.fuente = fuente[0]
                    else:
                        # estamos seguros q no existe la fuente, crearla
                        try:
                            self.fuente, _ = Origen.objects.get_or_create(fecha=self.fecha,
                                            hora=self.hora,
                                            horario=self.horario,
                                            tipo=self.tipo,
                                            servidor_original=self.
                                            servidor(),
                                            archivo=self.origen)
                        except:
                            self.fuente = None
            self.save()
        return self.fuente


    def set_horario(self, medio, programa=None, create_if_not_exists=False, programa_contains=None, fecha_alta=None):

        horarios = Horario.objects.filter(completa=True, fechaAlta__lte=self.fecha)
        if medio:
            horarios = horarios.filter(medio=medio)
        if programa:
            horarios = horarios.filter(programa=programa)
        if programa_contains:
            horarios = horarios.filter(programa__nombre__icontains=programa_contains)
        if fecha_alta:
            horarios = horarios.filter(fechaAlta=fecha_alta)

        if horarios:
            if self.hora != None:
                horarios = horarios.filter(Q(horaInicio__isnull=True) |
                        Q(horaInicio__lte=self.hora, horaFin__gte=self.hora))
            horarios = horarios.order_by("-fechaAlta")
            if horarios:
                # weekday devuelve 0 para lunes
                # al sumarle 1 módulo 7, queda 0 para el domingo
                dia = (self.fecha.weekday() + 1) % 7
                if dia > 0:
                    # salteo las comas
                    dia = dia * 2
                for horario in horarios:
                    if not horario.dias or horario.dias[dia] == "1":
                        if self.pagina_correcta(horario):  # para grafica
                            self.horario = horario
                            break

        if not self.horario and programa and medio:
            horarios = Horario.objects.filter(programa=programa, medio=medio)
            if horarios:
                horario = horarios[0]
            else:
                if create_if_not_exists:
                    horario, _ = Horario.objects.get_or_create(
                                programa=programa, medio=medio, completa=False, fechaAlta=self.fecha)
                else:
                    horario = None
            self.horario = horario

    def get_tanda_or_segmento(self):
        from adkiller.media_map.models import Segmento
        hasta = self.hora_real() + timedelta(seconds=self.duracion)
        tandas = Segmento.objects.filter(medio=self.horario.medio,
                                         tipo__nombre="Tanda",
                                         desde__lte=hasta,
                                         hasta__gte=self.hora_real())

        if tandas and self.tipo_publicidad.id in [1,18,16]:
            # si esta en una tanda
            # chequea los primary_key de los tipos de publicidad
            #  spot invitacional/ spot / presentacion-despedida
            return tandas[0]
        elif self.pk and self.segmentos.all():
            # no esta en una tanda
            # es una publicidad cargada desde MediaMap
            return self.segmentos.all()[0]


    class Meta:
        ordering = ['-fecha', '-hora', '-orden']
        verbose_name_plural = "Emisiones"


class Grafica(Emision):
    alto = models.IntegerField()
    ancho = models.IntegerField(verbose_name="Cantidad de columnas")
    color = models.BooleanField(default=False)
    contra_tapa = models.BooleanField(default=False)

    def servidor(self):
        """Devolver el servidor de esta gráfica"""
        return Servidor.objects.get(nombre='Edna')

    def calcular_tarifa(self):
        if self.horario and self.horario.tarifa and self.horario.ancho and\
            self.alto and self.ancho:
            tarifa = self.alto * self.ancho * self.horario.tarifa *\
                     self.horario.ancho
            if tarifa > 0:
                self.valor = tarifa
                medio = self.horario.medio
                incremento_color = medio.incremento_color
                if self.color and incremento_color and incremento_color > 0:
                    incremento = float(self.valor) * float(incremento_color) /\
                                    float(100)
                    self.valor = self.valor + int(incremento)

    def pagina_correcta(self, horario):
        correcta = True
        if horario.pagina_desde and horario.pagina_hasta:
            correcta = correcta and self.orden >= horario.pagina_desde
            correcta = correcta and self.orden <= horario.pagina_hasta
        if horario.pagina_desde and not horario.pagina_hasta:
            correcta = correcta and self.orden == horario.pagina_desde

        if horario.paginas == "I":
            correcta = correcta and self.orden % 2 == 1
        if horario.paginas == "P":
            correcta = correcta and self.orden % 2 == 0
        if horario.paginas == "C":
            correcta = correcta and self.contra_tapa
        return correcta


    def superficie(self):
        return self.alto * self.ancho

    def marca(self):
        return self.producto.marca

    def seccion(self):
        return self.horario.programa

    def save(self, *args, **kwargs):
        try:
            seccion = self.horario
        except:
            seccion = None
        if seccion and seccion.ancho:
            duracion = self.alto * self.ancho * seccion.ancho
            if duracion > 0:
                self.duracion = duracion
        if self.color:
            tipo = "Aviso Color"
        else:
            tipo = "Aviso ByN"
        self.tipo_publicidad = TipoPublicidad.objects.get(descripcion=tipo)
        super(Grafica, self).save(*args, **kwargs)


class AudioVisual(Emision):
    keys = (('producto', 'programa', 'fecha', 'hora', 'orden'),)
    inicio = models.DecimalField(max_digits=10, decimal_places=4, null=True,
                                blank=True)
    fin = models.DecimalField(max_digits=10, decimal_places=4, null=True,
                            blank=True)

    # confiabilidad del reconocimiento del patron
    score = models.DecimalField(max_digits=10, decimal_places=4, null=True,
                                blank=True)
    # duracion del patron
    duracion_adtrack = models.DecimalField(max_digits=10, decimal_places=4,
                                    null=True, blank=True)

    # url to the clip
    clip = models.CharField(max_length=300, null=True, blank=True)

    # is this audiovisual clip file cropped?
    is_clip_processed = models.BooleanField(verbose_name="Clip fue cortado",
                                            default=False)
    mail_fue_enviado = models.BooleanField(default=False)
    tanda_completa = models.BooleanField(default=True)

    def servidor(self):
        """Devolver el servidor de este AudioVisual"""

        result = None

        # si se migró de un server de corte, debe buscarse allí
        if self.migracion and self.migracion.servidor.roles.filter(nombre='Corte'):
            result = self.migracion.servidor

        # si tiene horario y el horario tiene medio y el medio tiene servidor,
        # debe buscarse allí
        elif self.horario and self.horario.medio:
            result = self.horario.medio.server


        # si es de video, devolver Edna
        elif self.origen and self.origen.endswith('mp4'):
            try:
                result = Servidor.objects.get(nombre="Edna")
            except ObjectDoesNotExist:
                pass

        # si es de radio, devolver Martin
        elif self.origen and self.origen.endswith('mp3'):
            try:
                result = Servidor.objects.get(nombre="Martin")
            except ObjectDoesNotExist:
                pass

        # si no hay server, devolver Edna
        if not result or result.nombre == "Maggie": # por qué el caso maggie?
            try:
                result = Servidor.objects.get(nombre="Edna")
            except ObjectDoesNotExist:
                result = Servidor.objects.all()[0]
        return result

    def save(self, *args, **kwargs):
        if not self.tipo_publicidad_id:
            self.set_tipo_publicidad()
        super(AudioVisual, self).save(*args, **kwargs)

    def calcular_tarifa(self):
        #if "Regular" in self.horario.programa.nombre:
        #    self.valor = 0
        #    return
        if self.duracion and self.horario.tarifa != None:
            self.valor = int(self.duracion) * int(self.horario.tarifa)


    def set_duracion(self):
        if self.producto.duracion:
            self.duracion = self.producto.duracion
        if self.tipo_publicidad and self.tipo_publicidad.descripcion == "Infomercial" and self.horario:
            if self.horario.medio.ciudad.nombre != "San Juan":
                self.duracion = 1780

        # ahora no se hace mas este chequeo de duraciones
        if False and self.horario and (not self.duracion or int(self.duracion) < 55) and\
            (not self.tipo_publicidad or self.tipo_publicidad.descripcion == "Spot"):
            marca = self.producto.marca.nombre.lower().strip()
            medio = self.horario.medio.nombre.lower().strip()
            if marca == "sprayette" or marca == "polishop":
                if  medio in ['a&e; mundo', 'a&e mundo', 'canal 9 sat',
                'history channel', 'el gourmet', 'cosmo', 'e!', 'crónica tv',
                'europa europa', 'quiero!', 'telefé interior']:
                    self.duracion = 60
                elif medio in ['discovery', 'a24', 'cn23', 'id', 'canal id',
                    'home & health', 'animal planet', 'volver', 'film & arts',
                    'discovery travel & living', 'fox']:
                    self.duracion = 90
                elif medio in ['canal 9 mza']:
                    self.duracion = 30

            if marca == "teve compras":
                if medio in ['a&e mundo', 'history channel', 'e!', 'quiero!']:
                    self.duracion = 60
                elif medio in ['a24', 'cn23', 'cosmo', 'el gourmet', 'volver',
                    'europa europa', 'film & arts', 'axn', 'crónica tv']:
                    self.duracion = 90

            if marca == "sprayette" or marca == "polishop" or \
                marca == "teve compras":
                if medio in ['fx channel', 'sony entertainment', 'fox life',
                    'universal channel', 'national geo', 'fx channel dtv']:
                    self.duracion = 120
                elif medio == "fox" or medio == "directv live" or \
                    medio == "américa interior" or \
                    self.horario.medio.ciudad.nombre.lower() != "buenos aires":
                    self.duracion = 90
                elif medio == "américa" or medio == "telefé":
                    self.duracion = 60
            if not self.fin is None and not self.duracion is None:
                self.inicio = self.fin - self.duracion

    def chequear_tipo_publicidad(self):
        if self.producto_id and self.producto.marca.sector_id and\
        self.producto.marca.sector.industria.nombre.lower() == "venta directa":
            INFOMERCIAL = TipoPublicidad.objects.get(descripcion="Infomercial")
            if self.tipo_publicidad == INFOMERCIAL:
                if int(self.duracion) < 280:
                    return False
            else:
                if int(self.duracion) >= 280:
                    return False
        return True

    def set_tipo_publicidad(self):
        if self.producto_id:
            if self.producto.tipo_publicidad_id and not self.tipo_publicidad_id:
                self.tipo_publicidad = self.producto.tipo_publicidad
            COMERCIAL = TipoPublicidad.objects.get(descripcion="Comercial")
            es_comercial = self.tipo_publicidad == COMERCIAL
            if self.producto.marca.sector_id:
                industria = self.producto.marca.sector.industria.nombre.lower()
            else:
                industria = None
            if self.producto.marca.sector_id and industria == "venta directa"\
                    or es_comercial:
                INFOMERCIAL = TipoPublicidad.objects.get(descripcion=
                                                                 "Infomercial")
                SPOT = TipoPublicidad.objects.get(descripcion="Spot")
                MICRO = TipoPublicidad.objects.get(descripcion="Micro")
                if self.tipo_publicidad == INFOMERCIAL or es_comercial:
                    if not self.duracion or int(self.duracion) < 280:
                        self.tipo_publicidad = SPOT
                elif self.tipo_publicidad == MICRO:
                    self.tipo_publicidad = SPOT
                else:
                    if int(self.duracion) >= 280:
                        self.tipo_publicidad = INFOMERCIAL

    def get_url_clip(self):
        return "http://{}/multimedia/ar/clips/{}".format(
                    self.fuente.servidor_original.url_completa(), self.clip)

    def send_mails(self):
        if not self.is_clip_processed or self.mail_fue_enviado:
            return

        email_ctx = Context({'title': self.titulo,
                        'observations': self.observaciones,
                        'link': self.get_url_clip()})
        from_email = 'info@infoxel.com'
        text_template = get_template('email_audiovisual_clip.txt')
        text_content = text_template.render(email_ctx)
        html_template = get_template('email_audiovisual_clip.html')
        html_content = html_template.render(email_ctx)
        for anun in self.anunciantes.all():
            for usuario in anun.usuarios.all():
                to = [usuario.email]
                subject = 'Medición de la inversión publicitaria en ' +\
                            'medios tradicionales'
                email = EmailMultiAlternatives(subject, text_content,
                                                from_email, to)
                email.attach_alternative(html_content, "text/html")
                email.send()
        # set this flag so we don't send this mail again
        self.mail_fue_enviado = True
        self.save()

    class Meta:
        verbose_name_plural = "Audiovisuales"


class Migracion(models.Model):
    fecha = models.DateField()
    hora = models.TimeField()
    servidor = models.ForeignKey("Servidor", related_name="servidor")
    completa = models.BooleanField(default=False)

    def __unicode__(self):
        if self.fecha != None and self.hora != None:
            return "%s_%s-%s" % (self.servidor, self.fecha, self.hora)
        else:
            return self.servidor

    def borrar_anteriores(self):
        """ borra la informacion de ese servidor de los anteriores 7 dias """
        query = Emision.objects.filter(migracion__servidor=self.servidor)
        query = query.filter(fecha__gt=self.fecha - timedelta(days=7))
        query = query.filter(fecha__lte=self.fecha)
        query.delete()

    class Meta:
        verbose_name_plural = "Migraciones"

class Rol(models.Model):
    nombre = models.CharField(max_length=40)

    class Meta:
        verbose_name_plural = 'Roles'

    def __unicode__(self):
        return self.nombre

class Servidor(models.Model):
    """ la idea es que cuando se cree el perfil se limite el filtro"""
    nombre = models.CharField(max_length=150, blank=True, null=True)
    url = models.CharField(max_length=150, blank=True, null=True)
    ip = models.CharField(max_length=100, blank=True, null=True)
    nro = models.PositiveIntegerField()
    automatico = models.BooleanField(default=False)
    fecha = models.DateField(db_index=True, blank=True, null=True)
    puerto_mysql = models.PositiveIntegerField(null=True, blank=True)
    puerto_ssh = models.PositiveIntegerField(null=True, blank=True)
    puerto_http = models.PositiveIntegerField(null=True, blank=True)
    plaza = models.ForeignKey(Ciudad, null=True, related_name="servidores",
                                blank=True)
    nombre_del_tecnico = models.CharField(max_length=150, blank=True, null=True, verbose_name='Nombre del técnico')
    e_mail_del_tecnico = models.EmailField(max_length=75, blank=True, null=True, verbose_name='E-mail del técnico')

    # el siguiente campo indica si el técnico del servidor recibió
    # un mail de alerta por servidor caido.
    # Se vuelve a False cuando enviamos el mail de agradecimiento.
    tecnico_alertado = models.BooleanField(default=False)

    recibir_patrones = models.BooleanField(default=False)

    online = models.BooleanField(default=True)
    ultima_vez_online = models.DateTimeField(blank=True,
                                             null=True,
                                             verbose_name='Último contacto con el server') # con el servidor online

    espacio_en_disco = models.IntegerField(null=True)
    maquina_virtual = models.CharField(max_length=50, blank=True, null=True)

    roles = models.ManyToManyField(Rol, blank=True)


    def url_completa(self):
        if self.puerto_http and self.puerto_http != 80:
            return "%s:%s" % (self.url, self.puerto_http)
        else:
            return self.url

    def __unicode__(self):
        return self.nombre

    def pedir_marca(self, marca):
        """ Lo hago con os.system para no quedarme esperando la respuesta"""
        url = "http://%s:%s" % (self.url, self.puerto_http)
        cmd = "wget --tries=3 --delete-after --timeout=5 " + url + "/cargar_marcas.php/?marca_id=" + str(marca.id)
        try:
            os.system(cmd)
        except:
            pass

    class Meta:
        verbose_name_plural = "Servidores"
        verbose_name = "Servidor"

    def mail_al_tecnico(self):
        if self.online:
            if self.tecnico_alertado:
                self.agradecer_al_tecnico(via='mail')
                self.tecnico_alertado = False
                self.save()
        else:
            self.alertar_al_tecnico(via='mail')
            self.tecnico_alertado = True
            self.save()

    def agradecer_al_tecnico(self, via='mail'):
        assert self.tecnico_alertado
        if via == 'mail':
            if not self.e_mail_del_tecnico:
                raise Exception('No tengo el e-mail del tecnico. SERVER: %s' % self.nombre)
                return
            email_ctx = Context({'nombre': ' %s' % self.nombre_del_tecnico.split()[0],
                            'servidor': '%s:%s' % (self.url, self.puerto_http) if self.puerto_http else self.url,
                            'responder': settings.RESPUESTAS_SERVER_CAIDO,
                            })
            from_email = 'errores@infoad.com.ar'
            text_template = get_template('email_agradecimiento_tecnico_servidor_caido.txt')
            text_content = text_template.render(email_ctx)
            html_template = get_template('email_agradecimiento_tecnico_servidor_caido.html')
            html_content = html_template.render(email_ctx)
            to = [self.e_mail_del_tecnico]
            to.append('mmodenesi@infoad.com.ar')
            subject = "Muchas Gracias!"
            email = EmailMultiAlternatives(subject, text_content, from_email, to)
            email.attach_alternative(html_content, "text/html")
            email.send()

    def alertar_al_tecnico(self, via='mail'):
        assert not self.online
        if via == 'mail':
            if not self.e_mail_del_tecnico:
                raise Exception('No tengo el e-mail del tecnico. SERVER: %s' % self.nombre)
                return
            if self.ultima_vez_online:
                hora = self.ultima_vez_online.strftime('%H:%M:%S')
                fecha = self.ultima_vez_online.strftime('%d/%m/%Y')
                fecha_hora = ' desde las %s del %s' % (hora, fecha)
            else:
                fecha_hora = ''
            email_ctx = Context({'nombre': ' %s' % self.nombre_del_tecnico.split()[0],
                            'servidor': '%s:%s' % (self.url, self.puerto_http) if self.puerto_http else self.url,
                            'responder': settings.RESPUESTAS_SERVER_CAIDO,
                            'ultima_vez_online': fecha_hora})
            from_email = 'errores@infoad.com.ar'
            text_template = get_template('email_alerta_tecnico_servidor_caido.txt')
            text_content = text_template.render(email_ctx)
            html_template = get_template('email_alerta_tecnico_servidor_caido.html')
            html_content = html_template.render(email_ctx)
            to = [self.e_mail_del_tecnico]
            to.append('mmodenesi@infoad.com.ar')
            subject = "Servidor Caido"
            email = EmailMultiAlternatives(subject, text_content, from_email, to)
            email.attach_alternative(html_content, "text/html")
            email.send()

    def mail_al_responsable_de_op(self, via='mail'):
        assert not self.online
        if via == 'mail':
            try:
                destino = settings.MAILS_OPERACIONES
            except:
                raise Exception('No tengo e-mails de los responsables de operaciones')

            if self.ultima_vez_online:
                hora = self.ultima_vez_online.strftime('%H:%M:%S')
                fecha = self.ultima_vez_online.strftime('%d/%m/%Y')
                fecha_hora = ' desde las %s del %s' % (hora, fecha)
            else:
                fecha_hora = ''
            email_ctx = Context({
                'servidor': '%s:%s' % (self.url, self.puerto_http) if self.puerto_http else self.url,
                'nombre_server': self.nombre,
                'mail_tecnico': self.e_mail_del_tecnico,
                'ultima_vez_online': fecha_hora})
            from_email = 'errores@infoad.com.ar'
            text_template = get_template('email_alerta_op_servidor_caido.txt')
            text_content = text_template.render(email_ctx)
            html_template = get_template('email_alerta_op_servidor_caido.html')
            html_content = html_template.render(email_ctx)
            to = destino
            to.append('mmodenesi@infoad.com.ar')
            subject = "Servidor Caido"
            email = EmailMultiAlternatives(subject, text_content, from_email, to)
            email.attach_alternative(html_content, "text/html")
            email.send()

class AliasMarca(models.Model):
    nombre = models.CharField(max_length=150)
    marca = models.ForeignKey(Marca, related_name="alias")

    def __unicode__(self):
        return "%s (%s)" % (self.nombre, self.marca)


class AliasMarca(models.Model):
    nombre = models.CharField(max_length=150)
    marca = models.ForeignKey(Marca, related_name="alias")

    def __unicode__(self):
        return "%s (%s)" % (self.nombre, self.marca)


class AliasProducto(models.Model):
    nombre = models.CharField(max_length=150)
    producto = models.ForeignKey(Producto, related_name="alias")
    marca = models.ForeignKey(Marca, null=True, blank=True, related_name="productos_aliasados")

    def __unicode__(self):
        return "%s (%s) (%s)" % (self.nombre, self.producto, self.marca)


class AliasMedio(models.Model):
    nombre = models.CharField(max_length=150)
    medio = models.ForeignKey(Medio, related_name="alias")

    def __unicode__(self):
        return "%s (%s)" % (self.nombre, self.medio)


class AliasPrograma(models.Model):
    nombre = models.CharField(max_length=150)
    programa = models.ForeignKey(Programa, related_name="alias")

    def __unicode__(self):
        return "%s (%s)" % (self.nombre, self.programa)


class PalabraClave(models.Model):
    palabra = models.CharField(max_length=255)
    marca = models.ForeignKey(Marca, related_name="palabras_clave")


class Descuento(models.Model):
    user = models.ForeignKey(User)
    medio = models.ForeignKey(Medio, related_name="descuentos")
    descuento = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name_plural = "Descuentos"
        unique_together = ['user', 'medio']


from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User


@receiver(post_save, sender=Medio)
def crear_descuento_medio(sender, instance, created, **kwargs):
    if created:
        for u in User.objects.all():
            Descuento.objects.get_or_create(medio=instance, user=u)


@receiver(post_save, sender=User)
def crear_descuento_user(sender, instance, created, **kwargs):
    if created:
        for m in Medio.objects.all():
            Descuento.objects.get_or_create(medio=m, user=instance)


def hay_emisiones_activas(**kwargs):
    emisiones = Emision.objects.filter(**kwargs)
    if emisiones:
        # emisiones.latest() ???
        ultima = emisiones.order_by('-fecha')[0]
        return ultima.fecha >= date.today() -\
                    timedelta(days=7)
    else:
        return False


class ReemplazarPorMedio(models.Model):
    medio = models.ForeignKey(Medio)
    producto = models.ForeignKey(Producto, related_name="reemplazos")
    reemplazar_por = models.ForeignKey(Producto)
    desde = models.DateField()  #default=lambda: date.today())
    hasta = models.DateField(null=True, blank=True)

    def reemplazar(self):
        emisiones = Emision.objects.filter(producto = self.producto, horario__medio=self.medio, fecha__gte=self.desde)
        if self.hasta:
            emisiones = emisiones.filter(fecha__lte=self.hasta)
        emisiones.update(producto=self.reemplazar_por)

    def __unicode__(self):
        return "%s - %s por %s" % (self.medio, self.producto, self.reemplazar_por)

    class Meta:
        verbose_name_plural = "Reemplazar productos"


class EtiquetaProducto(models.Model):

    nombre = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Etiquetas de Productos"
        ordering = ["nombre"]


class EtiquetaMedio(models.Model):

    nombre = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Etiquetas de Medios"
        ordering = ["nombre"]


class EtiquetaPrograma(models.Model):

    nombre = models.CharField(max_length=50)

    def __unicode__(self):
        return self.nombre

    class Meta:
        verbose_name_plural = "Etiquetas de Programas"
        ordering = ["nombre"]

    def unificar(self, otro):
        for p in otro.programas.all():
            p.etiquetas.remove(otro)
            p.etiquetas.add(self)
        otro.delete()

