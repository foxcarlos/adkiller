# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, old-ne-operator, no-self-use,
# pylint: disable=super-on-old-class, unused-argument, line-too-long
# pylint: disable=protected-access
"""Admin for app"""

import datetime
from grappelli_filters import RelatedAutocompleteFilter, FiltersMixin
import autocomplete_light

from django.contrib import admin
from django.contrib.admin import helpers
from django.contrib.admin.models import LogEntry, DELETION
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.db import models
from django.forms import CheckboxSelectMultiple
from django.forms import Textarea, TextInput, TimeInput
from django.template.response import TemplateResponse
from django.utils.html import escape

from adkiller.app.forms import AudioVisualForm, HorarioForm
from adkiller.app.forms import AudioVisualInlineForm
from adkiller.app.forms import GraficaForm
from adkiller.app.forms import MarcaForm
from adkiller.app.forms import OrigenForm
from adkiller.app.forms import ProgramaForm
from adkiller.app.models import Agencia
from adkiller.app.models import AliasMarca
from adkiller.app.models import AliasMedio
from adkiller.app.models import AliasProducto
from adkiller.app.models import AliasPrograma
from adkiller.app.models import Anunciante
from adkiller.app.models import AudioVisual
from adkiller.app.models import Ciudad
from adkiller.app.models import Descuento
from adkiller.app.models import EstiloPublicidad
from adkiller.app.models import EtiquetaMedio
from adkiller.app.models import EtiquetaProducto
from adkiller.app.models import EtiquetaPrograma
from adkiller.app.models import Grafica
from adkiller.app.models import GrupoMedio
from adkiller.app.models import Horario
from adkiller.app.models import Industria
from adkiller.app.models import Marca
from adkiller.app.models import Medio
from adkiller.app.models import Migracion
from adkiller.app.models import Origen
from adkiller.app.models import PalabraClave
from adkiller.app.models import Patron
from adkiller.app.models import Producto
from adkiller.app.models import Programa
from adkiller.app.models import RecursoPublicidad
from adkiller.app.models import ReemplazarPorMedio
from adkiller.app.models import Rol
from adkiller.app.models import Rubro
from adkiller.app.models import Sector
from adkiller.app.models import Servidor
# from adkiller.app.models import Sintonizador
from adkiller.app.models import Soporte
from adkiller.app.models import TipoPublicidad
from adkiller.app.models import ValorPublicidad
from adkiller.app.optimizer import LargeTableChangeList
from adkiller.app.optimizer import LargeTablePaginator
from adkiller.usuarios.models import add_log


class AliasMarcaInline(admin.TabularInline):
    model = AliasMarca


class PalabraClaveInline(admin.TabularInline):
    model = PalabraClave


class AliasMedioInline(admin.TabularInline):
    model = AliasMedio


class AliasProductoInline(admin.TabularInline):
    model = AliasProducto


class AliasProgramaInline(admin.TabularInline):
    model = AliasPrograma


class RubroAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Rubro)

    actions = ["unificar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Rubro.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Rubro, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class IndustriaAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Industria)
    list_display = ['__unicode__', 'rubro']
    actions = ["unificar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Industria.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Industria, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class SectorAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Sector)
    search_fields = ['nombre']
    list_display = ['__unicode__', 'industria']

    actions = ["unificar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Sector.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Sector, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class AnuncianteAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ['nombre', 'es_nacional', 'activo']
    actions = ["unificar", "marcar_como_nacional"]
    formfield_overrides = {
        models.ManyToManyField: {
            'widget': FilteredSelectMultiple("usuarios", is_stacked=False)},
    }

    def marcar_como_nacional(self, _, queryset):
        for q in queryset:
            q.es_nacional = True
            q.save()


    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Anunciante.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Anunciante,
                                correcto, 4, "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class MarcaAdmin(admin.ModelAdmin):
    search_fields = ['nombre']
    list_display = ['nombre', 'anunciante', 'sector']
    form = MarcaForm
    actions = ["unificar", "enviar_a_simpsons"]
    inlines = [AliasMarcaInline, PalabraClaveInline]
    list_filter = ['productos__emisiones__fecha']

#    def lookup_allowed(self, key, value):
#        if 'productos' in key:
#            return True
#        return super(MarcaAdmin, self).lookup_allowed(key, value)

    def enviar_a_simpsons(self, _, queryset):
        servidores = Servidor.objects.filter(automatico=True)
        for servidor in servidores:
            for m in queryset:
                print m
                servidor.pedir_marca(m)
                print "Ok"

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Marca.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Marca, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class PatronInline(admin.TabularInline):
    model = Patron


class ReemplazarPorMedioInline(admin.TabularInline):
    form = autocomplete_light.modelform_factory(ReemplazarPorMedio)
    model = ReemplazarPorMedio


class ProductoAdmin(FiltersMixin, admin.ModelAdmin):
    #form = autocomplete_light.modelform_factory(Producto)

    list_display = ['nombre', 'servers', 'creado', 'modificado', 'codigo_directv']
    exclude = ["nombre_del_patron", "id_en_simpson", "server"]
    search_fields = ['nombre', 'marca__nombre', 'codigo_directv']
    actions = ["unificar", 'copiar_patron']
    list_filter = (('marca', RelatedAutocompleteFilter),
                   'creado', 'modificado', 'tipo_publicidad',
                   'patrones__server',)
    date_hierarchy = 'creado'
    inlines = [PatronInline, AliasProductoInline]

    # define the raw_id_fields
    raw_id_fields = ('marca',)
    # define the autocomplete_lookup_fields
    autocomplete_lookup_fields = {'fk': ['marca'],}

    def copiar_patron(self, _, queryset):
        for elem in queryset:
            if elem.patron_principal():
                elem.patron_principal().copiar_en_simpsons()

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Producto.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Producto, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)

    def lookup_allowed(self, key, value):
        if 'marca' in  key:
            return True
        return super(ProductoAdmin, self).lookup_allowed(key, value)


class EmisionAdmin(admin.ModelAdmin):
    list_display = ['producto', 'horario', 'fecha', 'hora', 'origen', 'usuario']
    search_fields = ['producto__nombre']
    list_filter = ('fecha', 'tipo_publicidad',
                   'producto__marca__sector__industria',
                   'horario__medio__ciudad', 'producto__marca',
                   'horario__medio', 'migracion__servidor',
                   'horario__medio__soporte')
    date_hierarchy = 'fecha'


class AudioVisualAdmin(admin.ModelAdmin):

    #lazy_pagination = True
    paginator = LargeTablePaginator
    def get_changelist(self, _, **kwargs):
        return LargeTableChangeList

    form = AudioVisualForm
    fieldsets = (
        (None, {'fields':
                    (('tipo', 'fecha', 'hora', 'medio', 'programa', 'origen'),)
            }
        ),
        (None, {'fields':
                    (('tipo_publicidad', 'marca',
                      'producto', 'tanda_completa',
                      'score', 'valor'),
                      ('titulo', 'inicio', 'fin', 'duracion', 'orden'),
                    )
            }
        ),
        (None, {'fields':
                    ('observaciones',)
            }
        ),
    )
    list_display = ['producto', 'horario', 'fecha', 'hora', 'origen', 'usuario',
                    'valor', 'duracion', 'tipo_publicidad', 'a_number']
    search_fields = ['producto__nombre', 'producto__codigo_directv']
    list_filter = ('fecha', 'producto__marca', 'producto__marca__sector',
                   'horario__medio', 'tipo_publicidad', 'confirmado')
    date_hierarchy = 'fecha'
    list_per_page = 20
    radio_fields = {"tipo": admin.HORIZONTAL}

    readonly_fields = ('tanda_completa', 'valor')

    def a_number(self, audiovisual):
        """Devolver el A# del producto de este audiovisual"""
        return audiovisual.producto.codigo_directv
    a_number.short_description = 'A#'
    a_number.admin_order_field = 'producto__codigo_directv'

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.attname in ('inicio', 'fin',):
            kwargs['widget'] = TimeInput(attrs={'class': 'start-end-timepicker'})
        return super(AudioVisualAdmin, self).formfield_for_dbfield(db_field, **kwargs)


    def lookup_allowed(self, key, value):
        if key in ('horario',):
            return True
        return super(AudioVisualAdmin, self).lookup_allowed(key, value)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'usuario', None) is None:
            obj.usuario = request.user
        obj.save()


    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'usuario':
            kwargs['queryset'] = User.objects.filter(username=request.user.username)
        return super(AudioVisualAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_readonly_fields(self, _, obj=None):
        if obj is not None:
            return self.readonly_fields + ('usuario',)
        return self.readonly_fields

    def add_view(self, request, form_url="", extra_context=None):
        data = request.GET.copy()
        data['usuario'] = request.user
        request.GET = data
        return super(AudioVisualAdmin, self).add_view(request, form_url="",
                                                      extra_context=extra_context)

    actions = ["confirmar"]

    def confirmar(self, request, queryset):
        for q in queryset:
            q.confirmado = True
            q.save()


class ProgramaAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Programa)
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    list_display = ['nombre', "identificador"]
    list_filter = ['horarios__emisiones__horario__completa',
                   'horarios__medio__soporte']
    search_fields = ['nombre', 'identificador']
    actions = ["unificar"]
    inlines = [AliasProgramaInline]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Programa.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Programa, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class CiudadAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Ciudad)
    actions = ["unificar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Ciudad.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Ciudad, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)


class MedioAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Medio)
    formfield_overrides = {
        models.ManyToManyField: {'widget': CheckboxSelectMultiple},
    }
    list_display = ["nombre", "mediadelivery_id", "incremento_color",
                    "soporte", "dvr", "canal",
                    "identificador", "identificador_anterior",
                    "identificador_directv", "canal_directv", "grupo",
                    "encargado", "server", 'server_grabacion']
    search_fields = ['nombre', 'identificador', 'identificador_anterior']
    inlines = [AliasMedioInline, ReemplazarPorMedioInline]
    list_filter = ['ciudad', 'encargado', 'server', 'server_grabacion', 'dvr',
                   'soporte', 'publicar']
    exclude = ["grabando_en_cero", "grabando", "desfasando"]
    actions = ["unificar", "publicar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = Medio.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, Medio, correcto, 4,
                                "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context,
                                    current_app=self.admin_site.name)

    def publicar(self, request, queryset):
        for elem in queryset:
            medio = elem.nombre
            #medio = medio.encode("latin1").decode("latin1")
            import subprocess
            subprocess.Popen(['/home/infoxel/Envs/infoad/bin/python2.7', '/home/infoxel/projects/infoad/manage.py',
                'migrar', 'lineatiempo', 'Todos', 'medios="' + medio + '"'])


class SoporteAdmin(admin.ModelAdmin):
    form = autocomplete_light.modelform_factory(Soporte)
    list_display = ["nombre", "padre"]


class AliasProductoAdmin(admin.ModelAdmin):
#    form = autocomplete_light.modelform_factory(Producto)
    list_display = ['nombre']
    search_fields = ['nombre']


class extras(admin.ModelAdmin):
    list_display = ['nombre', 'descripcion']


class MigracionAdmin(admin.ModelAdmin):
    list_display = ['fecha', 'hora', 'servidor', 'completa']
    list_filter = ["servidor"]


class ServidorAdmin(admin.ModelAdmin):
    list_display = ['nro', 'nombre', 'plaza', 'automatico', 'url', 'ultima_vez_online']
    actions = ['elegir']

    def elegir(self, request, queryset):
        for elem in queryset:
            elem.automatico = not elem.automatico
            elem.save()

    elegir.short_description = "Not(Automatico)"


class GraficaAdmin(admin.ModelAdmin):
#    form = autocomplete_light.modelform_factory(Grafica)
    form = GraficaForm
    fields = (('medio', 'fecha'), 'programa', 'marca', 'producto',
              ('alto', 'ancho', 'orden', 'contra_tapa'),
              ('observaciones', 'color'))
    list_display = ['producto', 'marca', 'seccion', 'medio', 'alto', 'ancho',
                    'orden', 'duracion', 'color', 'valor', 'usuario', 'fecha',
                    'tipo_publicidad']
    search_fields = ['producto__nombre']
    list_filter = ('fecha', 'usuario')
    date_hierarchy = 'fecha'
    actions = ["corregir_fechas", "corregir_seccion", "select"]


#    class Media:
#        js = ["/static/grappelli/jquery/jquery-1.7.2.min.js", ]

    def corregir_fechas(self, request, queryset):
        if request.POST.get('post'):
            fecha = datetime.datetime.strptime(request.POST.get('fecha'), "%d/%m/%Y")
            for elem in queryset:
                elem.fecha = fecha
                elem.save()
        else:
            context = {
                'titulo': "Seguro",
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
                'hoy':datetime.date.today().strftime("%d/%m/%Y"),
            }
            return TemplateResponse(request, 'corregir_fechas.html', context,
                                    current_app=self.admin_site.name)


    def corregir_seccion(self, request, queryset):
        if request.POST.get('post'):
            if "seccion" in request.POST:
                if request.POST["seccion"]:
                    seccion = Programa.objects.get(id=int(request.POST["seccion"]))
                for elem in queryset:
                    elem.programa = seccion
                    elem.calcular_tarifa()
                    elem.save()
        else:
            context = {
                'titulo': "Seguro",
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
                'form':ProgramaForm(),
            }
            return TemplateResponse(request, 'corregir_seccion.html', context,
                                    current_app=self.admin_site.name)

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'usuario', None) is None:
            obj.usuario = request.user
        obj.save()

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'usuario':
            kwargs['queryset'] = User.objects.filter(username=request.user.username)
        return super(GraficaAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_readonly_fields(self, request, obj=None):
        if obj is not None:
            return self.readonly_fields + ('usuario',)
        return self.readonly_fields

    def add_view(self, request, form_url="", extra_context=None):
        data = request.GET.copy()
        data['usuario'] = request.user
        request.GET = data
        return super(GraficaAdmin, self).add_view(request, form_url="", extra_context=extra_context)


def confirmar_tarifas(modeladmin, request, queryset):
    """Para todos los horarios seleccionados,
    poner tarifaDudosa=False"""
    queryset.update(tarifaDudosa=False)

confirmar_tarifas.short_description = "Confirmar Tarifa"

class HorarioAdmin(admin.ModelAdmin):
    form = HorarioForm
    fields = (
        'medio',
        'programa',
        (
            'lunes',
            'martes',
            'miercoles',
            'jueves',
            'viernes',
            'sabado',
            'domingo'
        ),
        (
            'horaInicio',
            'horaFin',
            'fechaAlta'
        ),
        (
            'alto',
            'ancho',
            'cantColum',
            'pagina_desde',
            'pagina_hasta',
            'paginas'
        ),
        (
            'tarifa',
            'tarifaDudosa',
        ),
        'enElAire',
        'completa',
    )
    list_display = [
        'tarifa',
        'programa',
        'medio',
        'fechaAlta',
        'dias',
        'horaInicio',
        'horaFin',
        'alto',
        'ancho',
        'cantColum',
        'enElAire',
        'completa'
    ]
    list_filter = [
        'enElAire',
        'medio',
        'completa',
        'medio__soporte',
        'programa__identificador',
        'tarifaDudosa',
    ]
    date_hierarchy = 'fechaAlta'
    actions = [confirmar_tarifas]


class DescuentoAdmin(admin.ModelAdmin):
    list_display = ('user', 'medio', 'descuento',)
    list_filter = ['medio', 'user']


UserAdmin.list_display = ('username', 'email', 'first_name',
                          'last_name', 'last_login', 'is_active', 'is_staff')


class LogEntryAdmin(admin.ModelAdmin):

    date_hierarchy = 'action_time'

    readonly_fields = LogEntry._meta.get_all_field_names()

    list_filter = [
        'user',
        'content_type',
        'action_flag',
        'user__is_staff',
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'user',
        'content_type',
        'object_link',
        'action_flag',
        'change_message',
    ]

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser and request.method != 'POST'

    def has_delete_permission(self, request, obj=None):
        return False

    def object_link(self, obj):
        if obj.action_flag == DELETION:
            link = escape(obj.object_repr)
        else:
            ct = obj.content_type
            link = u'<a href="%s">%s</a>' % (
                reverse('admin:%s_%s_change' % (ct.app_label, ct.model), args=[obj.object_id]),
                escape(obj.object_repr),
            )
        return link
    object_link.allow_tags = True
    object_link.admin_order_field = 'object_repr'
    object_link.short_description = u'object'


class AudioVisualInline(admin.StackedInline):
    model = AudioVisual
    form = AudioVisualInlineForm
    fieldsets = (
        (None, {'fields': ('titulo', 'observaciones',)}),
        (None, {'fields': (('inicio', 'fin', 'duracion', 'clip',
                            'is_clip_processed', 'mail_fue_enviado',),)}),
        (None, {'fields': ('anunciantes',)}),
    )

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.attname == 'observaciones':
            # override the observaciones field to use a Textarea widget
            kwargs['widget'] = Textarea()
        elif db_field.attname == 'clip':
            # custom class to reference this fields from javascript
            kwargs['widget'] = TextInput(attrs={'class': 'vTextField clip-name'})
        elif db_field.attname in ('inicio', 'fin',):
            # custom class to reference this fields from javascript
            kwargs['widget'] = TimeInput(attrs={'class': 'start-end-timepicker'})
        return super(AudioVisualInline, self).formfield_for_dbfield(db_field, **kwargs)


class OrigenAdmin(admin.ModelAdmin):
    form = OrigenForm
    search_fields = ['archivo']
    actions = ['mandar_mails']
    list_display = ['archivo', 'fecha', 'hora', 'horario', 'user', 'servidor_original']
    fieldsets = (
        (None, {'fields':
                    (('fecha', 'hora', 'medio', 'programa',
                      'archivo', 'servidor_original'),)
            }
        ),
    )
    inlines = (AudioVisualInline,)
    list_filter = ['fecha', 'horario__medio', 'user', 'servidor_original', 'tipo']

    def mandar_mails(self, request, queryset):
        for origen in queryset:
            origen.send_mails()




# class SintonizadorAdmin(admin.ModelAdmin):
#     list_display = ['serial', 'receiver_id', 'access_card', 'codigo_estado',
#                     'canal_sintonizado', 'medio_asignado',
#                     'get_entrada_grabacion']
#     list_filter = ['serial', 'codigo_estado', 'ciudad']
#     search_fields = ['receiver_id']
#     change_form_template = 'admin/app/sintonizador/change_form.html'

#     def get_sintonizador(self,object_id):
#         try:
#             sintonizador = Sintonizador.objects.get(pk = object_id)
#         except:
#             raise Http404
#         return sintonizador


#     def change_view(self, request, object_id, form_url='', extra_context=None):
#         extra_context = extra_context or {}
#         extra_context['sintonizador'] = self.get_sintonizador(object_id)
#         return super(SintonizadorAdmin, self).change_view(request, object_id,
#             form_url, extra_context=extra_context)


#     def get_urls(self):
#         urls = super(SintonizadorAdmin, self).get_urls()
#         my_urls = patterns('',
#             (r'^estado_sintonizador/$', self.admin_site.admin_view(self.cambiar_estado)),
#         )
#         return my_urls + urls

#     def cambiar_estado(self,request):
#         comando = None
#         nuevo_canal = None
#         if request.method == "POST":
#             # import ipdb;ipdb.set_trace()
#             ip = request.POST.get('ip')
#             ip += ":8080"
#             if request.POST.has_key('command'):
#                 comando = request.POST.get('command')
#             if request.POST.has_key('new_channel'):
#                 nuevo_canal = request.POST.get("new_channel")
#             response = control_api.main_controller(ip,comando ,nuevo_canal)
#             return HttpResponse(json.dumps(response),content_type="application/json")

#         return HttpResponse("")


#         return render_to_response('admin/chage_form.html',context_instance=RequestContext(request))

class EtiquetaProgramaAdmin(admin.ModelAdmin):
    actions = ["unificar"]

    def unificar(self, request, queryset):
        if request.POST.get('post'):
            if "correcto" in request.POST:
                correcto = EtiquetaPrograma.objects.get(id=request.POST['correcto'])
                for q in queryset:
                    if q.id <> correcto.id:
                        add_log(request.user, EtiquetaPrograma, correcto, 4, "Unificar con %s" % q)
                        correcto.unificar(q)
        else:
            context = {
                'queryset': queryset,
                'action_checkbox_name': helpers.ACTION_CHECKBOX_NAME,
            }
            return TemplateResponse(request, 'unificar.html', context, current_app=self.admin_site.name)


admin.site.register(Agencia)
admin.site.register(AliasMarca, AliasProductoAdmin)
admin.site.register(AliasMedio)
admin.site.register(AliasProducto)
admin.site.register(Anunciante, AnuncianteAdmin)
admin.site.register(AudioVisual, AudioVisualAdmin)
admin.site.register(Ciudad, CiudadAdmin)
admin.site.register(Descuento, DescuentoAdmin)
admin.site.register(EstiloPublicidad, extras)
admin.site.register(EtiquetaMedio)
admin.site.register(EtiquetaProducto)
admin.site.register(EtiquetaPrograma, EtiquetaProgramaAdmin)
admin.site.register(Grafica, GraficaAdmin)
admin.site.register(GrupoMedio)
admin.site.register(Horario, HorarioAdmin)
admin.site.register(Industria, IndustriaAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(Marca, MarcaAdmin)
admin.site.register(Medio, MedioAdmin)
admin.site.register(Migracion, MigracionAdmin)
admin.site.register(Origen, OrigenAdmin)
admin.site.register(Producto, ProductoAdmin)
admin.site.register(Programa, ProgramaAdmin)
admin.site.register(RecursoPublicidad, extras)
admin.site.register(Rol)
admin.site.register(Rubro, RubroAdmin)
admin.site.register(Sector, SectorAdmin)
admin.site.register(Servidor, ServidorAdmin)
# admin.site.register(Sintonizador, SintonizadorAdmin)
admin.site.register(Soporte, SoporteAdmin)
admin.site.register(TipoPublicidad, extras)
admin.site.register(ValorPublicidad, extras)

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
