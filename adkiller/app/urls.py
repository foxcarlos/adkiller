# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, unused-wildcard-import
"""
Urls for app app
"""

from django.conf.urls import patterns
from django.conf.urls import url

from adkiller.app.views import actualizar_patron
from adkiller.app.views import audiovisuales_por_origen
from adkiller.app.views import comparar_fechas
from adkiller.app.views import compare_filter
from adkiller.app.views import control_de_calidad
from adkiller.app.views import control_remoto_decos
from adkiller.app.views import datos_tanda
from adkiller.app.views import export_csv
from adkiller.app.views import export_dtv
from adkiller.app.views import export_emisiones
from adkiller.app.views import export_ibope
from adkiller.app.views import export_olx
from adkiller.app.views import export_starcom
from adkiller.app.views import get_descuentos
from adkiller.app.views import get_duracion
from adkiller.app.views import get_origen
from adkiller.app.views import get_programa
from adkiller.app.views import get_server
from adkiller.app.views import get_tarifa
from adkiller.app.views import home
from adkiller.app.views import horarios
from adkiller.app.views import info_producto
from adkiller.app.views import obtener_fechas
from adkiller.app.views import set_descuentos
from adkiller.app.views import set_tanda_incompleta
from adkiller.app.views import set_unit
from adkiller.app.views import sin_permisos
from adkiller.app.views import tanda_completa
from adkiller.app.views import tandas_no_procesadas
from adkiller.app.views import thumb_emision
from adkiller.app.views import thumb_producto
from adkiller.app.views import tiempos_de_proceso
from adkiller.app.views import tiene_publicidad
from adkiller.app.views import trampolin
from adkiller.app.views import ultima_emision
from adkiller.app.views import unit_selector
from adkiller.app.views import url_list_emisiones
from adkiller.app.views import ver_video
from adkiller.app.views import video_30fps
from adkiller.app.views import video_emision
from adkiller.app.views import video_origen
from adkiller.app.views import video_producto

from adkiller.adtrack.views import get_anunciante_autocomplete
from adkiller.adtrack.views import get_etiquetas_producto
from adkiller.adtrack.views import get_marcas_autocomplete
from adkiller.adtrack.views import get_productos_autocomplete
from adkiller.adtrack.views import guardar_producto_ajax

from adkiller.filtros.views import add_filter
from adkiller.filtros.views import change_filter
from adkiller.filtros.views import get_ciudades
from adkiller.filtros.views import get_filters
from adkiller.filtros.views import get_options
from adkiller.filtros.views import get_options_csv
from adkiller.filtros.views import get_ordered_filters
from adkiller.filtros.views import graph
from adkiller.filtros.views import less_options
from adkiller.filtros.views import lookup_filtro_general
from adkiller.filtros.views import lookup_filtro_medio
from adkiller.filtros.views import more_options
from adkiller.filtros.views import open_filter
from adkiller.filtros.views import otras_marcas_value
from adkiller.filtros.views import remove_filter
from adkiller.filtros.views import reset_filters
from adkiller.filtros.views import set_ordered_filters
from adkiller.filtros.views import set_otras_marcas

urlpatterns = patterns(
    '',
    url(r'^$', home, name="home"),
    url(r'^get_ordered_filters/', get_ordered_filters),
    url(r'^set_ordered_filters/', set_ordered_filters),
    url(r'^get_filters/', get_filters),
    url(r'^add_filter/', add_filter),
    url(r'^change_filter/', change_filter),
    url(r'^remove_filter/', remove_filter),
    url(r'^compare_filter/', compare_filter),
    url(r'^open_filter/', open_filter),
    url(r'^url_list_emisiones', url_list_emisiones),
    url(r'^export_emisiones', export_emisiones),
    url(r'^export_dtv', export_dtv),
    url(r'^export_starcom', export_starcom),
    url(r'^export_olx', export_olx),
    url(r'^export_ibope', export_ibope),
    url(r'^exportar_csv', export_csv),
    url(r'^options/more/', more_options),
    url(r'^options/less/', less_options),
    url(r'^options_csv', get_options_csv),
    url(r'^options', get_options),
    url(r'^get_ciudades', get_ciudades),
    url(r'^graph', graph),
    url(r'^reset_filters', reset_filters),
    url(r'^reset', reset_filters),
    url(r'^comparar_fechas', comparar_fechas),
    url(r'^fechas', obtener_fechas),
    url(r'^set_otras_marcas/', set_otras_marcas),
    url(r'^otras_marcas_value/', otras_marcas_value),

    url(r'^set_unit', set_unit),
    url(r'^unit_selector', unit_selector),
    url(r'^get_descuentos', get_descuentos),
    url(r'^set_descuentos', set_descuentos),

    url(r'^ver_video', ver_video),
    url(r'^info_producto', info_producto),

    url(r'^video_producto/(?P<producto>\d+)/', video_producto),
    url(r'^thumb_producto/(?P<producto>\d+)/', thumb_producto),
    url(r'^video_emision/(?P<emision>\d+)/', video_emision),
    url(r'^video_emision/(?P<emision>\d+)', video_emision),
    url(r'^thumb_emision/(?P<emision>\d+)/', thumb_emision),
    url(r'^video_origen/(?P<origen>\d+)/', video_origen),

    url(r'^lookup/filtro_general/', lookup_filtro_general),
    url(r'^lookup/filtro_medio/', lookup_filtro_medio),
    url(r'^tarifa', get_tarifa),
    url(r'^get_origen', get_origen),
    url(r'^get_programa', get_programa),
    url(r'^get_duracion', get_duracion),
    url(r'^get_server', get_server),
    url(r'^actualizar_patron', actualizar_patron),

    url(r'^tiene_publicidad', tiene_publicidad),
    url(r'^audiovisuales_por_origen', audiovisuales_por_origen),
    url(r'^tanda_completa', tanda_completa),
    url(r'^set_tanda_incompleta', set_tanda_incompleta),
    url(r'^datos_tanda', datos_tanda),
    url(r'^ultima_emision', ultima_emision),
    url(r'^sin_permisos', sin_permisos),
    url(r'^staff/$', trampolin),
    url(r'^staff/control_remoto_decos$', control_remoto_decos),
    url(r'^staff/control_de_calidad$', control_de_calidad),
    url(r'^staff/horarios$', horarios),
    url(r'^tiempos_de_proceso/', tiempos_de_proceso),
    url(r'^tandas_no_procesadas/(?P<medio_id>\d+)/(?P<fecha>\w+)/', tandas_no_procesadas),

    # used to retrieve and populate the anunciantes
    # FilteredSelectMultiple widget with ajax in the admin
    url(r'^get_json_anunciantes/$',
        'adkiller.app.views.get_json_anunciantes',
        name='get_json_anunciantes'),

    url(r'^get_etiquetas_producto/', get_etiquetas_producto, name="get_etiquetas_producto"),
    url(r'^get_marcas_autocomplete/', get_marcas_autocomplete, name="get_marcas_autocomplete"),
    url(r'^get_productos_autocomplete/', get_productos_autocomplete, name="get_productos_autocomplete"),
    url(r'^get_anunciante_autocomplete/', get_anunciante_autocomplete, name="get_anunciante_autocomplete"),


    url(r'^guardar_producto_ajax/', guardar_producto_ajax),

    url(r'^video_30fps/', video_30fps)



)
