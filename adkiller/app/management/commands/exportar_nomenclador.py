import csv
from django.core.management.base import BaseCommand

from adkiller.app.models import *

# csv writer

class Command(BaseCommand):
    def handle(self, *args, **options):

        archivo = open('nomenclador.csv', 'w')

        writer = csv.writer(archivo, delimiter='\t', dialect='excel',  quoting=csv.QUOTE_ALL)
        writer.writerow(["Rubros", "Industrias", "Sectores", "Marcas"])


        for rub in Rubro.objects.all():
            for ind in rub.industrias.all():
                for sec in ind.sectores.all():
                    for marc in sec.marcas.all():
                        writer.writerow([rub, ind, sec, marc])
