# -*- coding: utf-8 -*-
from adkiller.app.models import Programa,Medio,Programa,Ciudad,Soporte,Emision, Grafica
from adkiller.app.models import Horario

from django.core.management.base import BaseCommand

from django.conf import settings
import datetime

def hora_in(hora, desde, hasta):
    return hora >= desde and hora <= hasta

class Command(BaseCommand):
    help = "actualizar_tarifas desde hasta solo_sin_tarifa medio" 
    def handle(self, *args, **options):
        desde = args[0]
        hasta = args[1]
        solo_sin_tarifa = args[2] == 'True'
        medio = args[3]
        actualizar_tarifas_grafica (desde, hasta, solo_sin_tarifa, medio)


def actualizar_tarifas_grafica(desde, hasta, solo_sin_tarifa, medio):
    graficas = Grafica.objects.all()
    if desde:
        graficas = graficas.filter(fecha__gte=desde)
    if hasta:
        graficas = graficas.filter(fecha__lte=hasta)
    if solo_sin_tarifa:
        graficas = graficas.filter(valor=0)
    if medio:
        graficas = graficas.filter(horario__medio__nombre=medio)

    graficas = graficas.order_by("-fecha")
    for elem in graficas:
        horarios = Horario.objects.filter(tarifa__gt=0, medio=elem.medio, programa=elem.programa)
        horarios_con_fecha = horarios.filter(fechaAlta__lte=elem.fecha)
        if horarios_con_fecha:
            horarios = horarios_con_fecha.order_by("-fechaAlta")
        if horarios:
            elem.horario = horarios[0]
            elem.calcular_tarifa()
            elem.save()
