# -*- coding:  utf-8 -*-

# Python Imports
import os, random, string
import csv
from datetime import datetime, timedelta
import unicodedata

# Django Imports
from django.contrib.contenttypes.models import ContentType
from django.core.management.base import BaseCommand
from django.template import Context
from django.contrib.auth.models import User
from optparse import make_option
from django.core.mail import EmailMultiAlternatives, EmailMessage,\
            get_connection

# Project Imports
from adkiller.filtros.models import Perfil, Periodo, Filtro, OpcionFiltro
from adkiller.app.models import Ciudad, Marca, Soporte, Medio, Industria, Sector
from adkiller.settings import MAIL_ALERTAS_CONF, SITE_URL


""" 
	Crear usuarios. 
	Se debe pasar la ruta completa del archivo csv con -p.
"""
class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--path', '-p', dest='path',
            help='Path completo del archivo csv'),
    )

    def handle(self, *args, **options):

    	# Abre el csv en solo lectura
        archivo = csv.reader(open(options['path'], "rb"))
        usuarios_nuevos = []
        for index,row in enumerate(archivo):
            soportes = row[0].split('-')
            industria = row[1]
            #sector = row[2]
            periodo_desde = row[2]
            periodo_hasta = row[3]
            ciudades = row[4].split('-')
            marca = row[5]
            nombre = row[6]
            apellido = row[7]
            email = row[8]
            
           	# Creamos un password
            chars = string.ascii_letters + string.digits
            random.seed = (os.urandom(1024))
            password =  ''.join(random.choice(chars) for i in range(10))

            #username = nombre[0].lower() + apellido.lower()
            #nkfd_form = unicodedata.normalize('NFKD', unicode(username))
            #username_sin_tildes =  u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

          	# Creamos el User
            user = User.objects.create_user(username=email, 
                email=email, password=password)
            user.first_name = nombre
            user.last_name = apellido
            user.save()

	      	# Creamos el perfil y periodo
            perfil = Perfil.objects.create(user=user, activo=True)

            Periodo.objects.create(perfil=perfil,
                init_fecha_desde=periodo_desde,
                init_fecha_hasta=periodo_hasta,
                fecha_desde=periodo_desde,
                fecha_hasta=periodo_hasta
            )

            # Creamos los filtros
            ct_marca = ContentType.objects.get(name='marca')
            ct_sector = ContentType.objects.get(name='sector')
            ct_industria = ContentType.objects.get(name='industria')
            ct_ciudad = ContentType.objects.get(name='ciudad')
            ct_soporte = ContentType.objects.get(name='soporte')
            
            filtro_marca = Filtro.objects.create(perfil=perfil, tipo=ct_marca)
            filtro_sector = Filtro.objects.create(perfil=perfil, tipo=ct_sector)
            filtro_industria = Filtro.objects.create(perfil=perfil, tipo=ct_industria)
            filtro_ciudad = Filtro.objects.create(perfil=perfil, tipo=ct_ciudad)
            filtro_soporte = Filtro.objects.create(perfil=perfil, tipo=ct_soporte)
            
            # Creamos opción filtro
            if marca != 'False':
                marca_id = Marca.objects.get(nombre=marca).id
                OpcionFiltro.objects.create(filtro=filtro_marca,opcion_id=marca_id,inicial=True)

            #sector_id = Sector.objects.get(nombre=sector).id
            #OpcionFiltro.objects.create(filtro=filtro_sector,opcion_id=sector_id,inicial=True)

            industria_id = Industria.objects.get(nombre=industria).id
            OpcionFiltro.objects.create(filtro=filtro_industria,opcion_id=industria_id,inicial=True)

            #sector_id = Sector.objects.get(nombre=industria).id
            #OpcionFiltro.objects.create(filtro=filtro_sector,opcion_id=sector_id,inicial=True)

            if len(ciudades) > 1:
                for ciudad in ciudades:
                    ciudad_id = Ciudad.objects.get(nombre=ciudad).id
                    OpcionFiltro.objects.create(filtro=filtro_ciudad,opcion_id=ciudad_id,inicial=True)
            else:
                ciudad_id = Ciudad.objects.get(nombre=ciudades).id
                OpcionFiltro.objects.create(filtro=filtro_ciudad,opcion_id=ciudad_id,inicial=True)

            if len(soportes) > 1:
                for soporte in soportes:
                    soporte_id = Soporte.objects.get(nombre=soporte).id
                    OpcionFiltro.objects.create(filtro=filtro_soporte,opcion_id=soporte_id,inicial=True)
            else:
                soporte_id = Soporte.objects.get(nombre=soportes).id
                OpcionFiltro.objects.create(filtro=filtro_soporte,opcion_id=soporte_id,inicial=True)
            
            nuevo = [user.username,user.email, password]
            usuarios_nuevos.append(nuevo)



        subject, from_email = "Usuarios Demos", "alertas@infoad.com.ar"

        html = '<ul>'
        for nu in usuarios_nuevos:
            html += '<li>'+ ' Username: '+ nu[0] + ' Email: ' + nu[1] + ' Pass: ' + nu[2] +'</li>'
        html += '</ul>'
        html_content = html
        msg = EmailMultiAlternatives(subject, html_content, from_email, ["diegolis@infoad.com.ar"])
        msg.attach_alternative(html_content, "text/html")
        msg.send()


