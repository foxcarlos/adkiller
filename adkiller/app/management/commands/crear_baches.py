# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long, too-many-locals, too-many-statements
"""
Crear baches para reanalizar ifep
"""

# python imports
from datetime import datetime
from datetime import timedelta
from optparse import make_option
import time

# django imports
from django.core.management.base import BaseCommand

# project imports
from adkiller.app.models import Medio
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento

# =============================================================================
# CONSTANTES
# =============================================================================


# Formato de fecha para los argumentos de línea de comandos
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

# Tamaño máximo de los baches
BACHE_MAX_DURATION = timedelta(minutes=10)


DATA_FEBRERO = None

def load_data_febrero():
    global DATA_FEBRERO
    if DATA_FEBRERO is None:
        DATA_FEBRERO = {}
        with open('data_febrero.csv', 'r') as f:
            fechas = []
            for line in f.readlines():
                line = line.replace('\r\n', '')
                line = line.replace('\n', '')
                if not fechas:
                    fechas = line.split(';')[1:]
                else:
                    data = line.split(';')
                    medio = data[0]
                    DATA_FEBRERO[medio] = {}
                    for (fecha, aut) in zip(fechas, data[1:]):
                        DATA_FEBRERO[medio][fecha] = aut == '1'
    return DATA_FEBRERO

def get_data_febrero():
    global DATA_FEBRERO
    if DATA_FEBRERO is None:
        DATA_FEBRERO = load_data_febrero()
    return DATA_FEBRERO

def autorizado(medio, desde, hasta):
    data = get_data_febrero()
    key = medio.nombre
    if key == u'América 2 [ARG]':
        key = 'América 2 [ARG]'
    elif key == u'América 24 [ARG]':
        key = 'América 24 [ARG]'
    elif key == u'Crónica TV [ARG]':
        key = 'Crónica TV [ARG]'
    elif key == u'Telefé [ARG]':
        key = 'Telefé [ARG]'

    return data[key][desde.strftime('%d-feb')]

class Command(BaseCommand):
    help = u"""
        Crear baches para su posterior análisis por baches ifep
        """
    option_list = BaseCommand.option_list + (
        make_option('--medio', help=u'Este medio'),
        make_option('--desde', help='analizar desde <YYYY-MM-DD HH:MM:SS>'),
        make_option('--hasta', help='analizar hasta <YYYY-MM-DD HH:MM:SS>'),
    )


    def handle(self, *args, **options):

        try:
            args_desde = datetime.strptime(options['desde'], DATE_FORMAT)
            args_hasta = datetime.strptime(options['hasta'], DATE_FORMAT)
            args_medio = options['medio']
        except KeyError:
            exit('Número incorrecto de argumentos')
        except ValueError as e :
            print e
            exit('No entiendo el formato de la fecha')

        try:
            args_medio = Medio.objects.filter(nombre=args_medio)
        except ObjectDoesNotExist:
            exit('No encuentro el medio "{}"'.format(args_medio))
        else:
            args_medio = args_medio[0]

        print '-----------------------------------------------------------'
        print args_medio
        print 'desde', args_desde.strftime(DATE_FORMAT)
        print 'hasta', args_hasta.strftime(DATE_FORMAT)

        tipo_bache, _ = TipoSegmento.objects.get_or_create(nombre='Bache-IFEP')

        # primero, borrar otros baches que pueda haber solapando este!
        anteriores = Segmento.objects.filter(
            tipo=tipo_bache,
            desde__gte=args_desde,
            hasta__lte=args_hasta,
            medio=args_medio
        )
        anteriores.delete()


        # obtener todas las tandas en ese período
        tandas = Segmento.objects.filter(
            tipo__nombre='Tanda',
            desde__gte=args_desde,
            hasta__lt=args_hasta,
            medio=args_medio
        ).order_by('desde')

        inicio_bache = args_desde
        fin_bache = None

        for tanda in tandas:
            fin_bache = tanda.desde - timedelta(seconds=10)
            if fin_bache - inicio_bache > timedelta(minutes=60):
                self.generar_baches(medio=args_medio,
                                   desde=inicio_bache,
                                   hasta=fin_bache)
            inicio_bache = tanda.hasta + timedelta(seconds=10)
            descripcion = tanda.descripcion or ''
            print u'\tTANDA {} - {} {}'.format(tanda.desde.strftime(DATE_FORMAT),
                                               tanda.hasta.strftime(DATE_FORMAT),
                                               descripcion)

        else:

            # esto se ejecuta cuando el for termina
            # ajá!!! no lo sabías!!! :-)
            fin_bache = args_desde
            if fin_bache - inicio_bache > timedelta(minutes=60):
                self.generar_baches(medio=args_medio,
                                   desde=inicio_bache,
                                   hasta=fin_bache)

    def generar_baches(self, medio, desde, hasta):
        """Generar baches"""

        tipo_bache, _ = TipoSegmento.objects.get_or_create(nombre='Bache-IFEP')

        inicio = desde - timedelta(seconds=1)
        while inicio < hasta:
            # no deben ser muy largos
            fin = min([inicio + BACHE_MAX_DURATION, hasta])
            inicio += timedelta(seconds=1)
            if (fin.strftime('%H:%M') < '06:30' and inicio.strftime('%H:%M') > '02:30'):
                print 'Excluyendo madrugada'
            elif 'Skinner' in medio.dvr and (10 < inicio.day < 20):
                print 'Excluyendo Skinner del 11 al 19'
            elif autorizado(medio, desde, hasta):
                nuevo_bache = Segmento(
                    desde=inicio,
                    hasta=fin,
                    medio=medio,
                    descripcion='Bache creado por command',
                    tipo=tipo_bache,
                )
                nuevo_bache.save()
                print '>>>>>>> BACHE: {} - {}'.format(nuevo_bache.desde.strftime(DATE_FORMAT),
                                                    nuevo_bache.hasta.strftime(DATE_FORMAT))
            else:
                print 'No autorizado'
            inicio = fin


