# -*- coding:  iso-8859-1 -*-
from cache import fncache
import csv
from adkiller.app.models import *

from django.core.management.base import BaseCommand

from django.conf import settings




class Command(BaseCommand):
    def handle(self, *args, **options):
        eliminar_cable()



def eliminar_cable():
    bsas = Ciudad.objects.get(nombre="Buenos Aires")
    cable,y = Ciudad.objects.get_or_create(nombre="Buenos Aires Cable")
    satelital,s = Ciudad.objects.get_or_create(nombre="Interior Satelital")
    medios =Medio.objects.filter(ciudad=cable)
    
    medio_satelital,x = Soporte.objects.get_or_create(nombre="TV Satelital")
    medio_cable,x = Soporte.objects.get_or_create(nombre="TV Cable")
    medio_aire,x = Soporte.objects.get_or_create(nombre="TV Aire")
    "TV Cable"
    for medio in medios:
        antiguo = Medio.objects.filter(nombre=medio.nombre,ciudad=bsas)
        if antiguo:
            horarios = Horario.objects.filter(medio=medio)
            for horario in horarios:
                horario.medio = antiguo[0]
                horario.save()
        else:
            medio.ciudad = bsas
            medio.soporte = medio_cable
            medio.save()
    
    medios = Medio.objects.filter(ciudad=satelital)
    
    for medio in medios:
        antiguo = Medio.objects.filter(nombre=medio.nombre,ciudad=satelital)
        if antiguo:
            horarios = Horario.objects.filter(medio=medio)
            for horario in horarios:
                horario.medio = antiguo[0]
                horario.save()
        else:
            medio.ciudad = bsas
            medio.soporte = medio_satelital
            medio.save()
        
    medios = Medio.objects.filter(soporte__nombre__icontains="tele")
    for medio in medios:
        medio.soporte = medio_aire
        medio.save()

    satelital.delete()
    cable.delete()
    
    

