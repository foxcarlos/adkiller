# -*- coding:  iso-8859-1 -*-
from cache import fncache
import csv
from adkiller.app.models import *

from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage

from django.conf import settings


class Command(BaseCommand):
    def handle(self, *args, **options):
        to =  settings.MAILS_DESARROLLO
        to = ['diegolis@gmail.com']

        general = ""
        for m in Medio.objects.filter(encargado__isnull=False):
            if not m.medios.filter(emisiones__fecha=datetime.date.today() - datetime.timedelta(days=1)):
                general +=  u"%s (%s)\n" % (m, m.encargado.email)
                email = EmailMessage('No llegan datos de %s' % m, "" , to=[m.encargado.email])
                email.send()

        if general:
            email = EmailMessage('Reporte canales faltantes', general, to=settings.MAILS_OPERACIONES)
            email.send()

        return

        mal_duraciones = []
        for e in AudioVisual.objects.filter(producto__duracion__isnull=False):
            if e.duracion <> e.producto.duracion and (not e.tipo_publicidad or not e.producto.tipo_publicidad or e.tipo_publicidad == e.producto.tipo_publicidad):
                mal_duraciones.append(e)
        if mal_duraciones:
            email = EmailMessage('Hay duraciones incorrectas', mal_duraciones , to=to)
            email.send()
        mal_duraciones = []
        for e in AudioVisual.objects.filter(producto__marca__sector__industria__nombre__iexact="Venta directa", horario__isnull=False, duracion__lt=55, tipo_publicidad__descripcion="Spot"):
            if e.producto.marca.nombre in ['Sprayette', 'TeVe Compras', 'Polishop']:
                mal_duraciones.append(e)
        if mal_duraciones:
            email = EmailMessage('Hay duraciones incorrectas Venta Directa', mal_duraciones , to=to)
            email.send()

        mal_tipo = []
        for e in AudioVisual.objects.filter(producto__marca__sector__industria__nombre__iexact="Venta directa", duracion__lt=280, tipo_publicidad__descripcion="Infomercial"):
            mal_tipo.append(e)
        for e in AudioVisual.objects.filter(producto__marca__sector__industria__nombre__iexact="Venta directa", duracion__gt=280).exclude(tipo_publicidad__descripcion="Infomercial"):
            mal_tipo.append(e)

        if mal_tipo:
            email = EmailMessage('Hay tipos incorrectos', mal_tipo , to=to)
            email.send()

        
        general = ""
        for m in Medio.objects.filter(encargado__isnull=False):
            if not m.medios.filter(emisiones__fecha=datetime.date.today() - datetime.timedelta(days=1)):
                general +=  u"%s (%s)\n" % (m, m.encargado.email)
                email = EmailMessage('No llegan datos de %s' % m, "" , to=[m.encargado.email])
                email.send()

        if general:
            email = EmailMessage('Reporte canales faltantes', general, to=settings.MAILS_OPERACIONES)
            email.send()
