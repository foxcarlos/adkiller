# -*- coding:  iso-8859-1 -*- # pylint: disable=too-many-lines
# pylint: disable=line-too-long, too-many-branches, too-many-statements
# pylint: disable=too-many-instance-attributes, bare-except, star-args
# pylint: disable=too-many-locals, no-self-use, too-many-arguments
# pylint: disable=unused-variable, redefined-builtin, missing-docstring
# pylint: disable=redefined-outer-name, unused-argument, broad-except
"""
Migrar lineatiempo de los simpsons a infoad
"""
# Python imports
from __future__ import unicode_literals
from BeautifulSoup import BeautifulSoup
from datetime import datetime, timedelta, date
from decimal import getcontext
from sets import Set
import base64
import codecs
import gzip
import os
import re
import traceback
import urllib2

# Django imports
from django import db
from django.conf import settings
from django.conf import settings
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from django.db import transaction
from django.db.models import Q
from django.db.models import F

# Project imports
from adkiller.app.management.commands.cache import fncache
from adkiller.app.models import Agencia
from adkiller.app.models import AliasMedio
from adkiller.app.models import AudioVisual
from adkiller.app.models import Ciudad
from adkiller.app.models import Emision
from adkiller.app.models import EstiloPublicidad
from adkiller.app.models import Grafica
from adkiller.app.models import Industria
from adkiller.app.models import Marca
from adkiller.app.models import Medio
from adkiller.app.models import Migracion
from adkiller.app.models import Producto
from adkiller.app.models import Programa
from adkiller.app.models import RecursoPublicidad
from adkiller.app.models import ReemplazarPorMedio
from adkiller.app.models import Rubro
from adkiller.app.models import Sector
from adkiller.app.models import Servidor
from adkiller.app.models import Soporte
from adkiller.app.models import TipoPublicidad
from adkiller.app.models import ValorPublicidad
from adkiller.usuarios.models import add_log

USER_SIMPSONS = settings.USER_SIMPSONS
PASSWORD_SIMPSONS = settings.PASSWORD_SIMPSONS
CREAR_AL_MIGRAR = settings.CREAR_AL_MIGRAR
CREAR_HORARIOS = True
try:
    CIUDAD_DEFAULT = settings.CIUDAD_DEFAULT
except AttributeError:
    CIUDAD_DEFAULT = ""
try:
    SOPORTE_DEFAULT = settings.SOPORTE_DEFAULT
except AttributeError:
    SOPORTE_DEFAULT = ""

ROW_DELIMITER = chr(9)
FIRST_ROW = 2
MAX_ROWS = 0
MES_DIA = False

medios_no_encontrados = Set()
programas_no_encontrados = Set()


MEDIOS_PLAYTOWN = [
    'Quiero! [LAT]', 'Volver [ARG]', 'Magazine [ARG]', 'Canal 9 [ARG]',
    'Canal 9 SAT [ARG]', 'América 2 SAT [ARG]', 'Canal 13 SAT [ARG]'
]

MEDIOS_CORDOBA = [
    'Canal 8 Córdoba [ARG]', 'Canal 10 Córdoba [ARG]', 'Canal 12 Córdoba [ARG]'
]

MEDIOS_RETAIL = [
    'Canal 7 Mendoza [ARG]', 'Canal 9 Mendoza  [ARG]', 'Canal 5 San Juan [ARG]',
    'Canal 8 San Juan [ARG]'
] + MEDIOS_CORDOBA

MEDIOS_AIRE = [
    'Telefé [ARG]', 'América 2 [ARG]', 'Canal 7 [ARG]', 'Canal 9 [ARG]',
    'Canal 13 [ARG]'
]

MEDIOS_OTROS_INTERIOR = [
    'Canal 9 Salta [ARG]', 'Canal 11 Salta [ARG]', 'Canal 10 Tucumán [ARG]',
    'Canal 8 Tucumán [ARG]', 'Canal 10 Mar del Plata [ARG]',
    'Canal 8 Mar del Plata [ARG]', 'Canal 5 Rosario [ARG]',
    'Canal 3 Rosario [ARG]', 'Canal 13 Corrientes [ARG]',
    'Canal 9 Resistencia [ARG]', 'Canal 9 Paraná [ARG]',
    'Canal 13 Santa Fe [ARG]'
]

MEDIOS_OTROS_CABLE = [
    'Europa Europa [LATS]', 'El Gourmet.com [ARG]', 'Fox Life [LAT]'
]

MEDIOS_POLITICA = MEDIOS_RETAIL + MEDIOS_AIRE + MEDIOS_OTROS_INTERIOR + MEDIOS_OTROS_CABLE

MEDIOS_A_MIGRAR = []
MEDIOS_A_MIGRAR_LIMPIO = []

def log(texto):
    """Agregar texto en el archivo de logging"""
    f = open(settings.MEDIA_ROOT + "migrar.log", "a")
    f.write("%s %s\n" % (datetime.now(), texto))
    f.close()

def not_null(value):
    """Dato no nulo en el sentido amplio"""
    return value and not value in ["S/D", "N/A", "UNKNOWN", "SOFT_DELETED", "DELETED"]

def row_useful(row):
    """Devolver si la fila es útil"""
    res = not_null(row.marca)
    res = res and row.producto not in "Inicio y Fin Espacio Publicitario"
    res = res and (row.origen or row.medio)
    return res

def parse_string(value):
    """...?"""
    if value:
        soup = BeautifulSoup(value)
        value = soup.contents[0]
#        value = value.decode('windows-1252').encode('utf-8')
        return value
    else:
        return ""


def parse_day(value):
    """String to date"""
    f = None
    if "-" in value:
        try:
            f = datetime.strptime(value, "%Y-%m-%d").date()
        except (ValueError, TypeError):
            try:
                f = datetime.strptime(value, "%d-%m-%Y").date()
            except (ValueError, TypeError):
                pass

    else:
        if len(value.split("/")[0]) == 4:
            formato = "%Y/%m/%d"
        else:
            if MES_DIA:
                dia_mes = "%m/%d"
            else:
                dia_mes = "%d/%m"
            if len(value) <= 8:
                formato = dia_mes + "/%y"
            else:
                formato = dia_mes + "/%Y"
        try:
            f = datetime.strptime(value, formato).date()
        except (ValueError, TypeError):
            pass

    return f


def parse_hour(value):
    """String to hour"""
    h = None
    if value and ":" in value:
        if len(value.split(":")) == 3:
            h = datetime.strptime(value, "%H:%M:%S").time()
        else:
            h = datetime.strptime(value, "%H:%M").time()
    return h


def get_medio(nombre):
    """string to Medio"""
    medio = None
    medios = Medio.objects.filter(
        Q(nombre__iexact=nombre) |
        Q(alias__nombre__iexact=nombre)
    )
    if medios:
        medio = medios[0]
    else:
        medios = Medio.objects.filter(nombre=nombre)
        if medios:
            medio = medios[0]
            AliasMedio.objects.create(nombre=nombre, medio=medio)
        else:
            ciudad = CIUDAD_DEFAULT
            if not ciudad:
                return None
            ciudad = Ciudad.objects.get(nombre=ciudad)
            soporte = SOPORTE_DEFAULT
            soporte, _ = Soporte.objects.get_or_create(nombre=soporte)
            medio, creo = Medio.objects.get_or_create(
                nombre=nombre,
                ciudad=ciudad,
                soporte=soporte
            )
            if not medio.identificador:
                medio.identificador = nombre
                medio.save()
            if not creo:
                AliasMedio.objects.create(nombre=nombre, medio=medio)
    return medio


def tomar_duracion_de_producto(producto):
    """Los productos pueden tener la duración
    aclarada en el nombre"""
    for duracion in re.findall(r'\((.*?)\)', producto):
        if duracion:
            if duracion[-1] == 's':
                duracion = duracion[:-1]
            if duracion.isdigit():
                return int(duracion)

class Row(object):
    """Fila del archivo csv que viene de los simpsons"""

    @staticmethod
    def transform_row(row):
        """si viene en formato string "('data', 'data', 'data'),"
        lo transforma en un arreglo ['data', 'data', 'data']"""
        last_word = u""
        is_string = False
        escape = False
        row = parse_string(row[1:-1])
        if row[-1] == ")":
            row = row[:-1]
        new_row = []
        for c in row:
            if escape:
                c = "´"
                escape = False
            if c == "'":
                is_string = not is_string
            elif c == "," and not is_string:
                new_row.append(last_word.strip())
                last_word = u""
            elif c == '\\':
                escape = True
            else:
                last_word += unicode(c)
        new_row.append(last_word.strip())
        return new_row

    def __init__(self, csv_row, diccionario):
        csv_row = csv_row.replace("\r", "")
        csv_row = csv_row.replace("\n", "")
        # antes usaba parse_string (para Chile no es necesario, pero tengo que ver que pasa con los simpsons)

        row = csv_row
        if "\t" in row:
            row = row.split("\t")
        elif ";" in row:
            row = row.split(";")
        else:
            row = row.split(",")
        cols = len(row)

        self.id = get_Key(row, diccionario, ["id", "Id"])
        self.marca = None
        self.rubro = None

        if len(row) == 1:
            return

        self.marca = get_Key(row, diccionario, ["marca", "MARCA", "Marca"])
        self.producto = get_Key(row, diccionario, ["producto", "PRODUCTO", "Producto", "Aviso"])

        if self.producto == "0":
            self.producto = "Producto"
        if self.producto:
            self.producto = self.producto[:120]
        self.tipo = get_Key(row, diccionario, ["tipo", "SOPORTE", "Tipo", "Soporte"])



        self.fecha = get_Key(row, diccionario, ["fecha", "FECHA", "Fecha", "FechaHora", "Fecha Hora"])
        self.dia = get_Key(row, diccionario, ["dia"]) # Necesario ?
        self.hora = None
        self.hora = get_Key(row, diccionario, ["hora", "Hora"])

        # si viene hours, minutes, seconds
        if not self.hora and get_Key(row, diccionario, ["Hours"]):
            self.hours = int(get_Key(row, diccionario, ["Hours"]))
            self.minutes = int(get_Key(row, diccionario, ["Minutes"]))
            self.seconds = int(get_Key(row, diccionario, ["Seconds"]))
            if self.hours >= 24:
                self.hours -= 24
                #if self.dia:
                #    self.dia += timedelta(days=1)
                #self.fecha += timedelta(days=1)
            self.hora = "%02d:%02d:%02d" % (self.hours, self.minutes, self.seconds)

        if not self.dia and self.fecha:
            self.dia = self.fecha.split(" ")[0]
        if not self.hora and self.fecha:
            if " " in self.fecha:
                self.hora = self.fecha.split(" ")[1]

        self.orden = get_Key(row, diccionario, ["orden", "PAGINA", "Orden"])
        if self.orden == "":
            self.orden = 0
        self.duracion = get_Key(row, diccionario, ["duracion", "Duracion", "Duración1", "Duración"])
        if self.duracion is None:
            self.duracion = 0

        duracion_de_producto = tomar_duracion_de_producto(self.producto)
        if duracion_de_producto and self.duracion != duracion_de_producto:
            self.duracion = duracion_de_producto

        self.formato = get_Key(row, diccionario, ["formato", "FORMATO", "IdSoporte"])

        self.rubro = get_Key(row, diccionario, ["rubro", "RUBRO", "Rubro", "Categoria"])
        if not self.rubro:
            self.rubro = "rubro"
        self.subrubro = get_Key(row, diccionario, ["subrubro", "INDUSTRIA", "Subrubro", "Industria"])
        self.sector = get_Key(row, diccionario, ["sector", "SECTOR", "Sector"])

        # esto es porque una vez nos mandaron datos sin sector desde Megatime
        if not self.sector:
            self.sector = "SECTOR"

        self.alto = 0
        self.ancho = 0
        self.superficie = 0

        try:
            self.valor = get_Key(row, diccionario, ["valor", "Valor"])
            self.valor = self.valor.replace(",", "")
            if len(self.valor) > 9:
                self.valor = '300000'
        except:
            self.valor = '0'

        if not self.valor:
            self.valor = 0

        self.observaciones = get_Key(row, diccionario, ["observaciones", "COMENTARIO", "Observaciones"])

        self.color = True
        self.seg = None
        self.origen = None
        self.ciudad = ""

        self.color = get_Key(row, diccionario, ["color", "Color"])
        self.seg = get_Key(row, diccionario, ["Seg", "seg", "Seg. - Cm/Columna"])
        if self.seg == '' or self.seg is None:
            self.seg = 0

        # si no está la duracion, tomo seg
        if not self.duracion and self.seg:
            self.duracion = self.seg


        self.medio = get_Key(row, diccionario, ["medio", "MEDIO", "Medio", "Canal", "Emisora", "Diario"])
        if self.medio:
            self.medio = self.medio.strip()
        self.programa = get_Key(row, diccionario, ["programa", "SECCION", "CALLE", "Programa", "Suplemento", "suplemento"]) # Nro
        self.origen = get_Key(row, diccionario, ["origen", "ID_IMAGEN", "Origen"])

        self.ciudad = get_Key(row, diccionario, ["ciudad", "CIUDAD", "Plaza", r"Plaza\r", "Ciudad"])
        if self.ciudad:
            self.ciudad = self.ciudad.strip()


        if self.ciudad == "Buenos Aires Cable":
            self.ciudad = "Buenos Aires"
            self.tipo = "TV Paga"
        elif self.ciudad == "Interior Satelital":
            self.ciudad = "Buenos Aires"
            self.tipo = "TV Satelital"

        if self.tipo and "Tele" in self.tipo:
            self.tipo = "TV Aire"

        if not self.tipo:
            self.tipo = "Televisión"

        self.agencia = None
        self.anunciante = None
        self.inicio = None
        self.fin = None
        self.score = None
        self.duracion_adtrack = None
        self.id_tipo_publicidad = None
        self.id_estilo_publicitario = None

        self.ids_valores_transmitidos = ""
        self.ids_recursos_utilizados = ""
        self.nombre_del_patron = ""

        if cols > 25 and row[25]:
            self.id_tipo_publicidad = get_Key(row, diccionario, ["id_tipo_publicidad"])

        self.agencia = get_Key(row, diccionario, ["agencia"])
        self.anunciante = get_Key(row, diccionario, ["anunciante", "ANUNCIANTE", "Empresa"])
        try:
            self.inicio = float(get_Key(row, diccionario, ["inicio"]))
        except:
            self.inicio = 0
        try:
            self.fin = float(get_Key(row, diccionario, ["fin"]))
        except:
            self.fin = 0
        self.score = get_Key(row, diccionario, ["score"])
        self.duracion_adtrack = get_Key(row, diccionario, ["duracion_adtrack"])

        self.id_tipo_publicidad = get_Key(row, diccionario, ["id_tipo_publicidad"])
        self.id_estilo_publicitario = get_Key(row, diccionario, ["id_estilo_publicitario"])
        self.ids_valores_transmitidos = get_Key(row, diccionario, ["ids_valores_transmitidos"])
        self.ids_recursos_utilizados = get_Key(row, diccionario, ["ids_recursos_utilizados"])


        self.nombre_del_patron = get_Key(row, diccionario, [" CodigoAviso ", "CodigoAviso", "Codigo Aviso"])
        if self.nombre_del_patron:
            self.nombre_del_patron = self.nombre_del_patron.replace(",", "") + ".wmv"
        else:
            row = get_Key(row, diccionario, ["patron_file", "ID_IMAGEN"])
            if row:
                row = row.split("\n")[0]
            else:
                row = ""
            if row == "N/A":
                row = ""
            self.nombre_del_patron = row

def get_file(source, path):
    """Obtener archivo de migración"""
    file_data = None
    if source == "url":
        print "source == url"
        # Set timeout en segundos (1 minuto)
        try:
            file_data = urllib2.urlopen(path, timeout=60)
        except urllib2.URLError as e:
            print "Url Error: %s" % e

    elif source == "file":
        print "source == file"
        os.system("iconv -f Windows-1250 -t UTF8 < %s > %s" % (path, path + "2"))
        if path[-3:] == ".gz":
            file_data = gzip.open(path, 'rb')
        else:
            file_data = codecs.open(path + "2", encoding='utf-8', mode="r")
#            file_data = codecs.open(path, encoding='utf-8',mode="r")

    return file_data



def get_Key(row, diccionario, label):
    """...."""
    res = None
    for item in label:
        if item in diccionario:
            if diccionario[item] < len(row):
                res = row[diccionario[item]]
    return res

# pylint: disable=protected-access
@fncache
def get_instance(model, crear=True, **kwargs):
    """Obtener instancia del model según los kwargs"""
    keys = {}
    non_keys = {}

    uniques = list(model._meta.unique_together)

    if hasattr(model.__class__, "keys"):
        uniques = uniques + list(model.__class__.keys)
    for parent in model._meta.get_parent_list():
        uniques = uniques + list(parent._meta.unique_together)

    for key, value in kwargs.items():

        # el campo es unico en sí mismo
        field = model._meta.get_field(key)

        # el campo es parte de un conjunto unique_toghether
        key_is_ut = any([key in ut for ut in uniques])
        if field.unique or key_is_ut:
            keys[key] = value
        else:
            non_keys[key] = value

    instance = None
    instances = model.objects.filter(**keys)
    if instances:
        instance = instances[0]
    else:
        if model in (Medio, Marca, Programa) or model == Producto and not 'marca' in kwargs:
            instance = model.objects.filter(alias__nombre=kwargs['nombre'])
            if instance:
                instance = instance[0]
        elif model == Producto:
            if kwargs['marca'].sector and kwargs['marca'].sector.nombre == "Partidos Políticos":
                instance = model.objects.filter(nombre=kwargs['nombre'],
                                                # marca__nombre__contains??
                                                marca__contains=kwargs['marca'])
                if instance:
                    instance = instance[0]
                else:
                    instance = model.objects.filter(alias__nombre=kwargs['nombre'],
                                                    alias__marca__contains=kwargs['marca'])
                    if instance:
                        instance = instance[0]
            else:
                instance = model.objects.filter(alias__nombre=kwargs['nombre'],
                                                alias__marca=kwargs['marca'])
                if instance:
                    instance = instance[0]

    if not instance:
        if crear:
            instance = model.objects.create(**kwargs)
            add_log(None, model, instance, 1, "Creado al migrar")
    return instance


def migrate_row(row, migracion, industria_a_migrar=None, tanda_a_migrar=None):
    """Migrar fila"""
    migrar = True

    sector = None
    if row.rubro:
        rubro = get_instance(Rubro, nombre=row.rubro)
        if row.subrubro:
            industria = get_instance(Industria, nombre=row.subrubro, rubro=rubro)
            if row.sector:
                sector = get_instance(Sector, nombre=row.sector, industria=industria)

    if migrar:
        if sector:
            marca = get_instance(Marca, crear=CREAR_AL_MIGRAR, nombre=row.marca, sector=sector)
        else:
            marca = get_instance(Marca, crear=CREAR_AL_MIGRAR, nombre=row.marca)

        if marca.sector_id and industria_a_migrar and marca.sector.industria.nombre != industria_a_migrar:
            migrar = False

    if migrar:
        if tanda_a_migrar and row.origen != tanda_a_migrar:
            migrar = False


    if migrar:
        producto = get_instance(Producto, crear=True, nombre=row.producto, marca=marca)

        # por ahora no grabamos agencia
        if not_null(row.agencia) and False:
            agencia = get_instance(Agencia, nombre=row.agencia)
            producto.agencia = agencia
            producto.save()

        patron = producto.patrones.filter(server=migracion.servidor)

        if patron:
            patron = patron[0]
        else:
            try:
                patron = producto.patrones.create(server=migracion.servidor)
            except:
                pass
        if patron and not patron.id_en_simpson and row.nombre_del_patron:
            if "segmento" not in row.nombre_del_patron:
                patron.filename = row.nombre_del_patron
                s = re.search(r'(.*)_(\d+)\.(.*)', patron.filename)
                if s:
                    patron.id_en_simpson = int(s.groups(0)[1])
                elif ".wmv" in patron.filename:
                    patron.id_en_simpson = int(patron.filename.split(".")[0])
                patron.save()
        #if producto.marca.anunciante and producto.marca.anunciante.es_nacional and settings.UNIFICAR_PATRONES_NACIONALES:
        #    # copia patrones solamente desde Barney, y recién a partir de la segunda vez que aparece el patrón, por si hay modificaciones en las primeras dos horas
        #    if migracion.servidor.nombre == "Barney" and migracion.servidor.nro <> 200 and not created:
        #        producto.copiar_en_simpsons()

        soporte = get_instance(Soporte, crear=CREAR_AL_MIGRAR, nombre=row.tipo)

    fecha = None
    hora = None
    if migrar and row.fecha:
        if " " in row.fecha:
            aux = row.fecha.split(" ")
            if len(aux) == 2:
                fecha = aux[0]
                hora = aux[1]
                fecha = parse_day(fecha)
                hora = parse_hour(hora)
        else:
            fecha = parse_day(row.fecha)
            hora = row.hora

        if not row.hora:
            hora = parse_hour("00:00:00")

        if not fecha or fecha > date.today():
            migrar = False

    if migrar and (fecha == None or hora == None):
        migrar = False

    # setear el medio
    if migrar:
        if not row.medio and row.origen:
            id = row.origen.split("-")[0]
            medios = Medio.objects.filter(identificador=id)
            if not medios:
                medios = Medio.objects.filter(identificador_anterior=id)
            if not medios:
                medios = Medio.objects.filter(nombre=id)
            if medios:
                row.medio = medios[0]
            else:
                print "MEDIO NO ENCONTRADO", id
                medios_no_encontrados.add(id)

        if isinstance(row.medio, basestring):
            row.medio = get_medio(row.medio)

    if not row.medio:
        migrar = False

    # setear programa
    if migrar:
        if row.medio.soporte.nombre not in ["Diario", "Revista"]:
            aux_programa = row.programa
            row.programa = row.medio.get_programa_from_horario(hora, fecha)
            if not row.programa:
                row.programa = get_instance(Programa, crear=CREAR_AL_MIGRAR, nombre=aux_programa)
        else:
            row.programa = get_instance(Programa, crear=CREAR_AL_MIGRAR, nombre=row.programa)



        # tipo y estilo publicitario
        if not_null(row.id_tipo_publicidad) and row.id_tipo_publicidad != "0":
            if row.id_tipo_publicidad != "":
                tipo_publicidad = get_instance(TipoPublicidad, crear=CREAR_AL_MIGRAR, nombre=row.id_tipo_publicidad)
                if not producto.tipo_publicidad:
                    producto.tipo_publicidad = tipo_publicidad
                    producto.save()

    if migrar and False:
        if not_null(row.id_estilo_publicitario) and row.id_estilo_publicitario != "0":
            if row.id_estilo_publicitario != "":
                estilo_publicitario = get_instance(EstiloPublicidad, crear=CREAR_AL_MIGRAR, nombre=row.id_estilo_publicitario)
                if not producto.estilo_publicidad:
                    producto.estilo_publicidad = estilo_publicitario
                    producto.save()


        if row.ids_valores_transmitidos and "," in row.ids_valores_transmitidos:
            for v in row.ids_valores_transmitidos.split(","):
                if v.strip() != "":
                    if int(v.strip()) <= 37:
                        valor = get_instance(ValorPublicidad, crear=CREAR_AL_MIGRAR, nombre=v)
                        producto.valores.add(valor)

        if row.ids_recursos_utilizados:
            for r in row.ids_recursos_utilizados.split(","):
                if r.strip() != "":
                    if int(r.strip()) <= 21:
                        recurso = get_instance(RecursoPublicidad, crear=CREAR_AL_MIGRAR, nombre=r)
                        producto.recursos.add(recurso)

    if migrar:
        if not row.origen:
            row.origen = ""

        valores = {
                'migracion': migracion,
                'producto': producto,
                'fecha': fecha,
                'hora': hora,
                'orden': row.orden,
                'duracion': row.duracion,
                'observaciones':row.observaciones,
                'valor': int(round(float(row.valor), 0)),
                'origen': row.origen,
            }
        if row.medio.soporte.nombre not in ["Diario", "Revista"]:
            getcontext().prec = 2
            valores['inicio'] = str(round(float(row.inicio), 2))
            valores['fin'] = str(round(float(row.fin), 2))
            if valores['inicio'] == u'':
                valores['inicio'] = 0
            if valores['fin'] == u'':
                valores['fin'] = 0
            if row.score:
                valores['score'] = str(round(float(row.score), 2))
                if float(valores['score']) > 1000000:
                    valores['score'] = "100"
                if valores['score'] == u'':
                    valores['score'] = 0
            if row.duracion_adtrack:
                valores['duracion_adtrack'] = str(round(float(row.duracion_adtrack), 2))
                if valores['duracion_adtrack'] == u'':
                    valores['duracion_adtrack'] = 0
            valores['tanda_completa'] = row.tanda_completa
            emision = AudioVisual.objects.create(**valores)
            emision.set_duracion()
            emision.set_tipo_publicidad()
            emision.save()
        else:
            valores['alto'] = row.alto
            valores['ancho'] = row.ancho
            if not row.color:
                row.color = True
            valores['color'] = row.color
            emision = Grafica.objects.create(**valores)

        if row.medio:
            # primero busco el horario que le corresponderia y le seteo la
            # tarifa de ese horario
            emision.set_horario(programa=row.programa, medio=row.medio, create_if_not_exists=CREAR_HORARIOS)
            emision.calcular_tarifa()
            # si es DTV (NO playtown) y el horario todavia no esta confirmado, le pongo PRRE
            #if row.medio.identificador_directv and not row.medio.es_para_playtown() and not emision.horario.estas_confirmado(emision.fecha):
            if row.medio.identificador_directv and not emision.horario.estas_confirmado(emision.fecha):
                emision.set_horario(programa_contains="Programación Regular", medio=row.medio)
            emision.save()



def obtenerFormato(linea):
    """Obtener formato del archivo mirando los títulos de las columnas"""
    linea = parse_string(linea)
    linea = linea.split("\n")[0] # le saco si tiene el enter
    if "\t" in linea:
        linea = linea.split("\t")
    else:
        linea = linea.split(";")
    res = {}
    contador = 0
    for elem in linea:
        res[elem] = contador
        contador += 1
    return res



def parse_csv(file_data, server=200, date=date.today(), time=datetime.now(), es_csv=True, industria_a_migrar=None, tanda_a_migrar=None):
    if server == 200:
        servidor = Servidor.objects.filter(nro=server, nombre="Archivos externos")
        if servidor:
            servidor = servidor[0]
        else:
            servidor = Servidor.objects.create(nro=server, nombre="Archivos externos", url="", ip="")
        servidor.automatico = False
    else:
        servidor = Servidor.objects.get(nro=server)
    migracion, created = Migracion.objects.get_or_create(fecha=date, hora=time, servidor=servidor)
    if not created:
        if migracion.completa:
            # ya ha sido migrado este archivo
            return
        # creo que esto no hace falta porque lo borra despues
        #else:
        #    migracion.emisiones.delete()
    print "EL SERVER ES :", server
    if False and server != 200 and datetime.now().hour in range(1, 5):
        # esto ya no lo hace
        print "Borrando migraciones anteriores (%s)" % migracion
        migracion.borrar_anteriores()

        return migrar_csv_transaction(file_data, migracion)
    else:
        return migrar_csv(file_data, migracion, industria_a_migrar=industria_a_migrar, tanda_a_migrar=tanda_a_migrar)


@transaction.commit_on_success
def migrar_csv_transaction(file_data, migracion):
    """..."""
    return migrar_csv(file_data, migracion)

class Tanda(object):
    """Represenetación de una tanda"""
    def __init__(self, migracion, industria_a_migrar, tanda_a_migrar=None):
        self._list = []
        self.migracion = migracion
        self.industria_a_migrar = industria_a_migrar
        self.tanda_a_migrar = tanda_a_migrar

    def add(self, row):
        """..."""
        self._list.append(row)

    def origen(self):
        """..."""
        return self._list[0].origen

    def medio(self):
        """..."""
        return self._list[0].medio

    def fecha(self):
        """..."""
        return self._list[0].fecha

    def migrar(self):
        """..."""
        if not self._list:
            return
        if not self.origen():
            return

        medios_a_migrar = MEDIOS_A_MIGRAR_LIMPIO

        if medios_a_migrar:
            # if self.origen().startswith('A2'):
                # log('comparando medio con origen')
            if not unicode(parse_string(self.medio())) in medios_a_migrar:
                # if self.origen().startswith('A2'):
                    # log('Falso')
                return
        completas = AudioVisual.objects.filter(
            origen=self.origen(),
            tanda_completa=True,
            migracion__isnull=False
        )
        incompletas = AudioVisual.objects.filter(
            origen=self.origen(),
            tanda_completa=False,
            migracion__isnull=False
        )

        #assert not completas or not incompletas, self.origen()

        if self.tanda_a_migrar:
            import ipdb; ipdb.set_trace()

        if completas and not incompletas:
            # si esa tanda ya estaba publicada como completa, no hago nada
            return
        else:
            tanda_completa = True
            alguna_fila_util = False
            for row in self._list:
                if (row.marca == "UNKNOWN" or row.marca == "SOFT_DELETED") and int(row.duracion) > 6:
                    tanda_completa = False
                if row_useful(row):
                    alguna_fila_util = True

            # si esa tanda ya estaba publicada como incompleta, la borro, y la migro (colocandola como incompleta o completa segun corresponda)
            if self.origen() and incompletas:  # antes decia and alguna_fila_util, pero no es correcto porque si se transforma todo a DELETED, debe borrar todo
                # solo borro si tiene origen, por si quedan registros viejos sin origen, que no los borre
                incompletas.delete()

            for row in self._list:
                row.tanda_completa = tanda_completa
                if row_useful(row):
                    migrate_row(row, self.migracion, industria_a_migrar=self.industria_a_migrar, tanda_a_migrar=self.tanda_a_migrar)

    def misma_tanda(self, row):
        """..."""
        try:
            row.origen
            return not self._list or row.origen and self.origen() == row.origen
        except:
            return not self._list


def migrar_csv(file_data, migracion, industria_a_migrar=None, tanda_a_migrar=None):
    """..."""
    servidor = migracion.servidor
    print "Leyendo archivo"
    line = 0
    lineas = file_data.readlines()
    print "Parseando lineas"
    if len(lineas) == 0:
        return "No hay lineas"
    header = lineas[0].replace("\r", "")
    diccionario = obtenerFormato(header)
    print "%s lineas" % len(lineas)


    medios_no_encontrados.clear()
    programas_no_encontrados.clear()

    tanda_actual = Tanda(migracion, industria_a_migrar, tanda_a_migrar=tanda_a_migrar)
    for csv_row in lineas:
        line += 1
        if line > 1:
            if MAX_ROWS > 0 and line > MAX_ROWS:
                break

            row = Row(csv_row, diccionario)
            if line % 1000 == 0:
                print line, datetime.now()
                # esto es para que borre las queries grabadas y no se agote la memoria
                db.reset_queries()

            # cuando llego a la ultima fila de la tanda
            if not tanda_actual.misma_tanda(row):
                tanda_actual.migrar()
                tanda_actual = Tanda(migracion, industria_a_migrar, tanda_a_migrar=tanda_a_migrar)

            if not tanda_a_migrar or row.origen == tanda_a_migrar:
                if tanda_a_migrar:
                    print row.origen
                tanda_actual.add(row)

    tanda_actual.migrar()



    print "Medios no encontrados:", list(medios_no_encontrados)
    print "Programas no encontrados:", list(programas_no_encontrados)


    migracion.completa = True
    migracion.save()

    mensaje = ""
    total = 0
    if servidor.nro != 200:

        for x in Medio.objects.filter(server=servidor):
            emisiones = Emision.objects.filter(horario__medio=x, migracion=migracion)
            mensaje = mensaje + "<p>" + x.nombre + " : <b>"+ str(emisiones.count()) + "</b> </p>"
            total += emisiones.count()

        otros = 0
        for medio in Medio.objects.exclude(server=servidor):
            emisiones = Emision.objects.filter(migracion=migracion, horario__medio=medio)
            otros = otros + emisiones.count()

        if otros:
            mensaje = mensaje + "<p>Otros: <b>"+ str(otros) + "</b> </p>"
            total += otros

        mensaje = mensaje + "<p> --------------------------------------------------- </p>"

        titulo = "<p><h1>%s - Se migraron %s registros</b> </h1></p>" % (servidor.nombre, total)
        mensaje = titulo + mensaje
    return mensaje


def migrar_servidor(servidor, industria_a_migrar=None, tanda_a_migrar=None):
    """..."""
    nro = servidor.nro
    print "Server "+servidor.nombre+" %s: %s" % (nro, servidor.url_completa())
    print "Obteniendo archivo"

    url = 'http://' + servidor.url_completa() + "/lineatiempo.php"
    print url
    request = urllib2.Request(url)
    base64string = base64.encodestring('%s:%s' % (USER_SIMPSONS, PASSWORD_SIMPSONS)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)

    file = get_file("url", request)
    file_name = settings.MEDIA_ROOT + 'lineatiempo.csv'
    res = ""

    if file:
        print "Transformando a UTF-8 archivo %s" % file_name
        f = open(file_name, 'w')
        for line in file.readlines():
            f.write(line)
        f.close()

        os.system("iconv -f Windows-1250 -t UTF8 < %s > %s" % (file_name, file_name + "2"))
        f = codecs.open(file_name + "2", 'r', "utf-8")
        res = parse_csv(f, nro, industria_a_migrar=industria_a_migrar, tanda_a_migrar=tanda_a_migrar)
        f.close()

        # respaldo del lineatiempo
        if datetime.now().hour in range(0, 3) or True:
            os.system("cp %s %s/%s-%s.csv" % (file_name + "2", "/home/infoad/projects/backup/lineatiempos", servidor.nombre, datetime.now().strftime("%Y_%m_%d-%M_%H_%S")))


    return res


def commandos(*args, **kwargs):
    """..."""
    # iconv -f Windows-1250 -t UTF8 < /home/diego/Descargas/lineatiempo_dump.xls >  /home/diego/Descargas/lineatiempo_dump2.xls
    mode = args[0]
    server = None
    industria_a_migrar = None
    tanda_a_migrar = None
    print "analizando argumentos"
    if args[0] == "lineatiempo":
        if len(args) > 1:
            server = args[1]
            if server == "Todos":
                server = ""
            print "Server", server
        for i in range(2, len(args)):
            param = args[i].decode("utf-8")
            nombre, valor = param.split("=")
            if nombre == "industria":
                industria_a_migrar = valor
                industria_a_migrar = industria_a_migrar.replace("'", "")
                print "Industria", industria_a_migrar
            elif nombre == "tanda":
                tanda_a_migrar = valor
                print "Tanda", tanda_a_migrar
            elif nombre == "medios":
                valor = valor.replace("'", "")
                # OBSERVACION: esto es un hack horrible, pero cuando se compara el
                # nombre del medio se parsea el string con beautiful soup y
                # aparece ese punto y coma. No puedo entender porque se
                # parsea el nombre del medio usando beautiful soup y no
                # tengo a nadie a quien preguntar a ahora :)
                # Marcos Modenesi, 2016-04-19
                valor = valor.replace('A&E', 'A&E;')
                for medio in valor.split(","):
                    if medio == "playtown":
                        MEDIOS_A_MIGRAR.extend(MEDIOS_PLAYTOWN)
                    elif medio == "retail":
                        MEDIOS_A_MIGRAR.extend(MEDIOS_RETAIL)
                    elif medio == "politica":
                        MEDIOS_A_MIGRAR.extend(MEDIOS_POLITICA)
                    elif medio == "directv":
                        for m in Medio.objects.filter(nombre__icontains="Directv Sport"):
                            MEDIOS_A_MIGRAR.append(m.nombre)
                    else:
                        MEDIOS_A_MIGRAR.append(medio)
                print "MEDIOS", MEDIOS_A_MIGRAR

    else:
        if len(args) > 1:
            path = args[1]
        if len(args) > 2:
            server = args[2]
        else:
            server = None

    if args[0] == "lineatiempo":
        servidores = Servidor.objects
        if server:
            servidores = servidores.filter(nombre=server)
        else:
            servidores = servidores.filter(automatico=True)
            servidores = list(servidores)
            barney = [s for s in servidores if s.nombre == 'Barney']
            if barney:
                barney = barney[0]
                servidores.remove(barney)
                servidores = [barney] + servidores

        resumen = ""

        if MEDIOS_A_MIGRAR:
            try:
                medios_a_migrar_limpio = [m.encode("windows-1252").decode("utf-8") for m  in MEDIOS_A_MIGRAR]
            except UnicodeDecodeError:
                medios_a_migrar_limpio = [m for m in MEDIOS_A_MIGRAR]

            for m in medios_a_migrar_limpio:
                MEDIOS_A_MIGRAR_LIMPIO.append(m.strip('"').strip("'"))

        for servidor in servidores:
            log("Servidor %s" % servidor)
            try:
                resumen += migrar_servidor(servidor, industria_a_migrar=industria_a_migrar, tanda_a_migrar=tanda_a_migrar)
            except Exception as exception:
                resumen += "Error al migrar %s \n" % servidor
                if traceback:
                    print "Error migrando server %s" % servidor
                    print "exception:", exception
                    print "mensaje", exception.message
                    error = 'Mensaje de excepcion: %s Tipo:(%s)' % (exception.message, type(exception))
                    error += "\n " + str(exception)
                    error += traceback.format_exc()
                    print error
                    log("Error migrando server %s" % servidor)
                    log("exception: %s" % exception)
                    log("mensaje %s "  %exception.message)
                    log(error)

                else:
                    error = ""
                to = settings.MAILS_DESARROLLO
                if to:
                    email = EmailMessage('Error al migrar %s en %s' % (servidor, settings.PAIS), error, to=to)
                    email.send()


        try:
            actualizar_duraciones_y_tipos()
        except Exception as exception:
            error = traceback.format_exc()
            to = settings.MAILS_DESARROLLO
            if to:
                email = EmailMessage('Error al actualizar duraciones y tipos', error, to=to)
                email.send()



    elif args[0] in ["url", "file"]:
        print "Getting file"
        if settings.PAIS == 'Chile':
            path = path.decode("utf-8")
            SOPORTE_DEFAULT = path.split('/')[-1].split('_')[0]
            if SOPORTE_DEFAULT == 'Cable':
                SOPORTE_DEFAULT = 'Tv Paga'
            elif SOPORTE_DEFAULT == 'Prensa':
                SOPORTE_DEFAULT = 'Diario'
            elif SOPORTE_DEFAULT == 'TV':
                SOPORTE_DEFAULT = u'Televisión'
            elif SOPORTE_DEFAULT == 'Television':
                SOPORTE_DEFAULT = u'Televisión'

            if not SOPORTE_DEFAULT in [u"Vía Publica", "Metro"]:
                print "Soporte:", SOPORTE_DEFAULT
                print "Parsing file"
                if not server:
                    server = 200
                path = unicode(path)
                os.system(u"iconv -f Windows-1250 -t UTF8 < {path} >  {path}2".format(path=path).encode("utf-8"))
                f = codecs.open(path + "2", 'r', "utf-8")
                parse_csv(f, server)
        else:
            file = get_file(mode, path)
            print "Parsing file"
            if not server:
                server = 200
            parse_csv(file, server)
    elif args[0] in ["via"]:
        print "queres ver via publica"
        nombre = "2012-12-20.csv"
        print "Obteniendo archivo"
        file = get_file("url", "http://174.37.209.218/media/via/csv/2012-12-21.csv")
        file_name = settings.MEDIA_ROOT + 'via.csv'
        print "Transformando a UTF-8 archivo"
        f = open(file_name, 'w')
        for line in file.readlines():
            f.write(line)
            print line
        f.close()
        os.system("iconv -f Windows-1250 -t UTF8 < %s > %s" % (file_name, file_name + "2"))
        f = codecs.open(file_name + "2", 'r', "utf-8")
        parse_csv(f, 2)
        f.close()
    else:
        files = sorted(os.listdir(path))
        m_prev = None
        for file in files:
            parse_re = r"(\d+)_(\d+)-(\d+)-(\d+)_(\d+):(\d+):(\d+).csv.gz"
            m = re.match(parse_re, file)

            if m:
                # es un archivo de adtrack
                new_day = True
                if m_prev:
                    # chequea si cambio de dia
                    if [m.group(i) for i in range(1, 5)] == [m_prev.group(i) for i in range(1, 5)]:
                        new_day = False

                if new_day:
                    m_prev = m
                    print "Leyendo archivo %s" % file
                    server = m.group(1)
                    fecha = date(int(m.group(2)), int(m.group(3)), int(m.group(4)))
                    hora = datetime.time(int(m.group(5)), int(m.group(6)), int(m.group(7)))
                    file = get_file("file", os.path.join(path, file))
                    parse_csv(file, 200, fecha, hora, es_csv=False)


class Command(BaseCommand):
    args = 'file|dir|url|lineatiempo <path> <server>'
    help = ('Migrar un archivo CSV de emisiones.\n'
            'Usar file para un archivo en disco,\n'
            '     dir para un directorio de backups,\n'
            '     url para bajar el archivo de una url\n'
            '     lineatiempo para que descargue automaticamente los ultimos CSV de cada servidor')

    def handle(self, *args, **options):
        """..."""
        print "Inicio"
        commandos(*args, **options)

        log("Termino")




def actualizar_duraciones_y_tipos():
    """..."""

    log("Reemplazar por medio")
    for e in ReemplazarPorMedio.objects.all():
        e.reemplazar()

    print "Duraciones venta directa"
    log("Duraciones venta directa")
    for e in AudioVisual.objects.filter(producto__marca__sector__industria__nombre__iexact="Venta directa", horario__isnull=False, duracion__lt=55):
        e.set_duracion()
        e.save()

    log("Duraciones infos")
    print "Duraciones infos"
    for e in AudioVisual.objects.filter(producto__marca__sector__industria__nombre__iexact="Venta directa", tipo_publicidad__descripcion="Infomercial", duracion__lt=1780):
        e.set_duracion()
        e.save()

    log("Otras duraciones")
    print "Otras duraciones"
    for e in AudioVisual.objects.filter(producto__duracion__isnull=False).exclude(producto__duracion=F("duracion")):
        e.set_duracion()
        e.save()

    log("Termino")
