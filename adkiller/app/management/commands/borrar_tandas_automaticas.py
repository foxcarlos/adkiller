# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import AudioVisual

INPUT_FILE = open('/home/infoad/tandas_borradas_20140129_115436.txt', 'r')

class Command(BaseCommand):
    def handle(self, *args, **options):
        for line in INPUT_FILE.readlines():
            archivo = line.strip('\n')
            AudioVisual.objects.filter(origen=archivo).delete()
