# -*- coding:  iso-8859-1 -*-
from adkiller.app.models import Programa,Medio,Programa,Ciudad,Soporte,Emision
import csv
from adkiller.app.models import Horario

from django.core.management.base import BaseCommand

from django.conf import settings




def migrar_horarios():
    """ Esta funcion estaba hecha para cargar las tarifas de grafica a adbout, 
        Ya no se usa"""
    proyect = settings.PROJECT_PATH
    path = proyect + "adkiller/app/fixtures/"
    archivos = ["apu", "barney", "frink","lisa","maggui","bart"]

    for elem in archivos:
        print "DESCARGANDO", elem
        with open(path+"horarios-"+elem+".csv", 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter='\t')
            lista = []
            for line,row in enumerate(spamreader):
                programa     = row[0]
                ident_programa  = row[1]
                tarifa    = row[2]
                medio      = row[3]
                ciudad = row[11]
                print ciudad
                tipo = row[10]
                print tipo
                
                
                if ciudad == "Buenos Aires Cable":
                    ciudad = "Buenos Aires"
                    tipo = "TV Paga"
                elif ciudad == "Interior Satelital":
                    ciudad = "Buenos Aires"
                    tipo = "TV Satelital"

                if tipo and "Tele" in tipo:
                    tipo = "TV Aire"

                
                
                
                soporte,x = Soporte.objects.get_or_create(nombre=tipo)
                if Medio.objects.filter(nombre=medio).count() >= 1:
                    medio = Medio.objects.filter(nombre=medio)[0]
                else:
                    ciudad,x = Ciudad.objects.get_or_create(nombre=ciudad)
                    medio,x = Medio.objects.get_or_create(nombre=medio,ciudad=ciudad,soporte=soporte)
                ident_dvr   = row[12]
                ident_medio = row[4]
                canal       = row[13]
                
                
                medio.identificador = ident_medio
                medio.dvr = ident_dvr
                medio.canal = canal
                print medio
                print elem
                print line

                medio.save()
                
                if Programa.objects.filter(nombre=programa).count() > 1:
                    programa = Programa.objects.filter(nombre=programa)[0]
                else:
                    programa,b = Programa.objects.get_or_create(nombre=programa)
                programa.identificador = ident_programa
                programa.save()

                en_el_aire = not(bool(int(row[9])))
                if medio:
                    dias = row[7]
                    hora_inicio   = row[5]
                    hora_fin   = row[6]
                    fecha_alta = row[8]
                    horarios = Horario.objects.filter(programa=programa,medio=medio,dias=dias)
                    if not horarios:
                        horario,c = Horario.objects.get_or_create(programa=programa,medio=medio,dias=dias)
                    else:
                        horario = horarios[0]
                    horario.tarifa = tarifa
                    horario.horaInicio = hora_inicio
                    horario.horaFin = hora_fin
                    horario.fechaAlta = fecha_alta
                    horario.enElAire = en_el_aire
                    horario.save()
                
                print ident_medio
                print ident_dvr
                print canal



    



class Command(BaseCommand):
    def handle(self, *args, **options):
        migrar_horarios()
