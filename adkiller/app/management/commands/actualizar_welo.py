# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import Medio

class Command(BaseCommand):
    def handle(self, *args, **options):
        all = Medio.objects.all()
        for i in all:
            i.update_in_welo()
