# -*- coding: iso-8859-15 -*-
"""Mandar mail a operaciones alertando sobre aquellos horarios
que tienen tarifaDudosa"""

from datetime import datetime
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand

from adkiller.app.models import Horario
from adkiller import local_settings


def enviar_alerta_tarifaDudosa():
    url = 'http://{server}/{view}/{get}'.format(
        server=local_settings.SITE_URL,
        view='admin/app/horario',
        get='?enElAire__exact=1&tarifaDudosa__exact=1'
    )

    para = local_settings.MAILS_OPERACIONES
    para = ['fverdenelli@infoad.com.ar', 'jmenardi@infoad.com.ar']

    asunto = "Tarifas dudosas {:%d-%m-%Y}".format(datetime.today())

    mensaje = """

    Existen horarios cuya tarifa precisa su supervisión.
    Para verlos visite {}
    """.format(url)

    mensaje_html = """

    <h3>Horarios con tarifas dudosas</h3>
    <hr>
    <p>Existen horarios cuya tarifa precisa su supervisión.</p>
    <p><a href="{}">Click aquí</a> para verlos</p>
    """.format(url)

    email = EmailMultiAlternatives(asunto, mensaje, to=para)
    email.attach_alternative(mensaje_html, 'text/html')
    email.send()


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        horarios = Horario.objects.filter(
            enElAire=True,
            tarifaDudosa=True
        )
        if horarios:
            enviar_alerta_tarifaDudosa()

