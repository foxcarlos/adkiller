# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long, too-many-locals, too-many-statements
"""
Buscar inicio y fin de espacio publicitario para un medio
"""

# python imports
from datetime import datetime
from datetime import timedelta
from lockfile import LockFile
from lockfile import LockTimeout
from optparse import make_option
from requests.exceptions import ConnectionError
import requests
import sys


# django imports
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

# project imports
from adkiller.app.models import Medio
from adkiller.app.utils import grouper
from adkiller.app.utils import get_fecha_hora_from_thumb_url
from adkiller.engines_cache.models import Chunk
from adkiller.media_map.models import AciertosIFEP
from adkiller.media_map.models import BoundingBox
from adkiller.media_map.models import BrandModel
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento
from django.conf import settings

# =============================================================================
# CONSTANTES
# =============================================================================

# Cantidad de imágenes que se analizan por llamado a brandtrack
IMG_PER_REQUEST = 420
URL = 'http://{}/sigmund/analize_batch/'.format(settings.BRANDTRACK_SERVER_URL)

# Medios cuyos thumbs se analizan cada 2 placas
MEDIOS_2 = [
    u'América 2 [ARG]',
    'Canal 9 [ARG]',
    'El Gourmet.com [ARG]',
    'ESPN 3 [LAT]',
    'ESPN + [LAT]',
    'FX [ARG]',
    'TN Todo Noticias [ARG]',
    'TNT [LATE]',
    'Universal Channel [ARG]',
    'Fox Sports [ARG]',
    'Fox Sports 2 [LAT]',
]

# Medios cuyos thumbs se analizan cada 3 placas
MEDIOS_3 = [
    'Animal Planet [ARG]',
    'AMC [LAT]',
    'AXN [ARG]',
    'CN23 [ARG]',
    'Canal 26 [ARG]',
    'Canal 7 [ARG]',
    'Cartoon Network [ARG]',
    'Cartoon [CHL]',
    'Cinemax [ARG]',
    'Cinemax [LATAM]',
    u'Crónica TV [ARG]',
    'DIRECTV Sport 111 [VEN]',
    'DIRECTV Sport 610 [ARG]',
    'DIRECTV Sport Pan 1610 [LAT]',
    'Discovery Channel [ARG]',
    'Disney Channel [ARG]',
    'Discovery H&H [ARG]',
    'Glitz [ARG]',
    'Golf Channel 628 [LAT]',
    'History [ARG]',
    'Investigation Discovery [ARG]',
    'Lifetime [LATAM]',
    'Magazine [ARG]',
    'Nat Geo [LATE]',
    'Quiero! [LAT]',
    'Space [ARG]',
    'TBS [ARG]',
    'TLC [ARG]',
    'TyC Sports [ARG]',
    'Volver [ARG]',
]

# Medios que no se analizan, por más que tengan patrones
MEDIOS_EXCLUIDOS = [
    # 'DIRECTV Sport + 613 [LAT]',
    # 'I-SAT [ARG]',
    'Canal 13 [ARG]',
]

# El mínimo tiempo hacia atrás que se miran los thumbs
RETROACTIVO = timedelta(hours=2)

# Formato de fecha para los argumentos de línea de comandos
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

def es_de_kantar(medio):
    """Devolver True si el medio es de Kantar"""
    return medio.dvr == 'Maggie'


class Command(BaseCommand):
    help = u"""
        Analizar los thumbs del medio para identificar
        inicio y fin de espacio publicitario
        """
    option_list = BaseCommand.option_list + (
        make_option('--medio', help=u'Sólo este medio'),
        make_option('--desde', help='analizar desde <YYYY-MM-DD HH:MM:SS>'),
        make_option('--hasta', help='analizar hasta <YYYY-MM-DD HH:MM:SS>'),
        make_option('-r', '--reverse', default=False, action='store_true', help='analizar en sentido inverso'),
        make_option('--bache_id', type=int, help='analizar este bache'),
    )


    def handle(self, *args, **options):

        args_desde = options.get('desde')
        args_hasta = options.get('hasta')
        args_medio = options.get('medio')
        args_bache = options.get('bache_id')

        if args_bache:
            args_bache = Segmento.objects.filter(id=args_bache)
            if not args_bache:
                exit('No hay bache con ese id')
            else:
                args_bache = args_bache[0]
                if args_bache.tipo.nombre != 'Bache-IFEP':
                    exit('El Segmento no es un bache')
                else:
                    print '\n========> Analizando Bache'
                    args_medio = args_bache.medio.nombre
                    args_desde = args_bache.desde.strftime(DATE_FORMAT)
                    args_hasta = args_bache.hasta.strftime(DATE_FORMAT)

        if args_medio is None:
            medios = Medio.objects.filter(soporte__nombre__icontains='tv')
        else:
            try:
                medios = Medio.objects.get(nombre=args_medio)
            except ObjectDoesNotExist:
                exit('No encuentro el medio "{}"'.format(args_medio))
            else:
                medios = [medios] # que aparezca como lista

        if options.get('reverse'):
            medios = medios.order_by('-nombre')

        if args_desde:
            try:
                args_desde = datetime.strptime(args_desde, DATE_FORMAT)
            except ValueError, e:
                print 'No entiendo el formato de la fecha:'
                exit(e.message)

        if args_hasta:
            try:
                args_hasta = datetime.strptime(args_hasta, DATE_FORMAT)
            except ValueError, e:
                print 'No entiendo el formato de la fecha:'
                exit(e.message)

        medios_con_patrones = []
        for medio in medios:
            if medio.nombre in MEDIOS_EXCLUIDOS:
                continue
            patrones_inicio, patrones_fin = medio.get_patrones_inicio_y_fin()

            if patrones_inicio or patrones_fin:
                medios_con_patrones.append((medio,
                                            patrones_inicio,
                                            patrones_fin))
        total = len(medios_con_patrones)

        # TIME STATS
        GLOBAL_STATS, LOCAL_STATS = 0, 1
        start = [datetime.now(), datetime.now()]
        analized_time = [0, 0]
        performance = [0, 0]

        for index, (medio, patrones_inicio, patrones_fin) in enumerate(medios_con_patrones, 1):

            inicios_id = [p.id_brandtrack for p in patrones_inicio]
            fines_id = [p.id_brandtrack for p in patrones_fin]
            print '\n-----------------------------------------------------------'
            print '{} / {} - {}'.format(index, total, medio)
            print 'patrones inicio:', inicios_id
            print 'patrones fin:', fines_id
            if not medio.dvr:
                print 'No se está grabando'
                continue

            try:
                with LockFile('/tmp/ifep_{}'.format(medio.id), timeout=1):
                    # refrescar
                    medio = Medio.objects.get(id=medio.id)

                    start[LOCAL_STATS] = datetime.now()
                    analized_time[LOCAL_STATS] = 0
                    performance[LOCAL_STATS] = 0

                    mask = self.get_mask(patrones_inicio + patrones_fin)

                    if not args_desde:
                        if es_de_kantar(medio):
                            print '\tMedio de KANTAR'
                            desde = medio.busque_tandas_hasta
                            if not desde:
                                desde = datetime.now() - timedelta(hours=5)
                        else:
                            desde = medio.busque_tandas_hasta
                            if not desde or desde < datetime.now() - RETROACTIVO:
                                desde = datetime.now() - RETROACTIVO
                    else:
                        desde = args_desde

                    if not args_hasta:
                        try:
                            hasta = min(medio.get_last_thumb_datetime(),
                                        datetime.now() - timedelta(minutes=50))
                        except (TypeError, ConnectionError):
                            # get_last_thumb_datetime devolvió 'ALERTA' o None
                            # o no me pude conectar
                            print 'ATENCIÓN: no se puede obtener la fecha del thumb más reciente'
                            hasta = datetime.now() - timedelta(minutes=50)
                    else:
                        hasta = args_hasta

                    print 'desde', desde
                    print 'hasta', hasta
                    if hasta < desde:
                        print 'ADVERTENCIA: La fecha inicial es posterior a la fecha posterior'
                        continue

                    if not args_desde and medio.busque_tandas_hasta and desde > medio.busque_tandas_hasta:
                        # se generó un 'Bache'
                        self.generar_bache(medio=medio,
                                           desde=medio.busque_tandas_hasta,
                                           hasta=desde)

                    # creo un Chunk sin salvarlo, sólo para obtener las imágenes
                    chunk = Chunk(desde=desde, hasta=hasta, medio=medio)
                    if medio.nombre in MEDIOS_3:
                        print 'analizando cada 3 imágenes!'
                        step = 3
                    elif medio.nombre in MEDIOS_2:
                        print 'analizando cada 2 imágenes!'
                        step = 2
                    else:
                        step = 1
                    images_urls = chunk.get_images_urls(step)

                    # variable para chequear si el análisis se pudo hacer, sobre
                    # todo para decidir si se elimina un bache o no
                    overall_success = 0

                    for images in grouper(sorted(images_urls.items()), IMG_PER_REQUEST, None):
                        images = sorted([im for im in images if im is not None])
                        total_images = len(images)
                        primera_time = images[0][0]
                        ultima_time = images[-1][0]
                        print ('de {:%Y-%m-%d %H:%M:%S} '
                               'a {:%Y-%m-%d %H:%M:%S}   '
                               'STATS: {:.2f} {:.2f}'.format(primera_time,
                                                             ultima_time,
                                                             performance[LOCAL_STATS],
                                                             performance[GLOBAL_STATS]))
                        sys.stdout.flush()
                        POST_data = {
                            'models': inicios_id + fines_id,
                            'images': [im[1] for im in images],
                            'mask': mask,
                            'tm': 1,
                        }
                        request = requests.post(URL, POST_data)

                        result = None
                        if request.ok and request.status_code == 200:
                            try:
                                response = request.json()
                            except ValueError:
                                print 'Problemas al entender la respuesta de brandtrack'
                                print request.text
                                response = {'success': False}

                            if response['success']:
                                result = response['data']
                            else:
                                print 'response success False'
                        else:
                            print 'Problemas con el request a brandtrack'

                        if result:

                            # ANÁLISIS DE LA PERFORMANCE
                            analized_images_time = (ultima_time - primera_time).total_seconds()
                            analized_time[GLOBAL_STATS] += analized_images_time
                            analized_time[LOCAL_STATS] += analized_images_time
                            performance[GLOBAL_STATS] = analized_time[GLOBAL_STATS] / (datetime.now() - start[GLOBAL_STATS]).total_seconds()
                            performance[LOCAL_STATS] = analized_time[LOCAL_STATS] / (datetime.now() - start[LOCAL_STATS]).total_seconds()

                            # RECOLECCIÓN DE DATOS
                            imagenes_404 = 0
                            tareas_perdidas = 0
                            detecciones = []

                            for thumb_url in sorted(result.keys()):

                                thumb_result = result[thumb_url]
                                exito = thumb_result['success']
                                boxes = thumb_result['boxes']

                                error_msg = thumb_result['error_msg']
                                if 'tareas perdidas' in error_msg:
                                    tareas_perdidas += 1
                                if 'Problemas al bajar' in error_msg:
                                    imagenes_404 += 1

                                if exito and boxes != {}:
                                    detecciones.append((thumb_url, boxes))

                            # GUARDAR DETECCIONES
                            for thumb_url, boxes in detecciones:
                                if any([int(key) in fines_id for key in boxes.keys()]):
                                    tipo = AciertosIFEP.FIN
                                else:
                                    tipo = AciertosIFEP.INICIO

                                bt_model = int(boxes.keys()[0])
                                model = BrandModel.objects.get(id_brandtrack=bt_model)

                                placa, creada = AciertosIFEP.objects.get_or_create(
                                    datetime=get_fecha_hora_from_thumb_url(thumb_url),
                                    medio=medio,
                                    tipo=tipo,
                                    thumb_url=thumb_url,
                                    brandmodel=model,
                                )
                                if creada:
                                    print '    NUEVA',
                                else:
                                    print 'EXISTENTE',
                                print placa

                            # ANÁLISIS DE LOS PROBLEMAS
                            if imagenes_404 or tareas_perdidas:
                                if imagenes_404:
                                    print '\t{} imágenes no pudieron descargarse'.format(imagenes_404)
                                if tareas_perdidas:
                                    print '\t{} tareas perdidas'.format(tareas_perdidas)

                            fallas = imagenes_404 + tareas_perdidas
                            este_lote_fracaso = not detecciones and (fallas * 2 > total_images)
                            if este_lote_fracaso:
                                overall_success -= 1
                                if fallas == total_images:
                                    print '\tFALLARON TODAS!'
                                elif fallas * 2 > total_images:
                                    print '\tFalló la mayoría'
                            else:
                                overall_success += 1

                            # CREACIÓN DE BACHE O REGISTRO DE QUE SE PUDO
                            # ANALIZAR ESTE FRAGMENTO
                            if args_desde is None and args_hasta is None:
                                # de lo contrario, es una corrida retroactiva

                                if este_lote_fracaso:
                                    # crear bache, se vuelve a analizar!
                                    self.generar_bache(medio=medio,
                                                       desde=primera_time,
                                                       hasta=ultima_time)
                                # si ya generé un bache o si no, seteo que
                                # busqué tandas hasta aquí
                                medio.busque_tandas_hasta = ultima_time
                                medio.save()

                    # end for images_url
                    if args_bache:
                        print
                        print 'overall success:', overall_success
                        print 'borrando bache!'
                        args_bache.delete()

                # end with
            # end try
            except LockTimeout:
                print 'Medio lockeado'
        # end for medio

        print 'FIN DE SCRIPT'

    def get_mask(self, patrones):
        """obtener máscara"""
        boxes = BoundingBox.objects.filter(
            usado_para_entrenar=True,
            model__in=patrones)
        x1 = min([b.x1 for b in boxes])
        y1 = min([b.y1 for b in boxes])
        x2 = max([b.x2 for b in boxes])
        y2 = max([b.y2 for b in boxes])
        return x1, y1, x2, y2

    def generar_bache(self, medio, desde, hasta):
        """Tratar de alargar un bache
        anterior o generar bache nuevo"""

        if hasta - desde < timedelta(seconds=10):
            return

        print 'ADVERTENCIA: Se generó un bache en el análisis:'
        print '\tdesde:', desde
        print '\thasta:', hasta
        tipo_bache, _ = TipoSegmento.objects.get_or_create(nombre='Bache-IFEP')

        # chequear si es una situación que empezó antes
        nuevo_bache = Segmento(
            desde=desde - timedelta(seconds=1),  # para que solape a otro anterior, si es que hay
            hasta=hasta,
            medio=medio,
            descripcion='Bache en el análisis automático IFEP',
            tipo=tipo_bache,
        )
        solapados = nuevo_bache.superpuestos().order_by('desde')
        if solapados:
            solapados[0].hasta = nuevo_bache.hasta
            solapados[0].save()
            print '\talargué un bache preexistente'
        else:
            print '\tgeneré un nuevo bache'
            nuevo_bache.save()

