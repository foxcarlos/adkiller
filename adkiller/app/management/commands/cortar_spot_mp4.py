# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import Producto, Patron

from datetime import datetime, timedelta

HACE_UN_MES = datetime.now() - timedelta(days=30)
HACE_UN_ANYO = datetime.now() - timedelta(days=365)

contador = 0
def tiene_emisiones_recientes(producto):
    global contador
    contador += 1
    if contador % 1000 == 0:
        print contador
    emisiones = producto.emisiones.filter(audiovisual__fecha__gt=HACE_UN_MES,
            audiovisual__score__gt=0)
    if emisiones:
        return True
    else:
        return False


class Command(BaseCommand):
    def handle(self, *args, **options):
        print "Buscando productos..."
        productos = Producto.objects.filter(patrones__mp4_filename__isnull=True, creado__gt=HACE_UN_ANYO).distinct()
        print len(productos)

        print "Filtrando los que tengan emisiones recientes..."
        productos = [p for p in productos if tiene_emisiones_recientes(p)]
        print len(productos)

        print "Filtrando los que no tengan patron_principal()..."
        productos = [p for p in productos if p.patron_principal()]
        print len(productos)

        print "Filtrando los que no tengan patron_principal().server..."
        productos = [p for p in productos if p.patron_principal().server]
        print len(productos)

        print "Filtrando los que si tengan patron_principal().mp4_filename..."
        productos = [p for p in productos if not p.patron_principal().mp4_filename]
        print len(productos)

        print "Filtrando los que tienen mp3 en el filename"
        productos = [p for p in productos if not p.patron_principal() or not p.patron_principal().filename or not '.mp3' in p.patron_principal().filename]
        cuantos = len(productos)
        print cuantos

        for index, producto in enumerate(productos):

            patron = producto.patron_principal()
            print
            print "%d/%d ========================" % (index, cuantos)
            print "Marca:", producto.marca.nombre
            print "Producto:", producto
            print "Servidor:", patron.server.nombre
            if 'test' in args:
                patron.mp4_filename = 'Creado'
                patron.save()
            else:
                patron.crear_mp4()
        print "Resutado final:", contador

