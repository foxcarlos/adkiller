# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import *

import codecs
LOG_FILE = codecs.open('/home/infoad/patrones_que_no_se_generaron.txt', 'w', encoding='latin-1')


def llamado_a_simpsons(tanda, inicio, fin, id_marca, observaciones, producto, id_sector):
    headers = {
	"Content-type": "application/x-www-form-urlencoded",
	"Accept": "text/html,application/xhtml+xml," +
			"application/xml;q=0.9,*/*;q=0.8"
    }
    params = {
        'nombre_tanda': tanda,
        'inicio': inicio,
        'fin': fin,
        'id_marca': id_marca,
        'observaciones': observaciones,
        'producto': producto,
        'id_sector': id_sector,
    }

    params = urllib.urlencode(params)
    h = httplib2.Http()
    url = "http://clips.infoad.tv:8060/API/oneshot_regenerar_patrones_maggie.php?" + params
    print 
    print url
    try:
	resp, content = h.request(url, "GET", headers=headers)
        print content
	content = json.loads(content)
    except:
        LOG_FILE.write(url)
        LOG_FILE.write('\n')
        LOG_FILE.flush()
	resp = {'status': "No se pudo conectar al simpson"}
	content = { 'result': False , 'msg': 'entre al expcept'}
    return (resp, content)

class Command(BaseCommand):
    def handle(self, *args, **options):
        print "Buscando patrones de Maggie"
        patrones = Patron.objects.filter(server__nombre='Maggie', filename__contains="mp3")
        print "Total: %d" % len(patrones)
        FECHA_LIMITE = '2014-03-01'
        print "Filtrando los que tengan emisiones despues de %s" % FECHA_LIMITE 
        patrones = patrones.filter(producto__emisiones__fecha__gt=FECHA_LIMITE, producto__emisiones__migracion__servidor__nombre="Maggie").distinct()[713:]
        total = len(patrones)
        print "Total: %d" % total
        for index, patron in enumerate(patrones):
            print "%d/%d" % (index + 1, total)
            # marca
            try:
                id_marca =  patron.producto.marca.id
            except:
                print "Sin marca"
                continue
            # producto
            try:
                producto = patron.producto.nombre
            except:
                print "Sin producto"
                continue
                
            print producto
            producto = producto.encode("utf-8")

            # id_sector
            try:
                id_sector = patron.producto.marca.sector.id
            except:
                print "Sin sector"
                continue
            observaciones = patron.producto.observaciones
            observaciones = observaciones.encode("utf-8")
            
            print observaciones
            # pasamos por las emisiones hasta que se pueda cargar el patron
            # llamar a Maggie
            emisiones = patron.producto.emisiones.filter(migracion__servidor__nombre='Maggie').order_by('-fecha')
            for emision in emisiones:
		av = emision.audiovisual
                tanda = emision.origen
                inicio = av.inicio
                duracion = av.duracion
                fin = inicio + duracion
                resp, content = llamado_a_simpsons(tanda, inicio, fin, id_marca, observaciones, producto, id_sector)
                print content
                if content['result']:
                    # lo logramos
                    print "Patron cargado"
                    break
                else:
                    # lo anotamos para otra vez
                    # tambien
                    print "Patron no cargado, anotado para otra vez..."
                    break
 
            
