# -*- coding: iso-8859-15 -*-
import httplib
import json
import commands
import smtplib
from email.mime.text import MIMEText
import os
import datetime


from adkiller.app.models import Producto, Marca, Horario, Servidor, Medio

from django.core.management.base import BaseCommand

from django.conf import settings

from django.core.mail import EmailMessage

TOLERANCIA_AVI = datetime.timedelta(minutes=30)
TOLERANCIA_MAIL_ENCARGADO_OP = datetime.timedelta(minutes=30)
TOLERANCIA_MAIL_TECNICO_INTERIOR = datetime.timedelta(hours=1)


def servidores(interior):
    servers = Servidor.objects.filter(url__isnull=False).exclude(url="")
    if interior != None:
        if interior:
            servers = servers.filter(roles__nombre="Interior")
        else:
            servers = servers.exclude(roles__nombre="Interior")
    return servers
    

def hay_luz(servers):
    onlines = servers.filter(online=True)
    # si no hay servidores online, considero que se cortó la luz
    return onlines.count()>0

# tiempo que tiene que pasar para alertar al RESPONSABLE DE OPERACIONES
# e instarlo a gestionar la solución del server caido (al técnico de cada
# plaza se le avisa a la hora)
TOL_SERVER_CAIDO = datetime.timedelta(hours=6)


def chequear(asunto, to, interior=None):
        mensaje = ""
        mensaje_operaciones = ""
        cambio = False

        servers = servidores(interior)
        habia_luz = hay_luz(servers)

        print "Interior=%s" % interior
        for server in servers:
            port = server.puerto_http
            if not port:
                port = 80
            url = server.url
            print server, url, port
            if url and port:
                tries = 0
                r1 = None
                while not r1 and tries < 3:
                    conn = httplib.HTTPConnection(host=url, port=int(port), timeout=10)
                    try:
                        conn.request("GET", "/estado_canales.json")
                        r1 = conn.getresponse()
                    except:
                        r1 = None
                    tries += 1


            online = r1 != None
            if server.online != online:
                if not online:
                    if interior:
                        # A los de interior sólo los consideramos offline si pasó mas de una hora en ese estado
                        if server.ultima_vez_online and datetime.datetime.now() - server.ultima_vez_online > TOLERANCIA_MAIL_TECNICO_INTERIOR:
                            cambio = True
                            server.online = online
                    else:
                        # A los de oficina los consideramos offline instantaneamente
                        cambio = True
                        server.online = online
                    if server.ultima_vez_online and datetime.datetime.now() - server.ultima_vez_online > TOL_SERVER_CAIDO:
                        # avisar al responsable de operaciones que el server está demorando mucho en volver,
                        # para que acelere las gestiones
                        # TODO
                        print "Proximamente: enviar mail al reponsable de op"
                else:
                    cambio = True
                    server.online = online
                if cambio:
                    server.save()
                    if interior:
                        server.mail_al_tecnico() # ATENCIÓN: pude ser de alerta o de agradecimiento
            if r1:
                server.ultima_vez_online = datetime.datetime.now()
                server.save()
                if r1.status == 200:
                    data1 = r1.read()
                    if data1 == "[]":
                        pass
                    d = json.loads(data1)

                    # CHEQUEO DE CANALES
                    # empiezo a revisar cada canal de este servidor
                    for row in d:
                        try:
                            medio = Medio.objects.get(dvr=row["dvr_ident"], canal=row["camara"], server_grabacion=server)
                        except:
                            print row['dvr_ident'], row['camara']
                            mensaje += "No encuentro el canal %s_%s de %s\n" % (row["dvr_ident"], row["camara"], server.nombre)
                            continue

                        # h264
                        #if  medio.grabando <> row["estado"]:
                        #    cambio = True
                        #    medio.grabando = row["estado"]
                        #    medio.save()
                        #if not row["estado"]:
                        #     mensaje += "Canal H264 caido en "+server.nombre+" Canal:"+row["dvr_ident"]+"_"+row["camara"]+"\n"

                        # avi
                        esta_grabando = row['estadoAvi']
                        if not isinstance(row['estadoAvi'], bool):
                            esta_grabando = row['estadoAvi'] < 5 # cantidad de corridas de status_canales sin encontrar videos de ese canal
                        if  medio.grabando <> esta_grabando:
                            cambio = True
                            medio.grabando = esta_grabando
                            medio.save()
                            try:
                                medio.alerta_grabacion()
                            except:
                                print "Problemas al enviar el mail"
                        if not medio.grabando:
                            m = "Canal AVI caido en "+server.nombre+" Canal:"+row["dvr_ident"]+"_"+row["camara"]+"\n"
                            mensaje += m

                        # graba pero en 0B
                        if row["grabando_en_cero"] <> medio.grabando_en_cero and False:
                            cambio = True
                            medio.grabando_en_cero = row['grabando_en_cero']
                            medio.save()
#                             medio.alerta_grabacion()
                        if medio.grabando_en_cero:
                            m = "Canal grabando en cero en "+server.nombre+" Canal:"+row["dvr_ident"]+"_"+row["camara"]+"\n"
                            mensaje += m



                        # chequear sincronizacion de thumbs
                        dt = medio.get_last_thumb_datetime()
                        if dt == None:
                            retraso = None
                        elif dt == "Sin thumbs":
                            retraso = -1
                        elif dt == "ALERTA":
                            # el last.txt esta en blanco
                            retraso = -2
                        else:
                            #TODO sincronizar hora con las procesadoras y el storage
                            retraso = abs((datetime.datetime.now() - dt).total_seconds())
                           
                        row['desfasando'] =  retraso != None and (retraso > 1800 or retraso <= -1) 
                    
                        if medio.desfasando != row['desfasando']:
                            cambio = True
                            medio.desfasando = row['desfasando']
                            medio.save()
                        if row['desfasando']:
                            if retraso > 0:
                                mensaje = mensaje + "Retrasando: " + medio.nombre + " %d minutos\n" % (retraso / 60)
                            elif retraso == -2:
                                mensaje = mensaje + "Last.txt en blanco de " + medio.nombre + "\n"
                            else:
                                mensaje = mensaje + "Posible retraso: " + medio.nombre + "\n"
                                
                        


                if r1.status <> 200:
                    mensaje += "No puedo acceder a la informacion de canales caidos en %s\n" % server.nombre
            else:
                if not server.online:
                    mensaje += "Servidor Caido: "+server.nombre+"\n"
                    if server.ultima_vez_online and datetime.datetime.now() - server.ultima_vez_online > TOLERANCIA_MAIL_ENCARGADO_OP:
                        # si pasó mucho tiempo en ese estado, alertamos al responsable de operaciones
                        # para que avance con los reclamos telefónicos
                        # 1 vez cada 3 horas
                        if (datetime.datetime.now().hour - server.ultima_vez_online.hour) % 3 == 0 and datetime.datetime.now().minute < 10:
                            print "mandando mail a operaciones"
                            server.mail_al_responsable_de_op()

            try:
                conn.request("GET","/estado_simpson.json")
                r2 = conn.getresponse()
            except:
                r2 = None
            if r2:
                if r2.status == 200:
                    data2 = r2.read()
                    try:
                        d2 = json.loads(data2)
                    except:
                        mensaje += "Error al procesar %s\n" % server.nombre
                        continue

                    # CHEQUEO DEL SERVIDOR

                    poco_espacio = d2['poco_espacio']
                    sin_espacio = 'sin_espacio' in d2 and d2['sin_espacio']
                    if poco_espacio:
                        espacio = 1
                    elif sin_espacio:
                        espacio = 0
                    else:
                        espacio = 2
                    if server.espacio_en_disco <> espacio:
                        cambio = True
                        server.espacio_en_disco = espacio
                        server.save()
                    if poco_espacio and not sin_espacio:
                        mensaje += "%s con poco Espacio " % server.nombre
                        if d2.has_key('tiempo_restante_estimado'):
                            mensaje += "(%s)" % d2['tiempo_restante_estimado']
                        if d2.has_key('du'):
                            mensaje += " Uso: %s" % d2['du']
                        mensaje += '\n'
                    if sin_espacio:
                        mensaje += "%s sin Espacio (CRITICO)\n" % server.nombre

                    # estan marcando
                    if d2.has_key('estan_marcando'):
                        estan_marcando = d2['estan_marcando']
                        if not estan_marcando:
                            mensaje_operaciones += "En %s NO han realizado marcas en las ultimas 24 hs\n" % server.nombre

                    # tiene marcas por cortar
                    if d2.has_key('quedan_tandas_marcadas_por_cortar'):
                        hay_marcas_por_cortar = d2['quedan_tandas_marcadas_por_cortar']
                        if hay_marcas_por_cortar:
                            #esta_cortando?
                            if d2.has_key('esta_cortando'):
                                esta_cortando = d2['esta_cortando']
                                if not esta_cortando:
                                    mensaje_operaciones += "%s NO ha cortado en la ultima hora.\n" % server.nombre


                    ### maquina virtual
                    #if server.roles.filter(nombre="Grabación") and d2.has_key('maquina_virtual_activa'):
                    #    mv = d2['maquina_virtual_activa']
                    #    if mv != 'Activa':
                    #        mensaje += "%s: Maquina virtual: %s\n" %(server.nombre, mv)


            conn.close()
        if not mensaje:
            mensaje = "%s: Todo esta bien :)" % settings.PAIS

        print
        print "=========="
        if cambio:
            if 'test' in settings.SITE_URL:
                mensaje = 'ATENCION: Este mensaje es de test_adkiler\n\n' + mensaje
            print "Enviando mail de cambio"
            email = EmailMessage(asunto, mensaje , to=to)
            try:
                email.send()
            except:
                print "Problemas al enviar el mail de cambio"
        else:
            print "No hubo cambios"

        print mensaje
        print "=========="
        print


        print "VERIFICANDO CORTE DE LUZ"
        servers = servidores(interior)
        responsables_corte_de_luz = ["diegolis@infoad.com.ar", "fverdenelli@infoad.com.ar", "pverdenelli@infoxel.com"]
        if not hay_luz(servers):
            email = EmailMessage("Corte de luz", "No tengo acceso a los servidores. Verifique si no hubo un corte de luz.", to=responsables_corte_de_luz)
            email.send()
        elif not habia_luz:
            email = EmailMessage("Volvió la luz", "Ya tengo acceso a uno o más servidores. La luz ha vuelto. Verifique que todo esté correctamente", to=responsables_corte_de_luz)
            email.send()
            



class Command(BaseCommand):
    def handle(self, *args, **options):

        if 'debug' in args:
            import ipdb; ipdb.set_trace()

        to = settings.MAILS_CONTROL_SERVIDORES
        pais = settings.PAIS
        chequear("Problemas en Oficina [%s]" % pais, to, interior=False)
        chequear("Problemas en Interior [%s]" % pais, to, interior=True)
