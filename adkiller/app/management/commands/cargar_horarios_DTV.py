# -*- coding: iso-8859-15 -*-
import codecs
import commands
import csv
import sys
import os
from datetime import time, datetime, timedelta
from adkiller.app.models import *
from django.db.models import F
from adkiller.app.utils import Lapso
from django.conf import settings
from django.core.mail import EmailMultiAlternatives

REGIONES = {3: {'pais': 'Venezuela',
                'medios': ['DTSV', 'DTVSHD.2'],
                'columna_hora': 5
                },
            5: {'pais': 'Argentina',
                'columna_hora': 3
                },
            }
DIRECTORY = './VDC_Plus'
#DIRECTORY = './tnsfiles/DTVLA_Daily_AdvantEdge'

csv.field_size_limit(sys.maxsize)

# ===================================== SETTINGS, GLOBALS

BORRAR_ARCHIVOS_LUEGO_DE_MIGRAR = False
DTV_DIR = os.path.join(settings.MEDIA_ROOT, 'DTV_Schedules')
FECHA_INICIAL = '2014-06-12' # no intentar migrar nada anterior a esta fecha

canales_con_horarios_incongruentes = []
canales_con_horarios_correctos = []
infomerciales_creados = []

long_html = ''

# ====================================== FUNCIONES

def enviar_mail_a_operaciones(infomerciales):
    print "Enviando mail a operaciones"
    html = "<h2>Infomerciales creados a partir de la programaci&oacute;n de Directv</h2>"
    html += "<p>A partir de los horarios de Directv hemos creado algunos AudioVisuales"
    html += " de forma autom&aacute;tica. S&oacute;lo falta que le asignen la marca correcta a los siguientes productos:</p>"
    html += "<ul>"
    for info in infomerciales:
        html += "<li><a href='http://%s/admin/app/producto/%d'>%s</a></li>" % (settings.SITE_URL, info.id, info.nombre)
    html += "</ul>"
    destino=['mmodenesi@infoad.com.ar', 'fverdenelli@infoad.com.ar', 'jmenardi@infoad.com.ar']
    asunto = "Productos creados sin marca"
    enviar_mail(mensaje=html, mensaje_html=html, para=destino, asunto=asunto)

def salir(mensaje=None, mensaje_html=None, mail=False, status_code=0):
    if mensaje:
        print mensaje
    if mail:
        enviar_mail(mensaje, mensaje_html)
    exit(status_code)

def enviar_mail(mensaje, mensaje_html=None, para=None, asunto=None):
    if not asunto:
        asunto = 'Errores al migrar archivos de DTV'
    if not para:
        para = ['diegolis@infoad.com.ar', 'jmenardi@infoad.com.ar', 'fverdenelli@infoad.com.ar', 'cciallella@infoad.com.ar']
    email = EmailMultiAlternatives(asunto, mensaje, to=para)
    if mensaje_html:
        email.attach_alternative(mensaje_html, 'text/html')
    email.send()


def obtener_programacion(canal, archivo):
    # mirando un archivo filtro toda la programación de un canal
    comando = "cut -d '|' -f1,3,12,13 %s | sort -t'|' -k3 | grep -w '%s' -" % (archivo, canal)
    status, output = commands.getstatusoutput(comando)
    # TODO: resolver encoding del output
    programacion = output.split('\n')
    # vuelvo a filtrar para sacar canales que se llaman parecido
    # por ej: filtrar el canal JOV me puede traer tambien JOV.1
    programacion = filter(lambda x : canal == x.split('|')[0], programacion)
    return programacion

def canal_con_errores(ident_canal, archivo, print_alerta=False):
    global long_html
    # analizo todos los horarios disponibles del canal
    # devuelvo True SI ENCUENTRO ERRORES como:
    # a) hay superposiciones de horarios
    if ident_canal in canales_con_horarios_incongruentes:
        # se ve que ya lo analicé
        return True
    if ident_canal in canales_con_horarios_correctos:
        # se ve que ya lo analicé
        return False
    # si llegué hasta aquí es que todavía no analicé ese canal
    # busco los horario en el arhcivo
    horarios = obtener_programacion(ident_canal, archivo)
    # seguridad:
    if not horarios:
        long_html += "El archivo '%s' no trajo la programacion de <strong>'%s'</strong>\n" % (archivo, ident_canal)
        if print_alerta:
            mensaje_error_importante = "\t\tATENCION: No pude obtener la programacion"
            print mensaje_error_importante
        canales_con_horarios_incongruentes.append(ident_canal)
        return True # tiene errores

    # aquí empieza el verdadero control
    hay_error = False
    for i in range (len(horarios) - 1):
        canal, prog, inicio1, fin1 = horarios[i].split('|')
        canal, prog, inicio2, fin2 = horarios[i + 1].split('|')
        # comparo un horario de fin con el inicio siguiente
        if fin1 > inicio2:
            if inicio1 == inicio2 and fin1 == fin2:
                # no voy a migrar el horario i+1
                #horarios.remove(horarios[i+1])
                pass
            else:
                # corresponde al error a
                hay_error = True
                break

    if hay_error:
        canales_con_horarios_incongruentes.append(ident_canal)
        long_html += "El canal <strong>'%s'</strong><br> tiene incongruencias en el archivo '%s'\n" % (ident_canal, archivo)
        return True # tiene errores
    else:
        canales_con_horarios_correctos.append(ident_canal)
        return False # no tiene errores, se puede migrar

def dia_to_string(dia):
    return ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'][dia]

def genera_dia_string(dia):
    return [
        '1,0,0,0,0,0,0',  # domingo
        '0,1,0,0,0,0,0',  # lunes
        '0,0,1,0,0,0,0',  # martes
        '0,0,0,1,0,0,0',  # miércoles
        '0,0,0,0,1,0,0',  # jueves
        '0,0,0,0,0,1,0',  # viernes
        '0,0,0,0,0,0,1'   # sábado
        ][dia]

def unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # código tomado de http://docs.python.org/2/library/csv.html
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(utf_8_encoder(unicode_csv_data),
                                        dialect=dialect, **kwargs)
    for row in csv_reader:
    # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]

def utf_8_encoder(unicode_csv_data):
    for line in unicode_csv_data:
        yield line.encode('utf-8')

def hora_a_segundos(hora):
    return (hora.hour * 60 + hora.minute) * 60 + hora.second

def crear_emision_de_infomercial(medio, titulo, fecha_archivo_dtv, inicio, fin, duracion_string):
    # duracion_string es para distinguir los infomerciales (28) de los spots (2)
    global infomerciales_creados
    fecha_archivo_dtv =  datetime.strptime(fecha_archivo_dtv, '%Y-%m-%d')
    if inicio < time(6, 0):
        # la fecha del archivo es del dia anterior
        fecha_archivo_dtv += timedelta(days=1)


    duracion = hora_a_segundos(fin) - hora_a_segundos(inicio)

    producto = Producto.objects.filter(nombre=titulo)
    if producto:
        # Acá primero chequeo que no exista una tal emisión (doble migración o por otra razón)
        audiovisuales = AudioVisual.objects.filter(horario__medio=medio,
                producto=producto[0], fecha=fecha_archivo_dtv, hora=inicio)
        if audiovisuales:
            return

    score = -1
    if int(duracion_string) == 28:
        tipo_publicidad = TipoPublicidad.objects.get(descripcion='Infomercial')
    elif int(duracion_string) == 2:
        tipo_publicidad = TipoPublicidad.objects.get(descripcion='Spot')
    else:
        # si no es spot ni infomercial, no hago nada
        return

    if producto:
        producto = producto[0]
    else:
        rubro, _ = Rubro.objects.get_or_create(nombre='Infomerciales')
        industria, _ = Industria.objects.get_or_create(nombre='Infomerciales', rubro=rubro)
        sector, _ = Sector.objects.get_or_create(nombre='Infomerciales', industria=industria)
        anunciante, _ = Anunciante.objects.get_or_create(nombre='Infomerciales')
        marca_generica, created = Marca.objects.get_or_create(
                nombre='Infomerciales',
                anunciante=anunciante,
                sector=sector)
        producto = Producto.objects.create(nombre=titulo, marca=marca_generica)
        infomerciales_creados.append(producto)
        print "He creado un nuevo producto (infomercial):", producto.id

    av = AudioVisual(fecha=fecha_archivo_dtv,
            hora=inicio, duracion=duracion,
            tipo_publicidad=tipo_publicidad,
            producto=producto)
    av.set_horario(medio=medio)
    av.calcular_tarifa()
    av.save()
    print "\t\tEmisión creada:", av.id


def unificar_si_se_puede(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha):
    # esta función se fija si hay horarios que coincidan en nombre, hora inicio y hora fin
    # pero de otro día de la semana, de modo que se unifiquen los horarios, en lugar de
    # crear nuevos (sin tarifa)

    horarios = Horario.objects.filter(medio=canal,
                                      programa__nombre=titulo_esp,
                                      horaInicio=inicio_arg_time,
                                      horaFin=fin_arg_time,
                                      fechaAlta__gt=fecha - timedelta(days=7),
                                      fechaAlta__lte=fecha)

    # no me interesa chequear qué otro día es, seguro que el mismo no es, si no no haría falta crearlo
    if horarios:
        # el programa debería ser único, todo debe estar unificado
        coincidencia = horarios[0]
        coincidencia.agregar_dia(dia)
        return coincidencia # <--- no hizo falta crearlo
    else:
        return False # <--- Como consecuencia, se creará


def cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha_archivo_dtv, tags_programa):
    # función central para cargar un nuevo horario
    crear = True
    creado = False
    nuevo_horario = None
    # si este horario esta entre las 00:00:00 y las 05:59:59, la fecha del archivo esta "atrasada"
    if inicio_arg_time < time(6, 0):
        fecha_archivo_dtv = (datetime.strptime(fecha_archivo_dtv, '%Y-%m-%d') + timedelta(days=1)).strftime('%Y-%m-%d')
    horarios = Horario.objects.filter(medio=canal, fechaAlta__lte=fecha_archivo_dtv).order_by('-fechaAlta')
    for horario in horarios:
        if horario.emitido_dia(dia):
            # caso patológico
            if horario.horaInicio > horario.horaFin:
                horario.enElAire=False
                horario.save()
                print "Dando de baja", horario.programa.nombre, "porque su inicio es mayor que su fin..."
                continue
            if horario.solapa_en_horario(inicio_arg_time, fin_arg_time):
                if (horario.programa.nombre == titulo_esp or horario.fechaAlta == fecha_archivo_dtv) and horario.horaInicio == inicio_arg_time and horario.horaFin == fin_arg_time:
                    # Caso 1) coincide con lo que tengo
                    crear = False
                    nuevo_horario = horario # no es nuevo, en verdad, pero a los efectos de tomar las emisiones...
                else:
                    crear = True
                break

    # obtener programa
    prog, _ = Programa.objects.get_or_create(nombre=titulo_esp)
    prog.identificador = 'PRRE'
    prog.tags = tags_programa[:250]
    prog.save()

    if crear:
        # antes de crearlo, chequeamos si se puede unificar con otro horario
        # por ej: Lunes 18:00 Los Simpsons, Martes 18:00 Los Simpsons
        fecha_del_horario = datetime.strptime(fecha_archivo_dtv, '%Y-%m-%d')
        horario = unificar_si_se_puede(dia, titulo_esp, inicio_arg_time, fin_arg_time, canal, fecha_del_horario)
        if horario:
            # Caso 2 a) se unifica con otro horario
            #print "Se unifico este horario con otro: '%s'" % titulo_esp
            nuevo_horario = horario
        else:
            # Caso 2 b) hay que crearlo y tomar la tarifa del mas cercano en su inicio
            print "\t\tCreando nuevo horario:",
            print titulo_esp, inicio_arg_time.strftime('%H:%M:%S'),
            print fin_arg_time.strftime('%H:%M:%S')


            dia_string = genera_dia_string(dia)


            horario = Horario.objects.create(horaInicio=inicio_arg_time, horaFin=fin_arg_time,
                                             programa=prog, medio=canal, dias=dia_string,
                                             fechaAlta=fecha_archivo_dtv)
            nuevo_horario = horario

            nuevo_horario.save()

    # independientemente de si lo creé, lo unifiqué o lo encontré bien,
    # paso las emisiones de su hora y fecha a este horario
    if nuevo_horario: # por las dudas pregunto..., pero debería existir
        nuevo_horario.actualizar_emisiones(fecha_archivo_dtv)
    else:
        assert False

def migrar(arch, medio, directorio=None):
    directorio = directorio if directorio is not None else DTV_DIR # esto es para testing, le paso otro directorio
    if not os.path.exists(os.path.join(directorio, arch)):
        print "No encuentro el archivo %s" % arch
        return
    fecha_archivo = arch.split('_')[1]
    reader = unicode_csv_reader(codecs.open(os.path.join(directorio, arch), 'r', 'iso-8859-15'), delimiter='|')

    for row in reader:
        try:
            (canal, ident, titulo_princ, episodio_ingl, titulo_alter, titulo_ingl,
                titulo_esp, episodio_ingl, episodio_esp, temporada, nro_episodio,
                inicio, fin, inicio_argentina, fin_argentina, duracion, categorias,
                director, actores, en_vivo, HD, audiencia, keywords, algo_extra_ver) = row
        except:
            print "Fila con valores de mas o de menos... no la puedo analizar"
            continue

        if canal != medio.identificador_directv:
            continue

        if not titulo_esp:
            titulo_esp = titulo_princ

        #if '_03.dat' in arch and canal != 'DTVSHD.2':
        #    # de los archivos de Venezuela sólo me interesa este canal
        #    continue

        # si llegue hasta acá, tengo horarios que cargar o comparar

        # datetime
        inicio_arg_datetime = datetime.strptime(inicio_argentina, '%Y-%m-%d %H:%M:%S')
        inicio_arg_time = time(inicio_arg_datetime.hour, inicio_arg_datetime.minute)
        fin_arg_datetime = datetime.strptime(fin_argentina, '%Y-%m-%d %H:%M:%S')
        # resto un segundo para tener un horario de la forma 13:59:59 en vez de 14:00:00
        if fin_argentina[-2:] == '00':
            fin_arg_datetime = fin_arg_datetime - timedelta(seconds=1)
        fin_arg_time = time(fin_arg_datetime.hour, fin_arg_datetime.minute, fin_arg_datetime.second)

        # weekday empieza a contar desde el lunes,
        # nosotos necesitamos desde el domingo, por eso sumo uno
        dia = inicio_arg_datetime.weekday() + 1
        if dia == 7:
            dia = 0

        # si empieza en dia y termina el dia siguiente, lo tomo como dos horarios separados:
        if inicio_arg_datetime.day != fin_arg_datetime.day:
            # creo un lapso desde el inicio hasta las 23:59:59
            fin_falso = time(23, 59, 59)
            lapso_DTV = Lapso(inicio_arg_time, fin_falso)
            cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_falso, medio, fecha_archivo, categorias)
            # creo un lapso desde las 00:00:00 hasta el fin EN EL DIA SIGUIENTE
            inicio_falso = time(0, 0, 0)
            lapso_DTV = Lapso(inicio_falso, fin_arg_time)
            dia_falso = dia + 1 if dia < 6 else 0
            cargar_horario_si_necesario(dia_falso, titulo_esp, inicio_falso, fin_arg_time, medio, fecha_archivo, categorias)
        else:
            # creo unica instancia de Lapso
            lapso_DTV = Lapso(inicio_arg_time, fin_arg_time)
            cargar_horario_si_necesario(dia, titulo_esp, inicio_arg_time, fin_arg_time, medio, fecha_archivo, categorias)

        if medio.identificador_directv == 'INFOMER.SA.1':
            crear_emision_de_infomercial(medio, titulo_esp, fecha_archivo, inicio_arg_time, fin_arg_time, duracion)




def descargar_archivo(fecha, territorio):
    # control de rigor
    if not os.path.exists(DTV_DIR):
        exit("No existe la carpeta %s" % DTV_DIR)

    # Conexión SFTP
    import pysftp
    server = pysftp.Connection(host="208.251.69.24", username="tnsresearch", password="saC@ebrAxaTH")

    print "Conectando al servidor"
    archivos = server.listdir(DIRECTORY)
    archivo_deseado = 'Schedules_%s_%02d.dat' % (fecha, territorio)
    print "buscando archivo '%s'" % archivo_deseado
    if not archivo_deseado in archivos:
        print 'No encontre el archivo "%s"' % archivo_deseado
        return
    print "Comenzando la descarga"
    try:
        os.chdir(DTV_DIR)
        server.get(DIRECTORY + '/%s' % archivo_deseado)
    except Exception, e:
        print "problemas:"
        print str(e)
    print "descargado!"



# ===================================== MAIN
def main(fecha_especifica=None, desde_fecha=None, medio_especifico=None, ident_medio_especifico=None):
    global long_html
    # control de rigor
    if not os.path.exists(DTV_DIR):
        mensaje = "No encuentro la carpeta '%s', el script no puede comenzar" % DTV_DIR
        mensaje_html = '<span style="color: red; font-weight: bold">Error grave:</span> No existe la carpeta \
                "<span style=\'font-family: monospace; font-size: 1.3em;\'>%s</span>"<br> \
                donde se descargan los arhcivos de DTV. <br>El script no pude comenzar.<br>' % DTV_DIR
        print mensaje
        salir(mensaje=mensaje, mensaje_html=mensaje_html, mail=True, status_code=0)

    # Conexión SFTP
    import pysftp
    print "Conectando al servidor"
    try:
        server = pysftp.Connection(host="208.251.69.24", username="tnsresearch", password="saC@ebrAxaTH")
    except Exception as e:
        mensaje = "Error al conectar a servidor de DTV:\n%s" % e
        salir(mensaje=mensaje, mail=True, status_code=0)

    archivos = server.listdir(DIRECTORY)
    # TODO: usar constante REGIONES
    archivos = filter(lambda x : 'Schedules' in x and ('_05.dat' in x or '_03.dat' in x), archivos)
    if fecha_especifica:
        archivos = filter(lambda x : 'Schedules_%s' % fecha_especifica in x, archivos)
    elif desde_fecha:
        archivos = filter(lambda x : x > 'Schedules_%s' % desde_fecha, archivos)
    else:
        archivos = filter(lambda x : x > 'Schedules_%s' % FECHA_INICIAL, archivos)
    archivos = sorted(archivos)

    if not fecha_especifica and not medio_especifico and not ident_medio_especifico and not desde_fecha:
        archivos_a_migrar = []
        for archivo in archivos:
            # ver si ya migré este archivo y si esa migración está completa
            fecha = archivo.split('_')[1]
            region = archivo.split('_')[2].split('.')[0]
            migracion = Migracion.objects.filter(servidor__nombre='DTV', fecha__gte=fecha, completa=True)
            if migracion:
                continue
            else:
                archivos_a_migrar.append(archivo)
    else:
        archivos_a_migrar = archivos

    for archivo in archivos_a_migrar:
        print "Descargando %s en %s" % (archivo, os.path.join(DTV_DIR, archivo))
        if not os.path.exists(os.path.join(DTV_DIR, archivo)):
            try:
                os.chdir(DTV_DIR)
                server.get(DIRECTORY + '/%s' % archivo)
            except Exception, e:
                print str(e)
        else:
            print "Ya tengo en disco un archivo con ese nombre..., no descargo."

    print "Desconectado del servidor"
    server.close()

    if not archivos_a_migrar:
        if not fecha_especifica:
            ultima_migracion = Migracion.objects.filter(servidor__nombre='DTV', completa=True).order_by('-fecha')[0].fecha
            long_html += "No hay archivos nuevos para migrar\n"
            long_html += "Fecha de la ultima migracion de DTV: %s\n" % ultima_migracion
        else:
            print "No encontre archivo de esa fecha:", fecha_especifica
        # fin del script

    for arch in archivos_a_migrar:
        # vacío estas listas para cada archivo a migrar
        canales_con_horarios_incongruentes = []
        canales_con_horarios_correctos = []

        print "Migrando datos para de '%s'" % arch
        if medio_especifico:
            medios = medio_especifico
        elif ident_medio_especifico:
            medios = ident_medio_especifico
        else:
            medios = Medio.objects.filter(
                    identificador_directv__isnull=False,
                    identificador_directv__gt='')

        for medio in medios:
            if "_03.dat" in arch:
                if medio.identificador_directv in ['ESP3', 'FOXS.EC.PLUS']:
                    continue
                if not medio.identificador_directv in REGIONES[3]['medios']:
                    continue
            print '\t* ', medio.nombre
            if canal_con_errores(medio.identificador_directv, os.path.join(DTV_DIR, arch), print_alerta=True):
                print "\t\tSalteo este medio."
                continue
            migrar(arch, medio)

        # seteo la migracion y borro el archivo
        if not fecha_especifica and not medio_especifico and not desde_fecha:
            server_DTV, creado = Servidor.objects.get_or_create(nombre='DTV', nro=300)
            fecha_archivo = arch.split('_')[1]
            migracion_terminada, creada = Migracion.objects.get_or_create(
                    fecha=fecha_archivo, hora='00:00:00', servidor=server_DTV)
            migracion_terminada.completa = True
            migracion_terminada.save()
            print "nueva migracion terminada"
            print "Migracion creada:", creada
            print migracion_terminada.fecha, migracion_terminada.hora


        if BORRAR_ARCHIVOS_LUEGO_DE_MIGRAR:
            print "borrando archivo %s" % arch
            os.system('rm %s' % os.path.join(DTV_DIR, arch))

    if long_html:
        mensaje = long_html.replace('<strong>', '')
        mensaje = long_html.replace('</strong>', '')
        salir(mensaje=long_html, mensaje_html=long_html.replace('\n', '<br>'), mail=True)


from django.core.management.base import BaseCommand
class Command(BaseCommand):
    def handle(self, *args, **options):
        correr_main = True
        fecha = None
        medio = None
        ident_medio = None
        desde_fecha = None
        for arg in args:
            try:
                nombre, valor = arg.split('=')
            except:
                print "problemas al procesar argumentos:", arg
                continue
            if nombre.lower() == 'descargar':
                correr_main = False
                descargar_archivo(valor, 3) # venezuela
                descargar_archivo(valor, 5) # argentina
            if nombre.lower() == 'fecha':
                try:
                    fecha = datetime.strptime(valor, '%Y-%m-%d')
                    print "Migrando solamente la fecha", valor
                    fecha = valor # necesito el string
                except:
                    print "la fecha provista debe tener el formato 'YYYY-MM-DD'"
                    correr_main = False
            if nombre.lower() == 'desde_fecha':
                try:
                    desde_fecha = datetime.strptime(valor, '%Y-%m-%d')
                    print "Migrando desde fecha", valor
                    desde_fecha = valor # necesito el string
                except:
                    print "la fecha provista debe tener el formato 'YYYY-MM-DD'"
                    correr_main = False
            if nombre.lower() == 'medio':
                medio = Medio.objects.filter(nombre=valor)
                if not medio:
                    print "No encuentro medio con ese nombre:", valor
                    correr_main = False
                else:
                    print "Migrando solamente el medio '%s'" % valor
            if nombre.lower() == 'identificador':
                ident_medio = Medio.objects.filter(identificador_directv=valor)
                if not ident_medio:
                    print "No encuentro medios con ese identificador:", valor
                    correr_main = False
                else:
                    print "Migrando solamente medios con identificador '%s'" % valor
        if correr_main:
            main(fecha_especifica=fecha, desde_fecha=desde_fecha, medio_especifico=medio, ident_medio_especifico=ident_medio)
        global infomerciales_creados
        if infomerciales_creados:
            enviar_mail_a_operaciones(infomerciales_creados)

