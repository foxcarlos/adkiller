# -*- coding: iso-8859-15 -*-
# pylint: disable=line-too-long, missing-docstring, unused-argument,
# pylint: disable=no-self-use
"""
Buscar ip de los decodificadores
"""
from adkiller.app.models import Sintonizador
from django.core.management.base import BaseCommand

import urllib
import json

class Command(BaseCommand):
    def handle(self, *args, **options):

        link = 'http://clips.infoad.tv/estado_decos.json' # iserver
        print 'Conectando a', link
        f = urllib.urlopen(link)
        print 'Ya tengo los datos'

        decos = json.loads(f.readline())
        # si pude obtener los datos, primero borro lo que tengo, para actualizarlo
        if len(decos.items()):
            print 'hay %d decodificadores detectados. Borrando ips viejas' % len(decos.items())
            Sintonizador.objects.update(ip=None)


        print 'Actualizando decos...'
        for ip, deco in decos.items():
            s, _ = Sintonizador.objects.get_or_create(receiver_id=deco['receiverId'])
            s.access_card = deco['accessCardId']
            s.ip = ip
            s.save()

        print 'Fin del script'
