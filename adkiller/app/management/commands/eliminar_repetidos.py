# -*- coding:  iso-8859-1 -*-

from adkiller.app.models import AudioVisual, Servidor, Emision, Patron
from adkiller.filtros.models import Perfil, Filtro
from django.contrib.contenttypes.models import ContentType

from django.core.management.base import BaseCommand

from django.conf import settings
import math
import datetime

"""
Comando para eliminar Grafica
graficas = Grafica.objects.all()
for grafica in graficas:
    repetidos =graficas.filter(id__gt=grafica.id,producto=grafica.producto,fecha=grafica.fecha,orden=grafica.orden,duracion=grafica.duracion,horario=grafica.horario,alto=grafica.alto,ancho=grafica.ancho)
    for repe in repetidos:
        conjunto.add(repe)"""
from django.db.models import Q

def seconds_in_time(t):
    return ((t.hour * 60 + t.minute) * 60) + t.second

def eliminar_corruptas(server):
    print ("=== %s ===" % server)
    f = open('/home/infoad/projects/adkiller/tandas_corruptas_%s.txt' % server, 'r')
    tandas = f.readlines()
    f.close()
    for t in tandas:
        emisiones = Emision.objects.filter(origen=tandas)
        if len(emisiones) > 0:
            print len(emisiones)
    #emisiones = Emision.objects.filter(origen__in=tandas)
    #print len(emisiones)

class Command(BaseCommand):
    def handle(self, *args, **options):


        print ("===Contar emisiones repetidas con la hora e inicio aproximado ===")
        total = 0
        for av in AudioVisual.objects.filter(fecha__year=2014, fecha__month=6, horario__isnull=False):
            for av2 in AudioVisual.objects.filter(fecha=av.fecha, producto=av.producto, hora=av.hora, horario__medio=av.horario.medio).exclude(id__gte=av.id):
                if abs((seconds_in_time(av.hora_real()) - seconds_in_time(av2.hora_real()))) < 5:
                    av2.delete()
                    print av.id, av.fecha, av.hora, av.inicio, av.horario, av.producto, av.orden, av.origen  #, av.migracion.fecha, av.migracion.servidor
                    print av2.id, av2.fecha, av2.hora, av2.inicio, av2.horario, av2.producto, av2.orden, av2.origen  #, av2.migracion.fecha, av2.migracion.servidor
                    total = total + 1
        print total
        return


        from django.db import connection
        cursor = connection.cursor()


        #print ("==Borrar tandas corruptas ==")
        #eliminar_corruptas("lisa")
        #eliminar_corruptas("bart")
        #eliminar_corruptas("barney")
        #exit (0)


        print ("===Borrar si hay mas de un filtro en un perfil===")
        for p in Perfil.objects.all():
            for t in ContentType.objects.all():
                filtros = Filtro.objects.filter(tipo=t, perfil=p)
                if len(filtros) > 1:
                    for f in filtros[1:]:
                        f.delete()
   

        print ("===Chequeando si un mismo producto esta en un server mas de una vez===")
        cursor.execute("select producto_id, server_id from app_patron  group by  producto_id, server_id having count(*) > 1 ;")
        for row in cursor.fetchall():
            print row
            patrones = Patron.objects.filter(producto_id=row[0], server_id=row[1])
            con_id = patrones.filter(id_en_simpson__isnull=False)
            if con_id:
                patrones.exclude(id=con_id[0].id).delete()
            else:
                patrones.exclude(id=patrones[0].id).delete()


        print ("===Borra emisiones repetidas con la misma hora y inicio aproximado ===")
        cursor.execute("select fecha, hora, round(inicio) from app_emision inner join app_audiovisual on app_emision.id = app_audiovisual.emision_ptr_id where not inicio is null and inicio <> 0 group by  fecha, hora, round(inicio) having count(*) > 1 order by fecha;")
        for row in []: #cursor.fetchall():
            a = AudioVisual.objects.filter(fecha=row[0], hora=row[1], inicio__gte=row[2]-1, inicio__lt=row[2]+1).order_by("horario__medio", "migracion__servidor__nro")
            for i in range(len(a) - 1):
                if a[i].horario and a[i+1].horario:
                    if a[i].horario.medio == a[i+1].horario.medio:
                        print "Borrar \n1) %s\n2) %s" % (a[i], a[i+1])
                        #a[i+1].delete()
                        i += 1


        print ("===Borra emisiones repetidas ===")
        cant = 0
        posibles_repetidos = AudioVisual.objects.filter(fecha__gte=datetime.date(2013, 7, 1)).filter(inicio__isnull=False, fin__isnull=False).exclude(horario__isnull=True)
        #.filter(producto__marca__sector__nombre__icontains=u"Partidos pol")
        for av in posibles_repetidos:
            a = AudioVisual.objects.exclude(id=av.id).filter(horario__medio=av.horario.medio, fecha=av.fecha, hora=av.hora).filter(duracion__lte=av.duracion).filter(producto__marca = av.producto.marca)
            a = a.filter(Q(inicio__gte=av.inicio, inicio__lte=av.fin) | Q(fin__gte=av.inicio, fin__lte=av.fin) | Q(fin__gte=av.fin, inicio__lte=av.inicio)  | Q(fin__lte=av.fin, inicio__gte=av.inicio))
            if a:
                print "Aproximados"
                print av
                print a
                a.delete()
                cant += 1

        print "Cantidad %s " % cant
        return
        
        print("===Borra tandas repetidas (que vienen de dos servers diferentes===")
        cursor.execute("select producto_id, horario_id, app_emision.fecha, app_emision.hora from app_emision inner join app_migracion on app_emision.migracion_id = app_migracion.id group by horario_id, app_emision.fecha, app_emision.hora, producto_id having max(servidor_id) <> min(servidor_id);")
        for row in cursor.fetchall():
            max_server = None
            max_cant = 0
            for s in Servidor.objects.all():
                emis = Emision.objects.filter(fecha=row[2], hora=row[3], producto_id=row[0], horario_id=row[1], migracion__servidor=s).count()
                if emis > max_cant:
                    max_cant = emis
                    max_server = s

            for s in Servidor.objects.all():
                if s <> max_server:
                    e = Emision.objects.filter(fecha=row[2], hora=row[3], producto_id=row[0], horario_id=row[1], migracion__servidor=s)
                    if e:
                        print "Borrar %s de %s" % (row, s)
                        #e.delete()


        print ("===Crea el audiovisual o grafica segun corresponda===")
        for e in Emision.objects.filter(audiovisual__isnull=True, grafica__isnull=True):
            if e.horario_id and e.horario.medio_id and e.horario.medio.soporte.nombre in ["Diario", "Revista"]:
                cursor.execute("insert into app_grafica (emision_ptr_id) values (%s)" % e.id)
            else:
                cursor.execute("insert into app_audiovisual (emision_ptr_id) values (%s)" % e.id)


        return 

        # Borra emisiones incluidas
        repetidos = 0
        audiovisuales = AudioVisual.objects.filter(inicio__isnull=False,fin__isnull=False,fecha__gte="2013-03-01")
        errores = set()
        print "Cantidad Total: " + str(audiovisuales.count())
        for cantidad,audiovisual in enumerate(audiovisuales):
            if audiovisual.fin > audiovisual.inicio and audiovisual.inicio >= 0:
                copiados = AudioVisual.objects.filter(producto=audiovisual.producto,id__gt=audiovisual.id,horario__medio=audiovisual.horario.medio, fecha=audiovisual.fecha, hora=audiovisual.hora)
                ya_existe = copiados.filter(Q(inicio__gte=audiovisual.inicio, inicio__lte=audiovisual.fin) | Q(fin__gte=audiovisual.inicio, fin__lte=audiovisual.fin))
                if ya_existe:
                    repetidos += ya_existe.count()
                    for elem in ya_existe:
                        errores.add(elem.id)
                if cantidad % 1000 == 0:
                    print cantidad
                    print len(errores)



        for elem in errores:
            AudioVisual.objects.get(id=elem).delete()
        return                

