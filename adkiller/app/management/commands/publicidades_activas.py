# -*- coding:  iso-8859-1 -*-

from datetime import datetime, date, timedelta
import httplib2, urllib
import re
from collections import namedtuple
import time

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError
from django.db.models import Q

from adkiller.app.models import Producto, Emision

class Command(BaseCommand):
    help = "Trae los archivos creados en la ultima de cada servididor, de cada tipo"
    def handle(self, *args, **options):
        hace_30_dias = date.today() - timedelta(days=30)
        for p in Producto.objects.filter(Q(creado__gte=hace_30_dias) |  Q(emisiones__fecha__gte=hace_30_dias)).distinct():
            print '"%s"' % p.nombre.encode("UTF").decode("UTF").encode("UTF")
