# -*- coding:  iso-8859-1 -*-
from cache import fncache
import csv
from adkiller.app.models import *

from django.core.management.base import BaseCommand

from django.conf import settings
import os, re


class Command(BaseCommand):
    def handle(self, *args, **options):
        number = 0
        for filename in os.listdir(settings.BACKUP_PATH):
            print filename
            m = re.match("copia(\d+)\.sql", filename)
            if m:
                print m.groups(1)[0]
                number = max(number, int(m.groups(1)[0]))
        number += 1
        #com = "mysqldump ad_bout3 -u root --password=worms  > %scopia%s.sql" % (settings.BACKUP_PATH, number)
        com = "export PGPASSWORD='worms'; pg_dump -U adbout -h localhost infoad > %scopia%s.sql" % (settings.BACKUP_PATH, number)
        os.system(com)

        for i in range(1, number - 5):
            file = "%scopia%s.sql" % (settings.BACKUP_PATH, i)
            if os.path.exists(file):
                os.remove(file)
