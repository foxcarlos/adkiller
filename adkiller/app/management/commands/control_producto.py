# -*- coding: utf-8 -*-                                                         

from django.core.management.base import BaseCommand
from adkiller.app.models import Producto, Emision, AudioVisual, Marca

class Command(BaseCommand):
    def handle(self, *args, **options):
        try:
            producto_arg = args[0]
        except:
            print "Debe proveer un nombre de producto"
            exit(0)

        print
        print
        producto = Producto.objects.filter(nombre__contains=producto_arg)

        if len(producto) != 1:
            if len(producto) == 0:
                print "no hay Productos con ese nombre"
            elif len(producto) > 1:
                print "hay mas de un producto con ese nombre"
                for p in producto:
                    print p.nombre
            exit(0)

        producto = producto[0]

        total = len(producto.emisiones.all())
        for index, em in enumerate(producto.emisiones.all().order_by('-fecha'), 1):
            print '%d/%d ========' % (index, total)
            try:
                a = em.audiovisual
            except:
                print "emision sin audiovisual..."
                continue
            print '%s - "%s"' % (producto.marca.nombre, producto.nombre)
            print '%s - %02d:%02d' % (em.origen, em.audiovisual.inicio / 60, em.audiovisual.inicio % 60)
            print 'http://%s/multimedia/ar/CapturaDVRs/%s' % (em.migracion.servidor.url_completa(), em.origen)
            raw_input('Siguiente...\n')
