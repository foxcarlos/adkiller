import psycopg2
import datetime

EJECUTAR = True

conn_string="dbname='adbout2' user='adbout' password='worms'"  
conn_string_test="dbname='test_adbout2' user='adbout' password='worms'"

conn = psycopg2.connect(conn_string)
conn_test = psycopg2.connect(conn_string_test)

cursor = conn.cursor()
cursor_test = conn_test.cursor()



def prepare_tuple_for_insert(tup):
    def to_string(v):
        if v == None:
            return "null"
        elif isinstance(v, str) or isinstance(v, datetime.date) or isinstance(v, datetime.time):
            return "'%s'" % v
        else:
            return str(v)
            
    return ",".join([to_string(v) for v in tup])
    


def publicar(origen):
    tanda_completa = False
    # borrar lo que esta publicado
    query = "SELECT * FROM app_emision WHERE migracion_id is not null and origen ='%s'" % origen
    cursor.execute(query)
    registros = cursor.fetchall()
    
    for registro in registros:
        # supuesto: es un audiovisual

        # ver si es tanda_completa
        query = "SELECT emision_ptr_id FROM app_audiovisual WHERE tanda_completa = True and emision_ptr_id = '%s'" % registro[0]
        cursor.execute(query)
        tanda_completa = len(cursor.fetchall()) > 0
        
        if not origen and not tanda_completa:
            # si no tiene origen, solo debe borrar los infomerciales, no las graficas, etc.
            continue

        # borra primero de audiovisuales
        query = "DELETE FROM app_audiovisual WHERE emision_ptr_id = '%s'" % registro[0]
        if EJECUTAR:
            cursor.execute(query)
        # luego borra de emisiones
        query = "DELETE FROM app_emision WHERE id = '%s'" % registro[0]
        if EJECUTAR:
            cursor.execute(query)


    # ahora publico los registros de la otra base
    query = "SELECT * FROM app_emision WHERE migracion_id is not null and origen = '%s'" % origen
    cursor_test.execute(query)
    registros = cursor_test.fetchall()
    for reg_tupla in registros:
        registro = list(reg_tupla)
        query = "SELECT * FROM app_audiovisual WHERE emision_ptr_id ='%s'" % registro[0]
        cursor_test.execute(query)
        registros_av = cursor_test.fetchall()
        if registros_av:
            # chequeo si existe el producto
            producto = registro[2]
            query = "SELECT * FROM app_producto WHERE id = %s" % producto
            cursor.execute(query)
            if not cursor.fetchall():
                # si no esta lo busco por nombre
                query = "SELECT nombre FROM app_producto WHERE id = %s" % producto
                cursor_test.execute(query)
                producto = cursor_test.fetchall()[0][0].decode("utf-8")

                query = "SELECT id FROM app_producto WHERE nombre = '%s'" % producto
                cursor.execute(query)
                productos = cursor.fetchall()
                if not productos:
                    query = "SELECT app_producto.id FROM app_aliasproducto, app_producto WHERE app_aliasproducto.producto_id = app_producto.id and app_aliasproducto.nombre = '%s'" % producto
                    cursor.execute(query)
                    productos = cursor.fetchall()
                    if not productos:
                        assert False, "Producto no encontrado: %s" % producto

                # tomo el id del producto con el mismo nombre
                producto = productos[0][0]
                registro[2] = producto

            # publicar la emision
            registro_tupla = prepare_tuple_for_insert(registro)
            query = "INSERT into app_emision VALUES (%s)" % registro_tupla
            if EJECUTAR:
                cursor.execute(query)

            # publicar como audiovisual
            registro_av = list(registros_av[0])
            # seteo si la tanda esta completa o no
            registro_av[-1] = tanda_completa
            registro_tupla = prepare_tuple_for_insert(registro_av)
            query = "INSERT into app_audiovisual VALUES (%s)" %  registro_tupla
            if EJECUTAR:
                cursor.execute(query)


# infomerciales
#publicar("")
#conn.commit()
#exit(0)


query = "SELECT origen, id FROM app_emision where migracion_id is not null and origen <> '' and origen is not null ORDER BY origen, id"

print "antes de la primer query"
cursor.execute(query)
print "antes de la segunda query"
cursor_test.execute(query)
print "antes del primer fechall()"

registros = cursor.fetchall()
print "antes del segundo fetchall()"
registros_test = cursor_test.fetchall()

print "registros:", len(registros)
print "registros_test:", len(registros_test)

incompletas = 0
inexistentes = 0
faltantes = set()


# pos en registros
i = 0
# pos en registros_test
j = 0

while j < len(registros_test):
    if registros[i] == registros_test[j]:
        i += 1
        j += 1
    elif registros[i] < registros_test[j]:
        # no haga nada, en registros puede haber ids que no esten en registros_test
        i += 1
    else:
        faltantes.add(registros_test[j][0])
        j += 1


print "faltantes: ", len(faltantes)

# comienzo del syncing
for origen in faltantes:
    if not origen:
        continue
    # busco el registro
    query = "SELECT producto_id FROM app_emision WHERE origen ='%s' order by producto_id" % origen
    cursor.execute(query)
    cursor_test.execute(query)
    
    registros = cursor.fetchall()
    registros_test = cursor_test.fetchall()
    
    if not registros:
        # en este caso no creemos que llegue aca
        inexistentes += 1
        inexistente = True
        publico = True
        print "Inexistente", origen
    else:
        inexistente = False
        publico = False
        i = 0
        j = 0
        publico = len(registros_test) > len(registros)
        #while not publico and j < len(registros_test) and i < len(registros):
        #    if registros[i] == registros_test[j]:
        #        i += 1
        #        j += 1
        #    else:
        #        publico = True
        #if j < len(registros_test):
        #    publico = True
        if publico:
            incompletas += 1

            # esto para saber de que server vino
            #print origen
            #query = "SELECT app_servidor.nombre FROM app_emision, app_migracion, app_servidor WHERE app_emision.migracion_id = app_migracion.id and app_migracion.servidor_id = app_servidor.id and app_emision.origen = '%s'" % origen
            #cursor_test.execute(query)
            #print cursor_test.fetchall()
    if publico:
        if origen:
            publicar (origen)
        else:
            assert False

# guardar los cambios
conn.commit()
print "Incompletas", incompletas
print "Inexistentes", inexistentes
