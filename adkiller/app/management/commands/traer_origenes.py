# -*- coding:  iso-8859-1 -*-

from datetime import datetime, date, timedelta
import httplib2, urllib
import re
from collections import namedtuple
import time

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from adkiller.app.models import Origen, Servidor, Medio, Programa

ARCHVIOS_REGEX = re.compile(r'href="([A-Za-z0-9\.\-\_]+.[flv|mp4|mp3|h264])"')
ArchivoData = namedtuple('ArchivoData', 'medio fecha hora nombre')

class Command(BaseCommand):
    help = "Trae los archivos creados en la ultima de cada servididor, de cada tipo"
    def handle(self, *args, **options):
        tipo = args[0] #toma como argumento el tipo de origen que se quiere traer
        print "tipo: ",tipo
        for servidor in Servidor.objects.filter(nombre="Barney"):
            print "servidor ",servidor
            if not servidor.url_completa().strip():
                continue  # if sever is missing it's domain, skip it

            # url de los origenes
            now = datetime.now()
            origenes = Origen.objects.filter(servidor_original=servidor, tipo=tipo).order_by('-fecha')
            if origenes:
                print "ultimo origen ",origenes[0]
                ultima = origenes[0].fecha  # guardamos la fecha del origen mas nuevo
            else:
                ultima = date.today() - timedelta(days=30)  # guardamos la fecha de (hoy menos una semana)

            for i in self.daterange(start_date=ultima, end_date=date.today()):
                fecha = i.strftime("%d%m%y")
                if tipo=='C':  #si es un contenido busca en ArchivosFLV
                    url = "http://%s/multimedia/ar/ArchivosFLV/?%s" % (
                        servidor.url_completa(),
                        "F=0&P=*-%(fecha)s-*.flv" % locals()
                        # F=0 lista los archivos en formato en HTML bien basico
                        # P = *, patron para filtrar archivos
                    )
                elif tipo=='P': #si es un publicidad busca en CapturaDVRs
                    url = "http://%s/multimedia/ar/CapturaDVRs/?%s" % (
                        servidor.url_completa(),
                        "F=0&P=*-%(fecha)s-*.mp4" % locals()
                    )
                elif tipo=='F': #si es una fuente busca en ArchivosH264
                    fecha = i.strftime("%y%m%d")
                    url = "http://%s/multimedia/ar/ArchivosH264/?%s" % (
                        servidor.url_completa(),
                        "F=0&P=*_%(fecha)s_*.h264" % locals()
                    )

                h = httplib2.Http()

                try:
                    resp, content = h.request(url)
                except Exception:
                    print("esta caido %s" % url)

                # iteramos por todos los archivos para una misma fecha traidos desde el server
                for archivo in self.parse_content(content, tipo):
                    existents=Origen.objects.filter(archivo=archivo.nombre)
                    if not existents:
                        ao = Origen.objects.create(
                            horario=archivo.medio.get_horario(hora=archivo.hora, fecha=archivo.fecha),
                            fecha=archivo.fecha,
                            hora=archivo.hora,
                            archivo=archivo.nombre,
                            servidor_original=servidor,
                            tipo=tipo,
                        )

    def daterange(self, start_date, end_date):
        """Nos aproximamos de a un dia desde start_date hasta end_date"""
        for n in range(int ((end_date - start_date).days)+1): #include end_date
            yield start_date + timedelta(n)

    def parse_content(self, content, tipo):
        # cache de medio
        medios = {}
        # cache de prograas
        programas = {}

        #extrae el nombre del archivo del html (<a href="xxx-0000-aaa-0000.flv">)
        archivos = ARCHVIOS_REGEX.findall(content)
        for archivo in archivos:
            print "archivo", archivo
            data = archivo
            if tipo != 'F':
                medio_id, fecha_str, programa_id, hora_str = data.split('-')
                fecha = datetime.strptime(fecha_str, "%d%m%y")
                hora = "%s:%s" % (hora_str[:2], hora_str[2:])

                medio = medios.get(medio_id)
                print medio_id
                if not medio:
                    for  m in Medio.objects.all():
                        print m.identificador, m.identificador_anterior
                    if Medio.objects.filter(identificador=medio_id):
                        print"i"
                        medio = Medio.objects.get(identificador=medio_id)
                    elif Medio.objects.filter(identificador_anterior=medio_id):
                        print "ia"
                        medio = Medio.objects.get(identificador_anterior=medio_id)
                print medio
                if medio:
                    medios[medio_id] = medio

                    #programa = programas.get(programa_id)
                    #if not programa:
                        #programa = medio.get_programa_from_horario(hora, fecha)
                        #programas[programa_id] = programa
            else:
                dvr_str, canal_str, fecha_str, hora_str = data.split('_')
                fecha = datetime.strptime(fecha_str, "%y%m%d")
                hora = "%s:%s" % (hora_str[:2], hora_str[2:]) #ver si aca anda asi
                if Medio.objects.filter(dvr=dvr_str, canal=canal_str):
                    medio = Medio.objects.get(dvr=dvr_str, canal=canal_str)
                    medios[medio.identificador] = medio
                    # horario = medio.get_horario(hora, fecha)
                    # programa = medio.get_programa_from_horario(hora, fecha)
                    # programas[programa_id] = programa

            yield ArchivoData(medio, fecha, hora, archivo)
