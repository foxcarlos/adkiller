# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-docstring, unused-argument,
# pylint: disable=no-self-use
"""
...
"""

from adkiller.app.models import Sintonizador
from django.core.management.base import BaseCommand

RETRYS = 3

class Command(BaseCommand):
    def handle(self, *args, **options):

        sintonizadores = Sintonizador.objects.filter(
            medio_asignado__isnull=False,
            medio_asignado__canal_directv__isnull=False
        )
        for s in sintonizadores:
            print
            print 'Controlando decodificador', s.receiver_id

            # llamada al deco para preguntar el canal
            for retry in range(RETRYS):
                print 'intento numero', retry + 1
                s.canal_sintonizado = s.get_canal_sintonizado()
                print 'está en el canal', s.canal_sintonizado
                if s.canal_sintonizado is None:
                    # por qué no salva?
                    continue
                s.save()
                if s.canal_sintonizado == int(s.medio_asignado.canal_directv):
                    print 'y eso es correcto!!!'
                    break
                print 'y deberia estar en ', s.medio_asignado.canal_directv

                # llamada al deco para sintonizar el canal
                print 'Llamando para setear el canal'
                s.set_canal_sintonizado(s.medio_asignado.canal_directv)

