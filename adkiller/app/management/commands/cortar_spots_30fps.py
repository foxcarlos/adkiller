# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long, too-many-locals, too-many-statements
"""
Recortar spots en 30 frames por segundo
"""

# python imports
from datetime import datetime
from datetime import timedelta
from optparse import make_option
from requests.exceptions import ConnectionError
import requests

# django imports
from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand

# project imports
from adkiller.app.models import AudioVisual
from adkiller.app.models import Producto
from adkiller.media_map.models import Segmento
from django.conf import settings

# =============================================================================
# CONSTANTES
# =============================================================================


class Command(BaseCommand):
    help = u"""
        Generar clips para los spots utilizando 30 fps (no tpa)
        """
    option_list = BaseCommand.option_list + (
        make_option('--id', type=int, help=u'Sólo el producto con este id'),
    )

    def handle(self, *args, **options):
        cli_args = parse_and_validate_options(options)
        for producto in cli_args.productos:
            emisiones = producto.emisiones.filter(
                audiovisual__score__gt=0,
                horario__medio__dvr__gt=['Adminpc'],
            ).order_by('-fecha')

            for emision in emisiones:
                print emision.horario.medio.dvr, emision.horario.medio.canal
                import ipdb; ipdb.set_trace() # BREAKPOINT
                if raw_input('cortar? (s/n)').lower().startswith('s'):
                    fecha = emision.hora_real()
                    segmento = Segmento(
                        medio=emision.medio(),
                        desde=emision.hora_real(),
                        hasta=emision.hora_real() + timedelta(seconds=emision.duracion)
                    )
                    segmento.make_clip(tpa=False, dest='S', published=False, force_creation=True)
                    # ATENCIÓN: no salvar el segmento
                    # setear el nombre del archivo
                    # producto.filename = ...k
                    break


class CliArgsError(Exception):
    pass

def parse_and_validate_options(options):
    """Parsear las opciones de línea de comandos. Si hay
    inconsistencias, levantar una excepción."""

    class Result(object):
        """Sólo para agrupar los cli_args"""
        pass

    result = Result()

    # medio
    if options['id']:
        try:
            producto = Producto.objects.get(pk=options['id'])
        except ObjectDoesNotExist:
            raise CliArgsError('No hay producto con ese id')
        else:
            result.productos = [producto]

    else:
        print 'WARNING: no filtro productos'
        result.producto = []

    return result

