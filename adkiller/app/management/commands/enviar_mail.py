# -*- coding: iso-8859-15 -*-
from django.core.management.base import BaseCommand
from adkiller import local_settings
from django.core.mail import EmailMultiAlternatives
import commands
import time
import os
import json

# ===================================== SETTINGS, GLOBALS

LOGS_DIR = local_settings.LOGS_PATH
JSON_FILE = os.path.join(LOGS_DIR, 'error_mails.json')
CANTIDAD_DE_LINEAS = 25 # a veces el log es muy extenso, sólo enviamos las últimas líneas
HORAS_ENTRE_MAILS = 4
datos= {}

def enviar_mail(asunto, mensaje, mensaje_html=None):
    para = ['mmodenesi@infoad.com.ar', 'diegolis@infoad.com.ar']
    email = EmailMultiAlternatives(asunto, mensaje, to=para)
    if mensaje_html:
        email.attach_alternative(mensaje_html, 'text/html')
    email.send()


def enviar_por_mail(log_file):
    asunto = "Error al ejecutar '%s'" % os.path.basename(log_file).replace('.log', '')
    mensaje = commands.getoutput('tail -n %d "%s"' % (CANTIDAD_DE_LINEAS, log_file))
    enviar_mail(asunto, mensaje)

def paso_un_tiempo_prudencial(script_name):
    global datos
    try:
        datos = json.loads(open(JSON_FILE).readline())
    except:
        pass
    ultimo = datos.get(script_name, 0)
    ahora = time.time()
    tolerancia = 60 * 60 * HORAS_ENTRE_MAILS
    return ahora - ultimo > tolerancia

class Command(BaseCommand):
    def handle(self, *args, **options):
        print
        script_name = args[0]
        log_file = script_name
        if not '.log' in log_file:
            log_file += '.log'
        log_file = os.path.join(LOGS_DIR, log_file)
        print "Enviar por mail el contenido de '%s'" % log_file
        if not os.path.exists(log_file):
            print "No encuentro el archivo especificado... abortando"
            exit(0)
        else:
            # chequear a qué hora salió el último mail de este log
            if paso_un_tiempo_prudencial(script_name):
                enviar_por_mail(log_file)
                # escribir para la próxima iteracion
                datos[script_name] = time.time()
                archivo = open(JSON_FILE, 'w')
                archivo.write(json.dumps(datos))
            else:
                print "Envie un mail hace menos de %d horas" % HORAS_ENTRE_MAILS
        print

