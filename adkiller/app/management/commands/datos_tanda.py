# -*- coding: utf-8 -*-                                                         

from django.core.management.base import BaseCommand
from adkiller.app.models import AudioVisual, Migracion, Servidor, Producto, Horario, Programa

class Command(BaseCommand):                                                        
    def handle(self, *args, **options):
        try:
            archivo = args[0]
        except:
            print "Debe proveer un nombre de tanda"
            exit(0) 

        print
        print "Datos de", archivo
        print
        datos_tanda = AudioVisual.objects.filter(origen=archivo).order_by('inicio')

        if len(datos_tanda) == 0:
            print "no hay Audivosuales con ese origen"   
            exit(0)              

        print "Programa:".ljust(15), datos_tanda[0].horario.programa.nombre
        print "Tanda completa:".ljust(15), datos_tanda[0].tanda_completa
        print "Fecha y hora:".ljust(15), datos_tanda[0].fecha, datos_tanda[0].hora
        print "Servidor:".ljust(15), datos_tanda[0].servidor().nombre
        print "Archivo:".ljust(15), datos_tanda[0].clip
        print "Migracion:".ljust(15), datos_tanda[0].migracion.fecha, datos_tanda[0].migracion.hora
        print
        for av in datos_tanda:
            try:
                print "%.3f" % av.inicio,
            except:
                print "Sin inicio",
            marca = "|| %s " % av.producto.marca 
            print " ||", av.tanda_completa, 
            print marca.ljust(25),
            producto = "|| %s " % av.producto.nombre
            print producto
        print
