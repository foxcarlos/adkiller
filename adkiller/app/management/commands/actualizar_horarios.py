# -*- coding: utf-8 -*-
from adkiller.app.models import Programa,Medio,Programa,Ciudad,Soporte,Emision, Grafica, AudioVisual
from adkiller.app.models import Horario

from django.core.management.base import BaseCommand
from django.db.models import Q

from django.conf import settings
import datetime

from copy import copy
from optparse import Option, OptionValueError


def check_date(option, opt, value):
    try:
        return datetime.datetime.strptime(value, "%Y-%m-%d").date()
    except ValueError:
        raise OptionValueError(
            "option %s: invalid date value: %r. Should have a format like \"YYYY-MM-DD\"" % (opt, value))

def check_list(option, opt, value, parser):
    option_list = ['RADIO', 'TV', 'ALL']
    if value.upper() in option_list:
        setattr(parser.values, option.dest, value.upper())
    else:
        raise OptionValueError(
            "option {option}: invalid medio value: {value}. Should"
            " have a only one option of ['TV', 'RADIO', 'ALL']"
            " default value is ALL".format(option=opt, value=value))

class dateOption (Option):
    TYPES = Option.TYPES + ("date",)
    TYPE_CHECKER = copy(Option.TYPE_CHECKER)
    TYPE_CHECKER["date"] = check_date


class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        dateOption('--one-shot',
                    type='date',
                    dest='date',
                    default=False,
                    help='Fechas para oneshot'),
        Option('--medio',
               type='string',
               action='callback',
               default='ALL',
               callback=check_list
               )
        )

    def handle(self, *args, **options):
        # que saque del aire los horarios que no tengan emisiones en los ultimos 45 dias
        # solo los horarios que se han cargado hace mas de 45 dias, porque los nuevos pueden no tener emisiones todavia
        # si es un horario viejo, que se dio de baja, y ahora se quiere dar de alta, es mejor crear uno nuevo, porque sino al otro día se da de baja
        #for h in Horario.objects.filter(enElAire=True, fechaAlta__lt=datetime.date.today() - datetime.timedelta(days=45)):
        #    nuevas = h.emisiones.filter(fecha__gt=datetime.date.today() - datetime.timedelta(days=45))
        #    if len(nuevas) == 0:
        #        h.enElAire = False
        #        h.save()

        # que desactive horarios anteriores cuando se carga un horario nuevo
        date = options.get('date', None)
        medio = options.get('medio')
        if medio == 'ALL':
            medios = ['TV Aire', 'TV Paga', 'Radio', 'TV Satelital']
        if medio == 'TV':
            medios = ['TV Aire', 'TV Paga', 'TV Satelital']
        if medio == 'RADIO':
            medios = ['Radio']
        if date:
            lockup = {
                'fecha__gte': date,
                'horario__medio__soporte__nombre__in': medios
                }
            for av in AudioVisual.objects.filter(**lockup):
                h = av.horario
                av.set_horario(medio=h.medio)
                if h != av.horario:
                    av.calcular_tarifa()
                    av.save()
            return


        horarios_nuevos = Horario.objects.filter(fechaAlta=datetime.date.today())
        for h in horarios_nuevos:
            anteriores = Horario.objects.filter(
                    medio=h.medio, enElAire=True).exclude(
                            Q(dias=None)|Q(dias="")).exclude(
                                Q(horaInicio=None)).exclude(
                                    Q(horaFin=None))

            # descarto los nuevos, que también me los trajo
            anteriores = filter(lambda x : x not in horarios_nuevos, anteriores)

            for ant in anteriores:
                if ant.solapa_en_horario(h.horaInicio, h.horaFin):
                    for i in range(7):
                        if ant.emitido_dia(i) and h.emitido_dia(i):
                            #TODO: quitar_dia se elimino del model
                            ant.quitar_dia(i) # recordar que esto da de baja el horario si no le quedan dias

            h.enElAire = True
            h.save()

        #unificar_horario()


def unificar_horario():
    """ Recorro todas las emisiones que tienen asignado un horario incompleto y
    verifico si existe un completo y lo asigno, al terminar elimino todos los
    horarios incompletos que no tienen asignados programas"""


    emisiones = Emision.objects.filter(horario__completa=False)
    antes = emisiones.count()
    print "Hay " + str(antes) + " emisiones ligadas a un horario incompleto"
    for emision in emisiones:
        horario = emision.horario
        emision.set_horario(horario.programa, horario.medio)
        emision.save()

    incompletos = Horario.objects.filter(completa=False)
    for incompleto in incompletos:
        if incompleto.emisiones.all().count() == 0:
            print "Eliminando horario incompleto sin emisiones"
            incompleto.delete()

    emisiones = Emision.objects.filter(horario__completa=False)
    despues = emisiones.count()
    print "Hay " + str(despues) + "emisiones ligadas a un horario incompleto"
    print "Se corrigieron " + str( antes-despues) + " emisiones"



def actualizar_tarifas_grafica(desde, hasta, solo_sin_tarifa, medio):
    graficas = Grafica.objects.all()
    if desde:
        graficas = graficas.filter(fecha__gte=desde)
    if hasta:
        graficas = graficas.filter(fecha__lte=hasta)
    if solo_sin_tarifa:
        graficas = graficas.filter(valor=0)
    if medio:
        graficas = graficas.filter(horario__medio=medio)

    for elem in graficas:
        horarios = Horario.objects.filter(tarifa__gt=0, medio=elem.medio, programa=elem.programa)
        horarios_con_fecha = horarios.filter(fechaAlta__lte=elem.fecha)
        if horarios_con_fecha:
            horarios = horarios_con_fecha.order_by("-fechaAlta")
        if horarios:
            elem.horario = horarios[0]
            elem.calcular_tarifa()
            elem.save()


#def oneshot_corregir_horarios():
    #for av in AudioVisual.objects.filter(fecha__year=2014, fecha__gte="2014-04-01", horario__medio__soporte__nombre__in=['TV Aire', 'TV Paga', 'Radio', 'TV Satelital']):
        #h = av.horario
        #av.set_horario(medio=h.medio)
        #if h != av.horario:
            #av.calcular_tarifa()
            #av.save()
