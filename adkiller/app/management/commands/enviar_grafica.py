# -*- coding: utf-8 -*-
from adkiller.app.models import Grafica
import csv, os
import codecs
from random import randint

import datetime

from django.core.management.base import BaseCommand

NRO_SERVER = 100

#(last_id+1, 'Oral-B', 'Pasta Oral-B Pro Salud- Limpieza profunda', 'Televisin', 'Canal 9 - Resistencia', 'N9 / NOTICIERO NUEVE 1 EDICIN', '2012-10-06 00:20:00', '2012-10-06',12,17, 'A', 'FARMACUTICA Y COSMTICA', 'ARTCULOS DE HIGIENE', 'HIGIENE BUCAL',1,0,0,2040, '', '0',17, 'CH09-061012-NOT9-0020.avi', '00:20:00', 'Chaco', 326.24, 'Spot','P&G',3),



def enviar_grafica():
    path = "/home/infoad/projects/csv_infoad/"
#    path = "/home/maxi/migraciones/"
    # Ultima semana
#    semana = datetime.date.today()- datetime.timedelta(60)
    # Migro las emisiones de la ultima semana
    emisiones = Grafica.objects.filter(migrado=False)
    file_name = "%s_%s.csv" % (NRO_SERVER, datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
    file = open(path + file_name, "w")
    file = codecs.open(path + file_name, encoding='utf-8',mode="w")
#    assert False, emisiones.count()
#    anunciante = emision.producto.marca.anunciante.nombre
    anunciante = "Anunciante 1"
    for id,emision in enumerate(emisiones):
        reg = "(last_id + %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %s, %s, '%s', '%s', '%s', '%s', %s, %s, %s, %s, '%s', '%s', %s, '%s', '%s', '%s', %s, '%s', '%s', %s)"
        fecha = emision.fecha.strftime("%Y-%m-%d")
        fecha = fecha + " 00:00:00"
        fecha_corta = emision.fecha.strftime("%Y-%m-%d")
        # Ver problema de UNICODE
	if emision.producto.marca.anunciante:
	    anunciante  = emision.producto.marca.anunciante.nombre
        reg = reg % (id+1, emision.producto.marca.nombre.replace("'",""), emision.producto.nombre.replace('""',''), emision.formato(),
                emision.horario.medio.nombre.replace("'",""), emision.horario.programa.nombre, fecha,fecha_corta,
                emision.orden, emision.ancho, "Diario",emision.producto.marca.sector.nombre,
                emision.producto.marca.sector.industria.nombre,
                emision.producto.marca.sector.industria.rubro.nombre, emision.alto, emision.ancho,
                emision.superficie(), emision.valor, emision.observaciones, str(int(emision.color)), 
                int(emision.duracion), emision.origen, "00:00:00",emision.horario.medio.ciudad.nombre,
                0, "Grafica", anunciante, 100)
        if id > 0:
            reg = ",\n" + reg
        assert "\t" not in reg
        file.write(reg)
    file.close()
    # Enviar Archivo al Server
    if emisiones:
        os.system("cd %s" % path)
        os.system("gzip %s" % (path + file_name))
        os.system("scp  %s.gz tools@172.16.1.100:/home/tools/CSVs" % (path + file_name))

    for emision in emisiones:
        emision.migrado = True
        emision.save()


class Command(BaseCommand):
    def handle(self, *args, **options):
        enviar_grafica()
