# -*- coding:  iso-8859-1 -*-
from adkiller.app.models import Programa,Medio,Producto,Ciudad,Soporte,Emision,AudioVisual
import csv
from adkiller.app.models import Horario

from django.core.management.base import BaseCommand

from django.conf import settings




def migrar_cambios(filename):
    """ Esta funcion parsea un CSV hecho a mano para cambiar nombres y duraciones de productos """

    proyect = settings.PROJECT_PATH

    with open(filename, 'rb') as csvfile:
        rows = csv.reader(csvfile, delimiter='\t')
        lista = []
        for line,row in enumerate(rows):
            if line == 0: continue
            hora = row[0]
            fecha = row[1]
            archivo = row[2]
            marca = row[3]
            nombre = row[4]
            duracion = row[5]
            nombre2 = row[6]
            duracion2 = row[7]

            try:
                producto = Producto.objects.get(marca__nombre=marca, nombre=nombre)
            except:
                producto = None

            if not producto and archivo:
                print "No encuentra producto %s - %s" % (marca, nombre)
                print "Saliendo"
                exit(1)
                
            producto2 = Producto.objects.filter(marca__nombre=marca, nombre=nombre2)
            if producto2:
                producto2 = producto2[0]
            if not archivo:
                # es un cambio de producto
                if producto:
                    if producto2:
                        # ya existe el otro producto, hay que unificar
                        producto2.unificar(producto)
                        producto2.duracion = duracion2
                        producto2.save()
                    else:
                        if nombre2:
                            producto.nombre = nombre2
                        producto.duracion = duracion2
                        producto.save()
            else:
                # es un cambio de audiovisual
                av = AudioVisual.objects.filter(origen=archivo, producto=producto)
                if not av:
                    print "No encuentra av %s %s" % (archivo, producto)
                else:
                    av = av[0]
                    if not nombre2 and not duracion2:
                        print "Borrando %s, %s por %s" % (archivo, nombre, nombre2)
                        av.delete()
                    else:
                        print "Cambiando %s, %s por %s" % (archivo, nombre, nombre2)
                        if producto2:
                            av.producto = producto2
                        elif nombre2:
                            print "No hay producto2 %s" % nombre2
                        av.duracion = duracion2
                        av.save()



            # caso particular
            producto = Producto.objects.get(nombre="Conectate - Arnet 10 Megas Wi Fi $149 (27s)*")
            producto2 = Producto.objects.get(nombre="Conectate - Arnet 10 Megas Wi Fi $149 (30s)")
            producto3 = Producto.objects.get(nombre="Arnet 10 Megas + Wifi + Llamadas Locales Libres $149 - Familia en la mesa (60)*")
            AudioVisual.objects.filter(producto=producto).update(producto=producto3, duracion=60)
            AudioVisual.objects.filter(producto=producto2).update(producto=producto3, duracion=60)
            for av in AudioVisual.objects.filter(producto=producto3):
                AudioVisual.objects.filter(producto=producto3, origen=av.origen).exclude(id=av.id).delete()
                
                    

class Command(BaseCommand):
    def handle(self, *args, **options):
        migrar_cambios(args[0])
