# -*- coding: utf-8 -*-
# pylint: disable=unused-argument, missing-docstring, line-too-long,
# pylint: disable=too-many-statements, attribute-defined-outside-init
# pylint: disable=too-many-locals, no-self-use, too-many-branches
"""
A partir de las detecciones de inicio y fin de espacio publicitario
(instancias de AciertosIFEP) generar Segmentos de tipo 'Tanda'
"""

from datetime import datetime
from datetime import timedelta

from adkiller.app.models import Medio
from adkiller.media_map.models import AciertosIFEP
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento
from adkiller.media_map.utils import pairwise

# django imports
from django.core.management.base import BaseCommand


# Tiempo que debe pasar desde que se analizó un thumb hasta que se use
# el resultado para generar tandas. Es importante esparar un tiempo
# para que se sigan analizando los thumbs siguientes y se de lugar a
# encontrar, por ejemplo, la placa de fin que completa una tanda
ESPERA_PRE_ANALISIS = timedelta(minutes=20)

# Tiempo entre placas para no considerarlas la misma placa
SEPARACION_MINIMA_ENTRE_PLACAS = timedelta(seconds=15)

MEDIOS_PRUEBA = [
    # 'Glitz [ARG]',
    # 'AXN [ARG]',
    # 'Fox Sports [ARG]',
    # 'Fox Sports 2 [LAT]',
]

class Command(BaseCommand):
    help = u"""
        Crear segmentos de tipo tanda a partir de las
        detecciones de placas de inicio y fin de espacio publicitario
        """

    def handle(self, *args, **options):

        medios_con_aciertos = Medio.objects.filter(
            soporte__nombre__icontains='tv',
            aciertos__analizado=False,
        ).distinct()

        for medio in medios_con_aciertos:
            print '------------------------------------------------------------'
            print medio
            if medio.nombre in MEDIOS_PRUEBA:
                print '>>>> salteo'
                continue

            espera_pre_analisis = ESPERA_PRE_ANALISIS
            if medio.dvr == 'Maggie':
                espera_pre_analisis = timedelta(hours=4)

            aciertos = AciertosIFEP.objects.filter(
                medio=medio,
                analizado=False,
                datetime__lt=datetime.now() - espera_pre_analisis,
            ).order_by('datetime')

            total = aciertos.count()

            aciertos = aciertos.exclude(
                brandmodel__definitivo=False,
            )
            if total != aciertos.count():
                print 'Hay aciertos cuyos models no son definitivos'

            if not aciertos:
                print 'No hay aciertos nuevos'
                continue

            # puede ser que placas con diferencia de un segundo sean de
            # distinto tipo. Confiamos en el tipo que tenga mayor cantidad y si
            # la cantidad es igual para los dos tipos...
            placas = self.eliminar_discrepancias(aciertos)

            # si una placa se detectó varias veces seguidas, nos quedamos con
            # una sola aparición (la primera si es de INICIO, la última si es
            # de FIN), limpiando repetidas.
            placas = self.eliminar_repeticiones(placas)

            # insertar las placas necesarias para evitar:
            #   - dos INICIOS seguidos,
            #   - dos FINES seguidos,
            #   - tandas demasiado largas
            placas = self.insertar_placas_inventadas(placas, medio)

            # con las placas que tenemos, generar tandas
            tandas = self.marcar_tandas(placas, medio)
            self.print_tandas(tandas)

            # marcar los aciertos utilizados en esta sesión como analizados,
            # para no volver a tenerlos en cuenta
            self.marcar_aciertos_como_analizados(tandas, medio, placas)

        print 'FIN DE SCRIPT'

    def eliminar_discrepancias(self, aciertos):
        """..."""
        placas = []
        mismo_grupo = []

        for acierto in aciertos:
            try:
                ultimo = mismo_grupo[-1]
            except IndexError:
                ultimo = None
            if not ultimo or acierto.datetime - ultimo.datetime < SEPARACION_MINIMA_ENTRE_PLACAS:
                mismo_grupo.append(acierto)
            else:
                placas.append(mismo_grupo)
                mismo_grupo = [acierto]

        # agregar el último grupo
        placas.append(mismo_grupo)

        for grupo in placas:
            if len(grupo) > 1:
                inicios = fines = 0
                for acierto in grupo:
                    if acierto.tipo == AciertosIFEP.FIN:
                        fines += 1
                    else:
                        inicios += 1
                if inicios != 0 and fines != 0:
                    # hay discrepancias
                    ganador = AciertosIFEP.INICIO if inicios >= fines else AciertosIFEP.FIN
                    for acierto in grupo:
                        acierto.tipo = ganador

        return placas

    def eliminar_repeticiones(self, placas):
        """Quedarse con una sola placa seguida (muy pegadas) del mismo tipo"""
        result = []
        for grupo in placas:
            if grupo[0].tipo == AciertosIFEP.INICIO:
                representante = grupo[0]
            else:
                representante = grupo[-1]
            result.append(representante)

        return result

    def insertar_placas_inventadas(self, placas, medio):

        result = []

        # obtener la duración de los inventos para este medio
        try:
            duracion_inventos = medio.get_duracion_tandas_inventadas()
        except NotImplementedError:
            duracion_inventos = timedelta(seconds=720)


        # algunas variables para llevar cuenta del estado
        ACEPTACION = 0
        ESPERANDO_FIN = 1

        estado = ACEPTACION

        menor_inicio_placas = min([p.datetime for p in placas])
        ultimo_fin = Segmento.objects.filter(
            tipo__nombre='Tanda',
            hasta__lt=menor_inicio_placas,
            medio=medio).order_by('-hasta')
        if ultimo_fin:
            ultimo_fin = ultimo_fin[0].hasta
        else:
            ultimo_fin = datetime(1900, 1, 1, 0, 0)

        ultimo_inicio = None


        for placa in placas:
            if placa.tipo == AciertosIFEP.INICIO:
                if estado == ESPERANDO_FIN:
                    # esperabamos un FIN y llegó un INICIO!
                    # inventar un FIN antes de este INICIO
                    invento_dt = min([placa.datetime - timedelta(seconds=1),
                                      ultimo_inicio + duracion_inventos])
                    fin_inventado = AciertosIFEP(
                        datetime=invento_dt,
                        tipo=AciertosIFEP.FIN
                    )
                    result.append(fin_inventado)
                    ultimo_fin = invento_dt
                # ya sea que estábamos en estado de aceptación o que acabamos
                # de insertar un FIN inventado, insertamos esta placa
                result.append(placa)

                # actualizar estado
                estado = ESPERANDO_FIN
                ultimo_inicio = placa.datetime
            else:
                # llegó un FIN
                if estado == ACEPTACION:
                    # esperábamos un INICIO y llegó un FIN!
                    # inventar un INICIO antes de este FIN
                    invento_dt = max([placa.datetime - duracion_inventos,
                                      ultimo_fin])
                    inicio_inventado = AciertosIFEP(
                        datetime=invento_dt,
                        tipo=AciertosIFEP.INICIO
                    )
                    result.append(inicio_inventado)
                else:
                    # controlar que no estemos generando una tanda muy larga
                    if placa.datetime - ultimo_inicio > duracion_inventos:
                        # generar un FIN y un INICIO
                        fin_dt = ultimo_inicio + duracion_inventos
                        fin_inventado = AciertosIFEP(
                            datetime=fin_dt,
                            tipo=AciertosIFEP.FIN
                        )
                        result.append(fin_inventado)
                        inicio_dt = max([placa.datetime - duracion_inventos,
                                         fin_dt + timedelta(seconds=1)])
                        inicio_inventado = AciertosIFEP(
                            datetime=inicio_dt,
                            tipo=AciertosIFEP.INICIO
                        )
                        result.append(inicio_inventado)

                # ya sea que estábamos en estado de aceptación o que acabamos
                # de insertar un INICIO inventado, insertamos esta placa
                result.append(placa)

                # actualizar estado
                estado = ACEPTACION
                ultimo_fin = placa.datetime

        # Si no terminamos en estado de aceptación, hay que eliminar la última
        # placa de inicio
        if estado == ESPERANDO_FIN:
            result = result[:-1]

        return result

    def marcar_tandas(self, placas, medio):
        """Generar tandas con las placas"""
        tipo_tanda, _ = TipoSegmento.objects.get_or_create(nombre='Tanda')

        result = []
        for inicio, fin in pairwise(placas):
            nueva_tanda = Segmento(
                desde=inicio.datetime,
                hasta=fin.datetime,
                medio=medio,
                descripcion='Tanda automática',
                tipo=tipo_tanda,
            )
            # controlar que no solape a otra tanda existente
            superpuestas = nueva_tanda.superpuestos()
            salvar = True
            for s in superpuestas:
                result.append((s, False))
                if s.desde <= nueva_tanda.desde and s.hasta >= nueva_tanda.hasta:
                    # está contenida, no crearla
                    salvar = False
                    break
                elif s.desde <= nueva_tanda.desde and s.hasta < nueva_tanda.hasta:
                    # la detectada tiene una parte final que queremos conservar
                    nueva_tanda.desde = s.hasta
                elif s.desde > nueva_tanda.desde and s.hasta >= nueva_tanda.hasta:
                    # la detectada tiene una parte inicial que queremos conservar
                    nueva_tanda.hasta = s.desde
                else:
                    # la detectada contiene a la otra. Caso difícil:
                    # hay que crear dos tandas a los costados
                    # por ahora simplemente la descarto
                    salvar = False
                # chequear que no sea una tanda ridículamente corta
                duracion = nueva_tanda.hasta - nueva_tanda.desde
                duracion_suficiente = duracion > timedelta(seconds=15)
                salvar = salvar and duracion_suficiente
            if salvar:
                nueva_tanda.save()
                result.append((nueva_tanda, True))
        return result


    def print_tandas(self, tandas):
        """Mostrar resultado por pantalla"""
        for tanda, creada in tandas:
            if creada:
                print 'Nueva Tanda:'
            else:
                print 'Tanda Preexistente:'
            print '  desde: {:%Y-%m-%d %H:%M:%S} - hasta: {:%Y-%m-%d %H:%M:%S}'.format(
                tanda.desde,
                tanda.hasta
            )

    def marcar_aciertos_como_analizados(self, tandas, medio, placas):
        """marcar aciertos como analizados"""
        if not tandas:
            return
        desde = min([t[0].desde for t in tandas] + [p.datetime for p in placas])
        hasta = max([t[0].hasta for t in tandas])
        # print 'Borrando aciertos desde {} hasta {}'.format(desde, hasta)
        aciertos = AciertosIFEP.objects.filter(
            medio=medio,
            analizado=False,
            datetime__gte=desde,
            datetime__lte=hasta
        )
        aciertos.update(analizado=True)

