from adkiller.app.models import Producto, Marca, Horario

from django.core.management.base import BaseCommand

from django.conf import settings




class Command(BaseCommand):
    def handle(self, *args, **options):
        clase = args[0]

        if args[0] == "marcas":
            for marca in Marca.objects.all():
                otras = Marca.objects.filter(nombre=marca.nombre, id__gt=marca.id)
                for otra in otras:
                    if otra.productos.filter(emisiones__isnull=True):
                        marca.unificar(otra)


        if args[0] == "horarios":
            for horario in Horario.objects.filter(completa=False, enElAire=True).values('programa','medio').distinct():
                opciones = Horario.objects.filter(medio=horario['medio'], programa=horario['programa'], completa=False, enElAire=True)
                if len(opciones) > 1:
                    primero = opciones[0]
                    for otro in opciones[1:]:
                        otro.emisiones.update(horario=primero)
                        otro.delete()

