import subprocess

from django.core.management.base import BaseCommand
from adkiller.app.models import AudioVisual


class Command(BaseCommand):
    help = 'Sends emails with clips to related users of an audiovisual'

    def handle(self, *args, **options):
        audiovisuales = AudioVisual.objects.filter(is_clip_processed=False)

        if not len(audiovisuales):
            self.stdout.write('No audiovisuales to process\n')
            return

        for aud in audiovisuales:

            server = aud.fuente.servidor_original

            # path where the source video resides in the remote server
            source_video_file_path = '/var/www/multimedia/ar/{}/{}'.format(aud.fuente.carpeta,
                                                                           aud.fuente.archivo)

            clip_filename = '{}_{}_{}'.format(aud.inicio, aud.fin, aud.fuente.archivo)

            # get the domain from the remote url to ssh later
            remote_hostname = 'infoxel@{}'.format(server.url)

            # location where to put the clip in the simpson
            destination_file_path = '/var/www/multimedia/ar/clips/{}'.format(clip_filename)

            result = subprocess.call(['ssh', remote_hostname, '-p', str(server.puerto_ssh),
                                      'ffmpeg', '-ss', str(aud.inicio), '-t', str(aud.duracion),
                                      '-y', '-i', source_video_file_path, destination_file_path])

            if result != 0:
               self.stdout.write('Error processing {}'.format(aud))
               continue

            # save new url for this clip file in current instance and mark it as processed
            aud.clip = clip_filename
            aud.is_clip_processed = True
            aud.save()

            self.stdout.write('Successfully processed {}'.format(aud))
