# -*- coding: utf-8 -*-
# pylint: disable=unused-argument, missing-docstring, line-too-long,
# pylint: disable=too-many-statements, attribute-defined-outside-init
# pylint: disable=too-many-locals, no-self-use, too-many-branches
"""
Cubrir los baches que dejó el ifep
"""

# python imports
from datetime import datetime
from datetime import timedelta

# project imports
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento

# django imports
from django.core.management.base import BaseCommand

TIPO_TANDA, _ = TipoSegmento.objects.get_or_create(nombre='Tanda')

class Command(BaseCommand):

    def handle(self, *args, **options):
        baches = Segmento.objects.filter(
            tipo__nombre='Bache-IFEP',
        ).order_by('desde')


        print '{} baches por cubrir'.format(len(baches))

        baches = list(baches)

        for bache in baches:
            print
            print 'bache', bache.id
            if False and bache.descripcion == 'Bache creado por command':
                print 'Creando tanda'
                tanda = Segmento(
                    desde=bache.desde,
                    hasta=bache.hasta,
                    medio=bache.medio,
                    descripcion='Tanda para cubrir bache',
                    tipo=TIPO_TANDA
                )
                tanda.save()
                bache.delete()
                time.sleep(1)
            else:
                if datetime.now() - bache.hasta > timedelta(hours=1):
                    bache.analizar_ifep()
                else:
                    print 'Muy reciente'

        print '==========================================================='

