# -*- coding:  iso-8859-1 -*-
# Python imports
from __future__ import unicode_literals
import traceback
import math
from cache import fncache
from decimal import *
import codecs
import sys
from datetime import datetime, date, timedelta
import csv
import urllib
import urllib2
import os
import re
import gzip
from BeautifulSoup import BeautifulSoup
import base64
from sets import Set

# Django imports
from django.core.management.base import BaseCommand, CommandError
from django import db
from django.db.models import Q
from django.conf import settings
from django.db import transaction
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.conf import settings

# Project imports
from adkiller.app.models import *
from adkiller.usuarios.models import add_log

USER_SIMPSONS = settings.USER_SIMPSONS
PASSWORD_SIMPSONS = settings.PASSWORD_SIMPSONS
CREAR_AL_MIGRAR  = settings.CREAR_AL_MIGRAR
CREAR_HORARIOS = True
CIUDAD_DEFAULT = ""
SOPORTE_DEFAULT = ""

ROW_DELIMITER = chr(9)
FIRST_ROW = 2
MAX_ROWS = 0
MES_DIA = False



def not_null(value):
    return value and not value in ["S/D", "N/A", "UNKNOWN", "SOFT_DELETED","DELETED"]


def row_useful(row):
    res = not_null(row.marca)
    return res
                     

def parse_string(value):
    if value:
        soup = BeautifulSoup(value)
        value = soup.contents[0]
#        value = value.decode('windows-1252').encode('utf-8')
        return value
    else:
        return ""

class Row:

    def transform_row(self, row):
        # si viene en formato string "('data', 'data', 'data'),"
        # lo transforma en un arreglo ['data', 'data', 'data']
        last_word = u""
        is_string = False
        escape = False
        row = parse_string(row[1:-1])
        if row[-1] == ")":
            row = row[:-1]
        new_row = []
        for c in row:
            if escape:
                c = "´"
                escape = False
            if c == "'":
                is_string = not is_string
            elif c == "," and not is_string:
                new_row.append(last_word.strip())
                last_word = u""
            elif c == '\\':
                escape = True
            else:
                last_word += unicode(c)
        new_row.append(last_word.strip())
        return new_row

    def __init__(self, csv_row , diccionario):
        csv_row = csv_row.replace("\r", "")
        csv_row = csv_row.replace("\n", "")
        row = parse_string(csv_row)
        if "\t" in row:
            row = row.split("\t")
        elif ";" in row:
            row = row.split(";")
        else:
            row = row.split(",")
        cols = len(row)

        if len(row) == 1:
            return

        self.marca = get_Key(row,diccionario,["marca","MARCA", "Marca"])
        self.anunciante = get_Key(row,diccionario,["anunciante","ANUNCIANTE","Empresa","empresa","EMPRESA"])
    


def get_file(source, path):
    file_data = None
    if source == "url":
        print "source == url"
        # Set timeout en segundos (1 minuto)
        try:
            file_data = urllib2.urlopen(path,timeout=60)
        except urllib2.URLError as e:
            print "Url Error: %s" % e

    elif source == "file":
        print "source == file"
        os.system("iconv -f Windows-1250 -t UTF8 < %s > %s" % (path, path + "2"))
        if path[-3:] == ".gz":
            file_data = gzip.open(path, 'rb')
            print "path[-3:] == True"
        else:
#            file_data = open(path + "2", "r")
            print "path[-3:] == False"
            file_data = codecs.open(path, encoding='utf-8',mode="r")
    
    return file_data



def get_Key(row,diccionario,label):
    res = None
    for item in label:
        if item in diccionario:
            if diccionario[item] < len(row):
                res = row[diccionario[item]]
    return res
    
@fncache
def get_instance(model,crear=True, **kwargs ):
    keys = {}
    non_keys = {}
    

    uniques = list(model._meta.unique_together)
    if hasattr(model.__class__, "keys"):
        uniques = uniques + list(model.__class__.keys)
    for parent in model._meta.get_parent_list():
        uniques = uniques + list(parent._meta.unique_together)

    for key, value in kwargs.items():
        if model._meta.get_field(key).unique or [ut for ut in uniques if key in ut]:
            keys[key] = value
        else:
            non_keys[key] = value


    
    instance = None
    instances = model.objects.filter(**keys)
    if instances:
        instance = instances[0]
    else:
        if model in (Medio, Marca, Programa) or model == Producto and not 'marca' in kwargs:
            instance = model.objects.filter(alias__nombre=kwargs['nombre'])
            if instance:
                instance = instance[0]
        elif model == Producto:
            instance = model.objects.filter(alias__nombre=kwargs['nombre'], marca=kwargs['marca'])
            if instance:
                instance = instance[0]
    if not instance:
        if crear:
            instance = model.objects.create(**kwargs)
    return instance


def migrate_row(row):

    if row.marca and row.anunciante:
        marca = get_instance(Marca,crear=CREAR_AL_MIGRAR,nombre=row.marca)
        anunciante = get_instance(Anunciante,crear=CREAR_AL_MIGRAR,nombre=row.anunciante)

    if marca and anunciante:
        marca.anunciante = anunciante
        marca.save()
        print "Marca editada: " + marca.nombre

def obtenerFormato(linea):
    linea = parse_string(linea)
    linea = linea.split("\n")[0] # le saco si tiene el enter
    if "\t" in linea:
        linea = linea.split("\t")
    else:
        linea = linea.split(";")
    res = {}
    contador = 0
    for elem in linea:
        res[elem] = contador
        contador += 1
    return res        
        


def parse_csv(file_data, server=200, date=date.today(), time=datetime.now(), es_csv=True, industria_a_migrar=None, tanda_a_migrar=None):
   return migrar_csv_transaction (file_data)
    


@transaction.commit_on_success
def migrar_csv_transaction(file_data):
    return migrar_csv(file_data)



class Tanda():
    def __init__(self):
        self._list = []

    def add(self, row):
        self._list.append(row)
    
    def migrar(self):
        if not self._list:
            return

        for row in self._list:
            if row_useful(row):
                migrate_row(row)

    def misma_tanda(self, row):
        return not self._list 


def migrar_csv(file_data):
   
    line = 0
    lineas = file_data.readlines()
    print "Parseando lineas"
    header = lineas[0].replace("\r", "")
    diccionario = obtenerFormato(header)
    print "%s lineas" % len(lineas)

    tanda_actual = Tanda()
    for csv_row in lineas:
        line += 1
        if line > 1:
            if MAX_ROWS > 0 and line > MAX_ROWS:
                break

            row = Row(csv_row,diccionario)
            if line % 1000 == 0:
                print line, datetime.now()
                # esto es para que borre las queries grabadas y no se agote la memoria
                db.reset_queries()

            # cuando llego a la ultima fila de la tanda
            if not tanda_actual.misma_tanda(row):
                tanda_actual.migrar()
                tanda_actual = Tanda()

            tanda_actual.add(row)


    tanda_actual.migrar()

    mensaje = "------"
    total = 0
    
    return mensaje


def migrar_servidor(servidor, industria_a_migrar=None, tanda_a_migrar=None):
    nro = servidor.nro
    print "Server "+servidor.nombre+" %s: %s" % (nro, servidor.url_completa())
    print "Obteniendo archivo"

    request = urllib2.Request('http://' + servidor.url_completa() + "/lineatiempo")
    base64string = base64.encodestring('%s:%s' % (USER_SIMPSONS, PASSWORD_SIMPSONS)).replace('\n', '')
    request.add_header("Authorization", "Basic %s" % base64string)   

    file = get_file("url", request)
    file_name = settings.MEDIA_ROOT + 'lineatiempo.csv'
    res = ""

    if file:
        print "Transformando a UTF-8 archivo %s" % file_name
        f = open(file_name, 'w')
        for line in file.readlines():
            f.write(line)
        f.close()
    
        os.system("iconv -f Windows-1250 -t UTF8 < %s > %s" % (file_name, file_name + "2"))
        import codecs
        f = codecs.open(file_name + "2", 'r', "utf-8")
        res = parse_csv(f, nro, industria_a_migrar=industria_a_migrar, tanda_a_migrar=tanda_a_migrar)
        f.close()
    
        # respaldo del lineatiempo
        if datetime.now().hour in range(0, 3) or True:
            os.system("cp %s %s/%s-%s.csv" % (file_name + "2", "/home/infoad/projects/backup/lineatiempos", servidor.nombre, datetime.now().strftime("%Y_%m_%d-%M_%H_%S")))


    return res


def commandos(*args, **kwargs):
        # iconv -f Windows-1250 -t UTF8 < /home/diego/Descargas/lineatiempo_dump.xls >  /home/diego/Descargas/lineatiempo_dump2.xls
        mode = args[0]
        server = None
        industria_a_migrar = None
        tanda_a_migrar = None
        print "analizando argumentos"
        
        
        if len(args) > 1:
            path = args[1]
        if len(args) > 2:
            server = args[2]
        else:
            server = None

        if args[0] == "file":
            print "Getting file"
            file = get_file(mode, path)
            print "Parsing file"
            if not server:
                server = 200
            parse_csv(file, server)


class Command(BaseCommand):
    args = 'file <path> <server>'
    help = ''

    def handle(self, *args, **options):
        print "Inicio"
        commandos(*args, **options)



