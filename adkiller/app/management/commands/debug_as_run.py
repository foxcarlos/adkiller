# -*- coding: iso-8859-15 -*-
"""
Command para debuguear el control asrun sin usar la interfaz http.
"""
from django.core.management.base import BaseCommand
from optparse import make_option

from adkiller.control.control_as_run import generar_output_as_run

class Command(BaseCommand):
    args = '<csv_file> <medio_id>'
    option_list = BaseCommand.option_list + (
        make_option(
            '-i',
            '--inverso',
            action='store_true',
            dest='inverso',
            default=False,
            help='Control inverso'
        ),
        make_option(
            '-c',
            '--crear-emisiones',
            action='store_true',
            dest='crear_no_encontradas',
            default=False,
            help='Crear las emisiones no encontradas'
        ),
    )

    def handle(self, *args, **options):
        """
        Llamar a generar_ouptut_as_run con
        los argumentos de la línea de comandos.
        """
        csv_file = args[0]
        medio_id = int(args[1])

        generar_output_as_run(csv_file,
                medio_id=medio_id,
                inverso=options['inverso'],
                crear_em_no_encontradas=options['crear_no_encontradas'],
                debugging=True)
