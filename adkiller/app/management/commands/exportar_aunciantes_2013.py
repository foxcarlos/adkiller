import csv

from adkiller.app.models import *

# csv writer

archivo = open('anunciantes_2013.csv', 'w')

writer = csv.writer(archivo, delimiter='\t', dialect='excel',  quoting=csv.QUOTE_ALL)

producto = 'producto__nombre'
marca = 'producto__marca__nombre'
anunciante = 'producto__marca__anunciante__nombre'


print "averiguando productos"
meses = ['', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
        'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre']

for i in range(1, 13):
    p = Producto.objects.filter(emisiones__fecha__year=2013, emisiones__fecha__month=i).distinct()
    print "Productos ", meses[i], "->", len(p)
    for producto in p:
        if producto.emisiones.filter(
                Q(horario__medio__soporte__nombre__iexact="Tv Aire") |
                Q(horario__medio__soporte__nombre__iexact="Tv Cable")):
            anunciante = (producto.marca.anunciante.nombre).encode('utf-8')
            marca = (producto.marca.nombre).encode('utf-8')
            nombre = (producto.nombre).encode('utf-8')
            writer.writerow([anunciante, marca, producto])
