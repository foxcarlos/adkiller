#!/bin/bash

cd /var/www/multimedia/ar/CapturaDVRs
dest_dir='/var/www/multimedia/ar/Thumbs/'

for file in *.avi; do
    destfile=`basename $file`
    destfile=$dest_dir$destfile
    if [ ! -f $destfile.jpg ]; then
        ffmpeg -i $file -an -ss 00:00:02 -an -r 1 -vframes 1 -y -f mjpeg $destfile.jpg
        mogrify -resize 90x67 $destfile.jpg
        #composite -gravity SouthWest -geometry +5+5 play.png $destfile.jpg $destfile.jpg
    fi
done
