# -*- coding: utf-8 -*-
# Project Imports
from models import *

# Django Imports
from django import forms
from django.db import transaction
from django.core.exceptions import ValidationError

# Other Imports
import autocomplete_light
import datetime
from widgets import AJAXFilteredSelectMultiple


class ProgramaForm(forms.Form):
    seccion = forms.ModelChoiceField(Programa.objects.all(), widget=autocomplete_light.ChoiceWidget('ProgramaAutocomplete'))

    class Meta:
        model = Programa


class GraficaForm(forms.ModelForm):
    observaciones = forms.CharField(widget=forms.Textarea(attrs={'size':'150'}),required=False)
    marca = forms.ModelChoiceField(Marca.objects.all(),label = "Marca",widget=autocomplete_light.ChoiceWidget('MarcaAutocomplete'))

    producto = forms.ModelChoiceField(Producto.objects.all(),label = "Producto",widget=autocomplete_light.ChoiceWidget('ProductoAutocomplete'))

    medio = forms.ModelChoiceField(Medio.objects.all(),label = "Medio",widget=autocomplete_light.ChoiceWidget('MedioAutocomplete'))

    programa = forms.ModelChoiceField(Programa.objects.all(),label = "Sección",widget=autocomplete_light.ChoiceWidget('ProgramaAutocomplete'))

    def __init__(self, *args, **kwargs):
        super(GraficaForm, self).__init__(*args, **kwargs)
        grafica = None
        if 'initial' in kwargs:
            if 'usuario' in kwargs['initial']:
                grafica = Grafica.objects.filter(usuario=kwargs['initial']['usuario']).order_by("-id")
                if grafica:
                    grafica = grafica[0]
                    if grafica.horario:
                        self.fields['medio'].initial = grafica.horario.medio
                        self.fields['programa'].initial = grafica.horario.programa
                    self.fields['fecha'].initial = grafica.fecha
        if 'instance' in kwargs:
            self.fields['marca'].initial = kwargs['instance'].producto.marca
            if kwargs['instance'].horario:
                self.fields['medio'].initial = kwargs['instance'].horario.medio
                self.fields['programa'].initial = kwargs['instance'].horario.programa

    def save(self, commit=True):
        m = super(GraficaForm, self).save(commit=False)

        programa = self.cleaned_data["programa"]
        medio = self.cleaned_data["medio"]
        m.set_horario(medio, programa=programa)

        if m.horario and m.horario.ancho:
            m.duracion = m.alto * m.ancho * m.horario.ancho   # cm2  # antes tenia m.ancho
        else:
            m.duracion = 0

        m.calcular_tarifa()

        if m.fecha > datetime.date.today():
            m.fecha = datetime.date.today()
        if commit:
            m.save()
        return m


    def clean(self):
        super(GraficaForm, self).clean()
        cd = self.cleaned_data
        if  not 'orden' in cd or not 'programa' in cd or not 'fecha' in cd or not 'medio' in cd or not 'contra_tapa' in cd:
            raise ValidationError("Error")

        m = Grafica(fecha=cd['fecha'], orden=cd['orden'], contra_tapa=['contra_tapa'])
        m.set_horario(programa=cd['programa'], medio=cd['medio'])


        if m.horario:
            seccion = m.horario
            condicion = False
            if not seccion.alto:
                error_list=self.errors.get("alto")
                if error_list is None:
                    error_list=forms.util.ErrorList()
                    self.errors["alto"]=error_list
                    error_list.append('La sección ' + str(seccion) + " no tiene asociado alto")
                condicion = True
            if not seccion.ancho:
                error_list=self.errors.get("ancho")
                if error_list is None:
                    error_list=forms.util.ErrorList()
                    self.errors["alto"]=error_list
                    error_list.append('La sección ' + str(seccion) + " no tiene asociado ancho")
                condicion = True
            if not seccion.cantColum:
                error_list=self.errors.get("orden")
                if error_list is None:
                    error_list=forms.util.ErrorList()
                    self.errors["orden"]=error_list
                    error_list.append('La sección ' + str(seccion) + " no tiene asociado la cantidad de columnas")
                condicion = True
            if condicion:
                raise ValidationError("Error")
            if 'alto' in cd:
                error_list=self.errors.get("orden")
                if cd['alto'] > seccion.alto:
                    if error_list is None:
                        error_list=forms.util.ErrorList()
                        self.errors["alto"]=error_list
                    error_list.append("Te pasaste del alto de la sección")
            if 'ancho' in cd:
                error_list=self.errors.get("orden")
                if cd['ancho'] > seccion.cantColum:
                    if error_list is None:
                        error_list=forms.util.ErrorList()
                        self.errors["ancho"]=error_list
                    error_list.append("Te pasaste de la cantidad de columnas de la sección")
        else:
            error_list=self.errors.get("medio")
            if error_list is None:
                error_list=forms.util.ErrorList()
                self.errors["medio"]=error_list
                error_list.append("No se encuentra un horario válido")
            raise ValidationError("Error")
        return cd

    class Meta:
        model = Grafica


class HorarioForm(forms.ModelForm):
    medio = forms.ModelChoiceField(Medio.objects.all(),label = "Medio",widget=autocomplete_light.ChoiceWidget('MedioAutocomplete'))
    programa = forms.ModelChoiceField(Programa.objects.all(),label = "Programa",widget=autocomplete_light.ChoiceWidget('ProgramaAutocomplete'))
    lunes = forms.BooleanField(initial=False,required=False)
    martes = forms.BooleanField(initial=False,required=False)
    miercoles = forms.BooleanField(initial=False,required=False)
    jueves = forms.BooleanField(initial=False,required=False)
    viernes = forms.BooleanField(initial=False,required=False)
    sabado = forms.BooleanField(initial=False,required=False)
    domingo = forms.BooleanField(initial=False,required=False)

    def save(self, commit=True):
        print "hola"
        m = super(HorarioForm, self).save(commit=False)
        m.dias = str(int(self.cleaned_data["domingo"]))
        m.dias += "," + str(int(self.cleaned_data["lunes"]))
        m.dias += "," + str(int(self.cleaned_data["martes"]))
        m.dias += "," + str(int(self.cleaned_data["miercoles"]))
        m.dias += "," + str(int(self.cleaned_data["jueves"]))
        m.dias += "," + str(int(self.cleaned_data["viernes"]))
        m.dias += "," + str(int(self.cleaned_data["sabado"]))
        print m.dias
        if m.dias == "0,0,0,0,0,0,0":
            m.dias = None
        if commit:
            m.save()
        return m


    def __init__(self, *args, **kwargs):
        super(HorarioForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            if kwargs['instance'].dias:
                dias = kwargs['instance'].dias
                self.fields['domingo'].initial = bool(int(dias[0]))
                self.fields['lunes'].initial = bool(int(dias[2]))
                self.fields['martes'].initial = bool(int(dias[4]))
                self.fields['miercoles'].initial =  bool(int(dias[6]))
                self.fields['jueves'].initial =  bool(int(dias[8]))
                self.fields['viernes'].initial =  bool(int(dias[10]))
                self.fields['sabado'].initial = bool(int(dias[12]))


    class Meta:
        model = Horario


class AudioVisualForm(forms.ModelForm):
    tipo = forms.ChoiceField(widget=forms.RadioSelect(), choices=CHOICES_TIPO_EMISION, required=True)
    observaciones = forms.CharField(widget=forms.Textarea(attrs={'size':'150'}),required=False)
    marca = forms.ModelChoiceField(Marca.objects.all(),label = "Marca",widget=autocomplete_light.ChoiceWidget('MarcaAutocomplete'),required=False)

    producto = forms.ModelChoiceField(Producto.objects.all(),label = "Producto",widget=autocomplete_light.ChoiceWidget('ProductoAutocomplete'))

    medio = forms.ModelChoiceField(Medio.objects.all(),label = "Medio",widget=autocomplete_light.ChoiceWidget('MedioAutocomplete'))

    programa = forms.ModelChoiceField(Programa.objects.all(),label = "Programa",widget=autocomplete_light.ChoiceWidget('ProgramaAutocomplete'))


    orden = forms.CharField(widget=forms.TextInput(attrs={'size':'150'}))


    def __init__(self, *args, **kwargs):
        super(AudioVisualForm, self).__init__(*args, **kwargs)
        emision = None
        if 'initial' in kwargs:
            if 'usuario' in kwargs['initial']:
                emision = Emision.objects.filter(usuario=kwargs['initial']['usuario']).order_by("-id")
                if emision:
                    emision = emision[0]
                    if emision.horario:
                        horario = emision.horario
                        if horario.medio:
                            self.fields['medio'].initial = horario.medio
                        if horario.programa:
                            self.fields['programa'].initial = horario.programa
                        if emision.producto:
                            self.fields['marca'].initial = emision.producto.marca
                            self.fields['producto'].initial = emision.producto
                        #self.fields['duracion'].initial = emision.duracion
                        self.fields['orden'].initial = emision.orden
                        #if emision.origen:
                        #    self.fields['origen'].initial = emision.origen
                        #if emision.observaciones:
                        #    self.fields['observaciones'].initial = emision.observaciones
                    self.fields['fecha'].initial = emision.fecha
                    self.fields['tipo'].initial = emision.tipo
        if 'instance' in kwargs:
            self.fields['marca'].initial = kwargs['instance'].producto.marca
            if kwargs['instance'].horario:
                self.fields['medio'].initial = kwargs['instance'].horario.medio
                self.fields['programa'].initial = kwargs['instance'].horario.programa


    def save(self, commit=True):
        m = super(AudioVisualForm, self).save(commit=False)
        # do custom stuff
        m.set_duracion()
        if not m.duracion and m.producto:
            emisiones = Emision.objects.filter(producto=m.producto,duracion__isnull=False)
            if emisiones:
                m.duracion = emisiones[0].duracion
        programa = self.cleaned_data["programa"]
        medio = self.cleaned_data["medio"]
        m.valor = 0
        m.set_horario(medio=medio)
        if m.horario and m.horario.tarifa and m.duracion:
            m.valor = m.duracion * m.horario.tarifa
        if not m.origen and m.horario:
            horario = m.horario
            m.set_origen()

        if m.fecha > datetime.date.today():
            m.fecha= datetime.date.today()
        m.set_tipo_publicidad()
        if commit:
            m.save()
        return m

    def clean(self):
        super(AudioVisualForm, self).clean()
        cd = self.cleaned_data
        tiene_programa = False
        if True:
            if "hora" in cd and "medio" in cd and "fecha" in cd:
                hora = cd['hora']
                medio = cd['medio']
                fecha = cd["fecha"]
                programa = medio.get_programa_from_horario(hora, fecha)
                if programa:
                    self.fields["programa"].initial = programa
                    tiene_programa = True
                    self.initial["programa"] = programa
                    cd["programa"] = programa

                if "programa" in self.errors:
                    self.errors.pop("programa")
                if not tiene_programa: # and not 'programa' in cd:
                    error_list=self.errors.get("programa")
                    if error_list is None:
                        error_list=forms.util.ErrorList()
                        self.errors["programa"]=error_list
                    error_list.append("No hay programas para esa Hora")
            if 'producto' in cd and cd['producto'].marca.sector and cd['producto'].marca.sector.industria.nombre.lower() == "venta directa":
                if cd['duracion'] and 'tipo_publicidad' in cd and cd['tipo_publicidad']:
                    if int(cd['duracion']) <= 280 and cd['tipo_publicidad'].descripcion == "Infomercial" or  int(cd['duracion']) > 280 and cd['tipo_publicidad'].descripcion != "Infomercial":
                        raise forms.ValidationError("Chequear el tipo de publicidad")
        return cd


    class Meta:
        exclude = ("horario","usuario",)
        model = AudioVisual


class MarcaForm(forms.ModelForm):
    anunciante = forms.ModelChoiceField(Anunciante.objects.all(),label = "Anunciante",widget=autocomplete_light.ChoiceWidget('AnuncianteAutocomplete'),
        help_text="Si hay más de uno, usar el primero que tenga razón social inmediatamente anterior. Si no encuentra el anunciante, poner el mismo nombre que la marca.")

    sector = forms.ModelChoiceField(Sector.objects.all(),label = "Sector",widget=autocomplete_light.ChoiceWidget('SectorAutocomplete'),
        help_text="Si la marca tiene varios productos en sectores diferentes, crear una marca para cada sector.")

    @transaction.commit_manually
    def save(self, commit=True):
        m = super(MarcaForm, self).save(commit=False)
        if commit:
            m.save()
        servidores = Servidor.objects.filter(automatico=True)
        m.save()
        transaction.commit()
        for servidor in servidores:
            servidor.pedir_marca(m)
        transaction.commit()
        return m

    class Meta:
        model = Marca


class OrigenForm(forms.ModelForm):
    # tipo = forms.ChoiceField(widget=forms.RadioSelect(), choices=CHOICES_TIPO_EMISION, required=True)
    medio = forms.ModelChoiceField(Medio.objects.all(),label = "Medio",widget=autocomplete_light.ChoiceWidget('MedioAutocomplete'))
    programa = forms.ModelChoiceField(Programa.objects.all(),label = "Programa",widget=autocomplete_light.ChoiceWidget('ProgramaAutocomplete'))

    def __init__(self, *args, **kwargs):
        super(OrigenForm, self).__init__(*args, **kwargs)

        emision = None
        if 'initial' in kwargs:
            if 'usuario' in kwargs['initial']:
                origen = Origen.objects.filter(user=kwargs['initial']['usuario']).order_by("-id")
                if origen:
                    origen = origen[0]
                    if origen.horario:
                        horario = origen.horario
                        if horario.medio:
                            self.fields['medio'].initial = horario.medio
                        if horario.programa:
                            self.fields['programa'].initial = horario.programa
                    self.fields['fecha'].initial = emision.fecha
                    #self.fields['tipo'].initial = emision.tipo
        if 'instance' in kwargs:
            if kwargs['instance'].horario:
                self.fields['medio'].initial = kwargs['instance'].horario.medio
                self.fields['programa'].initial = kwargs['instance'].horario.programa


    def save(self, commit=True):
        m = super(OrigenForm, self).save(commit=False)

        # Now set the Horario this Origen is attached to
        hora = self.cleaned_data["hora"]
        fecha = self.cleaned_data["fecha"]
        programa = self.cleaned_data["programa"]
        medio = self.cleaned_data["medio"]

        # trae el horario que corresponde a ese medio y programa
        programa = self.cleaned_data["programa"]
        medio = self.cleaned_data["medio"]
        m.set_horario(medio=medio)

        # esto no seria necesario cuando los campos sean readonly
        #if not m.archivo and m.horario:
        #    m.set_archivo()

        if commit:
            m.save()
        return m

    def clean(self):
        super(OrigenForm, self).clean()
        cd = self.cleaned_data
        tiene_programa = False
        if True:
            if "hora" in cd and "medio" in cd and "fecha" in cd:
                hora = cd['hora']
                medio = cd['medio']
                fecha = cd["fecha"]
                programa = medio.get_programa_from_horario(hora, fecha)
                if programa:
                    self.fields["programa"].initial = programa
                    tiene_programa = True
                    self.initial["programa"] = programa
                    cd["programa"] = programa

                if "programa" in self.errors:
                    self.errors.pop("programa")
                if not tiene_programa: # and not 'programa' in cd:
                    error_list=self.errors.get("programa")
                    if error_list is None:
                        error_list=forms.util.ErrorList()
                        self.errors["programa"]=error_list
                    error_list.append("No hay programas para esa Hora")
            if 'producto' in cd and cd['producto'].marca.sector and cd['producto'].marca.sector.industria.nombre.lower() == "venta directa":
                if cd['duracion'] and 'tipo_publicidad' in cd and cd['tipo_publicidad']:
                    if int(cd['duracion']) <= 280 and cd['tipo_publicidad'].descripcion == "Infomercial" or  int(cd['duracion']) > 280 and cd['tipo_publicidad'].descripcion != "Infomercial":
                        raise forms.ValidationError("Chequear el tipo de publicidad")
        return cd

    class Meta:
        exclude = ("horario", "usuario",)
        model = Origen


class AnunciantesAJAXForm(forms.Form):
    """Usado para hacer el autocomplete de anunciantes en el admin"""
    query = forms.CharField(required=True)


class AnunciantesModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    """Override default validation becouse we start with an empty
       queryset and populate this field with ajax"""
    def clean(self, value):
        return [val for val in value]


class AudioVisualInlineForm(forms.ModelForm):
    # we will populate this widget with ajax
    anunciantes = AnunciantesModelMultipleChoiceField(
        widget=AJAXFilteredSelectMultiple("anunciantes", is_stacked=False),
        queryset=Anunciante.objects.none()
    )

    def __init__(self, *args, **kwargs):
        """Populate the anunciantes queryset"""
        super(AudioVisualInlineForm, self).__init__(*args, **kwargs)
        try:
            instance = kwargs["instance"]
            qs = instance.anunciantes.all()
        except:
            qs = Anunciante.objects.none()
        self.fields['anunciantes'].queryset = qs

    class Meta:
        model = AudioVisual


class GetProgramaAJAXForm(forms.Form):
    """Usado para obtener el programa a partir de medio, fecha y hora en el admin"""
    fecha = forms.DateField(required=True)
    hora = forms.TimeField(required=True)
    medio = forms.IntegerField(required=True)


class TiempoProcesoForm(forms.Form):
    mes = forms.IntegerField(required=True)
    ano = forms.IntegerField(label="Año", required=True)
    servidor = forms.ModelChoiceField(Servidor.objects.filter(roles__nombre="Detección").distinct(), label="Servidor", required=False)

class MedioForm(forms.ModelForm):
    class Meta:
        model = Medio
        fields = ('dvr','canal','canal_directv','mediadelivery_id')
    