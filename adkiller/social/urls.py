from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
    url(r'^buscar_en_twitter', buscar_en_twitter),
    url(r'^buscar_en_facebook', buscar_en_facebook),
    url(r'^buscar_en_youtube', buscar_en_youtube),
    url(r'^buscar_en_noticias', buscar_en_noticias),
)
