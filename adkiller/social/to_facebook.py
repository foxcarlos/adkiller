import facebook
import urllib2, urllib

from django.conf import settings

def search(query):

    args = {"client_id": settings.FACEBOOK_APP_ID, "client_secret": settings.FACEBOOK_APP_SECRET, "grant_type": "client_credentials"}
    file = urllib2.urlopen("https://graph.facebook.com/oauth/access_token",
                                   urllib.urlencode(args))
    access_token = file.read().split("=")[1]
    graph = facebook.GraphAPI(access_token)
    #profile = graph.get_object("me")
    #friends = graph.get_connections("me", "friends")
    #graph.extend_access_token("173419742768794", "534ac6f673efdd0d77b77dc5ec867b37")
    sa =graph.request("/search", {"q": query, "type": "post"})
    return sa
