import twitter
from django.conf import settings

def connect(query):
    api = twitter.Api(
    		consumer_key=settings.TWITTER_CONSUMER_KEY, 
            consumer_secret = settings.TWITTER_CONSUMER_SECRET, 
            access_token_key=settings.TWITTER_ACCESS_TOKEN_KEY,
            access_token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET)
    last = api.GetHomeTimeline()[0]
    return api.GetSearch(term=query)[0]

if __name__ == "__main__":
    print connect("hola")
