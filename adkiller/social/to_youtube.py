import gdata.youtube
import gdata.youtube.service

def connect():
    yt_service = gdata.youtube.service.YouTubeService()
    query = gdata.youtube.service.YouTubeVideoQuery()
    query.vq = "Cristina"
    query.orderby = 'viewCount'
    query.racy = 'include'
    feed = yt_service.YouTubeQuery(query).entry
    
    print feed[0].author[0].name.text


if __name__ == "__main__":
    connect()
