# -*- coding: utf-8 -*-
from adkiller.app.models import *

from django.core.management.base import BaseCommand
from django.conf import settings

from adkiller.app.social import to_facebook, to_twitter

class Command(BaseCommand):
    def handle(self, *args, **options):
    	for palabra in PalabraClave.objects.all():
			medio = Medio.objects.get(nombre="Facebook")
       		results = to_facebook.connect(palabra.palabra)
    		for result in results:
				programa = Programa.objects.get_or_create(nombre=result.from.name)
    			producto = Producto.objects.create(nombre=result.message, marca=palabra.marca)
    			horario = Horario.objects.get_or_create(medio=medio, programa=programa)
    			e = Emision.objects.create(tipo="C", producto=producto, fecha=e.created_time.hour, e.created_time.time, horario=horario)
    			