# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

import twitter, facebook
import urllib2, urllib
import gdata.youtube
import gdata.youtube.service
import feedparser

def get_twitter():
    api = twitter.Api(consumer_key="FaKfh9KgImc6EXTw6B86VA", 
            consumer_secret = "zqrafW7SF6SQAJWnIhMlMCDXHoiF0PPT8LOTNSfWDVg", 
            access_token_key="307345723-jCld0772DfSyeN9i7OYcMwSmLkMe9DGDgMnJ20eA",
            access_token_secret="tye1f4e5H4CquS8yUwqIW1UANzGAnM3Qsz64544Yic")
    return api


def get_facebook():
    app_id = "173419742768794"
    app_secret = "534ac6f673efdd0d77b77dc5ec867b37"
    graph = facebook.GraphAPI()
    args = {"client_id": app_id, "client_secret": app_secret, "grant_type": "client_credentials"}
    file = urllib2.urlopen("https://graph.facebook.com/oauth/access_token",
                                   urllib.urlencode(args))
    access_token = file.read().split("=")[1]
    graph = facebook.GraphAPI(access_token)
    return graph

def buscar_en_twitter(request):
    api = get_twitter()
    tweets = api.GetSearch(term=request.GET['query'])
    return render_to_response("tweets.html", locals())


def buscar_en_facebook(request):
    graph = get_facebook()
    posts = graph.request("/search", {"q": request.GET['query'], "type": "post"})
    return render_to_response("facebook.html", locals())


def buscar_en_noticias(request):
    args = {'q': request.GET['query'], "output": "rss"}
    response = feedparser.parse("https://news.google.com/news/feeds?"+ urllib.urlencode(args))
    return render_to_response("news.html", locals())


def buscar_en_youtube(request):
    yt_service = gdata.youtube.service.YouTubeService()
    query = gdata.youtube.service.YouTubeVideoQuery()
    query.vq = request.GET['query']
    query.orderby = 'viewCount'
    query.racy = 'include'
    videos = yt_service.YouTubeQuery(query).entry
    return render_to_response("youtube.html", locals())
