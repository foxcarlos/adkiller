# -*- coding: utf-8 -*-

from django.conf import settings
from django.db import models
from adkiller.app.models import Medio


class RatingsFile(models.Model):
    location = models.FilePathField(path=settings.RATINGS_PATH)
    date = models.DateField()
    version = models.IntegerField()
    processed = models.BooleanField(default=False)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('date', 'version')


class Rating(models.Model):
    ratings_file = models.ForeignKey(RatingsFile)
    channel = models.ForeignKey(Medio)
    rid = models.IntegerField()
    date = models.DateTimeField()
    ABC1, C2, C3, D1, D2, E = 1, 2, 3, 4, 5, 6
    SOCIAL = ((ABC1, 'ABC1'),
              (C2, 'C2'),
              (C3, 'C3'),
              (D1, 'D1'),
              (D2, 'D2'),
              (E, 'E'))
    social = models.IntegerField(choices=SOCIAL)
    MASCULINO, FEMENINO = 1, 2
    GENDER = ((MASCULINO, 'Masculino'),
              (FEMENINO, 'Femenino'))
    gender = models.IntegerField(choices=GENDER)
    A4_12, A13_19, A20_29, A30_39, A40_49, A50_64, A65_99 = 1, 2, 3, 4, 5, 6, 7
    AGE = ((A4_12, '4 a 12'),
           (A13_19, '13 a 19'),
           (A20_29, '20 a 29'),
           (A30_39, '30 a 39'),
           (A40_49, '40 a 49'),
           (A50_64, '50 a 64'),
           (A65_99, '65 a 99'))
    age = models.IntegerField(choices=AGE)

