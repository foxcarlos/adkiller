# -*- coding: utf-8 -*-

from csv import DictReader
from datetime import datetime
from zipfile import is_zipfile, ZipFile

from django.utils import timezone

#from adkiller.app.models import Medio
#from adkiller.ratings.models import Rating


def process_ratings(csv_file):
    count = 0
    csv_rows = DictReader(csv_file, dialect='excel-tab')
    for row in csv_rows:
        rid = row['Id']
        channel = row['CANAL']
        social = row['INSE']
        gender = row['GENDER']
        age = row['AGE']
        date = ' '.join([row['Fecha'].split()[0], row['FECHA Y HORA']])
        date = datetime.strptime(date, '%Y-%m-%d %H:%M')
        date = timezone.localtime(date)
#        channel = Medio.objects.get(ratings_channel_id=row['CANAL'])
#        Rating.objects.create(rid=row['Id'], channel=channel, date=date,
#                              social=row['INSE'], gender=row['GENDER'],
#                              age=row['AGE'])
        count += 1
    return count


def process_ratings_file(ratings_file):
    count = 0
    if is_zipfile(ratings_file.name):
        with ZipFile(ratings_file.name, 'r') as zip_file:
            with zip_file.open('RATING.CSV', 'rU') as csv_file:
                count = process_ratings(csv_file)
    else:
        with open(ratings_file.name, 'rU') as csv_file:
            count = process_ratings(csv_file)
    return count

