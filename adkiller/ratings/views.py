# -*- coding: utf-8 -*-

from datetime import date
from os import path, remove, rename
from zipfile import is_zipfile

from django.conf import settings
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt

from adkiller.ratings.models import RatingsFile
from adkiller.ratings.forms import UploadRatingsForm


TOKEN = 'krxW6eJUn6nLXadpQECjRRjQ3XcRj7nKq4UvhgSm'


def _write_file(source, dest):
    with open(dest, 'w') as dest_file:
        for chunk in source.chunks():
            dest_file.write(chunk)


@csrf_exempt
def upload_ratings(request):
    if request.method == 'POST':
        form = UploadRatingsForm(request.POST, request.FILES)
        if form.is_valid():
            if request.POST.get('token') == TOKEN:
                # Get the POST contents.
                file_date = request.POST.get('date')
                file_version = request.POST.get('version')
                file_path = path.join(settings.RATINGS_PATH,
                                      '{}_{}'.format(file_date, file_version))

                # Write the ratings file to disk.
                _write_file(request.FILES['ratings_file'], file_path)

                # Write the channels files to disk.
                channels = False
                if 'channels_file' in request.FILES:
                    _write_file(request.FILES['channels_file'],
                                file_path + '_channels')
                    channels = True

                # Prepare the database query.
                ext = '.zip' if is_zipfile(file_path) else '.csv'
                file_date = date(*map(int, file_date.split('-')))
                defaults = {'location': file_path + ext}

                # Add the ratings file's data to the database.
                _, new = RatingsFile.objects.get_or_create(
                    date=file_date,
                    version=file_version,
                    defaults=defaults
                )

                # Rename the files or delete them and return.
                if new:
                    rename(file_path, file_path + ext)
                    if channels:
                        rename(file_path + '_channels',
                               file_path + '.channels')
                    return HttpResponse('OK\n')
                else:
                    remove(file_path)
                    if channels:
                        remove(file_path + '_channels')
                    return HttpResponse('DUPLICATED\n')

    raise Http404(request.path)

