# -*- coding: utf-8 -*-

from django import forms

class UploadRatingsForm(forms.Form):
    token = forms.CharField()
    ratings_file = forms.FileField()
    date = forms.DateField(input_formats=['%Y-%m-%d'])
    version = forms.IntegerField()
    channels_file = forms.FileField(required=False)

