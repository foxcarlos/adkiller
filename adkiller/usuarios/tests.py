"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase


from django.contrib.auth.models import User

class UserTest(TestCase):
    """ Test de la vista para crear Usuarios """

    def setUp(self): 
        self.user  = User.objects.create_user("adkiller", 'ho...@simpson.net', "worms") 

    def test_add_user_ok(self):
        """ logueo, siendo staff, creo un usuario, 
            verifico que se cree el usuario """

        self.user.is_staff = True        
        self.user.save()
        login = self.client.login(username="adkiller", password="worms") 
        self.assertTrue(login) 
        response = self.client.get("/accounts/register/")
        form = {}
<<<<<<< HEAD
        form["first_name"]  = "Hola"
        form["last_name"]  = "Que tal"
=======
        form["first_name"]  = "prueba_fn"
        form["last_name"]  = "prueba_ls"
>>>>>>> 3b271ef5d9e15e6c8083bdd295580e45f3042319
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["passwordValidate"]  = "1212"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),2)
        self.assertNotEqual(User.objects.get(username="prueba"),None)
        self.assertRedirects(response,"/accounts/register_ok/"+str(User.objects.get(username="prueba").id)+"/")
        
    def test_add_user_incomplete_form(self):
        """ logueo, siendo staff, creo un usuario sin el formulario incompleto"""

        self.user.is_staff = True        
        self.user.save()
        login = self.client.login(username="adkiller", password="worms") 
        self.assertTrue(login) 
        response = self.client.get("/accounts/register/")
        form = {}
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),1)

    def test_add_user_bad_pass(self):
        """ logueo, siendo staff, creo un usuario pero me equivoco en la clave!, entonces
        no se crea el segundo usuario"""

        self.user.is_staff = True        
        self.user.save()
        login = self.client.login(username="adkiller", password="worms") 
        self.assertTrue(login) 
        response = self.client.get("/accounts/register/")
        form = {}
        form["first_name"]  = "prueba_fn"
        form["last_name"]  = "prueba_ln"
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["passwordValidate"]  = "hola"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),1)

        
    def test_add_user_sin_login(self):
        """ Si un cliente hace un get sin estar logueado recibe un responseredirect a login """
<<<<<<< HEAD
        url = "/accounts/registro"
        response = self.client.get(url)
        self.assertRedirects(response,"/accounts/login/?next=%s" % url)
=======
        response = self.client.get("/accounts/registro")
        self.assertRedirects(response,"/accounts/login/?next=/accounts/registro")
>>>>>>> 3b271ef5d9e15e6c8083bdd295580e45f3042319


    def test_add_user_login_not_staff(self):
        """ Si realizo un get sin ser staff, devuelve un template generico """
        login = self.client.login(username="adkiller", password="worms") 
        self.assertTrue(login)
        assert not User.objects.get(username="adkiller").is_staff
        response = self.client.get("/accounts/register/")
        response
        form = {}
        form["first_name"]  = "prueba_fn"
        form["last_name"]  = "prueba_ln"
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),1)

    def test_add_two_user_ok(self):
        """ logueo, siendo staff, creo un usuario, 
            verifico que se cree el usuario, 
            creo otro usuario con el mismo nombre y verifico que no lo haya creado """

        self.user.is_staff = True        
        self.user.save()
        login = self.client.login(username="adkiller", password="worms") 
        self.assertTrue(login) 
        response = self.client.get("/accounts/register/")
        form = {}
<<<<<<< HEAD
        form["first_name"]  = "Hola"
        form["last_name"]  = "Que tal"
=======
        form["first_name"]  = "prueba_fn"
        form["last_name"]  = "prueba_ls"
>>>>>>> 3b271ef5d9e15e6c8083bdd295580e45f3042319
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["passwordValidate"]  = "1212"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),2)
        self.assertNotEqual(User.objects.get(username="prueba"),None)
        self.assertRedirects(response,"/accounts/register_ok/"+str(User.objects.get(username="prueba").id)+"/")
        
        
        response = self.client.get("/accounts/register/")
        form = {}
<<<<<<< HEAD
        form["first_name"]  = "Hola"
        form["last_name"]  = "Que tal"
=======
        form["first_name"]  = "prueba_fn"
        form["last_name"]  = "prueba_ln"
>>>>>>> 3b271ef5d9e15e6c8083bdd295580e45f3042319
        form["username"]  = "prueba"
        form["password"]  = "1212"
        form["passwordValidate"]  = "1212"
        form["email"]  = "a@a.com"
 
        response = self.client.post("/accounts/register/",form)

        self.assertEqual(User.objects.all().count(),2)
        self.assertNotEqual(User.objects.get(username="prueba"),None)
        self.assertContains(response,"Ya existe un usuario con ese nombre")

