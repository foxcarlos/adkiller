from django.contrib.auth.models import User
from django import forms

ACTION_CHOICES = [('Cargar', 'Cargar'), ('Guardar', 'Guardar'), ]

import autocomplete_light


class UserForm(forms.Form):
    first_name = forms.CharField(help_text="", label="Nombre:",
                                max_length=30)
    last_name = forms.CharField(help_text="", label="Apellido:",
                                max_length=30)
    username = forms.CharField(help_text="", label="Nombre de usuario:",
                                max_length=255)
    password = forms.CharField(widget=forms.PasswordInput)
    passwordValidate = forms.CharField(widget=forms.PasswordInput,
                                label="Ingrese nuevamente el password")
    email = forms.EmailField(label="Ingrese su email:", required=True)

    def clean(self):
        try:
            if self.cleaned_data['password'] !=\
                self.cleaned_data['passwordValidate']:
                raise forms.ValidationError("Los Passwords no Coinciden!")
        except KeyError:
            pass
        if User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError("Ya existe un usuario con ese nombre")
	  # didn't find what we expected in data -
          # fields are blank on front end.  Fields
          # are required by default so we don't need to worry about validation
        if User.objects.filter(username=self.cleaned_data['username']):
            raise forms.ValidationError("Ya existe un usuario con ese nombre")
        return self.cleaned_data


class PackageForm(forms.Form):
    accion = forms.ChoiceField(widget=forms.RadioSelect, choices=ACTION_CHOICES)
    user = forms.ModelChoiceField(User.objects.all(),
                widget=autocomplete_light.ChoiceWidget('UserAutocomplete'))

    class Meta:
        model = User
        exclude = ["orden", "unidad", "comparado"]

# class AccessForm(forms.Form):
#     new_users = forms.BooleanField()


