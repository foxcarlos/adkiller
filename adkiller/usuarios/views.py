# -*- coding: utf-8 -*-
import json
from django.core.serializers.json import DjangoJSONEncoder
from adkiller.filtros.models import *
from models import *
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.admin.models import LogEntry
from datetime import datetime, date, timedelta

from forms import *

@staff_member_required
def register(request):
    """ Vista para registrar un usuario"""
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            if User.objects.filter(username=username):
                # "Ya existe un usuario con ese nombre"
                pass
            else:
                password = str(form.cleaned_data['password'])
                email = form.cleaned_data['email']
                first_name = form.cleaned_data['first_name']
                last_name = form.cleaned_data['last_name']

                usuario = User(username=username, email=email,
                    first_name=first_name, last_name=last_name)
                usuario.set_password(password)
                usuario.save()

                perfil = Perfil(user=usuario)
                perfil.save()
                perfil.crear_filtros()

                periodo = Periodo(perfil=perfil)
                periodo.hora_desde = datetime.now().strftime("%H:%M:%s")
                periodo.hora_hasta = datetime.now().strftime("%H:%M:%s")
                periodo.fecha_desde = (date.today() - timedelta(days=15)).\
                                                        strftime("%Y-%m-%d")
                periodo.fecha_hasta = date.today().strftime("%Y-%m-%d")
                periodo.save()
                return HttpResponseRedirect(reverse(register_ok,
                                                args=[usuario.id]))
    else:
        form = UserForm()
    return render_to_response('registration/registro.html', locals(),
                                    context_instance=RequestContext(request))

@staff_member_required
def register_ok(request, usuario):
    """ Vista que devuelve el template de usuario creado"""
    usuario = User.objects.get(id=usuario)
    return render_to_response('registration/registro_ok.html', locals(),
                                    context_instance=RequestContext(request))


@csrf_exempt
def ajax_login(request):
    from django.contrib.auth import authenticate, login
    try:
        if 'user' in request.POST and request.POST['user'] and request.POST['pass']:
            user = authenticate(username=request.POST['user'],password=request.POST['pass'])
        elif 'user' in request.GET and request.GET['user'] and request.GET['pass']:
            user = authenticate(username=request.GET['user'],password=request.GET['pass'])
        if user is not None:
            if user.is_active:
                login(request, user)
                res = {"status": True, "msg": "Cargando..."}
            else:
                res = {"status": False, "msg": "Cuenta desactivada"}
        else:
            res = {"status": False, "msg": "Datos incorrectos"}

    except Exception, e:
        res = {"status": False, "msg": "Usuario no válido"}

    data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


@csrf_exempt
def change_password(request):
    """ Vista para cambiar password"""
    usuario = request.user
    old = request.GET['old']
    pass_1 = request.GET['pass_1']
    pass_2 = request.GET['pass_2']
    respuesta = ""
    if usuario.check_password(old):
        if pass_1 == pass_2:
            if pass_1:
                usuario.set_password(pass_1)
                usuario.save()
            else:
                respuesta = "Formulario incompleto"
        else:
            respuesta = "Las claves nuevas no coinciden"
    else:
        respuesta = "Esa no es tu clave"
    res = {'respuesta': respuesta}
    data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


LOGIN = 5  # Represents the login action in LogEntry


def access_report(request):
    """ Vista para reporte de accesos"""
    logs = LogEntry.objects.filter(action_flag=LOGIN).order_by('-action_time')
    years = {}  # dict of years
    for log in logs:
        if User.objects.filter(id=log.object_id, is_staff=False):
            year = log.action_time.year
            if not year in years:
                years[year] = {}  # dict of months
            month = log.action_time.month
            if not month in years[year]:
                years[year][month] = 0

            years[year][month] = years[year][month] + 1

    filas = []
    for year, months in years.items():
        for month, counts in months.items():
            filas.append( (year, month, counts) )
    filas = sorted(filas)

    return render_to_response('reporte.html', locals(),
                                    context_instance=RequestContext(request))



from django.db.models import Count

def access_report_new(request):
    """ Vista para reporte de accesos"""
    users = User.objects.filter(is_staff=False)  # Obtener usuarios que no sean staff
    logs = LogEntry.objects.filter(action_flag=LOGIN, object_id__in=users).order_by('-action_time') # Obtener logs que sean LOGIN y que no sean usuarios staff
    filas = logs.values('action_time__year', 'action_time__month').order_by().annotate(Count('pk'))
    return render_to_response('reporte.html', local(), context_instance=RequestContext(request))
