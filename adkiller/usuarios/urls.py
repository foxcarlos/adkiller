from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
    url(r'^change_password/', change_password),
    url(r'^ajax_login/', ajax_login),
    
    #reporte
    url(r'^reporte/', access_report),
)
