# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.admin.models import LogEntry
from django.contrib.contenttypes.models import ContentType


def add_log(user, Model, object, action, message):
    if not user:
        try:
            user = User.objects.get(username="root")
        except:
            user = User.objects.all()[0]
    entry = LogEntry.objects.create(user=user,
            content_type_id=ContentType.objects.get_for_model(Model).pk,
            object_id=object.id, object_repr=object.__unicode__(),
                action_flag=action, change_message=message)

