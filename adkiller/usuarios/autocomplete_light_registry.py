import autocomplete_light

from django.contrib.auth.models import User   

from adkiller.filtros.models import Perfil

class UserAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('username',)
    autocomplete_js_attributes={'placeholder': 'Buscar por Usuario ..'}

autocomplete_light.register(User,UserAutocomplete)
