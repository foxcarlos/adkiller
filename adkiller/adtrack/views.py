# -*- coding: utf-8 -*-

"""
Views para la app adtrack
"""
# pylint: disable=star-args, fixme, too-many-locals, unused-variable,
# pylint: disable=line-too-long

# Python Imports
import json
import datetime

# Django Imports
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext


# Project Imports
from adkiller.adtrack.models import Tanda
from adkiller.app.models import Producto
from adkiller.app.models import Marca
from adkiller.app.models import Patron
from adkiller.app.models import EtiquetaProducto
from adkiller.app.models import Medio
from adkiller.app.models import Emision
from adkiller.app.models import TipoPublicidad
from adkiller.app.models import Anunciante
from adkiller.filtros.models import Perfil
from adkiller.filtros.models import Filtro
from adkiller.filtros.utils import FILTERS
from adkiller.app.exceptions import ErrorAlBorrarPatron

def custom_redirect(url_name, *args, **kwargs):
    """..."""
    from django.core.urlresolvers import reverse
    import urllib
    url = reverse(url_name, args=args)
    params = urllib.urlencode(kwargs)

    params = "&".join(["%s=%s"  % (k, v) for (k, v) in kwargs.items()])
    return redirect(url + "?%s" % params)


def cortar_patron(request):
    """..."""
    # hago una copia de GET
    params = {}
    if 'fecha' in request.GET:
        params['fecha'] = request.GET['fecha']
    if 'medio' in request.GET:
        params['medio'] = request.GET['medio']
    if 'cambiar_fecha' in request.GET:
        params['cambiar_fecha'] = request.GET['cambiar_fecha']

    # si no especifica fecha, que tome la fecha de hoy
    #if not "fecha" in params:
    #    params['fecha'] = datetime.date.today().strftime("%d/%m/%Y")
    #    return custom_redirect(cortar_patron, **params)

    # cambio la fecha una cierta cantidad de dias
    if "cambiar_fecha" in params:
        fecha = datetime.datetime.strptime(params['fecha'], "%d/%m/%Y")
        fecha += datetime.timedelta(days=int(params['cambiar_fecha']))
        del params['cambiar_fecha']
        params['fecha'] = fecha.strftime("%d/%m/%Y")
        return custom_redirect(cortar_patron, **params)

    # renderizo el template
    return render_to_response("cortar_patron.html", locals(),
                                    context_instance=RequestContext(request))


def get_medios(_):
    """..."""
    response = [(m.nombre, m.id)
                for m in Medio.objects.filter(
                    Q(soporte__nombre__icontains="TV")|
                    Q(soporte__nombre__icontains="Tele"))]
    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_productos_marca(request):
    """..."""

    if request.GET and request.GET['id_marca']:
        productos = Producto.objects.filter(marca__pk=request.GET['id_marca'])
        response = [{'producto_id':producto.id, 'producto_nombre': producto.nombre}
                    for producto in productos]
        data = json.dumps(response, cls=DjangoJSONEncoder)
        return HttpResponse(data, mimetype='application/json')


def get_etiquetas_producto(request):
    """..."""
    term = request.GET['term']
    response = [e.nombre for e in EtiquetaProducto.objects.all()
                if term.lower() in e.nombre.lower()]

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')

def get_anunciante_autocomplete(request):
    """..."""
    term = request.GET['term']
    response = [a.nombre for a in Anunciante.objects.all()
                if term.lower() in a.nombre.lower()]

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')

def get_marcas_autocomplete(request):
    """ Datos para el autocomplete de marcas """
    if request.GET:
        if 'term' in request.GET:
            texto = request.GET['term']
        # marca
        options = []
        perfil = Perfil.objects.get(user=request.user, activo=True)
        for tipo in ['marca']:
            filtro = Filtro.objects.filter(tipo=ContentType.objects.
                                            get(name=tipo), perfil=perfil)[0]
            puede = True
            #opciones = OpcionFiltro.objects.filter(filtro=filtro)
            #for opcion in opciones:
                #puede = puede and not opcion.inicial
            if puede or request.user.is_staff:
                filtro = FILTERS[tipo]
                instances = filtro.cls.objects.all()
                instances = instances.filter(
                    nombre__aicontains=texto
                ).distinct()
                options += [(l.id, str(l), tipo) for l in instances]

    return render_to_response('options_for_autocomplete.html', locals())


def get_productos_autocomplete(request):
    """ Datos para el autocomplete de productos """
    if request.GET:
        if 'term' in request.GET:
            texto = request.GET['term']
        if 'marca' in request.GET:
            id_marca = request.GET['marca']
        options = []
        perfil = Perfil.objects.get(user=request.user, activo=True)
        for tipo in ['producto']:
            filtro = Filtro.objects.filter(tipo=ContentType.objects.
                                            get(name=tipo), perfil=perfil)[0]
            puede = True
            #opciones = OpcionFiltro.objects.filter(filtro=filtro)
            #for opcion in opciones:
                #puede = puede and not opcion.inicial
            if puede or request.user.is_staff:
                filtro = FILTERS[tipo]
                instances = filtro.cls.objects.all()
                instances = instances.filter(
                    nombre__aicontains=texto,
                    marca__id=int(id_marca)
                ).distinct()
                options += [(l.id, str(l), tipo) for l in instances]

    return render_to_response('options_for_autocomplete.html', locals())


@transaction.commit_on_success
def guardar_producto_ajax(request):
    """..."""
    if request.GET:
        nombre_anunciante = request.GET['nombre_anunciante']
        marca_principal = request.GET['marca_principal']
        id_producto = request.GET['id_producto']
        nombre_producto = request.GET['producto']
        observaciones = request.GET['observaciones']
        tipo_de_publicidad = request.GET['tipo_de_publicidad']

        anunciante = Anunciante.objects.get(nombre=nombre_anunciante)
        marca = Marca.objects.get(nombre=marca_principal)
        if marca.anunciante != anunciante:
            marca.anunciante = anunciante
            marca.save()
        tipo_de_publicidad = TipoPublicidad.objects.filter(
            descripcion=tipo_de_publicidad
        )
        producto = Producto.objects.get(pk=id_producto)
        producto.nombre = nombre_producto
        producto.marca = marca
        producto.observaciones = observaciones
        if tipo_de_publicidad:
            print tipo_de_publicidad
            producto.tipo_publicidad = tipo_de_publicidad[0]

        if request.GET['marcas_secundarias']:
            marcas_secundarias = request.GET['marcas_secundarias'].split(',')
            # primero sacamos las que había
            producto.otras_marcas.clear()
            for marca_secundaria in marcas_secundarias:
                ms = Marca.objects.get(nombre=marca_secundaria)
                print ms
                producto.otras_marcas.add(ms)

        if request.GET['etiquetas_producto']:
            etiquetas_producto = request.GET['etiquetas_producto'].split(',')
            # primero sacamos las que había
            producto.etiquetas.clear()
            for etiqueta in etiquetas_producto:
                # TODO: no permitir la creación de etiquetas
                et, _ = EtiquetaProducto.objects.get_or_create(nombre=etiqueta)
                producto.etiquetas.add(et)

        producto.categorizacion_pendiente = False
        producto.save()

    return HttpResponse("")

def get_tipos_de_publicidad(_):
    """..."""
    tipos = TipoPublicidad.objects.filter(descripcion__isnull=False)
    response = []
    for tipo in tipos:
        response.append(dict(identificador=tipo.nombre,
                             descripcion=tipo.descripcion))

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_productos_sin_categorizar(request):
    """Filtrar productos que muestra la view cargar_producto"""

    productos = Producto.objects.filter(

        # aún no se revisaron
        categorizacion_pendiente=True,

        # tiene segmentos
        segmentos__isnull=False,

        # y alguno de ellos se usó para entrenar AdTrack
        segmentos__usado_para_entrenar=True,

    )
    # ).filter(

        # esto está bueno pero hace más lento todo

        # ya tiene patrón (se migró desde server de detección)
        # o pasó suficiente tiempo y nunca se detectó
        # Q(emisiones__audiovisual__score__gt=0) |
        # Q(creado__gt=datetime.datetime.now() - datetime.timedelta(hours=12))
    # )

    if request.GET:
        filtro = request.GET['filter']
        valor = request.GET['value']

        if filtro == 'sector':
            productos = productos.filter(marca__sector=valor)
        elif filtro == 'industria':
            productos = productos.filter(marca__sector__industria=valor)
        else:
            productos = productos.filter(marca=valor)

    ayer = (datetime.datetime.now() - datetime.timedelta(days=1)).date()

    response = [
        {
            'pk':producto.id,
            'creado': '{:%Y-%m-%d %H:%M}'.format(producto.creado) if producto.creado else '',
            'video_url': producto.video(),
            'nombre': producto.nombre,
            'marca': producto.marca.nombre,
            'anunciante': producto.marca.anunciante.nombre if producto.marca.anunciante else '',
            'otras_marcas': ','.join([om.nombre for om in producto.otras_marcas.all()]),
            'thumb':producto.thumb(),
            'observaciones':producto.observaciones,
            'etiquetas': ','.join([et.nombre for et in producto.etiquetas.all()]),
            'tipo': producto.tipo_publicidad.descripcion if producto.tipo_publicidad else 'No tiene',
            'permitir_carga_archivo': (producto.patron_principal() and 'mp4' in producto.patron_principal().inventa_filename()),

            # cantidad de detecciones por media map
            'em_mm': producto.emisiones.filter(Q(audiovisual__score__isnull=True) | Q(audiovisual__score__lte=0)).filter(fecha__gte=ayer).count(),
            'em_auto': producto.emisiones.filter(audiovisual__score__gt=0).filter(fecha__gte=ayer).count(),
            'pat_ppal_id_s': producto.patrones.filter(id_en_simpson__isnull=False)[0].id_en_simpson if producto.patrones.filter(id_en_simpson__isnull=False) else -1,
        }
            for producto in productos]

    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_duracion_id_patron_producto(request):
    """..."""

    if request.GET and request.GET['id_producto'] and request.GET['server']:
        id_producto = request.GET['id_producto']
        server_url = request.GET['server'].split(':')[2]

        emision = Emision.objects.filter(producto__id=id_producto)
        if emision:
            duracion = emision[0].duracion
        else:
            duracion = ''

        patrones = Patron.objects.filter(
            producto__id=id_producto,
            server__puerto_http=server_url
        )
        if patrones:
            id_patron = patrones[0].pk
        else:
            id_patron = ''
        response = {
            'pk':id_producto,
            'duracion': duracion,
            'id_patron':id_patron
        }
        data = json.dumps(response, cls=DjangoJSONEncoder)
        return HttpResponse(data, mimetype='application/json')


def get_server_url(request):
    """..."""
    response = {}
    # TODO: Make a simple form for Medio Model and validate request data
    # received
    if request.method == 'GET' and request.is_ajax() and request.GET['medio']:
        try:
            medio = Medio.objects.get(pk=request.GET['medio'])
        except Medio.DoesNotExist:
            medio = None
        if medio:
            response = {
                'pk': medio.id,
                'url_server': 'http://{medio_url}:{port}'.format(
                    **{'medio_url': medio.server.url,
                       'port': medio.server.puerto_http})
                }
    data = json.dumps(response, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_estado_tanda(request):
    """Devuelve el estado de la tanda, True: completa, False: incompleta"""

    response = []
    lista_tandas = []
    if request.GET and request.GET['tandas']:
        tandas = request.GET['tandas'].split(',')
        for nombre_tanda in tandas:
            # Buscamos si está completa, sinó creamos una incompleta
            nombre_split = nombre_tanda.split('-')
            hora = nombre_split[3][0:2] + ':' + nombre_split[3][2:4]
            fecha = '20' + nombre_split[1][4:6] + '-' + nombre_split[1][2:4] + '-' + nombre_split[1][:2]
            tanda, _ = Tanda.objects.get_or_create(nombre=nombre_tanda, fecha=fecha, hora=hora)

            lista_tandas.append(tanda)

        response = [
                {
                    'tanda':tanda.nombre,
                    'completa':tanda.completa,
                    'fecha':formatear_fecha(tanda.fecha),
                    'hora': str(tanda.hora).split(':')[0]
                } for tanda in lista_tandas]
        data = json.dumps(response, cls=DjangoJSONEncoder)
        return HttpResponse(data, mimetype='application/json')


def set_estado_tanda(request):
    """Cambia el estado de una tanda"""

    if request.GET and request.GET['tanda']:
        try:
            tanda = Tanda.objects.get(nombre=request.GET['tanda'])
            tanda.completa = True
            tanda.save()
            respuesta = 'Ok'
        except Exception, e:
            raise e

    return HttpResponse(respuesta)

def formatear_fecha(fecha):
    """..."""
    fecha = str(fecha).split('-')
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0][2:4]

def validar_anunciante(request):
    """..."""
    if request.GET:
        nombre_anunciante = request.GET['nombre_anunciante']
        response = {'anunciante_existe': False}
        a = Anunciante.objects.filter(nombre=nombre_anunciante)
        if a:
            response['anunciante_existe'] = True

        data = json.dumps(response, cls=DjangoJSONEncoder)
        return HttpResponse(data)

def check_tag_exists(request):
    """..."""
    if request.GET:
        tipo = request.GET['tipo']
        valor = request.GET['valor']
        response = {'tipo': tipo, 'valor': valor, 'tag_exists': True}

        if tipo == 'marca':
            m = Marca.objects.filter(nombre=valor)
            if not m:
                response['tag_exists'] = False
        elif tipo == 'tag':
            e = EtiquetaProducto.objects.filter(nombre=valor)
            if not e:
                response['tag_exists'] = False
        else:
            response['tag_exists'] = False

        data = json.dumps(response, cls=DjangoJSONEncoder)
        return HttpResponse(data)


def lookup_filtro_cargar_producto(request):
    """..."""
    if request.GET:
        if 'texto' in request.GET:
            texto = request.GET['texto']
        # TODO: no harcodear esto
        fecha = datetime.datetime(2014, 8, 1)
        if 'fecha' in request.GET:
            fecha = request.GET['fecha']

        options = []
        perfil = Perfil.objects.get(user=request.user, activo=True)
        tipos = {
            'industria': 'sectores__marcas__productos',
            'sector': 'marcas__productos',
            'marca': 'productos',
            }
        for tipo in tipos.keys():
            print "\nfiltrando por", tipo
            filtro = Filtro.objects.filter(tipo=ContentType.objects.
                                            get(name=tipo), perfil=perfil)[0]
            filtro = FILTERS[tipo]
            instances = filtro.cls.objects.all()
            instances = instances.filter(nombre__aicontains=texto).distinct()
            dictio = {tipos[tipo] + "__categorizacion_pendiente": True}
            if fecha:
                dictio[tipos[tipo] + "__creado__gte"] = fecha
            instances = instances.filter(**dictio)
            if fecha:
                instances = instances.filter()

            print instances
            options += [(l.id, str(l), tipo) for l in instances]

    return render_to_response('options_for_autocomplete.html', locals())


@staff_member_required
def sube_video(request):
    """..."""
    if request.GET:
        producto = Producto.objects.get(id=request.GET['prod_id'])
        patron = producto.patron_principal()
        if not patron:
            return render_to_response("sube_video_no_hay_patron.html", {})

        nombre_salida = patron.mp4_filename
        if not nombre_salida:
            nombre_salida = patron.inventa_filename()
            # TODO: a quién se lo asigno si es de radio?
            patron.mp4_filename = nombre_salida
            patron.save()

        url_cargar_archivo = 'http://{}/API/put_video.php'.format(
            patron.server.url_completa()
        )
        context = RequestContext(
            request,
            {
                'nombre_en_simpson': nombre_salida,
                'url_cargar_archivo': url_cargar_archivo,
                'producto': producto,
                'etiquetas': ', '.join([e.nombre for e in
                                        producto.etiquetas.all()]),
                }
        )
        return render_to_response("sube_video.html", context)
    else:
        return custom_redirect('/adtrack/')

@staff_member_required
def borrar_patron(request):
    """Eliminar patrón principal del producto
    del servidor de detección"""

    try:
        id_en_simpson = int(request.POST.get('pat_ppal_id_s'))
        patron = Patron.objects.filter(id_en_simpson=id_en_simpson)[0]
        patron.desentrenar()
    except (KeyError, ObjectDoesNotExist, ErrorAlBorrarPatron, IndexError, AttributeError) as e:
        # result = dict(success=False, msg='Error al borrar patrón')
        result = dict(success=False, msg=e.message)
    else:
        result = dict(success=True, msg='Patrón borrado')
    result = json.dumps(result, cls=DjangoJSONEncoder)
    return HttpResponse(result, mimetype='application/json')


