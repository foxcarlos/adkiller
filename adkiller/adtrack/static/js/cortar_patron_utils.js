/*
    - Vaciar resultados.
    - Escape de caracteres especiales en autocomplete.
    - Colores para bordes y funciones para resaltado.
*/


function vaciar_resultados(){
  unk_tanda = {"tanda":"","reconocidas_por_adtrack":[],"no_reconocidas_por_adtrack":[],"nuevos_patrones":[]};
  // tiene el estado de las tandas (verde, gris, rojo)
  lista_tandas_estado = '';
  // Variables para identificar que thumb se selecciona
  grande1 = grande2 = chico1 = chico2 = "";
  no_tandas_pendientes = false;
  loader.fadeIn();
  $('#container').children().remove();
}

// Para evitar que escape valores como & en autocomplete.
var decodeEntities = (function() {

  var element = document.createElement('div');

  function decodeHTMLEntities (str) {
    if(str && typeof str === 'string') {
      // strip script/html tags
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }
    return str;
  }
  return decodeHTMLEntities;
})();


/* 
  Colores para los cortes y funciones para resaltar bordes
*/
i = -1;
function randomColor(){
  colors = ['#000099', '#FF00FF', '#663300', '#FF0000', '#993300', '#006600', '#9900CC', '#0F2E0F', '#800000','#660066' ,'#28ed7a', '#d3fc1b', '#ff0000'];
  if (i+1 == colors.length) {
    i = 0;
  } else {
    i = i+ 1;
  }
  return colors[i];
}

function resaltarBordes (id, color) {
  $('#' + id).addClass('resaltado').css('color',color);
};

function resaltarIntermedios(primero, segundo, color) {
  var segundo_split = segundo.split('-');
  var desde_primero = primero.split('-')[1];
  var desde_segundo = segundo_split[2];
  

  var nro_primero = '';
  var nro_segundo = '';

  if ($('#'+primero).attr('nro')) {
    nro_primero = $('#'+primero).attr('nro');
  } else {
    nro_primero = $('#'+primero).parents('.div-padre').attr('nro');
  }

  if ($('#'+segundo).attr('nro')) {
    nro_segundo = $('#'+segundo).attr('nro');
  } else {
    nro_segundo = $('#'+segundo).parents('.div-padre').attr('nro');
  }

  if (segundo_split.length == 4) {
    primero_publicidad = segundo_split[0] + '-0-' + segundo_split[2] + '-' + segundo_split[3];
  } else {
    primero_publicidad = segundo_split[0] + '-0-' + segundo_split[2]; 
  }

  if (nro_primero != nro_segundo) {
    // Si nro_segundo - nro_primero = 1 es porque son consecutivas
    // Recorremos desde el primer thumb del segundo hasta el ultimo
    if (parseInt(nro_segundo) - parseInt(nro_primero) == 1 ) {
      $("#i-"+ primero_publicidad).addClass("resaltado").css('color',color);
      $("#i-"+ primero_publicidad).nextUntil("#i-" + segundo).find('a').addClass("resaltado").css('color',color);
    } else {
      // Si nro_segundo - nro_primero > 1 es porque pinto toda la del medio
      // Pintar toda la del medio
      // Recorrer desde el primer thumb del último hasta el ultimo thumb
      var nro_medio = parseInt(nro_segundo) - 1;
      $('div[nro="'+nro_medio+'"]:first').find('a').addClass("resaltado").css('color',color);
      $("#i-"+ primero_publicidad).addClass("resaltado").css('color',color);
      $("#i-"+ primero_publicidad).nextUntil("#i-" + segundo).find('a').addClass("resaltado").css('color',color);

    }

  }

  $("#i-"+ primero).nextUntil("#i-" + segundo).find('a').addClass("resaltado").css('color',color);
};