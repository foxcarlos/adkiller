// chequear si una marca secundaria o tag existe en la bbdd
$(document).ready(function(){


function _control_tag_o_marca(tipo, valor)
{
    existe = true;
    $.ajax({
        dataType: "json",
        url: '/adtrack/check_tag_exists/',
        data: {'tipo': tipo, 'valor': valor},
        async: false,
        success: function(json){
            if(!json['tag_exists']) existe = false;
        },
    });
    return existe;
}

// Active al nav
$('#cargar_producto').addClass("active");

/* --- Buscador/Autocomple --- */
function autocompletar(){
    $("#autocomplete input").autocomplete({
        minLength: 3,
        source: function(request, response) {
        // TODO: ver cómo pasarle una fecha
            $.getJSON("/adtrack/lookup_filtro_cargar_producto/", { texto: request.term }, response);
        },
        select: function( event, ui ) {
            vaciar_resultados();
            buscador(ui.item.data, ui.item.id);
        }
    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.value + '<span style="float: right; color: #222;">' + item.data + "</span></a>" )
            .appendTo( ul );
    };
};

autocompletar();


var indice = 0;
var productos = '';
var tipos_de_publicidad = '';

$.getJSON('/adtrack/get_tipos_de_publicidad/', {}, function(data){
        // tomamos los datos para la variable global
        tipos_de_publicidad = data;
});



function buscador(filter, value) {

    var data = {};

    if (filter || value) {
        data = {'filter':filter,'value':value};
    }

    // muestro loader
    $('#video_loading').css({"display": "block"});

    $.getJSON("/adtrack/get_productos_sin_categorizar/", data, function(data){
        // borro loader
        $('#video_loading').css({"display":"none"})
        // Seteo cantidad pendientes inicial
        $('#productos-pendientes').text(data.length + ' pendientes');
        productos = data;
        mostrar();
    });

};

function mostrar()  {
    // Muestro el producto
    if (indice < productos.length) {
        resultados(productos[indice], indice);
    }
};



function resultados(producto, indice) {


    var $container = $('#container');

    var cant_emisiones = producto.em_auto + producto.em_mm;
    if (cant_emisiones > 0) {
        var autom = (producto.em_auto / (cant_emisiones) * 100).toFixed(2);
        var manual = (producto.em_mm / (cant_emisiones) * 100).toFixed(2);
    } else {
        var autom = '0.00';
        var manual = '0.00';
    }



    // Video
    html =  '<div id="'+indice+'" class="row row-offset-0">';
    html += '<div class="col-md-6">';

    html += '<video id="video-'+indice+'" class="video-js vjs-default-skin" controls preload="auto" width="700" height="400"';
    html += 'poster="'+producto.thumb+'"';
    html += 'data-setup="{"example_option":true}">';
    html += '<source src="'+producto.video_url+'" type="video/mp4" />';
    html += '<source src="'+producto.video_url+'" type="video/flv" /></video>';
    html += '<div id="datos_deteccion">';
    html += '<h4>Detecciones (últimas 24 hs):</h4>';
    html += '<table>';
    html += '<tr><td> Emisiones detectadas por AdTrack: </td><td>' + producto.em_auto + '</td><td>(' + autom + '%)</td><tr>';
    html += '<tr><td> Emisiones insertadas manualmente: </td><td>' + producto.em_mm + '</td><td>(' + manual + '%)</td><tr>';
    html += '<tr><td> Fecha de creación del producto: </td><td></td><td>' + producto.creado +'</td></tr>';
    html += '</table>';
    html += '</div>';
    html += '</div>';

    // Contenido aside
    html += '<div class="col-md-4 col-md-offset-1"><form role="form">';

    // Anunciante
    html += '<label for="">Anunciante <br><span style="color: rgb(18, 53, 238); font-size: 0.9em; font-weight: normal">Al cambiar el anunciante estará cambiando el anunciante de la <b>marca</b>!</span></label>';
    html += '<input id="anunciante-'+indice+'" type="text" class="form-control anunciante" value="'+producto.anunciante+'" name="anunciante" placeholder="Anunciante"><br>';

    // Marca principal
    html += '<label for="">Marca</label><div class="input-group">';
    html += '<input id="marca-'+indice+'" type="text" class="form-control marca_principal" value="'+producto.marca+'" name="marcaPrincipal" placeholder="Marca Principal">';


    // Marcas secundarias
    html += '<span class="input-group-btn">';
    html += '<button id="ms-'+indice+'" data-toggle="tooltip" title="Cargar marcas secundarias" type="button" class="btn btn-default marcaSecundaria"><i id="btn-'+indice+'" class="fa fa-plus"></i></button></span></div>';
    html += '<div  id="dvms-'+indice+'" class="form-group ocultocompleto"><br><label for="version">Marcas Secundarias</label>';
    html += '<input id="secundarias-'+indice+'" class="secundarias_tags" name="marcas_secundarias" type="text"';
    html += 'value="' + producto.otras_marcas + '"/></div><br>';

    // Producto
    html += '<div class="form-group"><label for="">Producto</label>';
    html += '<input id="producto-'+indice+'" type="text" class="form-control producto" placeholder="Producto" value="'+producto.nombre +'"></div>';

    // Campaña
    //html += '<div class="input-group">';
    //html += '<input type="text" class="form-control" id="version" placeholder="Campaña - Versión"><span class="input-group-btn">';
    //html += '<button data-toggle="tooltip" title="Cargar metadata" type="button" class="btn btn-default"><i class="fa fa-plus"></i></button></span></div><br>';

    // Tipo de publicidad
    html += '<div class="btn-group form-group"><label for="">Tipo de Publicidad</label><br>';
    html += '<a id="tipo_de_publicidad-'+indice+'" class="btn btn-default dropdown-toggle btn-select tipo_de_publicidad" data-toggle="dropdown" href="#">'+ producto.tipo + '&nbsp;<span class="caret"></span></a>';
    html += '<ul class="dropdown-menu">';
    for(var iii = 0; iii < tipos_de_publicidad.length; ++iii)
    {
        html += '<li><a href="#">' + tipos_de_publicidad[iii].descripcion + '</a></li>';
    }
    html += '</ul></div><br>';

    // Observaciones
    html += '<div class="form-group"><label for="">Observaciones</label>';
    html += '<input id="obervaciones-'+indice+'" type="text" class="form-control observaciones" placeholder="Observaciones" value="'+producto.observaciones +'"></div>';

    // Etiquetas
    html += '<div class="form-group"><label for="version">Etiquetas</label>';
    html += '<input id="etiquetas-'+indice+'" class="prod_tags" name="tags" type="text" ';
    html += 'value="' + producto.etiquetas + '"/></div>';

    // Storyboard - Detalle
    //html += '<div class="form-group"><label for="observaciones">Storyboard - Detalle</label>';
    //html += '<textarea class="form-control" rows="3" id="observaciones"></textarea></div>';
    html += '</form>';
    html += '</div><!-- / .col -->';
    html += '</div><!-- / .row --><br>';

    // Footer producto
    html += '<div id="footer-'+indice+'">';
    html += '<div id="progress-flag-save" class="container" style="background-color:lightgrey;">';
    html += '<div class="row">';
    //html += '<div class="col-md-1 col-md-offset-9">';
    //html += '<a href="#" data-toggle="tooltip" title="Reportar problema"><i style="color: #E60000;" class="fa fa-flag fa-2x"></i></a>';
    //html += '</div>';
    //html += '<button class="btn btn-info btn-lg btn-block" id="btnCargar" type="file"><span class="glyphicon glyphicon-cloud-upload"></span>Cargar Video</button>';

    // BOTON ANTERIOR
    html += '<div class="col-md-1">';
    html += '<button type="submit" class="btn btn-primary btn-file" id="btnAnterior"><span class="glyphicon glyphicon-arrow-left"></span></button>';
    html += '</div>';

    // BOTON SUBIR VIDEO
    html += '<div class="col-md-2">';
    if(producto.permitir_carga_archivo) {
        html += '<button id="btnCargar" class="btn btn-info btn-file btn-block"><span class="glyphicon glyphicon-cloud-upload"></span>Nuevo video</span>';
    }
    html += '</div>';

    // SEPARADOR
    html += '<div class="col-md-4">';
    html += '</div>';

    // BOTON BORRAR PATRON
    html += '<div class="col-md-2">';
    if (producto.pat_ppal_id_s > 0) {
        html += '<button id="btnBorrar" class="btn btn-danger btn-file btn-block"><span class="glyphicon glyphicon-remove"></span>Eliminar patrón</span>';
    };
    html += '</div>';

    // BOTON GUARDAR
    html += '<div class="col-md-2">';
    html += '<button type="submit" class="btn btn-success btn-file btn-block"  id="btnGuardar"><span class="glyphicon glyphicon-ok"></span>Guardar</button>';
    html += '</div>';

    // BOTON SIGUIENTE
    html += '<div class="col-md-1">';
    html += '<button type="submit" class="btn btn-primary btn-file" id="btnSiguiente"><span class="glyphicon glyphicon-arrow-right"></span></button>';
    html += '</div>';

    html += '</div><!-- fin row contiene botones -->';
    html += '</div><!-- fin class container -->';

    $container.html(html);

    $('#btnCargar').click(function(){
        window.open('/adtrack/sube_video?prod_id='+producto.pk,'_blank');
    });

    $('#btnSiguiente').click(function(){
        // poner este al final
        productos.push(productos.shift());
        mostrar();
    });

    $('#btnAnterior').click(function(){
        // poner este al final
        productos.unshift(productos.pop());
        mostrar();
    });

    $('#btnBorrar').click(function(){
        $('#btnBorrar').prop('disabled', true);
        $.ajax({
            url: '/adtrack/borrar_patron/',
            async: true,
            type: 'POST',
            dataType: 'json',
            data: {
                pat_ppal_id_s: producto.pat_ppal_id_s,
                csrfmiddlewaretoken: $('input[type=hidden]').prop('value'),
            },
            success : function(data){
                console.log(data);
                if (!data.success) {
                    alert(data.msg);
                    $('#btnBorrar').prop('disabled', false)
                } else {
                    $('#btnBorrar').prop('disabled', false)
                    $('#btnBorrar').html('Patrón eliminado');
                }
            },
            error : function(json){
                alert('Error al eliminar patrón');
                $('#btnBorrar').prop('disabled', false)
            },
        });
    });



    // función para responder a los cambios en el combobox "Tipo de publicidad"
    $(".dropdown-menu li a").click(function(){
          var selText = $(this).text();
            $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
            });


    // Autocomplete en marca
    $("#marca-"+indice).autocomplete({
        minLength: 3,
        source: '/app/get_marcas_autocomplete/',
    });

    // Tags autocomplete en marcas secundarias
    $('#secundarias-'+indice).tagsInput({
        'defaultText':'agregar',
        autocomplete_url: '/app/get_marcas_autocomplete/',
        'height':'100px',
        'width':'358px',
        onAddTag: function (marca) {
            existe = _control_tag_o_marca('marca', marca);
            if(!existe)
            {
                $('#secundarias-'+indice).removeTag(marca);
            }
        }
    });

    // Autocomplete en anunciante
    $("#anunciante-"+indice).autocomplete({
        minLength: 3,
        source: '/app/get_anunciante_autocomplete/',
    });


    // Tags autocomplete en etiquetas
    $('#etiquetas-'+indice).tagsInput({
        'defaultText':'agregar',
        autocomplete_url: '/app/get_etiquetas_producto/',
        'height':'100px',
        'width':'358px',
        onAddTag: function (tag) {
            existe = _control_tag_o_marca('tag', tag);
            if(!existe)
            {
                $('#etiquetas-'+indice).removeTag(tag);
            }
        }
    });
};


// Guardar producto
$(document).delegate("#btnGuardar", "click", function (e) {
    console.log('Guardando');
    e.preventDefault();
    var marca_principal = $('.marca_principal').val();
    var marcas_secundarias = $('.secundarias_tags').val();
    var producto = $('.producto').val();
    var tipo_de_publicidad = $('.tipo_de_publicidad').text().trim();
    var observaciones = $('.observaciones').val().trim();
    var etiquetas_producto = $('.prod_tags').val();
    var video = productos[indice].video_url;
    var id_producto = productos[indice].pk;
    var anunciante = $('.anunciante').val();


    if (marca_principal != '' && producto != '') {
        if (anunciante == ''){
            alert('Por favor, especifique el Anunciante.');
            return;
        } else {
            // Validar el anunciante
            var anunciante_existe = false;
            $.ajax({
                url: '/adtrack/validar_anunciante',
                async: false,
                type: 'GET',
                dataType: 'json',
                data: {
                    nombre_anunciante: anunciante,
                    },
                success : function(json){
                    console.log(json);
                    console.log(json['anunciante_existe']);
                    if(json['anunciante_existe']) {
                        console.log("Si existe");
                        anunciante_existe = true;
                    }
                },
            });
            if (!anunciante_existe){
                alert("Por favor, especifique un anunciante válido.");
                return;
            }
        }
        $.ajax({
            url: '/app/guardar_producto_ajax/',
            type: 'GET',
            data: {
                    nombre_anunciante: anunciante,
                    id_producto: id_producto,
                    marca_principal: marca_principal,
                    marcas_secundarias: marcas_secundarias,
                    producto: producto,
                    observaciones: observaciones,
                    tipo_de_publicidad: tipo_de_publicidad,
                    etiquetas_producto: etiquetas_producto,
                },
            success: function(data){
                // Efecto fadeout del producto y footer del producto y los elimino del DOM
                $('#'+indice).fadeOut( "slow", function() {
                    $(this).remove();
                });
                $('#footer-'+indice).fadeOut( "slow", function() {
                    $(this).remove();
                });
                indice = indice + 1;
                disminuir_pendientes();
                mostrar();
            },
        });
    } else {
        alert('Por favor, ingrese la marca y el nombre del producto.');
    }
});


$(document).delegate(".marcaSecundaria", "click", function (e) {
    e.preventDefault();
    // acá la variable indice se convirtió a string
    indice = $(this).attr('id').split('-')[1];
    indice = parseInt(indice);
    boton = $("#btn-" + indice);
    if (boton.hasClass('fa-plus')) {
      $("#dvms-"+indice).removeClass('ocultocompleto');
      boton.removeClass('fa-plus');
      boton.addClass('fa-minus');
    } else {
      $("#dvms-"+indice).addClass('ocultocompleto');
      boton.removeClass('fa-minus');
      boton.addClass('fa-plus');
    }
});


function disminuir_pendientes() {
    actual = parseInt($('#productos-pendientes').text().split(' ')[0]);
    nuevo = $('#productos-pendientes').text(actual-1 + ' pendientes');
}

function vaciar_resultados(){
    $('#container').children().remove();
}

buscador('', '');

});//fin document.ready
