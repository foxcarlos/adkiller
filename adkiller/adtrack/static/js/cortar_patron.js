$('#cortar_patron').addClass('active');
$('#detectadas').addClass('active');

/* Variables Globales */
loader = $('.Loader');
loader.fadeOut();

// flag para saber si hubo error al traer thumbs o no
error_thumbs = false;

// Numero de cada publicidad dentro de la tanda
nro = -1

// flag para cuando no hay tandas pendientes
no_tandas_pendientes = false;

// tiene el estado de las tandas (verde, gris, rojo)
lista_tandas_estado = '';

// Indice de las tandas
indice = 0;

// Fecha actual usada para la navegación de tandas
fecha_actual = '01/01/2000'

lapso = 'T';

// Variables para identificar que thumb se selecciona
grande1 = grande2 = chico1 = chico2 = "";

// json para guardar la info de los cortes
unk_tanda = {"tanda":"","reconocidas_por_adtrack":[],"no_reconocidas_por_adtrack":[],"nuevos_patrones":[]};

$("[rel='tooltip']").tooltip();  

// Guarda el servidor de la tanda actual
servidor_actual = '';


/* Devuelve las tandas del medio con el lapso 
   calculado en buscador. Setea "tandas pendientes" */
function traer_tandas(server,medio, desde, hasta) {
        tandas = '';
    $.ajax({
        url: server + "/API/get_tandas.php",
        data: {'medio':medio, 'lapso': 'T', 'desde': desde, 'hasta': hasta },
        async: false,
        dataType: 'json',
        success: function(respuesta) {
                if (respuesta['tandas'].length == 0) {
                        no_tandas_pendientes = true;
                        var html = '<br><br><div class="row"><div class="col-md-5 col-md-offset-3"><div id="no-tandas" class="alert alert-info">';
                        html += '<span class="glyphicon glyphicon glyphicon-info-sign"></span> No hay tandas pendientes para este medio y lapso.</div></div></div>';
                        $('#container').html(html);
                        loader.fadeOut();
                }
                var uniqueTandas = [];
                        $.each(respuesta['tandas'], function(i, el){
                            el = el.split(',');
                            var nombre = el[0];
                            var duracion = Math.round(parseInt(el[1]) / 60);
                            var hora_inicio = parseInt(nombre.split('.')[0].split('-')[3]);
                            var minuto_inicio = (hora_inicio / 100 >> 0) * 60 + hora_inicio % 100;
                            var tanda = {
                                'nombre': nombre,
                                'duracion': duracion,
                                'minuto_inicio': minuto_inicio,
                                'completa': false,
                                };
                            if($.inArray(tanda, uniqueTandas) === -1) uniqueTandas.push(tanda);
                        });
                tandas = uniqueTandas;
                // Seteo cantidad pendientes inicial
                        $('#tandas-pendientes').text(tandas.length + ' tandas pendientes');
        },
        error: function(e){
                loader.fadeOut();
        }
    });
    return tandas;
};


function mostrar()  {

        lapso = 'T';
    fecha = getUrlVars()['fecha'];
    if (!fecha ||fecha == "undefined") fecha=null;
        var medio = $('select option:selected').attr('idmedio');
        var medio_text = getUrlVars()['medio'];

        if (typeof medio === "undefined") {
            var html = '<br><br><div class="row"><div class="col-md-5 col-md-offset-3"><div id="error-cargar-tanda" class="alert alert-danger">';
        html += '<span class="glyphicon glyphicon-warning-sign"></span> Elija un medio y fecha.<br></div></div></div>';
                $('#container').html(html);
                return;
        }

        // Busco la url del server
        $.ajax({
                beforeSend: function() { 
            loader.fadeIn();
        }, 
        url: "/adtrack/get_server_url/",
        data: {'medio':medio},
        //async: false,
        dataType: 'json',
        success: function(respuesta) {
                var url_server = respuesta['url_server'];
                servidor_actual = url_server;
            if (fecha == null) { 
                $.ajax({
                        url: servidor_actual + "/API/get_tandas.php",
                        data: {'medio':medio, 'lapso': 'T', 'desde': "", 'hasta': "" },
                        async: false,
                        dataType: 'json',
                        success: function(respuesta) {
                            fecha = respuesta['tandas'][0].split("-")[1];
                            fecha = fecha.slice(0,2) + "/" + fecha.slice(2,4) + "/20" + fecha.slice(4,6);
                        window.location.search = "?medio=" + medio_text + "&fecha=" + fecha;
                   }
                });
                return;
            };

                if (indice == 0) {
                        tandas = traer_tandas(url_server,medio, fecha, fecha);
                        get_info_tanda_siguiente(tandas[indice+1]['nombre'], url_server,indice)
                        get_info_tandas(tandas[indice]['nombre'], url_server,indice);
                } else {
                                if (indice < tandas.length) {
                                        get_info_tanda_siguiente(tandas[indice+1]['nombre'], url_server,indice)
                                        get_info_tandas(tandas[indice]['nombre'], url_server,indice);
                                }
                }
                indice += 1;
        },
        error: function(e){
                loader.fadeOut();
                var html = '<br><br><div class="row"><div class="col-md-5 col-md-offset-3"><div id="error-cargar-tanda" class="alert alert-danger">';
                html += '<span class="glyphicon glyphicon-warning-sign"></span> Hubo un ERROR al cargar las tandas de este medio.</div></div></div>';
                $('#container').html(html);
        }
    });
};

/* ---------- TANDA SIGUIENTE -------------------------------*/
/* Busca la información de una tanda y llama a procesar_tanda*/
function get_info_tanda_siguiente(tanda, server,index) {
        $.ajax({
        url: server + "/API/get_info_tanda.php",
        data: {'filename': tanda },
        dataType: 'json',
        success: function(respuesta) {
                procesar_tanda_siguiente(respuesta['info'], tanda, server);
        }
        });
};

function procesar_tanda_siguiente(tanda, filename, server) {
        var acumulador = 0;

        $.each(tanda, function( index, value ) {
                var marca = value['marca'];
                var duracion = parseInt(value['duracion']);
                var hasta = acumulador + duracion;

                if (marca =='UNKNOWN' || marca =='SOFT_DELETED') {
                        url = server+'/API/get_thumbs.php?video_file='+filename+'&video_dir=/var/www/multimedia/ar/CapturaDVRs';
                        $.ajax({
                        url: url + '&seg_desde='+acumulador+'&seg_hasta='+hasta,
                        dataType: 'json',
                    });
                }
                acumulador += duracion;
        });
};

/* ---------- /TANDA SIGUIENTE -------------------------------*/

/* Busca la información de una tanda y llama a procesar_tanda*/
function get_info_tandas(tanda, server,index) {
        $.ajax({
                 complete: function()
        {
            loader.fadeOut();
        },
        url: server + "/API/get_info_tanda.php",
        data: {'filename': tanda },
        async: false,
        dataType: 'json',
        success: function(respuesta) {
                procesar_tanda(respuesta['info'], tanda, server)
        }
        });
};


/* Para cada pedazo de tanda se fija si es unk o no.
   Dependiendo el valor muestra los thumbs para cortar o
   solo marca y producto */
function procesar_tanda(tanda, filename, server) {
        var acumulador = parseFloat(tanda[0].inicio);
        var html = '';
        var header = false;

        var total = tanda.length;
        filename_global = filename;
        $.each(tanda, function( index, value ) {
                nro += 1;
                unk_tanda.tanda = filename;
                var marca = value['marca'];
                var producto = value['producto'];
                var duracion = parseInt(value['duracion']);
                var hasta = acumulador + duracion;

                if (index == 0) {
                        html += slide_tandas();
                        html +='<div class="row-offset-0">';
                        //html +='<span id="info-tanda" ></span><a id="ver-video-tanda"href="javascript:void(0)" data-toggle="tooltip" title="Ver video"><i class="fa fa-video-camera fa-lg"></i></a>';
                        html+= '</div>';
                        datos_de_tanda = datos_tanda(filename, hasta);
                }
                if (index < total -1) {
                        var siguiente = tanda[index+1]['marca'];
                }
                if (marca =='UNKNOWN' || marca =='SOFT_DELETED') {
                        var res = resultado(filename, acumulador, hasta, server);
                        if (res.indexOf('error') < 0) {
                                html += res;
                                error_thumbs = false;
                        } else {
                                error_thumbs = true;
                        }
                        header = true;
                        $('#container').append(html);
                        html = '';
                } else {
                        var inicio_reconocido = parseFloat(value['inicio'])
                        if (!error_thumbs && !no_tandas_pendientes) {
                        var id_publicidad = value['id_publicidad'];
                        var reconocidas_por_adtrack = $.parseJSON('{"inicio":"'+inicio_reconocido+'","patron_id":"'+id_publicidad+'"}');
                        unk_tanda.reconocidas_por_adtrack.push(reconocidas_por_adtrack);
                                if (header) {
                                        if (siguiente == 'UNKNOWN' || siguiente == 'SOFT_DELETED') {
                                                html += marca_producto(filename, acumulador, hasta, server,marca, producto,header,true,inicio_reconocido,nro);
                                                header = false;
                                        } else if (index+1 == tanda.length) {
                                                html += marca_producto(filename, acumulador, hasta, server,marca, producto,header,true,inicio_reconocido,nro);
                                                header = false;
                                        } else {
                                                html += marca_producto(filename, acumulador, hasta, server,marca, producto,header,false,inicio_reconocido,nro);
                                                header = false;
                                        }
                                } else {
                                        if (siguiente == 'UNKNOWN' || siguiente == 'SOFT_DELETED') {
                                                html += marca_producto(filename, acumulador, hasta, server,marca, producto,header,true,inicio_reconocido,nro);
                                        } else {
                                                html += marca_producto(filename, acumulador, hasta, server,marca, producto,header,false,inicio_reconocido,nro);
                                        }
                                }
                        }
                }
                acumulador += duracion;
        });


if (!error_thumbs  && !no_tandas_pendientes) {
                html += footer();
        } else if (error_thumbs) {
                html += '<br><br><div class="row"><div class="col-md-5 col-md-offset-3"><div id="error-cargar-tanda" class="alert alert-danger">';
                html += '<span class="glyphicon glyphicon-warning-sign"></span> Hubo un error al traer los thumbs:<br>';
                html += mensaje_error + '<br><a id="continuar-siguiente-tanda">Continuar con la siguiente tanda.</a></div></div></div>';
        }

        $('#container').append(html);
        //$('#info-tanda').html(datos_de_tanda)


        // ponerla como completa para que se vea verde
        colores_slide_tandas(filename);
        inicializar_slide_tandas();

        // Ponemos como "activo" el horario que esta
        var id_hora = datos_de_tanda.split('-')[1].split(':')[0];
        var fecha_info_tanda = $.trim(datos_de_tanda.split('-')[0]);
        var fecha_navegador_tanda = $('#fecha_elegida').text();
        if ( fecha_info_tanda == fecha_navegador_tanda ) {
                $('#'+ $.trim(id_hora)).children().addClass('hora-activa');
        }
        
        if ($('#detectadas').hasClass('active')) {
                mostrar_thumbs_reconocidos();
        } else {
                ocultar_thumbs_reconocidos();
        }
}

$(document).delegate("#continuar-siguiente-tanda", "click", function () {

        unk_tanda = {"tanda":"","reconocidas_por_adtrack":[],"no_reconocidas_por_adtrack":[],"nuevos_patrones":[]};
        loader.fadeIn();
        $(window).scrollTop(0);
        vaciar_resultados();
        mostrar();
        disminuir_pendientes();
});


function marca_producto(filename, acumulador, hasta, server,marca,producto,header,ultimo,inicio_reconocido,nro) {
    if (producto == 'N/A' || producto == null || producto == 'null') {
        producto = 'Sin Producto'
    }
    if (producto == 'Sin Producto')
        var marca_y_producto = marca;
    else
        var marca_y_producto = marca + " - " + producto;
        var html;
        html = '<div class="row row-offset-0 div-padre">';
        html += '<div class="col-md-11 marca-producto">';
        html += '<h5><a nro="'+nro+'" class="mostrar_reconocido eliminar_salida" title="Eliminar registro" marca="'+marca+'" producto="'+producto+'" file="'+filename+'" desde="'+acumulador+'" inicio_reconocido="'+inicio_reconocido+'" hasta="'+hasta+'" server="'+server+'"><span class="glyphicon glyphicon-minus"></span></a><a nro="'+nro+'" class="eliminar_patron" title="Eliminar patrón" desde="'+acumulador+'"><span class="glyphicon glyphicon-remove"></span></a><text class="marca_reconocida">  '+marca_y_producto+' </text></h5><div class="wrap-reconocido" id="div-'+acumulador+'"></div>';
        html += '</div>';
        html += '</div><br>';
        return html;
};


/* Función que busca los thumbs de un unk y arma el html */
var mensaje_error = ''
function resultado(video, desde, hasta, server) {

        // Thumbs
        var html = '<br><div class="row row-offset-0 div-padre">';

        // Llamada ajax que trae los thumbs de la tanda
        url = server+'/API/get_thumbs.php?video_file='+video+'&video_dir=/var/www/multimedia/ar/CapturaDVRs';
        $.ajax({
        url: url + '&seg_desde='+desde+'&seg_hasta='+hasta,
        async: false,
        dataType: 'json',
        success: function(data) {
                var errores = data['errores'];
                if (errores == '') {
                        thumbs = data['thumbs'];
                        html += '<br>';
                        $.each(thumbs, function( index, value ) {
                                        html += '<div id="i-g-'+index+'-'+desde+'" class="col-xs-1 div-thumb" >';
                                        html += '<a nro="'+nro+'" url="'+url+'" desde="'+desde+'" hasta="'+hasta+'"  id="g-'+index+'-'+desde+'" video="'+video+'""  class="thumbnail no-border grande" data-toggle="modal" href="#" server="'+server+'" >';
                                        html += '<img style="width:100px;" alt="img" src="http://'+value+'"></a>';
                                        html += '</div>';
                                });
                                html += '</div><br><!-- /.row -->';
                } else {
                        html = 'error';
                        mensaje_error = data['errores'];
                }
        },
    });
        return html;
};

function footer() {

        // Footer
        var html = '<br><div id="progress-flag-save" class="row row-offset-0 footer-guardar">';
        html += '<div class="col-md-2 col-md-offset-10">';
        html += '<button id="guardar" type="button" class="btn btn-success"><i class="fa fa-floppy-o"></i> Guardar</button>';
        html += '</div></div>';

        return html;
};


/* Llama a la api cortar_patron y le pasa los datos de todos los
patrones nuevos a cortar */
function cortar_patron() {
        $.ajax({
        url: servidor_actual + "/API/procesar_tanda.php",
        data: {'datos_tanda':unk_tanda},
        dataType: 'json',
        type: 'POST',
        success: function(respuesta) {
                console.log(respuesta);
                }
        });

        $.ajax({
        url: "/adtrack/set_estado_tanda/?tanda="+unk_tanda.tanda,
        dataType: 'json',
        type: 'GET',
        success: function(respuesta) {
                console.log(respuesta);
                }
        });

        // convertir esta tanda en un cuadrito verde

        $.each(tandas, function(){
                if (this.nombre == unk_tanda.tanda)
                        this.completa = true;

        });

        // Resetear la variable unk_tanda para la proxima tanda.
        unk_tanda = {"tanda":"","reconocidas_por_adtrack":[],"no_reconocidas_por_adtrack":[],"nuevos_patrones":[]};


}

function disminuir_pendientes() {
        actual = parseInt($('#tandas-pendientes').text().split(' ')[0]);
        nuevo = $('#tandas-pendientes').text(actual-1 + ' pendientes');
}


/*
        Eliminar salida
*/

$(document).delegate('.eliminar_salida', 'click', function() {

        // Asignamos al div padre el nro de publicidad
        var nro = $(this).attr('nro');
        $(this).parents( "div" ).attr('nro',nro);

        var desde = $(this).attr('desde');
        //if (confirm('¿Está seguro que desea eliminar la salida?')) {}

        // Ocultamos el texto con el nombre-marca y la opcion de eliminar patron
        $(this).parent().hide();

        // Sacamos el fondo gris
        $(this).parents('.marca-producto').removeClass('marca-producto');

        $.each(unk_tanda.reconocidas_por_adtrack, function( index, value ) {
                if (value.inicio == desde) {
                        unk_tanda.reconocidas_por_adtrack.splice(index,1);
                        return false;
                }
        });

        // Le agregamos la clase grande para que puedan ser marcados.
        $('.tdetectado[desde='+desde+']').addClass('grande');

});

/*
        Eliminar patrón.
*/
$(document).delegate('.eliminar_patron', 'click', function() {

        // Asignamos al div padre el nro de publicidad
        var nro = $(this).attr('nro');
        $(this).parents( "div" ).attr('nro',nro);       

        var desde = $(this).parent().children('.eliminar_salida').attr('inicio_reconocido');
        var server = $(this).parent().children('.eliminar_salida').attr('server');
        var tanda = $(this).parent().children('.eliminar_salida').attr('file');

        // Ocultamos el texto con el nombre-marca y la opcion de eliminar patron
        $(this).parent().hide();

        // Sacamos el fondo gris
        $(this).parents('.marca-producto').removeClass('marca-producto');


        $.each(unk_tanda.reconocidas_por_adtrack, function( index, value ) {
                if (value.inicio == desde) {
                        unk_tanda.reconocidas_por_adtrack.splice(index,1);
                        return false;
                } 
        });

        var desde_redondeado = $(this).attr('desde');

        // Le agregamos la clase grande para que puedan ser marcados.
        $('.tdetectado[desde='+desde_redondeado+']').addClass('grande');

        // Llamar a api para eliminar patron
        /*$.ajax({
        url: server + '/API/eliminar_patron.php',
        dataType: 'json',
        data: {'nombre_tanda':tanda,'inicio':desde },
        async: false,
        success: function(respuesta) {
                console.log("Patrón eliminado: ");
                console.log(respuesta);
        }
    });*/
        
         
});

function ingresarMarca(id_grande1,edicion) {

        if (edicion) {
                $('#tituloModalMarca').text('Editar Marca');
        } else {
                $('#tituloModalMarca').text('Asignar Marca');
        }

        $('.cerrarModalMarca').attr('grande1',id_grande1);

        $('#modalMarca').show();
        var html = '<input id="'+id_grande1+'" class="form-control input-marca" name="marca" placeholder="" autofocus required>';
        $('#marca').html(html);

        $(".input-marca").autocomplete({
            minLength: 3,
            source: '/app/get_marcas_autocomplete/',
            select: function( event, ui ) {
                $('.input-marca').val(decodeEntities(ui.item.value));
                $(this).attr('id_marca',ui.item.id);
                $(this).attr('nombre_marca',decodeEntities(ui.item.value));
                return false;
        },
        focus: function (e, ui) {
                    $('.input-marca').val(decodeEntities(ui.item.value));
                return false;
        }
        }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                if (typeof item.data === "undefined") {
                        var tipo =  '';
                } else {
                        var tipo = item.data;
                }
        return $( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + item.value + '<span style="float: right; color: #222;">' + tipo + "</span></a>" )
            .appendTo( ul ); 
    };
};


// Cuando asigna marca crear el objeto json de marca.
$('.asignarMarca').click( function() {
        var input = $('.input-marca');
        var id_grande1 = input.attr('id');

        if ($(this).attr('id') == 'propia') {
                var id_marca = 15836;
                var nombre_marca = 'Pauta Propia';
        } else {
                var id_marca = input.attr('id_marca');
                var nombre_marca = input.attr('nombre_marca');
        }
        
        $('#i-'+id_grande1).addClass('thumb');
        
        // Si ya tiene un caption es porque esta editando
        if($('#i-'+id_grande1).find('div.caption').length != 0) {
                $('#i-'+id_grande1).find('div.caption').remove();
        }
        if (nombre_marca.length > 32) {
        nombre_marca = nombre_marca.substring(0, 32) + "...";
    }
        var html = '<div class="caption"><p class="nombre-marca">'+nombre_marca+'</p>';
        html += '<p><a href="#" data-toggle="modal" class="label label-success editarMarca" rel="tooltip" title="Editar" grande1="'+id_grande1+'">Editar</a>';
        html += '<a href="#" data-toggle="modal" class="label label-danger eliminarCorte" rel="tooltip" title="Eliminar" grande1="'+id_grande1+'">Eliminar</a></p>';
        $('#i-'+id_grande1).append(html);

        $.each(unk_tanda["nuevos_patrones"], function(index,value) {
                if (id_grande1 == value.id_grande) {
                        value.id_marca = id_marca;
                        $('#i-' + id_grande1).nextUntil("#i-" + value.id_grande2).addClass("thumb");
                }
        });
        $('#modalMarca').hide();
        
});

$('#modalMarca').keypress(function(e) {
    if (e.which == '13') {
        $('#asignar').trigger( "click" );;
    }
});

$(document).delegate(".editarMarca", 'click', function() {
    var id_grande1 = $(this).attr('grande1');
    ingresarMarca(id_grande1,true);     
}); 

$(document).delegate(".cerrarModalMarca", 'click', function() {
        if ($('#tituloModalMarca').text() == 'Asignar Marca') {
                var id_grande1 = $(this).attr('grande1');
                $.each(unk_tanda["nuevos_patrones"], function(index,value) {
                        if (id_grande1 == value.id_grande) {
                                // Eliminar resaltados
                                var g1 = $('#i-' + id_grande1);
                                var g2 = $('#i-' + value.id_grande2);
                                g1.nextUntil("#i-" + value.id_grande2).find('a').removeClass("resaltado");
                                g1.find('a').removeClass("resaltado");
                                g2.find('a').removeClass("resaltado");
                                // Eliminar del json
                                unk_tanda.nuevos_patrones.splice(index,1);
                        }
                });
        } 
        $('#modalMarca').hide();
});

$(document).delegate(".eliminarCorte", 'click', function() {
    var id_grande1 = $(this).attr('grande1');
    $.each(unk_tanda["nuevos_patrones"], function(index,value) {
                if (id_grande1 == value.id_grande) {
                        // Eliminar resaltados
                        var g1 = $('#i-' + id_grande1);
                        var g2 = $('#i-' + value.id_grande2);

                        var nro_primero = '';
                        var nro_segundo = '';

                        var segundo_split = value.id_grande2.split('-');

                        if ($('#'+id_grande1).attr('nro')) {
                            nro_primero = $('#'+id_grande1).attr('nro');
                        } else {
                            nro_primero = $('#'+id_grande1).parents('.div-padre').attr('nro');
                    }

                        if ($('#'+value.id_grande2).attr('nro')) {
                            nro_segundo = $('#'+value.id_grande2).attr('nro');
                        } else {
                            nro_segundo = $('#'+value.id_grande2).parents('.div-padre').attr('nro');
                        }

                        if (segundo_split.length == 4) {
                            primero_publicidad = segundo_split[0] + '-0-' + segundo_split[2] + '-' + segundo_split[3];
                        } else {
                            primero_publicidad = segundo_split[0] + '-0-' + segundo_split[2]; 
                    }

                    if (nro_primero != nro_segundo) {
                            // Si nro_segundo - nro_primero = 1 es porque son consecutivas
                            // Recorremos desde el primer thumb del segundo hasta el ultimo
                            if (parseInt(nro_segundo) - parseInt(nro_primero) == 1 ) {
                              $("#i-"+ primero_publicidad).removeClass("resaltado");
                              $("#i-"+ primero_publicidad).nextUntil("#i-" + value.id_grande2).find('a').removeClass("resaltado");
                            } else {
                              // Si nro_segundo - nro_primero > 1 es porque pinto toda la del medio
                              // Pintar toda la del medio
                              // Recorrer desde el primer thumb del último hasta el ultimo thumb
                              var nro_medio = parseInt(nro_segundo) - 1;
                              $('div[nro="'+nro_medio+'"]:first').find('a').removeClass("resaltado");
                              $("#i-"+ primero_publicidad).removeClass("resaltado").css('color',color);
                              $("#i-"+ primero_publicidad).nextUntil("#i-" + value.id_grande2).find('a').removeClass("resaltado");
                            }
                        }
                        
                        console.log(nro_primero)
                        console.log(nro_segundo)
                        g1.removeClass('thumb');
                        g1.find('div.caption').remove();
                        g1.nextUntil("#i-" + value.id_grande2).find('a').removeClass("resaltado");
                        g1.find('a').removeClass("resaltado");
                        g2.find('a').removeClass("resaltado");
                        // Eliminar del json
                        unk_tanda.nuevos_patrones.splice(index,1);
                }
        });
}); 

// Menu contextual para los thumbs grandes
$(function(){
    $.contextMenu({
        selector: '.grande', 
        build: function($trigger, e) {
            return {
                callback: function(key, options) {
                    if (key == 'Ampliar') {
                        ampliar($trigger.attr('id'));
                    }; 
                },
                items: {
                    "Ampliar": {name: "Ampliar"},       
                }
            };
        }
    });
});




function datos_tanda(video,hasta) {

        var datos = video.split('-');
        var medio = datos[0];
        var fecha = datos[1];
        var fecha_span = (fecha.substr(0,2) + '/' + fecha.substr(2,2) + '/' + fecha.substr(4,5));
        var hora = datos[3].split('.')[0];
        var duracion = (hora.substr(0,2)+':'+hora.substr(2,3) + 'HS'  +' (' + Math.round((hasta/60) * 100) / 100 +' MIN)');

        var resultado = '<b>'+fecha_span + ' - ' + duracion +'<b>'+ '  ';

        // Seteamos la fecha_actual que usamos para la navegación de tandas
        fecha_actual = fecha_span;

        return resultado;
};

$(document).delegate("#ver-video-tanda", 'click', function() {
        var videoPlayer = videojs('videoTanda');
        var src = servidor_actual+'/multimedia/ar/CapturaDVRs/'+filename_global;
        $('#sourceVideo').attr('src',src);
        console.log(servidor_actual+'/multimedia/ar/CapturaDVRs/'+filename_global);
        $('#modalVideo').show();
        videoPlayer.play();
})

$(document).delegate(".cerrarModalVideo", 'click', function() {
        var videoPlayer = videojs('videoTanda');
        videoPlayer.pause();
        $('#modalVideo').hide();
});

// Abre un modal con el thumb seleccionado ampliado
function ampliar(id_grande) {
        var seg_thumb = parseInt(id_grande.split('-')[1]) * 2;
        var margen = parseInt(id_grande.split('-')[2]);
        var total_desde = seg_thumb + margen;
        url = servidor_actual+'/API/get_thumbs.php?video_file='+tandas[indice-1]+'&video_dir=/var/www/multimedia/ar/CapturaDVRs';
        $.ajax({
        url: url + '&seg_desde='+total_desde+'&seg_hasta='+total_desde+'&calidad=alta',
        dataType: 'json',
        success: function(data) {
                thumb = data['thumbs'];
                var html='';
                    html = '<div class="row row-offset-0">';
                    html += '<div class="col-md-12">';
                    html += '<img alt=""  src="http://'+thumb+'">';
                    html += '</div>';

                    $('#thumbAmpliado').html(html);
                    $('#modalAmpliar').modal('show');
        }
    });
};






/*
        Ver video, info de la tanda y ampliar thumb.
*/

/*function ver_video(id_grande) {
        var segundo_desde = (parseInt(id_grande.split('-')[1]) * 2) + parseInt(id_grande.split('-')[2]);
        
        var videoPlayer = videojs('videoTanda');
        var src = servidor_actual+'/multimedia/ar/CapturaDVRs/'+filename_global;
        $('#sourceVideo').attr('src',src);
        console.log(servidor_actual+'/multimedia/ar/CapturaDVRs/'+filename_global);
        $('#modalVideo').show();
        
};*/
