

$(document).delegate("#guardar", "click", function () {

        // Cargar los patrones existentes
        $('#modalPatronExistente').show();
        $('.marca_existente').focus();
        $('#modalPatronExistente').draggable();
        configurar_renglon($(".input-group"));
});


function configurar_renglon(form) {
    var producto_vacio = "<option>Elija un producto</option>";
                $(form).children(".producto_existente").html(producto_vacio);

                // Autocomplete marca
                $(form).children(".marca_existente").autocomplete({
                    minLength: 3,
                    source: '/app/get_marcas_autocomplete/',
                    select: function( event, ui ) {
                        $(this).val(decodeEntities(ui.item.value));
                        var marca_pk = ui.item.id;
                        $(this).attr('id_marca',ui.item.id);
                        $(this).attr('nombre_marca',decodeEntities(ui.item.value));
                        var marca = $(this);

                                $.ajax({
                                        url: '/adtrack/get_productos_marca/',
                                        dataType: 'json',
                                        data: {'id_marca':ui.item.id},
                                        async: false,
                                        success: function(respuesta) {
                                            var html = producto_vacio;
                                            $.each(respuesta, function(index, value){
                                                html += '<option id_producto="'+value.producto_id+'" nombre_producto="'+value.producto_nombre+'" >'+value.producto_nombre+'</option>';
                                                });
                                            marca.next().html(html);
                                            marca.next().focus();
                                        }
                                });
                },
                focus: function (e, ui) {
                            $('.input-marca-existente').val(decodeEntities(ui.item.value));
                        return false;
                }
                }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
                if (typeof item.data === "undefined") {
                                        var tipo =  '';
                                } else {
                                        var tipo = item.data;
                                }
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a>" + item.value + '<span style="float: right; color: #222;">' + tipo + "</span></a>" )
                    .appendTo( ul ); 
            };
            $(form).children(".marca_existente").focus();



    // Si toca enter 

    $('.marca_existente').keypress(function(e) {
        if (e.which == '13') {
           e.preventDefault();
        }
        
    });


    $('.producto_existente').keypress(function(e) {
        if (e.which == '13') {
            e.preventDefault();
           if (check_form_existentes($(this).closest(".input-group"))) { 
                enter_in_producto(this);
           };
        }
    });


};





$(document).delegate(".producto_existente", 'change', function() {
        var id_producto = $("option:selected",this).attr("id_producto");
        var nombre_producto = $("option:selected",this).attr("nombre_producto");
        $(this).attr("id_producto",id_producto);
        $(this).attr("nombre_producto",nombre_producto);
});



function check_form_existentes(form) {
        incomplete = false;
       // Vemos si hay marca sin producto asignado, salta la bandera.
       $.each($('.input-group'), function(index,value) {        
          var marca = $(value).children(".marca_existente").val();
          if (marca == '') {
             $(value).focus();
             incomplete = true;
          } 
          var prod = $(value).children(".producto_existente").val();
          if (prod == '' || prod == 'Elija un producto') {
             $(value).focus();
             incomplete = true;
          } 
       });
       return !incomplete;

};

function enter_in_producto(producto){
    console.log(producto);
    $(producto).next().children(".btn-add").trigger("click");
    
//           $('.btn-add').last().trigger( "click" );
};

// Agrega renglones para el modal de patrones existentes
$( document ).on( 'click', '.btn-add', function ( event ) {
        event.preventDefault();
        var field = $(this).closest( '.form-group' );

        if (check_form_existentes(field)) {
                
                var field_new = field.clone();

                configurar_renglon(field_new);
                
                $(this).toggleClass( 'btn-default' )
                        .toggleClass( 'btn-add' )
                        .toggleClass( 'btn-danger' )
                        .toggleClass( 'btn-remove' )
                        .html( '–' );

                field_new.find( 'input' ).val( '' );
                field_new.insertAfter( field );

                // Para que el renglón nuevo no arranque con los datos del anterior
                field_new.children('select').html('<option>Elija un producto</option>');
                field_new.children('.marca_existente').focus();
        }
} );



$(document).delegate(".cerrarModalPatronExistente", 'click', function() {
        $('#modalPatronExistente').hide();
});


$( document ).on( 'click', '.btn-remove', function ( event ) {
        event.preventDefault();
        $(this).closest( '.form-group' ).remove();
} );


// Click en el botón aceptar del modal de los patrones existentes
$(document).delegate("#asignarPatronExistente", 'click', function() { 

        // TODO refactorizar este control a una función ya que se 
        // repite 2 veces mas en el modal para el enter y boton de +
        var b = 0;
        // Vemos si hay marca sin producto asignado, salta la bandera.
        $.each($('.producto_existente'), function(index,value) {
                if ($(value).val() == 'Elija un producto') {
                        b = 1;
        } 
        });
        // Si hay un solo producto y vacio dejo pasar
        if (($('.producto_existente').length) == 1) {
                b = 0
        }
        if (b == 1) {
                alert('Elija primero el producto');
        } else {        
                b = 0;

                $('#modalPatronExistente').hide();
                
                var marcas_existentes = [];
                var productos_existentes = [];
                var id_patron_existentes = [];


                // Guardamos las marcas en un array
                $.each($('.marca_existente'), function(index,value) {
                        //marcas_existentes.push($(this).attr('id_marca'));
                        marcas_existentes.push($(this).val());
                });

                $.each($('.producto_existente'), function(index,value) {
                        //marcas_existentes.push($(this).attr('id_marca'));
                        productos_existentes.push($(this).val());
                });

                // Guardamos los productos en un array y buscamos el id del patron
                /*$.each($('.producto_existente'), function(index,value) {
                        var id_producto = $(this).attr('id_producto');
                        if (id_producto) {
                                productos_existentes.push($(this).attr('id_producto'));
                                $.getJSON("/adtrack/get_duracion_id_patron_producto/", {'id_producto':id_producto,'server':servidor_actual}, function(data){
                                        id_patron_existentes.push(data['id_patron']);
                            });
                        }
                });*/
                
                // Guardamos los datos del patron en el json
                $.each(marcas_existentes, function(index,value) {
                        var patron = id_patron_existentes[index];
                        if (marcas_existentes[index] != '') {
                                var unk = $.parseJSON('{"marca":"'+marcas_existentes[index]+'","producto":"'+productos_existentes[index]+'"}');
                                unk_tanda.no_reconocidas_por_adtrack.push(unk);
                        } 
                });

                // Mandar datos a la api cortar_patron
                cortar_patron();
         
                // Eliminamos todos los cargados en el modal de patrones no reconocidos
                $.each($('.marca_existente'), function(index,value) {
                        if (index > 0) {
                                $(value).closest( '.form-group' ).remove();
                        } else {
                                $('.marca_existente').val('');
                                $('.producto_existente').val('Elija un producto');
                                if ($('.marca_existente').length > 1) {
                                        $('.btn-mas-renglon').toggleClass( 'btn-danger' )
                                                .toggleClass( 'btn-remove' ).toggleClass( 'btn-default' )
                                                .toggleClass( 'btn-add' )
                                                .html( '+' );
                                        }
                                }
                                
                });
                
                loader.fadeIn();
                $(window).scrollTop(0);
                vaciar_resultados();
                mostrar();
                disminuir_pendientes();
        }
});
