        function crear_calendario(fecha_desde,fecha_hasta,calendario_id){
		        $(calendario_id).DatePicker({
			        flat: true,
			        date: [fecha_desde,fecha_hasta],
			        current: fecha_desde,
			        calendars: 1,
			        format:'d/m/Y',
//			        mode: 'range',
			        starts: 1,
		            onChange: function(formated) {
                     		$("#visoractual").val(formated[0]);
		                }
		                
		        });
        }
