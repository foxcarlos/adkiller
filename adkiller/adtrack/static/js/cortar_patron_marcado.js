/*
	Funciones que se encargan del marcado de un patrón.
*/


$(document).delegate(".grande", "click", function (e) { 

	var mostrar_alert = false;
	grande = $(this).attr('id');

	// Datos del video
	var url = $(this).attr('url');

	// Segundo donde empieza el UNK
	var desde_total = parseInt($(this).attr('desde'));

	// Segundo desde y hasta del corte
	var inicio = parseInt((grande.split('-')[1])) * 2;
	var fin = inicio + 2;

	// Segundo desde y hasta de este thumb
	desde_thumbs_chicos = desde_total + inicio;
	hasta_thumbs_chicos = desde_total + fin;

	// Vemos si es el primero o segundo
	if (grande1 == "") {
	  grande1 = grande;
	  desde1 = $(this).attr('desde');
	  $('#tituloThumbsChicos').text('Seleccione la imagen de inicio');
	  mostrar_alert = true;
	} else {
	  desde2 = $(this).attr('desde');
	  /* Controlamos que no vaya para atrás
	     Desde1 y Desde2 lo usamos para permitir que valla desde una publicidad hacia la otra
	     Si sacamos el AND nos daría error de que no se puede asignar thumb anterior ya que para
	     Saber si es anterior o no nos basamos en el indice del thumb dentro de la publicidad.*/
	  if (parseInt(grande.split('-')[1]) < parseInt(grande1.split('-')[1]) && (desde1 == desde2) )  {
	  	$('#myModalError').show();
	  } else {
	  	mostrar_alert = true;
	    grande2 = grande;
	    $('#tituloThumbsChicos').text('Seleccione la imagen de fin');
	  }
	}

	if (mostrar_alert) {
		// Si es el primero, mostramos los anteriores
		if (grande2 == '' && desde_thumbs_chicos != 0) {
			/* Modal con los thumbs chicos*/
		    $.ajax({
		    	url: url + '&seg_desde='+(desde_thumbs_chicos-2)+'&seg_hasta='+(hasta_thumbs_chicos-2)+'&dec_seg_separac=1&calidad=muy_baja',
		      	async: false,
		      	dataType: 'json',
		      	success: function(data) {
		        	thumbs_chicos = data['thumbs'];
		      	}
		    });
		} else {
			/* Modal con los thumbs chicos*/
		    $.ajax({
		    	url: url + '&seg_desde='+desde_thumbs_chicos+'&seg_hasta='+hasta_thumbs_chicos+'&dec_seg_separac=1',
		      	async: false,
		      	dataType: 'json',
		      	success: function(data) {
		        	thumbs_chicos = data['thumbs'];
		      	}
		    });
		}
	    html='';
	    html = '<div class="row row-offset-0">';
	    $.each(thumbs_chicos, function( index, value ) {
	      html += '<div class="col-xs-2 div-thumb-chico">';
	      html += '<a id="c'+index+'" indice="'+index+'" desde_tc="'+desde_thumbs_chicos+'" hasta_tc="'+hasta_thumbs_chicos+'" class="thumbnail chico"><img width="110" alt=""  src="http://'+value+'"></a></div>';
	    });
	    html += '</div>';

	    $('#body-chicos').html(html);
	    $('#myModal2').modal('show');
	}
  });

$('.cerrarModalError').click(function () {
	$('#myModalError').hide();
});

$(document).delegate(".chico", "click", function (e) { 
    e.preventDefault();
    chico = $(this).attr('id'); 

    // Vemos si es el primero o segundo
    if (chico1 == "") {
      chico1 = chico;
      // Guardamos el indice del thumb chico 1
      indice_chico1 = $(this).attr('indice'); 
      // Guardamos el desde global del chico 1
      desde_chico1 = $(this).attr('desde_tc');
      color = randomColor();
      resaltarBordes(grande1, color);
    } else {
      chico2 = chico;
      resaltarBordes(grande2, color);
      resaltarIntermedios(grande1, grande2,color);
    }

    $('#myModal2').modal('hide');

    if (chico2 != "") {

    	ingresarMarca(grande1, false);
    	// Datos del video que sacamos del thumb grande 1
      	var tg = $('#'+grande1);
      	var video = tg.attr('video');
      	var server = tg.attr('server');
      	var video_dir = '/var/www/multimedia/ar/CapturaDVRs'

      	// Datos de los segundos desde donde cortar y la duración de hasta donde cortar
      	var thumb_chico_desde = $('#'+chico1);
      	var thumb_chico_hasta = $('#'+chico2);
      	var desde_global = desde_chico1; // Desde del thumb grande.
      	var hasta_global = thumb_chico_hasta.attr('desde_tc'); // Desde del thumb grande
      	var desde_decima = indice_chico1; // Indice del thumb chico que corresponde a las decimas de segundo.
      	var hasta_decima = thumb_chico_hasta.attr('indice'); // Indice del thumb chico que corresponde a las decimas de segundo.

      	// Calculamos el total desde
        /*
      	if (desde_decima < 10) {
      		var desde_total = parseFloat(desde_global) + (parseFloat(desde_decima) / 10);
      	} else if (desde_decima == 10) {
		var desde_total = parseInt(desde_global) + 1;
      	} else {
      		var desde_total = parseFloat(parseInt(desde_global)+1) + (parseFloat(desde_decima - 10) / 10);
      	}
        */
        var desde_total = parseFloat(desde_global) + (parseFloat(desde_decima) / 10)
      	// Resto 2 segundos al desde ya que mostramos los 2 segundos anteriores.
        if(desde_global > 0) {
            desde_total = desde_total - 2;
        }
        
        console.log("desde_global:" + desde_global);
        console.log("desde_decima:" + desde_decima);
        console.log("desde_total (después de restar 2 segundos):" + desde_total);
        

      	// Calculamos el total hasta
        /*
	if (hasta_decima < 10) {
      		var hasta_total = parseFloat(hasta_global) + (parseFloat(hasta_decima) / 10);
      	} else if (hasta_decima == 10) {
		var hasta_total = parseInt(hasta_global) + 1;
      	} else {
      		var hasta_total = parseFloat(parseInt(hasta_global)+1) + (parseFloat(hasta_decima - 10) / 10);
      	} 
        */
        var hasta_total = parseFloat(hasta_global) + (parseFloat(hasta_decima) / 10);
        
        console.log("hasta_global:" + hasta_global);
        console.log("hasta_decima:" + hasta_decima);
        console.log("hasta_total:" + hasta_total);
       

      	var nuevos_patrones = $.parseJSON('{"id_grande":"'+grande1+'","id_grande2":"'+grande2+'","video":"'+video+'","server":"'+server+'","video_dir":"'+video_dir+'","inicio":"'+desde_total+'","fin":"'+hasta_total+'","id_marca":""}');

    	unk_tanda.nuevos_patrones.push(nuevos_patrones);
       	grande1 = "";
      	grande2 = "";
      	chico1 = "";
      	chico2 = "";
    }
});

/* Cuando se cierra el modal con el boton cerrar
 o con la cruz, se deben volver a inicializar las
 variables */
$('.cerrarModal').on('click', function (e) {
	if (grande2 != '') {
	  grande2 = '';
	  chico2 = '';
	} else {
	  grande1 = '';
	  chico1 = '';
	}
});
