/*
        - Filtro por medio (combo box medios).
        - Mostrar o no thumbs en publicidades detectadas.
        - Navegación de tandas por fecha.
*/


/*
        Filtro por medio
*/

// Combo medios
$(document).ready(function () {
    MINUTOS_POR_PIXEL = 3;
    var fecha = getUrlVars()['fecha'];
    if (fecha) {
        crear_calendario(fecha, fecha, '#elegir_fecha');
        $("#fecha_elegida").html(fecha);
    };
    
    var medio = getUrlVars()['medio'];
    $.ajax({
        type: "GET",
        url: "/adtrack/get_medios/",
        async: false,
        dataType: "json",
        success: function(respuesta) {
                        var html = '';
                        $.each(respuesta, function(index, value){
                            if(value[0] == medio) 
                                selected = 'selected=selected';
                            else
                                selected = ''
                            html += '<option class="medio-opcion" ' + selected + ' idmedio="'+value[1]+'">'+value[0]+'</option>';
                        });
                        $('.combo-medios').append(html);
            indice = 0;
                    vaciar_resultados();
            mostrar();
                },
    });
});

$(document).delegate(".combo-medios", 'change', function() {
        window.location.search = "?medio=" + $(this).val().toString()  ;
});


/*  
        Mostrar o no los thumbs de las publicidades detectadas
        al clickear en navbar.
*/
$('#detectadas').click(function() {
        if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                mostrar_thumbs_reconocidos();
        } else {
                $(this).removeClass('active');
                ocultar_thumbs_reconocidos();
        }
});

function ocultar_thumbs_reconocidos() {

        var detectados = $('.bloque-detectado');
        $.each(detectados, function( index, value ) { 
                $(this).hide();
        });
};

function mostrar_thumbs_reconocidos() {

        var reconocidos = $('.mostrar_reconocido');
        $.each(reconocidos, function( index, value ) {

                var filename = $(this).attr('file');
                var desde = $(this).attr('desde');
                var hasta = $(this).attr('hasta');
                var server = $(this).attr('server');
                var marca = $(this).attr('marca');
                var producto = $(this).attr('producto');
                var url = server+'/API/get_thumbs.php?video_file='+filename+'&video_dir=/var/www/multimedia/ar/CapturaDVRs';

                var html = '<br><div class="row row-offset-0 bloque-detectado">';

                $.ajax({
                        url: url + '&seg_desde='+desde+'&seg_hasta='+hasta,
                        async: false,
                        dataType: 'json',
                        success: function(data) {
                                thumbs_detectados = data['thumbs'];
                                html += '<br>';
                                $.each(thumbs_detectados, function( index, value ) { 
                                        html += '<div id="i-g-'+index+'-'+desde+'-detectado" class="col-xs-1 div-thumb" >';
                                        html += '<a url="'+url+'" desde="'+desde+'" hasta="'+hasta+'"  id="g-'+index+'-'+desde+'-detectado" video="'+filename+'"  class="tdetectado thumbnail" data-toggle="modal" href="#" server="'+server+'" >';
                                        html += '<img style="width:150px;" alt="img" src="http://'+value+'"></a>';
                                        html += '</div>';
                                });
                                html += '</div><br><!-- /.row -->';
                        }
                });
                $('#div-'+desde).html(html);
    });
};


/*
        Navegación de tandas por fecha.
*/

                
// Devuelve un array de objectos con fecha y hora de cada tanda
function fecha_hora_tandas(tandas) {
        var datos_slider = []
        $.each(tandas, function(index, value){
                var datos_tanda = value.nombre.split('-');
                var fecha_raw = datos_tanda[1]
                var hora_raw = datos_tanda[3].split('.')[0];
                var hora = hora_raw.substr(0,2) + ':' + hora_raw.substr(2,2);
                var fecha = fecha_raw.substr(0,2) + '/' +fecha_raw.substr(2,2) + '/' +fecha_raw.substr(4,2)
                var fecha_hora = {"indice":index,"tanda":value,"fecha":fecha,"hora":hora};
                datos_slider.push(fecha_hora)
        });
        return datos_slider
}

function inicializar_slide_tandas() {
        $('#fecha_elegida').text(fecha_actual);
};


function slide_tandas() {

    // Flechas y thumbs tandas
    html = ''; 

    // Flechas
    html += '<div style="float:left;"><a href="#" id="diaAnterior" data-toggle="tooltip" title="Fecha anterior" >';
    html += '<span style="font-size: 2em; padding-top:25px; padding-right:1em;" class="glyphicon glyphicon-chevron-left"></span></a></div>';


    
    for (var hora=0; hora <24; hora+=1) {
        html += '<div class="hora"><div class="numero_hora">' + hora.toString() + '</div>';
        // Cada minuto es un cuadradito diminuto
        for(var minuto = 0; minuto < 60; minuto+=MINUTOS_POR_PIXEL)
        {
            html += '<a class="minuto" id="'+(minuto+hora*60).toString()+'"><div class="square grey"></div></a>';
        }
        html += '</div>';
    }
    
    html += '<div class="hora">24</div>';

    html += '<div><a href="#" id="diaSiguiente" data-toggle="tooltip" title="Fecha siguiente" >';
    html += '<span style="font-size: 2em; padding-top:25px;" class="glyphicon glyphicon-chevron-right"></span></a></div>';

    html += '</br>';

    return html;
};

/*
        Asigna el color correspondiente al nav de tandas
*/
function colores_slide_tandas(tanda_actual) {
        /* Ordena primero las completa asi si en una hora hay mas de dos
           tandas y una está incompleta pone la hora como incompleta.
        */
                $.each($(".minuto"), function( ) {
                        $(this).children().removeClass('red green').addClass('grey');
                });

        /* Armamos un string con el formato ddmmaa para comparar la fecha
       actual con la del array de tandas */
        var copy_fecha_actual = fecha_actual;
        var fecha_filtro = copy_fecha_actual.replace("/","");
        fecha_filtro = fecha_filtro.replace("/","");

        // Filtramos las tandas por el dia actual del navegador de fechas
        var  copy_tandas = tandas;
        tandas_del_dia = jQuery.grep(copy_tandas, function( n, i ) {
          return (n['nombre'].split('-')[1] == fecha_filtro );
        });

        $.each(tandas_del_dia, function(){
                for (var min=0; min < this.duracion; min += MINUTOS_POR_PIXEL) {
                        var inicio = (this.minuto_inicio / MINUTOS_POR_PIXEL >> 0) * MINUTOS_POR_PIXEL;
            var div = $(".minuto#" + (inicio + min).toString()).children();
                        div.removeClass('grey green red');
                        if (this.completa){
                                div.addClass('green');
                        }
                        else if (this.nombre == tanda_actual)
                        {
                            div.addClass('orange');
                        }
                        else
                        {
                            div.addClass('red');
                        }
                        $(".minuto#" + (inicio + min).toString()).attr('title', this.nombre)
                        $(".minuto#" + (inicio + min).toString()).attr('tanda', this.nombre)
                };
        });


        /*var horas_con_info = [];
        $.each(ordenadasPorCompleta, function( index, value ) {
                if (value.fecha == fecha_actual && value.completa)  {
                        horas_con_info.push(value.hora);
                        $('#'+value.hora).children().removeClass('grey red').addClass('green');
                } else if (value.fecha == fecha_actual && !value.completa) {
                        horas_con_info.push(value.hora);
                        $('#'+value.hora).children().removeClass('grey green').addClass('red');
                }
        });*/ 
}

function navegar_dias_tandas(direccion) {
    var medio =getUrlVars()['medio'];
    var fecha =getUrlVars()['fecha'];
    window.location.search =  "?medio=" + medio + "&fecha=" + fecha + "&cambiar_fecha=" + direccion;
    return false;
    
        if (direccion < 0){
                var fechayhora = fecha_hora_tandas(tandas).reverse();
        } else {
                var fechayhora = fecha_hora_tandas(tandas);
        }
        
        var medio = getUrlVars()["medio"];
        
        $.each(fechayhora, function( index, value ) {
                var fecha = value.fecha.split('/');
                if (direccion < 0) {
                        if (fecha_actual > value.fecha) {
                                fecha_actual = value.fecha;
                                return false;
                        }
                } else {
                        if (fecha_actual < value.fecha) {
                                fecha_actual = value.fecha;
                                return false;
                                
                        }
                }
        });


        //colores_slide_tandas(true);

}

$(document).delegate("#diaAnterior", 'click', function() {
        navegar_dias_tandas(-1);
        
});

$(document).delegate("#diaSiguiente", 'click', function() {
        navegar_dias_tandas(1);
});

$(document).delegate(".minuto", 'click', function() {
        var minuto = $(this).attr('id');
        loader.fadeIn();
        vaciar_resultados();
        get_info_tandas($(this).attr('tanda'), servidor_actual,0);
        return false
});

function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,
    function(m,key,value) {
        vars[key] = value.replace("#", "");
        vars[key] = replaceAll("%20", " ", vars[key]);
    });
    return vars;
}
