# -*- coding: utf-8 -*-

# Django Imports
from django.db import models


class Tanda(models.Model):
    nombre = models.CharField(max_length=255, unique=True)
    fecha = models.DateField(db_index=True, null=True, blank=True)
    hora = models.TimeField(null=True, blank=True)
    completa = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s" % self.nombre

    class Meta:
        verbose_name_plural = "Tandas"

