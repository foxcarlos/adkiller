<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Subida de nuevo video</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="/static/css/customAT.css">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Carga de nuevo video</h1>
        <hr id="linea">
        <form action="" role="form" id="formSubeVideo">
          <label for="marca">Marca</label>
          <input type="text" class="form-control inputVideo" id="marca" disabled>
          <label for="nombre">Producto</label>
          <input type="text" class="form-control inputVideo" id="nombre" disabled>
          <label for="tipo">Tipo de publicidad</label>
          <input type="text" class="form-control inputVideo" id="tipo" disabled>
          <label for="observaciones">Observaciones</label>
          <input type="text" class="form-control inputVideo" id="observaciones" disabled>
          <label for="etiquetas">Etiquetas</label>
          <input type="text" class="form-control inputVideo" id="etiquetas" disabled>
          <input type="file" class="btn btn-info btn-lg btn-block" id="btnSearchFile">
          <button type="button" class="btn btn-primary btn-lg btn-block" id="btnSubir">
            <span class="glyphicon glyphicon-open"></span> Subir
          </button>
        </form>
      </div>
    </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  <script>
    (function($) {  
      $.get = function(key)   {  
          key = key.replace(/[\[]/, '\\[');  
          key = key.replace(/[\]]/, '\\]');  
          var pattern = "[\\?&]" + key + "=([^&#]*)";  
          var regex = new RegExp(pattern);  
          var url = unescape(window.location.href);  
          var results = regex.exec(url);  
          if (results === null) {  
              return null;  
          } else {  
              return results[1];  
          }
      }  
  })(jQuery); 

          var nombre = $.get("nombre"); 
          var marca = $.get("marca");
          var tipo = $.get("tipo");
          var observaciones = $.get("observaciones");
          var etiquetas = $.get("etiquetas");
          $('#nombre').val(nombre);
          $('#marca').val(marca);
          $('#tipo').val(tipo);
          $('#observaciones').val(observaciones);
          $('#etiquetas').val(etiquetas);
  </script>
</body>
</html>