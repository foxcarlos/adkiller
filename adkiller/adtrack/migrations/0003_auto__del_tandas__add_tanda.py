# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Tandas'
        db.delete_table('adtrack_tandas')

        # Adding model 'Tanda'
        db.create_table('adtrack_tanda', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('completa', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('adtrack', ['Tanda'])


    def backwards(self, orm):
        # Adding model 'Tandas'
        db.create_table('adtrack_tandas', (
            ('completa', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=255, unique=True)),
        ))
        db.send_create_signal('adtrack', ['Tandas'])

        # Deleting model 'Tanda'
        db.delete_table('adtrack_tanda')


    models = {
        'adtrack.tanda': {
            'Meta': {'object_name': 'Tanda'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['adtrack']