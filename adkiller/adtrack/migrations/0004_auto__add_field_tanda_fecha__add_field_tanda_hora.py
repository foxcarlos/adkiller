# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Tanda.fecha'
        db.add_column('adtrack_tanda', 'fecha',
                      self.gf('django.db.models.fields.DateField')(db_index=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Tanda.hora'
        db.add_column('adtrack_tanda', 'hora',
                      self.gf('django.db.models.fields.TimeField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Tanda.fecha'
        db.delete_column('adtrack_tanda', 'fecha')

        # Deleting field 'Tanda.hora'
        db.delete_column('adtrack_tanda', 'hora')


    models = {
        'adtrack.tanda': {
            'Meta': {'object_name': 'Tanda'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'hora': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['adtrack']