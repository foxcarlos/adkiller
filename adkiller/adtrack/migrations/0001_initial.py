# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'EtiquetaProducto'
        db.create_table('adtrack_etiquetaproducto', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('adtrack', ['EtiquetaProducto'])


    def backwards(self, orm):
        # Deleting model 'EtiquetaProducto'
        db.delete_table('adtrack_etiquetaproducto')


    models = {
        'adtrack.etiquetaproducto': {
            'Meta': {'object_name': 'EtiquetaProducto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['adtrack']