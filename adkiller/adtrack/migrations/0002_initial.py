# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Tandas'
        db.create_table('adtrack_tandas', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('completa', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('adtrack', ['Tandas'])


    def backwards(self, orm):
        # Deleting model 'Tandas'
        db.delete_table('adtrack_tandas')


    models = {
        'adtrack.tandas': {
            'Meta': {'object_name': 'Tandas'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['adtrack']