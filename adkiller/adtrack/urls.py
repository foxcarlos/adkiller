# -*- coding: utf-8 -*-
"""
Urls for app adtrack
"""

# django imports
from django.conf.urls import patterns, url
from django.contrib.admin.views.decorators import staff_member_required
from django.views.generic.base import RedirectView
from django.views.generic.simple import direct_to_template

# Project Imports
from adkiller.adtrack.views import borrar_patron
from adkiller.adtrack.views import check_tag_exists
from adkiller.adtrack.views import cortar_patron
from adkiller.adtrack.views import get_duracion_id_patron_producto
from adkiller.adtrack.views import get_estado_tanda
from adkiller.adtrack.views import get_medios
from adkiller.adtrack.views import get_productos_marca
from adkiller.adtrack.views import get_productos_sin_categorizar
from adkiller.adtrack.views import get_server_url
from adkiller.adtrack.views import get_tipos_de_publicidad
from adkiller.adtrack.views import lookup_filtro_cargar_producto
from adkiller.adtrack.views import set_estado_tanda
from adkiller.adtrack.views import sube_video
from adkiller.adtrack.views import validar_anunciante


urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url="/adtrack/cortar_patron/")),
    url(r'^cargar_producto/',
        staff_member_required(direct_to_template),
        {'template': 'cargar_producto.html'}),
    url(r'^sube_video/', sube_video),
    url(r'^cortar_patron/', cortar_patron),
    url(r'^get_productos_sin_categorizar/', get_productos_sin_categorizar),
    url(r'^get_duracion_id_patron_producto/', get_duracion_id_patron_producto),
    url(r'^get_tipos_de_publicidad/', get_tipos_de_publicidad),
    url(r'^get_server_url/', get_server_url),
    url(r'^get_medios/', get_medios),
    url(r'^get_estado_tanda/', get_estado_tanda),
    url(r'^set_estado_tanda/', set_estado_tanda),
    url(r'^get_productos_marca/', get_productos_marca),
    url(r'^check_tag_exists/', check_tag_exists),
    url(r'^validar_anunciante/', validar_anunciante),
    url(r'^lookup_filtro_cargar_producto/', lookup_filtro_cargar_producto),
    url(r'^borrar_patron/', borrar_patron),
)
