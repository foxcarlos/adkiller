# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long
"""
Analizar chunks automáticamente usando
el motor de detección visual (brandtrack)
"""

# python imports
from datetime import datetime

# django imports
from django.core.management.base import BaseCommand

# project imports
from adkiller.media_map.models import Evento
from adkiller.engines_cache.models import Chunk

class Command(BaseCommand):
    help = u"""Analizar chunks!"""

    def handle(self, *args, **options):
        print str(datetime.now())
        try:
            # atención: si me pasan un id
            # NO CHEQUEO SI EL ENTRENAMIENTO DE LAS MARCAS ESTÁ FINALIZADO
            # analizo sí o sí
            pk = int(args[0])
            chequear_marcas = False
        except (ValueError, IndexError):
            eventos_pendientes = Evento.objects.filter(status=Evento.PENDING)
            chequear_marcas = True
        else:
            eventos_pendientes = Evento.objects.filter(pk=pk, status=Evento.PENDING)

        if not eventos_pendientes:
            print 'No encontré eventos'
        for e in eventos_pendientes:
            print
            print 'Medio:', e.medio
            print 'Desde:', e.desde
            print 'Hasta:', e.hasta

            marcas_aprobadas = True

            for chunk in e.chunks.filter(status=Chunk.PENDING):

                # este control lo realizo una vez por chunk por si cambia
                # el estado de la marca entre un chunk y el otro
                if chequear_marcas:
                    for marca in e.marcas.all():
                        if not marca.brandtrack_entrenado:
                            print 'Entrenamiento de brandtrack no aprobado'
                            print 'para', marca.nombre
                            print 'no analizo este evento...'
                            marcas_aprobadas = False
                            break
                if not marcas_aprobadas:
                    break

                # obtener de la base de datos, nuevamente, para refrescar su status
                chunk = Chunk.objects.get(pk=chunk.id)
                if chunk.status == Chunk.PENDING:
                    print 'Analizando chunk', chunk.id
                    print '{}% - {:%H:%M:%S}'.format(e.porcentaje_avance(),
                                                    datetime.now())
                    self.stdout.flush()
                    chunk.analize()

            pendientes = e.chunks.filter(status=Chunk.PENDING).count()
            lockeados = e.chunks.filter(status=Chunk.LOCKED).count()
            if not pendientes and not lockeados:
                e.status = Evento.DONE
                e.cerrado = datetime.now()
                e.save()
