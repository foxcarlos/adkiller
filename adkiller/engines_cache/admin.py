# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, super-on-old-class
"""
Admin para app cache
"""
from django.contrib import admin
from adkiller.engines_cache.models import Chunk

class ChunkAdmin(admin.ModelAdmin):


    list_display = ['__unicode__', 'evento_original',
                    'medio', 'desde', 'hasta', 'status']
    list_filter = ['medio', 'status']
    date_hierarchy = 'desde'
    search_fields = ['medio__nombre', 'evento_original__marcas__nombre']
    fields = ()


    def has_add_permission(self, request):
        """No queremos que se agreguen chunks"""
        return False


admin.site.register(Chunk, ChunkAdmin)
