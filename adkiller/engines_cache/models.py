# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, too-many-branches, too-many-locals
# pylint: disable=too-many-statements
#
"""
Models para app engines_cache
"""

from datetime import timedelta
from collections import defaultdict
import requests
import numpy as np

from django.db import models
from django.conf import settings

from adkiller.app.models import Medio
from adkiller.media_map.models import BrandModel
from adkiller.media_map.models import BoundingBox
from adkiller.media_map.models import Evento
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento
from adkiller.media_map.utils import DateRange
from adkiller.media_map.utils import eliminar_detecciones_aisladas
from adkiller.media_map.utils import get_thumb_url
from adkiller.media_map.utils import pairwise
from adkiller.media_map.utils import scores_to_matches_index

CHUNK_MAX_MINUTES = 1

class LockedChunkException(Exception):
    """Lanzada cuando se intenta analizar un chunk que está lockeado"""

class DoneChunkException(Exception):
    """Lanzada cuando se intenta analizar un chunk que ya fue analizado"""

class Chunk(models.Model):
    """
    Un 'trozo' de material a analizar
    """
    PENDING = 1
    DONE = 2
    DIRTY = 3
    LOCKED = 4
    STATUS_CHOICES = (
        (PENDING, 'Pendiente'),
        (DONE, 'Cerrado'),
        (DIRTY, 'Dirty'),
        (LOCKED, 'Locked'),
    )

    medio = models.ForeignKey(Medio, related_name='chunks')
    evento_original = models.ForeignKey(Evento, related_name='chunks')
    desde = models.DateTimeField(db_index=True, null=True, blank=True)
    hasta = models.DateTimeField(db_index=True, null=True, blank=True)
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=PENDING)
    models = models.ManyToManyField(BrandModel, related_name='chunks', null=True)


    def __unicode__(self):
        return u'Chunk #{}'.format(self.pk)

    @staticmethod
    def factory(evento, medio, desde, hasta, brandmodels):
        """Limitar el tamaño de los chunks"""
        result = []

        delta = hasta - desde
        if delta.total_seconds() / 60 > CHUNK_MAX_MINUTES:
            # crear dos chunks de la mitad de este
            medio_delta = delta / 2
            for _desde, _hasta in [(desde, desde + medio_delta),
                                   (desde + medio_delta, hasta)]:

                # recursivamente crear chunks de la mitad de duración de este
                result.extend(
                    Chunk.factory(evento, medio, _desde, _hasta, brandmodels)
                )
        else:
            chunk = Chunk(
                medio=medio,
                desde=desde,
                hasta=hasta,
                status=Chunk.PENDING,
                evento_original=evento
            )
            chunk.save()
            for bm in brandmodels:
                chunk.models.add(bm)
            result.append(chunk)
        return result

    def save(self, *args, **kwargs):
        """Que ningún chunk sea demasiado largo"""
        if (self.hasta - self.desde).total_seconds() / 60 <= CHUNK_MAX_MINUTES:
            super(Chunk, self).save(*args, **kwargs)

    def analize(self):
        """Analizar este chunk!"""

        if self.status == Chunk.LOCKED:
            raise LockedChunkException

        if self.status == Chunk.DONE:
            raise DoneChunkException

        self.status = Chunk.LOCKED
        self.save()

        url = 'http://{}/sigmund/analize_batch/'.format(settings.BRANDTRACK_SERVER_URL)
        images_urls = self.get_images_urls()
        timestamps = sorted(images_urls.keys())
        POST_data = {
            'models': [m.id_brandtrack for m in self.models.all()],
            'images': images_urls.values()
        }
        request = requests.post(url, POST_data)

        if request.ok and request.status_code == 200:
            try:
                response = request.json()
            except ValueError:
                print 'Problemas al entender la respuesta de brandtrack'
                print request.text()

                self.status = Chunk.PENDING
                self.save()
                return

            if response['success']:
                result = response['data']
            else:
                print 'response success False'
                self.status = Chunk.PENDING
                self.save()
                return
        else:
            print 'Problemas con el request a brandtrack'
            print request.text
            self.status = Chunk.PENDING
            self.save()
            return

        data = defaultdict(lambda: defaultdict(list))
        # data es un diccionario cuyas claves son las marcas involucradas
        # en el análisis. Los valores de este diccionario son a su vez
        # diccionarios, cuyas claves son datetimes. Los valores de estos
        # sub-diccionarios son listas de bounding boxes. Ejemplo:
        # {
        #   <Marca: Natura Aceites>:
        #     {
        #       datetime.datetime(2015, 9, 22, 10, 5, 15):
        #         [
        #           {
        #            u'x1': 24,
        #            u'y1': 37,
        #            u'score': 1.0000001152350884,
        #            u'x2': 75,
        #            u'model': u'146',
        #            u'y2': 69
        #            },
        #         ],
        #         etc...

        # mapeo ids de models a instancias d model, para no consultar la bbdd
        # cada vez que necesito acceder a un model por el id de brandtrack
        models_id_to_instances = {}
        for m in self.models.all():
            # le agrego como atributo de instancia, la marca
            m.marca = m.get_marca()
            models_id_to_instances[str(m.id_brandtrack)] = m

        for date_time, image_url in images_urls.items():
            if result[image_url]['success']:
                image_data = result[image_url]['boxes']
            else:
                image_data = {}
            for model_id_brandtrack in image_data.keys():
                for box in image_data[model_id_brandtrack]:
                    # voy agrupando los bounding boxes
                    # detectados por marca y por datetime
                    marca = models_id_to_instances[model_id_brandtrack].marca
                    data[marca][date_time].append(box)

        # crear segmentos
        for marca in data.keys():
            # crear un arreglo de scores, donde cada índice
            # representa un thumb consultado (un segundo)
            scores = []
            boxes_in_times = defaultdict(list)
            for dt in timestamps:
                for bbox in data[marca].get(dt, []):
                    if bbox['score'] > models_id_to_instances[bbox['model']].score_threshold:
                        boxes_in_times[dt].append(bbox)
                if boxes_in_times.has_key(dt):
                    prom = np.mean([b['score'] for b in boxes_in_times[dt]])
                    scores.append(prom)
                else:
                    scores.append(0.0)

            # eliminar detecciones de esta marca que estén aisladas
            scores = eliminar_detecciones_aisladas(scores)

            # array de booleanos donde se detectó la marca
            matches_index = scores_to_matches_index(np.array(scores))

            # indices de los comienzos y finales de los matches
            for a, b in pairwise(matches_index):
                desde = timestamps[a]
                hasta = timestamps[b]
                print 'desde {:%H:%M:%S} hasta {:%H:%M:%S}'.format(desde, hasta)
                segmentos = [
                    Segmento(
                        desde=desde,
                        hasta=hasta,
                        medio=self.medio,
                        marca=marca,
                        tipo=TipoSegmento.objects.get(nombre='Logos')
                    )
                ]
                superpuestos = segmentos[0].superpuestos()
                salvar = True
                for s in superpuestos:
                    if not s.automatico():
                        if s.desde <= segmentos[0].desde and s.hasta >= segmentos[0].hasta:
                            # está contenido, no crearlo
                            salvar = False
                            break
                        elif s.desde <= segmentos[0].desde and s.hasta < segmentos[0].hasta:
                            # el detectado tiene una parte final que queremos conservar
                            segmentos[0].desde = s.hasta
                        elif s.desde > segmentos[0].desde and s.hasta >= segmentos[0].hasta:
                            # el detectado tiene una parte inicial que queremos conservar
                            segmentos[0].hasta = s.desde
                        else:
                            # el detectado contiene al manual. Caso difícil:
                            # hay que crear dos segmentos a los costados y
                            # distribuir los bounding boxes apropiadamente
                            segmentos.append(
                                Segmento(
                                    desde=s.hasta,
                                    hasta=segmentos[0].hasta,
                                    medio=self.medio,
                                    marca=marca,
                                    tipo=TipoSegmento.objects.get(nombre='Logos')
                                )
                            )
                            segmentos[0].hasta = s.desde

                if salvar:
                    for s in segmentos:
                        s.save()
                else:
                    continue
                scores = []
                formato = None
                segmento = segmentos[0]
                for t in timestamps[a:b+1]:
                    if t > segmento.hasta:
                        try:
                            segmento = segmentos[1]
                        except IndexError:
                            pass

                    for box in boxes_in_times[t]:
                        bbox = BoundingBox.objects.create(
                                datetime=t,
                                medio=self.medio,
                                model=models_id_to_instances[box['model']],
                                score=box['score'],
                                x1=box['x1'],
                                y1=box['y1'],
                                x2=box['x2'],
                                y2=box['y2']
                            )
                        bbox.save()
                        segmento.boxes.add(bbox)
                        scores.append(box['score'])
                        if formato is None:
                            formato = models_id_to_instances[box['model']].get_formato()
                if scores:
                    for s in segmentos:
                        s.score = np.mean(scores)
                        s.save()

                if formato:
                    for s in segmentos:
                        s.formato = formato
                        s.save()
        self.status = Chunk.DONE
        self.save()


    def get_images_urls(self, step=1):
        """Devolver mapeo de datetimes a urls de thumbs de este chunk"""
        date_range = DateRange(begin=self.desde,
                               end=self.hasta + timedelta(seconds=1),
                               step=timedelta(seconds=step))
        return {dt: get_thumb_url(self.medio, dt) for dt in date_range}

