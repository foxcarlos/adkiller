from django import forms
from models import Sintonizador, Datacenter

class DatacenterForm(forms.ModelForm):
	class Meta:
		model = Datacenter
		exclude = []

class SintonizadorForm(forms.ModelForm):
	class Meta:
		model = Sintonizador
		exclude = ['canal_sintonizado','rack','bandeja','posicion','serial','codigo_estado']

class SintonizadorFormUbicacion(forms.ModelForm):
	class Meta:
		model = Sintonizador
		fields = ['rack','bandeja','posicion']
