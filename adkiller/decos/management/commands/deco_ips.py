
from django.core.management.base import BaseCommand
from optparse import make_option
from adkiller.decos.models import Datacenter
from adkiller.app.models import Ciudad
from adkiller.decos.models import Sintonizador
import adkiller.decos.control_api as controller

def lines(a):
    afile = open(a, 'r')
    lines = afile.readlines()
    return lines


"importar decos desde txt"
class Command(BaseCommand):
    args = '<deco-file>'


    def handle(self ,*args, **options):
        cvs_file = args[0]
        entradas = lines(cvs_file)
        for i in range(0,len(entradas)):
            if i%2==0:

                ip = entradas[i].replace(" ", "")
                data = entradas[i+1].split(" ",1)
                sintonizador = Sintonizador.objects.get(access_card =data[0] , receiver_id = data[1].replace("\n","") )
                sintonizador.ip = ip.replace('\n',"")
                sintonizador.save()
                # response = controller
                # response= controller.change_channel("http://190.210.208.35/",ip.replace('\n',"") + ":8080", 200)
                # print response
                response= controller.press_key("http://190.210.208.35/",ip.replace('\n',"") + ":8080", "exit")
                print response
                response= controller.press_key("http://190.210.208.35/",ip.replace('\n',"") + ":8080", "menu")
                print response
                # # response= controller.press_key("http://190.210.208.35/",ip.replace('\n',"") + ":8080", "menu")
                # # print response
                print "----"