
from django.core.management.base import BaseCommand
from optparse import make_option
from adkiller.decos.models import Datacenter
from adkiller.app.models import Ciudad
from adkiller.decos.models import Sintonizador


def lines(a):
    afile = open(a, 'r')
    lines = afile.readlines()
    return lines



class Command(BaseCommand):
    args = '<cvs-file>'


    def handle(self ,*args, **options):
        cvs_file = args[0]
        entradas = lines(cvs_file)
        print entradas
        entradas = entradas[2:]
        a=0
        for line in entradas:
            i = line.split(",")
            print i
            a += 1
            print str(a)
            recid = i[5]
            accesscard = i[6]
            ciudad = i[7]
            datacenter = i[9]
            print datacenter
            rack = i[10]
            bandeja = i[11]
            posicion = i[12]
            Sintonizador.objects.get_or_create(receiver_id=recid, access_card=accesscard, rack=rack, bandeja=bandeja, posicion=posicion, ciudad=Ciudad.objects.get_or_create(nombre=ciudad)[0], datacenter=Datacenter.objects.get_or_create(nombre=datacenter)[0])
