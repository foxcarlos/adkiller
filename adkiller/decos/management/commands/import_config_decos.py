
from django.core.management.base import BaseCommand
from adkiller.decos.models import Datacenter
from adkiller.app.models import Ciudad, Medio
from adkiller.decos.models import Sintonizador


def lines(a):
    afile = open(a, 'r')
    lines = afile.readlines()
    return lines



class Command(BaseCommand):
    args = '<cvs-file>'


    def handle(self ,*args, **options):
        cvs_file = args[0]
        entradas = lines(cvs_file)
        entradas = entradas[2:]
        a=0
        for line in entradas:
            deco, device, slot, medio = line.split(",")
            medio = medio.replace("\n", "")
            print "'"  + medio + "'"
            s = Sintonizador.objects.get(receiver_id__endswith=deco[-4:])
            try:
                m = Medio.objects.get(nombre__iexact=medio)
            except:
                m = Medio.objects.filter(nombre__icontains=medio)
                if m:
                    m = m[0]
            print s, m, device, slot
            if m:
                s.medio_asignado = m
                s.save()
                m.dvr = device
                m.canal = slot
                m.save()
           