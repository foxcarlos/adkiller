
from django.core.management.base import BaseCommand
from optparse import make_option
from adkiller.decos.models import Datacenter
from adkiller.app.models import Ciudad
from adkiller.decos.models import Sintonizador
import adkiller.decos.control_api as controller
import httplib2

def lines(a):
    afile = open(a, 'r')
    lines = afile.readlines()
    return lines


"importar decos desde txt"
class Command(BaseCommand):
    args = '<deco-file><option><canal>'


    def handle(self ,*args, **options):
        cvs_file = args[0]
        # option = int(args[1])
        entradas = lines(cvs_file)
        # print "Los siguientes decos no tienen el 123."
        for i in range(0,len(entradas)):
            if i%2==0:
                ip = entradas[i].replace(" ", "")
                ip = ip.replace('\n',"")
                if ip in ['10.1.1.106','10.1.1.112','10.1.1.124','10.1.1.104']:
                    pass
                else:
                    ip += ":8080"
                    data = entradas[i+1].split(" ",1)
                    sintonizador = Sintonizador.objects.get(access_card =data[0] , receiver_id = data[1].replace("\n","") )
                    datacenter_url = sintonizador.datacenter.ip + "/"
                    # response = controller.change_channel( datacenter_url ,ip , 608 )
                    response = controller.get_channel(datacenter_url,ip)
                    print response
                    # if response < 0:
                    #     print data[1].replace('\n','')