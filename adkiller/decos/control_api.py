# -*- coding: utf8 -*-
import json
import httplib2
import commands
import os
import datetime
import time

# http://clips.infoad.tv/forward_decos.php?ip=192.168.1.100&funcion=tv/getTuned
# %load_ext autoreload

IP_TEST = "10.1.1.121:8080"
DATACENTER = "http://190.210.208.35/"
COMMANDS = {
'change_aspect16:9':["menu","down","down","down","right","select","down","select","down","down","select","down","select","exit"],
'change_aspect4:3':["menu","down","down","down","right","select","down","select","down","down","select","select","exit"],
'1':["1"],
"2":["2"],
"3":["3"],
"4":["4"],
"5":["5"], 
"6":["6"],
"7":["7"],
"8":["8"],
"9":["9"],
"0":["0"],
'power':["power"],
"select":["select"],
"menu":["menu"],
"left":["left"],
"up":["up"],
"down":["down"],
"right":["right"],
"exit":["exit"],
"chanup":["chanup"],
"chandown":["chandown"],
"guide":["guide"],
"dash":["dash"],
"back":["back"],
"ir_a_nexus": ["menu","down","down","down","right","select","down","down","down","down","down","down","down","select","down","select"],
"ver_info":["menu","down","down","down","right","select"],
"config":["menu","down","down","down","right","select"],
"ahorro_de_energia":["menu","down","down","down","right","select","down","down","down","down","down","select"],
"pantalla":["menu","down","down","down","right","select","down","select"],
}

def get_channel( datacenter_url,ip):
    try:
        h = httplib2.Http()
        string ="%sforward_decos.php?ip=%s&funcion=tv/getTuned" % (datacenter_url , ip)
        resp, content = h.request(string, "GET", headers={})
        content = json.loads(content)
    except Exception as e:
        return -1
    if content['status']['code']== 200:
        return content['major']
    else: #something wrong
        return -1 

def change_channel( datacenter_url , ip , channel):
    # sino existe canal se deja canal viejo

    old_channel = get_channel(datacenter_url,ip)
    if old_channel == -1:
        # print "El deco probablemente no tiene acceso externo >> " + ip
        return -1 
    try:
        h = httplib2.Http()
        string = "%sforward_decos.php?ip=%s&funcion=tv/tune?major=%s" % (datacenter_url,ip,channel)
        # print string
        resp , content = h.request(string , "GET", headers={})
        content = json.loads(content)
    except Exception as e:
        return -2
    if content['status']['commandResult']==0:
        return 0
    else:
        if content['status']['msg']=="Channel does not exist.":
            return -1 , "El canal no existe"

def press_key( datacenter_url ,ip , key):
    try:
        h = httplib2.Http()
        string = "%sforward_decos.php?ip=%s&funcion=remote/processKey?key=%s&hold=keyPress" % (datacenter_url, ip,key)
        # print string
        resp , content = h.request(string, "GET", headers={})
        content = json.loads(content)
    except Exception:
        return -1
    if content['status']['commandResult']==0:
        return 0
    else:
        return -1 

def command_exec(datacenter_url,ip , command):
    command_list = COMMANDS[command]
    for i in command_list:
        time.sleep(1)
        status = press_key(datacenter_url,ip,i)
        if status == -1:
            return -1 # nos largamos a llorar
    return 0

def get_mode(datacenter_url,ip):
    # import ipdb; ipdb.set_trace()
    # 0 is active
    # 1 is standby
    # if there is something, we are going to return  -1, 
    try:
        h = httplib2.Http()
        resp , content = h.request("%sforward_decos.php?ip=%s&funcion=info/mode&s" % (datacenter_url,ip), "GET", headers={})
        content = json.loads(content)
    except Exception:
        return -1 , "Algo a ocurrido mal"
    if content['status']['commandResult']==0:
        # if content['mode']==0:
        #     status = get_channel(datacenter_url, ip)
        #     if status == -1: #access refused or something like that
        #         return (-1 , "El sistematizador esta prendido, pero no responde.")
        return content['mode'] , "El sintonizador esta encedido."
    else: #que errores puede haber
        return -1 , content['status']['msg']


def main_controller(datacenter_url,ip, command = None,new_channel = None):
    response={}
    response['mode'] , response['msg-mode'] = get_mode(datacenter_url,ip)
    if command!= None:
        response['status'] = command_exec(datacenter_url,ip , command)
    if new_channel != None:
        response['status-new-channel'] = change_channel(datacenter_url,ip,new_channel)
    response['channel'] = get_channel( datacenter_url, ip)
    return response

