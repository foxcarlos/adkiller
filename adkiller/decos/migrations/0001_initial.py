# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Datacenter'
        db.create_table('decos_datacenter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=150)),
            ('ip', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ciudad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Ciudad'], null=True, blank=True)),
        ))
        db.send_create_signal('decos', ['Datacenter'])

        # Adding model 'Sintonizador'
        db.create_table('decos_sintonizador', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('serial', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('receiver_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('access_card', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('codigo_estado', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('canal_sintonizado', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('medio_asignado', self.gf('django.db.models.fields.related.OneToOneField')(blank=True, related_name='sintonizador', unique=True, null=True, to=orm['app.Medio'])),
            ('ip', self.gf('django.db.models.fields.GenericIPAddressField')(max_length=39, null=True, blank=True)),
            ('datacenter', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['decos.Datacenter'], unique=True, null=True, blank=True)),
            ('rack', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('bandeja', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('posicion', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
        ))
        db.send_create_signal('decos', ['Sintonizador'])


    def backwards(self, orm):
        # Deleting model 'Datacenter'
        db.delete_table('decos_datacenter')

        # Deleting model 'Sintonizador'
        db.delete_table('decos_sintonizador')


    models = {
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.etiquetamedio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EtiquetaMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'app.grupomedio': {
            'Meta': {'object_name': 'GrupoMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.medio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Medio'},
            'canal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'canal_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'codigo_ibope': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'desfasando': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dvr': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'encargado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'etiquetas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'medios'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.EtiquetaMedio']"}),
            'grabando': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'grabando_en_cero': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.GrupoMedio']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'incremento_color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'mediadelivery_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_deteccion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'server_grabacion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_grabacion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.rol': {
            'Meta': {'object_name': 'Rol'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'e_mail_del_tecnico': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'espacio_en_disco': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'maquina_virtual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nombre_del_tecnico': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'online': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'plaza': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'servidores'", 'null': 'True', 'to': "orm['app.Ciudad']"}),
            'puerto_http': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_ssh': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'recibir_patrones': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'roles': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['app.Rol']", 'symmetrical': 'False', 'blank': 'True'}),
            'tecnico_alertado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ultima_vez_online': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'padre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'decos.datacenter': {
            'Meta': {'object_name': 'Datacenter'},
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Ciudad']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'decos.sintonizador': {
            'Meta': {'ordering': "['receiver_id']", 'object_name': 'Sintonizador'},
            'access_card': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'bandeja': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'canal_sintonizado': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'codigo_estado': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'datacenter': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['decos.Datacenter']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.GenericIPAddressField', [], {'max_length': '39', 'null': 'True', 'blank': 'True'}),
            'medio_asignado': ('django.db.models.fields.related.OneToOneField', [], {'blank': 'True', 'related_name': "'sintonizador'", 'unique': 'True', 'null': 'True', 'to': "orm['app.Medio']"}),
            'posicion': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'rack': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'receiver_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'serial': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['decos']