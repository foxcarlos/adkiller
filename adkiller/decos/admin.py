# -*- coding: utf-8 -*-

import datetime
import json


from django.contrib import admin
from django.http import HttpResponse
from django.conf.urls import patterns
from django.template import RequestContext
from django.shortcuts import render_to_response, redirect
from django.http import Http404
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt


import adkiller.decos.control_api as control_api
from adkiller.decos.models import Datacenter, Sintonizador , Medio
from adkiller.decos.forms import DatacenterForm, SintonizadorForm , SintonizadorFormUbicacion
from adkiller.app.forms import MedioForm


class DatacenterAdmin(admin.ModelAdmin):
    list_display = ['nombre','ciudad','ip']
    search_fields = ['nombre']
    list_filter = ['ciudad']


class SintonizadorAdmin(admin.ModelAdmin):
    list_display = ['receiver_id', 'access_card','ip', 'medio_asignado', 'ciudad', 'datacenter', 'rack', 'bandeja', 'posicion', 'device', 'slot']
    list_filter = ['ciudad','datacenter','posicion','bandeja','medio_asignado__dvr']
    search_fields = ['receiver_id']
    change_form_template = 'admin/decos/sintonizador/change_form.html'

    def device(self,obj):
        return obj.medio_asignado.dvr

    def slot(self,obj):
        return obj.medio_asignado.canal


    def get_sintonizador(self,object_id):
        try:
            sintonizador = Sintonizador.objects.get(pk = object_id)
        except:
            raise Http404
        return sintonizador


    def change_view(self, request, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        sintonizador = self.get_sintonizador(object_id)


        extra_context['sintonizador'] = sintonizador
        extra_context['datacenter'] = sintonizador.datacenter
        extra_context['medio'] = sintonizador.medio_asignado
        #agregamos el form
        if sintonizador==None:
            form = SintonizadorForm()
            ubicacionform =  SintonizadorFormUbicacion()
        else:
            form = SintonizadorForm(instance = sintonizador)
            ubicacionform =  SintonizadorFormUbicacion(instance = sintonizador)
        if sintonizador.medio_asignado != None:
            extra_context['medioform'] = MedioForm(instance = sintonizador.medio_asignado)

        extra_context['form'] = form
        extra_context['ubicacionform'] = ubicacionform

        return super(SintonizadorAdmin, self).change_view(request, object_id,
            form_url, extra_context=extra_context)


    def get_urls(self):
        urls = super(SintonizadorAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^estado_sintonizador/$', self.admin_site.admin_view(self.cambiar_estado)),
            (r'^verificar_last/(?P<pk>\d+)/$', self.admin_site.admin_view(self.verificar_last)),
            (r'^manage_sintonizador_form1/(?P<pk>\d+)/$',self.admin_site.admin_view(self.manage_sintonizador_form)),
            (r'^manage_sintonizador_form2/(?P<pk>\d+)/$',self.admin_site.admin_view(self.manage_sintonizador_form_ubicacion)),
            (r'^manage_sintonizador_form3/(?P<pk>\d+)/$',self.admin_site.admin_view(self.manage_medio_form)),

        )
        # import ipdb;ipdb.set_trace()
        return my_urls + urls

    def manage_sintonizador_form(self, request, pk):
        try:
            sintonizador = Sintonizador.objects.get(pk = pk)
        except Exception:
            raise Http404

        if request.method == 'POST':
            form = SintonizadorForm(request.POST, instance = sintonizador)
            if form.is_valid():
                form.save()
        return redirect('/admin/decos/sintonizador/'+str(pk))


    def manage_sintonizador_form_ubicacion(self, request, pk):
        try:
            sintonizador = Sintonizador.objects.get(pk = pk)
        except Exception:
            raise Http404

        if request.method == 'POST':
            form_ubicacion = SintonizadorFormUbicacion(request.POST , instance = sintonizador)
            if form_ubicacion.is_valid():
                form_ubicacion.save()
        return redirect('/admin/decos/sintonizador/'+str(pk))


    def manage_medio_form(self, request, pk):
        try:
            sintonizador = Sintonizador.objects.get(pk = pk)
        except Exception:
            raise Http404
        if request.method == 'POST':
            medioform = MedioForm(request.POST , instance = sintonizador.medio_asignado)
            if medioform.is_valid():
                medioform.save()
        return redirect('/admin/decos/sintonizador/'+str(pk))

    # el pk de la función de abajo es de un medio
    def verificar_last(self, request , pk ):
        try:
            medio = Medio.objects.get(pk = pk)
        except Exception:
            raise Http404
        esta_atrasado = medio.get_retraso()
        response = {}
        response['retrasado'] = esta_atrasado[0]
        response['tiempo'] = esta_atrasado[1]
        return HttpResponse(json.dumps(response),content_type="application/json")



    @csrf_exempt
    def cambiar_estado(self,request):
        comando = None
        nuevo_canal = None
        # import ipdb; ipdb.set_trace()
        if request.method == "POST":
            primary_key = request.POST.get('pk')
            try:
                sintonizador = Sintonizador.objects.get(pk = primary_key)
            except Exception:
                raise Http404
            if sintonizador.ip == None:
                return HttpResponse("")
            ip = sintonizador.ip + ":8080"
            datacenter_url = sintonizador.datacenter.ip
            if datacenter_url[len(datacenter_url) -1] != '/':
                datacenter_url += '/'
            # me fijo que comandos tengo que ejecutar
            if request.POST.has_key('command'):
                comando = request.POST.get('command')
            if request.POST.has_key('new_channel'):
                nuevo_canal = request.POST.get("new_channel")
            # obtengo la informacion del decodficador
            response = control_api.main_controller(datacenter_url,ip, comando ,nuevo_canal)

            #si el canal en el deco es distinto del canal en el model y el commando fue cambiar canal entonces
            #actualizo model
            if response['channel'] != sintonizador.canal_sintonizado and nuevo_canal!=None:
                sintonizador.canal_sintonizado =  nuevo_canal
                sintonizador.save()
            return HttpResponse(json.dumps(response),content_type="application/json")

        return HttpResponse("")





admin.site.register(Sintonizador , SintonizadorAdmin)
admin.site.register(Datacenter , DatacenterAdmin)


