from django.db import models
from adkiller.app.models import Medio
from adkiller.app.models import Ciudad
# Create your models here.

class Datacenter(models.Model):
    nombre = models.CharField(max_length=150, unique=True)
    ip = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True)


    def __unicode__(self):
        return self.nombre

    def get_ip(self):
    	return self.ip


class Sintonizador(models.Model):
    serial = models.CharField(max_length=100, null=True, blank=True)
    receiver_id = models.CharField(max_length=100, unique=True)
    access_card = models.CharField(max_length=100, null=True, blank=True)
    codigo_estado = models.IntegerField(default=0)
    canal_sintonizado = models.IntegerField(null=True, blank=True)
    medio_asignado = models.ForeignKey(Medio,related_name="sintonizador", null=True, blank=True)
    ip = models.GenericIPAddressField(null=True, blank=True)
    datacenter = models.ForeignKey(Datacenter, null=True, blank=True)
    rack = models.CharField(max_length=100, null=True , blank = True)
    bandeja = models.CharField(max_length=100, null=True , blank = True)
    posicion = models.CharField(max_length=100, null=True , blank = True)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True)

    def __unicode__(self):
        return self.receiver_id

    def get_canal_sintonizado(self):
        h = httplib2.Http()
        url = "http://clips.infoad.tv/forward_decos.php?ip=%s&funcion=tv/getTuned" % (self.ip)
        resp, content = h.request(url, "GET", headers={})
        if resp['status'] == '200' and content:
            content = json.loads(content.decode('utf-8'))
            if "OK" in content['status']['msg']:
                return content['major']
            else:
                print content['status']
        else:
            print "Problemas al conectar a Iserver"

    def set_canal_sintonizado(self, canal):
        h = httplib2.Http()
        minor=65535
        clientAddr="0"
        url = "http://clips.infoad.tv/forward_decos.php?ip=%s&funcion=tv/tune?major=%s&minor=%s&clientAddr=%s" % (self.ip, str(canal), str(minor), clientAddr)
        resp, content = h.request(url, "GET", headers={})
        if resp['status'] == '200' and content:
            content = json.loads(content.decode('utf-8'))
            if "OK" in content['status']['msg']: #TODO en otra parte de la documentacion dice "OK."
                return content['status']['commandResult'] == 0
            else:
                print content['status']
        else:
            print "Problemas al conectar a Iserver"

    def get_entrada_grabacion(self):
        dvr = self.medio_asignado.dvr
        canal = self.medio_asignado.canal
        return "%s - %s" % (dvr, canal)

    class Meta:
        verbose_name_plural = "Sintonizadores"
        ordering = ["receiver_id"]
