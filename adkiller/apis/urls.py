from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^filter_origenes_sponsoring/', filter_origenes_sponsoring),
    url(r'^get_mediadelivery_id/', get_mediadelivery_id),
)
