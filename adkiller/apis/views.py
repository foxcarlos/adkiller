# -*- coding: iso-8859-15 -*-
# pylint: disable=line-too-long
"""
Views for app apis
"""
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

import json

from adkiller.app.models import Medio, Origen

@csrf_exempt
def get_mediadelivery_id(request):
    """Recibo un elemento para filtrar el medio
    devuelvo su mediadelivery_id"""
    result = -1
    if request.GET:
        q = request.GET['q']
        m = Medio.objects.filter(nombre=q) or Medio.objects.filter(identificador=q)
        if m and (u"rdoba" in m[0].nombre or not "Interior" in m[0].soporte.nombre) and not "Radio" in m[0].soporte.nombre and not "Satelital" in m[0].soporte.nombre:
            result = m[0].mediadelivery_id

    data = json.dumps(result, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


@csrf_exempt
def filter_origenes_sponsoring(request):
    """recibo una lista de origenes y una serie de reglas para filtrar
    evuelvo un json con los que reúnen las condiciones necesarias"""
    if request.POST:

        def aplicar_filtro(origen, medio):
            """..."""
            etiquetas_medio = medio.etiquetas.all()

            # las etiquetas son comparadas como minúsculas
            if etiquetas_medio and 'deporte' in [e.nombre.lower() for e in etiquetas_medio]:
                return True
            etiquetas_programa = origen.horario.programa.etiquetas.all()
            if etiquetas_programa and 'deportivo' in [e.nombre.lower() for e in etiquetas_programa]:
                if medio.soporte.nombre == 'TV Paga' or (medio.ciudad.nombre == 'Buenos Aires' and medio.soporte.nombre == 'TV Aire'):
                    return True
            if etiquetas_programa and 'noticias' in [e.nombre.lower() for e in etiquetas_programa]:
                if medio.ciudad.nombre == 'Buenos Aires' and medio.soporte.nombre == 'TV Aire':
                    return True
                if any([ident in medio.identificador for ident in ['A24', 'TN', 'C5N', 'CN23']]):
                    return True

        result = []

        data = json.loads(request.POST.keys()[0])
        file_list = data['file_list']

        dvr = camara = medio = None
        for elem in file_list:
            # parsear el nombre, teniendo en cuenta que suelen venir muchos
            # archivos seguidos del mismo medio
            _dvr, _camara = elem.split('_')[:2]
            if _dvr != dvr or _camara != camara:
                dvr, camara = _dvr, _camara
                try:
                    medio = Medio.objects.get(dvr=dvr, canal=int(camara))
                except ObjectDoesNotExist:
                    medio = None
                    continue
            # crear un origen
            origen = Origen(archivo=elem)
            origen.set_horario(medio=medio)

            if aplicar_filtro(origen, medio):
                result.append(elem)

        data = json.dumps(result, cls=DjangoJSONEncoder)
        return HttpResponse(data, mimetype='application/json')


