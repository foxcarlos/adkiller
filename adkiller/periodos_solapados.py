# -*- coding: iso-8859-15 -*-
import datetime

class Periodo:
    def __init__(self, inicio, fin):
        self.inicio = datetime.datetime.today()
        self.inicio = self.inicio.replace(hour=inicio.hour)
        self.inicio = self.inicio.replace(minute=inicio.minute)
        self.inicio = self.inicio.replace(second=inicio.second)
        self.inicio = self.inicio.replace(microsecond=inicio.microsecond)

        self.fin = datetime.datetime.today()
        self.fin = self.fin.replace(hour=fin.hour)
        self.fin = self.fin.replace(minute=fin.minute)
        self.fin = self.fin.replace(second=fin.second)
        self.fin = self.fin.replace(microsecond=fin.microsecond)

        if self.fin < self.inicio:
            self.fin = self.fin + datetime.timedelta(days=1)

        self.duracion = self.fin - self.inicio


    def imprime(self):
        print "inicio:   ", self.inicio
        print "fin:      ", self.fin
        print "duracion: ", self.duracion

    def solapa(self, otro_periodo):
        # si yo empiezo antes o junto
        if self.inicio <= otro_periodo.inicio:
            # me fijo si termino después de que empiece el otro (o junto)
            return self.fin >= otro_periodo.inicio
        # si yo empiezo después
        else:
            # me fijo si el otro termina después que yo empiece (o junto)
            return otro_periodo.fin >= self.inicio

    def get_inicio(self):
        return self.inicio

    def get_fin(self):
        return self.fin


if __name__ == '__main__':
    p1 = Periodo(datetime.time(23, 00), datetime.time(23, 59, 59))
    p2 = Periodo(datetime.time(00, 00), datetime.time(00, 59, 59))

    print
    print "Período 1"
    p1.imprime()
    print "Período 2"
    p2.imprime()

    if p1.solapa(p2):
        print "SE SOLAPAN"
    else:
        print "NO SE SOLAPAN"

    if p2.solapa(p1):
        print "SE SOLAPAN"
    else:
        print "NO SE SOLAPAN"
