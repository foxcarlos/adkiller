(function($){
	$(document).ready(function(){

        // Scroll General 
        $.scrollIt();

		// Scroll Top
		$('a[href=#header]').on('click', function(e){
			e.preventDefault();
       		$('html, body').animate({scrollTop:0}, {duration: 1000, easing: 'easeOutExpo'});
    	});

    	$('ul.footer-menu a.trigger').on('click', function(e) {
    		e.preventDefault();
    		var target = $(this).data('href');

    		$('html, body').animate({scrollTop:0}, {duration: 1000, easing: 'easeOutExpo', complete: function() {
    			$('#'+target).parents('.dropdown').addClass('open');
    		}});
    	});

		// Forms
		$('a.dismiss').on('click', function(e){
			e.preventDefault();
			$(this).parents('.dropdown').removeClass('open');
		});

    	$('form input').tooltip({
    		placement: 'right',
    		trigger: 'manual'
    	});

        $('#country').val(geoplugin_countryName());

    	$('form').on('submit', function(e){
          	 if (e.preventDefault) {
          	     e.preventDefault();
      	     } else {
      	         e.returnValue = false;
  	         }

    		var camposRequeridos = $('input[data-required="true"]', this);
    		var readyToGo = 0;
            var tipo = $(this).data('type');

    		camposRequeridos.each(function(){
    			if ($(this).val() == '') {
    				$(this).tooltip('show');
    			} else {
    				readyToGo++;
    			}
    		});

    		if (readyToGo == camposRequeridos.size()) {
    			var form = $(this),
    				submitBtn = form.find('.submit'),
    				msgBox = form.find('.msg-box');

    			var async = $.ajax({
    				type: 'POST',
    				url: form.attr('action'),
    				data: form.serialize(),
    				beforeSend: function(){
    					// Previene multiples envios
                        msgBox.hide();
    					submitBtn.attr('disabled', 'disabled');
    				}
    			});

                if (tipo == 'contacto') {
                    if ($("#nombre").val() == "" || $("#empresa").val() == "" || $("#telefono").val() == "" || $("#email").val() == "") {
                        alert("Debe completar todos los datos para poder comunicarnos con usted");
                        return;
                    };
                    async.complete(function(){

            			var async = $.ajax({
            				type: 'GET',
            				url: "/home/solicitar_contacto/",
            				data: form.serialize()
            			});

                        if (msgBox.hasClass('en')) {
                            msgBox.fadeIn().text('Thank You for contact us.');
                        } else {
                            msgBox.fadeIn().text('Gracias por su solicitud. En breve nos comunicaremos con usted');
                        }
                        
                        setTimeout(function() {
                            $('#contacto').dropdown('toggle');
                        }, 3000);
                        window.location.href="/thankyou/";
                    });
                }

                if (tipo == 'login') {
        			async.done(function(response){
        				if (response.status) {
                            if (response.msg == "Cargando...") {
                                msgBox.html('<img id="loader" src="static/home/img/ajax-loader.gif"/>');
                                msgBox.fadeIn();
                            } else {
                                msgBox.fadeIn().text(response.msg);
                            };

    						setTimeout(function() {
    							window.location.href = '/app/';
    						}, 2000);
        					
        				} else {
        					msgBox.fadeIn().text(response.msg);
        					submitBtn.removeAttr('disabled', 'disabled');
        				}
        			});

        			async.fail(function(error){
                        console.log(error.statusText);
        				msgBox.fadeIn().text(error.statusText);
        				submitBtn.removeAttr('disabled', 'disabled');
        			});
                }
    		}
    	});


    	// Intro Carousel
		$('#features').carousel();

		$('#features').on('slid.bs.carousel', function () {
		var $this = $(this),
			captionTarget = $(this).find('div.item.active').data('caption');
			//$('div.caption').fadeOut(100);
			$('#captions').find('div.' + captionTarget).addClass('showing').siblings().removeClass('showing');
		});

		// Mostrar primer caption
		$(window).load(function(){
			$('#captions .caption:first').addClass('showing');
		});

        // Blog Posts

        var blogJson = $.ajax({
            url: '//infoad.com.ar/blog/?json=get_recent_posts&count=3',
            type: 'GET',
            dataType: 'JSONP'
        });

        blogJson.done(function(blog) {
            $('#redes-sociales').fadeIn();
            if (blog.count > 0) {
                // Post grande
                var post1Data = blog.posts[0];
                var post1 = '<a target="_blank" href="'+post1Data.url+'">';
                    post1 += '<h4>'+post1Data.title_plain+'</h4>';
                    if (post1Data.attachments.length) {
                        post1 += '<img width="554" height="271" src="'+post1Data.attachments[0].images.large.url+'" />';
                    }
                    post1 += '</a>';

                $('#post-large').html(post1);

                // Post chico
                var post2Data = blog.posts[1];
                var post2 = '<a target="_blank" href="'+post2Data.url+'">';
                    post2 += '<h4>'+post2Data.title_plain+'</h4>';
                    if (post2Data.attachments.length) {
                        post2 += '<img width="271" height="271" src="'+post2Data.attachments[0].images['thumbnail-port'].url+'" />';
                    }

                $('#post-small').html(post2);

                // Post chico2
                var post3Data = blog.posts[2];
                var post3 = '<a target="_blank" href="'+post3Data.url+'">';
                    post3 += '<h4>'+post3Data.title_plain+'</h4>';
                    if (post3Data.attachments.length) {
                        post3 += '<img width="271" height="271" src="'+post3Data.attachments[0].images['thumbnail-port'].url+'" />';
                    }

                $('#post-small-2').html(post3);


            }
        });

        blogJson.fail(function(error){
            console.log(error);
        });
        
        /*
        //Twitter
        $.ajax({
            url: 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=fdsoria&count=2',
            type: 'GET',
            dataType: 'JSONP',
            success: function(tweet) {
                $('#tweets').html(tweet[0].text);
            }
        });
        */
	});

    
       
            
        
})(jQuery);