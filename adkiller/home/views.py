import json
# Django Imports
from django.template.loader import get_template
from django.shortcuts import HttpResponseRedirect, render_to_response
from django.template import RequestContext
from django.conf import settings
from django.core.mail import EmailMultiAlternatives, EmailMessage, get_connection
from django.template import Context
from django.http import HttpResponse
from django.views.decorators.cache import cache_page

# project imports
from adkiller.settings import MAIL_ALERTAS_CONF, SITE_URL


@cache_page(60 * 60 * 24)
def root(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/app/")
    else:
        return render_to_response('landing.html', {'settings': settings}) #,  context_instance=RequestContext(request))


def solicitar_contacto(request):
    params = request.GET
    
    mails = []
    subject = "InfoAD - Gracias por contactarnos"
    template = get_template("email_solicitar_demo.html")
    ctx = {'nombre': params['first_name']}
    msg_body = template.render(Context(ctx)).encode("utf-8")
    to_addr = params['email']
    from_addr = "alertas@infoad.com.ar"
    recipients = [to_addr]
    msg = EmailMessage(subject, msg_body, from_addr, recipients)
    msg.content_subtype = "html"
    mails.append(msg)

    centro_de_experiencia = settings.MAILS_CENTRO_EXPERIENCIA
    msg = EmailMessage("Solicitud de demo", "%s [%s] de la empresa %s, tel %s, ha solicitado una demo" % (params['first_name'], params['email'], params['company'], params['phone']), from_addr, centro_de_experiencia)
    msg.content_subtype = "html"
    mails.append(msg)
        
    mail_connection = get_connection(**MAIL_ALERTAS_CONF)
    mail_connection.open()
    mail_connection.send_messages(mails)
    mail_connection.close()

    return HttpResponse("")
