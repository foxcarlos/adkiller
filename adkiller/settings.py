# Django settings for adkiller project.

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
    ('InfoAD', 'alertas@infoad.com.ar'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'adtrack',                      # Or path to database file if using sqlite3.
        'USER': 'postgres',                      # Not used with sqlite3.
        'PASSWORD': 'fede',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = "Etc/GMT+3" #America/Argentina/Cordoba"
#TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'es-Ar'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'egn7p&amp;87yk(+%7^oj8$6=rs8#1*!s=me70ve3e=3_9e4pe0ru^'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)


MIDDLEWARE_CLASSES = (

    'debug_toolbar.middleware.DebugToolbarMiddleware',
    'corsheaders.middleware.CorsMiddleware',
     'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'adkiller.app.utils.LoginRequiredMiddleware',
    'adkiller.app.utils.LogUserActivityMiddleWare',
    #'adkiller.app.middleware.ProfileMiddleware',
)

ROOT_URLCONF = 'adkiller.urls'

# Python dotted path to the WSGI application used by Django's runserver.
# WSGI_APPLICATION = 'adkiller.wsgi.application'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'adkiller.home',
    'adkiller.app',
    'adkiller.material',
    'adkiller.filtros',
    'adkiller.usuarios',
    'adkiller.control',
    'adkiller.informes',
    'adkiller.social',
    'adkiller.adtrack',
    'adkiller.media_map',
    'adkiller.engines_cache',
    'adkiller.oneshots',
    'adkiller.decos',
    'adkiller.ratings',
    'grappelli',
    'django.contrib.admin',
    'tastypie',
    'south',
    'autocomplete_light',
    'longerusername',
    'django_dowser',
    'django_extensions',
    'grappelli_filters',
    'debug_toolbar',
    'corsheaders',
    'widget_tweaks',
)


import local_settings
try:
    INSTALLED_APPS += local_settings.LOCAL_INSTALLED_APPS
except:
    pass

try:
    MIDDLEWARE_CLASSES += local_settings.LOCAL_MIDDLEWARE
except:
    pass

from adkiller.logging_filters import skip_suspicious_operations
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'skip_suspicious_operations': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': skip_suspicious_operations,
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false', 'skip_suspicious_operations'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


MAIN_URL = "/"

API_LIMIT_PER_PAGE = 20

ALLOWED_HOSTS = [
    'www.infoad.com.ar',
    'infoad.com.ar',
    'www.infoad.tv',
    'infoad.tv',
    'www.infoad.tv',
    'infoad.cl',
    'www.infoad.com.py',
    'infoad.com.py',
    'welo.tv',
    'www.welo.tv',
    'tmp.welo.tv',
    'beta.welo.tv',
    'dev.welo.tv',
]

import warnings
warnings.filterwarnings(
    'ignore',
    r"'override_urls' is a deprecated method & will be removed by v1.0.0. Please use ``prepend_urls`` instead.",
    UserWarning, r'tastypie.*')

# Email Settings
# from local_settings import EMAIL_SERVER

# Mail de errores
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'errores@infoad.com.ar'
EMAIL_HOST_PASSWORD = 'ERROR404'
#EMAIL_HOST_USER = 'alertas@infoad.com.ar'
#EMAIL_HOST_PASSWORD = 'gr4ndcl4ss3'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Mail de alertas
MAIL_ALERTAS_CONF = {
    'host': 'smtp.gmail.com',
    'port': 587,
    'username': 'alertas@infoad.com.ar',
    'password': 'gr4ndcl4ss3',
    'user_tls': True
}


# Mail de informes
# MAIL_INFORMES_CONF = {
#     host: 'smtp.gmail.com',
#     port: 587,
#     username: 'informes@infoad.com.ar',
#     password: '********',
#     user_tls: True
# }

# DEFAULT_FROM_EMAIL = "alertas@infoad.com.ar"

AUTH_PROFILE_MODULE = "filtros.perfil"

DATE_FORMAT = "d/m/Y"

DATE_OUTPUT_FORMAT = "d/m/Y"

DEBUG_TOOLBAR_PATCH_SETTINGS = False

LOGIN_URL =  "/accounts/login/"
LOGIN_REDIRECT_URL = MAIN_URL
LOGOUT_URL = "/accounts/logout/"
LOGIN_EXEMPT_URLS = (
    r'^$',
    r'^$/',
    r'^accounts/login_demo/',
    r'^api',
    r'^app/audiovisuales_por_origen',
    r'^app/datos_tanda',
    r'^app/set_tanda_incompleta',
    r'^app/tanda_completa',
    r'^app/tiene_publicidad',
    r'^app/ultima_emision',
    r'^google',
    r'^home/',
    r'^media_map/post_segments',
    r'^propuesta/',
    r'^robots.txt',
    r'^sitemap.xml',
    r'^static',
    r'^thankyou/',
    r'^usuarios/ajax_login/',
    r'^media_map/',
    r'^app/ver_video/',
    r'^app/video_emision/',
    r'^app/video_producto/',
    r'^app/video_origen/',
    r'^app/info_producto/',
    r'^jwplayer',
    r'^decos/config/',
    r'^ratings',

)


CORS_ORIGIN_WHITELIST = (
        'localhost:8100',
        'localhost:8000',
        'localhost:8001',
        'infoad.tv',
        'tmp.infoad.tv',
        'dev.infoad.tv',
        'test.infoad.tv',
        'welo.tv',
        'www.welo.tv',
        'beta.welo.tv',
        'dev.welo.tv',
        'tmp.welo.tv',
)

#para correr sqlite

SOUTH_TESTS_MIGRATE = False

# Local Configuration Import
# Always put at the end of this file
# Ref: https://code.djangoproject.com/wiki/SplitSettings#Multiplesettingfilesimportingfromeachother

try:
    from local_settings import *
except ImportError:
    print u'File local_settings.py is not found. Continuing with production settings.'
