# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long, too-many-locals, too-many-statements
"""
Imprimir lista de medios con patrones de inicio y fin de espacio publicitario
"""

# django imports
from django.core.management.base import BaseCommand

# project imports
from adkiller.app.models import Medio
from adkiller.media_map.models import AciertosIFEP

class Command(BaseCommand):
    help = u"""
        Imprimir lista de medios con patrones de
        inicio y fin de espacio publicitario
        """

    def handle(self, *args, **options):
        if len(args) == 0: # sin argumentos, todo por defecto
            medios = Medio.objects.filter(soporte__nombre__icontains='tv')#.order_by('-nombre')
        else:
            try:
                medio_nombre = args[0]
                medios = Medio.objects.filter(nombre=medio_nombre)
            except ObjectDoesNotExist:
                exit('No encuentro el medio "{}"'.format(medio_nombre))

        medios_con_patrones = []

        for medio in medios:
            medio = Medio.objects.get(id=medio.id)
            patrones_inicio, patrones_fin = medio.get_patrones_inicio_y_fin()

            if patrones_inicio or patrones_fin:
                medios_con_patrones.append((medio,
                                            patrones_inicio,
                                            patrones_fin))

        total = len(medios_con_patrones)
        for index, (medio, patrones_inicio, patrones_fin) in enumerate(medios_con_patrones, 1):

            inicios_id = [p.id_brandtrack for p in patrones_inicio]
            print '------------------------------------------------'
            print '{} / {} - {}'.format(index, total, medio)
            print 'patrones inicio:', inicios_id
            fines_id = [p.id_brandtrack for p in patrones_fin]
            print 'patrones fin:', fines_id

