# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-docstring, no-self-use
# pylint: disable=attribute-defined-outside-init, no-value-for-parameter
# pylint: disable=unused-argument, too-many-branches
# pylint: disable=too-many-locals, unnecessary-lambda, unused-variable

"""
Crear videos para las tandas de chile de marzo en calidad 30 frames por segundo
"""


# python imports
import sqlite3
from optparse import make_option
from datetime import datetime
from datetime import timedelta
from tempfile import NamedTemporaryFile
import urllib2
import os
import commands
import time
import sys

# django imports
from django.core.management.base import BaseCommand

# project imports
from adkiller.media_map.models import Segmento


DATE_FORMAT = '%Y-%m-%d %H:%M'
EDNA_TANDAS_CHILE_DIR = 'infoxel@edna.infoad.tv:/var/www/multimedia/ar/TandasChile'
DB_FILE = '/home/infoxel/logs/tandas_chile_2.db'
ESPERA = 30 # segundos de espera máxima entre tanda y tanda (si no se crea)

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--id', help=u'Sólo la tanda con este id de segmento'),
        make_option('--desde', help=u'Sólo tandas posteriores a esta fecha'),
        make_option('--hasta', help=u'Sólo tandas anteriores a esta fecha'),
        make_option('--medio', help=u'Sólo tandas de este medio'),
    )

    def get_status_url(self, segmento):
        """Devolver status_url si ya pedí que se cree este segmento,
        o none de lo contrario"""
        cursor = self.conn.cursor()
        cursor.execute('SELECT status_url FROM segmentos WHERE id = {}'.format(segmento.id))
        row = cursor.fetchone()
        result = None
        if row and row[0].startswith('http'):
            result = row[0]
        return result

    def get_file_url(self, segmento):
        """Devolver file_url"""
        cursor = self.conn.cursor()
        cursor.execute('SELECT file_url FROM segmentos WHERE id = {}'.format(segmento.id))
        row = cursor.fetchone()
        result = None
        if row and row[0].startswith('http'):
            result = row[0]
        return result

    def already_uploaded(self, segmento):
        """Devolver true si ya lo subí a Edna"""
        cursor = self.conn.cursor()
        cursor.execute('SELECT subido FROM segmentos WHERE id = {}'.format(segmento.id))
        row = cursor.fetchone()
        return row and row[0] == 1

    def set_as_creado(self, segmento, status_url, file_url):
        """Registrar que ya creé este segmento"""
        cursor = self.conn.cursor()
        cursor.execute('INSERT INTO segmentos (id, status_url, file_url, subido) VALUES ( {}, "{}", "{}", 0)'.format(segmento.id, status_url, file_url))
        self.conn.commit()

    def set_as_uploaded(self, segmento):
        """Registrar que ya subi a edna este segmento"""
        cursor = self.conn.cursor()
        cursor.execute('UPDATE segmentos SET subido = 1 WHERE id = {}'.format(segmento.id))
        self.conn.commit()

    def handle(self, *args, **options):

        if not os.path.exists(DB_FILE):
            self.conn = sqlite3.connect(DB_FILE)
            # crear tabla
            cursor = self.conn.cursor()
            cursor.execute('CREATE TABLE segmentos (id int, status_url text default "null", file_url text default "null", subido int)')
            self.conn.commit()
        else:
            self.conn = sqlite3.connect(DB_FILE)


        segmentos = self.get_segmentos(options)
        total = segmentos.count()

        for index, segmento in enumerate(segmentos, 1):
            print '{} / {} - {}'.format(index, total, segmento)

            status_url = self.get_status_url(segmento)
            ya_subido = self.already_uploaded(segmento)

            if ya_subido:
                print 'ya lo creé y ya está en Edna'
                print
                continue

            if not status_url:
                # nunca lo creé
                primer_pedido = True
                result = segmento.make_clip()
                if result['success']:
                    # check status url
                    status_url = result['status_url']
                    file_url = result['url']
                    self.set_as_creado(segmento, status_url, file_url)
                else:
                    print 'Problemas al pedir el clip'
                    continue
            else:
                primer_pedido = False
                print 'No es el primer pedido, chequeare {} pero no esperaré'.format(status_url)

            try:
                creado = segmento.check_clip_creado(status_url)
            except:
                creado = False
            start = datetime.now()
            while primer_pedido and not creado and datetime.now() - start < timedelta(seconds=ESPERA):
                print ('\rEsperando que se cree por {} segundos más... '
                       'Chequeando esta direción ----> {}'.format(ESPERA - int((datetime.now() - start).total_seconds()),
                                                                  status_url)),
                sys.stdout.flush()
                time.sleep(1)
                try:
                    creado = segmento.check_clip_creado(status_url)
                except:
                    pass

            if not creado:
                print 'no lo creó...'
            else:
                print 'creado!'
                file_url = self.get_file_url(segmento)
                if file_url is None:
                    print "neceisto tu ayuda porque no guarde file url"
                    continue

                moved_ok = FileTransferGenius.move_file(
                    source=file_url,
                    dest=EDNA_TANDAS_CHILE_DIR
                )
                if moved_ok:
                    self.set_as_uploaded(segmento)
                    print 'Copiado a Edna!'


    def get_segmentos(self, cli_args):

        segmentos = Segmento.objects.filter(
            medio__nombre__contains='[CHL]',
            tipo__nombre='Tanda',
        ).order_by('medio', 'desde')

        # DESDE
        try:
            desde = datetime.strptime(cli_args['desde'] or '2016-03-01 00:00',
                                      DATE_FORMAT)
        except ValueError, e:
            exit(e.message)
        else:
            segmentos = segmentos.filter(desde__gte=desde)


        # HASTA
        try:
            hasta = datetime.strptime(cli_args['hasta'] or '2016-04-01 00:00',
                                      DATE_FORMAT)
        except ValueError, e:
            exit(e.message)
        else:
            segmentos = segmentos.filter(hasta__lt=hasta)


        # ID
        if cli_args['id']:
            segmentos = segmentos.filter(id=cli_args['id'])


        # MEDIO
        if cli_args['medio']:
            segmentos = segmentos.filter(medio__nombre__icontains=cli_args['medio'])

        return segmentos

class FileTransferGenius(object):
    """Ayuda para mover archivos entre servidores"""

    @staticmethod
    def move_file(source, dest):
        """Mover source to dest,
        donde source y dest pueden estar en distintas máquinas"""

        result = False
        with NamedTemporaryFile() as tmpfile:
            try:
                tmpfile.write(urllib2.urlopen(source).read())
                tmpfile.flush()
            except urllib2.HTTPError:
                pass
            else:
                basename = os.path.basename(source)
                scp_cmd = 'scp {} {}/{}'.format(tmpfile.name, dest, basename)
                status_1, output_1 = commands.getstatusoutput(scp_cmd)
                if status_1 == 0:
                    chmod_cmd =  ('ssh infoxel@edna.infoad.tv chmod 777 '
                                 '/var/www/multimedia/ar/TandasChile/{}'.format(basename))
                    status_2, output_2 = commands.getstatusoutput(chmod_cmd)
                    result = status_2 == 0
        return result



FileTransferGenius = FileTransferGenius()
