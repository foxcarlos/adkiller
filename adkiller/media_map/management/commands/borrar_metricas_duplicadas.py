# -*- coding: utf-8 -*-
"""
Borrar métricas duplicadas.

Esto es un oneshot, no debería ser normal correr este script todo el tiempo
"""

from django.core.management.base import BaseCommand
from django.db.models import Count
from django.db.models import Min
from django.db.models import Max


from adkiller.control.models import MetricaDiariaUser
from adkiller.control.models import Navigation


                                              # pylint: disable=unused-argument
class Command(BaseCommand):
    """..."""
    def handle(self, *args, **options):
        """..."""
        total_metricas = self.eliminar_metricas_repetidas()
        print 'Borre {} métricas repetidas'.format(total_metricas)
        total_navigations = self.eliminar_navigations_repetidas()
        print 'Borre {} navigations repetidas'.format(total_navigations)
        print 'Colapsando navigations coincidentes'
        total_colapsadas = self.colapsar_navigations_coincidentes()
        print 'Colapsé {} navigations'.format(total_colapsadas)

    @staticmethod
    def eliminar_metricas_repetidas():
        """Eliminar metricas repetidas y
        devolver el número total de elementos borrados"""
        repetidos = MetricaDiariaUser.objects.all().order_by(
            'fecha', 'user', 'medio', 'tipo_segmento', 'formato', 'accion'
        ).values(
            'fecha', 'user', 'medio', 'tipo_segmento', 'formato', 'accion'
        ).annotate(cant=Count('id')).filter(cant__gt=1)

        total_borrados = 0
        for repetido in repetidos:
            print
            metricas = MetricaDiariaUser.objects.filter(
                fecha=repetido['fecha'],
                accion=repetido['accion'],
                user=repetido['user'],
                medio=repetido['medio'],
                tipo_segmento=repetido['tipo_segmento'],
                formato=repetido['formato'],
            )
            elegido = metricas[0]

            print ('limpiando duplicados de {m.id} - {m.fecha} {m.user} '
                   '{m.medio} {m.tipo_segmento} {m.formato}'.format(m=elegido))

            ok = 's' # raw_input('ok? (s/n)')
            if ok.lower().startswith('s'):
                for m in metricas:
                    if m.id != elegido.id:
                        m.delete()
                        total_borrados += 1
        return total_borrados

    @staticmethod
    def eliminar_navigations_repetidas():
        """Eliminar navigations repetidas y
        devolver el número total de elementos borrados"""
        repetidos = Navigation.objects.all().order_by(
            'user', 'medio', 'desde_thumb', 'hasta_thumb',
            'inicio', 'fin', 'modo', 'capas_activas',
        ).values(
            'user', 'medio', 'desde_thumb', 'hasta_thumb',
            'inicio', 'fin', 'modo', 'capas_activas',
        ).annotate(cant=Count('id')).filter(cant__gt=1)

        total_borrados = 0
        for repetido in repetidos:
            print
            navigations = Navigation.objects.filter(
                user=repetido['user'],
                medio=repetido['medio'],
                desde_thumb=repetido['desde_thumb'],
                hasta_thumb=repetido['hasta_thumb'],
                inicio=repetido['inicio'],
                fin=repetido['fin'],
                modo=repetido['modo'],
                capas_activas=repetido['capas_activas'],
            )
            elegida = navigations[0]

            print ('limpiando duplicados de {n.id} - {n.medio} {n.user} '
                   '{n.desde_thumb} {n.hasta_thumb}'.format(n=elegida))

            ok = 's' # raw_input('ok? (s/n)')
            if ok.lower().startswith('s'):
                for n in navigations:
                    if n.id != elegida.id:
                        n.delete()
                        total_borrados += 1
        return total_borrados

    @staticmethod
    def colapsar_navigations_coincidentes():
        """Si las métricas tienen ciertos campos en común,
        conservar sólo una de ellas"""
        total_colapsadas = 0

        master_navigations = Navigation.objects.values(
            'user', 'medio', 'inicio'
        ).annotate(
            Min('pk'),
            Max('fin'),
            Min('desde_thumb'),
            Max('hasta_thumb'),
            count=Count('pk')
        ).filter(count__gt=1)

        for mn in master_navigations:
            Navigation.objects.filter(
                user=mn['user'],
                medio=mn['medio'],
                inicio=mn['inicio']
            ).exclude(pk=mn['pk__min']).delete()

            elegida = Navigation.objects.get(pk=mn['pk__min'])
            elegida.desde_thumb = mn['desde_thumb__min']
            elegida.hasta_thumb = mn['hasta_thumb__max']
            elegida.fin = mn['fin__max']
            elegida.save()
            total_colapsadas += 1

        return total_colapsadas

                                              # pylint: enable=unused-argument


