# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use
# pylint: disable=line-too-long
"""
Analizar partido y noticieros del partido "ESTUDIANTES vs. QUILMES
del 17/10.
"""

from collections import namedtuple

from django.core.management import call_command
from django.core.management.base import BaseCommand


evento = namedtuple('evento', ['medio', 'desde', 'hasta'])

# donde se emitió el partido
CANAL9 = 'Canal 9 [ARG]'

# canales de noticias
CANAL7 = 'Canal 7 [ARG]'
CANAL13 = 'Canal 13 [ARG]'
AMERICA = 'América 2 [ARG]'
TELEFE = 'Telefé [ARG]'
TN = 'TN Todo Noticias [ARG]'
A24 = 'América 24 [ARG]'
C5N = ' C5N [ARG]'

# canales deportivos
TYC = 'TyC Sports [ARG]'
ESPN = 'ESPN [ARG]'
DTVSPORT = 'DIRECTV Sport 610 [ARG]'
FOXSPORT = 'Fox Sports [ARG]'

EVENTOS = {

    'partido': [
        evento(medio=CANAL9, desde='2015-10-17 20:20:00', hasta='2015-10-17 22:24:00')
    ],

    'noticieros': [
        # al mediodía
        evento(medio=TELEFE, desde='2015-10-18 12:00:00', hasta='2015-10-18 14:30:00'),
        evento(medio=CANAL9, desde='2015-10-18 12:00:00', hasta='2015-10-18 14:30:00'),
        evento(medio=CANAL7, desde='2015-10-18 12:00:00', hasta='2015-10-18 14:30:00'),
        evento(medio=CANAL13, desde='2015-10-18 12:00:00', hasta='2015-10-18 14:30:00'),
        evento(medio=AMERICA, desde='2015-10-18 12:00:00', hasta='2015-10-18 14:30:00'),

        # a la tarde-noche
        evento(medio=TELEFE, desde='2015-10-18 19:00:00', hasta='2015-10-18 21:00:00'),
        evento(medio=CANAL9, desde='2015-10-18 19:00:00', hasta='2015-10-18 21:00:00'),
        evento(medio=CANAL7, desde='2015-10-18 19:00:00', hasta='2015-10-18 21:00:00'),
        evento(medio=CANAL13, desde='2015-10-18 19:00:00', hasta='2015-10-18 21:00:00'),
        evento(medio=AMERICA, desde='2015-10-18 19:00:00', hasta='2015-10-18 21:00:00'),
    ],

    'canales_deportivos': [
        # todo el lunes
        evento(medio=TYC, desde='2015-10-17 22:00:00', hasta='2015-10-18 23:00:00'),
        evento(medio=ESPN, desde='2015-10-17 22:00:00', hasta='2015-10-18 23:00:00'),
        evento(medio=DTVSPORT, desde='2015-10-17 22:00:00', hasta='2015-10-18 23:00:00'),
        evento(medio=FOXSPORT, desde='2015-10-17 22:00:00', hasta='2015-10-18 23:00:00'),
    ],

    'canales_de_noticias': [
        # toda la mañana y la siesta
        evento(medio=TN, desde='2015-10-18 06:00:00', hasta='2015-10-18 15:00:00'),
        evento(medio=A24, desde='2015-10-18 06:00:00', hasta='2015-10-18 15:00:00'),
        evento(medio=C5N, desde='2015-10-18 06:00:00', hasta='2015-10-18 15:00:00'),
    ]
}

class Command(BaseCommand):

    def handle(self, *args, **options):
        for key in EVENTOS:
            print key
            for evento in EVENTOS[key]:
                print evento.medio, evento.desde, evento.hasta
                call_command('analizar_eventos', evento.medio,
                                                 evento.desde,
                                                 evento.hasta,
                                                 'DIRECTV')

