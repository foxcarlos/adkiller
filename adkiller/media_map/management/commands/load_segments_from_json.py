# -*- coding: utf-8 -*-
# pylint: disable=import-error, no-name-in-module, missing-docstring, no-member
# pylint: disable=no-init, too-few-public-methods, no-self-use,
# pylint: disable=too-many-locals, unused-argument
"""
Cargar segmentos desde un archivo json y escribir su contenido en la BBDD
"""

# python import
import json
import os
from datetime import datetime, timedelta

# django imports
from django.core.management.base import BaseCommand
from django.conf import settings

# projects import
from adkiller.app.models import Marca, Medio
from adkiller.media_map.models import Segmento, JsonMigrado


class Command(BaseCommand):
    def handle(self, *args, **options):
        if not os.path.exists(settings.BRANDING_PARSED_JSON_FILES):
            os.makedirs(settings.BRANDING_PARSED_JSON_FILES)

        for basename in os.listdir(settings.BRANDING_JSON_FILES):
            jsonfile = os.path.join(settings.BRANDING_JSON_FILES, basename)

            if not basename.endswith('.json'):

                continue
            if JsonMigrado.objects.filter(nombre=basename):
                continue

            print 'Migrando {}'.format(basename)

            with open(jsonfile, 'r') as jf:
                data = json.loads(jf.read())

            dispositivo, camara = data['metadata']['medio']
            inicio_video = datetime.strptime(
                data['metadata']['video_init_datetime'],
                data['metadata']['datetime_format']
            )

            for marca in data['brands']:
                # hardcodeado
                if marca.lower() != 'exo':
                    continue
                marca_infoad = Marca.objects.get(nombre='Exo')
                medio_infoad = Medio.objects.get(dvr=dispositivo, canal=camara)
                for s1, s2 in data[marca]:
                    desde = inicio_video + timedelta(seconds=s1)
                    hasta = inicio_video + timedelta(seconds=s2)

                    # crear segmento
                    segmento = Segmento.objects.create(
                        medio=medio_infoad,
                        marca=marca_infoad,
                        desde=desde,
                        hasta=hasta
                    )
                    print segmento

            # recordar migración
            JsonMigrado.objects.create(nombre=basename)

            # mover json a otra carpeta
            dest = os.path.join(settings.BRANDING_PARSED_JSON_FILES, basename)
            cmd = 'mv {} {}'.format(jsonfile, dest)
            os.system(cmd)

