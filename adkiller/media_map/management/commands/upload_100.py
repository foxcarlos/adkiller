# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-docstring
# pylint: disable=unused-argument, no-self-use, too-many-arguments
"""
Generar "tandas" para el 100% del material de un medio

Surgió como script para levantar el 100% del material de sandmouse,
pero la idea es poder generalizarlo para subir el 100% del material
de cualquier medio que no se pueda cortar de forma automática
"""

import os
from datetime import datetime
from datetime import timedelta
from simplejson.scanner import JSONDecodeError

from adkiller.app.models import Medio
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento

# django imports
from django.core.management.base import BaseCommand

TIPO_MATERIAL, _ = TipoSegmento.objects.get_or_create(nombre='Material')
TIPO_TANDA, _ = TipoSegmento.objects.get_or_create(nombre='Tanda')
DESCRIPCION_ESTANDAR = 'Segmento creado por {}'.format(os.path.basename(__file__))
RETROACTIVO = timedelta(minutes=20)

class Regla(object):
    """Clase para decidir cuándo y cómo crear
    un segmento para subir el 100% del medio"""

    def __init__(self, duracion, horarios_excluidos, destdir, tpa, tipo):
        self.horarios_excluidos = horarios_excluidos
        self.duracion = duracion - timedelta(seconds=1)
        self.destdir = destdir
        self.tpa = tpa
        self.tipo_segmento = tipo

    def get_ultimo_segmento(self, medio):
        """Devolver el último segmento con estas características"""

        segmento = Segmento.objects.filter(
            tipo=self.tipo_segmento,
            medio=medio,
        ).order_by('-hasta')[:1]

        if segmento:
            ultimo = segmento[0]
        else:
            # fake it!
            hasta = datetime.now() - self.duracion - 2 * RETROACTIVO
            ultimo = Segmento(
                desde=hasta - self.duracion,
                hasta=hasta,
                medio=medio,
                tipo=self.tipo_segmento
            )
        return ultimo

    def fragmentar_tiempo(self, medio):
        """Devolver lista de (desde, hasta)"""

        result = []

        # buscar la última con estas características
        ultimo = self.get_ultimo_segmento(medio)
        print ('Último segmento: {:%Y-%m-%d %H:%M:%S} a '
               '{:%Y-%m-%d %H:%M:%S}'.format(ultimo.desde, ultimo.hasta))

        current = ultimo.hasta + timedelta(seconds=1)
        retroactivo = RETROACTIVO
        if medio.dvr == 'Maggie':
            retroactivo = timedelta(hours=6)

        while current < datetime.now() - retroactivo:

            desde = current
            for i_exc, f_exc in self.horarios_excluidos:
                # i_exc: inicio del horario excluido
                # f_exc: fin    del horario excluido
                if i_exc <= desde.strftime('%H:%M') < f_exc:
                    # esta regla de exclusión incluye el 'inicio' del segmento
                    # debemos avanzar el 'desde' hasta el final del tiempo excluido
                    desde = datetime.strptime(desde.strftime('%Y-%m-%d ') + f_exc,
                                              '%Y-%m-%d %H:%M')
                    # no necesito controlar otros horarios excluidos
                    break

            hasta = desde + self.duracion
            for i_exc, f_exc in self.horarios_excluidos:
                # i_exc: inicio del horario excluido
                # f_exc: fin    del horario excluido
                if i_exc < hasta.strftime('%H:%M') <= f_exc:
                    # esta regla de exclusión incluye el 'hasta' del segmento
                    # debemos retroceder el 'hasta' hasta el inicio del tiempo excluido
                    hasta = datetime.strptime(hasta.strftime('%Y-%m-%d ') + i_exc,
                                              '%Y-%m-%d %H:%M')
                    # no necesito controlar otros horarios excluidos
                    break

            if hasta < datetime.now() - retroactivo:
                if hasta != desde:
                    result.append((desde, hasta))

            current = hasta + timedelta(seconds=1)

        return result

    def cortar_segmento(self, medio, desde, hasta):
        """Cortar segmento"""
        segmento = Segmento(
            medio=medio,
            desde=desde,
            hasta=hasta,
            tipo=self.tipo_segmento,
            descripcion=DESCRIPCION_ESTANDAR,
        )
        # comento snap to origins para probar: 18-04-2016
        # segmento.snap_to_origins()
        # asegurarse que no solape a otro segmento ya creado
        solapados = segmento.superpuestos().order_by('-hasta')
        if solapados:
            segmento.desde = solapados[0].hasta + timedelta(seconds=1)

        try:
            segmento.save()
        except JSONDecodeError:
            segmento.delete()
            segmento = None
        else:
            if segmento.tipo != TIPO_TANDA:
                # las tandas se cortan automáticamente
                try:
                    segmento.make_clip(tpa=self.tpa, dest=self.destdir)
                except JSONDecodeError:
                    segmento.delete()
                    segmento = None
        return segmento

    def aplicar(self, medio):
        """Aplicar los procesos definidos por esta regla"""
        fragmentos = self.fragmentar_tiempo(medio)
        for inicio, fin in fragmentos:
            print 'de {} a {}'.format(inicio, fin)
            segmento = self.cortar_segmento(medio, inicio, fin)
            if segmento:
                print segmento
            else:
                print 'Problemas al generar el segmento'


###############################################################################
# REGLAS
###############################################################################

"""
Definí tus reglas aquí:

Ejemplo:
    regla_soundmouse = Regla(
        duracion=timedelta(hours=1),
        horarios_excluidos=[('03:00', '07:30'), ('13:30', '13:40')],
        desdtir='SM',
        tpa=False,
        tipo=TIPO_MATERIAL
    )
"""


regla_soundmouse = Regla(
    duracion=timedelta(hours=1),
    horarios_excluidos=[],
    destdir='SM',
    tpa=True,
    tipo=TIPO_MATERIAL
)

regla_tandas_a_edna = Regla(
    duracion=timedelta(minutes=10),
    horarios_excluidos=[],
    destdir='T',
    tpa=True,
    tipo=TIPO_TANDA
)

regla_play_town = Regla(
    duracion=timedelta(minutes=10),
    horarios_excluidos=[('00:00', '00:55'), ('02:05', '23:59')],
    destdir='PT',
    tpa=True, # TODO: cambiar esto
    tipo=TIPO_MATERIAL
)

REGLAS = {
    u'A&E [ARG]': [regla_tandas_a_edna],
    u'AMC [LAT]': [regla_tandas_a_edna],
    u'ATV Perú': [regla_soundmouse],
    u'AXN [ARG]': [regla_tandas_a_edna],
    u'America TV Canal 4 [PER]': [regla_soundmouse],
    u'América 2 [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'América 24 [ARG]': [regla_tandas_a_edna],
    u'Animal Planet [ARG]': [regla_tandas_a_edna],
    u'C5N [ARG]': [regla_tandas_a_edna],
    u'Canal 10 Córdoba [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Canal 12 Córdoba [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Canal 13 [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Canal 26 [ARG]': [regla_tandas_a_edna],
    u'Canal 7 [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Canal 8 Córdoba [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Canal 9 [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'Cartoon Network [ARG]': [regla_tandas_a_edna],
    u'Cinecanal [ARG]': [regla_tandas_a_edna],
    u'Cinemax [LAT]': [regla_tandas_a_edna],
    u'Chilevisión [CHL]': [regla_soundmouse],
    u'CN23 [ARG]': [regla_tandas_a_edna],
    u'Crónica TV [ARG]': [regla_play_town, regla_tandas_a_edna],
    u'Discovery Channel [ARG]': [regla_tandas_a_edna],
    u'Discovery H&H [ARG]': [regla_tandas_a_edna],
    u'Discovery Kids [ARG]': [regla_tandas_a_edna],
    u'Disney Channel [ARG]': [regla_tandas_a_edna],
    u'Disney XD [ARG]': [regla_tandas_a_edna],
    u'E! Entertainment [ARG]': [regla_tandas_a_edna],
    u'ESPN + [LAT]': [regla_tandas_a_edna],
    u'ESPN 2 [ARG]': [regla_tandas_a_edna],
    u'ESPN 3 [LAT]': [regla_tandas_a_edna],
    u'ESPN [ARG]': [regla_tandas_a_edna],
    u'FX [ARG]': [regla_tandas_a_edna],
    u'Fox Life [LAT]': [regla_tandas_a_edna],
    u'Fox Sports 2 [LAT]': [regla_tandas_a_edna],
    u'Fox Sports [ARG]': [regla_tandas_a_edna],
    u'Fox [LATE]': [regla_tandas_a_edna],
    u'Glitz [ARG]': [regla_tandas_a_edna],
    u'El Gourmet.com [ARG]': [regla_tandas_a_edna],
    u'Europa Europa [ARG]': [regla_tandas_a_edna],
    u'History [ARG]': [regla_tandas_a_edna],
    u'I-SAT [ARG]': [regla_tandas_a_edna],
    u'Investigation Discovery [ARG]': [regla_tandas_a_edna],
    u'Lifetime [LATAM]': [regla_tandas_a_edna],
    u'Magazine [ARG]': [regla_tandas_a_edna],
    u'Mega [CHL]': [regla_soundmouse],
    u'Monte Carlo TV [UY]': [regla_soundmouse, regla_tandas_a_edna],
    u'Nat Geo [LATE]': [regla_tandas_a_edna],
    u'Quiero! [LAT]': [regla_tandas_a_edna],
    u'Red TV Chile [CHL]': [regla_soundmouse],
    u'Sony [ARG]': [regla_tandas_a_edna],
    u'Space [ARG]': [regla_tandas_a_edna],
    u'Studio Universal [ARG]': [regla_tandas_a_edna],
    u'TBS [ARG]': [regla_tandas_a_edna],
    u'TLC [ARG]': [regla_tandas_a_edna],
    u'TN Todo Noticias [ARG]': [regla_tandas_a_edna],
    u'TNT [LATE]': [regla_tandas_a_edna],
    u'TNT Series [LATAM]': [regla_tandas_a_edna],
    u'TVN [CHL]': [regla_soundmouse],
    u'Teledoce [UY]': [regla_soundmouse, regla_tandas_a_edna],
    u'Telefé [ARG]': [regla_soundmouse, regla_tandas_a_edna],
    u'The Film Zone [LAT]': [regla_tandas_a_edna],
    u'TyC Sports [ARG]': [regla_tandas_a_edna],
    u'TCM [ARG]': [regla_tandas_a_edna],
    u'UCTV Canal 13 [CHL]': [regla_soundmouse],
    u'Universal Channel [ARG]': [regla_tandas_a_edna],
    u'Volver [ARG]': [regla_tandas_a_edna],
    u'Warner [ARG]': [regla_tandas_a_edna],
}


class Command(BaseCommand):

    def handle(self, *args, **options):
        medios = Medio.objects.filter(soporte__nombre__icontains='tv')
        for medio in medios:
            if REGLAS.has_key(medio.nombre):
                print '\n-----------------------------------------------'
                print medio.nombre
                for regla in REGLAS[medio.nombre]:
                    regla.aplicar(medio)
