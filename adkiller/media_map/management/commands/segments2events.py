# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=unused-argument, line-too-long
"""
Generar eventos a partir de los segmentos de tipo 5
"""

# django imports
from django.core.management.base import BaseCommand

# project imports
from adkiller.media_map.models import Evento, Segmento, BrandModel
from adkiller.engines_cache.models import Chunk

class Command(BaseCommand):

    def handle(self, *args, **options):
        try:
            MAX = int(args[0])
        except IndexError:
            MAX = 1
        segmentos = Segmento.objects.filter(tipo__nombre='Evento')

        count = 0
        cree = False
        for segmento in segmentos:
            if count >= MAX:
                break

            if Evento.objects.filter(desde=segmento.desde,
                                     hasta=segmento.hasta,
                                     medio=segmento.medio).exists():
                continue
            else:
                cree = True
                count += 1
                print
                print 'desde:', segmento.desde
                print 'hasta:', segmento.hasta
                print 'medio:', segmento.medio
                print 'marcas:', ', '.join([m.nombre for m in segmento.marcas.all()])


                e = Evento.objects.create(desde=segmento.desde, hasta=segmento.hasta,
                                          medio=segmento.medio)
                for m in segmento.marcas.all():
                    e.marcas.add(m)
                brandmodels = BrandModel.get_models_from_marcas(e.marcas.all())
                Chunk.factory(e, e.medio, e.desde, e.hasta, brandmodels)



        print
        if not cree:
            print 'Nada que crear'
        else:
            print 'fin del script'

