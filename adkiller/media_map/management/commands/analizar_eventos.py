# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=line-too-long, too-many-locals, too-many-statements
"""
Analizar eventos automáticamente usando
el motor de detección visual (brandtrack)
"""

# python imports
import requests
import numpy as np
from collections import defaultdict
from datetime import datetime
from datetime import timedelta
from optparse import make_option

# django imports
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings

# project imports
from adkiller.app.models import Medio
from adkiller.app.models import Marca
from adkiller.media_map.models import BrandModel
from adkiller.media_map.models import BoundingBox
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento
from adkiller.media_map.utils import DateRange
from adkiller.media_map.utils import eliminar_detecciones_aisladas
from adkiller.media_map.utils import scores_to_matches_index
from adkiller.media_map.utils import pairwise



class Command(BaseCommand):
    args = '<medio> <desde> <hasta> <marca1 marca2 ...>'
    help = u"""
        Analizar eventos automáticamente usando
        el motor de detección visual (brandtrack)
        Argumentos posicionales obligatorios:
            - Nombre del medio en InfoAd
            - Desde, formato YYYY-MM-DD
            - Hasta, formato YYYY-MM-DD
            - Lista de marcas, nombre en InfoAd
    """
    FORMATO_FECHA = '%Y-%m-%d %H:%M:%S'
    IMG_TPL = 'http://media.welo.tv/prog_thumbs/{}/{:%Y%m%d}/{:02d}{:02d}/{:03d}.jpg'
    BRANDTRACK_VIEW = 'http://{}/sigmund/analize/'.format(
        settings.BRANDTRACK_SERVER_URL
    )

    timestamps = None

    desde = None
    hasta = None
    medio = None
    models = None
    marcas = None

    option_list = BaseCommand.option_list + (
        make_option(
            '-y',
            action='store_true',
            dest='not_confirm',
            help=u'No pedir confirmación'
        ),
    )

    def handle(self, *args, **options):

        self.parse_and_check_args(args)
        self.timestamps = [dt for dt in DateRange(self.desde,
                                                  self.hasta + timedelta(seconds=1),
                                                  timedelta(seconds=1))]
        self.print_args()

        if not options.get('not_confirm'):
            prompt = '¿Proceder? (s/n) '
            user_response = raw_input(prompt).strip().lower()
            if not user_response.startswith('s'):
                exit(0)

        data = defaultdict(lambda: defaultdict(list))
        # data es un diccionario cuyas claves son las marcas involucradas
        # en el análisis. Los valores de este diccionario son a su vez
        # diccionarios, cuyas claves son datetimes. Los valores de estos
        # sub-diccionarios son listas de bounding boxes. Ejemplo:
        # {
        #   <Marca: Natura Aceites>:
        #     {
        #       datetime.datetime(2015, 9, 22, 10, 5, 15):
        #         [
        #           {
        #            u'x1': 24,
        #            u'y1': 37,
        #            u'score': 1.0000001152350884,
        #            u'x2': 75,
        #            u'model': u'146',
        #            u'y2': 69
        #            },
        #         ],
        #         etc...

        print
        for dt in self.timestamps:
            print '\rAnalizando {:%H:%M:%S}'.format(dt),
            self.stdout.flush()
            boxes = self.analize_thumb(dt) or []
            for box in boxes:
                # voy agrupando los bounding boxes
                # detectados por marca y por datetime
                marca = self.models[box['model']].marca
                data[marca][dt].append(box)

        # crear segmentos
        print
        for marca in data.keys():
            print 'Creando segmentos para', marca
            # crear un arreglo de scores, donde cada índice
            # representa un thumb consultado (un segundo)
            scores = []
            boxes_in_times = defaultdict(list)
            for dt in self.timestamps:
                for bbox in data[marca].get(dt, []):
                    if bbox['score'] > self.models[bbox['model']].score_threshold:
                        boxes_in_times[dt].append(bbox)
                if boxes_in_times.has_key(dt):
                    prom = np.mean([b['score'] for b in boxes_in_times[dt]])
                    scores.append(prom)
                else:
                    scores.append(0.0)

            # eliminar detecciones de esta marca que estén aisladas
            scores = eliminar_detecciones_aisladas(scores)

            # array de booleanos donde se detectó la marca
            matches_index = scores_to_matches_index(np.array(scores))
            print matches_index

            # indices de los comienzos y finales de los matches
            for a, b in pairwise(matches_index):
                desde = self.timestamps[a]
                hasta = self.timestamps[b]
                print 'desde {:%H:%M:%S} hasta {:%H:%M:%S}'.format(desde, hasta)
                segmentos = [
                    Segmento(
                        desde=desde,
                        hasta=hasta,
                        medio=self.medio,
                        marca=marca,
                        tipo=TipoSegmento.objects.get(nombre='Logos')
                    )
                ]
                superpuestos = segmentos[0].superpuestos()
                salvar = True
                for s in superpuestos:
                    if not s.automatico():
                        if s.desde <= segmentos[0].desde and s.hasta >= segmentos[0].hasta:
                            # está contenido, no crearlo
                            salvar = False
                            break
                        elif s.desde <= segmentos[0].desde and s.hasta < segmentos[0].hasta:
                            # el detectado tiene una parte final que queremos conservar
                            segmentos[0].desde = s.hasta
                        elif s.desde > segmentos[0].desde and s.hasta >= segmentos[0].hasta:
                            # el detectado tiene una parte inicial que queremos conservar
                            segmentos[0].hasta = s.desde
                        else:
                            # el detectado contiene al manual. Caso difícil:
                            # hay que crear dos segmentos a los costados y
                            # distribuir los bounding boxes apropiadamente
                            segmentos.append(
                                Segmento(
                                    desde=s.hasta,
                                    hasta=segmentos[0].hasta,
                                    medio=self.medio,
                                    marca=marca,
                                    tipo=TipoSegmento.objects.get(nombre='Logos')
                                )
                            )
                            segmentos[0].hasta = s.desde

                if salvar:
                    for s in segmentos:
                        s.save()
                else:
                    continue
                scores = []
                formato = None
                segmento = segmentos[0]
                for t in self.timestamps[a:b+1]:
                    if t > segmento.hasta:
                        try:
                            segmento = segmentos[1]
                        except IndexError:
                            pass

                    for box in boxes_in_times[t]:
                        bbox = BoundingBox.objects.create(
                                datetime=t,
                                medio=self.medio,
                                model=self.models[box['model']],
                                score=box['score'],
                                x1=box['x1'],
                                y1=box['y1'],
                                x2=box['x2'],
                                y2=box['y2']
                            )
                        bbox.save()
                        segmento.boxes.add(bbox)
                        scores.append(box['score'])
                        if formato is None:
                            formato = self.models[box['model']].get_formato()
                if scores:
                    for s in segmentos:
                        s.score = np.mean(scores)
                        s.save()

                if formato:
                    for s in segmentos:
                        s.formato = formato
                        s.save()

    def parse_and_check_args(self, cli_args):
        """Parsear argumentos del script y chequear
        precondiciones para correr el script"""
        if len(cli_args) < 4:
            exit('Número incorrecto de argumentos. '
                 'Ejecute con -h para obtener ayuda')
        medio, desde, hasta = cli_args[:3]
        #medio = unicode(medio)

        # chequear medio
        try:
            self.medio = Medio.objects.get(nombre=medio)
        except ObjectDoesNotExist:
            exit('No encuentro el medio "{}"'.format(medio))

        if self.medio.mediadelivery_id is None:
            exit('El medio especificado no tiene campo "mediadelivery_id"')

        # chequear fechas
        def str2datetime(f):
            """parsear string y devolver datetime"""
            try:
                return datetime.strptime(f, self.FORMATO_FECHA)
            except ValueError:
                exit('No entiendo la fecha "{}", '
                     'utilice el formato "{}"'.format(f, self.FORMATO_FECHA))

        self.desde, self.hasta = map(str2datetime, (desde, hasta))

        # chequear marca(s)
        self.marcas = []
        for m in cli_args[3:]:
            try:
                self.marcas.append(Marca.objects.get(nombre=m))
            except ObjectDoesNotExist:
                exit('No encuentro la marca "{}"'.format(m))

        # reunir models
        # lo tendremos en un diccionario cuyas claves son los id_brandtrack de
        # los models como string, los
        # valores asociados son los brandmodels mismos, A LOS CUALES LES AGREGO
        # COMO ATRIBUTO LA MARCA
        # {
        #    '130': <brandmodel#141>,
        #    '21':  <brandmodel#120>,
        #    '133': <brandmodel#121>
        # }

        self.models = {}
        for bm in BrandModel.objects.filter(id_brandtrack__isnull=False):
            bm_marca = bm.get_marca()
            for marca in self.marcas:
                if bm_marca and bm_marca == marca:
                    bm.marca = marca
                    self.models[str(bm.id_brandtrack)] = bm
                    # listo este model
                    break


        if not self.models:
            exit('No hay models para las marcas pedidas. Entrene a brandtrack.')

    def analize_thumb(self, dt):
        """Return data produced by brandtrack for certain thumb"""
        result = None
        _dt = dt
        hora = _dt.hour
        minuto = _dt.minute
        segundo = _dt.second
        img_url = self.IMG_TPL.format(self.medio.mediadelivery_id,
                                      _dt,
                                      hora,
                                      minuto / 10 * 10,
                                      (minuto % 10) * 60 + segundo
                                      )
        GET_params = {
            'image': img_url,
            'models': ','.join(self.models.keys())
        }
        request = requests.get(self.BRANDTRACK_VIEW, params=GET_params)

        if request.ok and request.status_code == 200:
            try:
                response = request.json()
            except ValueError:
                print 'Problemas al entender la respuesta de brandtrack'
            if response['success']:
                result = response['data']
            else:
                print response['error_msg']
        else:
            print 'Problemas al comunicarse con brandtrack'

        return result


    def print_args(self):
        """Imprimir argumentos para dejar
        en claro lo que se va a hacer"""
        tpl = ('\n\tAnálisis automático de evento'
               '\n\t-----------------------------'
               '\n\tMEDIO: {}'
               '\n\tDESDE: {}'
               '\n\tHASTA: {}'
               '\n\tMARCAS: {}'
               '\n\t-----------------------------\n')
        marcas = ','.join([m.nombre for m in self.marcas])
        print tpl.format(self.medio, self.desde, self.hasta, marcas)


