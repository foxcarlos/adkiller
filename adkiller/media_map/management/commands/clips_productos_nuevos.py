# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-docstring, no-self-use
# pylint: disable=attribute-defined-outside-init, no-value-for-parameter
# pylint: disable=unused-argument, too-many-branches
# pylint: disable=too-many-locals, unnecessary-lambda, unused-variable

"""
Crear clips de video para productos que se cargan en mediamap
"""

# python imports
from optparse import make_option
from datetime import datetime
from datetime import timedelta
from tempfile import NamedTemporaryFile
import urllib2
import commands
import os
import time

# django imports
from django.core.management.base import BaseCommand
from django.db.models import Q
# from django.core.mail import EmailMessage

# project imports
from adkiller.app.models import Producto
from adkiller.app.models import Patron
from adkiller.app.models import Servidor

RETROACTIVO_MAX_DAYS = 7 # más de eso, no hay orígenes
EDNA_SPOTS_DIR = 'infoxel@edna.infoad.tv:/var/www/multimedia/ar/SpotsMP4'
SPOTS_DIR = '/var/www/multimedia/ar/SpotsMP4'

EDNA = Servidor.objects.get(nombre='Edna')

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--id', help=u'Sólo el producto con este id'),
    )

    def handle(self, *args, **options):
        productos = Producto.objects.filter(
            segmentos__isnull=False,
            creado__gte=datetime.now() - timedelta(days=RETROACTIVO_MAX_DAYS),
        ).filter(
            # no tiene patrones o
            Q(patrones__isnull=True) |
            # tiene patrones sin mp4_filename
            Q(patrones__mp4_filename__isnull=True)
        ).exclude(
            # pero NINGUN patrón tiene mp4_filename
            patrones__mp4_filename__isnull=False
        ).exclude(
            patrones__mp4_filename__gt=''
        ).distinct()
        total = productos.count()
        for index, producto in enumerate(productos, 1):
            print '{} / {} - PRODUCTO "{}"'.format(index, total, producto)
            print '          MARCA', producto.marca
            # si el segmento se usó para entrenar a AdTrack, esperar (al menos
            # una hora para que ya exista el video copado)
            cargado_mmap = producto.segmentos.filter(usado_para_entrenar=True).count() > 0
            reciente = producto.creado >= datetime.now() - timedelta(hours=24)
            detecciones_adtrack = producto.emisiones.filter(audiovisual__score__gt=0).count() > 0
            if cargado_mmap and reciente and not detecciones_adtrack:
                print 'Es cargado por MM, voy a esperar'
                continue

            # fijarse si edna tiene el spot en mp4
            patron_ppal = producto.patron_principal()
            if patron_ppal and patron_ppal.id_en_simpson:
                mp4_filename = 'pub_{}.mp4'.format(patron_ppal.id_en_simpson)
                ssh_cmd = 'ssh infoxel@edna.infoad.tv "ls {}/{}"'.format(SPOTS_DIR, mp4_filename)
                status, _ = commands.getstatusoutput(ssh_cmd)
                if status == 0:
                    # ya se creo el mp4
                    patron = Patron.objects.create(
                        producto=producto,
                        filename=mp4_filename,
                        mp4_filename=mp4_filename,
                        server=EDNA,
                    )
                    print 'Patrón creado'
                    continue

            segmentos = producto.segmentos.all().order_by('desde')
            for segmento in segmentos:
                result = segmento.make_clip()
                if result['success']:

                    # check status url
                    status_url = result['status_url']
                    start = datetime.now()
                    creado = segmento.check_clip_creado(status_url)
                    while not creado and datetime.now() - start < timedelta(minutes=8):
                        print status_url
                        print '\rNo creado, esperando 10 seg',
                        time.sleep(10)
                        creado = segmento.check_clip_creado(status_url)
                    print

                    if not creado:
                        print 'Demasiada espera, no lo creó...'
                    else:
                        print 'creado!'
                        moved_ok = FileTransferGenius.move_file(
                            source=result['url'],
                            dest=EDNA_SPOTS_DIR
                        )
                        if moved_ok:
                            print 'Copiado a Edna!'
                            patron = Patron.objects.create(
                                producto=producto,
                                filename=os.path.basename(result['url']),
                                mp4_filename=os.path.basename(result['url']),
                                server=EDNA,
                            )
                            print 'Patron creado'
                            break

class FileTransferGenius(object):
    """Ayuda para mover archivos entre servidores"""

    @staticmethod
    def move_file(source, dest):
        """Mover source to dest,
        donde source y dest pueden estar en distintas máquinas"""

        result = False
        with NamedTemporaryFile() as tmpfile:
            try:
                tmpfile.write(urllib2.urlopen(source).read())
                tmpfile.flush()
            except urllib2.HTTPError:
                pass
            else:
                basename = os.path.basename(source)
                scp_cmd = 'scp {} {}/{}'.format(tmpfile.name, dest, basename)
                status_1, output_1 = commands.getstatusoutput(scp_cmd)
                if status_1 == 0:
                    chmod_cmd = ('ssh infoxel@edna.infoad.tv chmod 777 '
                                 '/var/www/multimedia/ar/SpotsMP4/{}'.format(basename))
                    status_2, output_2 = commands.getstatusoutput(chmod_cmd)
                    result = status_2 == 0
        return result



FileTransferGenius = FileTransferGenius()
