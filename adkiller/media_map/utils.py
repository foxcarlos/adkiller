# -*- coding: utf-8 -*-
# pylint: disable=too-many-branches
# pylint: disable=line-too-long, too-many-locals, too-many-statements
"""
Funciones útiles para la app media_map
"""

from django.conf import settings

from collections import defaultdict
from datetime import timedelta
from itertools import izip
import numpy as np
import requests
from datetime import datetime

from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento

st = {
    'NTAP': 4,
}

def get_origins_from_welo(mediadelivery_id, desde, hasta):
    """Consultar view de welo y devolver el resultado"""
    result = None
    url = '{}get_origins/'.format(settings.WELO_SERVER)
    data = {
        'ch': mediadelivery_id,
        'd': desde.strftime('%Y-%m-%d %H:%M:%S'),
        'h': hasta.strftime('%Y-%m-%d %H:%M:%S'),
    }
    try:
        request = requests.post(url, params=data)
    except Exception:
        pass
    else:
        if request.ok and request.status_code == 200:
            data = request.json()
            result = data['origins']
            for elem in result:
                elem['start_time'] = datetime.strptime(elem['start_time'], data['dateformat'])
                elem['end_time'] = datetime.strptime(elem['end_time'], data['dateformat'])
    return result

# sólo se usa dentro de este archivo
def boxes_in_times(boxes, date_from):
    """ Organizar boxes en un diccionario de listas donde
    las claves son el segundaje de un datetime """
    result = defaultdict(list)
    for bb in boxes:
        result[int((bb.datetime-date_from).total_seconds())].append(bb)
    return result

# sólo se usa dentro de este archivo
def raw_scores(times, date_from, date_to):
    """devuelve los scores promedio de cada
    segundo (de una lista de boxes de una sola marca)"""

    def get_score(seconds, second_number):
        """Return score for a second"""
        bboxes = seconds[second_number]
        if bboxes:
            return np.mean([bb.score for bb in bboxes])
        else:
            return 0

    seconds = range(int((date_to-date_from).total_seconds()))
    return np.array([(s, get_score(times, s)) for s in seconds])


# se utiliza aquí y en command analizar_eventos
def pairwise(iterable):
    "s -> (s0,s1), (s2,s3), (s4, s5), ..."
    a = iter(iterable)
    return izip(a, a)


def segments_from_boxes(boxes, date_from, date_to):
    """Convert a dictionary where keys are frame numbers and
    values are scores to a list of time intervals where
    the brand was matched on some video"""

    # organizar los bounding boxes por tiempo:
    # times es un diccionario donde cada clave es un entero
    # (cantidad de segundos desde (date_from) en el que se encuentra el bbox)
    times = boxes_in_times(boxes, date_from)
    scores = raw_scores(times, date_from, date_to)


    # first of all, filter scores that are isolated
    for i in range(1, scores.shape[0] - 1):
        if np.sum(scores[i-1:i+2, 1]) == scores[i, 1]:
            scores[i, 1] = 0


    ntap = st['NTAP']
    scores[:, 1] = np.convolve(scores[:, 1],
                               np.ones(ntap)/float(ntap),
                               'same')

    score_threshold = 0.018


    matches = (scores[:, 1] > score_threshold)
    # prepend false value to avoid starting in a true state
    matches = np.hstack([np.array([False]), matches])
    # append false value to avoid loosing last interval
    matches = np.hstack([matches, np.array([False])])

    # r-shift + XOR
    matches_index = np.where(matches[:-1] ^ matches[1:])[0]


    for start, end in pairwise(matches_index):
        starttime = date_from + timedelta(seconds=start)
        endtime = date_from + timedelta(seconds=end)
        score = np.average(scores[start:end, 1])

        yield (starttime,
               endtime,
               score,
               [times[i] for i in range(start, end+1)])


def eliminar_detecciones_aisladas(scores):
    """Scores es una lista de números flotantes.
    Se convierten a cero los valores que estén rodeados de ceros.
    """

    # chequear esto, no corrige valores aislados en el primer o último
    # casillero
    for i in range(1, len(scores) - 1):
        if np.sum(scores[i-1:i+2]) == scores[i]:
            scores[i] = 0

    return scores

def suavizar_scores(scores):
    """Aplicar tratamiento matemático a los scores para
    suavizar la curva"""
    ntap = st['NTAP']
    scores = np.convolve(scores, np.ones(ntap)/float(ntap), 'same')
    return scores


def scores_to_matches_index(scores):
    """Devuelvo un arreglo de booleanos donde scores > 0"""

    # pre: scores es un array de numpy

    if not np.any(scores > 0):
        return np.array([])

    score_threshold = 0.2
    matches = (scores > score_threshold)

    # prepend false value to avoid starting in a true state
    matches = np.hstack([np.array([False]), matches])
    # append false value to avoid loosing last interval
    matches = np.hstack([matches, np.array([False])])

    # r-shift + XOR
    matches_index = np.where(matches[:-1] ^ matches[1:])[0]

    # avoid returning index out of range
    try:
        matches_index[-1] = min(len(scores) - 1, matches_index[-1])
    except IndexError:
        pass
    return matches_index

def create_segments(boxes, medio, marca, date_from, date_to):
    """Recibo una lista de bounding boxes (instancias de infoad)
    un medio, una marca, un desde y un hasta.
    Creo segmentos y les asocio los bounding boxes."""
    segmentos = segments_from_boxes(boxes, date_from, date_to)
    for starttime, endtime, score, list_of_list_of_boxes in segmentos:
        segmento = Segmento.objects.create(desde=starttime,
                        hasta=endtime,
                        medio=medio,
                        marca=marca,
                        score=score,
                        tipo=TipoSegmento.objects.get(id=2)
                        )
        for list_of_boxes in list_of_list_of_boxes:
            for b in list_of_boxes:
                segmento.boxes.add(b)
                b.set_model()
                b.save()


class DateRange(object):
    """Iterator to go through time"""
    def __init__(self, begin, end, step):
        """(begin) and (end) are datetimes,
        (step) is timedelta"""
        self.begin = begin - step
        self.end = end
        self.step = step

    def __iter__(self):
        return self

    def next(self):
        """..."""
        self.begin += self.step
        if self.begin < self.end:
            return self.begin
        else:
            raise StopIteration

def marcas_entrenadas():
    """Devolver lista de marcas entrenadas"""
    result = set()
    segmentos = Segmento.objects.filter(marca__isnull=False,
                                        boxes__usado_para_entrenar=True)
    for segmento in segmentos:
        result.add(segmento.marca)
    return list(result)

def get_thumb_url(medio, date_time):
    """Devolver la url del thumb buscado"""
    URL_TPL = 'http://media.welo.tv/prog_thumbs/{}/{:%Y%m%d}/{:02d}{:02d}/{:03d}.jpg'
    hora = date_time.hour
    minuto = date_time.minute
    segundo = date_time.second
    return URL_TPL.format(medio.mediadelivery_id,
                          date_time,
                          hora,
                          minuto / 10 * 10,
                          (minuto % 10) * 60 + segundo)

