# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring
# pylint: disable=line-too-long
"""
Formularios para la app cache
"""

from django import forms

from adkiller.media_map.models import Evento
from adkiller.media_map.utils import marcas_entrenadas
from adkiller.app.models import Medio
from adkiller.app.models import Marca

class EventoForm(forms.ModelForm):

    medio = forms.ModelChoiceField(queryset=Medio.objects.filter(mediadelivery_id__isnull=False))
    marcas = forms.ModelMultipleChoiceField(
        queryset=Marca.objects.filter(pk__in=[m.id for m in marcas_entrenadas()]),
        required=True
    )

    def clean_hasta(self):
        """Chequear que hasta sea posterior a desde"""
        if self.cleaned_data['hasta'] <= self.cleaned_data['desde']:
            raise forms.ValidationError('"Hasta" debe ser posterior a "Desde"')
        return self.cleaned_data['hasta']

    class Meta:
        model = Evento


