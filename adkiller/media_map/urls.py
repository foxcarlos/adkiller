# -*- coding: utf-8 -*-
# pylint: disable=import-error
"""
Urls para app media_map
"""

from django.conf.urls import patterns, url

from adkiller.control.views import add_navigation
from adkiller.media_map.views import add_box_in_segment
from adkiller.media_map.views import add_product
from adkiller.media_map.views import add_segment
from adkiller.media_map.views import analyze
from adkiller.media_map.views import autocomplete
from adkiller.media_map.views import cargar_patron_audio
from adkiller.media_map.views import channel
from adkiller.media_map.views import delete_detected_box
from adkiller.media_map.views import export_ocr_annotations
from adkiller.media_map.views import export_sponsoring
from adkiller.media_map.views import get_audio
from adkiller.media_map.views import get_brand_models
from adkiller.media_map.views import get_segments
from adkiller.media_map.views import get_video
from adkiller.media_map.views import get_preview
from adkiller.media_map.views import make_query_on_radiodb
from adkiller.media_map.views import next_segment
from adkiller.media_map.views import remove_box
from adkiller.media_map.views import remove_segment
from adkiller.media_map.views import train_box
from adkiller.media_map.views import train_segment
from adkiller.media_map.views import update_segment
from adkiller.media_map.views import view_clip
from adkiller.media_map.views import view_mediamap
from adkiller.media_map.views import get_format_by_product


urlpatterns = patterns(
    '',
    url(r'^get_segments/', get_segments),
    url(r'^add_segment/', add_segment),
    url(r'^update_segment/', update_segment),
    url(r'^export_sponsoring', export_sponsoring),
    url(r'^export_ocr_annotations', export_ocr_annotations),
    url(r'^remove_segment/', remove_segment),
    url(r'^next_segment/', next_segment),
    url(r'^train_segment/', train_segment),
    url(r'^get_audio/', get_audio),
    url(r'^get_video/', get_video),
    url(r'^get_preview/', get_preview),
    url(r'^get_format_by_product/', get_format_by_product),


    # Boxes
    url(r'^add_box_in_segment/', add_box_in_segment),
    url(r'^remove_box/', remove_box),
    url(r'^train_box/', train_box),
    url(r'^get_brand_models/', get_brand_models),
    url(r'^delete_detected_box/', delete_detected_box),
    url(r'^analyze/', analyze),

    # Navigation
    url(r'^add_navigation/', add_navigation),


    url(r'^autocomplete/', autocomplete),
    url(r'^add_product/', add_product),

    url(r'^channel/', channel),
    url(r'^view/', view_mediamap),

    url(r'^make_radio_query/',
        make_query_on_radiodb,
        name="make_query_on_radiodb"),

    url(r'^clips/(?P<clip_id>.+)/', view_clip),

    # entrenar motor de audio
    url(r'^cargar_patron_audio/', cargar_patron_audio),
)
