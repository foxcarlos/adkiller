# -*- coding: utf-8 -*-
# pylint: disable=old-style-class, super-on-old-class, line-too-long
# pylint: disable=unused-argument, fixme, deprecated-lambda
# pylint: disable=attribute-defined-outside-init, too-many-locals
# pylint: disable=too-many-public-methods, broad-except
"""
Models for media_map app
"""

# python imports
import requests
from datetime import timedelta
import thread
import httplib2
import sys

# project imports
from adkiller.app.models import Medio, Marca, Producto, Sector, Emision, TipoPublicidad
from adkiller.app.models import AudioVisual
from adkiller.media_map.exceptions import SegmentoTipoIncorrecto
from adkiller.media_map.exceptions import ErrorAlEntrenarSegmento

# django imports
from django.contrib.auth.models import User
from django.conf import settings
from django.core.management import call_command
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.db.models.signals import pre_delete, post_delete, post_save
from django.dispatch import receiver


DIFERENCIA_C_WELO = timedelta(hours=3)

class BrandModel(models.Model):
    """Representación de un model para detección en brandtrack"""
    #marca = models.ForeignKey(Marca, related_name='models', null=True)
    score_threshold = models.FloatField(null=True, blank=True, default=0.01)
    id_brandtrack = models.IntegerField(null=True, blank=True)
    definitivo = models.BooleanField(default=False)

    @staticmethod
    def get_models_from_marcas(marcas):
        """Devolver lista de models usados para reconocer (marcas)"""
        try:
            _ = marcas[0]
        except IndexError:
            # no hay marcas
            return []
        except TypeError:
            marcas = [marcas]

        segmentos = Segmento.objects.filter(marca__in=marcas,
                                            boxes__usado_para_entrenar=True)

        result = set()
        for segmento in segmentos:
            for box in segmento.boxes.filter(usado_para_entrenar=True):
                result.add(box.model)
        return list(result)

    def get_formato(self):
        """Obtener formato de este brandmodel"""
        if not self.boxes.count():
            return None
        try:
            b = self.boxes.filter(
                usado_para_entrenar=True,
                segmentos__formato__isnull=False
            ).order_by('segmentos__desde')[0]
        except IndexError:
            return None
        s = b.segmentos.filter(formato__isnull=False)
        if not s:
            return None
        return s[0].formato

    def get_marca(self):
        """Obtener marca de este brandmodel"""
        if not self.boxes.count():
            return None
        try:
            b = self.boxes.filter(segmentos__marca__isnull=False)[0]
        except IndexError:
            return None
        s = b.segmentos.filter(marca__isnull=False)
        if not s:
            return None
        return s[0].marca

    def get_producto(self):
        """Obtener producto asociado a este brandmodel"""
        if not self.boxes.count():
            return ''
        b = self.boxes.all()[0]
        s = b.segmentos.filter(producto__isnull=False)
        if not s:
            return ''
        return s[0].producto.nombre

    def get_medio(self):
        """Obtener medio asociado a este brandmodel"""
        if not self.boxes.count():
            return ''
        b = self.boxes.all()[0]
        s = b.segmentos.filter(tipo__nombre='Tanda')
        if not s:
            return ''
        return s[0].medio.nombre

    def get_images_urls(self):
        """Devolver json con imagenes activas e inactivas"""
        server = settings.BRANDTRACK_SERVER_URL
        url = 'http://{}/lerner/view_crop/{}/?json=1'.format(server, self.id_brandtrack)
        request = requests.get(url)
        try:
            result = request.json()
        except ValueError:
            result = {'success': False}
        else:
            result['success'] = True
        bt_url = 'http://' + server
        if 'activas' in result:
            result['activas'] = map(lambda x: bt_url + x, result['activas'])
        if 'inactivas' in result:
            result['inactivas'] = map(lambda x: bt_url + x, result['inactivas'])

        return result

    def __unicode__(self):
        result = u'Brandtrack model #{}'.format(self.id_brandtrack)
        boxes = self.boxes.all()
        if boxes:
            segmentos = boxes[0].segmentos.all()
            if segmentos:
                con_producto = segmentos.filter(producto__isnull=False)
                con_marca = segmentos.filter(marca__isnull=False)
                ifep = segmentos.filter(tipo__nombre='Tanda')
                if con_producto:
                    producto = con_producto[0].producto.nombre
                    result = u'Patrón de producto ({})'.format(producto)
                elif con_marca:
                    marca = con_marca[0].marca
                    result = u'Patrón esponsoreo ({})'.format(marca)
                elif ifep:
                    medio = ifep[0].medio
                    pi, _ = medio.get_patrones_inicio_y_fin()
                    inicios_id = [p.id_brandtrack for p in pi]
                    tipo = 'INICIO' if self.id_brandtrack in inicios_id else 'FIN'
                    result = u'PATRÓN {} espacio publicitario ({})'.format(tipo, medio)
        return result


class BoundingBox(models.Model):
    """Representación de un recuadro señalado en una imagen,
    ya sea de detección automática o añadido manualmente"""
    datetime = models.DateTimeField(db_index=True)
    medio = models.ForeignKey(Medio, related_name='boxes')
    model = models.ForeignKey(BrandModel, null=True, blank=True, related_name='boxes')
    score = models.FloatField(null=True, blank=True)
    x1 = models.FloatField()
    y1 = models.FloatField()
    x2 = models.FloatField()
    y2 = models.FloatField()
    usado_para_entrenar = models.BooleanField(default=False)

    # el siguiente campo es usado para señalar que el bounding box fue creado
    # por el motor (detectado automáticamente) y un humano eliminó el segmento
    # que lo contenía (falso postivo)
    eliminado = models.BooleanField(default=False)

    def eliminar_fp_en_brandtrack(self):
        """Llamar a brandtrack para informar que soy
        un falso positivo y que debería dejar de reconocerme"""
        url_template = ('http://{server}/lerner/remove_fp/?'
                        'model={model}&'
                        'image={image}&'
                        'coords={coords}')
        url = url_template.format(
            server=settings.BRANDTRACK_SERVER_URL,
            model=self.model.id_brandtrack,
            image=self.get_image(),
            coords=','.join(self.get_coords()),
        )
        print 'Eliminando falso positivo'
        print url
        r = requests.get(url)
        try:
            result = r.json()
        except ValueError:
            return False
        else:
            print r
            return result['success']

    def eliminar(self):
        """En ciertos casos no queremos borrar de la base
        de datos un bounding box, por ejemplo, si es un
        falso positivo de brandtrack, preferimos MARCARLO como
        eliminado pero conservarlo para fines estadísticos."""

        # si tengo model, pero no fui usado para entrenar,
        # es porque fui creado como consecuencia de un análisis
        # automático hecho con brandtrack
        if self.model is not None and not self.usado_para_entrenar:
            thread.start_new_thread(self.eliminar_fp_en_brandtrack, ())
            self.eliminado = True
            self.save()
            for segmento in self.segmentos.all():
                segmento.boxes.remove(self)
        else:
            # fue cargado manualmente
            self.delete()


    def get_coords(self):
        """lista de coordenadas, como strings"""
        return [str(x) for x in (self.x1, self.y1, self.x2, self.y2)]


    def get_size(self):
        """Area del bounding box, es un porcentaje del total de la imagen"""
        return (self.x2-self.x1)*(self.y2-self.y1)/100

    def to_json(self):
        """Serializar los datos de este boundingbox
        en un diccionario para enviar como json"""
        return dict(
            id=self.id,
            datetime=self.datetime + DIFERENCIA_C_WELO,
            x1=self.x1,
            y1=self.y1,
            x2=self.x2,
            y2=self.y2,
            trained=self.usado_para_entrenar,
        )

    def get_models(self):
        """Devolver una lista con los posibles models
        en que podría llegar a reconocerse este crop.
        Se supone que se está queriendo entrenar a brandtrack
        con este crop, y queremos mandarle a brandtrack los otros
        models de la misma marca/producto/tipo/etc.,
        para ver si está relacionado"""
        s = self.segmentos.all()[0]
        if s.tipo.nombre == 'Tanda':
            # si es un patrón de inicio o fin de espacio publicitario,
            # no queremos que pueda caer en el model equivocado, así
            # que devolvemos lista vacía y brandtrack lo va a meter
            # en un nuevo model
            return []

        q = Q(boxes__segmentos__tipo=s.tipo)
        if s.marca_id:
            q = q & Q(boxes__segmentos__marca=s.marca)
        if s.producto_id:
            q = q & Q(boxes__segmentos__producto=s.producto)
        if s.sector_id:
            q = q & Q(boxes__segmentos__sector=s.sector)
        if s.formato_id:
            q = q & Q(boxes__segmentos__formato=s.formato)
        return BrandModel.objects.filter(q).distinct()

    def get_image(self):
        """Obtener url de la imagen donde aparece este box"""
        URL_TPL = 'http://media.welo.tv/prog_thumbs/{}/{:%Y%m%d}/{:02d}{:02d}/{:03d}.jpg'
        hora = self.datetime.hour
        minuto = self.datetime.minute
        segundo = self.datetime.second
        return URL_TPL.format(self.medio.mediadelivery_id,
                              self.datetime,
                              hora,
                              minuto / 10 * 10,
                              (minuto % 10) * 60 + segundo)


    def train(self):
        """Entrenar a brandtrack con este crop"""

        # buscamos models de la misma marca o producto, o sector o tipo
        # puede ser un queryset vacío
        bt_models = self.get_models()

        image = self.get_image()
        if not image:
            return {
                'success': False,
                'error_msg': 'Este box no tiene imagen!'
            }

        url_template = ('http://{server}/lerner/add_crop/?'
                        'image={image}&'
                        'coords={coords}&'
                        'models={models}&'
                        'id={crop_id}')
        url = url_template.format(
            server=settings.BRANDTRACK_SERVER_URL,
            image=image,
            coords=','.join(self.get_coords()),
            models=','.join([str(m.id_brandtrack) for m in bt_models]),
            crop_id=self.id
        )
        r = requests.get(url)
        print url, r
        result = r.json()
        print result
        if result['success']:
            self.model, _ = BrandModel.objects.get_or_create(id_brandtrack=int(result['model']))
            self.usado_para_entrenar = True
            self.save()
            self.model.score_threshold = result['optimum_score']
            self.model.save()
        else:
            # aquí se puede usar result['error_msg'] para informar algo al
            # usuario
            pass

        return result

    def borrar_en_brandtrack(self):
        """Decirle a brandtrack que este crop ya no debe
        ser tenido en cuenta como parte del model en el que está"""
        url_template = 'http://{server}/lerner/delete_crop/?id={crop_id}'
        url = url_template.format(
            server=settings.BRANDTRACK_SERVER_URL,
            crop_id=self.id
        )
        # TODO: robustecer esto: qué pasa si BRANDTRACK_SERVER está caído?
        r = requests.get(url)
        print r

    def automatico(self):
        """Devolver True si este bounding box fue generado automaticamente"""
        return self.model is not None and not self.usado_para_entrenar


@receiver(pre_delete, sender=BoundingBox)
def reentrenar_modelo(sender, instance, **kwargs):
    """Reentrenar modelo en brandtrack al eliminar el crop"""
    if instance.model is not None and instance.usado_para_entrenar:
        instance.borrar_en_brandtrack()

class TipoSegmento(models.Model):
    """Tipo de segmento"""
    nombre = models.CharField(max_length=250)

    def __unicode__(self):
        return self.nombre


class Formato(models.Model):
    """Formato de un segmento"""
    nombre = models.CharField(max_length=250)
    tipo = models.ForeignKey(TipoSegmento)

    def __unicode__(self):
        return self.nombre



class Segmento(models.Model):
    """
    Un segmento es una fracción de emisión
    que presenta algún interés.
    """
    desde = models.DateTimeField(db_index=True, null=True, blank=True)
    hasta = models.DateTimeField(db_index=True, null=True, blank=True)
    medio = models.ForeignKey(Medio, related_name='segmentos')
    marca = models.ForeignKey(Marca, related_name='segmentos', null=True, blank=True)
    marcas = models.ManyToManyField(Marca, related_name='segmentos_marcas', null=True, blank=True)
    producto = models.ForeignKey(Producto, related_name='segmentos', null=True, blank=True)
    sector = models.ForeignKey(Sector, related_name='segmentos', null=True, blank=True)
    formato = models.ForeignKey(Formato, related_name='segmentos', null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)
    boxes = models.ManyToManyField(BoundingBox, null=True, blank=True, related_name="segmentos")
    tipo = models.ForeignKey(TipoSegmento, null=True, blank=True, related_name="segmentos")
    score = models.FloatField(null=True, blank=True)

    user = models.ForeignKey(User, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    # el siguiente campo es usado para señalar que el segmento fue creado
    # por el motor (detectado automáticamente) y un humano lo eliminó (es un
    # falso positivo)
    eliminado = models.BooleanField(default=False)
    emision = models.ForeignKey(Emision, null=True, blank=True, related_name="segmentos")
    clip_creado = models.BooleanField(default=False)
    intentos_de_recreacion = models.IntegerField(default=0, blank=True, null=True)

    # esto indica si el segmento fue usado para entrenar
    usado_para_entrenar = models.BooleanField(default=False)


    def cerrar_tanda(self):
        """Señalar que esta tanda ya está completa
        para que no migre más datos"""
        audiovisuales = []
        if self.tipo.nombre == 'Publicidad':
            ad = self.to_ad()
            tanda = ad.get_tanda_or_segmento()
            if tanda is not None:
                audiovisuales = tanda.get_audiovisuales()
        elif self.tipo.nombre == 'Tanda':
            audiovisuales = self.get_audiovisuales()
        else:
            raise SegmentoTipoIncorrecto

        for au in audiovisuales:
            au.tanda_completa = True
            au.save()


    def get_audiovisuales(self):
        """..."""
        if not self.tipo.nombre == 'Tanda':
            raise SegmentoTipoIncorrecto
        audiovisuales = AudioVisual.objects.filter(
            horario__medio=self.medio,
            fecha__gte=(self.desde - timedelta(days=1)).date(),
            fecha__lte=(self.desde + timedelta(days=1)).date(),
            hora__isnull=False,
            inicio__isnull=False,
        ).extra({
            'i': 'fecha + hora'
        })
        result = []
        for a in audiovisuales:
            inicio = max(0, int(round(a.inicio))) # a veces tienen inicio negativo, pertenece a otra tanda
            comienzo_incluido = self.desde <= (a.i + timedelta(seconds=inicio)) < self.hasta
            if comienzo_incluido:
                result.append(a)
        return result

    @classmethod
    def from_ad(cls, ad):
        """..."""
        seg = Segmento(
            desde=ad.hora_real(),
            hasta=ad.hora_real() + timedelta(seconds=ad.duracion),
            marca=ad.producto.marca,
            producto=ad.producto,
            sector=ad.producto.marca.sector,
            #format=dict(id=ad.producto.formato.id, nombre=ad.producto.formato.nombre),
            descripcion=ad.observaciones,
            id=ad.id,
            tipo_id=1
        )
        return seg

    def to_ad(self):
        """..."""
        ad = AudioVisual(
            producto=self.producto,
            fecha=self.desde.date(),
            hora=self.desde.time(),
            duracion=self.get_duration(),
            usuario=self.user,
        )
        if self.formato_id:
            self.tipo_publicidad = TipoPublicidad.objects.get(
                descripcion=self.formato.nombre
            )
        ad.set_horario(medio=self.medio)
        ad.calcular_tarifa()
        ad.set_origen()  #hay que ver bien esto
        return ad

    def analizar_ifep(self):
        """
        Analizar ifep, sólo para segmentos tipo Bache-IFEP
        """
        if self.tipo.nombre != 'Bache-IFEP':
            print 'Este método sólo se ejecuta en segmentos tipo Bache-IFEP'
            return
        return call_command('ifep',
                            bache_id=self.id,
                            interactive=True)

    def __unicode__(self):
        if self.marca:
            m = self.marca.nombre
        else:
            m = '(sin marca)'
        tpl = (u'{t.nombre} - {s.medio} - {m} - {s.desde} / {s.hasta}')
        return tpl.format(t=self.tipo, s=self, m=m)

    def superpuestos(self):
        """Devolver segmentos superpuestos
        misma marca, mismo medio, mismo todo...
        con superposición temporal"""
        result = Segmento.objects.filter(
            desde__lt=self.hasta,
            hasta__gt=self.desde,
            medio=self.medio,
            tipo=self.tipo
        ).exclude(id=self.id)

        if self.marca is not None:
            result = result.filter(
                marca=self.marca
            )
        return result

    def to_json(self):
        """Serializar los datos de este segmento
        en un diccionario para enviar como json"""
        d = dict(
            begin=self.desde + DIFERENCIA_C_WELO,
            end=self.hasta + DIFERENCIA_C_WELO,
            id=self.id,
            boxes=[],
            type=self.tipo.id
        )
        if self.marca_id:
            d['brand'] = dict(id=self.marca.id, nombre=self.marca.nombre)
        if self.producto_id:
            d['product'] = dict(id=self.producto.id, nombre=self.producto.nombre)
        if self.sector_id:
            d['sector'] = dict(id=self.sector.id, nombre=self.sector.nombre)
        if self.formato_id:
            d['format'] = dict(id=self.formato.id, nombre=self.formato.nombre)
        if self.descripcion:
            d['description'] = self.descripcion

        for bb in self.boxes.filter(eliminado=False):
            d['boxes'].append(bb.to_json())
        return d

    def main_thumb_url(self):
        ts = self.get_thumbs_urls()
        if ts:
            return ts[0]

    def get_thumbs_urls(self):
        """Devolver una lista con las urls
        para ver los thumbs de este segmento"""
        i = self.desde
        cota = self.hasta

        # por las dudas...
        if cota < i:
            cota = i
        r = []

        while i <= cota:
            # creo un box ficticio en este segundo, que no voy a guardar,
            # y le pregunto su imagen:
            fake_box = BoundingBox(
                datetime=i,
                medio=self.medio
            )
            r.append(fake_box.get_image())
            i += timedelta(seconds=1)

        return r

    def get_horario(self):
        """Devolver el horario de este segmento, a partir de un
        audiovisual equivalente"""
        # averiguar programa desde audiovisual equivalente
        av = AudioVisual(fecha=self.desde.date(), hora=self.desde.time())
        av.set_horario(self.medio)
        return av.horario

    def get_programa(self):
        """Devolver el programa de este segmento"""
        horario = self.get_horario()
        return horario.programa

    def get_tarifa(self):
        """Calcular la tarifa de este segmento"""

        def ponderado(size):
            """devolver porcentaje de la tarifa que tomamamos
            según el tamaño de los boxes del segmento"""
            return {'P': 0.1, 'C': 0.2, 'M': 0.5, 'G': 1.0}.get(size, 0)

        horario = self.get_horario()
        tarifa_bruta = horario.tarifa or 0.0
        segundos = self.get_duration()
        return tarifa_bruta * ponderado(self.get_size_as_name()) * segundos


    def get_duration(self):
        """Duración de este segmento, en segundos"""
        return (self.hasta - self.desde).total_seconds()

    def get_size_as_name(self):
        """..."""
        s = int(round(self.get_size()))
        if s <= 5:
            return 'P'
        elif 5 < s <= 10:
            return 'C'
        elif 10 < s < 50:
            return 'M'
        else:
            return 'G'

    def get_size(self):
        """Tamaño promedio de los boxes de este segmento,
        o None si no hay boxes"""
        l = [b.get_size() for b in self.boxes.all()]
        if len(l):
            return sum(l) / float(len(l))
        else:
            return 0


    def train(self):
        """Entrenar a los motores para futuro reconocimiento"""
        if self.tipo.nombre == 'Publicidad':
            retval = self.train_audio()
            self.usado_para_entrenar = True
            self.save()
            return retval
        else:
            raise SegmentoTipoIncorrecto


    def train_audio(self):
        """
        Entrenar motor de audio.
        Puede lanzar ErrorAlEntrenarSegmento si no se puede conectar al servidor
        o hay un error en el pedido.
        """
        try:
            entreno_audio = settings.TRAIN_AUDIO
        except AttributeError:
            entreno_audio = False
        if not entreno_audio:
            raise ErrorAlEntrenarSegmento('No entreno Audio')
        server = self.medio.server
        if not server:
            raise ErrorAlEntrenarSegmento('Medio sin server')
        if not server.url:
            raise ErrorAlEntrenarSegmento('Medio sin url')
        url = 'http://{}/API/agregar_patron.php'.format(server.url)
        data = {
            'mediadelivery_id': self.medio.mediadelivery_id,
            'medio__identificador': self.medio.identificador,
            'medio__canal': self.medio.canal,
            'inicio': '{:%Y-%m-%d %H:%M:%S}'.format(self.desde - timedelta(seconds=1)),
            'fin': '{:%Y-%m-%d %H:%M:%S}'.format(self.hasta),
            'producto__formato': self.formato.nombre or '',
            'producto__marca_id': self.producto.marca.pk,
            'producto__nombre': self.producto.nombre,
            'producto__observaciones': self.producto.observaciones or self.descripcion or '',
        }

        data['producto__nombre'] = unicode(data['producto__nombre'])
        data['producto__observaciones'] = unicode(data['producto__observaciones'])
        request = requests.post(url, data)

        if request.status_code != 200:
            raise ErrorAlEntrenarSegmento('Error al conectar con {}'.format(url))
        try:
            response = request.json()
        except ValueError:
            raise ErrorAlEntrenarSegmento('Respuesta de {} incomprensible'.format(url))
        else:
            if not response['result']:
                raise ErrorAlEntrenarSegmento(response['msg'])
            else:
                return True

    def snap_to_origins(self):
        """Hacer que el inicio y el fin de este segmento
        coincidan con inicio y fin de orígenes"""
        from adkiller.media_map.utils import get_origins_from_welo
        origenes = get_origins_from_welo(self.medio.mediadelivery_id, self.desde, self.hasta) or []
        origenes = sorted(origenes, key=lambda x: x['start_time'])
        if len(origenes) == 1:
            self.desde = origenes[0]['start_time']
            self.hasta = origenes[0]['end_time']
        elif len(origenes) > 1:
            self.desde = origenes[0]['start_time']
            self.hasta = origenes[-2]['end_time']
        # DO NOT SAVE HERE!!!!!!!!

    def make_clip(self, tpa=False, dest=None, force_creation=False, published=None):
        """realizar clip de este segmento,
        de video o audio solo, llamando a welo.
        Devolver diccionario con información útil
        sobre la llamada y su resultado
        """
        try:
            corte_permitido = settings.CUT_CLIPS
        except AttributeError:
            corte_permitido = False
        if not corte_permitido:
            return {
                'success': False,
                'error_msg': 'No creo clips',
            }

        if self.medio.dvr == "radiocut":
            base_url = "http://martin.infoad.tv/radio.php"
            base_without_radio = "http://martin.infoad.tv"
            desde = self.desde.strftime("%Y-%m-%d-%H-%M-%S")
            hasta = self.hasta.strftime("%Y-%m-%d-%H-%M-%S")
            parameters = "?desde=%s&hasta=%s&identificador=%s&canal=%s" % (desde, hasta, self.medio.identificador, self.medio.canal)
            print base_url + parameters
            try:
                h = httplib2.Http()
                _, content = h.request(base_url + parameters, "GET", headers={})
                data = content.replace('\n', "")

            except Exception: # agregar tipo de exception que se intenta catpurar
                return -1

            result = {
                'success':True,
                'url' : base_without_radio + "/multimedia/ar/RadioSegments/" + data,
                'status':100,
            }
            return result

        else:
            url = '{}make_segment'.format(settings.WELO_SERVER)
            data = {
                'channel_id': self.medio.mediadelivery_id,
                # revisar esta suma de seis horas
                'start_time': '{:%Y-%m-%dT%H:%M:%S}'.format(self.desde + timedelta(hours=6)),
                'end_time': '{:%Y-%m-%dT%H:%M:%S}'.format(self.hasta + timedelta(hours=6)),
                'tpa': tpa,
            }
            if dest is not None:
                data['destdir'] = dest
            if force_creation:
                data['force_creation'] = True
            if published is not None:
                data['published'] = published

            request = requests.post(url, params=data)
            response = request.json()
            hash_id = response['hash_id']
            file_url = response['file_url']

            # esto esta hardcodeado
            if 'home/infoxel' in file_url:
                file_url = file_url.replace('home/infoxel', '')

            result = {
                'success': response.get('success', True),
                'generation_url': url,
                'generation_data': data,
                'url': settings.WELO_SERVER + file_url,
                'status_url': '{}status_clip/?hash_id={}'.format(settings.WELO_SERVER, hash_id)
            }
            sys.stdout.flush()
            return result

    @staticmethod
    def check_clip_creado(status_url):
        """Chequear url para ver si está creado el clip.
        No maneja las excepciones que puedan ocurrir."""
        r = requests.get(status_url)
        data = r.json()
        creado = data['status'] == 'uploaded' and data['completed'] == 100
        return creado

    def automatico(self):
        """Devolver True si este segmento fue generado automaticamente"""
        if self.tipo.nombre == 'Logos':
            for box in self.boxes.all():
                if box.automatico():
                    return True
            return False
        else:
            raise NotImplementedError

    def lazy_url(self):
        """..."""
        return "http://infoad.tv/media_map/clips/"+ str(self.id)

    class Meta:
        """Metaclass para Segmento"""
        verbose_name_plural = "Segmentos"

    def save(self, *args, **kwargs):
        """..."""
        # si está cambiando la marca y tiene bounding boxes que fueron usados
        # para entrenar a brandtrack, hay que ordenar y reentrenar
        if self.pk is not None:
            bbdd_instance = Segmento.objects.get(pk=self.pk)

            # sólo cambiar si se modificó marca, producto, sector o formato

            if bbdd_instance.marca != self.marca \
                or bbdd_instance.producto != self.producto \
                or bbdd_instance.formato != self.formato \
                or bbdd_instance.sector != self.sector:

                for box in self.boxes.filter(usado_para_entrenar=True):
                    box.borrar_en_brandtrack()
                    box.usado_para_entrenar = False
                    box.model = None
                    box.save()
                    box.train()

        if self.tipo_id == 1:
            # si es publicidad
            if self.eliminado:
                if self.emision_id:
                    self.emision.delete()
                    self.emision = None
            else:
                if self.emision_id:
                    self.emision.producto = self.producto
                    self.emision.fecha = self.desde.date()
                    self.emision.hora = self.desde.time()
                    self.emision.duracion = self.get_duration()
                    if self.formato_id:
                        self.emision.tipo_publicidad = TipoPublicidad.objects.get(descripcion=self.formato.nombre)
                    self.emision.save()
                else:
                    emi = self.to_ad()
                    emi.save()
                    self.emision = emi

        super(Segmento, self).save(*args, **kwargs)


@receiver(post_delete, sender=Segmento)
def delete_segmento(sender, instance, *args, **kwargs):
    """..."""
    if instance.emision_id:
        try:
            instance.emision.delete()
        except (AssertionError, ObjectDoesNotExist):
            # ya esta borrada
            pass


class JsonMigrado(models.Model):
    """
    Un archivo json del cual se
    migraron datos.
    """
    nombre = models.CharField(max_length=150)


class Evento(models.Model):
    """
    Un evento a analizar
    """
    PENDING = 1
    DONE = 2
    STATUS_CHOICES = (
        (PENDING, 'Pendiente'),
        (DONE, 'Terminado'),
    )
    medio = models.ForeignKey(Medio, related_name='eventos')
    desde = models.DateTimeField(db_index=True, null=True, blank=True)
    hasta = models.DateTimeField(db_index=True, null=True, blank=True)
    status = status = models.SmallIntegerField(choices=STATUS_CHOICES, default=PENDING)
    marcas = models.ManyToManyField(Marca, related_name='eventos')
    creado = models.DateTimeField(auto_now_add=True)
    cerrado = models.DateTimeField(editable=False, blank=True, null=True)

    def __unicode__(self):
        return u'Evento #{}'.format(self.pk)

    def porcentaje_avance(self):
        """Devolver un float indicando el porcentaje
        de chunks de este evento que ya se han analizado"""
        total = self.chunks.all().count()
        if total == 0:
            return 0.0
        cerrados = self.chunks.filter(status=2).count() # DONE
        return float(cerrados) / total * 100

    def cantidad_segmentos_automaticos(self):
        """ Cantidad de segmentos de tipo logo
        con alguna de las marcas del evento """
        return self.get_segmentos_automaticos().count()

    def get_segmentos_automaticos(self):
        """Devolver segmentos de tipo logo
        con alguna de las marcas del evento, que se
        hayan detectado automáticamente"""
        segm = Segmento.objects.filter(medio=self.medio,
                                       desde__gte=self.desde,
                                       desde__lte=self.hasta,
                                       marca__in=self.marcas.all(),
                                       tipo__id=2).filter(
                                           boxes__usado_para_entrenar=False,
                                           boxes__model__isnull=False
                                       ).distinct()
        return segm

    def link_a_mediamap(self):
        """Devolver url de este evento en mediamap, como string"""
        result = ('/media_map/view/#?channel={channel}'
                  '&datetime={dt:%Y-%m-%dT%H:%M:%S.000Z}'
                  '&layers=5&layers=2&interval=1&zoom=7')
        return result.format(channel=self.medio.id,
                             dt=self.desde + DIFERENCIA_C_WELO)

    def borrar_detecciones_automaticas(self):
        """Borrar detecciones automáticas dentro del evento que se borra"""
        for segmento in self.get_segmentos_automaticos():
            segmento.boxes.all().delete()
            segmento.delete()

class AciertosIFEP(models.Model):
    """Detecciones de placa inicio o fin de espacio publicitario"""
    INICIO = 0
    FIN = 1
    TIPO_CHOICES = (
        (INICIO, 'Inicio'),
        (FIN, 'Fin'),
    )
    datetime = models.DateTimeField()
    medio = models.ForeignKey(Medio, related_name='aciertos')
    tipo = models.SmallIntegerField(choices=TIPO_CHOICES)
    analizado = models.BooleanField(default=False)
    thumb_url = models.URLField(null=True, blank=True)
    brandmodel = models.ForeignKey(BrandModel, related_name='aciertos', null=True)

    def __str__(self):
        template = 'Placa {} @ {}'
        return template.format(self.TIPO_CHOICES[self.tipo][1], self.thumb_url)


@receiver(pre_delete, sender=Evento)
def eliminar_detecciones_automaticas(sender, instance, **kwargs):
    """Borrar detecciones automáticas dentro del evento que se borra"""
    instance.borrar_detecciones_automaticas()

@receiver(post_save, sender=Segmento)
def clip_segmentos_nuevos(sender, instance, created, **kwargs):
    """Quizás, pedir que se corte el clip en Welo"""
    if created:
        if instance.tipo.nombre == 'Tanda':
            if not instance.superpuestos():
                try:
                    instance.make_clip(tpa=True, dest='T')
                except AttributeError:
                    # no WELO_SERVER on settings...
                    pass
            else:
                # mensaje de error? TODO: mejorar esto
                instance.clip_creado = True
                instance.descripcion = 'Tanda no cortada por estar superpuesta'

