# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, line-too-long
"""
Admin para app media_map
"""
import os

from django.contrib import admin
from django.contrib import messages
from adkiller.media_map.forms import EventoForm
from adkiller.media_map.models import AciertosIFEP
from adkiller.media_map.models import BoundingBox
from adkiller.media_map.models import BrandModel
from adkiller.media_map.models import Evento
from adkiller.media_map.models import Formato
from adkiller.media_map.models import JsonMigrado
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento

class SegmentoAdmin(admin.ModelAdmin):
    fields = ['tipo', 'medio', 'desde', 'hasta', 'marca']
    list_display = ['marca', 'producto', 'medio', 'desde', 'hasta', 'user',
                    'descripcion', 'created_at', 'tipo']
    list_filter = ['medio', 'marca', 'desde', 'hasta', 'tipo']
    search_fields = ['medio__nombre', 'marca__nombre']
    date_hierarchy = "desde"

class AciertosIFEPAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'medio', 'tipo', 'analizado', 'view_detection', 'patron_definitivo']
    list_filter = ['medio', 'tipo']
    readonly_fields = ['brandmodel']
    actions = ['tooggle_definitivo']

    def view_detection(self, acierto):
        """return html img tag"""
        return ('<a href="{}" target="_blank">'
                '<img src="{}" alt="" height="120">'
                '</a>'.format(acierto.thumb_url, acierto.thumb_url))

    view_detection.allow_tags = True
    view_detection.short_description = 'Placa'

    def patron_definitivo(self, acierto):
        return acierto.brandmodel.definitivo
    patron_definitivo.boolean = True

    def tooggle_definitivo(self, _, queryset):
        """Cambiar el estado de definitivo de los bmodels seleccionados"""
        for acierto in queryset:
            if acierto.brandmodel_id:
                acierto.brandmodel.definitivo = not acierto.brandmodel.definitivo
                acierto.brandmodel.save()

    tooggle_definitivo.short_description = 'Activar / Desactivar'


class JsonMigradoAdmin(admin.ModelAdmin):
    model = JsonMigrado

class BBAdmin(admin.ModelAdmin):
    list_display = ['medio', 'datetime', 'score',
                    'usado_para_entrenar', 'eliminado']
    list_filter = ['medio']
    date_hierarchy = 'datetime'
    model = BoundingBox

class BrandModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'id_brandtrack',
                    '__unicode__',
                    'formato',
                    'view_crops',
                    'definitivo']
    fields = ['definitivo']
    actions = ['tooggle_definitivo']

    def tooggle_definitivo(self, _, queryset):
        """Cambiar el estado de definitivo de los bmodels seleccionados"""
        for brandmodel in queryset:
            brandmodel.definitivo = not brandmodel.definitivo
            brandmodel.save()

    tooggle_definitivo.short_description = 'Activar / Desactivar'

    def view_crops(self, model):
        """Show images"""
        images = model.get_images_urls()
        result = ''
        if images['success']:
            for im in images['activas']:
                style = ''
                result += '<p><img style="{}" src="{}"></p>'.format(style, im)
            for im in images['inactivas']:
                style = 'border-style: solid; border-width: 3px; border-color: red;'
                result += '<p><img style="{}" src="{}"></p>'.format(style, im)
        else:
            result = 'Error al mostrar imágenes'
        return result

    view_crops.allow_tags = True

    def formato(self, model):
        return model.get_formato() or ''


class EventoAdmin(admin.ModelAdmin):

    form = EventoForm

    fieldsets = (
        (None, {
            'fields': ('medio', 'desde', 'hasta', 'marcas'),
        }),
    )

    list_display = ['__str__', 'medio', 'desde', 'hasta', 'duracion',
                    'avance', 'cantidad_segmentos_automaticos', 'mediamap',
                    '_creado', '_cerrado']#, 'marcas']
    list_filter = ['medio', 'status']
    date_hierarchy = "desde"
    search_fields = ['medio__nombre', 'marcas__nombre']
    actions = ['borrar_detecciones', 'forzar_analisis']

    def borrar_detecciones(self, _, queryset):
        """Borrar detecciones automáticas hechas dentro de este evento"""
        for evento in queryset:
            evento.borrar_detecciones_automaticas()

    borrar_detecciones.short_description = 'Eliminar detecciones automáticas'

    def forzar_analisis(self, request, queryset):
        """Para este evento, analizar aunque el entrenamiento de alguna de
        sus marcas no esté aprobado del todo"""
        if queryset.count() > 1:
            error = 'Sólo puede forzar el análisis de un evento por vez'
            messages.add_message(request, messages.ERROR, error)
        else:
            evento = queryset[0]
            if evento.status == evento.DONE:
                error = ('El evento ya se analizó. SUGERENCIA: borre '
                         'el evento; se creará un evento nuevo en su lugar.')
                messages.add_message(request, messages.ERROR, error)
            else:
                cmd_tpl = (u'/home/infoxel/Envs/infoad/bin/python2.7 '
                           '/home/infoxel/projects/infoad/manage.py '
                           'analizar_chunks {} &')
                os.system(cmd_tpl.format(evento.id))
    forzar_analisis.short_description = 'Forzar análisis'


    def _creado(self, evento):
        """Formatear la fecha"""
        return '{:%Y/%m/%d %H:%M:%S}'.format(evento.creado)

    def _cerrado(self, evento):
        """Formatear la fecha"""
        if evento.cerrado:
            return '{:%Y/%m/%d %H:%M:%S}'.format(evento.cerrado)
        else:
            return '-'

    def avance(self, evento):
        """Porcentaje de análisis de este evento"""
        return '{:.2f}%'.format(evento.porcentaje_avance())

    def duracion(self, evento):
        """ en minutos"""
        return int((evento.hasta - evento.desde).total_seconds() / 60)

    def mediamap(self, obj):
        return '<a href="%s" target="_blank">%s</a>' % (obj.link_a_mediamap(),
                                                        "MediaMap")
    mediamap.allow_tags = True

    def save_related(self, request, form, formsets, change):
        """Si es nuevo, crear chunks a analizar"""
        crear_chunks = not change
        obj = form.instance
        super(EventoAdmin, self).save_related(request, form, formsets, change)

        if crear_chunks:
            from adkiller.engines_cache.models import Chunk
            obj = Evento.objects.get(id=obj.pk)
            bmodels = BrandModel.get_models_from_marcas(obj.marcas.all())
            _ = Chunk.factory(
                evento=obj,
                medio=obj.medio,
                desde=obj.desde,
                hasta=obj.hasta,
                brandmodels=bmodels,
            )

admin.site.register(AciertosIFEP, AciertosIFEPAdmin)
admin.site.register(Segmento, SegmentoAdmin)
admin.site.register(JsonMigrado, JsonMigradoAdmin)
admin.site.register(BoundingBox, BBAdmin)
admin.site.register(BrandModel, BrandModelAdmin)
admin.site.register(TipoSegmento)
admin.site.register(Formato)
admin.site.register(Evento, EventoAdmin)
