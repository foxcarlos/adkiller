# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Segmento.emision'
        db.add_column('media_map_segmento', 'emision',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Emision'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Segmento.emision'
        db.delete_column('media_map_segmento', 'emision_id')


    models = {
        'app.agencia': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Agencia'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.anunciante': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Anunciante'},
            'es_nacional': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'usuarios': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.emision': {
            'Meta': {'ordering': "[u'-fecha', u'-hora', u'-orden']", 'object_name': 'Emision'},
            'anunciantes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['app.Anunciante']", 'null': 'True', 'blank': 'True'}),
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'codigo_directv': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'confirmado': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'duracion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2016, 1, 13, 0, 0)', 'db_index': 'True'}),
            'fuente': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Origen']"}),
            'hora': ('django.db.models.fields.TimeField', [], {'default': "u'00:00'"}),
            'horario': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Horario']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'migracion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Migracion']"}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'orden': ('django.db.models.fields.IntegerField', [], {'default': '0', 'null': 'True', 'blank': 'True'}),
            'origen': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.Producto']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "u'P'", 'max_length': '1'}),
            'tipo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'emisiones'", 'null': 'True', 'to': "orm['app.TipoPublicidad']"}),
            'titulo': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'usuario': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'valor': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'app.estilopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EstiloPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.etiquetamedio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EtiquetaMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'app.etiquetaproducto': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EtiquetaProducto'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'app.etiquetaprograma': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EtiquetaPrograma'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'app.grupomedio': {
            'Meta': {'object_name': 'GrupoMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.horario': {
            'Meta': {'ordering': "[u'-fechaAlta']", 'object_name': 'Horario'},
            'alto': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ancho': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '10', 'decimal_places': '2', 'blank': 'True'}),
            'cantColum': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'dias': ('django.db.models.fields.CommaSeparatedIntegerField', [], {'max_length': '13', 'null': 'True', 'blank': 'True'}),
            'enElAire': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'fechaAlta': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_index': 'True'}),
            'horaFin': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'horaInicio': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Medio']"}),
            'pagina_desde': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'pagina_hasta': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'paginas': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'programa': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'horarios'", 'to': "orm['app.Programa']"}),
            'tarifa': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tarifaDudosa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'vigenciaHasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'db_index': 'True'})
        },
        'app.industria': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Industria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'rubro': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'industrias'", 'to': "orm['app.Rubro']"})
        },
        'app.marca': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Marca'},
            'anunciante': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'marcas'", 'null': 'True', 'to': "orm['app.Anunciante']"}),
            'brandtrack_entrenado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'marcas'", 'null': 'True', 'to': "orm['app.Sector']"})
        },
        'app.medio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Medio'},
            'busque_tandas_hasta': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'canal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'canal_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'codigo_ibope': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'desfasando': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dvr': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'encargado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'etiquetas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'medios'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.EtiquetaMedio']"}),
            'grabando': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'grabando_en_cero': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.GrupoMedio']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'incremento_color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'mediadelivery_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_deteccion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'server_grabacion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_grabacion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.migracion': {
            'Meta': {'object_name': 'Migracion'},
            'completa': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'hora': ('django.db.models.fields.TimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'servidor': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'servidor'", 'to': "orm['app.Servidor']"})
        },
        'app.origen': {
            'Meta': {'object_name': 'Origen'},
            'archivo': ('django.db.models.fields.CharField', [], {'max_length': '128', 'unique': 'True', 'null': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2016, 1, 13, 0, 0)', 'db_index': 'True'}),
            'hora': ('django.db.models.fields.TimeField', [], {'default': "u'00:00'"}),
            'horario': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'origenes'", 'null': 'True', 'to': "orm['app.Horario']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'servidor_original': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'+'", 'to': "orm['app.Servidor']"}),
            'tipo': ('django.db.models.fields.CharField', [], {'default': "u'F'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'origenes'", 'null': 'True', 'to': "orm['auth.User']"})
        },
        'app.producto': {
            'Meta': {'ordering': "[u'marca', u'nombre']", 'unique_together': "((u'nombre', u'marca'),)", 'object_name': 'Producto'},
            'agencia': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.Agencia']"}),
            'categorizacion_pendiente': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'codigo_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'null': 'True', 'blank': 'True'}),
            'duracion': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'estilo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.EstiloPublicidad']"}),
            'etiquetas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.EtiquetaProducto']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_en_simpson': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'marca': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'productos'", 'to': "orm['app.Marca']"}),
            'modificado': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '120', 'db_index': 'True'}),
            'nombre_del_patron': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'nuevo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'observaciones': ('django.db.models.fields.CharField', [], {'max_length': '500', 'blank': 'True'}),
            'otras_marcas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'otros_productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.Marca']"}),
            'recursos': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.RecursoPublicidad']"}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Servidor']", 'null': 'True', 'blank': 'True'}),
            'tipo_publicidad': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'to': "orm['app.TipoPublicidad']"}),
            'valores': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'productos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.ValorPublicidad']"})
        },
        'app.programa': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Programa'},
            'etiquetas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'programas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.EtiquetaPrograma']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'})
        },
        'app.recursopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'RecursoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.rol': {
            'Meta': {'object_name': 'Rol'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'app.rubro': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Rubro'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.sector': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Sector'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'industria': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'sectores'", 'to': "orm['app.Industria']"}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '150'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'e_mail_del_tecnico': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'espacio_en_disco': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'maquina_virtual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nombre_del_tecnico': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'online': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'plaza': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'servidores'", 'null': 'True', 'to': "orm['app.Ciudad']"}),
            'puerto_http': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_ssh': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'recibir_patrones': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'roles': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['app.Rol']", 'symmetrical': 'False', 'blank': 'True'}),
            'tecnico_alertado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ultima_vez_online': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'padre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'})
        },
        'app.tipopublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'TipoPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'es_publicidad': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'app.valorpublicidad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'ValorPublicidad'},
            'descripcion': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.IntegerField', [], {'unique': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'media_map.boundingbox': {
            'Meta': {'object_name': 'BoundingBox'},
            'datetime': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'eliminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'boxes'", 'to': "orm['app.Medio']"}),
            'model': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'boxes'", 'null': 'True', 'to': "orm['media_map.BrandModel']"}),
            'score': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'usado_para_entrenar': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'x1': ('django.db.models.fields.FloatField', [], {}),
            'x2': ('django.db.models.fields.FloatField', [], {}),
            'y1': ('django.db.models.fields.FloatField', [], {}),
            'y2': ('django.db.models.fields.FloatField', [], {})
        },
        'media_map.brandmodel': {
            'Meta': {'object_name': 'BrandModel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_brandtrack': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'score_threshold': ('django.db.models.fields.FloatField', [], {'default': '0.01', 'null': 'True', 'blank': 'True'})
        },
        'media_map.evento': {
            'Meta': {'object_name': 'Evento'},
            'cerrado': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'creado': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'desde': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'hasta': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marcas': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'eventos'", 'symmetrical': 'False', 'to': "orm['app.Marca']"}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'eventos'", 'to': "orm['app.Medio']"}),
            'status': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'})
        },
        'media_map.formato': {
            'Meta': {'object_name': 'Formato'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media_map.TipoSegmento']"})
        },
        'media_map.jsonmigrado': {
            'Meta': {'object_name': 'JsonMigrado'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150'})
        },
        'media_map.segmento': {
            'Meta': {'object_name': 'Segmento'},
            'boxes': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['media_map.BoundingBox']"}),
            'descripcion': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'desde': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'eliminado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'emision': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Emision']", 'null': 'True', 'blank': 'True'}),
            'formato': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'to': "orm['media_map.Formato']"}),
            'hasta': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marca': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'to': "orm['app.Marca']"}),
            'marcas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'segmentos_marcas'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.Marca']"}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'segmentos'", 'to': "orm['app.Medio']"}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'to': "orm['app.Producto']"}),
            'score': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'sector': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'to': "orm['app.Sector']"}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'segmentos'", 'null': 'True', 'to': "orm['media_map.TipoSegmento']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'})
        },
        'media_map.tiposegmento': {
            'Meta': {'object_name': 'TipoSegmento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['media_map']