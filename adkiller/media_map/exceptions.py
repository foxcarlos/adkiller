# -*- coding: utf-8 -*-
"""
Excepciones de la app media_map
"""


class SegmentoTipoIncorrecto(Exception):
    """Excepción utilizada para señalar que
    un segmento no es de tipo Tanda"""


class ErrorAlEntrenarSegmento(Exception):
    """Excepción usada para señalar que no
    se pudo entrenar un segmento"""
