# -*- coding: utf-8 -*-
"""
Tests de la app media_map
"""
# python imports
from datetime import datetime, timedelta
import json

# django imports
from django.test import TestCase

# Project imports
from adkiller.app.models import Ciudad
from adkiller.app.models import Industria
from adkiller.app.models import Marca
from adkiller.app.models import Medio
from adkiller.app.models import Rubro
from adkiller.app.models import Sector
from adkiller.app.models import Soporte
from adkiller.media_map.models import Segmento


class MediaMapTestCase(TestCase):
    """Clase de la que heredan todos los tests de la aplicación.
    Define un setup completo y abarcativo """

    def setUp(self):
        """
        Crear Medios, Marcas para correr los tests
        """
        # crear marcas
        consumo = Rubro.objects.create(nombre='consumo')
        industria = Industria.objects.create(rubro=consumo, nombre='Tecnología')
        sector = Sector.objects.create(industria=industria, nombre='Tecnología')

        self.marcas = {
            'exo': Marca.objects.create(sector=sector, nombre='Exo')
        }

        # crear medios
        cordoba = Ciudad.objects.create(nombre='Córdoba')
        tv_aire = Soporte.objects.create(nombre='tv_aire')

        self.medios = {
            'canal12': Medio.objects.create(nombre='Canal 12',
                                            ciudad=cordoba,
                                            soporte=tv_aire),
        }



class GetSegmentsOldTestCase(MediaMapTestCase):
    """Tests para la view get_segments_old"""

    @staticmethod
    def prepare_dts(datetime_string):
        """ Le sumo 3 horas y devuelvo un string apto para la consulta por GET
        '2015-07-30 09:42:41' -> '2015-07-30T12:42:41.000Z'
        """
        INPUT_FORMAT = '%Y-%m-%d %H:%M:%S'
        OUTPUT_FORMAT = '%Y-%m-%dT%H:%M:%S.00Z'
        input_date = datetime.strptime(datetime_string, INPUT_FORMAT)
        return datetime.strftime(input_date + timedelta(hours=3), OUTPUT_FORMAT)

    def setUp(self):
        """
        Crear segmentos
        """
        MediaMapTestCase.setUp(self)

        self.url_tpl = ('/media_map/get_segments_old/?'
                        'cada={cada}&'
                        'cantidad={cantidad}&'
                        'desde={desde}&'
                        'medio_id={medio_id}')

        # segmentos disponibles:
        #   18:00:00    a   18:00:10
        #   18:00:05    a   18:00:15
        #   18:00:10    a   18:00:20
        self.segmentos = [
            Segmento.objects.create(desde='2015-07-14 18:00:00',
                                    hasta='2015-07-14 18:00:10',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
            Segmento.objects.create(desde='2015-07-14 18:00:05',
                                    hasta='2015-07-14 18:00:15',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
            Segmento.objects.create(desde='2015-07-14 18:00:10',
                                    hasta='2015-07-14 18:00:20',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
        ]

    def test_isodates(self):
        """Testeo que la lista devuelta por la view
        tenga la cantidad de intervalos pedidos y las fechas correctas"""

        desde = self.prepare_dts('2015-07-14 17:50:00')
        cada = 1
        cantidad = 5
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        intervalos = json.loads(response.content)
        self.assertEqual(len(intervalos), cantidad)

        for index, intervalo in enumerate(intervalos):
            isodate = datetime.strptime(desde, '%Y-%m-%dT%H:%M:%S.%fZ')
            isodate += timedelta(seconds=cada * index)
            isodate = isodate.strftime('%Y-%m-%dT%H:%M:%S')
            self.assertEqual(intervalo['isodate'], isodate)


    def test_no_segments(self):
        """Testeo que la view get_segments_old me devuelva el número correcto
        de segmentos que comienzan, transcurren o terminan para distintos
        tiempos consultados"""
        desde = self.prepare_dts('2015-07-14 17:50:00')
        cada = 1
        cantidad = 5
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalos = json.loads(response.content)

        for intervalo in intervalos:
            self.assertEqual(intervalo['begin_end'], 0)
            self.assertEqual(intervalo['begin'], 0)
            self.assertEqual(intervalo['end'], 0)
            self.assertEqual(intervalo['along'], 0)

    def test_one_interval_1(self):
        """Pido desde las 17:59:55, 1 intervalo de 40 segundos"""
        desde = self.prepare_dts('2015-07-14 17:59:55')
        cada = 40
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalo = json.loads(response.content)[0]

        # 18:00:00 a 18:00:10  -> empieza y termina
        # 18:00:05 a 18:00:15  -> empieza y termina
        # 18:00:10 a 18:00:20  -> empieza y termina
        self.assertEqual(intervalo['begin_end'], 3)
        self.assertEqual(intervalo['begin'], 0)
        self.assertEqual(intervalo['end'], 0)
        self.assertEqual(intervalo['along'], 0)

    def test_one_interval_2(self):
        """Pido desde las 17:59:55, 1 intervalo de 10 segundos"""
        desde = self.prepare_dts('2015-07-14 17:59:55')
        cada = 10
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalo = json.loads(response.content)[0]

        # 18:00:00 a 18:00:10  -> empieza
        # 18:00:05 a 18:00:15  -> (nada)
        # 18:00:10 a 18:00:20  -> (nada)

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 1)
        self.assertEqual(intervalo['end'], 0)
        self.assertEqual(intervalo['along'], 0)

    def test_one_interval_3(self):
        """Pido desde las 18:00:02, 1 intervalo de 10 segundos"""
        desde = self.prepare_dts('2015-07-14 18:00:02')
        cada = 10
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalo = json.loads(response.content)[0]

        # 18:00:00 a 18:00:10  -> termina
        # 18:00:05 a 18:00:15  -> empieza
        # 18:00:10 a 18:00:20  -> empieza

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 2)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 0)

    def test_one_interval_4(self):
        """Pido desde las 18:00:02, 1 intervalo de 20 segundos"""
        desde = self.prepare_dts('2015-07-14 18:00:02')
        cada = 20
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalo = json.loads(response.content)[0]

        # 18:00:00 a 18:00:10  -> termina
        # 18:00:05 a 18:00:15  -> empieza y termina
        # 18:00:10 a 18:00:20  -> empieza y termina

        self.assertEqual(intervalo['begin_end'], 2)
        self.assertEqual(intervalo['begin'], 0)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 0)

    def test_one_interval_5(self):
        """Pido desde las 18:00:08, 1 intervalo de 3 segundos"""
        desde = self.prepare_dts('2015-07-14 18:00:08')
        cada = 3
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalo = json.loads(response.content)[0]

        # 18:00:00 a 18:00:10  -> termina
        # 18:00:05 a 18:00:15  -> along
        # 18:00:10 a 18:00:20  -> empieza

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 1)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 1)

    def test_several_intervals_1(self):
        """Pido desde las 18:00:00, 6 intervalos de 5 segundos"""
        desde = self.prepare_dts('2015-07-14 18:00:00')
        cada = 5
        cantidad = 6
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalos = json.loads(response.content)


        # --------------------------
        # PRIMER INTERVALO
        # --------------------------
        intervalo = intervalos[0]
        # de 18:00:00 a 18:00:05

        # 18:00:00 a 18:00:10  -> empieza
        # 18:00:05 a 18:00:15  -> (nada)
        # 18:00:10 a 18:00:20  -> (nada)

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 1)
        self.assertEqual(intervalo['end'], 0)
        self.assertEqual(intervalo['along'], 0)

        # --------------------------
        # SEGUNDO INTERVALO
        # --------------------------
        intervalo = intervalos[1]
        # de 18:00:05 a 18:00:10

        # 18:00:00 a 18:00:10  -> along
        # 18:00:05 a 18:00:15  -> empieza
        # 18:00:10 a 18:00:20  -> (nada)

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 1)
        self.assertEqual(intervalo['end'], 0)
        self.assertEqual(intervalo['along'], 1)

        # --------------------------
        # TERCER INTERVALO
        # --------------------------
        intervalo = intervalos[2]
        # de 18:00:10 a 18:00:15

        # 18:00:00 a 18:00:10  -> termina
        # 18:00:05 a 18:00:15  -> along
        # 18:00:10 a 18:00:20  -> empieza

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 1)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 1)

        # --------------------------
        # CUARTO INTERVALO
        # --------------------------
        intervalo = intervalos[3]
        # de 18:00:15 a 18:00:20

        # 18:00:00 a 18:00:10  -> (nada)
        # 18:00:05 a 18:00:15  -> termina
        # 18:00:10 a 18:00:20  -> along

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 0)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 1)

        # --------------------------
        # QUINTO INTERVALO
        # --------------------------
        intervalo = intervalos[4]
        # de 18:00:20 a 18:00:25

        # 18:00:00 a 18:00:10  -> (nada)
        # 18:00:05 a 18:00:15  -> (nada)
        # 18:00:10 a 18:00:20  -> termina

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 0)
        self.assertEqual(intervalo['end'], 1)
        self.assertEqual(intervalo['along'], 0)

        # --------------------------
        # SEXTO INTERVALO
        # --------------------------
        intervalo = intervalos[5]
        # de 18:00:25 a 18:00:30

        # 18:00:00 a 18:00:10  -> (nada)
        # 18:00:05 a 18:00:15  -> (nada)
        # 18:00:10 a 18:00:20  -> (nada)

        self.assertEqual(intervalo['begin_end'], 0)
        self.assertEqual(intervalo['begin'], 0)
        self.assertEqual(intervalo['end'], 0)
        self.assertEqual(intervalo['along'], 0)

class GetSegmentsTestCase(MediaMapTestCase):
    """Tests para la view get_segments"""

    def setUp(self):
        """
        Crear segmentos
        """
        MediaMapTestCase.setUp(self)

        self.url_tpl = ('/media_map/get_segments/?'
                        'cada={cada}&'
                        'cantidad={cantidad}&'
                        'desde={desde}&'
                        'medio_id={medio_id}')

        # segmentos disponibles:
        #   18:00:00    a   18:00:10
        #   18:00:05    a   18:00:15
        #   18:00:10    a   18:00:20
        self.segmentos = [
            Segmento.objects.create(desde='2015-07-14 18:00:00',
                                    hasta='2015-07-14 18:00:10',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
            Segmento.objects.create(desde='2015-07-14 18:00:05',
                                    hasta='2015-07-14 18:00:15',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
            Segmento.objects.create(desde='2015-07-14 18:00:10',
                                    hasta='2015-07-14 18:00:20',
                                    medio=self.medios['canal12'],
                                    marca=self.marcas['exo']),
        ]

    def test_no_segments(self):
        """Testeo que la view get_segments no devuelva ningún segmento """
        desde = GetSegmentsOldTestCase.prepare_dts('2015-07-14 17:50:00')
        cada = 1
        cantidad = 5
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalos = json.loads(response.content)
        self.assertEqual(len(intervalos), 0)


    def test_segments(self):
        """Pido desde las 17:59:55, 1 intervalo de 40 segundos"""
        desde = GetSegmentsOldTestCase.prepare_dts('2015-07-14 17:59:55')
        cada = 40
        cantidad = 1
        medio_id = self.medios['canal12'].id
        url = self.url_tpl.format(desde=desde,
                                  cada=cada,
                                  cantidad=cantidad,
                                  medio_id=medio_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        intervalos = json.loads(response.content)

        # 18:00:00 a 18:00:10
        self.assertEqual(intervalos[0]['begin'], '2015-07-14T21:00:00')
        self.assertEqual(intervalos[0]['end'], '2015-07-14T21:00:10')
        # 18:00:05 a 18:00:15
        self.assertEqual(intervalos[1]['begin'], '2015-07-14T21:00:05')
        self.assertEqual(intervalos[1]['end'], '2015-07-14T21:00:15')
        # 18:00:10 a 18:00:20
        self.assertEqual(intervalos[2]['begin'], '2015-07-14T21:00:10')
        self.assertEqual(intervalos[2]['end'], '2015-07-14T21:00:20')

