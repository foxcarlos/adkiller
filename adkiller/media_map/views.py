# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, too-many-locals, maybe-no-member, broad-except
# pylint: disable=too-many-branches, too-many-statements, bare-except, fixme
# pylint: disable=no-value-for-parameter
"""
Views para app media_map
"""

# python imports
import httplib2
import json
import csv
from datetime import datetime
import requests

# django imports
from django.conf import settings
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from django.db.models import Max
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt

# project imports
from adkiller.app.models import Medio, Marca, Emision, Producto, Sector, AudioVisual
from adkiller.control.utils import MetricasManager
from adkiller.filtros.models import Perfil
from adkiller.filtros.utils import get_qs_ocr
from adkiller.filtros.utils import get_qs_segmentos
from adkiller.media_map.exceptions import SegmentoTipoIncorrecto
from adkiller.media_map.exceptions import ErrorAlEntrenarSegmento
from adkiller.media_map.models import BoundingBox
from adkiller.media_map.models import BrandModel
from adkiller.media_map.models import DIFERENCIA_C_WELO
from adkiller.media_map.models import Formato
from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento
from adkiller.usuarios.models import add_log

# from adkiller.media_map.utils import *

def get_params(data, post=False, body=False):
    """Función para parsear el diccinario de POST"""
    if body:
        result = json.loads(data.body)
    elif post:
        result = json.loads(dict(data).keys()[0])
    else:
        result = data.copy()

    if 'medio_id' in result:
        get_medio_id = result['medio_id']
        result['medio'] = Medio.objects.get(id=get_medio_id)
    elif 'canal_id' in result:
        get_medio_id = result['canal_id']
        result['medio'] = Medio.objects.get(mediadelivery_id=get_medio_id)

    if 'segment' in result:
        result['desde'] = result['segment']['begin']
        result['hasta'] = result['segment']['end']

    if 'desde' in result and result['desde'] != "":
        get_desde = datetime.strptime(result['desde'], '%Y-%m-%dT%H:%M:%S.%fZ')
        get_desde -= DIFERENCIA_C_WELO
    else:
        get_desde = None
    result['desde'] = get_desde

    if 'hasta' in result and result['hasta'] != "":
        get_hasta = datetime.strptime(result['hasta'], '%Y-%m-%dT%H:%M:%S.%fZ')
        get_hasta -= DIFERENCIA_C_WELO
    else:
        get_hasta = None
    result['hasta'] = get_hasta

    return result


def filter_segments(medio, desde=None, hasta=None, tipos=None):
    """Filtrar segmentos por medio, desde y hasta"""
    lista = Segmento.objects.filter(medio=medio)
    if desde:
        lista = lista.filter(hasta__gte=desde)
    if hasta:
        lista = lista.filter(desde__lte=hasta)
    if tipos:
        lista = lista.filter(tipo_id__in=tipos)
    return lista.distinct().order_by('desde')


def filter_ads(medio, desde, hasta):
    """Filtrar emisiones"""
    result = AudioVisual.objects.filter(
        horario__medio=medio,
        segmentos__isnull=True,
        producto__isnull=False)
    if desde:
        result = result.filter(
            fecha=desde.date())
        result = result.extra(
            where=[
                """
                    "app_emision"."hora" + COALESCE("app_audiovisual"."inicio", 0) * interval '1 second' >= %s
                """
            ],
            params=[desde.strftime("%H:%M:%S")]
        )
    elif hasta:
        result = result.filter(
            fecha=hasta.date())
        result = result.extra(
            where=[
                """ "app_emision"."hora" +  interval '1 second'  *  (COALESCE("app_audiovisual"."inicio", 0) +"app_emision"."duracion")  <= %s
                """
            ],
            params=[hasta.strftime("%H:%M:%S")]
        )
    result = result.distinct().order_by('fecha', 'hora')
    return result

def get_segments(request):
    """Devolver segmentos que matchean el (request)

    Recibo 4 datos:
        - Medio
        - un tiempo inicial
        - un tiempo final

    Ejemplo:
    =======
        - Medio: Canal 7
        - Desde: 2015-07-28T15:07:32
        - Hasta: 2015-07-28T16:07:32

    Con esta información construyo una lista con segmentos (dict)
    Cada diccionario luce así:

    {
        'begin': '2015-07-28T15:07:32',
        'end': '2015-07-28T15:07:32',
        'type': '1',
    },
    """

    result = []
    if request.GET:
        params = get_params(request.GET)

        desde = params['desde']
        hasta = params['hasta']
        if 'layer_id' in params:
            layer_id = params['layer_id']
        else:
            layer_id = None

        for segment in filter_segments(params['medio'], desde, hasta, tipos=layer_id).order_by("desde"):
            result.append(segment.to_json())

        if (not layer_id or layer_id == 1) and True:
            # solo para publicidad o cuando pido todas las capas
            for ad in filter_ads(params['medio'], desde, hasta):
                seg = Segmento.from_ad(ad)
                result.append(seg.to_json())

    data = json.dumps(result, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


@csrf_exempt
def add_segment(request):
    """Agregar un nuevo segmento"""

    params = get_params(request.POST, post=True)

    medio = params['medio']
    tipo = TipoSegmento.objects.get(id=params['segment']['type'])
    desde = params['desde']
    hasta = params['hasta']
    brands = params['segment'].get('brands')
    boxes = params['segment'].get('boxes')

    segment = Segmento.objects.create(
        medio=medio,
        tipo=tipo,
        desde=desde,
        hasta=hasta,
        clip_creado=False
    )

    # Guardo los datos del segmento para las nociticas
    if 'brand' in params['segment'] and params['segment']['brand']:
        segment.marca_id = params['segment']['brand']['id']
    if 'sector' in params['segment'] and params['segment']['sector']:
        segment.sector_id = params['segment']['sector']['id']
    if 'description' in params['segment']:
        segment.descripcion = params['segment']['description']
    if 'format' in params['segment'] and params['segment']['format']:
        segment.formato_id = params['segment']['format']['id']

    if request.user.is_authenticated():

        segment.user = request.user
        segment.save()

        # tiempo que llevó hacer el segmento
        time_spent = params['segment'].get('time_spent', 0)

        mm = MetricasManager()
        mm.incrementar_cuenta(segment, time_spent, MetricasManager.REGISTRO)

        if brands:
            for marca in brands:
                segment.marcas.add(Marca.objects.get(id=marca['id']))

        if boxes:
            for box in boxes:
                d = datetime.strptime(box['datetime'], '%Y-%m-%dT%H:%M:%S.%fZ')
                d -= DIFERENCIA_C_WELO
                bb = BoundingBox(medio=medio,
                                 datetime=d,
                                 x1=box['x1'],
                                 y1=box['y1'],
                                 x2=box['x2'],
                                 y2=box['y2'])
                bb.save()
                segment.boxes.add(bb)

            mm.incrementar_cuenta(segment,
                                  time_spent,
                                  MetricasManager.CARGA_PATRON_IMAGEN,
                                  cantidad_boxes=len(boxes))



        mm.guardar_cambios()

        data = json.dumps(segment.to_json(), cls=DjangoJSONEncoder)
    else:
        data = json.dumps({'error': 'user is not authenticated'}, cls=DjangoJSONEncoder)

    return HttpResponse(data, mimetype='application/json')


def update_emision(emision, params):
    """..."""
    from adkiller.app.models import TipoPublicidad
    if 'product' in params['segment'] and params['segment']['product']:
        emision.producto_id = params['segment']['product']['id']
    if 'description' in params['segment']:
        emision.observaciones = params['segment']['description']
    if 'format' in params['segment'] and params['segment']['format']:
        emision.tipo_publicidad = TipoPublicidad.objects.get(descripcion=params['segment']['format']['nombre'])

    # VALIDAR
    # si esta tanda se modificó por mediamap, que la próxima migración no
    # deshaga los cambios manuales
    AudioVisual.objects.filter(
        origen=emision.audiovisual.origen
    ).update(tanda_completa=True)
    emision.save()

    return HttpResponse(json.dumps(params, cls=DjangoJSONEncoder),
                        mimetype='application/json')


@csrf_exempt
def update_segment(request):
    """Actualizar datos de un segmento"""
    try:
        params = get_params(request.POST, post=True)
    except ValueError:
        # me pasaron un producto con ampersand
        params = get_params(request, body=True)

    try:
        segment = Segmento.objects.get(id=params['segment']['id'])
    except ObjectDoesNotExist:
        try:
            emision = Emision.objects.get(id=params['segment']['id'])
        except ObjectDoesNotExist:
            return HttpResponse("No se encuentra el segmento",
                                mimetype='application/json')

        # esto es porque actualmente muestro las emisiones como segmentos, y con esto se pueden modificar
        # eliminar cuando todo se haga desde mediamap
        return update_emision(emision, params)

    segment.desde = params['desde']
    segment.hasta = params['hasta']

    if 'brand' in params['segment'] and params['segment']['brand']:
        segment.marca_id = params['segment']['brand']['id']
    if 'product' in params['segment'] and params['segment']['product']:
        segment.producto_id = params['segment']['product']['id']
    if 'sector' in params['segment'] and params['segment']['sector']:
        segment.sector_id = params['segment']['sector']['id']
    if 'description' in params['segment']:
        segment.descripcion = params['segment']['description']
    if 'format' in params['segment'] and params['segment']['format']:
        segment.formato_id = params['segment']['format']['id']
    segment.tipo = TipoSegmento.objects.get(id=params['segment']['type'])

    segment.save()

    try:
        segment.cerrar_tanda()
    except SegmentoTipoIncorrecto:
        # No es de tipo publicidad
        pass

    # tiempo que llevó hacer el segmento
    time_spent = params['segment'].get('time_spent', 0)

    mm = MetricasManager()
    mm.incrementar_cuenta(segment, time_spent, MetricasManager.EDICION)

    mm.guardar_cambios()

    return HttpResponse(json.dumps(params, cls=DjangoJSONEncoder),
                        mimetype='application/json')


def get_segment_filename(identificador, start, end):
    """
    Retorna el nombre del archivo que va a reprensentar determinado segmento
    todo: refactor usando str.format
    """
    start_hour = str(start.hour).zfill(2)+str(start.minute).zfill(2)+str(start.second).zfill(2)
    end_hour = str(end.hour).zfill(2)+str(end.minute).zfill(2)+str(end.second).zfill(2)
    start_date = str(start.day).zfill(2) + str(start.month).zfill(2) + str(start.year)
    return identificador + "_"+ start_date + "_" + start_hour + "_" + end_hour+ ".mp3"


@csrf_exempt
def remove_segment(request):
    """Eliminar segmento"""
    data = {'success': True}
    try:
        params = get_params(request.POST, post=True)
    except ValueError:
        params = get_params(request, body=True)

    try:
        segment = Segmento.objects.get(id=params['segment']['id'])
    except ObjectDoesNotExist:
        # es una emisión?
        try:
            emision = Emision.objects.get(id=params['segment']['id'])
        except ObjectDoesNotExist:
            data = {'success': False}
            return HttpResponse(json.dumps(data), mimetype='application/json')
        else:
            try:
                # tratar de 'cerrar' la tanda
                tanda = emision.get_tanda_or_segmento()
                tanda.cerrar_tanda()
            except (AttributeError, SegmentoTipoIncorrecto):
                pass

            # borrar la emisión
            emision.delete()

            return HttpResponse(json.dumps(data), mimetype='application/json')



    clip_name = get_segment_filename(segment.medio.identificador,
                                     segment.desde,
                                     segment.hasta)
    base_url = "http://martin.infoad.tv/radio.php"
    parameters = "?path=%s" %(clip_name)

    if ".mp3" in clip_name: #no vaya a ser cosa que borremos algo importante, aseguremos que es un mp3
        try:
            h = httplib2.Http()
            _, _ = h.request(base_url + parameters, "GET", headers={})
            # la variable data no se usa:
            # data = content.replace('\n', "")

        except Exception: # Especificar excepción que se queire atrapar
            pass
    for bb in segment.boxes.all():
        if bb.segmentos.count() == 1:
            bb.eliminar()
    segment.delete()
    return HttpResponse(json.dumps(data), mimetype='application/json')


def next_segment(request):
    """Siguiente segmento"""
    if request.GET:
        params = get_params(request.GET)

        if 'layer_id' in params:
            layer_id = params['layer_id']
        else:
            layer_id = None

        print layer_id
        lista = Segmento.objects.filter(medio=params['medio'])
        if 'desde' in params and params['desde']:
            lista = lista.filter(desde__gte=params['desde'])
        if 'hasta' in params and params['hasta']:
            lista = lista.filter(hasta__lte=params['hasta'])
        if layer_id:
            lista = lista.filter(tipo_id=layer_id)
        segs = lista.distinct()

        ads = filter_ads(params['medio'], params['desde'], params['hasta'])

        if segs or ads:
            if params['hasta']:
                segs = segs.order_by("-desde")
                ads = ads.order_by("-fecha", "-hora")
            else:
                segs = segs.order_by("desde")
                ads = ads.order_by("fecha", "hora")

            if not segs:
                result = Segmento.from_ad(ads[0])
            elif not ads:
                result = segs[0]
            else:
                if params['hasta']:
                    result = Segmento.from_ad(ads[0])
                else:
                    result = Segmento.from_ad(ads[0])

            data = json.dumps(result.to_json(), cls=DjangoJSONEncoder)
            return HttpResponse(data, mimetype='application/json')
        else:
            return HttpResponse("", mimetype='application/json')


@csrf_exempt
def add_box_in_segment(request):
    """Agregar bounding box en un segmento"""
    params = get_params(request.POST, post=True)
    segment = Segmento.objects.get(id=params['segment_id'])
    box = params['box']
    d = datetime.strptime(box['datetime'], '%Y-%m-%dT%H:%M:%S.%fZ')
    d -= DIFERENCIA_C_WELO
    bb = BoundingBox(medio=segment.medio, datetime=d, x1=box['x1'], y1=box['y1'], x2=box['x2'], y2=box['y2'])
    bb.save()
    segment.boxes.add(bb)

    # tiempo que llevó hacer el segmento
    time_spent = params.get('time_spent', 0)

    mm = MetricasManager()
    mm.incrementar_cuenta(segment,
                          time_spent,
                          MetricasManager.CARGA_PATRON_IMAGEN,
                          cantidad_boxes=1)
    mm.guardar_cambios()

    data = json.dumps(bb.to_json(), cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


@csrf_exempt
def remove_box(request):
    """borrar bbox desde mediamap"""
    params = get_params(request.POST, post=True)
    box = BoundingBox.objects.get(id=params['id'])
    box.eliminar()
    return HttpResponse("", mimetype='application/json')


@csrf_exempt
def train_box(request):
    """Usar crop para entrenar a brandtrack"""
    params = get_params(request.GET, post=False)
    box = BoundingBox.objects.get(id=params['id'])
    result = box.train()
    return HttpResponse(json.dumps(result), mimetype='application/json')


@csrf_exempt
def train_segment(request):
    """Entrenar un segmento,
    actualmente segmento.train() no hace nada"""
    params = get_params(request.POST, post=True)
    seg = Segmento.objects.get(id=params['id'])
    seg.train()
    return HttpResponse("", mimetype='application/json')


def get_audio(request):
    """Obtener audio de un segmento"""
    params = get_params(request.GET, post=False)
    try:
        segment = Segmento.objects.get(id=params['id'])
    except ObjectDoesNotExist:
        return HttpResponse("No se encuentra el segmento", mimetype='application/json')
    result = segment.make_clip(tpa=True, dest="P")
    return HttpResponse(json.dumps(result), content_type='application/json')


def get_video(request):
    """Obtener video de un segmento"""
    params = get_params(request.GET, post=False)
    segment = Segmento.objects.get(id=params['id'])
    result = segment.make_clip()
    return HttpResponse(json.dumps(result), content_type='application/json')

def get_preview(request):
    """Obtener video de un segmento"""
    params = get_params(request.GET, post=False)
    segment = Segmento.objects.get(id=params['id'])
    result = segment.make_clip(tpa=True)
    return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def get_brand_models(_):
    """Devolver json con los id en brandtrack de
    los models disponibles para analizar imágenes"""
    result = []
    for m in BrandModel.objects.all():
        try:
            marca = m.get_marca().nombre
        except AttributeError:
            marca = '?'
        result.append({'model': m.id_brandtrack,
                       'marca': marca,
                       'producto': m.get_producto()})
    return HttpResponse(json.dumps(result), mimetype='application/json')


def delete_detected_box(request):
    """que hace esto?"""
    url_template = ('http://{server}/lerner/remove_fp/?'
                    'model={model}&'
                    'image={image}&'
                    'coords={coords}')
    url = url_template.format(
        server=settings.BRANDTRACK_SERVER_URL,
        model=request.GET['model'],
        image=request.GET['image'],
        coords=request.GET['coords'],
    )
    r = requests.get(url)
    r = requests.get(url)
    try:
        result = r.json()
    except ValueError:
        result = {'status': 'error', 'data': []}
    return HttpResponse(json.dumps(result), mimetype='application/json')


def analyze(request):
    """
    Analizar imágenes en busca de logos
    """
    if 'brands' in request.GET:
        models = []
        get_brands = request.GET['brands']
        if isinstance(get_brands, basestring):
            get_brands = [get_brands]
        for b in get_brands:
            brand = json.loads(b)
            marca = Marca.objects.get(id=brand['id'])
            for model in BrandModel.get_models_from_marcas([marca]):
                models.append(str(model.id_brandtrack))
    else:
        models = []
    url_template = ('http://{server}/sigmund/analize/?'
                    'models={models}&'
                    'image={image}')
    url = url_template.format(
        server=settings.BRANDTRACK_SERVER_URL,
        models=",".join(models),
        image=request.GET['image'],
    )
    r = requests.get(url)
    return HttpResponse(json.dumps(r.json()), mimetype='application/json')


def get_format_by_product(request):
    """Devolver el formato según el producto"""
    product_id = request.GET['product_id']
    try:
        s = Segmento.objects.filter(
            producto_id=product_id,
            formato__isnull=False
        )[0]
        format_id = s.formato.id
        format_name = s.formato.nombre
    except IndexError:
        format_id = ""
        format_name = ""

    result = {
        'nombre': format_name,
        'id':format_id
    }

    return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder),
                        mimetype='application/json')


def autocomplete(request):
    """Autocompletado,
    se llama desde ...
    devuelve ...
    """
    model = request.GET['model']
    text = request.GET['text']
    limite = 10
    if model == "marca":
        # ordenar las marcas por
        # productos más recientemente creados
        candidatos = Marca.objects.all().extra(select={'length':'Length(nombre)'}).order_by('length')
            # productos__creado__isnull=False,
        # ).annotate(mpc=Max('productos__creado')).distinct().order_by('-mpc')

    elif model == "marca_con_model":
        candidatos = Marca.objects.filter(
            segmentos__boxes__usado_para_entrenar=True
        ).distinct().order_by('nombre')
        limite = 100

    elif model == "producto":

        # sólo los vigentes
        candidatos = Producto.objects.filter(
            marca_id=request.GET['marca_id'],
        ).annotate(mef=Max('emisiones__fecha')).distinct().order_by('-mef')

        if request.GET.has_key('segment_id'):
            # ordenar por menor diferencia con la duración del segmento
            try:
                s = Segmento.objects.get(id=request.GET['segment_id'])
            except ObjectDoesNotExist:
                pass
            else:
                duracion = (s.hasta - s.desde).total_seconds()
                candidatos = candidatos.extra(
                    {'dif': 'abs(app_producto.duracion - {})'.format(duracion)}
                ).order_by('dif', '-mef')

    elif model == "sector":
        candidatos = Sector.objects

    elif model == "formato":
        limite = 1000
        candidatos = Formato.objects.filter(tipo_id=request.GET['tipo_id'])

    if text:
        candidatos = candidatos.filter(nombre__icontains=text)
    else:
        candidatos = candidatos.all()
    # antes ordenábamos por cantidad de letras en el nombre, ahora prefiero
    # que el orden venga dado por lo que se filtra más arriba
    result = [{'id': r.id, 'nombre': r.nombre} for r in candidatos][:limite]
    return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder),
                        mimetype='application/json')

@csrf_exempt
def add_product(request):
    """Crear un producto nuevo en la bbdd"""
    try:
        params = get_params(request.POST, post=True)
    except ValueError:
        # me pasaron un producto con ampersand
        params = get_params(request.body)
    # incluye un espacio
    params['product_name'] = params['product_name'].strip()

    try:
        producto = Producto.objects.create(
            marca_id=params['brand_id'],
            nombre=params['product_name']
        )

        duracion = producto.obtener_duracion_de_nombre()
        if duracion is None:
            try:
                segmento = Segmento.objects.get(pk=params['segment_id'])
                duracion = segmento.get_duration()
                # redondear al múltiplo de 5 más cercano
                duracion = max(5, int(round(duracion / 5.)) * 5)
            except ObjectDoesNotExist:
                pass

        if duracion:
            producto.duracion = duracion
            producto.save()

        result = {
            'id': producto.id,
            'nombre': producto.nombre,
            'ok': True,
        }
    except: # Qué podría salir mal? KeyError?
        result = {
            'ok': False,
        }
    return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder),
                        mimetype='application/json')


def channel(_):
    """Devolver json con medios que están en welo"""
    medios = Medio.objects.filter(Q(mediadelivery_id__isnull=False)|Q(dvr="radiocut"))
    result = [{'id': m.mediadelivery_id, 'id_infoad': m.id, 'nombre': m.nombre, 'identificador' : m.identificador, "dvr": m.dvr, 'canal': m.canal} for m in medios]
    return HttpResponse(json.dumps(result, cls=DjangoJSONEncoder),
                        mimetype='application/json')

def export_ocr_annotations(request):
    """Exportar csv con datos anotados de OCR"""
    perfil = Perfil.objects.get(user=request.user, activo=True)
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'
    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter='\t', dialect="excel")
    titulos = ['FECHA', 'HORA', 'MEDIO', 'IMAGEN', 'COORDENADAS (x1, y1, x2, y2)', 'TEXTO']
    writer.writerow(titulos)

    segmentos = get_qs_ocr(perfil)
    for segmento in segmentos.order_by('desde'):

        if segmento.get_duration() > 0:

            fecha = segmento.desde.strftime("%Y-%m-%d")
            hora = segmento.desde.strftime("%H:%M:%S")
            medio = segmento.medio

            try:
                box = segmento.boxes.all().order_by('datetime')[0]
            except IndexError:
                continue
            thumb = box.get_image()
            coordenadas = box.get_coords()
            texto = segmento.descripcion

            row = [
                fecha,
                hora,
                medio,
                thumb,
                coordenadas,
                texto
            ]
            writer.writerow(row)
    return response


def export_sponsoring(request):
    """Exportar csv con datos de segmentos según los filtros vigentes"""
    include_publis = request.GET.get('p', False)
    user = get_user(request)
    perfil = Perfil.objects.get(user=user, activo=True)
    response = HttpResponse(mimetype='text/csv')
    response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    response['Set-Cookie'] = 'fileDownload=true; path=/'
    response.write(u'\ufeff'.encode('utf8'))

    writer = csv.writer(response, delimiter='\t', dialect="excel")
    titulos = ['TIPO', 'FECHA', 'HORA', 'MEDIO', 'SOPORTE',
               'PROGRAMA', 'SECTOR', 'MARCA', 'PRODUCTO',
               'FORMATO', 'DURACION', 'TAMAÑO', 'TARIFA',
               'THUMB', 'CLIP', 'DESCRIPCION', 'BOX', 'A#']
    writer.writerow(titulos)

    segmentos = get_qs_segmentos(perfil, publicidades=include_publis)
    for segmento in segmentos.order_by('desde'):

        if segmento.get_duration() > 0:

            tipo = segmento.tipo
            fecha = segmento.desde.strftime("%Y-%m-%d")
            hora = segmento.desde.strftime("%H:%M:%S")
            medio = segmento.medio
            soporte = medio.soporte
            programa = segmento.get_programa()
            sector = None
            marca = None
            producto = None
            formato = None
            descripcion = None
            box = ''
            codigo_directv = ''
            if segmento.sector_id:
                sector = segmento.sector
            if segmento.marca_id:
                marca = segmento.marca
                if sector == None:
                    sector = marca.sector
            if segmento.producto_id:
                producto = segmento.producto
            if segmento.formato_id:
                formato = segmento.formato
            if segmento.descripcion:
                descripcion = segmento.descripcion.encode("utf-8")
                descripcion = descripcion.replace('\n', ' ')

            if segmento.emision_id:
                codigo_directv = segmento.emision.get_codigo_directv()
            elif segmento.producto_id and segmento.producto.codigo_directv:
                codigo_directv = segmento.producto.codigo_directv.split(',')[0]

            duracion = int(round(segmento.get_duration()))

            clip = segmento.lazy_url()

            try:
                size = segmento.get_size_as_name()
            except TypeError:
                size = ''

            try:
                thumb = segmento.boxes.all()[0].get_image()
                box = segmento.boxes.all()[0].get_coords()
            except IndexError:
                thumbs = segmento.get_thumbs_urls()
                if thumbs:
                    # len(thumbs) -> thumb_index
                    #     1       ->    0
                    #     2       ->    0
                    #     3       ->    1
                    #     4       ->    1
                    #     5       ->    2
                    #     6       ->    2
                    #     7       ->    3
                    #     8       ->    3
                    #    ...
                    thumb = thumbs[(len(thumbs) - 1) / 2]
                else:
                    thumb = ''

            # si es radio para que queremos el thumb? chau
            if segmento.medio.dvr == "radiocut":
                thumb = ''

            try:
                tarifa = int(round(segmento.get_tarifa()))
            except AttributeError:
                tarifa = ''

            row = [
                tipo,
                fecha,
                hora,
                medio,
                soporte,
                programa,
                sector,
                marca,
                producto,
                formato,
                duracion,
                size,
                tarifa,
                thumb,
                clip,
                descripcion,
                box,
                codigo_directv,
            ]
            writer.writerow(row)

    add_log(perfil.user, Perfil, perfil, 6, "Exportó a Excel Segmentos")

    return response


def view_clip(request, clip_id):
    """Reproducir un clip (lo crea si no existe)"""
    tpa = True
    if 'video' in request.GET:
        tpa = False
    segment = Segmento.objects.get(id=int(clip_id))
    response = segment.make_clip(tpa=tpa, dest="P")
    return render_to_response("preview.html", response)


@login_required
def view_mediamap(request):
    """
    qué hace esta función?
    """
    return render(request, "view_mediamap.html")



def make_query_on_radiodb(request):
    """
    Devuelve un origen y un offset para empezar a escuchar desde mm
    """
    params = get_params(request.GET, post=False)
    _channel = params['channel']
    desde = params['desde']
    hour = desde.strftime("%H:%M:%S")
    date = desde.strftime("%Y-%m-%d")
    try:
        h = httplib2.Http()
        string = "http://martin.infoad.tv/radio_query.php?hour=%s&date=%s&channel=%s" % (hour, date, _channel)
        _, content = h.request(string, "GET", headers={})
        data = json.loads(content)

    except Exception: # especificar excepción que se intenta capturar
        return -1

    url = 'http://martin.infoad.tv/multimedia/ar/CapturaDVRs/{}'.format(data['result']['file_name'])
    response = {}
    inicio = datetime(int(data['result']['fecha'][0:4]),
                      int(data['result']['fecha'][5:7]),
                      int(data['result']['fecha'][8:10]),
                      int(data['result']['hora'].split(":")[0]),
                      int(data['result']['hora'].split(":")[1]),
                      int(data['result']['hora'].split(":")[2]))
    diferencia = desde - inicio
    response['url'] = url
    response['offset'] = diferencia.seconds
    return HttpResponse(json.dumps(response), content_type="application/json")


def cargar_patron_audio(request):
    """Entrenar a adtrack con el producto
    correspondiente a un segmento"""
    try:
        params = get_params(request.POST, post=True)
        segment = Segmento.objects.get(id=params['segment_id'])
        segment.train()
    except SegmentoTipoIncorrecto:
        result = {'success': False, 'msg': 'Patron de audio solo en segmento tipo publicidad'}
    except ErrorAlEntrenarSegmento as e:
        result = {'success': False, 'msg': e.message}
    except Exception as e:
        result = {'success': False, 'msg': e.message}
    else:
        result = {'success': True}
    return HttpResponse(json.dumps(result), content_type="application/json")

