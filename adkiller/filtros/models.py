# -*- coding: utf-8 -*-
# Python Imports
from datetime import date, timedelta, datetime

# Django Imports
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

# Project Imports

ORDEN_DEFAULT = "marca,soporte,medio,programa,industria,sector,ciudad,anunciante,tipo publicidad"
                 #+,agencia,valor publicidad,tipo publicidad,estilo publicidad,recurso publicidad"

FILTROS = ['programa', 'medio', 'soporte', 'ciudad', 'producto', 'marca',
        'anunciante', 'sector', 'industria', 'rubro', "recurso publicidad"]
        #'valor publicidad', 'estilo publicidad', 'tipo publicidad',
        #'agencia',

def formatear_clave(clave):
    if clave == "estilopublicidad":
        clave = "estilo publicidad"
    elif clave == "valorpublicidad":
        clave = "valor publicidad"
    elif clave == "tipopublicidad":
        clave = "tipo publicidad"
    elif clave == "recursopublicidad":
        clave = "recurso publicidad"
    elif clave == "etiquetaprograma":
        clave = "etiqueta programa"
    elif clave == "etiquetamedio":
        clave = "etiqueta medio"
    elif clave == "etiquetaproducto":
        clave = "etiqueta producto"
    elif clave == "etiquetaproducto":
        clave = "etiqueta producto"
    elif clave == "grupomedio":
        clave = "grupo medio"
    return clave


class Perfil(models.Model):
    """ la idea es que cuando se cree el perfil se limite el filtro"""
    user = models.ForeignKey(User)
    orden = models.CharField(max_length=150, default=ORDEN_DEFAULT)
    unidad = models.CharField(max_length=20, default="cantidad")
    comparado = models.ForeignKey(ContentType, null=True, blank=True)
    activo = models.BooleanField(default=True)
    nombre = models.CharField(max_length=150, default="Búsqueda")
    tortas_visibles = models.BooleanField(default=False)
    tipo_comparacion = None
    otras_marcas = models.BooleanField(default=False)
    alerta_producto = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "Perfiles"

    def __unicode__(self):
        return u"Perfil %s de %s" % (self.nombre, self.user)

    def alertas_activas(self):
        return self.alertas.filter(activa=True)

    def inicializar(self):
        self.crear_filtros()
        self.setear_cantidades()
        periodo = Periodo.objects.create(perfil=self)
        periodo.hora_desde = datetime.now().strftime("%H:%M:%s")
        periodo.hora_hasta = datetime.now().strftime("%H:%M:%s")
        periodo.fecha_desde = (date.today() -
                            timedelta(days=15)).strftime("%Y-%m-%d")
        periodo.fecha_hasta = date.today().strftime("%Y-%m-%d")
        self.save()

    def seleccionada(self):
        if self.activo:
            return True
        else:
            act = Perfil.objects.get(activo=True, user=self.user)
            return act.filtros_to_dict(False, con_fechas=False) ==\
                                self.filtros_to_dict(False, con_fechas=False)

    def crear_clon_filtros(self, nombre=None):
        """
            Crea un perfil nuevo con los mismos filtros seleccionados

            No clona la parte de periodos
        """
        perfil_nuevo = Perfil.objects.create(user=self.user, activo=False)

        if nombre == None:
            nombre = self.nombre
        perfil_nuevo.nombre = nombre

        self.copiar_filtros_a(perfil_nuevo)

        return perfil_nuevo

    def copiar_periodo_y_filtros_a(self, perfil_destino, como_iniciales=False):
        self.copiar_periodo_a(perfil_destino, como_iniciales)
        self.copiar_filtros_a(perfil_destino, como_iniciales)

    def copiar_periodo_a(self, perfil_destino, como_iniciales=False):
        periodoTo = perfil_destino.get_periodo_or_create()
        periodoFrom = Periodo.objects.filter(perfil=self)[0]

        periodoTo.lapso = periodoFrom.lapso
        periodoTo.hora_desde = periodoFrom.hora_desde
        periodoTo.hora_hasta = periodoFrom.hora_hasta
        periodoTo.fecha_desde = periodoFrom.fecha_desde
        periodoTo.fecha_hasta = periodoFrom.fecha_hasta

        if como_iniciales:
            periodoTo.init_fecha_desde = periodoFrom.fecha_desde
            periodoTo.init_fecha_hasta = periodoFrom.fecha_hasta

        periodoTo.save()

    def copiar_filtros_a(self, perfil_destino, como_iniciales=False):
        perfil_destino.unidad = self.unidad
        perfil_destino.comparado = self.comparado
        perfil_destino.otras_marcas = self.otras_marcas

        # Resetear filtros:
        for filter in Filtro.objects.filter(perfil=perfil_destino):
            options = OpcionFiltro.objects.filter(filtro=filter)
            for item in options:
                item.delete()
                # FIXME: tener en cuenta que al agregar varios,
                # esto borra solo el primero.

        # Setear filtros:
        filtros = Filtro.objects.filter(perfil=self)
        for filtro in filtros:
            opciones = OpcionFiltro.objects.filter(filtro=filtro)
            for item in opciones:
                set_filter = perfil_destino.get_or_crear_filtro(filtro.tipo)
                OpcionFiltro(filtro=set_filter, inicial=como_iniciales or item.inicial,
                                opcion_id=item.opcion_id).save()

    def set_unit(self, unidad):
        """ seteo la unidad"""
        self.unidad = str(unidad)
        self.save()

    def get_unit(self):
        unidad = "cantidad"
        if self.unidad:
            unidad = self.unidad
        return unidad

    # --->
    def setear_cantidades(self):
        filtros = Filtro.objects.filter(perfil=self)
        for filtro in filtros:
            filtro.cantidadActual = filtro.incremento_en_cantidad
            filtro.save()

    # ---> Periodos
    def get_periodo_or_create(self):
        periodos = Periodo.objects.filter(perfil=self)
        if len(periodos) > 0:
            periodo = periodos[0]
            periodo.validar()
        else:
            periodo = Periodo.objects.create(lapso='M', perfil=self)

        return periodo

    def get_comparativo_periodo(self):
        """
            Crea un diccionario para comparaciones de periodos
            Con el periodo a comparar para calcular variaciones

            Si no hay uno definido por el usuario, calcula el periodo
            de igual longitud al principal inmediatamente anterior
        """
        periodos = Periodo.objects.filter(perfil=self)
        definido_por_usuario = periodos.count() > 1

        # FIXME: periodos[0] siempre existe? Si no está, uso lapso
        # FIXME: chequear q no me pase del init_desde si existiera
        if definido_por_usuario:
            # si estoy comparando dos periodos, utilizar las fechas
            # desde y hasta de ese periodo para el comparativo de
            # filtros activados por el usuario
            desde_anterior = periodos[1].fecha_desde
            hasta_anterior = periodos[1].fecha_hasta
        else:
            # genera las fechas para el periodo de la misma longitud
            # inmediatamente anterior
            desde_anterior, hasta_anterior = periodos[0].get_fechas_periodo_anterior()

        comparativo = {'fecha__gte': desde_anterior,
                        'fecha__lte': hasta_anterior}

        return comparativo, definido_por_usuario

    # ---> Filtros
    # Las siguientes 3 funciones todas pueden recibir
    # el nombre del tipo filtro a crear,
    # o directamente el objeto ContentType
    def get_filtro(self, key):
        if key == "otramarca":
            tipo = ContentType.objects.get(name="marca")
        elif key in ContentType.objects.all():
            tipo = key
        else:
            tipo = ContentType.objects.get(name=formatear_clave(key))

        # FIXME: ver por qué esto podría devolver dos filtros
        filtro = Filtro.objects.filter(perfil=self, tipo=tipo)
        if key == "otramarca":
            filtro = filtro.filter(top=True)

        if len(filtro) > 0:
            filtro = filtro[0]
        else:
            filtro = None

        return filtro

    def crear_filtro(self, key):
        if key == "otramarca":
            tipo = ContentType.objects.get(name="marca")
        elif key in ContentType.objects.all():
            tipo = key
            key = tipo.name
        else:
            tipo = ContentType.objects.get(name=formatear_clave(key))

        filtro = Filtro.objects.create(perfil=self, tipo=tipo)
        if key == "otramarca":
            filtro.top = True
            filtro.save()
        if key == "producto":
            filtro.cantidadActual = 4
            filtro.incremento_en_cantidad = 4
            filtro.save()
        if "etiqueta" in key:
            filtro.cantidadActual = 10
            filtro.incremento_en_cantidad = 10
            filtro.save()

        return filtro

    def get_or_crear_filtro(self, key):
        filtro = self.get_filtro(key)
        if not filtro:
            filtro = self.crear_filtro(key)

        return filtro

    def crear_filtros(self):
        for key in FILTROS:
            filtro = self.get_or_crear_filtro(key)
            if key == "marca" or key == "soporte":
                filtro.abierto = True
            filtro.save()

    def add_filter(self, key, data):
        """
            Agrego una opción activa a un filtro
        """
        self.setear_cantidades()

        if key not in ['periodo', 'fecha_desde', 'fecha_hasta', 'fechas']:
            if int(data) == -1:
                self.remove_filter(key, data)
            else:
                filtro = self.get_filtro(key)
                # Crea la opción solo si no está activada ya
                opcion = OpcionFiltro.objects.filter(filtro=filtro, opcion_id=data, inicial=False)
                if not opcion:
                    opcion = OpcionFiltro(filtro=filtro, inicial=False)
                    opcion.opcion_id = data
                    opcion.save()
        else:
            periodo = self.get_periodo_or_create()
            if key == 'fecha_desde':
                periodo.lapso = ""
                periodo.set_fecha_desde(data)
            elif key == 'fecha_hasta':
                periodo.lapso = ""
                periodo.set_fecha_hasta(data)
            elif key in ["periodo", "fechas"]:
                periodos = Periodo.objects.filter(perfil=self)
                if len(periodos) == 2:
                    periodos[1].delete()

                if key == "periodo":
                    periodo.lapso = data[0].upper()
                    periodo.actualizar_fechas()
                elif key == 'fechas':
                    desdeValue, hastaValue = data.strip().split(" - ")
                    periodo.set_fecha_desde(desdeValue)
                    periodo.set_fecha_hasta(hastaValue)
                    periodo.lapso = None
                    # import ipdb; ipdb.set_trace()
            periodo.save()

    def remove_filter(self, key, opcion_id):
        """ borro una opción de un filtro que estaba seleccionada
            si recibe -1 como opcion_id, desactiva todas las opciones del filtro
            que no sean iniciales
        """
        filtro = self.get_filtro(key)

        # FIXME: debería haber una sola opción con ese id,
        # pero hay varias en algunos casos, por eso el filter
        if opcion_id != -1 and opcion_id != '-1':
            opciones_a_borrar = OpcionFiltro.objects.filter(filtro=filtro,
                                        opcion_id=opcion_id, inicial=False)
        else:
            opciones_a_borrar = OpcionFiltro.objects.filter(filtro=filtro,
                                                            inicial=False)

        for opcion in opciones_a_borrar:
            if not opcion.inicial:
                opcion.delete()

        # Si estaba haciendo una comparación de opciones de este filtro,
        # y quedó sólo una opción seleccionada, se desactiva la comparación
        if self.comparado:
            filtro_comparado = self.get_filtro(key=self.comparado)
            if filtro_comparado == filtro:
                opciones_restantes = OpcionFiltro.objects.filter(filtro=filtro)
                if len(opciones_restantes) < 2:
                    self.comparado = None
                    self.save()

    def reset_filters(self):
        """ borro un filtro que tenia asignado"""
        filtros = Filtro.objects.filter(perfil=self)
        tipo_ciudad = ContentType.objects.get(name='ciudad')

        for filtro in filtros:
            if filtro.tipo != tipo_ciudad:
                opciones = OpcionFiltro.objects.filter(filtro=filtro)
                for opcion in opciones:
                    if not opcion.inicial or self.user.is_staff:
                        opcion.delete()

        self.comparado = None
        self.save()
        self.setear_cantidades()

    def get_ordered_filters(self):
        """devuelve una lista de Strings con el orden de los filtros"""
        filters = self.orden.split(",")
        adicionales = ["etiqueta medio", "etiqueta programa", "etiqueta producto", "otramarca"]
        for adicional in adicionales:
            if not adicional in filters:
                filters.append(adicional)
        if "grupomedio" in filters:
            filters.remove("grupomedio")
        if "anunciante" in filters:
            filters.remove("anunciante")
        return filters

    def set_ordered_filters(self, orden):
        """ Obtengo el string separado por comas con el orden
            de los filtros y lo seteo"""
        #Verifico que el largo del string sea el mismo
        self.orden = orden
        self.save()

    def set_open_filter(self, key):
        """ seteo el estado del filtro a abierto o cerrado segun el bool"""
        filtro = self.get_filtro(key)
        filtro.set_abierto(not filtro.abierto)
        filtro.save()

    def filtros_to_dict(self, iniciales, con_fechas=True):
        """
            Genera un diccionario con los filtros activos y sus correspondientes
            opciones seleccionadas.

            Puedo pedir o sólo los iniciales, o también los activos en la sesión actual

            Cuando pedis los iniciales te agrega el init_fecha_desde
            y el init_fecha_hasta en los filtros
        """
        res = {}
        filtros = Filtro.objects.filter(perfil=self)
        for filtro in filtros:
            opciones = OpcionFiltro.objects.filter(filtro=filtro, inicial=iniciales)
            if opciones:
                nombre_filtro = str(filtro.tipo)
                # remuevo duplicados
                ids_opciones = list(set([item.opcion_id for item in opciones]))
                res[nombre_filtro] = ids_opciones

        # FIXME: revisar manejo de periodos
        if con_fechas:
            periodo = self.get_periodo_or_create()
            if iniciales:
                if periodo.init_fecha_desde:
                    res["fecha_desde"] = periodo.init_fecha_desde
                if periodo.init_fecha_hasta:
                    res["fecha_hasta"] = periodo.init_fecha_hasta
            else:
                res["fecha_desde"] = periodo.fecha_desde
                res["fecha_hasta"] = periodo.fecha_hasta
            # FIXME: dónde se valida el periodo seleccionado?
            # fecha_desde y fecha_hasta deberían tener un seter
            # que asegure q son válidas

            # FIXME: ver que función cumple esto (el periodo se agrega
            # en realidad a través de fecha_desde y fecha_hasta)
            # lapso = periodo.get_lapso_display()
            # if lapso:
            #     res["periodo"] = lapso

        return res


class Filtro(models.Model):
    """ Marca, Producto, Rubro, etc, para un perfil dado """
    perfil = models.ForeignKey("Perfil", related_name="filtros")
    cantidadActual = models.IntegerField(default=5)
    incremento_en_cantidad = models.IntegerField(default=5)
    abierto = models.BooleanField(default=True)
    top = models.BooleanField(default=False)
    #orden
    tipo = models.ForeignKey(ContentType)
    nivel = models.IntegerField(default=1, null=True)

    def __unicode__(self):
        return u"%s de %s" % (self.tipo, self.perfil)

    def name(self):
        if str(self.tipo).lower() == "marca" and self.top:
            return "otramarca"
        else:
            return str(self.tipo).lower()

    def clonar(self):
        # Devuelve una copia del filtro, no asigna el perfil ni guarda
        filtro = Filtro(perfil=self.perfil, tipo=self.tipo)
        cantidadActual = self.cantidadActual,
        incremento_en_cantidad = self.incremento_en_cantidad
        filtro.abierto = self.abierto
        filtro.top = self.top
        filtro.save()

        return filtro

    def set_abierto(self, esta_abierto):
        self.abierto = esta_abierto
        self.save()

    def lookup(self):
        """ TODO: Usar constante de views """
        return ""

    def reverse(self):
        return ""

    def has_more_options(self):
        """ devuelve si tiene o no mas opciones el filtro"""
        # FIXME: no debería tener en cuenta sólo las opciones de la búsqueda actual?
        # opciones = OpcionFiltro.objects.filter(filtro=self)
        # return self.cantidadActual < len(opciones)
        return True

    def has_less_options(self):
        """ devuelve si es posible quitar opciones visibles del filtro"""
        # return filtro.cantidad - 5 > 0
        return True

    # Si seteo con multiplos de 5 no puedo saber cuantos en realidad esta viendo
    # entonces puede traer problemas para el boton more
    def more_options(self):
        """ aumento el contador de ese filtro,
            deberia llamarse previamente a has_more_options """
        self.cantidadActual += self.incremento_en_cantidad
        self.save()

    # Si seteo con multiplos de 5 no puedo saber cuantos en realidad esta viendo
    # entonces puede traer problemas para el boton less
    def less_options(self):
        """ decremento el contador de ese filtro, devuelvo la cantidad actual"""
        self.cantidadActual =\
                    max(0, self.cantidadActual - self.incremento_en_cantidad)
        self.save()
        return self.cantidadActual


class OpcionFiltro(models.Model):  # Este estoy viendo actualmente
    """
    p = Perfil de "pepito" (unico para pepito)
    f =  Filtro "Marca" de "p" (uno de Marca, otro de Rubro, etc)
    c1 = opcion "Coca Cola" de "f"
    c2 = opcion "Pepsi" de "f"
    """
    filtro = models.ForeignKey(Filtro, related_name="opciones")
    opcion_id = models.PositiveIntegerField()
    inicial = models.BooleanField(default=False)

    def get_instance(self):
        return self.filtro.tipo.model.objects.get(id=self.opcion_id)

    def clonar(self):
        # Copia un opcion filtro, no asigna el filtro ni lo crea
        opcion = OpcionFiltro(opcion_id=self.opcion_id, inicial=self.inicial)
        return opcion

    def __unicode__(self):
        return "%s: id - %s" % (self.filtro.id, self.opcion_id)


class Periodo(models.Model):
    LAPSO_CHOICES = (
        ('A', 'año'),
        ('M', 'mes'),
        ('S', 'semana'),
        ('D', 'dia'),
    )

    lapso = models.CharField(max_length=1, choices=LAPSO_CHOICES, null=True, blank=True)
    perfil = models.ForeignKey(Perfil, related_name="periodos")
    init_fecha_desde = models.DateField(null=True, blank=True)
    init_fecha_hasta = models.DateField(null=True, blank=True)
    fecha_desde = models.DateField(null=True, blank=True)
    fecha_hasta = models.DateField(null=True, blank=True)
    hora_desde = models.TimeField(null=True, blank=True)
    hora_hasta = models.TimeField(null=True, blank=True)

    def __unicode__(self):
        return u"%s %s" % (self.perfil, self.lapso)

    def get_label(self):
        fecha_desde = self.fecha_desde.strftime("%d/%m/%Y")
        fecha_hasta = self.fecha_hasta.strftime("%d/%m/%Y")
        if fecha_desde != fecha_hasta:
            return "{0} al {1}".format(fecha_desde, fecha_hasta)
        else:
            return fecha_desde

    def validar_fecha(self, fecha):
        """
            Dada una fecha la valida según los permisos del usuario,
            y la ajusta en caso de que se escape de los límites
        """
        if type(fecha) != datetime:
            fecha = datetime.strptime(fecha, '%d/%m/%Y').date()
        if self.init_fecha_desde:
           fecha = max(fecha, self.init_fecha_desde)
        if self.init_fecha_hasta:
            fecha = min(fecha, self.init_fecha_hasta)
        return fecha

    def set_fecha_desde(self, fecha):
        self.fecha_desde = self.validar_fecha(fecha).strftime("%Y-%m-%d")

    def set_fecha_hasta(self, fecha):
        self.fecha_hasta = self.validar_fecha(fecha).strftime("%Y-%m-%d")

    def longitud(self):
        return (self.fecha_hasta - self.fecha_desde).days

    # TODO: init periodo
    def validar(self):
        if not self.fecha_desde:
            self.fecha_desde = self.init_fecha_desde
        if not self.fecha_hasta:
            self.fecha_hasta = self.init_fecha_hasta
        desde, hasta = self.fecha_desde, self.fecha_hasta
        if self.init_fecha_desde:
            desde = max(desde, self.init_fecha_desde)
            hasta = max(hasta, self.init_fecha_desde)
        if self.init_fecha_hasta:
            desde = min(desde, self.init_fecha_hasta)
            hasta = min(hasta, self.init_fecha_hasta)

        self.fecha_desde, self.fecha_hasta = desde, hasta

    def actualizar_fechas(self):
        # Este metodo actualiza el periodo segun el lapso elegido
        lapso = self.get_lapso_display()
        if lapso:
            self.fecha_desde, self.fecha_hasta = get_fechas_periodo_actual(lapso)
            self.validar()
            self.save()


    def clonar(self):
        # Devuelve una copia del periodo,
        # no asigna el perfil ni guarda
        periodo = Periodo(hora_desde=self.hora_desde, hora_hasta=self.hora_hasta,
            fecha_desde=self.fecha_desde, fecha_hasta = self.fecha_hasta)
        periodo.init_fecha_desde = self.init_fecha_desde
        periodo.init_fecha_hasta = self.init_fecha_hasta
        periodo.lapso = self.lapso

        return periodo

    def get_fechas_periodo_anterior(self):
        self.actualizar_fechas()
        desde = self.fecha_desde
        hasta = self.fecha_hasta
        if desde != None and hasta != None:
            distancia = hasta - desde
            hasta_anterior = desde - timedelta(days=1)
            desde_anterior = hasta_anterior - distancia

            return desde_anterior, hasta_anterior
        else:
            return desde, hasta

def get_fechas_periodo_actual(lapso):
    """ Fechas desde y hasta correspondiente a un periodo """
    if lapso == u"dia":
        dias = 1
    elif lapso == u"semana":
        dias = 7
    elif lapso == u"mes":
        dias = 30
    elif lapso == u"año":
        dias = 365

    # FIXME: considerar hora cuando dias == 1
    hoy = date.today()
    desde = hoy - timedelta(days=dias - 1)

    if lapso == u"dia":
        desde = date.today() - timedelta(hours=24)
        hoy = desde

    return desde, hoy
