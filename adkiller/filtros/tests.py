# -*- coding: utf-8 -*-
# Python imports
import simplejson
from datetime import date, datetime, timedelta
import re
import autocomplete_light
autocomplete_light.autodiscover()
import mock

# Django imports
from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.contenttypes.models import ContentType
from django.test.client import Client
from django.core import management

# Project imports
from adkiller.app.tests import InfoadTestCase, FakeDate
from adkiller.app.views import set_unit, compare_filter, home
from adkiller.filtros.models import Filtro, Perfil, OpcionFiltro, Periodo
from adkiller.app.models import Rubro, Marca, Sector, Industria, \
        Producto, Ciudad, Medio, Programa, Emision, Soporte, Anunciante, \
        Horario
from adkiller.filtros.views import add_filter, get_ordered_filters, \
        set_ordered_filters, get_filters, remove_filter, graph, \
        get_options, lookup_filtro_general, more_options, less_options,\
        open_filter, crear_busqueda, reset_filters, get_busquedas,\
        eliminar_busqueda, get_busqueda, get_alerta

WHITESPACE_PATTERN = re.compile(r'\s+')

def remove_whitespace(string):
    """Removes all whitespace and line breaks in string"""
    return re.sub(WHITESPACE_PATTERN, '', string)


@mock.patch('adkiller.filtros.views.date', FakeDate)
@mock.patch('adkiller.filtros.models.date', FakeDate)
class BusquedasTest(InfoadTestCase):
    """
        También se testean aquí las funcionalidades de
        salvar y cargar búquedas
    """
    def crear_emision(self, producto, horario, fecha, duracion, precio_minuto):
        params = {
             "producto": producto,
             "horario": horario,
             "fecha": fecha,
             "valor": duracion*precio_minuto,
             "duracion": duracion,
             "hora": "12:01",
             "orden": 1,
            }

        return Emision.objects.create(**params)

    def crear_busqueda_y_guardar(self, dict_filtros, nombre):
        data = {}
        for (filtro, opcion) in dict_filtros.items():
            data['filter'] = filtro
            data['value'] = opcion
            self.client.get(reverse(add_filter), data)
        self.client.get(reverse(crear_busqueda), {"nombre": nombre})

    def setUp(self):
        InfoadTestCase.setUp(self)

        # Piso el today
        FakeDate.today = classmethod(lambda cls: date(2012, 12, 31))

        # Creo filtros para el perfil de usuario
        self.usuario.get_profile().crear_filtros()

        # Creo varias emisiones, desparramadas sobre todo el año 2012
        # para poder testear sobre distintos periodos

        # Los precios por minuto crecen con los meses
        # El minuto del 12 cuesta el doble que el del 10
        # Las propagandas de Coca-Cola duran el doble que las de Chizitos
        for i in range(12):
            for j in range(4):
                mes = i + 1; semana = j + 1; ppm = i + 1
                fecha = datetime(2012, mes, 7 * semana)
                self.crear_emision(self.cocacolalife, self.horario_del_10, fecha,
                                        duracion=2, precio_minuto=ppm)
                self.crear_emision(self.chizitos, self.horario_del_10, fecha,
                                        duracion=1, precio_minuto=ppm)
                self.crear_emision(self.cocacolalife, self.horario_del_12, fecha,
                                        duracion=2, precio_minuto=2 * ppm)
                self.crear_emision(self.chizitos, self.horario_del_12, fecha,
                                        duracion=1, precio_minuto=2 * ppm)

    def test_guardar_y_cargar_busqueda_con_lapso(self):
        """
            Cuando se especifica un lapso (última semana, mes, etc.),
            en la creación de una búsqueda, este debe actualizarse
            cada vez que se carga la búsqueda
        """
        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # agrego algunos filtros
        data = {}
        data['filter'] = "marca"
        data['value'] = str(self.pepsico.id)
        self.client.get(reverse(add_filter), data)

        data['filter'] = "medio"
        data['value'] = str(self.canal10.id)
        self.client.get(reverse(add_filter), data)

        data['filter'] = "periodo"
        data['value'] = "semana"
        self.client.get(reverse(add_filter), data)

        # guardo mi búsqueda
        data = {"nombre": "chizitos en el 10"}
        self.client.get(reverse(crear_busqueda), data)

        # borro todo
        self.client.get(reverse(reset_filters))

        # me deslogueo
        self.client.logout()

        # pasan unos días
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 7))

        # me logueo de nuevo
        self.client.login(username="non_staff", password="yy")

        # leo la lista de busquedas
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # vuelvo a cargar la que cree
        mi_busqueda = data_busquedas[0]
        id_busqueda, nombre_busqueda, estado_busqueda =\
        mi_busqueda[0], mi_busqueda[1], mi_busqueda[2]

        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda) + "/")

        # verifico que se cargaron bien los filtros
        response = self.client.get(reverse(get_filters))
        filters_dict = simplejson.loads(response.content)
        self.assertEquals(len(filters_dict), 2)
        self.assertEquals(["marca", "medio"], filters_dict.keys())
        self.assertContains(response, "Pepsico Snacks")
        self.assertContains(response, "canal 10")

        # verifico que se actualizó el lapso
        response = self.client.get("/app/fechas/")
        self.assertEquals(response.content, '"01/06/2012 - 07/06/2012"')

    def test_guardar_y_cargar_busqueda_sin_lapso(self):
        """
            Cuando una búsqueda se crea sin explicitar un lapso,
            se la considera de periodo fijo, y debe cargarse siempre
            con el mismo periodo, sin tener en cuenta el paso de los días
        """
        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # agrego algunos filtros
        data = {}
        data['filter'] = "marca"
        data['value'] = str(self.pepsico.id)
        self.client.get(reverse(add_filter), data)

        # defino un periodo fijo
        data['filter'] = "fecha_desde"
        data['value'] = "21/03/2012"
        self.client.get(reverse(add_filter), data)

        data['filter'] = "fecha_hasta"
        data['value'] = "20/06/2012"
        self.client.get(reverse(add_filter), data)

        # guardo mi búsqueda
        data = {"nombre": "chizitos en otoño"}
        self.client.get(reverse(crear_busqueda), data)

        # borro todo
        self.client.get(reverse(reset_filters))

        # me deslogueo
        self.client.logout()

        # pasan tres meses
        FakeDate.today = classmethod(lambda cls: date(2012, 9, 1))

        # me logueo de nuevo
        self.client.login(username="non_staff", password="yy")

        # leo la lista de busquedas
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # vuelvo a cargar la que cree
        mi_busqueda = data_busquedas[0]
        id_busqueda, nombre_busqueda, estado_busqueda =\
        mi_busqueda[0], mi_busqueda[1], mi_busqueda[2]

        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda) + "/")

        # verifico que se cargaron bien los filtros
        response = self.client.get(reverse(get_filters))
        filters_dict = simplejson.loads(response.content)
        self.assertEquals(len(filters_dict), 1)
        self.assertEquals(["marca"], filters_dict.keys())
        self.assertContains(response, "Pepsico Snacks")

        # verifico que las fechas no cambiaron
        response = self.client.get("/app/fechas/")
        self.assertEquals(response.content, '"21/03/2012 - 20/06/2012"')

    def test_menu_busquedas(self):
        """
            Creo tres búsquedas diferentes, veo que se muestren en el menú
            Elimino uno, me aseguro de que no aparezca
        """
        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo tres búsquedas distintas
        dict_filtros = {"marca": str(self.cocacola.id),
                        "medio": str(self.canal12.id),
                        "periodo": "mes"
        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="Cocacola en el 12")
        self.client.get(reverse(reset_filters))

        dict_filtros["marca"] = str(self.pepsico.id)
        self.crear_busqueda_y_guardar(dict_filtros, nombre="Chizitos en el 12")
        self.client.get(reverse(reset_filters))

        dict_filtros["medio"] = str(self.canal10.id)
        dict_filtros["periodo"] = "semana"
        self.crear_busqueda_y_guardar(dict_filtros, nombre="Chizitos semanales en el 10")

        # me deslogueo
        self.client.logout()

        # me logueo de nuevo
        self.client.login(username="non_staff", password="yy")

        # FIXME: asegurarme que esto es lo que lee el menú
        # leo la lista de busquedas
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 3)

        # vuelvo a cargar la 2da que cree
        for data in data_busquedas:
            if data[1] == "Chizitos en el 12":
                id_2da = data[0]

        self.client.get("/filtros/cargar_busqueda/" + str(id_2da) + "/")

        # verifico que se cargaron bien los filtros
        response = self.client.get(reverse(get_filters))
        filters_dict = simplejson.loads(response.content)
        self.assertEquals(["marca", "medio"], filters_dict.keys())
        self.assertContains(response, "Pepsico Snacks")
        self.assertContains(response, "canal 12")

        # chequeo que sólo la 2da esté marcada activa
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 3)

        for data in data_busquedas:
            self.assertEquals(data[2], data[0] == id_2da)

    def test_eliminar_busqueda(self):
        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo 2 búsquedas
        dict_filtros = {"marca": str(self.cocacola.id),
                        "periodo": "mes"
        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="cocacola este mes")
        self.client.get(reverse(reset_filters))

        dict_filtros = {"marca": str(self.pepsico.id),
                        "periodo": "dia"
                        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="chizitos hoy")

        # leo la lista de busquedas, deberia haber dos
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 2)

        # elimino la primer búsqueda
        data = data_busquedas[0]
        for i in range(len(data_busquedas)):
            data = data_busquedas[i]
            if data[1] == "cocacola este mes":
                id_mi_busq = data[0]

        self.client.get(reverse(eliminar_busqueda), {"id": str(id_mi_busq)})

        # leo de nuevo la lista de busquedas, deberia haber solo una
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)
        self.assertContains(response, "chizitos hoy")


    def test_sobreescribir_busqueda(self):
        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo una búsquedas
        dict_filtros = {"marca": str(self.cocacola.id),
                        "periodo": "mes"
        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="mi busqueda")
        self.client.get(reverse(reset_filters))

        dict_filtros = {}
        dict_filtros["medio"] = str(self.canal10.id)
        dict_filtros["fecha_desde"] = "01/07/2012"
        dict_filtros["fecha_hasta"] = "30/07/2012"
        self.crear_busqueda_y_guardar(dict_filtros, nombre="mi busqueda")
        self.client.get(reverse(reset_filters))

        # leo la lista de busquedas, deberia haber solo una
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # cargo la búsqueda
        data = data_busquedas[0]
        if data[1] == "mi busqueda":
            id_mi_busq = data[0]

        self.client.get("/filtros/cargar_busqueda/" + str(id_mi_busq) + "/")

        # verifico q se cargó la ultima q cree
        response = self.client.get(reverse(get_filters))
        filters_dict = simplejson.loads(response.content)
        self.assertEquals(["medio"], filters_dict.keys())
        self.assertContains(response, "canal 10")

        response = self.client.get("/app/fechas/")
        self.assertEquals(response.content, '"01/07/2012 - 30/07/2012"')

    def test_editar_busqueda(self):

        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo dos búsquedas
        dict_filtros = {"marca": str(self.cocacola.id),
                        "periodo": "mes"
        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="busqueda1")
        self.client.get(reverse(reset_filters))

        dict_filtros = {"medio": str(self.canal10.id),
                        "periodo": "semana"
        }

        self.crear_busqueda_y_guardar(dict_filtros, nombre="busqueda2")
        self.client.get(reverse(reset_filters))

        # leo la lista de busquedas, deberia haber 2
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 2)

        # cargo la búsqueda1
        id_busqueda1 = data_busquedas[0][0]

        # clickeo en editar busqueda
        # se carga la busqueda
        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda1) + "/")

        # se busca los datos de la busqueda
        response = self.client.get(reverse(get_busqueda), {'id':  str(id_busqueda1)})
        data_busquedas = simplejson.loads(response.content)

        # se busca las alertas
        response = self.client.get(reverse(get_alerta), {'perfil':  str(id_busqueda1)})

        # no deberia tener
        self.assertEqual(response.content, "sin alertas")

        # creo una busqueda con alerta
        params = {'nombre': 'cantidad diaria < 4', 'alertar': 'on',
                'variable': 'inversion', 'comparador': 'menor',
                'variable_valor': '10', 'periodo': 'dia'}
        self.client.get(reverse(crear_busqueda), params)

        # leo la lista de busquedas, deberia haber 3
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 3)

        # cargo la cantidad diaria < 4
        id_busqueda1 = data_busquedas[2][0]

        # clickeo en editar busqueda
        # se carga la busqueda
        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda1) + "/")

        # se busca los datos de la busqueda
        response = self.client.get(reverse(get_busqueda), {'id':  str(id_busqueda1)})
        data_busquedas = simplejson.loads(response.content)

        # se busca las alertas
        response = self.client.get(reverse(get_alerta), {'perfil':  str(id_busqueda1)})
        print response.content
        data_alertas = simplejson.loads(response.content)

        # deberia haber una
        self.assertEqual(len(data_alertas), 1)

    def test_crear_busqueda_alerta_producto(self):

        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo una busqueda con alerta producto
        params = {'nombre': 'cantidad diaria < 4', 'alertar_producto':'true'}
        self.client.get(reverse(crear_busqueda), params)

        # leo la lista de busquedas, deberia haber 1
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # cargo la búsqueda
        id_busqueda1 = data_busquedas[0][0]

        # se carga la busqueda
        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda1) + "/")

        # se busca los datos de la busqueda
        response = self.client.get(reverse(get_busqueda), {'id':  str(id_busqueda1)})
        data_busquedas = simplejson.loads(response.content)

        # deberia haber una
        self.assertEqual(len(data_busquedas), 1)

        # alerta_producto debe ser True
        self.assertEqual(Perfil.objects.get(pk=id_busqueda1).alerta_producto, True)

    def test_editar_busqueda_agregar_alerta_producto(self):

        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        # creo una busqueda con alerta
        params = {'nombre': 'busqueda1'}
        self.client.get(reverse(crear_busqueda), params)

        # leo la lista de busquedas, deberia haber 1
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # cargo la búsqueda
        id_busqueda1 = data_busquedas[0][0]

        # se carga la busqueda
        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda1) + "/")

        # se busca los datos de la busqueda
        response = self.client.get(reverse(get_busqueda), {'id':  str(id_busqueda1)})
        data_busquedas = simplejson.loads(response.content)

        # se busca las alertas
        response = self.client.get(reverse(get_alerta), {'perfil':  str(id_busqueda1)})

        # no deberia tener
        self.assertEqual(response.content, "sin alertas")

        # alerta_producto debe ser False
        self.assertEqual(Perfil.objects.get(pk=id_busqueda1).alerta_producto, False)

        # Agrego alerta_producto
        params = {'nombre': 'busqueda1','alertar_producto':'true'}
        self.client.get(reverse(crear_busqueda), params)

        # leo la lista de busquedas, deberia haber 1
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)

        # cargo la búsqueda
        id_busqueda_modificada = data_busquedas[0][0]

        # se carga la busqueda
        self.client.get("/filtros/cargar_busqueda/" + str(id_busqueda_modificada) + "/")

        # alerta_producto debe ser True
        self.assertEqual(Perfil.objects.get(pk=id_busqueda_modificada).alerta_producto, True)


    # FIXME: por ahora no tiene en cuenta periodo
    #def test_resaltado_de_busqueda(self):
    #    """
    #        Cuando los filtros seleccionados por el usuario
    #        coinciden con la configuracion de una búsqueda salvada,
    #        el nombre de esta debería resaltarse en el menú de búsquedas

    #        Cuando no hay coincidencia, debe mostrar
    #        "Seleccionar búsqueda guardada"
    #    """

    #    # piso la fecha de hoy
    #    FakeDate.today = classmethod(lambda cls: date(2012, 6, 1))

    #    # me logueo como usuario
    #    self.client.login(username="non_staff", password="yy")

    #    # creo una búsquedas
    #    dict_filtros = {"marca": str(self.cocacola.id),
    #                    "periodo": "mes"
    #    }

    #    self.crear_busqueda_y_guardar(dict_filtros, nombre="mi busqueda")
    #    self.client.get(reverse(reset_filters))

    #    # agrego un filtro igual
    #    data = {}
    #    data['filter'] = "marca"
    #    data['value'] = str(self.cocacola.id)
    #    self.client.get(reverse(add_filter), data)

        # leo la lista de busquedas
    #    response = self.client.get(reverse(get_busquedas))
    #    data_busquedas = simplejson.loads(response.content)

        # cargo la búsqueda
    #    data = data_busquedas[0]
    #    if data[1] == "mi busqueda":
    #        id_mi_busq = data[0]

    #    response = self.client.get(reverse(home))
    #    self.assertContains(response, '<a href="/filtros/cargar_busqueda/'+ str(id_mi_busq) +'/" class="busqueda">mi busqueda</a>', status_code=200)

    '''def test_acentos_lookup_filtro_general(self):
        # creo un sector
        rubro = Rubro.objects.create(nombre="rubro")
        industria = Industria.objects.create(rubro=rubro, nombre="industria")
        sector = Sector.objects.create(industria=industria, nombre="Venta Telefónica")

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        response = self.client.get(reverse(lookup_filtro_general), {'texto': u'Venta Telefonica'})
        data = simplejson.loads(response.content)
        self.assertEqual(data[0]['value'], u'Venta Telefónica')

    def test_acentos_lookup_marcas_y_anunciantes(self):

        # creo una marca
        rubro = Rubro.objects.create(nombre="rubro")
        industria = Industria.objects.create(rubro=rubro, nombre="industria")
        sector = Sector.objects.create(industria=industria, nombre="Venta Telefónica")
        marca = Marca.objects.create(sector=self.sector, nombre="Asociación Argentina de Televisión por Cable")

        # creo un anunciante
        Anunciante.objects.create(nombre='Sindicato Argentino de Televisión')

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        response = self.client.get(reverse(lookup_filtro_general), {'texto': u'Television'})
        data = simplejson.loads(response.content)
        self.assertEqual(data[0]['value'], u'Asociación Argentina de Televisión por Cable')
        self.assertEqual(data[1]['value'], u'Sindicato Argentino de Televisión')'''


    def test_guardar_y_cargar_busqueda_sin_filtros(self):
        """
            Creamos una búsqueda sólo con restricciones de fecha
            y vemos q se guarden apropiadamente
        """

        # piso la fecha de hoy
        FakeDate.today = classmethod(lambda cls: date(2012, 6, 7))

        # me logueo como usuario
        self.client.login(username="non_staff", password="yy")

        dict_filtros = {"fecha_desde":"01/07/2012", "fecha_hasta":"30/07/2012"}

        self.crear_busqueda_y_guardar(dict_filtros, nombre="busqueda sin filtros")
        self.client.get(reverse(reset_filters))

        # me deslogueo
        self.client.logout()

        # me logueo de nuevo
        self.client.login(username="non_staff", password="yy")

        # leo la lista de busquedas, deberia haber una
        response = self.client.get(reverse(get_busquedas))
        data_busquedas = simplejson.loads(response.content)
        self.assertEqual(len(data_busquedas), 1)
        self.assertContains(response, "busqueda sin filtros")

        response = self.client.get("/app/fechas/")
        self.assertEquals(response.content, '"01/07/2012 - 30/07/2012"')


# FIXME: esta función parcha el problema de los ids en GetOptions,
# habría que ver por q se da y resolverlo
def remove_ids(string):
    """Removes all whitespace and line breaks in string"""
    id_pattern = re.compile(r'"id":\d+,')
    return re.sub(id_pattern, '', string)


# FIXME: ver por qué los ids cambian
class GetOptionsViewTest(InfoadTestCase):
    def setUp(self):
        InfoadTestCase.setUp(self)

        # Me logueo como usuario
        self.client.login(username="non_staff", password="yy")

    """Tests para la vista filtros.views.get_options"""
    def test_get_options_una_marca_una_emision(self):
        """Cuando solicito los filtros de marca deveria devolverme la marca,
        con id=1 y procentaje 100%"""
        # creo una emision
        fecha = datetime(2012, 07, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario,
                            producto=self.cocacolalife, hora='12:01', duracion=1,
                                orden=1, valor=1)

        response = self.client.get('/app/options/', {'data': 'marca'})
        expected_json = """[[{"variacion_porcentual": "-",
                              "absoluto_acotado": "1",
                              "valor": 1.0,
                              "comparando": true,
                              "item": "Coca Cola",
                              "porcentaje": "100.0",
                              "porcentaje_relativo": 100.0,
                              "porcentaje_valor": 100.0,
                              "id": 1,
                              "int_porcentaje_relativo": "100%"}],
                              false, false, "cantidad"]"""

        self.assertEqual(remove_ids(remove_whitespace(expected_json)),
                         remove_ids(remove_whitespace(response.content)))

    def test_get_options_dos_marcas_dos_emisiones(self):
        """Cuando solicito los filtros de marca deveria devolverme las
           dos marcas, con id=1 e id=2 procentajes 50% c/u"""
        # crear dos emisiones para dos productos distintos
        fecha = datetime(2012, 07, 01)
        Emision.objects.create(fecha=fecha, horario=self.horario,
                producto=self.cocacolalife, hora='12:01', duracion=1, orden=1, valor=1)
        Emision.objects.create(fecha=fecha, horario=self.horario,
                producto=self.chizitos, hora='12:01', duracion=1, orden=1, valor=1)


        response = self.client.get('/app/options/', {'data': 'marca'})
        expected_json = """[[{"variacion_porcentual":"-",
                              "absoluto_acotado":"1",
                              "valor":1.0,
                              "comparando":true,
                              "item":"Coca Cola",
                              "porcentaje":"50.0",
                              "porcentaje_relativo":100.0,
                              "porcentaje_valor":50.0,
                              "id":7,
                              "int_porcentaje_relativo":"100%"},
                             {"variacion_porcentual":"-",
                              "absoluto_acotado":"1",
                              "valor":1.0,
                              "comparando":true,
                              "item":"Pepsico Snacks",
                              "porcentaje":"50.0",
                              "porcentaje_relativo":100.0,
                              "porcentaje_valor":50.0,
                              "id":8,
                              "int_porcentaje_relativo":"100%"}
                            ],false,false,"cantidad"]"""

        self.assertEqual(remove_ids(remove_whitespace(expected_json)),
                         remove_ids(remove_whitespace(response.content)))

    def test_get_options_perfil_con_periodo_init_desde_y_hasta(self):
        """
            Cuando solicito los filtros de marca deveria devolverme la marca,
            con id=1 y procentaje 100%
        """
        # cargo el perfil y limpio periodos
        perfil = self.usuario.get_profile()
        perfil.set_unit("cantidad")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # crear un periodo con init_fecha_desde e init_fecha_hasta
        init_fecha_desde = datetime(2012, 7, 11)
        init_fecha_hasta = datetime(2012, 7, 16)
        fecha_desde= datetime(2012, 7, 14)
        fecha_hasta = init_fecha_hasta
        Periodo.objects.create(perfil=perfil,
                               init_fecha_desde=init_fecha_desde,
                               init_fecha_hasta=init_fecha_hasta,
                               fecha_desde=fecha_desde,
                               fecha_hasta=fecha_hasta)

        # crear una marca con un producto
        producto = self.chizitos

        # crear cuatro emisiones para el mismo producto
        fecha = datetime(2012, 7, 12)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='12:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 15)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='12:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 15)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='13:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 15)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='14:01', duracion=1, orden=1, valor=1)

        response = self.client.get('/app/options/', {'data': 'marca'})
        expected_json = """[[{"variacion_porcentual": "200",
                              "absoluto_acotado": "3",
                              "valor": 3.0,
                              "comparando": true,
                              "item": "Pepsico Snacks",
                              "porcentaje": "100.0",
                              "porcentaje_relativo": 100.0,
                              "porcentaje_valor": 100.0,
                              "id": 10,
                              "int_porcentaje_relativo": "100%"}],
                              false, false, "cantidad"]"""
        self.assertEqual(remove_ids(remove_whitespace(expected_json)),
                         remove_ids(remove_whitespace(response.content)))

    def test_get_options_con_variacion_negativa(self):
        """Cuando solicito los filtros de marca deveria devolverme la marca, con id=1 y procentaje 100%"""
        # cargo el perfil y limpio periodos
        perfil = self.usuario.get_profile()
        perfil.set_unit("cantidad")
        for p in Periodo.objects.filter(perfil=perfil):
            p.delete()

        # crear un periodo con init_fecha_desde e init_fecha_hasta
        init_fecha_desde = datetime(2012, 7, 1)
        init_fecha_hasta = datetime(2012, 7, 6)
        fecha_desde= datetime(2012, 7, 4)
        fecha_hasta = datetime(2012, 7, 6)
        Periodo.objects.create(perfil=perfil,
                               init_fecha_desde=init_fecha_desde,
                               init_fecha_hasta=init_fecha_hasta,
                               fecha_desde=fecha_desde,
                               fecha_hasta=fecha_hasta)


        # Elijo un producto
        producto = self.cocacolalife

        # crear cuatro emisiones para este producto
        fecha = datetime(2012, 7, 1)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='12:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 2)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='12:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 3)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='13:01', duracion=1, orden=1, valor=1)

        fecha = datetime(2012, 7, 4)
        Emision.objects.create(fecha=fecha, horario=self.horario, producto=producto,
                               hora='14:01', duracion=1, orden=1, valor=1)

        response = self.client.get('/app/options/', {'data': 'marca'})
        expected_json = """[[{"variacion_porcentual": "-67",
                              "absoluto_acotado": "1",
                              "valor": 1.0,
                              "comparando": true,
                              "item": "Coca Cola",
                              "porcentaje": "100.0",
                              "porcentaje_relativo": 100.0,
                              "porcentaje_valor": 100.0,
                              "id": 5,
                              "int_porcentaje_relativo": "100%"}],
                              false, false, "cantidad"]"""
        self.assertEqual(remove_ids(remove_whitespace(expected_json)),
                         remove_ids(remove_whitespace(response.content)))

    def test_get_options_varias_opciones_un_tipo_filtro(self):
        pass


    # -------> Pendientes - FIX
    # def test_get_more_options(self):
    #     data = {}
    #     data['data'] = "sector"
    #     perfil = self.root.get_profile()
    #     filtro = perfil.get_filtro("sector")
    #     antes = filtro.cantidadActual
    #     self.assertEqual(antes, 5)
    #     response = self.client.get(reverse(more_options), data)
    #     perfil = self.root.get_profile(); filtro = perfil.get_filtro("sector")
    #     self.assertEqual(filtro.cantidadActual, antes + filtro.incremento_en_cantidad)

    # def test_get_less_options(self):
    #     data = {}
    #     data['data'] = "sector"
    #     perfil = self.root.get_profile(); filtro = perfil.get_filtro("sector")
    #     antes = filtro.cantidadActual
    #     self.assertEqual(antes, 5)
    #     response = self.client.get(reverse(less_options), data)
    #     perfil = self.root.get_profile(); filtro = perfil.get_filtro("sector")
    #     self.assertEqual(filtro.cantidadActual,
    #                     antes - filtro.incremento_en_cantidad)

    # def test_get_more_options_barras_azules(self):
    #     """
    #         Chequeo que al traer nuevas opciones estas
    #         tengan barras menores a las que ya había
    #     """
    # -> creo 10 productos distintos
    # -> creo una emision para cada uno, con precios de 1 a 10
    # -> traigo 5 opciones
    # -> traigo 5 más
    # -> chequeo que las últimas tengan todas barras azules menores a las anteriores
    # -> chequeo que sumen menos de 100


    # def test_get_filtros_multiples(self):
    #     data = {}
    #     data['filter'] = "sector"
    #     data['value'] = "1"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(OpcionFiltro.objects.all().count(), 1)

    #     data['filter'] = "sector"
    #     data['value'] = "2"
    #     response = self.client.get(reverse(add_filter), data)
    #     self.assertEqual(OpcionFiltro.objects.all().count(), 2)

    #     data = {}
    #     data['data'] = True
    #     response = self.client.get(reverse(get_filters), data)
    #     self.assertContains(response, "Sector1")
    #     self.assertContains(response, "Sector2")

    # def test_get_options(self):
    #     """ verifico get_opciones para un sector con una emision"""
    #     p = Producto(marca=Marca.objects.get(nombre="id2"))
    #     p.save()
    #     Soporte(nombre="soporte").save()
    #     Ciudad(nombre="soporte").save()
    #     medio = Medio(soporte=Soporte.objects.get(),
    #                 ciudad=Ciudad.objects.get())
    #     medio.save()
    #     programa = Programa(nombre="programa", medio=medio)
    #     programa.save()

    #     hoy = date.today()
    #     dictio = {
    #          "producto": p,
    #          "programa": programa,
    #          "fecha": hoy,
    #          "hora": "12: 01",
    #          "orden": 1,
    #          "duracion": 1,
    #          "alto": 1,
    #          "ancho": 1,
    #          "superficie": 0,
    #          "seg": 1000,
    #          "valor": 15
    #     }

    #     Emision(**dictio).save()

    #     data = {}
    #     data['data'] = "sector"
    #     response = self.client.get(reverse(get_options), data)
    #     self.assertContains(response, "Sector1")

    # def test_get_options_letras_millones(self):
    #     """ verifico get_opciones para un sector con una emision
    #         esta vez forzando a que use los resumenes de letras"""
    #     p = Producto(marca=Marca.objects.get(nombre="id2"))
    #     p.save()
    #     Soporte(nombre="soporte").save()
    #     Ciudad(nombre="soporte").save()
    #     medio = Medio(soporte=Soporte.objects.get(),
    #                     ciudad=Ciudad.objects.get())
    #     medio.save()
    #     programa = Programa(nombre="programa", medio=medio)
    #     programa.save()

    #     hoy = date.today()
    #     dictio = {
    #      "producto": p,
    #      "programa": programa,
    #      "fecha": hoy,
    #      "hora": "12: 01",
    #      "orden": 1,
    #      "duracion": 1,
    #      "alto": 1,
    #      "ancho": 1,
    #      "superficie": 0,
    #      "seg": 100000,
    #      "valor": 150000
    #     }

    #     Emision(**dictio).save()

    #     data = {}
    #     data['data'] = "sector"
    #     response = self.client.get(reverse(get_options), data)
    #     self.assertContains(response, "Sector1")

    # def test_get_options_letras_miles(self):
    #     """ verifico get_opciones para un sector con una emision
    #         esta vez forzando a que use los resumenes de letras"""
    #     p = Producto(marca=Marca.objects.get(nombre="id2"))
    #     p.save()
    #     Soporte(nombre="soporte").save()
    #     Ciudad(nombre="soporte").save()
    #     medio = Medio(soporte=Soporte.objects.get(),
    #                     ciudad=Ciudad.objects.get())
    #     medio.save()
    #     programa = Programa(nombre="programa", medio=medio)
    #     programa.save()

    #     hoy = date.today()
    #     dictio = {
    #      "producto": p,
    #      "programa": programa,
    #      "fecha": hoy,
    #      "hora": "12: 01",
    #      "orden": 1,
    #      "duracion": 1,
    #      "alto": 1,
    #      "ancho": 1,
    #      "superficie": 0,
    #      "seg": 10010,
    #      "valor": 10010
    #     }

    #     Emision(**dictio).save()

    #     data = {}
    #     data['unit'] = "volumen"
    #     response = self.client.get(reverse(set_unit), data)

    #     data = {}
    #     data['data'] = "sector"
    #     response = self.client.get(reverse(get_options), data)
    #     self.assertContains(response, "Sector1")
    #     self.assertContains(response, "10.0k")

    # def test_save_open_filters(self):
    #     login = self.client.login(username="non_staff", password="yy")
    #     self.assertTrue(login)
    #     response = self.client.get(reverse('home'))
    #     self.assertEqual(response.status_code, 200)

    #     self.assertEqual(Filtro.objects.filter(abierto=True).count(),2)
    #     data = {}
    #     data['data'] = "marca"
    #     response = self.client.get(reverse(open_filter),data)
    #     self.assertEqual(Filtro.objects.filter(abierto=True).count(),1)
    #     data = {}
    #     data['data'] = "medio"
    #     response = self.client.get(reverse(open_filter),data)
    #     self.assertEqual(Filtro.objects.filter(abierto=True).count(),2)


class SelectorCiudadesViewTest(InfoadTestCase):
    def setUp(self):
        InfoadTestCase.setUp(self)

        # Creo 4 ciudades extra
        self.bsas = Ciudad.objects.create(nombre="Buenos Aires")
        self.rosario = Ciudad.objects.create(nombre="Rosario")
        self.mendoza = Ciudad.objects.create(nombre="Mendoza")
        self.sanjuan = Ciudad.objects.create(nombre="San Juan")

        # me logueo como root
        self.client.login(username="staff", password="xx")
        self.client.get(reverse(home))

        # creo filtros
        self.root.get_profile().crear_filtros()
        self.usuario.get_profile().crear_filtros()

        # elijo 3 ciudades
        for ciudad in [self.cordoba, self.rosario, self.mendoza]:
            ciudad_id = str(ciudad.id)
            data = {'filter': "ciudad", 'value': ciudad_id}
            self.client.get(reverse(add_filter), data)

        # guardo estas opciones para no_staff
        data = {}
        data["package-accion"] = "Guardar"
        data["package-user"] = self.usuario.id
        data['package'] = "Enviar"
        self.client.post(reverse(home), data)

        self.client.logout()


    def test_selector_ciudades_sin_duplicados(self):
        # FIXME: completar
        # me logueo como staff
        self.client.login(username="non_staff", password="yy")
        self.client.get(reverse(home))

        # chequeo que tenga activadas las 3 ciudades como
        # iniciales
        perfil = self.usuario.get_profile()
        filtro = perfil.get_filtro(key='ciudad')
        ciudades = OpcionFiltro.objects.filter(filtro=filtro)
        self.assertEqual(len(ciudades), 3)

        # filtro por una de ellas
        ciudad_id = str(self.rosario.id)
        data = {'filter': "ciudad", 'value': ciudad_id}
        self.client.get(reverse(add_filter), data)

        # chequeo que getoptions muestre la q filtré
        # response = self.client.get('/app/options/', {'data': 'ciudad'})
        # self.assertContains(response, '"item": "Rosario"')

        # chequeo que el menú d la derecha siga conteniendo las 3

        # y q muestre el nombre d la ciudad elegida

        # agrego otra

        # vuelvo a chequear

        # veo q el menú diga "varias"

        # elijo la ciudad q falta

        # veo que el menú diga "todas"

        # aprieto la "x" para cancelar los filtros
        # actuales

        # chequeo q arriba esté en "todas"

        # chequeo q a la izquierda muestre las 3 ciudades

    def test_ciudades_usuario_nuevo(self):
        pass
        # me logueo como root

        # creo un usuario nuevo

        # me deslogueo

        # me logueo con el usuario nuevo

        # deberían estar disponibles todas las ciudades


    def test_ciudades_usuario_no_afectan_root(self):
        pass
        # me logueo como root

        # selecciono 2 ciudades

        # guardo las ciudades filtradas en el perfil no-staff

        # me deslogueo

        # me vuelvo a loguear

        # chequeo q sigo teniendo disponibles todas las ciudades




# # FIXME hacer andar estos tests
# class PerfilesTest(InfoadTestCase):
#     """ Test para manejo de sesiones (autosave de Perfil), verifica que al
#         desloguearse se mantengan los datos del perfil.
#
#           También acá deberían testearse las funciones de cargar y guardar
#            perfil que se manejan desde un usuario root
#     """
#     def test_save_ordered(self):
#         # Me logueo como usuario y voy a la homepage
#         self.client.login(username="non_staff", password="xx")
#         self.client.get(reverse(home))

#         response = self.client.get(reverse(get_ordered_filters))
#         data = simplejson.loads(response.content)
#         filters = [d['name'] for d in data]

#         data = {}
#         lista = "soporte,medio,programa,marca,sector,industria,ciudad,anunciante,agencia"
#         data['data'] = lista
#         response = self.client.get(reverse(set_ordered_filters), data)
#         self.assertEqual(response.status_code, 200)

#         response = self.client.get(reverse(get_ordered_filters))
#         self.assertEqual(response.status_code, 200)
#         data = simplejson.loads(response.content)
#         filters = [d['name'] for d in data]
#         self.assertEqual(filters, lista.split(","))

#         self.client.logout()
#         login = self.client.login(username="non_staff", password="xx")

#         response = self.client.get(reverse(get_ordered_filters))
#         self.assertEqual(response.status_code, 200)

#         data = simplejson.loads(response.content)
#         filters = [d['name'] for d in data]
#         self.assertEqual(filters, lista.split(","))

#     def test_save_filter(self):
#         usuario  = User.objects.get()

#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)

#         self.client.get(reverse('home'))
#         rubro = Rubro()
#         rubro.save()
#         industria = Industria(rubro=Rubro.objects.get())
#         industria.save()
#         sector =Sector(industria=Industria.objects.get())
#         sector.save()
#         m = Marca(sector=Sector.objects.get(),nombre="id")
#         m.save()

#         data = {}
#         data['filter'] = "marca"
#         data['value'] = 1
#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(response.status_code, 200)

#         self.client.logout()

#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)
#         self.client.get(reverse('home'))
#         response = self.client.get(reverse(get_filters))
#         self.assertEqual(response.status_code, 200)
#         data = simplejson.loads(response.content)
#         self.assertNotEqual(data,{})

#     def test_save_filter_top_fecha_desde(self):
#         usuario  = User.objects.get()

#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)

#         self.client.get(reverse('home'))

#         data = {}
#         data['filter'] = "fecha_desde"
#         data['value'] = "06/11/2012"


#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(response.status_code, 200)
#         self.client.logout()
#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)
#         self.client.get(reverse('home'))
#         self.assertEqual(Periodo.objects.get().fecha_desde.strftime("%d/%m/%Y"),"06/11/2012")

#     def test_save_filter_top_fecha_hasta(self):
#         usuario  = User.objects.get()

#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)

#         self.client.get(reverse('home'))

#         data = {}
#         data['filter'] = "fecha_hasta"
#         data['value'] = "06/11/2012"


#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(response.status_code, 200)
#         self.client.logout()
#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)
#         self.client.get(reverse('home'))
#         self.assertEqual(Periodo.objects.get().fecha_hasta.strftime("%d/%m/%Y"),"06/11/2012")


#     def test_save_filter_top_fechas(self):
#         """ seteo las fechas con el formato fechas :  %d/%m/%Y - %d/%m/%Y """
#         usuario  = User.objects.get()

#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)

#         self.client.get(reverse('home'))

#         data = {}
#         data['filter'] = "fechas"
#         data['value'] = "06/11/2012 - 09/11/2012"


#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(Periodo.objects.get().fecha_desde.strftime("%d/%m/%Y"),"06/11/2012")
#         self.assertEqual(Periodo.objects.get().fecha_hasta.strftime("%d/%m/%Y"),"09/11/2012")


# # FIXME: unir con GraphDataGeneration
# class FiltrosGraphTest(InfoadTestCase):
#     def setUp(self):
#         anunciante = Anunciante(nombre="anunciante 1")
#         anunciante.save()
#         rubro = Rubro()
#         rubro.save()
#         industria = Industria(rubro=Rubro.objects.get())
#         industria.save()
#         sector =Sector(industria=Industria.objects.get(),nombre="sector1")
#         sector.save()
#         m = Marca(sector=Sector.objects.get(),nombre="id1",anunciante=anunciante)
#         m.save()
#         m = Marca(sector=Sector.objects.get(),nombre="id2",anunciante=anunciante)
#         m.save()
#         sector =Sector(industria=Industria.objects.get(),nombre="sector2")
#         sector.save()
#         p = Producto(marca=m)
#         p.save()

#         ciudad = Ciudad(nombre="cba")
#         ciudad.save()

#         soporte = Soporte(nombre="tv")
#         soporte.save()

#         medio = Medio(nombre="canal 12", ciudad=ciudad, soporte=soporte)
#         medio.save()

#         programa = Programa(nombre="programa1",medio=medio)
#         programa.save()

#         hoy = date.today()

#         dict = {
#          "producto":p,
#          "programa":programa,
#          "fecha":hoy,
#          "hora": "12:01",
#          "orden":1,
#          "duracion":1,
#          "alto":1,
#          "ancho":1,
#          "superficie":0,
#          "seg":1000,
#          "valor":15
#         }

#         Emision(**dict).save()
#         dict["orden"] = 2
#         Emision(**dict).save()

#         dict['hora'] = "13:01"
#         Emision(**dict).save()

#         dict['hora'] = "15:01"
#         Emision(**dict).save()


#         dict["fecha"] = hoy - timedelta(days=15)
#         Emision(**dict).save()

#         self.root  = User.objects.create_user("adkiller", 'ho...@simpson.net', "worms")
#         self.root.is_staff=True
#         self.root.save()
#         login = self.client.login(username="non_staff", password="xx")
#         self.assertTrue(login)
#         self.client.get(reverse('home'))

#     def test_opciones_productos(self):
#         """ Falta verificar que devuelve datos para comparar"""
#         data = {}
#         data['data'] = "producto"
#         data['limit'] = "3"
#         response = self.client.get(reverse(get_options),data)
#         data = simplejson.loads(response.content)
#         self.assertEqual(len(data[0]),1)

#     def test_comparar_marcas(self):
#         """ Falta verificar que devuelve datos para comparar"""
#         data = {}
#         data['filter'] = "sector"
#         data['value'] = "1"
#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(OpcionFiltro.objects.all().count(),1)
#         data = {}
#         data['filter'] = "sector"
#         data['value'] = "2"
#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(OpcionFiltro.objects.all().count(),2)


#         data = {}
#         data['data'] = True
#         response = self.client.get(reverse(get_filters),data)
#         self.assertContains(response,"Sector1")
#         self.assertContains(response,"Sector2")


#         data = {}
#         data['filter'] = "sector"
#         response = self.client.get(reverse(compare_filter),data)

#         self.assertEqual(Perfil.objects.get().comparado,ContentType.objects.get(name="sector"))

#         response = self.client.get(reverse(graph))
#         data = simplejson.loads(response.content)

#         periodo = Periodo.objects.get()
#         distancia = periodo.fecha_hasta - periodo.fecha_desde
#         dias = distancia.days + 1
#         self.assertEqual(len(data[0]),dias)
#         res = []
#         for elem in data[0]:
#             self.assertEqual(len(elem['valor']),2)

#     def test_autocomplete(self):
#         data = {}
#         data['texto'] = "pro"
#         response = self.client.get(reverse(lookup_filtro_general),data)
#         self.assertContains(response,"programa1")

#     def test_cambiar_ciudad(self):
#         """ Elijo la ciudad1 como filtro, luego elijo la ciudad2 como filtro
#             y verifico que haya cambiado el filtro a ciudad2 y que no haya
#             creado dos veces lo mismo"""
#         ciudad_1 =Ciudad(nombre="ciudad 2")
#         ciudad_1.save()
#         ciudad_2 = Ciudad(nombre="ciudad3")
#         ciudad_2.save()
#         perfil = self.root.get_profile()
#         aux = str(perfil)
#         aux = str(Filtro.objects.all()[0])
#         self.assertEqual(OpcionFiltro.objects.all().count(),0)
#         data = {}
#         data['filter'] = "ciudad"
#         data['value'] = "1"
#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(OpcionFiltro.objects.all().count(),1)
#         self.assertEqual(OpcionFiltro.objects.all()[0].opcion_id,1)

#         data = {}
#         data['filter'] = "ciudad"
#         data['value'] = "2"
#         response = self.client.get(reverse(add_filter),data)
#         self.assertEqual(OpcionFiltro.objects.all().count(),1)
#         self.assertEqual(OpcionFiltro.objects.all()[0].opcion_id,2)


# # FIXME: ver qué hacen y mover a Graph si hace falta
# class CompararPeriodosTest(InfoadTestCase):
#     def setUp(self):
#         InfoadTestCase.setUp(self)

#         hoy = date.today()
#         params = {
#             "fecha": hoy,
#             "hora": "12:01",
#             "valor": 15
#         }
#         self.crear_emision(**params)

#         params["orden"] = 2
#         self.crear_emision(**params)

#         params['hora'] = "13:01"
#         self.crear_emision(**params)

#         params['hora'] = "15:01"
#         self.crear_emision(**params)

#         params["fecha"] = hoy - timedelta(days=15)
#         self.crear_emision(**params)

#         # Me logueo como root y voy al home
#         self.client.login(username="staff", password="xx")
#         self.client.get(reverse('home'))

#     # FIXME: acomodar siguientes a nuevo formato de gráficos
#     # FIXME: ver qué hacen y dónde irían
#     def test_ok(self):
#         """ Elijo una fecha acorde para comparar y ambas marcas tienen datos
#             para ambos dias (Verificar Orden de los datos)"""
#         perfil = self.root.get_profile()
#         periodo = Periodo.objects.get(perfil=perfil)

#         hoy = date.today()
#         periodo.fecha_desde = hoy
#         periodo.fecha_hasta = hoy + timedelta(days=3)
#         periodo.save()

#         ayer = hoy - timedelta(days=3)

#         inicial = periodo
#         segundo = Periodo(perfil=perfil, fecha_desde=ayer, fecha_hasta=hoy).save()
#         response = self.client.get(reverse(graph))


#         data = simplejson.loads(response.content)

#         # Verifico que coincidan las fechas que estoy comparando
#         labels = data[4]
#         condicion_1  = comparar_periodos_con_label(inicial, labels[0])
#         self.assertTrue(condicion_1)
#         condicion_2  = comparar_periodos_con_label(segundo, labels[1])
#         self.assertTrue(condicion_2)

#         elementos = data[0]
#         for elem in elementos:
#             self.assertEqual(len(elem['valor']),2)

#         # Verifico que este comparado 2 items
#         self.assertEqual(data[3], 2)

#         # Verifico que este comparado
#         self.assertEqual(data[2], True)

#         # Verifico que este comparado Periodos
#         self.assertEqual(data[5], True)

#         # Verifico que la cantidad de elementos sea igual a la cantidad de dias comparados
#         distancia = periodo.fecha_hasta - periodo.fecha_desde
#         diferencia = distancia.days + 1
#         self.assertEqual(len(elementos), diferencia)

#         # Realizo un Control de los Datos
#         HASTA = inicial.fecha_hasta
#         final = inicial.fecha_desde - timedelta(days=1)
#         fechas = []
#         while (HASTA != final):
#             fechas.append((HASTA, Emision.objects.filter(fecha=HASTA).count()))
#             HASTA -= timedelta(days=1)

#         for x,y in zip(fechas,data[0]):
#             self.assertEqual(x[1], y['valor'][0])

#         HASTA = segundo.fecha_desde
#         final = segundo.fecha_hasta + timedelta(days=1)
#         fechas = []
#         while (HASTA != final):
#             fechas.append((HASTA, Emision.objects.filter(fecha=HASTA).count()))
#             HASTA += timedelta(days=1)

#         for x,y in zip(fechas,data[0]):
#             self.assertEqual(x[1], y['valor'][1])

#     def test_sin_datos(self):
#         """ Elijo una fecha acorde para comparar y ninguna tiene datos
#             para comparar (Verificar Orden de los datos)"""

#         perfil = self.root.get_profile()
#         periodo = Periodo.objects.get(perfil=perfil)

#         hoy = date.today() - timedelta(days=30)
#         periodo.fecha_desde = hoy
#         periodo.fecha_hasta = hoy + timedelta(days=3)
#         periodo.save()
#         ayer = hoy - timedelta(days=3)
#         Periodo(perfil=perfil,fecha_desde=ayer,fecha_hasta=hoy).save()
#         response = self.client.get(reverse(graph))
#         inicial = Periodo.objects.get(id=1)
#         segundo = Periodo.objects.get(id=2)

#         data = simplejson.loads(response.content)

#         # Verifico que coincidan las fechas que estoy comparando
#         labels = data[4]
#         condicion_1  = comparar_periodos_con_label(inicial,labels[0])
#         self.assertTrue(condicion_1)
#         condicion_2  = comparar_periodos_con_label(segundo,labels[1])
#         self.assertTrue(condicion_2)

#         elementos = data[0]
#         for elem in elementos:
#             self.assertEqual(len(elem['valor']),2)

#         # Verifico que este comparado 2 items
#         self.assertEqual(data[3],2)

#         # Verifico que este comparado
#         self.assertEqual(data[2],True)

#         # Verifico que este comparado Periodos
#         self.assertEqual(data[5],True)

#         # Verifico que la cantidad de elementos sea igual a la cantidad de dias comparados
#         distancia = periodo.fecha_hasta - periodo.fecha_desde
#         diferencia = distancia.days + 1
#         self.assertEqual(len(elementos),diferencia)

#         # Realizo un Control de los Datos
#         HASTA = inicial.fecha_hasta
#         final = inicial.fecha_desde - timedelta(days=1)
#         fechas = []
#         while (HASTA != final):
#             fechas.append((HASTA,Emision.objects.filter(fecha=HASTA).count()))
#             HASTA -= timedelta(days=1)

#         for x,y in zip(fechas,data[0]):
#             self.assertEqual(x[1],y['valor'][0])

#         HASTA = segundo.fecha_desde
#         final = segundo.fecha_hasta + timedelta(days=1)
#         fechas = []
#         while (HASTA != final):
#             fechas.append((HASTA,Emision.objects.filter(fecha=HASTA).count()))
#             HASTA += timedelta(days=1)

#         for x,y in zip(fechas,data[0]):
#             self.assertEqual(x[1],y['valor'][1])

#     def test_solo_un_dato(self):
#         """ Elijo una fecha acorde para comparar y una tiene datos
#         para comparar (Verificar Orden de los datos) """
#         pass

#     def test_comparar_periodos_distintos(self):
#         """ Elijo una fecha no acorde para comparar y ... comparo la desde la
#             fecha desde del periodo del perfil hasta el tiempo correspondiente
#             al solicitado (Verificar Orden de los datos)"""
#         pass

#     def test_comparar_un_dia(self):
#         """ Elijo parar comparar el ultimo dia (Verificar Orden de los datos)"""
#         pass

#     def comparar_periodos_con_label(periodo, label):
#         condicion1 = label.split(" al ")[0] == periodo.fecha_desde.strftime("%d/%m/%Y")
#         # print periodo.fecha_desde.strftime("%d/%m/%Y"),label.split(" al ")[0]
#         condicion2 = label.split(" al ")[1] == periodo.fecha_desde.strftime("%d/%m/%Y")
#         return condicion1 or condicion2


