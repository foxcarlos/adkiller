# -*- coding: utf-8 -*-
# Funciones auxiliares para la generación de gráficos

# Python Imports
import itertools
import operator
from datetime import timedelta, datetime, time, date


# Project Imports
from utils import get_agg_for_magnitude, aplicar_descuentos, order_dictionary

def datetime_from_date_and_time(date, time):
    return datetime(date.year, date.month, date.day, time.hour)

def date_generator(initial_date, op=operator.add):
    """Yields the initial_date incrementing/decrementing one day each time"""
    while True:
        yield initial_date
        initial_date = op(initial_date, timedelta(days=1))


def date_range(initial_date, final_date):
    """Returns a list with the range of dates between
       initial and final dates"""
    delta = final_date - initial_date
    return itertools.islice(date_generator(initial_date), delta.days + 1)


def get_scale_for_periodo(periodo):
    if not periodo.fecha_hasta or not periodo.fecha_desde:
        if periodo.lapso == 'D':
            nDias = 1
        elif periodo.lapso == 'M':
            nDias = 30
        elif periodo.lapso == 'S':
            nDias = 7
        elif periodo.lapso == 'A':
            nDias = 365
        else:
            nDias = 7
    else:
        nDias = (periodo.fecha_hasta - periodo.fecha_desde).days + 1
    if nDias == 1:
        return {'unidad': "hora", 'valor': nDias}
    elif nDias > 1 and nDias <=27:
        return {'unidad': "dia", 'valor': 1}
    else:
        nSemanas = nDias / 7
        if nSemanas >= 16 and nSemanas < 48:
            return {'unidad': "semana", 'valor': 1}
        elif nSemanas < 16:
            return {'unidad': "dia", 'valor': 1}
        else:
            return {'unidad': "mes", 'valor': 1}


def get_column_header_for_scale(graph_scale):
    unidad = graph_scale['unidad']
    if unidad == 'hora':
        return 'datetime'
    else:
        return 'date'


def format_time_for_graph(time, graph_scale):
    if graph_scale['unidad'] == 'hora':
        return time.strftime("%d/%m/%Y %H:%M")
    else:
        return time.strftime("%d/%m/%Y")
        # FIXME: pensar formato para el caso de semanas


def get_data_for_a_single_graph(periodo, emisiones_filtradas, graph_scale,
                                selected_magnitude):
    """Devuelve diccionario con:
       key = punto en el eje-x (por ej.: "2013/08/15" o "13:50")
       value = la suma de la magnitud (inversion, cantidad, volumen)
       correspondiente"""
    con_descuentos = selected_magnitude == 'inversion'
    agg_field = get_agg_for_magnitude(selected_magnitude)

    desde = periodo.fecha_desde

    # -> Generamos listas con los puntos del eje x, para rellenar huecos si los hubiera
    if graph_scale['unidad'] == 'hora':
        nHoras = graph_scale['valor']
        db_field = 'fechaHora'
        desde = datetime_from_date_and_time(desde, time(0))
        # armar diccionario de 24 puntos vacio (N dias (24*N horas) agrupadas de a N horas)
        range_list = [{'fechaHora': desde + timedelta(hours=nHoras*x), 'magnitud': 0} for x in range(0, 24)]
    else:
        db_field = 'fecha'
        nDays = periodo.longitud()
        if graph_scale['unidad'] == 'dia':
            # armar diccionario vacio dia por dia para todo el rango de fechas
            diasPaso = graph_scale['valor']
            paso = timedelta(days=diasPaso)
            range_list = [{'fecha': desde + x*paso, 'magnitud': 0} for x in range(0, nDays/diasPaso+1)]        
        elif graph_scale['unidad'] == 'semana':
            semanasPaso = graph_scale['valor']
            diasPaso = 7*semanasPaso
            paso = timedelta(days=diasPaso)
            range_list = [{'fecha': desde + x*paso, 'magnitud': 0} for x in range(0, (nDays-1)/diasPaso+1)]        
        elif graph_scale['unidad'] == 'mes':
            desde, hasta = periodo.fecha_desde, periodo.fecha_hasta
            year_start = desde.year             
            year_end = hasta.year
            if year_end == year_start:
                range_list = [{'fecha': date(year_start, m, 1), 'magnitud': 0} for m in range(desde.month, hasta.month + 1)]
            else:                        
                range_list = [{'fecha': date(year_start, m, 1), 'magnitud': 0} for m in range(desde.month, 13)]
                year = year_start + 1
                while year < year_end:
                    range_list += [{'fecha': date(year, m, 1), 'magnitud': 0} for m in range(1, 13)]
                range_list += [{'fecha': date(year_end, m, 1), 'magnitud': 0} for m in range(1, hasta.month + 1)]

    # -> traemos los datos para el gráfico
    if con_descuentos:
        # FIXME: refactorizar aplicar_descuentos a perfil
        # y que los resultados sean solo los campos necesarios
        query_results = aplicar_descuentos(emisiones_filtradas)
        list_magnitudes = []
        for elem in query_results:
            datatiempo = {'fecha': elem[4], 'hora': elem[5]}
            if db_field == 'fechaHora':
                fecha = elem[4]; hora=elem[5]
                datatiempo['fechaHora'] = datetime_from_date_and_time(fecha, hora)
            list_magnitudes.append({db_field: datatiempo[db_field],
                                    'magnitud': int(elem[-1])})
    else:
        if db_field == 'fechaHora':
            list_magnitudes = emisiones_filtradas.values('fecha', 'hora').annotate(magnitud=agg_field)
            list_magnitudes = [{'fechaHora': datetime_from_date_and_time(p['fecha'], p['hora']), 'magnitud': p['magnitud']}
                                for p in list_magnitudes]
        else:
            list_magnitudes = emisiones_filtradas.values(db_field).annotate(magnitud=agg_field)

    # -> agrupamos según la escala correspondiente
    # FIXME: - estamos tratando a hora como datetime
    #        - estamos asumiendo que 'hora' solo se usa para el caso de un día
    if graph_scale['unidad'] == 'hora':
        nHoras = graph_scale['valor']
        list_magnitudes_agrupadas = [{'fechaHora': datetime_from_date_and_time(p['fechaHora'], time(hour=(p['fechaHora'].hour/nHoras)*nHoras)),
                                    'magnitud': p['magnitud']} for p in list_magnitudes]
    else:
        list_magnitudes_agrupadas = []
        for i in list_magnitudes:
            fecha = i['fecha']
            x = 0           
            # buscamos en q intervalo meter esta fecha, o sea, x tal que
            # range_list[x]['fecha'] <= fecha < range_list[x+1]['fecha']
            while x < len(range_list) and range_list[x]['fecha'] <= fecha:
                x += 1
            x -= 1
            list_magnitudes_agrupadas.append({'fecha': range_list[x]['fecha'], 'magnitud': i['magnitud']})

    # unir listas para completar espacios intermedios
    list_magnitudes_agrupadas = list(list_magnitudes_agrupadas) + range_list

    # sumar los resultados
    # [{'a': 1}, {'a': 2}, {'b': 1}] --> {'a':3, 'b': 1}
    results = dict()
    for i in list_magnitudes_agrupadas:
        key = i[db_field]

        # FIXME: no deberían estar llegando en None
        if i['magnitud'] is None:
            i['magnitud'] = 0
        if key in results:
            results[key] += i['magnitud']  # sum values
        else:
            results[key] = i['magnitud']  # add to results
    
    return order_dictionary(results)


def render_graph(graph_data, graph_scale, comparison_type, magnitude):
    """
    Toma:
        - la data de gráficos (a dictionary <label, graph_data>),
        - graph_scale = puede ser "dia", "hora", "2 horas", etc.
        - comparison_type = 'periodos', 'opciones_filtro' o 'simple'
        - magnitude = 'inversion', 'cantidad' o 'volumen'

    la combina y genera un array con la data del gráfico, en este formato:
                [["date", "inversion"],
                 ["2013-06-12", 153733973],
                 ["2013-06-11", 145386109],
                 ["2013-06-13", 152824583]
                ]
    o, en caso de una comparacion, algo como:
                [["date", "Canal 12 Cba.", "Canal 10 Cba."],
                 ["2013-07-01", 3, 1],
                 ["2013-07-02", 2, 2],
                 ["2013-07-03", 1, 3]
                ]

    it assumes that each individual piece of graph_data
    has information over an equivalent period of time
    (the exact same period or periods of the same length)
    """
    # we assume all requested graphs are over the same period
    # FIXME/CHECK: actually, it is a period of the same length

    first_column_header = get_column_header_for_scale(graph_scale)
    if comparison_type == 'periodos':
        # FIXME: graph_scale should be something like '1d' or '2h'
        header = [first_column_header] + graph_data.keys()
        combined_data_array = [header]

        max_period_length = max([len(d) for d in graph_data.values()])

        # we complete the table to include the longest period
        # filling the gaps in the shorter periods with 0s

        # Si comparo periodos de varios días o meses
        # el eje x (primera columna) simplemente lleva ordinales
        #  (1st day, 2nd day, ...)

        # FIXME mejorar la lógica de manejo de periodos
        periodo_de_un_dia = (graph_scale['unidad'] == 'hora' and
                                max_period_length == 24)

        if periodo_de_un_dia:
            x_axis_points = ["%2d:00" % x for x in range(0, 24)]
        else:
            x_axis_points = range(1, max_period_length + 1)

        for x in x_axis_points:
            row = [x]
            for graph in graph_data.values():
                if x < len(graph.values()):
                    row.append(graph.values()[x])
                else:
                    row.append(0)
            combined_data_array.append(row)
        graph_data_array = combined_data_array
        # Si estoy comparando un día con otro, la primera columna
        # lleva horas

    elif comparison_type == 'opciones_filtro':
        opciones_comparadas = sorted(graph_data.keys())
        header = [first_column_header] + opciones_comparadas
        combined_data_array = [header]

        # we expect every single graph to be over the same period
        # i.e.: its data will have the same time keys
        x_axis_points = set()
        for graph in graph_data.values():
            x_axis_points.update(graph.keys())
        x_axis_points = sorted(list(x_axis_points))

        for time in x_axis_points:
            row = [format_time_for_graph(time, graph_scale)]
            for opcion in opciones_comparadas:
                graph = graph_data[opcion]
                if time in graph.keys():
                    row.append(graph[time])
                else:
                    row.append(0)
            combined_data_array.append(row)
        graph_data_array = combined_data_array

    elif comparison_type == 'simple':
        the_graph = graph_data['single_graph']
        # header = [graph_scale['unidad'], magnitude]
        header = [first_column_header, magnitude]
        data_array = [header]
        for time in sorted(the_graph.keys()):
            formatted_time = format_time_for_graph(time, graph_scale)
            data_array.append([formatted_time, the_graph[time]])
        graph_data_array = data_array

    # Limpiar colas de ceros
    last_non_zero_row_found = False
    i = len(graph_data_array)
    n_columns = len(graph_data_array[0])

    while not last_non_zero_row_found and i > 3:
        # la tabla debe tener al menos un header y dos filas,
        # si no el gráfico se ve raro
        row = graph_data_array[i - 1]
        for j in range(1, n_columns):
            if row[j] != 0:
                last_non_zero_row_found = True
            else:
                i -= 1

    return graph_data_array[:i]
