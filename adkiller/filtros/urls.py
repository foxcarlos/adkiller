from django.conf.urls import patterns, include, url
from views import *

urlpatterns = patterns('',
    url(r'^crear_busqueda/', crear_busqueda),
    url(r'^cargar_busqueda/(?P<id_busq>\d+)/', cargar_busqueda),
    url(r'^mostrar_busqueda/(?P<id_busq>\d+)', mostrar_busqueda),
    url(r'^eliminar_busqueda/', eliminar_busqueda),
    url(r'^get_busquedas/', get_busquedas),
    url(r'^get_busqueda/', get_busqueda),
    url(r'^get_alerta/', get_alerta),
    url(r'^mostrar_tortas/', mostrar_tortas),
    url(r'^mostrar_producto/(?P<id_prod>\d+)', mostrar_nuevo_producto),
    url(r'^eliminar_alerta_producto/', eliminar_alerta_producto),
    url(r'^eliminar_alerta/', eliminar_alerta),
    url(r'^set_nivel_filtro/', set_nivel_filtro),
    url(r'^get_nivel_filtro/', get_nivel_filtro),
 )



