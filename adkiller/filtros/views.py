# -*- coding: utf-8 -*-
# Create your views here.

# Python Imports
import json, csv
from datetime import timedelta, date
from copy import deepcopy

# Django Imports
from django.contrib.contenttypes.models import ContentType
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect

# Project Imports
from models import Perfil, Filtro, Periodo, OpcionFiltro, formatear_clave
from utils import FILTERS, obtener_filtros, obtener_opciones, get_dict_filtros,\
            get_qs_emisiones_filtradas, get_Q_from_dict
from graph import get_scale_for_periodo, get_data_for_a_single_graph,\
                render_graph
from adkiller.app.models import Emision, Ciudad, Producto
from adkiller.informes.models import Alerta, AlertaComparativa, AlertaComportamiento
from adkiller.app.views import home

##### ---> Filtros <--- #####
def get_ordered_filters(request):
    """
    Lista de filtros en el orden elegido por el usuario
    """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    keys = [formatear_clave(k) for k in perfil.get_ordered_filters()]
    data = []
    for clave in keys:
        fdict = FILTERS[clave].as_json()
        f = perfil.get_or_crear_filtro(clave)

        if f.abierto:
            fdict['opened'] = "block"
        else:
            fdict['opened'] = "None"
        
        if clave not in ['agencia', "estilo publicidad", "valor publicidad", "recurso publicidad", "anunciante", "etiqueta medio"]:
            data.append(fdict)

    data = json.dumps(data, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def reset_filters(request):
    """
    Descarta todos los filtros seleccionados
    FIXME: solo debe descartar los filtros de la izquierda, no los del top
    """
    # Descarta todas las OpcionFiltro: inicial = False
    Perfil.objects.get(user=request.user, activo=True).reset_filters()

    return HttpResponse("")


def change_filter(request):
    if request.GET:
        key = request.GET['filter']
        value = request.GET['value']

    activar_todas = value == 'all'
    # sólo se usa en ciudades
    # FIXME: refactorizar

    perfil = Perfil.objects.get(user=request.user, activo=True)
    filtro = perfil.get_filtro(key)

    seleccionadas = OpcionFiltro.objects.filter(filtro=filtro, inicial=False)
    for opcion in seleccionadas:
        perfil.remove_filter(key, opcion.opcion_id)

    if not activar_todas:
        add_filter(request)

    return HttpResponse("")


def add_filter(request):
    """ Agregar un filtro a la seleccion """
    perfil = Perfil.objects.get(user=request.user, activo=True)

    if request.GET:
        key = request.GET['filter']
        data = request.GET['value']

    if data:
        perfil.add_filter(key, data)
        return HttpResponseRedirect("/")

    return HttpResponse("")


def remove_filter(request):
    """ Quitar un filtro de la seleccion """
    # Quiere Borrar
    # Cuando puede borrar:
    # Si tiene oopcion filtro and no es inicial then la borro
    if request.POST:
        key = request.POST['filter']
        data = request.POST['id']
    else:
        key = request.GET['filter']
        data = request.GET['id']

    perfil = Perfil.objects.get(user=request.user, activo=True)
    perfil.remove_filter(key, data)

    return HttpResponse("")


def set_nivel_filtro(request):
    filtro = request.GET['filtro']
    nivel = request.GET['nivel']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    f = perfil.get_filtro(filtro)
    f.nivel = nivel
    f.save()
    return HttpResponse("")


def get_nivel_filtro(request):
    filtro = request.GET['filtro']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    return HttpResponse(str(perfil.get_filtro(filtro).nivel))


def open_filter(request):
    """ Se activa cuando el usuario abre el panel de un filtro """
    data = request.GET['data']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    perfil.set_open_filter(data)

    return HttpResponse("")


def set_ordered_filters(request):
    """
    Establecer el orden de los filtros
    Recibe un string con los filtros separados por coma
    """
    orden = request.GET['data']
    Perfil.objects.get(user=request.user, activo=True).\
                                            set_ordered_filters(orden)
    return HttpResponse("")


def get_filters(request):
    """
    Filtros elegidos
    (del top o de la izquierda, segun GET['top'])
    """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    estado = perfil.comparado is None
    if 'top' in request.GET:
        top = request.GET['top'] == "True"
    else:
        top = False

    data = obtener_filtros(top, perfil, False, perfil.comparado)
    data = json.dumps(data, cls=DjangoJSONEncoder)

    response =  HttpResponse(data, mimetype='application/json')
    response["Pragma"] = "no-cache" 
    response["Cache-Control"] = "no-cache" 
    response["Expires"] =  "0"


    return response


def get_ciudades(request):
    """ Obtiene todas las ciudades disponibles para el usuario actual """
    perfil = Perfil.objects.get(user=request.user, activo=True)    
    filtro_ciudad = perfil.get_or_crear_filtro(key='ciudad')
    op_ciudades = OpcionFiltro.objects.filter(filtro=filtro_ciudad,
                                                inicial=True)
    # Si un usuario no tiene filtros de ciudad especificados,
    # asumimos que puede ver todas
    if request.user.is_staff or len(op_ciudades) == 0:
        result = [{'item': c.nombre, 'id': c.id} for c in Ciudad.objects.all()]
    else:
        result = []
        for op_ciudad in op_ciudades:
            ciudad = Ciudad.objects.get(id=op_ciudad.opcion_id)
            result.append({'item': ciudad.nombre, 'id': ciudad.id})

    data = json.dumps(result, cls=DjangoJSONEncoder)
    return HttpResponse(data)


def get_options(request):
    """Obtiene los registros de un filtro,
       acorde a los demas filtros seleccionados"""
    options = request.GET
    data = options['data']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    unidad = perfil.get_unit()
    filtro = perfil.get_or_crear_filtro(key=data)

    if 'offset' in options:
        offset = int(options['offset'])
    else:
        offset = 0
    
    if 'limit' in options:
        limit = int(options['limit'])
    else:
        limit = filtro.incremento_en_cantidad

    if data in ["etiquetamedio", "etiquetaproducto", "etiquetaprograma"]:
        limit = 100

    opciones, has_more, has_less = obtener_opciones(perfil, unidad, filtro,
                                                    offset, limit)

    data = json.dumps((opciones, has_more, has_less, unidad),
                      cls=DjangoJSONEncoder)
    return HttpResponse(data)


def get_options_csv(request):
    """Obtiene los registros de un filtro,
       acorde a los demas filtros seleccionados, para exportar a csv"""
    options = request.GET
    data = options['data']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    unidad = perfil.get_unit()
    filtro = perfil.get_or_crear_filtro(key=data)

    cantidad = 100000000 #que traiga todo
    opciones, has_more, has_less = obtener_opciones(perfil, unidad, filtro,
                                                    0, cantidad)


    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s.csv"' % data

    writer = csv.writer(response, delimiter=';')
    writer.writerow([data.capitalize(), 'Valor', 'Porcentaje'])
    for opcion in opciones:
        writer.writerow([opcion['item'], opcion['valor'], opcion['porcentaje']])
    return response


def more_options(request):
    """ Traer 5 registros mas en un filtro """
    opt = request.GET
    data = opt['data']
    perfil = Perfil.objects.get(user=request.user, activo=True)
    filtro = perfil.get_filtro(data)
    count = filtro.cantidadActual
    
    opt = opt.copy()
    if not "offset" in request.GET:
        opt['offset'] = count

    filtro.more_options()

    request.GET = opt

    return get_options(request)


def less_options(request):
    """ Traer 5 registros menos en un filtro """
    # FIXME: qué hace esto? dónde se usa?
    opt = request.GET
    data = opt['data']
    # perfil = Perfil.objects.get(user=request.user, activo=True)
    # data = formatear_clave(data)
    #filtro = Filtro.objects.filter(perfil=perfil,
    #tipo=ContentType.objects.get(name=data))[0]

    return HttpResponse("")


def lookup_filtro_general(request):
    """ Datos para el autocomplete del filtro general """
    if request.GET:
        if 'texto' in request.GET:
            texto = request.GET['texto']
        # marca, sector, industria, medio, programa
        options = []
        perfil = Perfil.objects.get(user=request.user, activo=True)
        for tipo in ['marca', 'sector', 'industria', 'medio', 'programa',
                        'ciudad', 'anunciante', 'soporte', 'grupo medio']:

            filtro = Filtro.objects.filter(tipo=ContentType.objects.
                                            get(name=tipo), perfil=perfil)[0]
            opciones = [o.opcion_id for o in OpcionFiltro.objects.filter(filtro=filtro, inicial=True)]
            filtro = FILTERS[tipo]
            instances = filtro.cls.objects.all()
            #if tipo == "soporte":
            #    instances = instances.filter(padre__isnull=True)
            instances = instances.filter(nombre__aicontains=texto).distinct()

            if opciones and not request.user.is_staff:
                instances = filter(lambda i: i.id in opciones, instances)
                #[filtro.cls.objects.get(id=of.opcion_id) for of in opciones]
            try:
                new_options = [(l.id, str(l), tipo) for l in instances]
            except:
                new_options = []
            options += new_options

    return render_to_response('options_for_autocomplete.html', locals())

def lookup_filtro_medio(request):
    """ Datos para el autocomplete del filtro de medios """
    if request.GET:
        if 'texto' in request.GET:
            texto = request.GET['texto']
        # marca, sector, industria, medio, programa
        options = []
        perfil = Perfil.objects.get(user=request.user, activo=True)
       
        filtro = Filtro.objects.filter(tipo=ContentType.objects.
                                        get(name='medio'), perfil=perfil)[0]
        puede = True
        #opciones = OpcionFiltro.objects.filter(filtro=filtro)
        #for opcion in opciones:
            #puede = puede and not opcion.inicial
        if puede or request.user.is_staff:
            filtro = FILTERS['medio']
            instances = filtro.cls.objects.all()
            instances = instances.filter(server__isnull=False).filter(nombre__aicontains=texto).distinct()
            options += [(l.id, str(l), 'medio') for l in instances]

    return render_to_response('options_for_autocomplete.html', locals())

##### ---> Búsquedas <--- #####
def crear_busqueda(request):
    """ 
        Vista para crear una Busqueda, solo necesito un nombre y si
        envia minimo o maximo creo una alerta, asociada a la busqueda

        * Devuelve "Error", si no ingreso el nombre y
        * si todo fue correcto, devuelve el id de la nueva búsqueda
        * si además se creó una alerta, devuelve también el id de la alerta
    """
    respuesta = {"respuesta": "Error"}
    if "nombre" in request.GET:
        perfil_actual = Perfil.objects.get(user=request.user, activo=True)

        # Me fijo si ya existe un filtro con ese nombre y lo borro
        lista_perfiles = Perfil.objects.all().filter(user=request.user)
        for perfil in lista_perfiles:
            if perfil.nombre == request.GET["nombre"]:
                perfil.delete()

        ## Creo una busqueda (que es un perfil)
        busqueda_nueva = perfil_actual.crear_clon_filtros(nombre=request.GET["nombre"])
        
        # Seteo el alerta de campaña
        if "alertar_producto" in request.GET:
            if request.GET['alertar_producto'] == 'true':
                busqueda_nueva.alerta_producto = True
            else:
                busqueda_nueva.alerta_producto = False

        if "variable" in request.GET:
            busqueda_nueva.unidad = request.GET['variable']
        else:
            busqueda_nueva.unidad = perfil_actual.unidad 
        busqueda_nueva.save()

        # Copio el periodo del perfil_actual
        perfil_actual.copiar_periodo_a(busqueda_nueva)
        
        # --> (Una búsqueda tiene un solo periodo)
        if "periodo" in request.GET:
            lapso = request.GET['periodo'][0].upper()
            perfil_actual.get_periodo_or_create().lapso = lapso

        respuesta["respuesta"] = "OK"
        respuesta["busqueda_id"] = busqueda_nueva.id
        
        ## crear alerta, asociada a la nueva busqueda
        if 'variacion' in request.GET:
            tipo = "Va"  # Variacion
        else:
            tipo = "VA"  # Valor absoluto
        
        if 'alertar' in request.GET and request.GET['alertar'] == 'on':
            params = {
                "busqueda": busqueda_nueva,
                "tipo": tipo, 
                "comparador": request.GET["comparador"],
                "valor": int(request.GET["variable_valor"]),
                "lapso": lapso,
            }
            if params["comparador"] == "<<":
                params["comparador"] == "<"
            if params["comparador"] == ">>":
                params["comparador"] == ">"
                
            if "seriea" in request.GET:
                params["serie_a"] = request.GET['seriea']
                params["serie_b"] = request.GET['serieb']
                alerta = AlertaComparativa()
            else:
                alerta = AlertaComportamiento()
            alerta.setear(**params)
            respuesta["alerta_id"] = alerta.id
            alerta.save();

    data = json.dumps(respuesta, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def cargar_busqueda(request, id_busq):
    """
    Toma un id de Buqueda, y la carga, es decir,
    cambio los valores del perfil actual por la busqueda guardada,
    es decir copio todo, no uso la busqueda

    El perfil actual simpre el unico tal que activo == True,
     el resto son busqueda
    """
    busqueda = Perfil.objects.get(id=id_busq, user=request.user)
    perfil = Perfil.objects.get(user=request.user, activo=True)

    perfil.filtros.all().delete()
    perfil.periodos.all().delete()

    periodo_buscado = Periodo.objects.filter(perfil=busqueda)
    # Debería haber exactamente un período asociado a la búsqueda
    if len(periodo_buscado) == 1:
        periodo_buscado = periodo_buscado[0]
        periodo_nuevo = periodo_buscado.clonar()
    else:
        periodo_nuevo = Periodo(lapso="M")

    periodo_nuevo.perfil = perfil
    periodo_nuevo.save()

    filtros = Filtro.objects.filter(perfil=busqueda)

    for filtro in filtros:
        nuevo_filtro = filtro.clonar()
        nuevo_filtro.perfil = perfil
        nuevo_filtro.save()
        
        opciones = OpcionFiltro.objects.filter(filtro=filtro)
        for opcion in opciones:
            nueva_opcion = opcion.clonar()
            nueva_opcion.filtro = nuevo_filtro
            nueva_opcion.save()

    return HttpResponseRedirect("/app")
    #   return HttpResponse("")


def mostrar_busqueda(request, id_busq):
    """
    Toma un id de usuario y uno de Busqueda,
    chequea que esté logueado,
    carga la búsqueda y la muestra

    se usa para la url del link a la búsqueda en los mails
    de notificación de alertas
    """
    busqueda = Perfil.objects.filter(id=id_busq, user=request.user)
    if busqueda:
        home(request)
        cargar_busqueda(request, id_busq)
        return HttpResponseRedirect("/app")
    else:
        return HttpResponse(u"ERROR: La búsqueda con id=%s no pertenece al usuario %s" % (id_busq, request.user.username))    
    


def eliminar_busqueda(request):
    """ Eliminar busquedas"""
    if 'id' in request.GET:
        search_id = request.GET['id']
        busqueda = Perfil.objects.get(id=search_id, user=request.user)
        busqueda.delete()
    return HttpResponse("")


def get_busquedas(request):
    """
    Listo todas las busquedas del usuario,
    una busqueda es un perfil : activo = False
    """
    busquedas = Perfil.objects.filter(user=request.user, activo=False)
    res = []
    for elem in busquedas:
        res.append([elem.id, elem.nombre, elem.seleccionada(), elem.alertas_activas().count()])
    data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_busqueda(request):
    """
    Devuelve una busqueda en particular.
    una busqueda es un perfil : activo = False
    """
    if 'id' in request.GET:
        search_id = request.GET['id']
        busqueda = Perfil.objects.get(id=search_id, user=request.user)
        res = []
        res.append([busqueda.id, busqueda.nombre, busqueda.seleccionada(), busqueda.alerta_producto])
        data = json.dumps(res, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def get_alerta(request):
    """
    Devuelve los datos de una alerta.
    """

    if 'perfil' in request.GET:
        try:
            search_id = request.GET['perfil']
            alerta = Alerta.objects.get(perfil=search_id)
            perfil = Perfil.objects.get(id=search_id, user=request.user)
            periodo = Periodo.objects.all().filter(perfil=perfil)[0]
            res = []
            res.append([alerta.id, alerta.comparador, alerta.valor, alerta.tipo, perfil.unidad, periodo.lapso])
            data = json.dumps(res, cls=DjangoJSONEncoder)
            return HttpResponse(data, mimetype='application/json')
        except Exception, e:
            return HttpResponse("sin alertas")
        

def eliminar_alerta_producto(request):
    """ 
    Elimina la alerta de producto de una búsqueda.
    """

    if 'perfil' in request.GET:
        try:
            id_perfil = request.GET['perfil']
            perfil = Perfil.objects.get(id=id_perfil, user=request.user)
            perfil.alerta_producto = False
            perfil.save()
            return render_to_response('alerta_eliminada.html', {'nombre':perfil.nombre},
                                    context_instance=RequestContext(request))
        except Exception, e:
            return HttpResponse("Alerta inexistente")
    else:
        return HttpResponse("No hay perfil")


def eliminar_alerta(request):
    """
    Elimina la alerta de una búsqueda.
    """

    if 'alerta' in request.GET: 
        try:
            alerta = Alerta.objects.get(pk=request.GET['alerta'])
            nombre = alerta.perfil.nombre
            alerta.delete()
        except Exception, e:
            return HttpResponse("Alerta inexistente")

    return render_to_response('alerta_eliminada.html', {'nombre':nombre},
                                    context_instance=RequestContext(request))



##### ---> Gráficos <--- #####
def graph(request):
    """
    Decide si se va a generar un gráfico simple o comparado,
    y si va a ser compración de periodos, o comparación por filtros
    y genera un JSON con los datos que necesita drawMainNew
    en graficos.js para dibujar el gráfico, a saber:
        * graph_data_array: data el gráfico en el formato que necesita
                            la lib de visualizaciones de google.
        * comparison_type: opciones de filtro, periodos o simple
                                                        (no hay comparación)
    """
    perfil = Perfil.objects.get(user=request.user, activo=True)

    # Filtra por filtros iniciales y activos y arma un queryset con
    # todas las emisiones correspondientes
    dict_filtros_activos = get_dict_filtros(perfil)

    periodos = Periodo.objects.filter(perfil=perfil)
    comparando_periodos = (len(periodos) == 2)

    # Chequeamos que perfil.comparado no haya quedado mal seteado
    if perfil.comparado is None:
        comparando_filtros = False
    else:
        tipo_filtro_comparado = perfil.comparado
        filtro_comparado = FILTERS[str(tipo_filtro_comparado)]
        lookup_comparado = filtro_comparado.lookup
        comparando_filtros = lookup_comparado in dict_filtros_activos and \
                            (len(dict_filtros_activos[lookup_comparado]) >= 2)

    if comparando_filtros:
        comparando_periodos = False
    else:
        perfil.comparado = None

    unidad = perfil.get_unit()
    graph_scale = get_scale_for_periodo(periodos[0])
    graph_data = dict()
    # will contain the dict with data for the graph

    # FIXME: Si hay varios periodos asumimos que tienen la misma longitud
    # (en realidad, a esto habría que chequearlo/limitarlo,
    # cuando el usuario crea la comparación)

    # Decidir qué vamos a graficar
    # si está activa una comparación de periodos, no comparamos filtros
    if comparando_periodos:
        comparison_type = 'periodos'
        # TODO: ver de mover la desmarcada de comparación
        # a la vista del calendario
        perfil.comparado = None
        # FIXME: que pasa si comparo un periodo de un dia contra otro periodo
        # de mas de un dia???
        comparativo_periodo = {}
        comparativo_periodo["fecha__gte"] = periodos[1].fecha_desde
        comparativo_periodo["fecha__lte"] = periodos[1].fecha_hasta

        qs_emisiones_filtradas, qs_emisiones_filtradas_comparadas = \
                    get_qs_emisiones_filtradas(perfil, comparativo_periodo)
        listas_filtradas = [qs_emisiones_filtradas,
                                    qs_emisiones_filtradas_comparadas]

        for i in [0, 1]:
            periodo = periodos[i]
            graph_data[periodo.get_label()] = \
            get_data_for_a_single_graph(periodo=periodo,
                                        emisiones_filtradas=listas_filtradas[i],
                                        graph_scale=graph_scale,
                                        selected_magnitude=unidad)

        formatted_graph_data = render_graph(graph_data, graph_scale,
                            comparison_type='periodos', magnitude=unidad)

    elif comparando_filtros:
        comparison_type = 'opciones_filtro'
        periodo = periodos[0]
        for i in range(1, len(periodos)):
            periodo.delete()

        opciones_a_comparar = dict_filtros_activos[lookup_comparado]
        dict_filtros_activos_sin_comp = deepcopy(dict_filtros_activos)
        dict_filtros_activos_sin_comp[lookup_comparado] = []
        # TODO:
        #    1. ordenar opciones alfabéticamente
        #    2. usar lista de pares en lugar de dict,
        #        para garantizar q se mantenga el orden
        for opcion_id in opciones_a_comparar:
            dict_filtros_activos_mas_opcion = dict_filtros_activos_sin_comp
            dict_filtros_activos_mas_opcion[lookup_comparado] = [opcion_id]

            Q_filtros_activos_mas_opcion = get_Q_from_dict(
                                    dict_filtros_activos_mas_opcion, perfil)
            qs_emisiones_filtradas_con_opcion = Emision.objects.filter(
                                                Q_filtros_activos_mas_opcion)

            nombre_opcion = str(filtro_comparado.cls.objects.get(id=opcion_id))
            graph_data[nombre_opcion] = \
                get_data_for_a_single_graph(periodo=periodo,
                        emisiones_filtradas=qs_emisiones_filtradas_con_opcion,
                        graph_scale=graph_scale,
                        selected_magnitude=unidad)

        formatted_graph_data = render_graph(graph_data, graph_scale,
                    comparison_type='opciones_filtro', magnitude=unidad)

    else:  # no hay comparación, tiramos un gráfico simple
        comparison_type = 'simple'
        periodo = periodos[0]

        Q_filtros_activos = get_Q_from_dict(dict_filtros_activos, perfil)
        qs_emisiones_filtradas = Emision.objects.filter(Q_filtros_activos)

        graph_data['single_graph'] = \
            get_data_for_a_single_graph(periodo=periodo,
                                emisiones_filtradas=qs_emisiones_filtradas,
                                graph_scale=get_scale_for_periodo(periodo),
                                        selected_magnitude=unidad)
        formatted_graph_data = render_graph(graph_data, graph_scale,
                    comparison_type='simple', magnitude=unidad)
    
    graph_data_JSON = json.dumps((formatted_graph_data, comparison_type, graph_scale['unidad'], graph_scale['valor']),
                        cls=DjangoJSONEncoder)

    # print graph_data_JSON
    response = HttpResponse(graph_data_JSON, mimetype='application/json')
    response["Pragma"] = "no-cache" 
    response["Cache-Control"] = "no-cache" 
    response["Expires"] =  "0"
    return response


def mostrar_tortas(request):
    """ Mostrar u ocultar las tortas """
    perfil = Perfil.objects.get(user=request.user, activo=True)
    #perfil = Perfil.objects.get(id=id,user=request.user,activo=False)
    perfil.tortas_visibles = request.GET['visible'] == "true"
    perfil.save()
    print perfil, perfil.tortas_visibles
    return HttpResponse("")


def set_otras_marcas(request):
    """ Setea otras_marcas del perfil"""

    perfil = Perfil.objects.get(user=request.user, activo=True)
    
    if request.GET:
        value = request.GET['value']
        if value == 'true':
            perfil.otras_marcas = True
        else:
            perfil.otras_marcas = False
        perfil.save()

    return HttpResponse("")


def otras_marcas_value(request):
    """Devuelve el valor de otras_marcas del perfil"""

    perfil = Perfil.objects.get(user=request.user, activo=True)
    
    data = json.dumps(perfil.otras_marcas, cls=DjangoJSONEncoder)
    return HttpResponse(data, mimetype='application/json')


def mostrar_nuevo_producto(request, id_prod):
    """
    Toma un id de usuario y uno de producto,
    chequea que esté logueado.
    se usa para la url del link a la búsqueda en los mails
    de notificación de alertas por nueva campaña
    """

    # Descarta todas las OpcionFiltro: inicial = False
    Perfil.objects.get(user=request.user, activo=True).reset_filters()
    perfil = Perfil.objects.get(user=request.user, activo=True)
    try:
        producto = Producto.objects.get(pk=id_prod)
    except:
        producto = None

    if producto:
        #home(request)
        perfil.add_filter('producto', id_prod)
        return HttpResponseRedirect("/")
        #return HttpResponseRedirect("/app")
    else:
        return HttpResponse(u"ERROR") 
    return HttpResponse("")
