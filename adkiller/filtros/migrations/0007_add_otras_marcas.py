# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):


    def forwards(self, orm):
        # Adding field 'Filtro.nivel'
        db.add_column('filtros_perfil', 'otras_marcas',
                      self.gf('django.db.models.fields.BooleanField')(default=False, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Filtro.nivel'
        db.delete_column('filtros_perfil', 'otras_marcas')

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'filtros.filtro': {
            'Meta': {'object_name': 'Filtro'},
            'abierto': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'cantidadActual': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'incremento_en_cantidad': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'nivel': ('django.db.models.fields.IntegerField', [], {'default': '1', 'null': 'True'}),
            'perfil': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'filtros'", 'to': "orm['filtros.Perfil']"}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'top': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'filtros.opcionfiltro': {
            'Meta': {'object_name': 'OpcionFiltro'},
            'filtro': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'opciones'", 'to': "orm['filtros.Filtro']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inicial': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'opcion_id': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'filtros.perfil': {
            'Meta': {'object_name': 'Perfil'},
            'activo': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'alerta_producto': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comparado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'default': "'B\\xc3\\xbasqueda'", 'max_length': '150'}),
            'orden': ('django.db.models.fields.CharField', [], {'default': "'marca,soporte,medio,programa,industria,sector,ciudad,anunciante,tipo publicidad'", 'max_length': '150'}),
            'otras_marcas': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tortas_visibles': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'unidad': ('django.db.models.fields.CharField', [], {'default': "'cantidad'", 'max_length': '20'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'filtros.periodo': {
            'Meta': {'object_name': 'Periodo'},
            'fecha_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fecha_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'hora_desde': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'hora_hasta': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'init_fecha_desde': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'init_fecha_hasta': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'lapso': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True', 'blank': 'True'}),
            'perfil': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'periodos'", 'to': "orm['filtros.Perfil']"})
        }
    }

    complete_apps = ['filtros']