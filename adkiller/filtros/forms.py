from django import forms
import autocomplete_light
from adkiller.app.models import Medio


class ClonarForm(forms.Form):
    date = forms.DateField(label="Fecha", input_formats=["%d/%m/%Y"])
    date.widget.attrs.update({'id' : 'fechaClona'})
    medio = forms.ModelChoiceField(Medio.objects.all(), required=False, label="Medio", widget=autocomplete_light.ChoiceWidget('MedioAutocomplete'))
    medio.widget.attrs.update({'id' : 'medioClona'})
