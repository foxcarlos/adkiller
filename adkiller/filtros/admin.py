from django.contrib import admin

from models import Periodo, Perfil, Filtro, OpcionFiltro


class PeriodoAdmin(admin.ModelAdmin):
    list_display = ['fecha_desde', 'fecha_hasta', 'perfil', 'lapso']
    list_filter = ['perfil__user']


admin.site.register(Periodo, PeriodoAdmin)


class PerfilAdmin(admin.ModelAdmin):
    list_display = ['user', 'activo', 'nombre']
    list_filter = ['user']
    actions = ['elegir']
    search = ['nombre']

    def elegir(self, request, queryset):
        for elem in queryset:
            elem.activo = True
            elem.save()

    elegir.short_description = "Activar"

admin.site.register(Perfil, PerfilAdmin)


class FiltroAdmin(admin.ModelAdmin):
    list_filter = ['perfil__user', 'tipo', 'top']

admin.site.register(Filtro, FiltroAdmin)


class OpcionFiltroAdmin(admin.ModelAdmin):
    raw_id_fields = ['filtro']
    list_filter = ['opcion_id', 'filtro__perfil__user',
                    'filtro__tipo']
    list_display = ['opcion_id', 'inicial', 'filtro']

admin.site.register(OpcionFiltro, OpcionFiltroAdmin)
