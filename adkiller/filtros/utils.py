# -*- coding: utf-8 -*-

# Python Imports
import operator
from collections import OrderedDict
from datetime import date, datetime, timedelta

# Django Imports
from django.db.models import Count, Sum
from django.conf import settings
from django.db.models import Q
from django.db import connection

# Project Imports
from adkiller.app.models import Programa, Medio, Soporte, Ciudad, Producto,\
    Marca, Anunciante, Sector, Industria, Emision, Migracion, GrupoMedio, \
    Servidor, AudioVisual, Grafica, Rubro, ValorPublicidad, EstiloPublicidad,\
    TipoPublicidad, RecursoPublicidad, Agencia, EtiquetaProducto, EtiquetaMedio, EtiquetaPrograma

from adkiller.media_map.models import Segmento
from adkiller.media_map.models import TipoSegmento


def get_absoluto_acotado(absoluto):
    """Devuelve el label del valor con formato M y k para
       millones y miles respectivamente"""
    absoluto_acotado = absoluto
    letras = ""
    if absoluto_acotado > 1000000:
        absoluto_acotado = absoluto_acotado / 1000000
        letras = letras + "M"
    if absoluto_acotado > 1000:
        absoluto_acotado = absoluto_acotado / 1000
        letras = "k" + letras
    if letras:
        absoluto_acotado = "%.1f%s" % (absoluto_acotado, letras)
    else:
        absoluto_acotado = "%.0f" % absoluto_acotado
    return absoluto_acotado


class Filter:
    """ Representa toda la informacion relacionada
    a un filtro de la plataforma """
    def __init__(self, cls, lookup, reverse, plural=None, orden=0, top=False,
                                                  multiple=True, name=None):
        self.cls = cls
        self.lookup = lookup
        self.reverse = reverse
        if plural is None and self.cls:
            plural = self.cls._meta.verbose_name_plural
        self.plural = plural
        if name:
            self.name = name
        elif self.cls:
            self.name = self.cls.__name__.lower()
        self.orden = orden
        self.top = top
        self.multiple = multiple

    def get_lookup(self, cobranding=False):
        if cobranding and self.cls:
            if self.cls.__name__ == "Anunciante":
                return "producto__otras_marcas__anunciante"
            if self.cls.__name__ == "Sector":
                return 'producto__otras_marcas__sector'
            if self.cls.__name__ == 'Industria':
                return "producto__otras_marcas__sector__industria"
            if self.cls.__name__ == "Rubro":
                return "producto__otras_marcas__sector__industria__rubro"
            if self.name == "marca":
                return "producto__otras_marcas"
        return self.lookup

    def as_json(self, cobranding=False):
        return {'lookup': self.get_lookup(cobranding), 'plural': self.plural,
            'name': self.name, 'orden': self.orden}


""" Diccionario con todos los filtros de la plataforma """
FILTERS = {
    'programa': Filter(Programa, 'horario__programa', 'horario__emisiones',
                        orden=6),
    'medio': Filter(Medio, 'horario__medio', 'horarios__emisiones', orden=5),
    'grupo medio': Filter(GrupoMedio, 'horario__medio__grupo', 'medios__horarios__emisiones', orden=5),
    'soporte': Filter(Soporte, 'horario__medio__soporte__padre',
                    'medios__horarios__emisiones', orden=4),
    'ciudad': Filter(Ciudad, 'horario__medio__ciudad',
                    'medios__horarios__emisiones', top=True),
    'producto': Filter(Producto, 'producto', 'emisiones', orden=3),

    'etiqueta producto': Filter(EtiquetaProducto, 'producto__etiquetas', 'productos__emisiones', orden=3),
    'etiqueta medio': Filter(EtiquetaMedio, 'horario__medio__etiquetas', 'medios__horarios__emisiones', orden=3),
    'etiqueta programa': Filter(EtiquetaPrograma, 'horario__programa__etiquetas', 'programas__horarios__emisiones', orden=3),

    'marca': Filter(Marca, 'producto__marca', 'productos__emisiones', orden=2),
    'anunciante': Filter(Anunciante, 'producto__marca__anunciante',
                        'marcas__productos__emisiones', orden=1),
    'sector': Filter(Sector, 'producto__marca__sector',
                    'marcas__productos__emisiones', orden=9),
    'industria': Filter(Industria, 'producto__marca__sector__industria',
                        'sectores__marcas__productos__emisiones', orden=8),
    'rubro': Filter(Rubro, 'producto__marca__sector__industria__rubro',
                'industrias__sectores__marcas__productos__emisiones', orden=10),

    'otramarca': Filter(Marca, 'producto__marca', 'productos__emisiones', orden=2, name="otramarca", plural="Otras Marcas"),

    'valor publicidad': Filter(ValorPublicidad, 'producto__valores',
                            'productos__emisiones', orden=11),
    'estilo publicidad': Filter(EstiloPublicidad, 'producto__estilo_publicidad',
                        'productos__emisiones', orden=12),
    'tipo publicidad': Filter(TipoPublicidad, 'tipo_publicidad', 'emisiones',
                            orden=13),
    'recurso publicidad': Filter(RecursoPublicidad, 'producto__recursos',
                            'productos__emisiones', orden=14),
    'agencia': Filter(Agencia, 'producto__agencia', 'productos__emisiones',
                        orden=7),
    'fecha_desde': Filter(None, 'fecha__gte', '', top=True, multiple=False),
    'fecha_hasta': Filter(None, 'fecha__lte', '', top=True, multiple=False),

    'hora_desde': Filter(None, 'hora__gte', '', top=True, multiple=False),
    # BUG
    'hora_hasta': Filter(None, 'hora__lte', '', top=True, multiple=False),
    # BUG

    'periodo': Filter(None, '', '', top=True, multiple=False),
}


def order_dictionary(dictionary):
    ordered_dictionary = OrderedDict()
    for k in sorted(dictionary.keys()):
        ordered_dictionary[k] = dictionary[k]
    return ordered_dictionary


def format_con_comas(x):
    if type(x) not in [type(0), type(0L)]:
        raise TypeError("Parameter must be an integer.")
    if x < 0:
        return '-' + format_con_comas(-x)
    result = ''
    while x >= 1000:
        x, r = divmod(x, 1000)
        result = ".%03d%s" % (r, result)
    return "%d%s" % (x, result)


def obtener_filtros(top, perfil, iniciales, comparando):
    """
        FIXME: Ver qué hace
    """
    data = {}
    for key, value in perfil.filtros_to_dict(iniciales).items():
        # Ahora f es una lista
        f = FILTERS[key]
        if key == "periodo" and top:
            data[key] = value
        if f.top == top or key == "otramarca":
            data[key] = value
            if f.multiple:
                data[key] = []
                for v in value:
                    if f.cls:
                        val = FILTERS[key].cls.objects.get(id=v).__unicode__()
                    data[key].append([val, v, comparando == None or comparando.name != key ])
                data[key] = sorted(data[key], key=(lambda x: x[0]))
            else:
                if key == "fecha_desde" or key == "fecha_hasta":
                    if type(value) == 'date':
                        value = value.strftime("%Y-%m-%d")
                    else:
                        value = value
                data[key] = value
    return data


###### ----> metodos de la clase perfil
def get_dict_filtros(perfil, append_id=False, solo_iniciales=False):
    """
        Arma un diccionario con todos los filtros activos
        y sus opciones seleccionadas
        (incluye todo: iniciales, actuales, filtros de periodo
        y condiciones extra)
    """
    iniciales = perfil.filtros_to_dict(iniciales=True)
    if solo_iniciales:
        actuales = {}
    else:
        actuales = perfil.filtros_to_dict(iniciales=False)

    dict_filtros = {}
    filter_keys = set(iniciales.keys())
    filter_keys.update(actuales.keys())
    for filter_key in filter_keys:
        filtro = FILTERS[filter_key]
        lookup = filtro.get_lookup(perfil.otras_marcas)
        if append_id and filtro.cls:
            lookup += "__id"
        if filter_key in actuales:
            dict_filtros[lookup] = actuales[filter_key]
        else:
            dict_filtros[lookup] = iniciales[filter_key]

    if perfil.otras_marcas:
        dict_filtros["producto__otras_marcas__isnull"] = False

    # esto es para que no aparezca pauta propia
    dict_filtros["producto__marca__sector__isnull"] = False

    # esto es por los medios que no deben publicarse
    dict_filtros["horario__medio__publicar"] = True

    # esto es para que no aparezcan productos null
    dict_filtros["producto__isnull"] = False
    return dict_filtros


def get_Q_from_key_value(key, value):
    if key[-14:] == "soporte__padre":
        key = "horario__medio__soporte"
        return Q(**{key: value}) | Q(**{key + "__padre": value})
    else:
        return Q(**{key: value})

def get_Q_from_dict(dictio, perfil, lookup="", con_descuentos=None, filtro_fecha=True):
    # obtiene la consulta a partir de los filtros seleccionados
    # arma una consulta a partir del diccionario de filtros
    # si un filtro tiene mas de una opcion hace un OR entre las opciones
    # al final hace un AND entre todos los filtros
    #
    # Nota: si ponemos con_descuentos=True, hay que pasarle un usuario
    if con_descuentos is None:
        con_descuentos = perfil.get_unit() == 'inversion'

    usuario = perfil.user
    res = Q()
    q_list = []
    for key, value in dictio.items():
        if (filtro_fecha == False ) and (key == 'fecha__gte' or key == 'fecha__lte'):
            continue
        else:
            if type(value) == list:
                q_filtro = []
                for v in value:
                    if v != None:
                        q_filtro.append(get_Q_from_key_value(key, v))
                q_list.append(reduce(operator.or_, q_filtro))
            elif value != None:
                q_list.append(get_Q_from_key_value(key, value))

    # Agrego la tabla de descuentos por usuario, si corresponde
    if con_descuentos:
        q_list.append(Q(horario__medio__descuentos__user=usuario))

    ## esto es para que no aparezca pauta propia
    #q_list.append(Q(producto__marca__sector__isnull=False))

    ## esto es por los medios que no deben publicarse
    #q_list.append(Q(horario__medio__publicar=True))

    ## esto es para que no aparezcan productos null
    #q_list.append(Q(producto__isnull=False))

    # Traigo todas las tablas hasta el modelo que quiero obtener (NUEVO)
    if lookup:
        query = {lookup + "__id__isnull": False}
        q_list.append(Q(**query))

    if q_list:
        res = reduce(operator.and_, q_list)

    return res

def get_qs_ocr(perfil):
    """Genera el query_set correspondiente a los segmentos
    de tipo Publicidad que tienen observaciones,
    de acuerdo a los filtros activos en un perfil """

    # filtrar por medio, marca, sector fecha
    filtros_dict = perfil.filtros_to_dict(False)

    # {'fecha_desde': datetime.date(2015, 8, 1),
    # 'fecha_hasta': datetime.date(2015, 8, 1),
    # 'marca': [16642L],
    # 'medio': [40L]}


    # filtrar por fecha
    desde = filtros_dict['fecha_desde']
    hasta = filtros_dict['fecha_hasta'] + timedelta(days=1)
    q_list = [Q(desde__gte=desde) & Q(desde__lte=hasta)]

    for key in filtros_dict:
        if key == ['fecha_desde', 'fecha_hasta']:
            continue
        elif key == 'medio':
            q_medios = []
            for medio_id in filtros_dict['medio']:
                q_medios.append(Q(medio__id=medio_id))
            q_list.append(reduce(operator.or_, q_medios))
        elif key == 'sector':
            q_sector = []
            for sector_id in filtros_dict['sector']:
                q_sector.append(Q(sector__id=sector_id))
            q_list.append(reduce(operator.or_, q_sector))
        elif key == 'marca':
            q_marca = []
            for marca_id in filtros_dict['marca']:
                q_marca.append(Q(marca__id=marca_id))
            q_list.append(reduce(operator.or_, q_marca))

    q = reduce(operator.and_, q_list)
    qs_segmentos_filtrados = Segmento.objects.filter(q)
    tipo_publicidad = TipoSegmento.objects.filter(nombre='Publicidad')
    return qs_segmentos_filtrados.filter(tipo=tipo_publicidad)

def get_qs_segmentos(perfil, publicidades=False):
    """Genera el query_set correspondiente a los segmentos
    de acuerdo a los filtros activos en un perfil """
    # filtrar por medio, marca, sector fecha
    filtros_dict = perfil.filtros_to_dict(False)

    # {'fecha_desde': datetime.date(2015, 8, 1),
    # 'fecha_hasta': datetime.date(2015, 8, 1),
    # 'marca': [16642L],
    # 'medio': [40L]}


    # filtrar por fecha
    desde = filtros_dict['fecha_desde']
    hasta = filtros_dict['fecha_hasta'] + timedelta(days=1)
    q_list = [Q(desde__gte=desde) & Q(desde__lte=hasta)]

    # sólo exportaremos segmentos de estos tipos
    tipos_a_exportar = [
        'Evento',
        'Logos',
        'Welo',
        'Noticia',
    ]
    if publicidades:
        tipos_a_exportar.append('Publicidad')

    q_list.append(Q(tipo__nombre__in=tipos_a_exportar))

    for key in filtros_dict:
        if key == ['fecha_desde', 'fecha_hasta']:
            continue
        elif key == 'medio':
            q_medios = []
            for medio_id in filtros_dict['medio']:
                q_medios.append(Q(medio__id=medio_id))
            q_list.append(reduce(operator.or_, q_medios))
        elif key == 'sector':
            q_sector = []
            for sector_id in filtros_dict['sector']:
                q_sector.append(Q(sector__id=sector_id))
            q_list.append(reduce(operator.or_, q_sector))
        elif key == 'marca':
            q_marca = []
            for marca_id in filtros_dict['marca']:
                q_marca.append(Q(marca__id=marca_id))
            q_list.append(reduce(operator.or_, q_marca))

    q = reduce(operator.and_, q_list)
    qs_segmentos_filtrados = Segmento.objects.filter(q)
    return qs_segmentos_filtrados

def get_qs_emisiones_filtradas(perfil, comparativo_periodo=None,
                                    lookup_filtro=None, con_descuentos=None, filtro_fecha=True, filtros_adicionales=None):
    """
        Genera el query_set correspondiente a todas las emisiones filtradas
        para un perfil, según sus filtros iniciales y periodo inicial y
        los filtros y periodos seleccionados en el momento.

        Devuelve una lista con un queryset por periodo

        Se le puede pasar un filtro extra (para cuando estamos viendo opciones)
    """
    # FIXME: debería usar la info del perfil para deducir el comparativo_periodo
    unidad = perfil.get_unit()

    if con_descuentos is None:
        con_descuentos = unidad == "inversion"
    dict_filtros_activos = get_dict_filtros(perfil)


    Q_filtros_activos = get_Q_from_dict(dict_filtros_activos, perfil,
                                                lookup=lookup_filtro,
                                                con_descuentos=con_descuentos,
                                                filtro_fecha=filtro_fecha)

    # usado hasta ahora para las comparaciones
    if filtros_adicionales:
        for key, value in filtros_adicionales.items():
            Q_filtros_activos = Q_filtros_activos & Q(**{key: value})


    qs_emisiones_filtradas = Emision.objects
    qs_emisiones_filtradas = qs_emisiones_filtradas.filter(Q_filtros_activos)

    qs_emisiones_filtradas_comparadas = None
    if comparativo_periodo:
        # sobreescribimos el diccionario de filtros con el periodo comparado
        dict_filtros_activos.update(comparativo_periodo)
        Q_periodo_comparado = get_Q_from_dict(dict_filtros_activos, perfil,
                                                lookup=lookup_filtro,
                                                con_descuentos=con_descuentos,
                                                filtro_fecha=filtro_fecha)

        qs_emisiones_filtradas_comparadas = Emision.objects.filter(Q_periodo_comparado)

    return qs_emisiones_filtradas, qs_emisiones_filtradas_comparadas


def get_resultados_from_queryset(queryset, agg, lookup=None,):
    if lookup is None:
        # FIXME: revisar esto
        return queryset.values().distinct().annotate(magnitud=agg).order_by('-magnitud')
    else:
        return queryset.values(lookup + "__id").distinct().annotate(magnitud=agg).order_by('-magnitud')


def get_resultados(qs_emisiones_filtradas, unidad, perfil, filtro=None, filtro_fecha=True):
    lookup_filtro = FILTERS[filtro.name()].get_lookup(perfil.otras_marcas)
    aplico_descuentos = unidad == "inversion"
    if aplico_descuentos and filtro_fecha:
        #   => si la magnitud es inversion, tengo que agregar el select de descuentos
        #      y hacer la parte del aggregate "a mano"
        # hay q editar la query SQL "a mano" para poder hacer el aggregate por tarifa_real
        resultados = aplicar_descuentos(qs_emisiones_filtradas, filtro)
        resultados = formatear_resultados(resultados)
        resultado_total = aplicar_descuentos(qs_emisiones_filtradas, total=True)
        total = resultado_total[0][0]
    else:
    # => Calculo el total para las emisiones filtradas en la magnitud pedida
    #   => primero obtengo el campo sumario segun la unidad seleccionada
        agg = get_agg_for_magnitude(unidad)
        resultados = get_resultados_from_queryset(qs_emisiones_filtradas, agg,
                                                    lookup_filtro)
        total = qs_emisiones_filtradas.aggregate(agg).values()[0]

    return total, resultados


def obtener_opciones(perfil, unidad, filtro, offset, limit, filtro_fecha=True):
    """
        Obtiene las opciones más importantes para un filtro seleccionado.
        Se usa para traer la información de la barra de la izquierda.

        Dado un perfil y un filtro devuelve cierta cantidad de opciones
        con la magnitud asociada (inversion, cantidad, volumen),
        en el periodo especificado en el perfil

        si limit es 0 devuelve la lista entera de opciones
    """
    # path desde emisiones hasta el filtro
    lookup_filtro = FILTERS[filtro.name()].get_lookup(perfil.otras_marcas)
    has_less = filtro.cantidadActual > 5

    unidad = perfil.get_unit()
    aplico_descuentos = unidad == "inversion"


    if filtro.name() == "etiqueta programa":
        return ([{"item": e.nombre, "id": e.id, 
            "variacion_porcentual": "0", "absoluto_acotado": "0k", "valor": 0, "comparando": False,  
            "porcentaje": "0", "porcentaje_relativo": 0, "porcentaje_valor": 0, "int_porcentaje_relativo": "100%"
        } for e in EtiquetaPrograma.objects.all()], False, True)
        


    # trae un dict con fecha_desde y fecha_hasta para el periodo a comparar
    # si no hay uno definido por el usuario, se usa el periodo anterior
    comparativo_periodo, definido_por_usuario = perfil.get_comparativo_periodo()


    qs_emisiones_filtradas, qs_emisiones_filtradas_comparadas = \
        get_qs_emisiones_filtradas(perfil, comparativo_periodo, lookup_filtro, filtro_fecha=filtro_fecha)


    total, resultados = get_resultados(qs_emisiones_filtradas, unidad, perfil, filtro, filtro_fecha=filtro_fecha)

    if filtro_fecha == False and resultados:
        return resultados
    # queremos evitar divisones por cero en
    # los cálculos de porcentajes
    sum_total = 1.0
    if total:
        sum_total = float(str(total))

    # TODO: meter Paginator
    # aplico offset y limit
    # Verifico si tiene mas valores
    if resultados:
        opcion_maxima = resultados[0]
    else:
        opcion_maxima = None

    if limit != 0:
        aux = resultados[offset: offset + limit]
    else:
        aux = resultados[offset:]

    has_more = False  # Dejo de tomar solo el offset
    if len(resultados[offset: offset + limit + 1]) > len(aux):
        has_more = True

    resultados = aux


    # FIXME: mejorar para que no tome tantos parametros
    output_dict = get_output_dict_from_opciones(resultados, opcion_maxima, sum_total, perfil, filtro,
                unidad, qs_emisiones_filtradas_comparadas, aplico_descuentos)

    return (output_dict, has_more, has_less)


# FIXME: simplificar
def get_output_dict_from_opciones(opciones, first, sum_total, perfil, filtro, unidad,
                        qs_emisiones_filtradas_comparadas, aplico_descuentos):
    # Toma una lista de opciones ordenadas por id y genera el diccionario de salida, calculando:
    # * Porcentajes del total
    # * Porcentaje respecto de la opción máxima, para las barras azules
    #   (FIXME: esto es visualización, mover al javascript)

    # el model del filtro
    cls = FILTERS[filtro.name()].cls
    lookup_filtro = FILTERS[filtro.name()].get_lookup(perfil.otras_marcas)
    agg = get_agg_for_magnitude(unidad)

    output_dict = []

    # solo puedo calcular valores relativos si hay resultados:
    if opciones:
        if first['magnitud']:
            max_porc = float(str(first['magnitud'])) / sum_total * 100
        else:
            max_porc = 0

    for op in opciones:
        if op['magnitud']:
            absoluto = float(str(op['magnitud']))
        else:
            absoluto = 0.0

        porc = ((absoluto + 0.0) / sum_total) * 100
        porc_string = "%.1f" % porc

        if aplico_descuentos and 'id' in op:
            id_opcion = op['id']
        else:
            id_opcion = op[lookup_filtro + "__id"]

        opcion = {'id': id_opcion, 'valor': absoluto, 'porcentaje_valor': porc,
                  'porcentaje': porc_string,
                  'absoluto_acotado': get_absoluto_acotado(absoluto)}

        # si estamos devolviendo filtro productos, agrego el filetype
        if str(filtro.tipo).lower() == "producto":
            producto = Producto.objects.get(id=id_opcion)
            patron = producto.patron_principal()
            opcion['filetype'] = patron.filetype() if patron else None

        output_dict.append(opcion)

    # calculo los porcentajes relativos (para dibujar las barritas azules)
    if qs_emisiones_filtradas_comparadas != None:
        for op in output_dict:
            if max_porc:
                op['porcentaje_relativo'] = op['porcentaje_valor'] / max_porc * 100
            else:
                op['porcentaje_relativo'] = 0
            op['int_porcentaje_relativo'] = str(int(op['porcentaje_relativo'])) + "%"

            instance = cls.objects.get(id=op['id'])

            # FIXME: obtener emisiones filtradas del periodo anterior, fuera del loop antes de empezar
            if aplico_descuentos:
                qs_con_filtro_actual = qs_emisiones_filtradas_comparadas.filter(**{lookup_filtro: instance.id})
                resultados_comparados = aplicar_descuentos(qs_con_filtro_actual, filtro)
                resultados_comparados = formatear_resultados(resultados_comparados)
                if resultados_comparados:
                    cantidad_comparada = resultados_comparados[0]['magnitud']
                else:
                    cantidad_comparada = 0
            else:
                # filtro desde el queryset que ya tengo
                resultados_comparados = get_resultados_from_queryset(qs_emisiones_filtradas_comparadas, agg, lookup_filtro)
                filtrado = qs_emisiones_filtradas_comparadas.filter(**{lookup_filtro: instance.id})
                dict_emisiones = {}
                dict_emisiones['sin_descuentos'] = filtrado
                cantidad_comparada = obtener_cantidad(dict_emisiones, unidad)

            #comparando = Periodo.objects.filter(perfil=perfil).count() > 1
            op['comparando'] = True  # FIXME esto esta forzado a True, por que?

            # calculo el procentaje de incremento/decremento respecto al
            # periodo anterior, si cantidad == 0 quiere decir que en el periodo
            # anterior no hubo emisiones
            cantidad_comparada = float(cantidad_comparada)
            if cantidad_comparada > 0:
                op['variacion_porcentual'] =\
                        "%.0f" % (((op['valor'] - cantidad_comparada) * 100) /
                                    cantidad_comparada)
            else:
                op['variacion_porcentual'] = " - "

            op['item'] = str(instance)  # instance.nombre

            if str(filtro.tipo) == "producto":
                op['marca'] = ""
                if instance.marca:
                    op['marca'] = instance.marca.nombre
                op['anunciante'] = ""
                if instance.marca.anunciante:
                    op['anunciante'] = instance.marca.anunciante.nombre

    return output_dict


def formatear_resultados(resultados):
    results = []
    for elem in resultados:
        results.append({"id": elem[0], 'nombre': elem[1], 'magnitud': elem[2]})
    # order by magnitud desc
    return sorted(results, key=lambda t: t['magnitud'], reverse=True)


def aplicar_descuentos(query_set, filtro=None, total=False):
    """
        toma un queryset de emisiones filtradas y les aplica descuentos

        Si le paso un filtro, agrupa los resultados por opciones de ese filtro
        y ordena las opciones de mayor a menor según la magnitud indicada en la query
    """
    conexion = connection.cursor()
    consulta = reparar_consulta(query_set.query.sql_with_params())

    tarifa_real = "(app_emision.valor * (100.0 - app_descuento.descuento)/100)"
    select_part, from_part = consulta.lower().split("from")
    sin_select = "FROM " + from_part
    sin_order_by = sin_select.split("order by")[0]

    if total:
        consulta_con_descuentos = 'select Sum(%s) as tarifa_real %s ' \
                % (tarifa_real, sin_order_by)
    elif filtro is None:
        consulta_con_descuentos = '%s, %s as tarifa_real %s'  \
                % (select_part, tarifa_real, sin_order_by)
    else:
        nombre_filtro = filtro.tipo.name
        tabla = nombre_filtro.replace(" ", "")
        consulta_con_descuentos = 'select app_%s.id as ID, app_%s.nombre as nombre, Sum(%s) as tarifa_real %s  \
                GROUP BY app_%s.id, app_%s.nombre ORDER BY Sum(%s)' \
                % (tabla, tabla, tarifa_real, sin_order_by, tabla, tabla, tarifa_real)

    conexion.execute(consulta_con_descuentos)
    resultados = conexion.fetchall()

    return resultados


def reparar_consulta(consulta):
    """
        Funcion auxiliar, para recuperar la consulta de las emisiones filtradas, no
        uso directamente la query que me pasan porque le faltan las "" a las fechas
    """
    strings = []
    is_sqlite = settings.DATABASES['default']['ENGINE'] ==\
                                                "django.db.backends.sqlite3"
    # Le agrego las "" a las fechas porque sino la consulta da []
    for elem in consulta[1]:
        if "-" in str(elem):
            try:
                fecha = datetime.strptime(str(elem), "%Y-%m-%d").\
                                                        strftime("%Y-%m-%d")
            except:
                fecha = None

            if fecha and not is_sqlite:
                """ https://code.djangoproject.com/ticket/17741 """
                dato = """ DATE '""" + fecha + """' """
            else:
                dato = elem

        else:
            dato = elem
        strings.append(dato)

    query = consulta[0] % tuple(strings)
    return query


def get_agg_for_magnitude(unidad):
    if unidad == "cantidad":
        agg = Count("id")
    elif unidad == "volumen":
        agg = Sum("duracion")
    elif unidad == "inversion":
        agg = Sum("valor")
    else:
        raise Exception("La unidad {} no vale".format(unidad))

    return agg


def cantidades_filtradas(perfil, opcion_comparada=None):
    """ devuelde las cantidades filtradas
    unidad Filtrada
        la suma en esa unidad
        la cantidad_anuncios
        la cantidad_segundos
        la cantidad_inversion (aplicando descuentos)
    """
    unit = perfil.get_unit()

    comparativo_periodo, definido_por_usuario = perfil.get_comparativo_periodo()
    # VER: el vs. sólo se muestra cuando hay comparación de periodos pedida por el usuario
    if not definido_por_usuario:
        comparativo_periodo = None

    filtros_adicionales = {}

    # agrego un filtro para filtrar por una opcion comparada
    if opcion_comparada:
        filtro_comparado = FILTERS[str(perfil.comparado)]
        lookup_comparado = filtro_comparado.lookup
        filtros_adicionales[lookup_comparado] = opcion_comparada

    qs_emisiones_filtradas = {}
    qs_emisiones_filtradas['actuales'] = {}
    qs_emisiones_filtradas['comparadas'] = {}

    qs_emisiones_filtradas['actuales']['sin_descuentos'], qs_emisiones_filtradas['comparadas']['sin_descuentos'] =\
            get_qs_emisiones_filtradas(perfil, comparativo_periodo, con_descuentos=False, filtros_adicionales=filtros_adicionales)

    qs_emisiones_filtradas['actuales']['con_descuentos'], qs_emisiones_filtradas['comparadas']['con_descuentos'] =\
            get_qs_emisiones_filtradas(perfil, comparativo_periodo, con_descuentos=True, filtros_adicionales=filtros_adicionales)

    cantidades = {}

    for magnitud in ["cantidad", "volumen", "inversion"]:
        cantidades[magnitud] = [obtener_cantidad(qs_emisiones_filtradas['actuales'], magnitud, con_formato=True)]

    if definido_por_usuario:
        for magnitud in ["cantidad", "volumen", "inversion"]:
            cantidades[magnitud].append(obtener_cantidad(qs_emisiones_filtradas['comparadas'], magnitud, con_formato=True))

    sum_total = cantidades[unit][0]

    return (unit, sum_total, cantidades["cantidad"], cantidades["volumen"],
                cantidades["inversion"])


def obtener_cantidad(emisiones, magnitud, con_formato=False):
    """ devuelve la cantidad de emisiones segun el tipo de filtro aplicado
    a las cantidades
    """
    if magnitud == "inversion":
        emisiones = emisiones['con_descuentos']
        resultado_total = aplicar_descuentos(emisiones, total=True)
        cantidad = resultado_total[0][0]
    else:
        emisiones = emisiones['sin_descuentos']
        agg = get_agg_for_magnitude(magnitud)
        cantidad = emisiones.aggregate(agg).values()[0]

    if not cantidad:
        cantidad = 0

    if con_formato:
        cantidad = formatear_cantidad(cantidad, magnitud)

    return cantidad


def formatear_cantidad(cantidad, unidad):
    cantidad = format_con_comas(int(float(str(cantidad))))

    if unidad == "inversion":
        cantidad = "$" + cantidad
    elif unidad == "volumen":
        # FIXME ver dónde poner esas unidades
        # (sacarlas de la vista del unit selector)
        # cantidad = cantidad + u" seg|cm²"
        pass

    return cantidad


def clonar_emisiones(perfil, inicio_periodo_clonado, clonar_a_medio=None):
    """
        Toma las emisiones del filtradas segun el perfil dado
        y las 'clona' en un periodo de igual longitud al periodo principal
        del perfil, comenzando en la fecha dada, en el medio dado
    """
    servidor = Servidor.objects.filter(nombre="Archivos externos")[0]
    migracion = Migracion.objects.create(fecha=date.today(),
                                        hora=datetime.now(),
                                        servidor=servidor)
    periodo = perfil.get_periodo_or_create()
    actual_date = periodo.fecha_desde
    emisiones = get_qs_emisiones_filtradas(perfil)[0]
    diff = inicio_periodo_clonado - actual_date

    n_avs = 0
    n_graf = 0
    n_otros = 0

    for e in emisiones:
        try:
            av = e.audiovisual
        except:
            av = None
        try:
            gr = e.grafica
        except:
            gr = None
        if av:
            n_avs += 1
            av2 = AudioVisual.objects.get(id=av.id)
            av2.id = None
            av2.pk = None
            av2.fecha = av2.fecha + diff
            av2.migracion = migracion
            av2.inicio = 0
            av2.fin = 0
            #av2.origen = ""
            if clonar_a_medio:
                av2.set_horario(clonar_a_medio)
            av2.save()
        elif gr:
            n_graf += 1
            gr2 = Grafica.objects.get(id=gr.id)
            gr2.id = None
            gr2.pk = None
            gr2.fecha = gr2.fecha + diff
            gr2.migracion = migracion
            if clonar_a_medio:
                gr2.set_horario(clonar_a_medio)
            gr2.save()
        else:
            n_otros += 1

    print "clonamos %d avs y %d graficas.\n" % (n_avs, n_graf)
    print "Encontramos %d otros" % n_otros
