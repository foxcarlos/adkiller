//Angular Script

(function(){


	var app = angular.module('infoxel_multimedia',[]);

    app.config(function($interpolateProvider) {
        $interpolateProvider.startSymbol('{$');
        $interpolateProvider.endSymbol('$}');
    }).config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
		$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    });


	var filters = {
		canal: undefined,
		fecha: undefined,
		programa: undefined,
	};


	app.controller('MultimediaController', ['$http', function($http){
		var multimedia = this;	//para distintos scopes	

		this.filters = filters;
		this.post_disabled = true;		
		this.programas = [];
		this.archivos_disponibles = [];
		this.error_message = undefined;
		this.ajax = {
			is_loading : false,
			message : 'Procesando...',
			block_ui : false
		}

		minutos_a_esperar = 5;

		this.make_filter = function(){

			//Hace el Get para obtener programas disponibles
			if ( this.custom_validation_form() ) {
				
				this.programas = [];
				$('#id_programa').eq(0);

				this.showAjaxMessage('Obteniendo Programas disponibles...');

				$http({
				    url: urlPostForm, 
				    method: "GET",
				    params: {
				    	'action':'get_programas',
				    	'params':{
					    	canal: this.filters.canal,
					    	fecha: this.filters.fecha,
					    }
				    }
				 }).success(function(response){
				 	multimedia.hideAjaxMessage();

				 	if (response.error.error_code == 0){
			 			multimedia.programas = response.data;	//porque por el scope de la funcion this.programas no es valido		 		
				 	}else{
				 		multimedia.programas = []	
				 		$('#id_programa').eq(0);
				 	}
				 });

			}

			this.check_form();						 	
		};

		this.check_form = function() {
			//alert('AZUZU');
			if ( this.custom_validation_form() && this.filters.programa ){
	
				this.post_disabled = false;
	
			}else{
				this.post_disabled = true;
			}

		}


		this.custom_validation_form = function(){
			//console.log('custom_validation_form -->'+this.filters.canal +' <- '+ this.filters.fecha);
			if (this.filters.canal && this.filters.fecha && this.validDate(this.filters.fecha) ){
				return true;
			}else{
				return false;
			}
		}


		this.post_multimedia_form = function(){

			var name = "ultima_descarga=";
		    var ca = document.cookie.split(';');
		    var value = 0;

		    for(var i=0; i<ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1);
		        if (c.indexOf(name) != -1) value = c.substring(name.length,c.length);
		    }

	     	var dif = Math.abs( new Date() - new Date(value) ) / (60*1000);

	     	if ((dif - minutos_a_esperar).toFixed(0) >= 0){//
				multimedia.archivos_disponibles = [];

				multimedia.error_message = false;
				this.showAjaxMessage('Obteniendo Archivos disponibles...');
				this.block_ui(true);
				$http({
					    url: urlPostForm, 
					    method: "POST",
					    data: $.param(this.filters)
					 }).success(function(response){
					 	multimedia.hideAjaxMessage();
					 	multimedia.block_ui(false);
					 	if (response.error.error_code == 0){
					 		multimedia.archivos_disponibles = response.data;
					 		//debug
					 		console.log(multimedia.archivos_disponibles);
					 	}else{
					 		multimedia.error_message = response.error.error_message;
					 		multimedia.archivos_disponibles = [];
					 	}

					 });
			} else {
				alert('Debe esperar  '+parseFloat(minutos_a_esperar - dif).toFixed(0)+' minutos después de descargar un archivo para realizar una nueva búsqueda.');
			}

		}

		this.validDate = function(p_date){ //basico valida date por rango
			var pieces = p_date.split('-');
			pieces.reverse();
			var reversed = pieces.join('-');

			date = new Date(reversed);
			if (date >= new Date('01-01-2999')){
				console.log('validDate '+ false)
				return false;
			}else{
				console.log('validDate '+ true)
				return true;
			}

		}



		this.showAjaxMessage = function(message){
			this.ajax.is_loading = true;
			this.ajax.message = message;
		}

		this.hideAjaxMessage = function(){
			this.ajax.is_loading = false;
			this.ajax.message = 'Procesando...';
		}

		this.block_ui = function(param){
			this.ajax.block_ui = param;
		}


		this.download_file = function(url){

			document.cookie="ultima_descarga="+new Date();
			console.log(' iframing '+url);
	    	iframe = document.createElement('iframe');
			iframe.style.display = 'none';
			document.body.appendChild(iframe);
			iframe.src = url;
		}


	}]);
	


})();