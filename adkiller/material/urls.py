
from django.conf.urls import patterns, url
from django.contrib import admin
from views import MainView, download_file
from django.contrib.auth.decorators import login_required
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', MainView.as_view(), name='material_home'),
    url(r'^download/(?P<params>.*)/$', download_file, name='material_download_file'),
    #login_required()
)
