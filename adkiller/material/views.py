from django.http import HttpResponse
from django.views.generic.edit import FormView
from forms import MultimediaForm
from adkiller.app.models import Medio, Horario
import json
import os
import datetime
import urllib2
from django.core.servers.basehttp import FileWrapper
import time
from django.http import HttpResponse as StreamingHttpResponse
from django.db.models import Q

import logging
logger = logging.getLogger(__name__)

# Esto se supone que iria en Settings o en un getter mas dinamico
multimedia_paths = (
    {'path': 'multimedia/ar/Origenes/'},
    {'path': 'multimedia/ar/OrigenesCortados/'}
)
archivos_disponibles = []

class MainView(FormView):
    form_class = MultimediaForm
    template_name = 'multimedia.html'
    success_url = '/'

    def __init__(self, *args, **kwargs):
        super(MainView, self).__init__(*args, **kwargs)

    def get(self, request, *args, **kwargs):

        ajax_actions = {
            'get_programas': self._ajax_get_programas,
        }

        if request.is_ajax():
            return ajax_actions[request.GET['action']](json.loads(request.GET['params']))

        return super(MainView, self).get(self, request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        form.fields['programa'].choices = [(horario.id, horario.programa.nombre) for horario in
                                           self._get_horarios(request.POST)]
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        f_medio_id = form.cleaned_data['canal'].id
        f_fecha = form.cleaned_data['fecha']
        f_programa_id = form.cleaned_data['programa']

        archivos = self._get_archivos_disponibles(f_fecha, f_medio_id,f_programa_id)

        if archivos:
            data_to_return = {
                'error': {
                    'error_code': 0,
                },
                'data': list(archivos)
            }
        else:
            data_to_return = {
                'error': {
                    'error_code': 1,
                    'error_message': 'No hay archivos disponibles. Verifique filtros.'
                }
            }

        return HttpResponse(json.dumps(data_to_return), content_type="application/json")

    def _ajax_get_programas(self, params):
        """
            Retorna programas filtrando por parametros enviados
            :param params: parametros que pasan desde el GET
            """
        horarios = self._get_horarios(params)
        if horarios:
            data_to_return = {
                'error': {
                    'error_code': 0,
                },
                'data': [{'id': h.id, 'programa__nombre': h.programa.nombre} for h in horarios]
            }

        else:
            data_to_return = {
                'error': {
                    'error_code': 1,
                    'error_message': '0 objetos recuperados'
                }
            }

        return HttpResponse(json.dumps(data_to_return), content_type="application/json")

    def _get_horarios(self, params=None, medio_id=None, fecha=None):

        horarios_list = []

        #params de Query
        f_medio_id = medio_id if medio_id else params['canal']
        f_fecha = fecha if fecha else params['fecha']

        if not type(f_fecha) is datetime.date:
            f_fecha = datetime.datetime.strptime(f_fecha, '%d/%m/%Y')
        f_day = f_fecha.weekday()
        f_day = (f_day + 1) % 7

        medio = Medio.objects.get(id=f_medio_id)
        horarios = medio.horarios_de_una_fecha(f_fecha)

        for horario in horarios:
            horarios_list.append(horario)
                #{
                #'programa_id': horario.programa_id,
                #'programa__nombre': horario.programa.nombre,
                #'dias': horario.dias,
                #})

        return horarios_list

    def _get_archivos_disponibles(self, fecha=None, medio_id=None, f_horario_id=None):
        """
        Retorna lista de archivos disponibles con los parametros recibidos.
        :param medio_id: filtro id de Medio
        :param desde_fecha: filtro Desde fecha
        :param hasta_fecha: filtro Hasta Fecha
        :param hora_inicio: filtro Hora Inicio
        :param hora_fin: filtro Hora Fin
        :param horario_id: filtro id horario
        :return: lista de dct con definicion de archivos [{url, file_name},{url, file_name}}]
        """
        archivos_disponibles = []
        horarios = []
        file_id = 0
        f_fecha = fecha

        if not type(f_fecha) is datetime.date:
            f_fecha = datetime.datetime.strptime(f_fecha, '%d/%m/%Y')

        f_day = f_fecha.weekday()    # 0=Lunes, 1=Martes,etc
        f_day = (f_day + 1) % 7

        medio = Medio.objects.get(id=medio_id)

        for horario in Horario.objects.filter(id=f_horario_id):
            for folder in multimedia_paths:

                url = medio.server_grabacion.url
                puerto = medio.server_grabacion.puerto_http
                print 'parsing'


                desde = medio.dvr + "_" + str(medio.canal) + "_" + datetime.datetime.strftime(fecha, '%Y%m%d') + "_"
                hasta = desde 


                url = 'http://{0}:{1}/adtrack/listar_videos.php?folder={2}&desde={3}&hasta={4}'.format(url, puerto, folder['path'], desde, hasta)
                print url
                try:
                    html_to_parse = urllib2.urlopen(url).read()
                except urllib2.URLError as e:
                    html_to_parse = ''
                    logger.error('urllib2.URLError: %s' % e)
                print 'Fin_parsing'
                for link in str(html_to_parse).split("\n"):
                    f = link.split("/")[-1]

                    if len(f.split('.')) > 1 and len(f.split('_')) < 5:  # si es con extension
                        file_name = f.split('.')[0]
                        file_extension = f.split('.')[1]
                        file_params = file_name.split('_')
                        file_medio_dvr = file_params[0]
                        file_medio_canal = int(file_params[1])
                        file_date = datetime.datetime.strptime(file_params[2], '%Y%m%d').date()
                        file_time = datetime.datetime.strptime(file_params[3], '%H%M%S').time()

                        if (horario.horaInicio <= file_time <= horario.horaFin):

                            file_action = '/material/download/{0}&{1}&{2}&{3}'.format(medio.server_grabacion.puerto_http,
                                                                            medio.server_grabacion.url.replace('.', '%2E'),
                                                                            folder['path'].replace('/', '%2F'),
                                                                            f.replace('.', '%2E'))

                            archivo_disponible = {
                                'file_server': medio.server_grabacion.url,
                                'file_port': medio.server_grabacion.puerto_http,
                                'file_folder': folder['path'],
                                'file_name_clean': clean_name(f),
                                'file_name': f,
                                'file_id': file_id,
                                'file_action': file_action
                            }

                            archivos_disponibles.append(archivo_disponible)
                            file_id += 1

        return archivos_disponibles

def clean_name(name):
    start = name.rfind("_")
    end = name.rfind(".")
    name = name[start+1:end-2]
    name = name[:2]+":"+name[2:]+" hs"
    return name

def stream_generator(url, buffer=8192):
    sock = urllib2.urlopen(url)
    while True:
        content = sock.read(buffer)
        if not content: break
        yield content


def download_file(request, params):

    file_server = params.split('&')[1].replace('%2E', '.')
    port = params.split('&')[0]
    file_folder = params.split('&')[2].replace('%2F', '/')
    file_name = params.split('&')[3].replace('%2E', '.')

    url = 'http://{0}:{1}/{2}/{3}'.format(file_server, port, file_folder, file_name)
    print url
    #response = StreamingHttpResponse(stream_generator(url), content_type="video/x-msvideo")
    #response['Content-Disposition'] = "filename='%s'" % file_name

    response = StreamingHttpResponse(stream_generator(url), mimetype='application/force-download')
    response['Content-Disposition'] = "attachent; filename='%s'" % file_name

    return response


