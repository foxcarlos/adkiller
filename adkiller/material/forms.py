# -*- coding: utf-8 -*-
from django import forms
from adkiller.app.models import Medio
from django.contrib.admin.widgets import AdminTimeWidget, AdminDateWidget
from django.utils import timezone
from django.db.models import Q

medios = Medio.objects.filter(soporte__nombre="TV Aire Interior Básica", publicar=True)

class MultimediaForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(MultimediaForm, self).__init__(*args, **kwargs)
        print 'INIT FORM'
        self.set_angular_attr()

    canal = forms.ModelChoiceField(
        required=True,
        empty_label='No Seleccionado',
        widget=forms.Select(
            attrs={
                'class': 'form-control',
            }
        ),
        label='Medio',
        queryset=medios.order_by('nombre'),
        to_field_name='id',

    )

    fecha = forms.DateField(
        required=True,
        initial=timezone.now(),
        label='Desde Fecha',
        widget=forms.DateInput(
            attrs={
                'class': 'form-control',
                'type': 'date',
            }

        )

    )

    programa = forms.ChoiceField(
        #required=True, #Valida Angular
        choices=((None, 'No Seleccionado'),),
        widget=forms.Select(
            attrs={
                'class': 'form-control',
                #'disabled': 'disabled'
            }

        )
    )

    def set_angular_attr(self):
        """
            Setea los attr de AngularJs para utilizar en la vista
            :param form: form de la view
            """
        print 'set Angular_attr'
        self.fields['canal'].widget.attrs['ng-click'] = 'multimedia.make_filter()'
        self.fields['canal'].widget.attrs['ng-model'] = 'multimedia.filters.canal'


        # widget desde_fecha
        self.fields['fecha'].widget.attrs['ng-change'] = 'multimedia.make_filter()'
        self.fields['fecha'].widget.attrs['ng-model'] = 'multimedia.filters.fecha'

        #widget programa
        self.fields['programa'].widget.attrs['ng-blur'] = 'multimedia.check_form()'
        self.fields['programa'].widget.attrs['ng-click'] = 'multimedia.check_form()'
        self.fields['programa'].widget.attrs['ng-model'] = 'multimedia.filters.programa'













    # def get_programas_choices(self):
    #     if self.cleaned_data.canal and self.cleaned_data.desde_fecha and self.cleaned_data.hasta_fecha \
    #             and self.cleaned_data.hora_inicio and self.cleaned_data.hora_fin:
    #
    #         medio = Medio.objects.get(id=self.cleaned_data['canal'])
    #         horarios = medio.horarios.filter(
    #             Q(fechaAlta__gte=self.cleaned_data['desde_fecha']) &
    #             Q(fechaAlta__lte=self.cleaned_data['hasta_fecha']) |
    #             Q(vigenciaHasta__gte=self.cleaned_data['desde_fecha']) &
    #             Q(vigenciaHasta__lte=self.cleaned_data['hasta_fecha']),
    #             Q(horaInicio__gte=self.cleaned_data['hora_inicio']) &
    #             Q(horaInicio__lte=self.cleaned_data['hora_fin']) |
    #             Q(horaFin__gte=self.cleaned_data['hora_inicio']) &
    #             Q(horaFin__lte=self.cleaned_data['hora_fin'])
    #         ).values('programa_id', 'programa__nombre')
    #
    #         return [(horario['programa_id'], horario['programa__nombre']) for horario in horarios]
    #     else:
    #         return ((None, 'No Seleccionado'),),