from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.simple import direct_to_template

from longerusername.forms import AuthenticationForm
from django import forms


class AuthForm(AuthenticationForm):
    username = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(AuthenticationForm, self).__init__(*args, **kwargs)
        self.fields['username'].help_text = None
        self.fields['username'].max_length = 120


import autocomplete_light
autocomplete_light.autodiscover()

from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api
from app.api import *
from home.views import root
from app.views import home, exportar_programas, exportar_marcas
from ratings.views import upload_ratings

emisiones = Api(api_name='emisiones')
emisiones.register(ProgramaResource())
emisiones.register(ProductoResource())
emisiones.register(EmisionResource())
emisiones.register(HorarioResource())
emisiones.register(CiudadResource())
emisiones.register(MedioResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'adkiller.views.home', name='home'),
    # url(r'^adkiller/', include('adkiller.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/exportar_programas', exportar_programas),
    url(r'^api/exportar_marcas', exportar_marcas),
    (r'^api/', include(emisiones.urls)),
    (r'^app/', include('adkiller.app.urls')),
    (r'^decos/', include('adkiller.decos.urls')),
    (r'^material/', include('adkiller.material.urls')),
    (r'^home/', include('adkiller.home.urls')),
    (r'^social/', include('adkiller.social.urls')),
    (r'^filtros/', include('adkiller.filtros.urls')),
    (r'^informes/', include('adkiller.informes.urls')),
    (r'^usuarios/', include('adkiller.usuarios.urls')),
    (r'^adtrack/', include('adkiller.adtrack.urls')),
    (r'^control/', include('adkiller.control.urls')),
    (r'^apis/', include('adkiller.apis.urls')),
    (r'^media_map/', include('adkiller.media_map.urls')),
    url(r'^$', root, name='root'),
    (r'^autocomplete/', include('autocomplete_light.urls')),
    (r'^grappelli/', include('grappelli.urls')),
    (r'^accounts/login/$', 'django.contrib.auth.views.login',  {'authentication_form': AuthForm}),
    (r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    (r'^accounts/', include('django.contrib.auth.urls')),
    (r'^accounts/register/$','adkiller.usuarios.views.register'),
    (r'^estado_server/(?P<server>\w+)/','adkiller.app.views.estado_server'),
    (r'^estado_server/','adkiller.app.views.estado_servers'),
    (r'^accounts/register_ok/(?P<usuario>\d+)/','adkiller.usuarios.views.register_ok'),
    (r'^dowser/', include('django_dowser.urls')),

    (r'^thankyou/', direct_to_template, {'template': 'thankyou_page.html','extra_context':{'settings':settings}}),
    (r'^propuesta/', direct_to_template, {'template': 'landing_propuesta.html','extra_context':{'settings':settings}}),
    (r'^accounts/login_demo/$', 'django.contrib.auth.views.login',{'template_name': 'registration/login_demo.html', 'authentication_form': AuthForm, 'extra_context':{'settings':settings}}),

    # archivos especiales
    (r'^jwplayer/jwplayer\.js$', 'django.views.generic.simple.redirect_to', {'url': '/static/js/player/jwplayer.js', 'permanent': True}),
    (r'^jwplayer/jwplayer\.html5\.js$', 'django.views.generic.simple.redirect_to', {'url': '/static/js/player/jwplayer.html5.js', 'permanent': True}),
    (r'^jwplayer/jwplayer\.flash\.swf$', 'django.views.generic.simple.redirect_to', {'url': '/static/js/player/jwplayer.flash.swf', 'permanent': True}),
    (r'^googledae215d9eaff8abb\.html$', 'django.views.generic.simple.redirect_to', {'url': '/static/googledae215d9eaff8abb.html'}),
    (r'^google2f37b16a81f0f6e7\.html$', 'django.views.generic.simple.redirect_to', {'url': '/static/google2f37b16a81f0f6e7.html'}),
    (r'^sitemap\.xml$', 'django.views.generic.simple.redirect_to', {'url': '/static/home/sitemap.xml'}),
    (r'^robots\.txt$', 'django.views.generic.simple.redirect_to', {'url': '/static/home/robots.txt'}),
    (r'^htaccess\.txt$', 'django.views.generic.simple.redirect_to', {'url': '/static/home/htaccess.txt'}),
    (r'^ratings/', upload_ratings),



) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += [
    url(r'^mediamap/static/(?P<path>.*)$',  'django.views.static.serve', {'document_root': settings.MEDIAMAP_ROOT}),
]

urlpatterns += staticfiles_urlpatterns()

#Para detectar memory leaks:
#import guppy
#from guppy.heapy import Remote
#Remote.on()


#DEBUG TOOLBAR: ejecutarlo con servidor de desarrollo de django
#DEBUG_TOOLBAR_PATCH_SETTINGS = False
#INTERNAL_IPS = ["174.37.209.218", "127.0.0.1"]
#from django.conf.urls import include, patterns, url
#if settings.DEBUG:
#    import debug_toolbar
#    urlpatterns += patterns('',
#        url(r'^__debug__/', include(debug_toolbar.urls)),
#    )
