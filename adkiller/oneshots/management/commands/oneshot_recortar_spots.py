# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import Patron, Producto, Marca, Sector, Industria


class Command(BaseCommand):
    def handle(self, *args, **options):
        industria = args[0]
        print "Buscando patrones de industria", industria
        productos = Producto.objects.filter(
                patrones__mp4_filename__isnull=False).filter(
                        emisiones__fecha='2014-06-17',
                        marca__sector__industria__nombre=industria,
                        ).distinct()
        for index, producto in enumerate(productos):
            patron = producto.patron_principal()
            if patron.server.nombre == "Maggie":
                continue
            print
            print "%d: trabajando con patron" % index, patron.id_en_simpson
            print patron.producto.id
            print patron.producto.marca.nombre
            print patron.producto.nombre
            print patron.producto.marca.sector.industria.nombre
            print "direccion para controlar:",
            print "http://infoad.com.ar/app/ver_video/?path=/app/video_producto/%d&filetype=mp4" % producto.id
            print
            recortar = raw_input("Desea recortar este spot? (s/n)")
            if recortar.lower().startswith('s'):
                backup = patron.mp4_filename
                patron.mp4_filename = None
                resultado = patron.crear_mp4()
                if resultado:
                    print "\nlisto!"
                else:
                    patron.mp4_filename = backup
