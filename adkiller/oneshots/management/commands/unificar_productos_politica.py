# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import *
import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        for prod in Producto.objects.filter(marca__sector__nombre__iexact="Partidos Políticos"):
            otros = Producto.objects.filter(nombre=prod.nombre, marca__nombre__contains=prod.marca.nombre).exclude(id=prod.id)
            if otros:
                otros[0].unificar(prod)
                continue
            otros = Producto.objects.filter(alias__nombre=prod.nombre, marca__nombre__contains=prod.marca.nombre).exclude(id=prod.id)
            if otros:
                otros[0].unificar(prod)
                continue
            