# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import Patron, Producto, Marca, Sector, Industria
import codecs


class Command(BaseCommand):
    def handle(self, *args, **options):

        f = codecs.open("productos.csv", "w", "utf-8")
        f.write( "id,marca,producto\n")
        for producto in Producto.objects.filter(codigo_directv__isnull=False):
            f.write("%d,%d,'%s'\n" % (producto.id, producto.marca.id, producto.nombre))
        f.close()