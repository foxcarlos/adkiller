# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from adkiller.app.models import *
import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):

        ass = AudioVisual.objects.filter(fecha__gte=datetime.date.today() - datetime.timedelta(days=7))
        for a in ass:
            if len(a.origen.split(".")) == 2:
                origen = a.origen.split(".")[0]
                hora = int(origen.split("-")[-1][:2])
                if abs(int(a.hora.strftime("%H")) - hora) == 12 :
                    print a.origen, a.hora
                    a.hora = datetime.time((a.hora.hour + 12) % 24, a.hora.minute, a.hora.second, a.hora.microsecond)
                    print "Nueva hora:", a.hora
                    if a.horario:
                        a.set_horario(a.horario.medio)
                    a.save()
                
