# -*- coding: utf-8 -*-
"""
Urls for app control
"""

from django.conf.urls import patterns, url

from adkiller.control.views import as_run
from adkiller.control.views import programacion_dtv
from adkiller.control.views import metricas_diarias_user
from adkiller.control.views import control_tandas
from adkiller.control.views import alerta_falla_video

urlpatterns = patterns(
    '',
    url(r'^as_run', as_run),
    url(r'^programacion_dtv', programacion_dtv),
    url(r'^metricas/', metricas_diarias_user),
    url(r'^tandas/', control_tandas),
    url(r'^vnod/', alerta_falla_video), # vnod: video no descarga
)
