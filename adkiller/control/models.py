# -*- coding: utf-8 -*-
# pylint: disable=fixme, too-many-arguments, redundant-keyword-arg
# pylint: disable=old-style-class
"""
Models for app control
"""

# python imports
from datetime import timedelta

# django imports
from django.db import models
from django.contrib.auth.models import User
from django.contrib.admin.models import LogEntry

# project imports
from adkiller.app.models import Emision
from adkiller.app.models import Medio
from adkiller.app.models import Soporte
from adkiller.media_map.models import Formato
from adkiller.media_map.models import TipoSegmento

class MetricasGrab(models.Model):
    """..."""
    # MUESTRA:
    # el dia que se esta midiendo
    fecha = models.DateField()
    # es el conjunto de canales que se esta midiendo
    media_pack = models.ForeignKey(Soporte, null=True, blank=True)

    # GRABACION:
    # son los que tienen servidor de grabacion, es decir,
    # los que se piensan grabar
    medios = models.IntegerField()
    # son los que no tienen pérdida de material en ese dia
    medios_grabados = models.IntegerField()
    # son los que cumplen los requerimientos mínimos
    # (imagen en color, con audio, etc)
    medios_en_calidad_estandar = models.IntegerField()

    # para llenar con datos viejos, primero hay que
    # definir la fecha y media_pack
    def calculate_old(self):
        """..."""
        self.medios = Emision.objects.filter(
            fecha=self.fecha,
            horario__medio__soporte=self.soporte
        ).values("horario__medio").distinct().count()

    # para llenar con datos de ayer, primero hay que
    # definir el media_pack
    def calculate(self):
        """..."""
        self.medios = Medio.objects.filter(
            server_grabacion__isnull=False,
            soporte=self.soporte
        )


class MetricasProc(models.Model):
    """..."""
    # SOLO DESPUES DE 72 hs

    # MUESTRA:
    # el dia que se esta midiendo
    fecha = models.DateField()
    # es el conjunto de canales que se esta midiendo
    media_pack = models.ForeignKey(Soporte, null=True, blank=True)


    # PROCESAMIENTO:
    # cantidad de registros
    registros_infoad = models.IntegerField()
    # cantidad de horas que se aplicaron para generar esos registros
    horas_aplicadas_infoad = models.IntegerField()

    def calculate(self):
        """..."""
        self.registros_infoad = Emision.objects.filter(
            fecha=self.fecha,
            horario__medio__soporte=self.media_pack
        ).count()
        # FIXME
        self.horas_aplicadas_infoad = 10


class MetricasUser(models.Model):
    """..."""

    # MUESTRA:
    # el dia que se esta midiendo
    fecha = models.DateField()

    # USUARIOS
    # cantidad de nuevos usuarios en el dia
    nuevos_prospectos_infoad = models.IntegerField()
    # cantidad de logins en el dia
    ingresos_infoad = models.IntegerField()


    def calculate(self):
        """..."""
        tomorrow = self.fecha + timedelta(days=1)
        self.nuevos_prospectos_infoad = User.objects.filter(
            date_joined__gte=self.fecha,
            date_joined__lt=tomorrow
        ).count()
        self.ingresos_infoad = LogEntry.objects.filter(
            action_flag=5,
            action_time__gte=self.fecha,
            action_time__lt=tomorrow
        ).count()


class MetricaDiariaUser(models.Model):
    """Clase para medir performance diaria
    de un usuario en la carga de patrones"""

    BLANK = 0
    CARGA_PATRON_IMAGEN = 1
    CARGA_PATRON_AUDIO = 2
    REGISTRO = 3
    EDICION = 4
    CONTROLANDO = 5
    CERRANDO = 6
    NAVEGACION = 6

    ACCION_CHOICES = (
        (BLANK, u''),
        (CARGA_PATRON_IMAGEN, u'Carga patrón imágen'),
        (CARGA_PATRON_AUDIO, u'Carga patrón audio'),
        (REGISTRO, u'Registro de datos'),
        (EDICION, u'Actualización de datos'),
        (NAVEGACION, u'Navegación'),
    )

    fecha = models.DateField()
    fecha_material = models.DateField(null=True)
    user = models.ForeignKey(User)
    medio = models.ForeignKey(Medio)
    tipo_segmento = models.ForeignKey(TipoSegmento)
    formato = models.ForeignKey(Formato, null=True)
    cantidad = models.IntegerField()
    material = models.IntegerField(null=True)
    duracion = models.FloatField()
    accion = models.SmallIntegerField(choices=ACCION_CHOICES, default=REGISTRO)


    def to_string(self):
        """Devolver str resumen"""
        tpl = '\n----------------------------------------------------------\n'
        tpl += u'Métrica {self.fecha} - {self.user.username} - {self.medio}\n'
        tpl += 'TIPO: {self.tipo_segmento} '
        if self.formato:
            tpl += 'FORMATO: {self.formato:10} '
        tpl += '\n'
        tpl += ('ACCION: {accion}\n'
                'CANTIDAD: {self.cantidad} '
                'TIEMPO: {self.duracion:.1f} segundos')
        tpl += '\n-----------------------------------------------------------'
        return tpl.format(self=self,
                          accion=[desc for (ac, desc) in self.ACCION_CHOICES
                                  if ac == self.accion][0])

    def get_accion_as_string(self):
        """Devolver string según el tipo de accion"""
        for choice in self.ACCION_CHOICES:
            if self.accion == choice[0]:
                return choice[1]
        # no debería llegar acá
        return u'Acción desconocida'

class Navigation(models.Model):
    """Clase para medir el tiempo
    que un usuario emplea en mediamap"""

    NM_CONTROLANDO = 0
    NM_CERRANDO = 1

    class Meta:
        """'Enforzar' clave candidata"""
        unique_together = (
            ('user', 'medio', 'inicio'),
        )

    NAVIGATION_MODE = (
        (NM_CONTROLANDO, u'Controlando'),
        (NM_CERRANDO, u'Cerrando'),
    )
    user = models.ForeignKey(User)
    medio = models.ForeignKey(Medio)
    desde_thumb = models.DateTimeField()
    hasta_thumb = models.DateTimeField()
    inicio = models.DateTimeField()
    fin = models.DateTimeField()

    # acción que se hizo luego de hacer esta navegación
    accion_destino = models.ForeignKey(MetricaDiariaUser, null=True, blank=True)

    modo = models.SmallIntegerField(choices=NAVIGATION_MODE,
                                    default=NM_CONTROLANDO)
    capas_activas = models.ManyToManyField(TipoSegmento)


