# -*- coding: utf-8 -*-
"""
Forms para app control
"""
from datetime import datetime

from django import forms
from django.contrib.auth.models import User
from django.forms.extras.widgets import SelectDateWidget

from adkiller.app.models import Medio
from adkiller.media_map.models import TipoSegmento

class AsRunForm(forms.Form):
    """As run form"""
    medio = forms.ModelChoiceField(
        Medio.objects.filter(
            soporte__nombre__in=[
                'Televisión',
                'TV Aire',
                'TV Cable',
                'TV Paga',
                'Tv Satelital'
            ]
        )
    )
    archivo = forms.FileField()
    inverso = forms.BooleanField(initial=False, required=False)
    crear_emisiones_no_encontradas = forms.BooleanField(initial=False,
                                                        required=False)

class MetricasDiariasUserForm(forms.Form):
    """Formulario para consultar métricas diarias"""
    desde = forms.DateField(initial=datetime.now().date(),
                            widget=SelectDateWidget)
    hasta = forms.DateField(initial=datetime.now().date(),
                            widget=SelectDateWidget)

    users = User.objects.filter(
        is_staff=True,
        is_active=True
    ).exclude(username='root').order_by('username')
    filtrar_por_usuario = forms.ModelChoiceField(required=False, queryset=users)
    ACCION_CHOICES = (
        (0, u''),
        (1, u'Carga patrón imágen'),
        (2, u'Carga patrón audio'),
        (3, u'Registro de datos'),
        (4, u'Actualización de datos'),
    )
    filtrar_por_accion = forms.ChoiceField(required=False,
                                           widget=forms.Select(),
                                           choices=ACCION_CHOICES)

    segmentos = TipoSegmento.objects.all().exclude(
        nombre='Material').exclude(
        nombre__contains='Navigation').exclude(
        nombre='Bache-IFEP')
    filtrar_por_tipo_de_segmento = forms.ModelChoiceField(segmentos,
                                                          required=False)

    medios = Medio.objects.all().exclude(soporte__nombre='Revista')
    medios = medios.exclude(soporte__nombre='Diario')
    medios = medios.exclude(soporte__nombre='Diarios Capital')
    medios = medios.exclude(soporte__nombre='Diarios Interior')

    filtrar_por_medio = forms.ModelChoiceField(queryset=medios, required=False)
