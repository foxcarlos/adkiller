# -*- coding: utf-8 -*-
# pylint: disable=no-self-use
"""
Admin para app control
"""
from django.contrib import admin
from django.contrib import messages
from django.shortcuts import redirect

from adkiller.control.models import MetricaDiariaUser
from adkiller.control.models import Navigation


class ReadOnlyModel(admin.ModelAdmin):
    """Admin para objetos que no queremos que se manipulen"""

    # pylint: disable=unused-argument
    def has_add_permission(self, request):
        """Nadie puede agregar!"""
        return False

    def has_delete_permission(self, request, obj=None):
        """Nadie puede borrar!"""
        return False
    # pylint: disable=unused-argument

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """Nadie puede cambiar"""
        error = 'Disculpe, pero la edición de estos datos no está permitida'
        messages.error(request, error)
        return redirect(request.META['HTTP_REFERER'])


class MetricaDiariaUserAdmin(ReadOnlyModel):
    """Admin para MetricaDiariaUser"""
    list_display = [
        'fecha',
        'user',
        'medio',
        'tipo_segmento',
        'formato',
        'accion',
        'cantidad',
        'duracion',
    ]
    search_fields = ['user', 'medio', 'accion', 'tipo_segmento']
    list_filter = ('fecha', 'user', 'medio',
                   'accion', 'tipo_segmento')


class NavigationAdmin(ReadOnlyModel):
    """Admin para Navgation"""

    list_display = [
        'user',
        'medio',
        'desde_thumb',
        'hasta_thumb',
        'inicio',
        'fin',
        'modo',
        '_accion_destino'
    ]
    def _accion_destino(self, obj):
        """No mostrar (None)"""
        result = ''
        if obj.accion_destino:
            result = obj.accion_destino.get_accion_as_string()
        return result

    search_fields = ['user', 'medio', ]
    list_filter = ('user', 'medio',)

admin.site.register(MetricaDiariaUser, MetricaDiariaUserAdmin)
admin.site.register(Navigation, NavigationAdmin)
