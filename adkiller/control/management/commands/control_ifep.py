# -*- coding: utf-8 -*-
"""
Controlar que se estén detectando placas
"""

from datetime import timedelta
from datetime import datetime

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage
from adkiller.media_map.models import AciertosIFEP
from adkiller.app.models import Medio
from adkiller.app.management.commands.ifep import MEDIOS_EXCLUIDOS
from adkiller.app.management.commands.ifep import es_de_kantar

import json

MINUTOS_ALERTA = {
    'canal': 10,
}


MAILS_ALERTA = settings.MAILS_CONTROL_IFEP
ATRASO_IFEP = 50 # minutos
TOLERANCIA = ATRASO_IFEP + 180 # tres horas

# estos medios no se controlan
MEDIOS_ALTERNANTES = [
    'DIRECTV Sports 611 [LAT]',
    'DIRECTV Sports + 613 [LAT]',
    'DIRECTV Sports 614 [LAT]',
    'DIRECTV Sports 615 [LAT]',
    'DIRECTV Sports 616 [LAT]',
    'DIRECTV Sports 617 [LAT]',
    'DIRECTV Sports 618 [LAT]',
    'DIRECTV Sports 635 [ECU]',
]

class Command(BaseCommand):
    JSON_FILE = 'contol_ifep.json'

    def load_last_run(self):
        try:
            data = json.loads(open(self.JSON_FILE, 'r').read())
            result = []
            for elem in data:
                result.append(tuple(elem))
            return result

        except (IOError, ValueError):
            return []

    def save_run(self, data):
        open(self.JSON_FILE, 'w').write(json.dumps(data))

    def handle(self, *args, **options):

        ultima_placa = AciertosIFEP.objects.all().order_by('-datetime')[1:][0]
        if datetime.now() - ultima_placa.datetime > timedelta(minutes=80):
            # recordar que ifep mira 50 minutos para atrás
            print 'Pasó mucho tiempo sin detectar NINGUNA placa'
            self.alerta_ifep_trabado(ultima_placa.datetime)
            exit(0)


        medios = Medio.objects.filter(soporte__nombre__icontains='tv')
        medios_sin_placas = []

        for medio in medios:
            if medio.nombre in MEDIOS_ALTERNANTES:
                continue
            if medio.nombre in MEDIOS_EXCLUIDOS:
                continue
            if not medio.dvr:
                continue

            pi, pf = medio.get_patrones_inicio_y_fin(solo_activos=True)

            if pi or pf:
                last = AciertosIFEP.objects.filter(
                    medio=medio,
                ).order_by('-datetime')[:1]
                if not last:
                    medios_sin_placas.append((medio.nombre, u'Jamás detectado'))
                else:
                    last = last[0]
                    tolerancia = MINUTOS_ALERTA.get(medio.nombre, TOLERANCIA)
                    if es_de_kantar(medio):
                        print 'ES DE KANTAR'
                        tolerancia += (60 * 3) # tres horas más de tolerancia
                    tiempo_sin_placas = datetime.now() - last.datetime
                    if tiempo_sin_placas > timedelta(minutes=tolerancia):
                        medios_sin_placas.append((medio.nombre, '{:%Y-%m-%d %H:%M:%S}'.format(last.datetime)))

        self.alerta_placas(medios_sin_placas)

    def alerta_ifep_trabado(self, ultima):
        msg = 'ALERTA URGENTE: última placa encontrada {}'.format(ultima)
        self.enviar_mail(msg)

    def alerta_placas(self, nuevos):
        """..."""
        viejos = self.load_last_run()
        diferencia = set(nuevos) ^ set(viejos)
        if diferencia:
            print 'enviando mail con cambios'
            msg = u'<h1>Control IFEP</h1>'
            if nuevos:
                msg += u'<p>Pasó mucho tiempo sin detectar placas:</p><ul>'
                msg += u'<table><tr><th>Medio</th><th>Última placa</th></tr>'
                for (medio, tiempo) in sorted(nuevos, key=lambda t: t[1]):
                    msg += u'<tr><td>{}</td><td>{}</td></tr>'.format(medio, tiempo)
                msg += '</table>'
            else:
                msg += u'<p>Todo bien!</p>'
            self.enviar_mail(msg)
            self.save_run(nuevos)
        else:
            print 'no hubo cambios'

    def enviar_mail(self, msg):
        if 1 <= datetime.now().hour < 9:
            print 'No enviamos mails a esta hora...'
        else:
            email = EmailMessage('Control IFEP', msg, to=MAILS_ALERTA)
            email.content_subtype = 'html'
            email.send()
