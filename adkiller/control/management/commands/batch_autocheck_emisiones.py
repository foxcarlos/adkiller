# -*- coding: utf-8 -*-
# pylint: disable=unused-argument, no-self-use
"""
Correr autochek emisiones en batch
"""

from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.core.management import call_command

def fecha_iterator(d, h):
    """Devolver fecha tras fecha"""
    FORMATO = '%Y-%m-%d'
    fecha = datetime.strptime(d, FORMATO)
    limite = datetime.strptime(h, FORMATO)

    while fecha <= limite:
        yield fecha.strftime(FORMATO)
        fecha += timedelta(days=1)
    raise StopIteration


class Command(BaseCommand):
    """..."""
    def handle(self, *args, **options):
        """..."""
        try:
            desde = args[0]
            hasta = args[1]
        except IndexError:
            desde = '2015-07-20'
            hasta = '2015-08-07'
        for f in fecha_iterator(desde, hasta):
            call_command('autocheck_emisiones', f)

