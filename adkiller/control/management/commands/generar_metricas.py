# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, missing-docstring, no-self-use
# pylint: disable=attribute-defined-outside-init, no-value-for-parameter
# pylint: disable=unused-argument, too-many-branches
# pylint: disable=too-many-locals, unnecessary-lambda, unused-variable

"""
Calcular y registrar métricas de operaciones
"""

# python imports
from optparse import make_option
from datetime import datetime
from datetime import timedelta
from collections import defaultdict

# django imports
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage

# project imports
from adkiller.app.models import Medio
from adkiller.media_map.models import Segmento
from adkiller.control.models import MetricaDiariaUser
from adkiller.control.utils import MetricasManager

DATE_FORMAT = '%Y-%m-%d'
MAX_DURATION_SECONDS = 300

JERARQUIA_ORDEN = [
    ('fecha', None),
    ('user', lambda u: u.username),
    ('medio', lambda m: m.nombre),
    ('tipo_segmento', lambda t: t.pk),
]

def print_formato(f):
    """__str__ inteligente"""
    if f is None:
        return ''
    else:
        return f.nombre

def print_accion(a):
    """__str__ inteligente"""
    if a >= 0:
        return [desc for (ac, desc) in MetricaDiariaUser.ACCION_CHOICES if ac == a][0]
    else:
        return ''

def print_valor(c):
    """print cantidad de la métrica"""
    if c:
        return c
    else:
        return ""

JERARQUIA_TABLA = [
    ('Fecha', 'fecha', print_valor),
    ('Fecha Material', 'fecha_material', print_valor),
    ('Usuario', 'user', lambda u: u.username),
    ('Medio', 'medio', lambda m: m.nombre),
    ('Tipo de Segmento', 'tipo_segmento', lambda t: t.nombre),
    (u'Acción', 'accion', print_accion),
    ('Formato', 'formato', print_formato),
    ('Cantidad', 'cantidad', print_valor),
    ('Material', 'material', print_valor),
    ('Tiempo Aplicado', 'duracion', lambda d: '{}'.format(int(d))),
]

class Command(BaseCommand):

    option_list = BaseCommand.option_list + (
        make_option('--medio', help=u'Sólo este medio'),
        make_option('--user', help=u'Sólo este user'),
        make_option('--desde', help='analizar desde <YYYY-MM-DD>'),
        make_option('--hasta', help='analizar hasta <YYYY-MM-DD>'),
        make_option('--ayer', action='store_true', help='el día de ayer'),
        make_option('--html', action='store_true', help='Formatear resultado como html'),
        make_option('--mailto', help='Enviar output a este mail'),
    )

    def handle(self, *args, **options):
        exit('Script inhabilitado')
        try:
            cli_args = parse_and_validate_options(options)
        except CliArgsError as e:
            exit(str(e))

        segmentos = Segmento.objects.filter(
            created_at__gte=cli_args.desde,
            created_at__lt=cli_args.hasta,
            user__isnull=False,
            user__in=cli_args.users
        ).exclude(
            tipo__nombre__in=['Bache-IFEP', 'Material', 'Welo']
        ).order_by('created_at')

        if not segmentos:
            print 'No hay segmentos'

        users = segmentos.order_by('user').values_list('user', flat=True).distinct()
        mm = MetricasManager()

        for user in users:
            print 'Chequeando métricas para user id:', user

            segmentos_user = segmentos.filter(user_id=user)
            previo = None
            for segmento in segmentos_user:
                if previo:
                    # tomar min de algo y esto
                    segundos = min(MAX_DURATION_SECONDS, (segmento.created_at - previo.created_at).total_seconds())
                else:
                    segundos = 0
                mm.incrementar_cuenta(segmento, segundos=segundos, accion=MetricasManager.REGISTRO)
                boxes = segmento.boxes.all()
                if boxes:
                    mm.incrementar_cuenta(segmento,
                                          segundos=segundos,
                                          accion=MetricasManager.CARGA_PATRON_IMAGEN,
                                          cantidad_boxes=boxes.count())
                previo = segmento

        metricas = mm.metricas_list()
        mm.guardar_cambios()

        # salvar a base de datos
        for metrica in metricas:
            print metrica

        output = ''
        if cli_args.html:
            output = HtmlFormatter(metricas).obtener_html()
        else:
            for metrica in MetricasSorter(metricas).ordenar():
                output += metrica.to_string()

        print output

        if cli_args.mailto:
            email = EmailMessage(u'Métricas', output, to=[cli_args.mailto])
            if cli_args.html:
                email.content_subtype = 'html'
            email.send()
            print 'E-mail enviado a', cli_args.mailto


class CliArgsError(Exception):
    pass

def parse_and_validate_options(options):
    """Parsear las opciones de línea de comandos. Si hay
    inconsistencias, levantar una excepción."""

    class Result(object):
        """Sólo para agrupar los cli_args"""
        pass

    result = Result()

    # medio
    if options['medio']:
        try:
            medio = Medio.objects.get(nombre=options['medio'])
        except ObjectDoesNotExist:
            raise CliArgsError('No hay medio llamado "{}"'.format(options['medio']))
        else:
            result.medios = [medio]

    else:
        result.medios = Medio.objects.filter(soporte__nombre__icontains='tv')

    # fecha
    if options['ayer']:
        hoy = datetime.now().date()
        result.desde = hoy - timedelta(days=1)
        result.hasta = hoy
    else:
        if not options['desde'] or not options['hasta']:
            raise CliArgsError('Debe proveer "DESDE" y "HASTA" o utilizar la opción "--ayer"')
        try:
            result.desde = datetime.strptime(options['desde'], DATE_FORMAT).date()
            result.hasta = datetime.strptime(options['hasta'], DATE_FORMAT).date()
        except ValueError as e:
            raise CliArgsError(str(e))
    if not result.hasta <= datetime.now().date():
        raise CliArgsError('Hasta tiene que ser menor o igual que hoy')

    # user
    if options['user']:
        try:
            user = User.objects.get(username=options['user'])
        except ObjectDoesNotExist:
            raise CliArgsError('No hay usuario llamado "{}"'.format(options['user']))
        else:
            result.users = [user]

    else:
        result.users = User.objects.filter(
            is_active=True,
            is_staff=True,
            # last_login__gte=result['desde'],
        ).exclude(username='root')

    # salida
    result.html = bool(options['html'])

    if options['mailto']:
        result.mailto = options['mailto']
    else:
        result.mailto = False

    return result


class HtmlFormatter(object):
    """Generar tabla html con las métricas"""

    def __init__(self, metricas):
        self._data = MetricasSorter(metricas).ordenar()
        self._html = None
        self._styles = defaultdict(str)
        self._styles['table'] = 'background-color: #EEE;'
        self._styles['th'] = 'background-color: #BBB; padding: 10px 4px; margin: 2px 0px 2px 0px;'
        self._styles['td'] = 'padding: 4px'
        self._styles['fila_par'] = 'background-color: #DDD;'

    def obtener_html(self):
        """Devoler el html"""
        if self._html is None:
            self._format_html()
        return self._html

    def _format_html(self):
        """Generar el html"""

        thead = ''
        for (title, attr, _) in JERARQUIA_TABLA:
            thead += self._wrap(title, 'th', style='th')
        thead = self._wrap(thead, 'tr')
        thead = self._wrap(thead, 'thead')

        tbody = ''
        impar = True
        for metrica in self._data:

            row = ''
            for (_, attr, f) in JERARQUIA_TABLA:
                value = getattr(metrica, attr)
                if f:
                    value = f(value)
                row += self._wrap(value, 'td', 'td')
            if impar:
                style = 'fila_impar'
                impar = False
            else:
                style = 'fila_par'
                impar = True

            row = self._wrap(row, 'tr', style=style)
            tbody += row
        tbody = self._wrap(tbody, 'tbody')

        self._html = self._wrap(thead + tbody, 'table', style='table')

    def _wrap(self, content, tag, style='nope'):
        """Envolver string content en tags"""
        return u'<{tag} style="{style}">{content}</{tag}>'.format(tag=tag,
                                                                  content=content,
                                                                  style=self._styles[style])


class MetricasSorter(object):
    """Ordenar las métricas en una
    estructura de datos práctica para
    formatearlo luego"""

    def __init__(self, metricas):
        self._data = metricas
        self._sorted = False
        self._ordered_data = None

    def ordenar(self):
        """Devolver la info ordenada"""
        if not self._sorted:
            self._data.sort(cmp=self._cmp)
            self._sorted = True
        return self._data

    def _cmp(self, elem1, elem2):
        """comparar dos elementos

        In Py2.x, sort allowed an optional function which can be called for
        doing the comparisons. That function should take two arguments to be
        compared and then return a negative value for less-than, return zero if
        they are equal, or return a positive value for greater-than"""
        for (attr, f) in JERARQUIA_ORDEN:
            attr_1 = getattr(elem1, attr)
            attr_2 = getattr(elem2, attr)
            if f:
                attr_1 = f(attr_1)
                attr_2 = f(attr_2)
            if attr_1 < attr_2:
                return -1
            if attr_2 < attr_1:
                return 1
        return 0


