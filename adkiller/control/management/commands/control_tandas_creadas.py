# -*- coding: utf-8 -*-
# pylint: disable=missing-docstring, no-self-use, too-many-branches
# pylint: disable=line-too-long, too-many-locals, too-many-statements
"""
Controlar que se hayan creado las tandas
"""

# python imports
import requests
import time
from datetime import datetime
from datetime import timedelta
from optparse import make_option


# django imports
from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand

# project imports
from adkiller.media_map.models import Segmento

MARGEN_CORTE = timedelta(hours=8)
RETROACTIVO = timedelta(days=7)

MAILS_ALERTA = [
    'mmodenesi@infoad.com.ar',
    'maximiliano.vilamajo@infoad.com.ar',
]


# cantidad de veces que se re-intenta crear una tanda
MAX_RETRIES = 2

# Formato de fecha para los argumentos de línea de comandos
DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

class EdnaRequestError(Exception):
    pass

def get_status_tanda(filename):
    """Llamar a view homónima de edna"""

    url = 'http://edna.infoad.tv/API/get_status_tanda.php?filename={}'

    full_url = url.format(filename)
    request = requests.get(full_url)

    if not request.ok or request.status_code != 200:
        raise EdnaRequestError(request.text)

    try:
        response = request.json()
    except ValueError:
        raise EdnaRequestError(request.text)

    if not response['success']:
        raise EdnaRequestError(response['error'])

    return (response['exists'] == 'True',
            int(response['duration']),
            response['audio'] == 'True')


def get_info_tanda(filename):
    """Llamar a view homónima de edna"""
    url = 'http://edna.infoad.tv/API/get_info_tanda.php?filename={}'
    full_url = url.format(filename)
    request = requests.get(full_url)

    if not request.ok or request.status_code != 200:
        raise EdnaRequestError(request.text)

    try:
        response = request.json()
    except ValueError:
        raise EdnaRequestError(request.text)

    if not response['result']:
        raise EdnaRequestError(response['msg'])

    return response['info']


def get_nombre_post_renombre(segm):
    """Construir el nombre que llevaría la tanda en edna"""
    return "{}-{:%d%m%y}-PRRE-{:%H%M%S}.mp4".format(
        segm.medio.identificador,
        segm.desde,
        segm.desde
    )


class Command(BaseCommand):
    help = u"""
    Chequear que las tandas se hayan cortado y tengan audio
    """
    option_list = BaseCommand.option_list + (
        make_option('--medio', help=u'Sólo este medio'),
        make_option('--desde', help='analizar desde <YYYY-MM-DD HH:MM:SS>'),
        make_option('--hasta', help='analizar hasta <YYYY-MM-DD HH:MM:SS>'),
        make_option('--segmento_id', type=int, help='analizar sólo este segmento'),
    )
    tandas_no_creadas = []


    def alertar_tandas_no_creadas(self):
        """Mandar mail alertando que ciertas tandas no se pudieron crear"""
        self.tandas_no_creadas.sort(key=lambda t: t[0].desde)
        if self.tandas_no_creadas:
            msg = '<h1>Hay tandas que nunca se crearon</h1>'
            msg += '<table><tr><th>id</th><th>Medio</th><th>Desde</th><th>Hasta</th><th>Status</th></tr>'
            for (t, status) in self.tandas_no_creadas:
                msg += '<tr><td>{id}</td><td>{medio}</td><td>{desde}</td><td>{hasta}</td><td>{status}</td></tr>'.format(
                        id=t.id, medio=t.medio, desde=t.desde, hasta=t.hasta, status=status
                    )
            msg += '</table>'
            email = EmailMessage('Tandas no creadas', msg, to=MAILS_ALERTA)
            email.content_subtype = 'html'
            email.send()

    def handle(self, *args, **options):


        # tomar todos los segmentos tipo tanda
        # no chequeados de los últimos días
        segmentos = Segmento.objects.filter(
            created_at__lt=datetime.now() - MARGEN_CORTE,
            created_at__gte=datetime.now() - RETROACTIVO,
            intentos_de_recreacion__lt=MAX_RETRIES,
            tipo__nombre='Tanda',
            medio__soporte__nombre__icontains='tv',
            clip_creado=False,
        ).order_by('medio__nombre')

        medio = options.get('medio')
        if medio:
            segmentos = segmentos.filter(medio__nombre=medio)

        desde = options.get('desde')
        if desde:
            segmentos = segmentos.filter(desde__gte=desde)

        hasta = options.get('hasta')
        if hasta:
            segmentos = segmentos.filter(hasta__lt=hasta)

        segmento_id = options.get('segmento_id')
        if segmento_id:
            segmentos = segmentos.filter(pk=segmento_id)

        total = len(segmentos)

        for index, s in enumerate(segmentos, 1):

            recrear = False

            basename = get_nombre_post_renombre(s)
            print '{} / {} - {} (Segmento {})'.format(index,
                                                      total,
                                                      basename,
                                                      s.id)
            try:
                existe, duracion, audio = get_status_tanda(basename)
            except EdnaRequestError as e:
                print e
                continue
            status_tanda = '\tEXISTE: {}, DURACION: {}, AUDIO: {}'.format(existe, duracion, audio)
            print status_tanda

            if not existe:
                recrear = True
                print '\tNo existe, debo recrear!'
            else:
                duracion_aceptable = (s.hasta - s.desde).total_seconds() * 0.8
                if duracion < duracion_aceptable:
                    recrear = True
                    print '\tDuración insuficiente, debo recrear!'
                elif not audio:

                    # averiguar si tiene algo publicado porque si es así ya
                    # cargaron datos manualmente y no la queremos recrear
                    try:
                        info = get_info_tanda(basename)
                    except EdnaRequestError as e:
                        print '\tERROR al obtener publicaciones de la tanda:', e
                        continue
                    else:
                        recrear = all([registro['marca'] == 'UNKNOWN'
                                       for registro in info])
                        print '\tNo tiene audio y no tiene registros publicados, debo recrear'

            if recrear:

                s.intentos_de_recreacion += 1
                s.save()

                if s.intentos_de_recreacion >= MAX_RETRIES:
                    # bum! ya se mandó muchas veces, se manda mail de alerta
                    # y se suma 1 al número para no tomarla en cuenta la
                    # próxima vez
                    self.tandas_no_creadas.append((s, status_tanda))
                else:
                    try:
                        s.make_clip(tpa=True, dest='T', force_creation=True)
                    except Exception as e:
                        print '\tERROR al crear el clip', e
                    else:
                        print '\tClip pedido para regenerar'
            else:
                s.clip_creado = True
                s.save()

            # fin del for que recorre todas las tandas
        self.alertar_tandas_no_creadas()
