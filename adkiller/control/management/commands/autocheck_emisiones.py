# -*- coding: utf-8 -*-
# pylint: disable=too-many-locals, unused-argument, no-self-use, unused-variable
# pylint: disable=line-too-long, too-many-branches, too-many-statements

"""
Corregir emisiones de política con detección visual
"""

import requests
import time
import operator
from datetime import datetime
from optparse import make_option

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from adkiller.app.models import AudioVisual
from adkiller.app.models import Producto
from adkiller.app.models import Medio
from adkiller.app.models import Marca


# =================================
# CONSTANTES
# =================================

DISTRITOS = {
    u'360 TV Digital [ARG]': ['Buenos Aires', 'CABA'],
    u'América 2 [ARG]': ['Buenos Aires', 'CABA'],
    u'América 2 SAT [ARG]': ['Buenos Aires', 'CABA'],
    u'América 24 [ARG]': ['Buenos Aires', 'CABA'],
    u'Argentinísima Satelital [ARG]': ['Buenos Aires', 'CABA'],
    u'C5N [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal (á) [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 10 Córdoba [ARG]': ['Córdoba'],
    u'Canal 10 Mar del Plata [ARG]': ['Buenos Aires'],
    u'Canal 10 Tucumán [ARG]': ['Tucumán'],
    u'Canal 11 Salta [ARG]': ['Salta'],
    u'Canal 12 Córdoba [ARG]': ['Córdoba'],
    u'Canal 13 [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 13 Corrientes [ARG]': ['Corrientes'],
    u'Canal 13 Santa Fe [ARG]': ['Santa Fe'],
    u'Canal 13 SAT [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 26 [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 3 Rosario [ARG]': ['Santa Fe'],
    u'Canal 5 Rosario [ARG]': ['Santa Fe'],
    u'Canal 5 San Juan [ARG]': ['San Juan'],
    u'Canal 7 [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 7 Mendoza [ARG]': ['Mendoza'],
    u'Canal 8 Córdoba [ARG]': ['Córdoba'],
    u'Canal 8 Mar del Plata [ARG]': ['Buenos Aires'],
    u'Canal 8 San Juan [ARG]': ['San Juan'],
    u'Canal 8 Tucumán [ARG]': ['Tucumán'],
    u'Canal 9 [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal 9 Mendoza  [ARG]': ['Mendoza'],
    u'Canal 9 Paraná [ARG]': ['Entre Rios'],
    u'Canal 9 Resistencia [ARG]': ['Chaco'],
    u'Canal 9 Salta [ARG]': ['Salta'],
    u'Canal Ciudad [ARG]': ['Buenos Aires', 'CABA'],
    u'Canal Rural [ARG]': ['Buenos Aires', 'CABA'],
    u'CN23 [ARG]': ['Buenos Aires', 'CABA'],
    u'Construir TV [ARG]': ['Buenos Aires', 'CABA'],
    u'Crónica TV [ARG]': ['Buenos Aires', 'CABA'],
    u'Depor TV [ARG]': ['Buenos Aires', 'CABA'],
    u'El Gourmet.com [ARG]': ['Buenos Aires', 'CABA'],
    u'Encuentro [ARG]': ['Buenos Aires', 'CABA'],
    u'ESPN': ['Buenos Aires', 'CABA'],
    u'ESPN+ [ARG]': ['Buenos Aires', 'CABA'],
    u'Europa Europa [LATS]': ['Buenos Aires', 'CABA'],
    u'Fox Life [LAT]': ['Buenos Aires', 'CABA'],
    u'Fox Sports [ARG]': ['Buenos Aires', 'CABA'],
    u'Golf Channel 628 [LAT]': ['Buenos Aires', 'CABA'],
    u'Magazine [ARG]': ['Buenos Aires', 'CABA'],
    u'Metro': ['Buenos Aires', 'CABA'],
    u'Quiero! [LAT]': ['Buenos Aires', 'CABA'],
    u'Tec TV [ARG]': ['Buenos Aires', 'CABA'],
    u'Telefé [ARG]': ['Buenos Aires', 'CABA'],
    u'Telefé Interior [ARG]': ['Buenos Aires', 'CABA'],
    u'TN Todo Noticias [ARG]': ['Buenos Aires', 'CABA'],
    u'TyC Sports [ARG]': ['Buenos Aires', 'CABA'],
    u'Volver [ARG]': ['Buenos Aires', 'CABA'],
}

CERTEZA_CORRECTO = 1
CERTEZA_CAMBIADO = 2
INCERTEZA_SIN_RESULTADOS = 3
INCERTEZA_MAS_DE_UN_RESULTADO = 4
CERTEZA_VARIAS_OPCIONES = 5
INCERTEZA_SIN_PRODUCTOS_CANDIDATOS = 6

OBSERVACIONES = {
    CERTEZA_CORRECTO: 'Producto CONFIRMADO por Brandtrack',
    CERTEZA_CAMBIADO: 'Producto ASIGNADO por Brandtrack (anterior: {})',
    INCERTEZA_SIN_RESULTADOS: u'Brandtrack no detecta ningún producto',
    INCERTEZA_MAS_DE_UN_RESULTADO: u'Brandtrack detecta {} productos ({})',
    CERTEZA_VARIAS_OPCIONES: (u'Brandtrack ASIGNÓ una entre varias opciones '
                              '({}) por tener mayor score'),
    INCERTEZA_SIN_PRODUCTOS_CANDIDATOS: (u'No hubo productos candidatos para'
                                          'consultar a Brandtrack')
}

# si el score más alto es mayor que RATIO_CONFIABLE veces que el
# el siguiente, lo considreamos como confiable y lo asignamos
RATIO_CONFIABLE = 4

# cualquier producto creado antes de esta fecha no será considerado una opción
FECHA_CREACION_MINIMA = '2015-09-25'

FORMATOS_FECHA = ['%Y-%m-%d %H:%M:%S', '%Y-%m-%d']

# máxima diferencia de segundos entre productos para
# considerarlos "de la misma familia"
DURACION_DELTA = 4

WRITE_DB = True

IMAGE_URL_TPL = 'http://edna.infoad.tv/get_thumb.php?file={}&second={}'
BRANDTRACK_URL = 'http://{}/sigmund/analize/'.format(settings.BRANDTRACK_SERVER_URL)

class Command(BaseCommand):
    """Chequear emisiones de política"""

    args = '<desde> <hasta> <medio1 medio2 ...>'
    help = u"""
        Chequear emisiones de política
        Argumentos posicionales obligatorios:
            - Desde
            - Hasta
        Opcionalmente:
            - Lista de medios, nombre en InfoAd
    """

    desde = None
    hasta = None
    medios = []
    emisiones = []

    option_list = BaseCommand.option_list + (
        make_option(
            '--medios',
            nargs='+',
            help=u'Sólo estos medios'
        ),
    )

    def handle(self, *args, **options):
        """..."""

        self.parsear_argumentos(args, options)
        self.reunir_emisiones()

        total = len(self.emisiones)
        for index, emision in enumerate(self.emisiones, 1):
            print
            print u'{}/{} - {}'.format(index, total, emision.producto.nombre)
            productos_candidatos = self.obtener_productos_candidatos(emision)
            if len(productos_candidatos) == 1:
                continue
            productos_detectados = self.decidir_entre_productos(productos_candidatos, emision)
            self.escribir_en_bbdd(productos_detectados, emision)

    def reunir_emisiones(self):
        """Filtrar emisiones que serán chequeadas"""

        # Tomar todas las emisiones, menos las de radio
        emisiones = AudioVisual.objects.exclude(
            horario__medio__soporte__nombre__icontains='radio'
        ).order_by('producto__marca__nombre', 'producto__nombre')

        # Sólo de partidos políticos, detectadas por adtrack
        emisiones = emisiones.filter(
            producto__marca__sector__nombre__iexact='Partidos Políticos',
            score__gt=0
        )


        # por las dudas que se relance el proceso o se repitan fechas
        # excluyo las ya analizadas por este script
        emisiones = emisiones.exclude(
            observaciones__icontains='brandtrack'
        )

        # Filtrar por fecha y por medio
        emisiones = emisiones.filter(
            fecha__gte=self.desde,
            fecha__lte=self.hasta,
            horario__medio__in=self.medios
        )

        emisiones = emisiones.exclude(
            horario__medio__nombre='Patrones Barney'
        )

        # filtros agregados, hacer automático:
        emisiones = emisiones.filter(
            producto__marca__nombre__icontains='frente para la victoria'
        )
        emisiones = emisiones.filter(
            horario__medio__nombre__icontains='tucum'
        )

        self.emisiones = emisiones

    def parsear_argumentos(self, args, options):
        """Parsear argumentos de la línea de consola
        y asignar valores a la atributos de instancia"""

        if len(args) < 2:
            exit('Número incorrecto de argumentos. '
                 'Ejecute con -h para obtener ayuda')

        def parsear_fecha(f):
            """Descubrir formato de la fecha"""
            for formato in FORMATOS_FECHA:
                try:
                    return datetime.strptime(f, formato)
                except ValueError:
                    pass
            assert False, 'No descubrí el formato de la fecha...'

        self.desde = parsear_fecha(args[0])
        self.hasta = parsear_fecha(args[1])

        medios = options.get('medios') or []
        for m in medios:
            try:
                self.medios.append(Medio.objects.get(nombre=m))
            except ObjectDoesNotExist:
                exit('No encuentro el medio {}'.format(m))

        # si no hubo argumento medios, tomar todos los medios de tv
        if not self.medios:
            self.medios = Medio.objects.filter(soporte__nombre__icontains='tv')

    def obtener_productos_candidatos(self, emision):
        """Buscar productos que podrían haber
        sido confundidos en esta emisión"""

        # buscamos otras marcas del mismo anunciante, la del distrito al que
        # corresponde el medio y la del distrito nacional
        medio = emision.horario.medio.nombre or ''
        distritos = DISTRITOS.get(medio, [])

        # que sea del mismo anunciante
        q_anunciante = Q(anunciante=emision.producto.marca.anunciante)

        # que en el nombre de la marca aparezca cualquiera de los distritos
        # que se emiten en el medio
        q_distritos = []
        for distrito in distritos:
            q_distritos.append(Q(nombre__icontains=distrito))
        # o que el nombre de la marca sea el mismo nombre que el anunciante
        # (es decir, que sea un producto de orden nacional)
        q_distritos.append(Q(nombre=emision.producto.marca.anunciante.nombre))

        q_distritos = reduce(operator.or_, q_distritos)
        q_marcas = reduce(operator.and_, [q_anunciante, q_distritos])
        marcas_posibles = Marca.objects.filter(q_marcas)

        # buscamos los otros productos de estas marcas,
        # candidatos a haber sido confundidos por adtrack
        productos = Producto.objects.filter(
            creado__gte=FECHA_CREACION_MINIMA,
            marca__in=marcas_posibles
        ).distinct()

        # buscar sólo productos que tengan aproximadamente la misma duración:
        if any((p.duracion is None for p in productos)):
            for p in productos:
                if p.duracion is None:
                    print ' -> Seteando duración de', p.nombre
                    p.obtener_duracion_de_nombre()
                    p.save()

        productos = productos.filter(
            duracion__gte=emision.duracion - DURACION_DELTA,
            duracion__lte=emision.duracion + DURACION_DELTA,
            # devolver sólo los productos que tengan un crop asociado
            segmentos__boxes__usado_para_entrenar=True,
        ).distinct()

        return productos


    def decidir_entre_productos(self, candidatos, emision):
        """dados una lista de candidatos y una emisión, devolver
        la 'opinión' de brandtrack"""

        if not candidatos:
            return None


        # un diccionario cuyas claves son los ids de los models y los valores
        # son los productos a los que pertenece cada model
        model_id_to_prod = {}

        for c in candidatos:
            for m in c.get_models():
                model_id_to_prod[m.id_brandtrack] = c


        # un diccionario para recolectar las detecciones. Las claves son id
        # de brandmodel, como string, los valores son scores
        model_detected = {}

        thumb_seconds = range(5)
        for index in range(len(thumb_seconds)):
            # si ya miré tres thumbs y tengo producto definido, no seguir
            if index >= 2 and model_detected.keys():
                break
            print 'Intento número', index + 1, '- segundo', thumb_seconds[index]

            image = IMAGE_URL_TPL.format(emision.origen,
                                         emision.fin - thumb_seconds[index])

            print image

            params = {
                'image': image,
                'models': ','.join(map(str, model_id_to_prod.keys()))
            }
            r = requests.get(BRANDTRACK_URL, params=params)
            # print r.url

            try:
                detected = r.json()
            except ValueError:
                print "Problemas al obtener respuesta, esperando y siguiendo"
                time.sleep(10)
                continue
            else:

                print r.text

                if 'data' in detected:
                    for bb in detected['data']:
                        model = bb['model']
                        score = bb['score']
                        if not model in model_detected or model_detected[model] < score:
                            model_detected[model] = score


        model_detected = model_detected.items()
        model_detected = sorted(model_detected, key=lambda x: x[1], reverse=True)
        prod_detected = {}
        for model_id, score in model_detected:
            producto = model_id_to_prod[int(model_id)]
            if not prod_detected.has_key(producto):
                prod_detected[producto] = 0
            prod_detected[producto] = max(prod_detected[producto], score)
        return prod_detected.items()

    def escribir_en_bbdd(self, productos_detectados, emision):
        """Decidir qué modificaciones hacer en la base de
        datos según los productos que se hayan detectado"""

        if productos_detectados is None:
            mensaje = OBSERVACIONES[INCERTEZA_SIN_PRODUCTOS_CANDIDATOS]
        elif len(productos_detectados) == 0:
            mensaje = OBSERVACIONES[INCERTEZA_SIN_RESULTADOS]
        elif len(productos_detectados) == 1:
            producto_detectado, score = productos_detectados[0]
            if producto_detectado == emision.producto:
                mensaje = OBSERVACIONES[CERTEZA_CORRECTO]
            else:
                mensaje = OBSERVACIONES[CERTEZA_CAMBIADO].format(emision.producto.id)
                if WRITE_DB:
                    emision.producto = producto_detectado
        else:
            fst_score = productos_detectados[0][1]
            snd_score = productos_detectados[1][1]
            if fst_score / snd_score >= RATIO_CONFIABLE:
                mensaje = OBSERVACIONES[CERTEZA_VARIAS_OPCIONES].format(
                    len(productos_detectados),
                )
                if WRITE_DB:
                    emision.producto = productos_detectados[0][0]
            else:
                mensaje = OBSERVACIONES[INCERTEZA_MAS_DE_UN_RESULTADO].format(
                    len(productos_detectados),
                    ', '.join([str(p[0].id) for p in productos_detectados])
                )

        if WRITE_DB:
            emision.observaciones = mensaje
            emision.save()

        print mensaje

