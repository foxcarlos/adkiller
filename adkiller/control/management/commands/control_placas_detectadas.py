# -*- coding: utf-8 -*-
"""
Controlar que se estén detectando placas
"""

from datetime import timedelta
from datetime import datetime

from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage
from adkiller.media_map.models import AciertosIFEP
from adkiller.app.models import Medio

import json

MINUTOS_ALERTA = {
    'canal': 10,
}


class Command(BaseCommand):
    JSON_FILE = 'contol_ifep.json'

    def load_last_run(self):
        try:
            data = json.loads(open(self.JSON_FILE, 'r').read())
            result = []
            for elem in data:
                result.append(tuple(elem))
            return result

        except (IOError, ValueError):
            return []

    def save_run(self, data):
        open(self.JSON_FILE, 'w').write(json.dumps(data))


    def handle(self, *args, **options):

        ultima_placa = AciertosIFEP.objects.all().order_by('-datetime')[1:][0]
        if datetime.now() - ultima_placa.datetime > timedelta(minutes=65):
            # recordar que ifep mira 50 minutos para atrás
            print 'Pasó mucho tiempo sin detectar NINGUNA placa'
            self.alerta_ifep_trabado(ultima_placa)
            exit(0)


        medios = Medio.objects.filter(soporte__nombre__icontains='tv')
        medios_sin_placas = []

        for medio in medios:

            pi, pf = medio.get_patrones_inicio_y_fin()

            if pi or pf:
                last = AciertosIFEP.objects.filter(
                    medio=medio,
                ).order_by('-datetime')[:1]
                if not last:
                    medios_sin_placas.append((medio.nombre, u'Jamás detectado'))
                else:
                    last = last[0]
                    tolerancia = MINUTOS_ALERTA.get(medio.nombre, 200)
                    tiempo_sin_placas = datetime.now() - last.datetime
                    if tiempo_sin_placas > timedelta(minutes=tolerancia):
                        medios_sin_placas.append((medio.nombre, '{:%Y-%m-%d %H:%M:%S}'.format(last.datetime)))

        self.send_mail(medios_sin_placas)

    def alerta_ifep_trabado(self, ultima):
        msg = 'ALERTA URGENTE: última placa encontrada {}'.format(ultima)
        email = EmailMessage('Control IFEP', msg, to=['mmodenesi@infoad.com.ar'])
        email.send()

    def send_mail(self, nuevos):
        """..."""
        viejos = self.load_last_run()
        diferencia = set(nuevos) ^ set(viejos)
        if diferencia:
            print 'enviando mail con cambios'
            msg = u'<h1>Control IFEP</h1>'
            if nuevos:
                msg += u'<p>Pasó mucho tiempo sin detectar placas:</p><ul>'
                msg += u'<table><tr><th>Medio</th><th>Última placa</th></tr>'
                for (medio, tiempo) in sorted(nuevos, key=lambda t: t[1]):
                    msg += u'<tr><td>{}</td><td>{}</td></tr>'.format(medio, tiempo)
                msg += '</table>'
            else:
                msg += u'<p>Todo bien!</p>'
            email = EmailMessage('Control IFEP', msg, to=['mmodenesi@infoad.com.ar'])
            email.content_subtype = 'html'
            email.send()
            self.save_run(nuevos)
        else:
            print 'no hubo cambios'

