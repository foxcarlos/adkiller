# -*- coding: utf-8 -*-

# pylint: disable=unused-argument, missing-docstring, no-self-use
"""
Si no se modifica el '.log' del '.lock' correspondiente en 24 horas,
hay que eliminar el '.lock'.
Es probable que haya quedado bloqueado, y el proceso no pueda seguir en
ejecución.
"""

import glob
import os.path
from datetime import datetime
from datetime import timedelta

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage

MAILS_ALERTA = [admin[1] for admin in settings.ADMINS]
TOLERANCIA = 12 # horas


LOCKS = '/home/infoxel/locks/'
LOGS = '/home/infoxel/logs/'

def enviar_mail_alerta(lockname):
    """Enviar mail con el problema"""
    print 'Enviando mail de alerta'
    titulo = 'Infoad: Lock Trabado'
    formato_courier = 'font-family: courier; font-size: 0.9em; font-weight:bold'
    mensaje = 'El lock <span style="%s"><b>%s</b></span> '
    mensaje += 'se ha detectado como un lock trabado.<br>'
    mensaje += 'Por favor revise si est&aacute; todo bien.<br><br>'
    mensaje += 'Esta alerta se repetir&aacute; en 1 hora.<br>'
    mensaje %= (formato_courier, lockname)
    email = EmailMessage(titulo, mensaje, to=MAILS_ALERTA)
    email.content_subtype = 'html'
    email.send()

def get_lock_name(lockpath):
    """Obtengo el nombre del lock actual."""
    name = os.path.splitext(os.path.basename(lockpath))[0]
    return name


class Command(BaseCommand):

    def handle(self, *args, **options):
        # Listar la carpeta de locks
        locks_list = glob.glob(LOCKS + '*.lock')

        # Por cada .lock buscar el .log correspondiente
        for lock in sorted(locks_list):
            lock_name = get_lock_name(lock)
            log = glob.glob(LOGS + lock_name + ('.log'))
            print
            print 'LOCK:', lock_name

            if not log:
                print 'no tiene .log'
                continue

            # Obtengo la fecha de modificacion de cada archivo (lock y log)
            lock_mod = datetime.fromtimestamp(os.path.getmtime(lock))
            log_mod = datetime.fromtimestamp(os.path.getmtime(log[0]))

            # Diferencia entre las fechas de modificacion
            delta_mod = lock_mod - log_mod

            # Si hay una diferencia de x horas entre los archivos
            # eliminar el .lock (Puede ser que no se haya liberado el bloqueo)
            if delta_mod > timedelta(hours=TOLERANCIA):
                print 'Lock sospechoso'
                enviar_mail_alerta(lock_name)
            else:
                print 'Todo bien'

