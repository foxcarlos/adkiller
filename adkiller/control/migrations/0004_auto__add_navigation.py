# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Navigation'
        db.create_table('control_navigation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('medio', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Medio'])),
            ('desde_thumb', self.gf('django.db.models.fields.DateTimeField')()),
            ('hasta_thumb', self.gf('django.db.models.fields.DateTimeField')()),
            ('inicio', self.gf('django.db.models.fields.DateTimeField')()),
            ('fin', self.gf('django.db.models.fields.DateTimeField')()),
            ('modo', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
        ))
        db.send_create_signal('control', ['Navigation'])

        # Adding M2M table for field capas_activas on 'Navigation'
        m2m_table_name = db.shorten_name('control_navigation_capas_activas')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('navigation', models.ForeignKey(orm['control.navigation'], null=False)),
            ('tiposegmento', models.ForeignKey(orm['media_map.tiposegmento'], null=False))
        ))
        db.create_unique(m2m_table_name, ['navigation_id', 'tiposegmento_id'])


    def backwards(self, orm):
        # Deleting model 'Navigation'
        db.delete_table('control_navigation')

        # Removing M2M table for field capas_activas on 'Navigation'
        db.delete_table(db.shorten_name('control_navigation_capas_activas'))


    models = {
        'app.ciudad': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Ciudad'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.etiquetamedio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'EtiquetaMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'app.grupomedio': {
            'Meta': {'object_name': 'GrupoMedio'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'app.medio': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Medio'},
            'busque_tandas_hasta': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'canal': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'canal_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ciudad': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Ciudad']"}),
            'codigo_ibope': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'desfasando': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dvr': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'encargado': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'etiquetas': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'medios'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['app.EtiquetaMedio']"}),
            'grabando': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'grabando_en_cero': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'grupo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.GrupoMedio']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificador': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_anterior': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'identificador_directv': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'incremento_color': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'mediadelivery_id': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'publicar': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'server': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_deteccion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'server_grabacion': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'medios_grabacion'", 'null': 'True', 'to': "orm['app.Servidor']"}),
            'soporte': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'medios'", 'to': "orm['app.Soporte']"})
        },
        'app.rol': {
            'Meta': {'object_name': 'Rol'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'app.servidor': {
            'Meta': {'object_name': 'Servidor'},
            'automatico': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'e_mail_del_tecnico': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'espacio_en_disco': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'fecha': ('django.db.models.fields.DateField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'maquina_virtual': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nombre_del_tecnico': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'nro': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'online': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'plaza': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'servidores'", 'null': 'True', 'to': "orm['app.Ciudad']"}),
            'puerto_http': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_mysql': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'puerto_ssh': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'recibir_patrones': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'roles': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['app.Rol']", 'symmetrical': 'False', 'blank': 'True'}),
            'tecnico_alertado': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ultima_vez_online': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'padre': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'control.metricadiariauser': {
            'Meta': {'object_name': 'MetricaDiariaUser'},
            'accion': ('django.db.models.fields.SmallIntegerField', [], {'default': '3'}),
            'cantidad': ('django.db.models.fields.IntegerField', [], {}),
            'duracion': ('django.db.models.fields.FloatField', [], {}),
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'formato': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media_map.Formato']", 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Medio']"}),
            'tipo_segmento': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media_map.TipoSegmento']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'control.metricasgrab': {
            'Meta': {'object_name': 'MetricasGrab'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'media_pack': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'}),
            'medios': ('django.db.models.fields.IntegerField', [], {}),
            'medios_en_calidad_estandar': ('django.db.models.fields.IntegerField', [], {}),
            'medios_grabados': ('django.db.models.fields.IntegerField', [], {})
        },
        'control.metricasproc': {
            'Meta': {'object_name': 'MetricasProc'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'horas_aplicadas_infoad': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'media_pack': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'}),
            'registros_infoad': ('django.db.models.fields.IntegerField', [], {})
        },
        'control.metricasuser': {
            'Meta': {'object_name': 'MetricasUser'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_infoad': ('django.db.models.fields.IntegerField', [], {}),
            'nuevos_prospectos_infoad': ('django.db.models.fields.IntegerField', [], {})
        },
        'control.navigation': {
            'Meta': {'object_name': 'Navigation'},
            'capas_activas': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['media_map.TipoSegmento']", 'symmetrical': 'False'}),
            'desde_thumb': ('django.db.models.fields.DateTimeField', [], {}),
            'fin': ('django.db.models.fields.DateTimeField', [], {}),
            'hasta_thumb': ('django.db.models.fields.DateTimeField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inicio': ('django.db.models.fields.DateTimeField', [], {}),
            'medio': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Medio']"}),
            'modo': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'media_map.formato': {
            'Meta': {'object_name': 'Formato'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'tipo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['media_map.TipoSegmento']"})
        },
        'media_map.tiposegmento': {
            'Meta': {'object_name': 'TipoSegmento'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '250'})
        }
    }

    complete_apps = ['control']