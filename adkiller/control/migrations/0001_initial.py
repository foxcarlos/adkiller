# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MetricasGrab'
        db.create_table('control_metricasgrab', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('media_pack', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Soporte'], null=True, blank=True)),
            ('medios', self.gf('django.db.models.fields.IntegerField')()),
            ('medios_grabados', self.gf('django.db.models.fields.IntegerField')()),
            ('medios_en_calidad_estandar', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('control', ['MetricasGrab'])

        # Adding model 'MetricasProc'
        db.create_table('control_metricasproc', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('media_pack', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['app.Soporte'], null=True, blank=True)),
            ('registros_infoad', self.gf('django.db.models.fields.IntegerField')()),
            ('horas_aplicadas_infoad', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('control', ['MetricasProc'])

        # Adding model 'MetricasUser'
        db.create_table('control_metricasuser', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha', self.gf('django.db.models.fields.DateField')()),
            ('nuevos_prospectos_infoad', self.gf('django.db.models.fields.IntegerField')()),
            ('ingresos_infoad', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('control', ['MetricasUser'])


    def backwards(self, orm):
        # Deleting model 'MetricasGrab'
        db.delete_table('control_metricasgrab')

        # Deleting model 'MetricasProc'
        db.delete_table('control_metricasproc')

        # Deleting model 'MetricasUser'
        db.delete_table('control_metricasuser')


    models = {
        'app.soporte': {
            'Meta': {'ordering': "[u'nombre']", 'object_name': 'Soporte'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'})
        },
        'control.metricasgrab': {
            'Meta': {'object_name': 'MetricasGrab'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'media_pack': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'}),
            'medios': ('django.db.models.fields.IntegerField', [], {}),
            'medios_en_calidad_estandar': ('django.db.models.fields.IntegerField', [], {}),
            'medios_grabados': ('django.db.models.fields.IntegerField', [], {})
        },
        'control.metricasproc': {
            'Meta': {'object_name': 'MetricasProc'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'horas_aplicadas_infoad': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'media_pack': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['app.Soporte']", 'null': 'True', 'blank': 'True'}),
            'registros_infoad': ('django.db.models.fields.IntegerField', [], {})
        },
        'control.metricasuser': {
            'Meta': {'object_name': 'MetricasUser'},
            'fecha': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ingresos_infoad': ('django.db.models.fields.IntegerField', [], {}),
            'nuevos_prospectos_infoad': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['control']