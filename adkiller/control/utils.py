# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, unused-variable, protected-access
# pylint: disable=too-many-locals, star-args
"""
utils for app control
"""

from django.db.models import Count

from adkiller.control.models import MetricaDiariaUser
from adkiller.control.models import Navigation
from adkiller.media_map.models import TipoSegmento
from adkiller.media_map.models import Formato

from datetime import datetime
from datetime import timedelta

def compute_delta_seconds(segmentos):
    """Computar cantidad de segundos empleados en crear
    todos estos segmentos"""
    result = timedelta(seconds=0)
    previo = None
    for segmento in segmentos:
        if not previo:
            previo = segmento.created_at
        else:
            delta = segmento.created_at - previo
            if delta > timedelta(minutes=5):
                result += delta
            else:
                # discutible...
                result += timedelta(seconds=30)
    return result.total_seconds()


class ModifyingPreComputedMetricError(Exception):
    """Para señalar que se está intentando modificar
    una métrica previamente salvada"""


class MetricasManager(object):
    """Clase encargada de ordenar las métricas"""

    CARGA_PATRON_IMAGEN = MetricaDiariaUser.CARGA_PATRON_IMAGEN
    CARGA_PATRON_AUDIO = MetricaDiariaUser.CARGA_PATRON_AUDIO
    REGISTRO = MetricaDiariaUser.REGISTRO
    EDICION = MetricaDiariaUser.EDICION

    ATTR_NAME_TPL = ('_metrica_{user_id}_{medio_id}_{fecha:%Y%m%d}'
                     '_{tipo_segmento_id}_{formato}_{accion}')

    METRICAS_DICT = {}

    def guardar_cambios(self):
        """Guardar métricas nuevas"""
        for metrica in self.metricas_list():
            # buscar la navegación que dió origen a esta métrica
            navigations = Navigation.objects.filter(
                user=metrica.user,
                medio=metrica.medio,
                fin__gte=datetime.now() - timedelta(hours=1),
                accion_destino__isnull=True,
            ).order_by('-fin')
            if navigations:
                navigation = navigations[0]
                navigation.accion_destino = metrica
                navigation.save()

            metrica.save()

    def metricas_list(self):
        """Devolver lista de métricas"""
        return [self.METRICAS_DICT[key] for key in sorted(self.METRICAS_DICT.keys())]

    def incrementar_cuenta(self, segmento, segundos, accion, cantidad_boxes=None):
        """Incrementar contador y sumar tiempo a la métrica
        apropiada según el segmento recibido"""

        key = self._make_key(segmento, accion)
        metrica = self.METRICAS_DICT[key]
        if accion == metrica.CARGA_PATRON_IMAGEN:
            if cantidad_boxes is None:
                metrica.cantidad += segmento.boxes.all().count()
            else:
                metrica.cantidad += cantidad_boxes
        else:
            metrica.cantidad += 1
        metrica.duracion += segundos

    # pylint: disable=unused-argument
    def _make_key(self, segmento, accion):
        """Armar el string que sirve
        como key para encontrar la métrica
        correspondiente, inicializándola si
        no existe"""

        user_id = segmento.user.pk
        fecha = segmento.created_at.date()
        medio_id = segmento.medio.pk
        tipo_segmento_id = segmento.tipo.pk
        formato = segmento.formato

        nombre = self.ATTR_NAME_TPL.format(**locals())
        self.METRICAS_DICT.setdefault(nombre, self._get_or_create_metrica(**locals()))
        return nombre
    # pylint: enable=unused-argument

    @staticmethod
    def _get_metrica(**kwargs):
        """Consultar bbdd y devolver métrica, puede lanzar excepción"""
        return MetricaDiariaUser.objects.get(
            fecha=kwargs['fecha'],
            user_id=kwargs['user_id'],
            medio_id=kwargs['medio_id'],
            tipo_segmento_id=kwargs['tipo_segmento_id'],
            formato=kwargs['formato'],
            accion=kwargs['accion']
        )

    @staticmethod
    def _get_or_create_metrica(**kwargs):
        """Consultar bbdd y devolver métrcia o crearla"""
        result = None
        pre_existente = MetricaDiariaUser.objects.filter(
            fecha=kwargs['fecha'],
            user_id=kwargs['user_id'],
            medio_id=kwargs['medio_id'],
            tipo_segmento_id=kwargs['tipo_segmento_id'],
            formato=kwargs['formato'],
            accion=kwargs['accion']
        )

        if pre_existente:
            result = pre_existente[0]
        else:
            nueva = MetricaDiariaUser(
                fecha=kwargs['fecha'],
                user_id=kwargs['user_id'],
                medio_id=kwargs['medio_id'],
                tipo_segmento_id=kwargs['tipo_segmento_id'],
                formato=kwargs['formato'],
                cantidad=0,
                duracion=0,
                accion=kwargs['accion']
            )
            result = nueva

        return result

def navigations_to_metricas(navigations):
    """Transformar una serie de navigations en algo parecido a métricas"""

    # tipo de navegación genérica
    tipo_nav, _ = TipoSegmento.objects.get_or_create(nombre='Navigation')

    result = list()

    values = [
        'fecha',
        'fecha_material',
        'user',
        'medio',
        'modo',
        'accion_destino__accion',
        'accion_destino__tipo_segmento',
        ]

    # agregar el pseudocampo 'fecha'
    navigations = navigations.extra({'fecha': 'date(inicio)', 'fecha_material': 'date(desde_thumb)'})

    for nav_group in navigations.values(*values).annotate(Count('id')):
        # cada registro en esta consulta anotada es una familia de navigations
        # que me interesa agrupar y sumar su duración y su cantidad, ya que
        # todas tienen el mismo user, el mismo medio, la misma fecha, etc.

        nav = navigations.filter(
            inicio__year=nav_group['fecha'].year,
            inicio__month=nav_group['fecha'].month,
            inicio__day=nav_group['fecha'].day,
            desde_thumb__year=nav_group['fecha_material'].year,
            desde_thumb__month=nav_group['fecha_material'].month,
            desde_thumb__day=nav_group['fecha_material'].day,
            user=nav_group['user'],
            medio=nav_group['medio'],
            modo=nav_group['modo'],
            accion_destino__accion=nav_group['accion_destino__accion'],
            accion_destino__tipo_segmento=nav_group['accion_destino__tipo_segmento'],
            ).extra({'duracion': 'fin - inicio',
                     'cantidad': 'hasta_thumb - desde_thumb'})

        cantidad = int(sum([n.cantidad.total_seconds() for n in nav]))
        duracion = max(1, int(sum([n.duracion.total_seconds() for n in nav])))

        # convertir esto a métrica, pero no salvarla
        muestra = nav[0]
        if muestra.modo == Navigation.NM_CERRANDO:
            formato = Formato(nombre='Cerrando')
        elif muestra.modo == Navigation.NM_CONTROLANDO:
            formato = Formato(nombre='Controlando')
        else:
            formato = Formato(nombre='')
        m = MetricaDiariaUser(
            fecha_material=muestra.desde_thumb.date(),
            fecha=muestra.inicio.date(),
            user=muestra.user,
            medio=muestra.medio,
            tipo_segmento=tipo_nav,
            cantidad=None,
            material=cantidad,
            duracion=duracion,
            formato=formato,
        )
        m.accion = MetricaDiariaUser.NAVEGACION  #muestra.accion_destino.accion
        if muestra.accion_destino is not None:
            tipo_seg, _ = TipoSegmento.objects.get_or_create(
                #nombre='Navigation ({})'.format(muestra.accion_destino.tipo_segmento.nombre)
                nombre=muestra.accion_destino.tipo_segmento.nombre,
            )
        else:
            tipo_seg, _ = TipoSegmento.objects.get_or_create(
                nombre="Control",
            )
        m.tipo_segmento = tipo_seg
            
        result.append(m)

    return result
