# -*- coding: utf-8 -*-
# pylint: disable=line-too-long, undefined-loop-variable, too-many-locals
# pylint: disable=too-many-branches, too-many-statements
"""
Views for app control
"""
# Django Imports
from django.conf import settings
from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt

# project imports
from adkiller.app.models import Migracion
from adkiller.control.control_as_run import generar_output_as_run
from adkiller.control.forms import MetricasDiariasUserForm
from adkiller.control.management.commands.generar_metricas import HtmlFormatter
from adkiller.control.utils import navigations_to_metricas
from adkiller.control.models import MetricaDiariaUser
from adkiller.control.models import Navigation
from adkiller.media_map.models import DIFERENCIA_C_WELO
from adkiller.media_map.models import Segmento

# Python imports
import json
from datetime import datetime
from datetime import timedelta
from collections import defaultdict
import os


@csrf_exempt
def add_navigation(request):
    """Agregar tiempo de navegación"""

    if not (request.user and request.user.is_authenticated()):
        data = {'success': False, 'error': 'User is not authenticated'}
        return HttpResponse(json.dumps(data), mimetype='application/json')

    user = request.user
    nav_data = json.loads(dict(request.POST).keys()[0])['navigation']
    if not nav_data:
        data = {'success': False, 'error': 'No POST data'}
        return HttpResponse(json.dumps(data), mimetype='application/json')

    medio_id = nav_data['medio_infoad']
    F_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'

    # desde dónde hasta dónde se navegó
    d = datetime.strptime(nav_data['first_thumb'], F_FORMAT)
    h = datetime.strptime(nav_data['last_thumb'], F_FORMAT)

    # cuánto tiempo se invirtió en esta navegación
    i = datetime.strptime(nav_data['t1'], F_FORMAT)
    f = datetime.strptime(nav_data['t2'], F_FORMAT)

    desde_thumb, hasta_thumb, inicio, fin = [dt - DIFERENCIA_C_WELO for dt in [d, h, i, f]]

    # Si inicio es igual a fin, no seguir
    if i == f:
        data = {'success': False, 'error': 'Inicio es igual a Fin'}
        return HttpResponse(json.dumps(data), mimetype='application/json')

    modo = int(nav_data['mode'])

    # 0 es controlando, 1 es grabando
    if not modo in [0, 1]:
        data = {'success': False, 'error': 'Modo desconocido'}
        return HttpResponse(json.dumps(data), mimetype='application/json')

    capas_activas = nav_data.get('layers', '')
    if capas_activas:
        capas_activas = map(int, capas_activas.split(','))
    else:
        capas_activas = []

    # chequear si existe un navigation previo con estas características
    previas = Navigation.objects.filter(
        user=user,
        medio_id=medio_id,
        inicio=inicio,
        modo=modo
    )
    if capas_activas:
        # debe tener las mismas capas activas, TODAS
        for ca in capas_activas:
            previas = previas.filter(capas_activas__id=ca)

    # el invariante es que len(previas) <= 1
    if previas:
        previa = previas[0]
        previa.desde_thumb = min(previa.desde_thumb, desde_thumb)
        previa.hasta_thumb = max(previa.hasta_thumb, hasta_thumb)
        previa.fin = max(previa.fin, fin)
        previa.save()
        nav = previa
    else:
        # crear una nueva
        nav = Navigation.objects.create(
            user=user,
            medio_id=medio_id,
            inicio=inicio,
            fin=fin,
            desde_thumb=desde_thumb,
            hasta_thumb=hasta_thumb,
            modo=modo
        )
        for ca in capas_activas:
            nav.capas_activas.add(ca)
    if nav.accion_destino is None:
        # Setear en qué se empleó finalmente el tiempo de esta navegación
        s_id = nav_data.get('segment_id')
        if s_id is not None:
            # obtengo el segmento para saber el tipo
            try:
                segmento = Segmento.objects.get(pk=int(s_id))
            except ObjectDoesNotExist:
                pass
            else:
                tipo = segmento.tipo
                accion = nav_data.get('action') # update? create?
                # creación: 1, update: 0
                if accion == '1':
                    accion = MetricaDiariaUser.REGISTRO
                elif accion == '0':
                    accion = MetricaDiariaUser.EDICION
                mdu = MetricaDiariaUser.objects.filter(
                    user=nav.user,
                    medio=nav.medio,
                    fecha=nav.desde_thumb.date(),
                    tipo_segmento=tipo,
                    accion=accion,
                )
                if mdu:
                    nav.accion_destino = mdu[0]


    data = {'success': True}
    return HttpResponse(json.dumps(data), mimetype='application/json')


class MyMedio(object):
    """Data para control_tandas"""
    def __init__(self, nombre, tandas, baches):
        self.nombre = nombre
        self.tandas = tandas
        self.baches = baches
        self.total = len(tandas)
        total_automaticas = len([t for t in tandas
                                 if t.descripcion and
                                 t.descripcion.startswith('Tanda autom')])
        if self.total:
            self.auto = total_automaticas / float(self.total) * 100
        else:
            self.auto = 0


@staff_member_required
def control_tandas(request):
    """
    Mostrar cantidad de tandas manuales y automáticas
    """

    retroactivo = timedelta(days=int(request.GET.get('d', 1)))
    tandas = Segmento.objects.filter(
        tipo__nombre='Tanda',
        desde__gte=datetime.now() - retroactivo,
    ).order_by('desde')

    baches = Segmento.objects.filter(
        tipo__nombre='Bache-IFEP',
        desde__gte=datetime.now() - retroactivo,
    ).order_by('desde')

    segmentos = defaultdict(lambda: defaultdict(list))

    for t in tandas:
        segmentos[t.medio.nombre]['tandas'].append(t)
    for b in baches:
        segmentos[b.medio.nombre]['baches'].append(b)

    medios = [MyMedio(key, segmentos[key]['tandas'], segmentos[key]['baches'])
              for key in segmentos.keys()]

    context = {
        'medios': medios
    }

    return render(request,
                  'tandas.html',
                  context,
                  context_instance=RequestContext(request))


@staff_member_required
def as_run(request):
    """
    esta view muestra el formulario y redirige a
    otra donde se muestran los resultados
    """
    from adkiller.control.forms import AsRunForm
    from adkiller.app.utils import save_file_to_disk

    if request.method == 'POST':
        form = AsRunForm(request.POST, request.FILES)
        if form.is_valid():
            folder = os.path.join(settings.MEDIA_ROOT, 'as_run')
            if not os.path.exists(folder):
                os.system('mkdir -p "%s"' % folder)
                os.system('chmod 777 "%s"' % folder)
            filepath = os.path.join(folder, "as_run_%s.csv" % datetime.now().strftime("%Y%m%d_%H%M%SS"))
            save_file_to_disk(request.FILES['archivo'], filepath)
            print "Salvando el archivo a", filepath
            try:
                inverso = request.POST['inverso']
            except KeyError:
                inverso = False
            try:
                crear_em_no_econtradas = request.POST['crear_emisiones_no_encontradas']
            except KeyError:
                crear_em_no_econtradas = False
            return generar_output_as_run(archivo=filepath,
                                         medio_id=request.POST['medio'],
                                         inverso=inverso,
                                         crear_em_no_encontradas=crear_em_no_econtradas)
    else:
        # crear form en blanco
        form = AsRunForm()

    return render(request, 'as_run.html', {'form': form}, context_instance=RequestContext(request))


def programacion_dtv(request):
    """..."""
    if request.GET['region'] == "argentina":
        territorio = 5
    elif request.GET['region'] == "venezuela":
        territorio = 3
    if 'fecha' in request.GET:
        fecha = request.GET['fecha']
    else:
        m = Migracion.objects.filter(servidor__nombre="DTV").order_by("-fecha")[0]
        fecha = m.fecha.strftime("%Y-%m-%d")
    direc = "/home/infoxel/projects/media/infoad/DTV_Schedules/"
    filepath = "Schedules_%s_%02d.dat" % (fecha, territorio)
    abspath = open(direc + filepath, 'r')
    response = HttpResponse(content=abspath.read())
    response['Content-Type'] = 'application/text'
    response['Content-Disposition'] = 'attachment; filename=%s' % filepath
    return response

TIPOS_METRICAS = (
    ("inversion", "Inversión Publicitaria"),
    ("cantidad", "Cantidad de Publicidades"),
    ("volumen", "Segundos"),
    ("material", "Cantidad de Medios"),
    ("registros", "Cantidad de Registros"),
    ("usuarios", "Usuarios Activos"),
)


@staff_member_required
def metricas(request):
    """..."""
    if not "tipo" in request.session:
        request.session["tipo"] = TIPOS_METRICAS[0][0]
    else:
        for i in range(len(TIPOS_METRICAS)):
            if TIPOS_METRICAS[i][0] == request.session["tipo"]:
                break
        request.session["tipo"] = TIPOS_METRICAS[(i + 1) % len(TIPOS_METRICAS)][0]

    tipo = request.session["tipo"]

    for tipo_titulo in TIPOS_METRICAS:
        if tipo_titulo[0] == tipo:
            titulo = tipo_titulo[1]

    if tipo == "inversion":
        prefix = "$"
    else:
        prefix = ""
    valores = {
        "argentina": prefix + "100.000",
        "brasil": prefix + "0",
        "chile": prefix + "0",
        "colombia": prefix + "0",
        "paraguay": prefix + "0",
        "peru": prefix + "0",
        "venezuela": prefix + "0",
    }

    context = {
        "valores": valores,
        "titulo": titulo
    }

    return render(request, 'map.html', context, context_instance=RequestContext(request))


@staff_member_required
def metricas_diarias_user(request):
    """View para controlar las métricas diarias"""
    context = {}
    if request.POST:
        form = MetricasDiariasUserForm(request.POST)
        if form.is_valid():
            desde = form.cleaned_data['desde']
            hasta = form.cleaned_data['hasta']
            user = form.cleaned_data['filtrar_por_usuario']
            accion = int(form.cleaned_data['filtrar_por_accion'])
            medio = form.cleaned_data['filtrar_por_medio']
            tipo_segmento = form.cleaned_data['filtrar_por_tipo_de_segmento']
            context['desde'] = desde
            context['hasta'] = hasta
            context['user'] = user
            context['accion'] = accion
            context['medio'] = medio
            context['tipo_segmento'] = tipo_segmento
            if hasta < desde:
                context['error'] = 'El período no es válido'
            else:
                if hasta > datetime.now().date():
                    context['error'] = 'Aún no tengo métricas del futuro... :-)'
                else:
                    mdu = MetricaDiariaUser.objects.filter(fecha__range=(desde, hasta))
                    navigations = Navigation.objects.filter(
                        inicio__range=(desde, hasta + timedelta(days=1))
                    )
                    if user:
                        mdu = mdu.filter(user=user)
                        navigations = navigations.filter(user=user)
                    if accion:
                        mdu = mdu.filter(accion=accion)
                        navigations = navigations.filter(accion_destino__accion=accion)
                    if medio:
                        mdu = mdu.filter(medio=medio)
                        navigations = navigations.filter(medio=medio)
                    if tipo_segmento:
                        mdu = mdu.filter(tipo_segmento=tipo_segmento)
                        navigations = navigations.filter(accion_destino__tipo_segmento=tipo_segmento)
                    navigations = navigations.extra(where=["EXTRACT(EPOCH FROM (fin - inicio)) < 60*60"])
                    mdu_list = list(mdu)
                    mdu_list.extend(navigations_to_metricas(navigations))

                    # dar formato html
                    if mdu_list:
                        context['metricas_table'] = HtmlFormatter(mdu_list).obtener_html()
                    else:
                        context['error'] = 'No hay métricas de este periodo'
    else:
        # crear form en blanco
        form = MetricasDiariasUserForm()
    context['form'] = form

    return render(request, 'metricas_diarias_user.html',
                  context, context_instance=RequestContext(request))

def alerta_falla_video(request):
    """mandar mail a operaciones
    alertando que un usuario no pudo
    descargar el video que quería"""
    deep_status_url = request.GET['deep_status_url']
    msg = 'Problemas al descargar el <a href="{}">video</a>'.format(deep_status_url)
    email = EmailMessage('Video no descargado', msg,
                         to=[a[1] for a in settings.ADMINS])
    email.content_subtype = 'html'
    email.send()

    return HttpResponse(json.dumps({'ok': True}), mimetype='application/json')


