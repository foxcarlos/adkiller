import autocomplete_light

from django.contrib.auth.models import User

from adkiller.app.models import Medio

from adkiller.filtros.models import Perfil

class UserAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('username',)
    autocomplete_js_attributes={'placeholder': 'Buscar por Usuario ..'}

autocomplete_light.register(User,UserAutocomplete)


class NombreAutocomplete(autocomplete_light.AutocompleteModelBase):
    search_fields=('nombre',)
    autocomplete_js_attributes={'placeholder': 'Ingrese valor ..',}
    
    def choices_for_request(self):
        q = self.request.GET.get('q', '')
        choices = self.choices.all()
        if q:
            choices = choices.filter(nombre__aicontains=q)
        return self.order_choices(choices)

class MedioAutocomplete(NombreAutocomplete):
    pass

autocomplete_light.register(Medio, MedioAutocomplete)
