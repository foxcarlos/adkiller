# -*- coding: utf-8 -*-
from adkiller.app.models import Emision, Medio, Servidor, Producto, AudioVisual, Marca
from django.conf import settings
from django.http import HttpResponse
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist



from datetime import datetime, time, date, timedelta
import commands
import csv
import json
import os
import re
import urllib

# CONSTANTES
MARGEN_INFOAD = 0 # minutos para considerar que una tanda de infoad podría contener una emisión del CSV
MARGEN_CAPTURADVR = 10 # minutos para considerar que una tanda cortada podría contener una emisión del CSV
MARGEN_TANDAMARCADA = 10 # minutos para considerar que una tanda marcada podría contener una emisión del csv
TOLERANCIA = timedelta(minutes=10) # tiempo en que una tanda de simpson podría contener una emisión del CSV
FILES_PATH = os.path.join(settings.MEDIA_ROOT, 'as_run')
COL_ID_PROD_DTV = 0
COL_SPOTNAME = 1
COL_STATUS = 2
COL_FECHA = 3
COL_HORA = 4
COL_DURACION = 5
COL_PROGRAMA = 6
COL_MARCA = 7
COL_PRODUCTO = 8
FORMATOS_FECHA_POSIBLES = ['%d/%m/%y', '%d/%m/%Y', '%y/%m/%d', '%Y/%m/%d',
                           '%d-%m-%y', '%d-%m-%Y', '%y-%m-%d', '%Y-%m-%d']
FORMATOS_HORA_POSIBLES = ['%H:%M:%S', '%H/%M/%S']

# GLOBALES
wget = {}
tandas_marcadas = {}
formato_fecha = None
formato_hora = None
separador = None
orden_columnas = []

def descubrir_formato_csv(archivo):
    global formato_fecha
    global formato_hora
    global separador
    global orden_columnas
    archivo = open(archivo)

    # la primera fila es el titulo, descubramos el delimitador
    primera_fila = archivo.readline()
    pyc = len(primera_fila.split(';'))
    coma = len(primera_fila.split(','))
    tab = len(primera_fila.split('\t'))

    separador = max(pyc, coma, tab)
    if separador == pyc:
        separador = ';'
    elif separador == coma:
        separador = ','
    elif separador == tab:
        separador = '\t'

    print "Separador: '%s'" % separador


    # leo la columna de los títulos
    titulos = primera_fila.split(separador)
    print "Columnas:"
    for index, t in enumerate(titulos):
        t = t.lower()
        t = t.replace('"', '')
        t = t.replace("'", '')
        t = t.strip()
        if t == '':
            continue
        print t,
        if t in 'id ident identificador':
            orden_columnas.append(COL_ID_PROD_DTV)
            print '-> identificador'
        elif t in 'spot spotname nombre':
            orden_columnas.append(COL_SPOTNAME)
            print '-> spot name'
        elif t in 'status':
            orden_columnas.append(COL_STATUS)
            print '-> status'
        elif t in 'fecha date':
            orden_columnas.append(COL_FECHA)
            print '-> fecha'
        elif t in 'hora hour time':
            orden_columnas.append(COL_HORA)
            print '-> hora'
        elif t in 'duración duracion duration length len longitud largo':
            orden_columnas.append(COL_DURACION)
            print '-> duracion'
        elif t in 'programa program':
            orden_columnas.append(COL_PROGRAMA)
            print '-> programa'
        elif t in 'marca':
            orden_columnas.append(COL_MARCA)
            print '-> marca'
        elif t in 'producto pruduct prod':
            orden_columnas.append(COL_PRODUCTO)
            print '-> producto'


    # averiguo formato de fecha y hora:

    lineas_vacias = 0
    while lineas_vacias < 20 and not (formato_hora and formato_fecha):
        linea = archivo.readline()
        if linea == '':
            lineas_vacias += 1

        if not (COL_FECHA in orden_columnas and COL_HORA in orden_columnas):
            # nunca la voy a descrubrir porque no sé qué columna es
            break

        fecha, hora = get_from_fila(linea.split(separador), [COL_FECHA, COL_HORA])
        if not fecha or not hora:
            continue

        for formato in FORMATOS_FECHA_POSIBLES:
            try:
                fecha = datetime.strptime(fecha, formato)
                formato_fecha = formato
                break
            except:
                pass

        for formato in FORMATOS_HORA_POSIBLES:
            try:
                hora = datetime.strptime(hora, formato)
                formato_hora = formato
                break
            except:
                try:
                    # a veces viene con centésimas de segundo...
                    hora = datetime.strptime(hora[:-3], formato)
                    formato_hora = formato
                    break
                except:
                    pass

    if formato_fecha and formato_hora:
        print "formato de fecha:", formato_fecha
        print "formato de hora:", formato_hora

    archivo.close()

def detalles_del_formato():
    detalles = 'DETALLES DEL FORMATO:\nCOLUMNAS USADAS: '

    if COL_FECHA in orden_columnas:
        detalles += 'Fecha (%d);' % orden_columnas.index(COL_FECHA)
    if COL_HORA in orden_columnas:
        detalles += 'Hora (%d);' % orden_columnas.index(COL_HORA)
    if COL_ID_PROD_DTV in orden_columnas:
        detalles += 'Identificador (%d);' % orden_columnas.index(COL_ID_PROD_DTV)
    else:
        if COL_MARCA in orden_columnas:
            detalles += 'Marca (%d);' % orden_columnas.index(COL_MARCA)
        if COL_PRODUCTO in orden_columnas:
            detalles += 'Producto (%d);' % orden_columnas.index(COL_PRODUCTO)

    detalles += '\nFORMATO DE FECHA; %s\n' % formato_fecha
    detalles += 'FORMATO DE HORA; %s\n' % formato_hora
    return detalles


def get_from_fila(fila, columnas):
    result = []
    for c in columnas:
        try:
            data = fila[orden_columnas.index(c)]
        except:
            data = None
        if data is not None:
            data = data.replace('"', '')
            data = data.replace("'", '')
            data = data.strip()
        result.append(data)
    if len(result) == 1:
        result = result[0]
    return result

def get_tandas_marcadas(fecha, medio):
    global tandas_marcadas
    server = medio.server_grabacion.nombre
    if not tandas_marcadas.has_key(server):
        tandas_marcadas[server] = {}
    if not tandas_marcadas[server].has_key(fecha):
        tandas_marcadas[server][fecha] = get_data_from_db(fecha, medio)
    return tandas_marcadas[server][fecha]


def get_data_from_db(fecha, medio):
    dvr_camara_fecha = '_'.join([medio.dvr, str(medio.canal), fecha])
    url = 'http://' + medio.server_grabacion.url_completa() + '/API/get_tandas_marcadas.php?ident=%s' % dvr_camara_fecha
    print "Buscando tandas marcadas en ", url
    try:
        html = urllib.urlopen(url)
    except Exception as e:
        print url
        print e
    try:
        response = json.loads(html.readline())
    except Exception, e:
        print e
        return []
    if response['result']:
        return response['info']
    else:
        return []


def check_in_tabla_tandas(medio, fecha_tanda, hora):
    fecha_str = fecha_tanda.strftime('%Y%m%d')
    fecha_tanda = datetime(fecha_tanda.year,
                           fecha_tanda.month,
                           fecha_tanda.day,
                           hora.hour,
                           hora.minute,
                           hora.second)
    tandas = get_tandas_marcadas(fecha_str, medio)
    for t in tandas:
        inicio_tanda = get_datetime_from_filename(t['archivo1'], int(t['desde']))
        fin_tanda = get_datetime_from_filename(t['archivo2'], int(t['hasta']))
        if inicio_tanda < fecha_tanda < fin_tanda:
            return str(t['id_tanda']), t['archivo1']
    return False, ""


def get_datetime_from_filename(archivo, seg):
    if 'temp' in archivo:
        archivo = archivo.replace('_temp', '')
    if 'merge' in archivo:
        for i in range(10):
            archivo = archivo.replace('_merge{}'.format(i), '')
    _, _, fecha, hora = archivo.split('.')[0].split('_')
    return datetime.strptime(fecha + hora, '%Y%m%d%H%M%S') + timedelta(seconds=seg)


def get_tandas_analizadas(medio):
    global wget
    server = medio.server.nombre
    ident = str(medio.identificador)
    if not server in wget.keys():
        wget[server] = {}
    if not ident in wget[server].keys():
        wget[server][ident] = check_in_tabla_pendientes(medio)
    return wget[server][ident]

def check_in_tabla_pendientes(medio):
    ident = str(medio.identificador)
    url = 'http://' + medio.server.url_completa() + '/API/get_tandas_pendientes.php?ident=%s' % ident
    result = {}
    print "Buscando tandas analizadas en ", url
    try:
        html = urllib.urlopen(url)
    except Exception as e:
        print url
        print e
    try:
        response = json.loads(html.readline())
    except Exception, e:
        print e
        return {}
    if response['result']:
        # las ordeno por fecha
        for t in response['tandas']:
            fecha = t.split('-')[1]
            if not result.has_key(fecha):
                result[fecha] = []
            result[fecha].append(t)
        return result
    else:
        return {}


# esta función dejo de usarse porque es muy lento obtener las tandas así
def obtener_ls_captura_dvr(server):
    resultado = {}
    url = 'http://' + server.url_completa() + '/multimedia/ar/CapturaDVRs'
    print "Buscando tandas en ", url
    try:
        html = urllib.urlopen(url)
    except Exception as e:
        print url
        print e

    for linea in html.readlines():
        result = re.search('<a href="(?P<tanda>[A-Za-z0-9\-\.]*)', linea)
        if result:
            tanda = result.groupdict()['tanda']
            medio = tanda.split('-')[0]
            if not resultado.has_key(medio):
                print "agregando %s al diccionario de tandas" % medio
                resultado[medio] = []
            resultado[medio].append(tanda)

    return resultado


def csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
    # código tomado de http://docs.python.org/2/library/csv.html
    # csv.py doesn't do Unicode; encode temporarily as UTF-8:
    csv_reader = csv.reader(unicode_csv_data, dialect=dialect, **kwargs)
    for row in csv_reader:
    # decode UTF-8 back to Unicode, cell by cell:
        yield [unicode(cell, 'utf-8') for cell in row]


def get_fecha_sup_inf_hora_sup_inf(fecha, hora, margen):
    try:
        fecha = datetime.strptime(fecha, formato_fecha)
    except:
        raise Exception("No entiendo la fecha '%s'" % fecha)
    fecha = date(day=fecha.day, month=fecha.month, year=fecha.year)
    try:
        # a veces la hora viene con décimas de segundo
        hora = hora.split('.')[0]
    except:
        pass

    hora = datetime.strptime(hora, formato_hora)
    dt = datetime(fecha.year, fecha.month, fecha.day, hora.hour, hora.minute, hora.second)

    # si el margen es de N minutos, tomamos
    # N/2 para atrás y N/2 para adelante:
    lim_inferior = dt - timedelta(minutes=margen / 2)
    hora_inferior = lim_inferior.time()
    fecha_inferior = lim_inferior.date()
    lim_superior = dt + timedelta(minutes=margen / 2)
    hora_superior = lim_superior.time()
    fecha_superior = lim_superior.date()

    return(fecha_superior, fecha_inferior, hora_superior, hora_inferior)


def entre_horas(em, f_inf, f_sup, h_inf, h_sup):
    dt_inf = datetime(f_inf.year, f_inf.month, f_inf.day, h_inf.hour, h_inf.minute, h_inf.second)
    dt_sup = datetime(f_sup.year, f_sup.month, f_sup.day, h_sup.hour, h_sup.minute, h_sup.second)
    dt_em = datetime(em.fecha.year, em.fecha.month, em.fecha.day, em.hora.hour, em.hora.minute, em.hora.second)
    return dt_inf <= dt_em <= dt_sup


def get_emisiones_from_fila(fila, medio):
    try:
        ident, fecha, hora, marca, producto,= get_from_fila(fila,
                [COL_ID_PROD_DTV, COL_FECHA, COL_HORA, COL_MARCA, COL_PRODUCTO])
    except:
        # la fila está mal formateada
        ident = hora = marca = None

    emisiones = []

    if ident:
        # Hacemos el control considerando A-NUMBER del producto (codigo_directv)
        if ident.strip().startswith('A'):
            f_sup, f_inf, h_sup, h_inf = get_fecha_sup_inf_hora_sup_inf(fecha, hora, MARGEN_INFOAD)

            emisiones = Emision.objects.filter(
                    #        (~Q(fecha=f_sup) | Q(hora__lte=h_sup))).filter(
                    #(~Q(fecha=f_inf) | Q(hora__gte=h_inf))).filter(
                    Q(fecha__gte=f_inf, fecha__lte=f_sup)).filter(
                    horario__medio=medio)
            emisiones = filter(lambda em : entre_horas(em, f_inf, f_sup, h_inf, h_sup), list(emisiones))
            emisiones = filter(lambda em : em.tiene_codigo_directv(ident), emisiones)
    elif marca and producto:
        # Control por MARCA Y PRODUCTO
        f_sup, f_inf, h_sup, h_inf = get_fecha_sup_inf_hora_sup_inf(fecha, hora, MARGEN_INFOAD)

        emisiones = Emision.objects.filter(
                    Q(fecha__gte=f_inf, fecha__lte=f_sup)).filter(
                        horario__medio=medio,
                        producto__nombre=producto,
                        producto__marca__nombre=marca)
        emisiones = filter(lambda em : entre_horas(em, f_inf, f_sup, h_inf, h_sup), list(emisiones))

    return emisiones


def coincide_con_infoad(fila, medio):
    resultado = False
    emisiones = get_emisiones_from_fila(fila, medio)

    exacto = False
    if emisiones:

        ident, fecha, hora, marca, producto,= get_from_fila(fila,
                [COL_ID_PROD_DTV, COL_FECHA, COL_HORA, COL_MARCA, COL_PRODUCTO])

        resultado = "COINCIDE CON INFOAD; %d emision(es) cerca" % len(emisiones)

        for emision in emisiones:
            if ( ident and                                                  # estamos haciendo control por ident
                 emision.producto and emision.producto.codigo_directv and   # la emisión tiene lo que tiene que tener
                 emision.producto.tiene_codigo_directv(ident)):             # la emisión tiene el mismo codigo_directv
                resultado = "COINCIDE CON INFOAD; Emision de '%s' a las %s" % (emision.producto.nombre, emision.hora.strftime("%H:%M"))
                exacto = True
                # seteo el codigo_directv de esta emisión!
                emision.codigo_directv = ident
                emision.save()
            elif ( marca and producto and                                   # estamos haciendo el control por marca y producto
                   emision.producto and emision.producto.marca and          # la emision tiene lo que tiene que tener
                   emision.producto.nombre == producto and                  # es el mismo producto
                   emision.producto.marca.nombre == marca and               # misma marca
                   emision.fecha and
                   fecha == emision.fecha.strftime(formato_fecha) and       # la misma fecha
                   emision.hora and
                   hora == emision.hora.strftime(formato_hora)):            # la misma hora
                resultado = "COINCIDE CON INFOAD; Emision de '%s' a las %s" % (emision.producto.nombre, emision.hora.strftime("%H:%M"))
                exacto = True
    return (resultado, exacto)


def se_corto_una_tanda(fila, medio):
    resultado = ''
    tandas = get_tandas_analizadas(medio)
    ident_medio = medio.identificador
    if not ident_medio:
        return "ERROR; El medio no tiene identificador!", ""
    fecha, hora = get_from_fila(fila, [COL_FECHA, COL_HORA])
    fecha_key = datetime.strptime(fecha, formato_fecha).strftime('%d%m%y')
    fecha_sup, fecha_inf, hora_sup, hora_inf = get_fecha_sup_inf_hora_sup_inf(fecha, hora, MARGEN_CAPTURADVR)

    fecha_inf = datetime.combine(fecha_inf, hora_inf)
    fecha_sup = datetime.combine(fecha_sup, hora_sup)
    fecha_emision = datetime.strptime(fecha + hora, formato_fecha + formato_hora)

    if not fecha_key in tandas.keys():
        return resultado, ""

    tanda = None
    minima_diferencia = None
    for t in sorted(tandas[fecha_key]):
        # elimino el identificador del medio y la extensión
        fecha_string, hora_string = t.split('-')[1::2]
        hora_string = hora_string.split('.')[0]
        # obtego la fecha como datetime
        try:
            fecha_t = datetime.strptime(fecha_string + hora_string, '%d%m%y%H%M')
        except:
            fecha_t = datetime.strptime(fecha_string + hora_string, '%d%m%y%H%M%S')
        if fecha_t > fecha_sup:
            # las tengo ordenadas, así que no tiene sentido seguir buscando
            break
        if minima_diferencia is None or abs(fecha_t - fecha_emision) < minima_diferencia:
            # comparar con la fecha superior es redundante,
            # pero lo dejo por claridad
            if fecha_inf <= fecha_t <= fecha_sup:
                resultado = "COINCIDE CON TANDA CORTADA"
                tanda = t
                minima_diferencia = abs(fecha_t - fecha_emision)

    if tanda is None:
        return resultado, ""
    else:
        return resultado, tanda


def se_marco_una_tanda(fila, medio):
    # me conecto a la bbdd del server de corte
    resultado = False
    fecha, hora = get_from_fila(fila, [COL_FECHA, COL_HORA])
    fecha, _, hora, _ = get_fecha_sup_inf_hora_sup_inf(fecha, hora, 0) # sin margen!
    id_tanda, tanda = check_in_tabla_tandas(medio, fecha, hora)
    if id_tanda:
        # hay una tanda marcada
        return "COINCIDE CON TANDA MARCADA; id_tanda: %s" % id_tanda, tanda
    return resultado, ""


def encode_utf8(fila):
    result = []
    for elem in fila:
        result.append(elem.encode('utf-8'))
    return result

def chequeos_post_infoad(fila, medio):
    # si no coincide con la información de infoad,
    # se hacen controles extra

    resultado = ''
    tanda = ""

    # 1ro: chequeo si existe una tanda cortada
    tanda_cortada, tanda = se_corto_una_tanda(fila, medio)
    if tanda_cortada:
        resultado = tanda_cortada
    else:
        # 2do: chequeo si se marcó una tanda (pero no se cortó)
        tanda_marcada, tanda = se_marco_una_tanda(fila, medio)
        if tanda_marcada:
            resultado = tanda_marcada
        else:
            # realmente no tenemos nada!
            resultado = "NO COINCIDE CON NADA; :("
    return resultado, tanda

def controlar_fila(fila, medio, crear_emisiones=False):
    fila = encode_utf8(fila)
    resultado = ''

    ident = get_from_fila(fila, [COL_ID_PROD_DTV])

    # chequeo que sea un spot
    if ident and not ident.strip().startswith('A'):
        fila.append('IGNORADO')
        return fila

    coincidio_exacto = False
    # chequeo si coincide con algo emitido por nosotros
    audiovisuales, coincidio_exacto = coincide_con_infoad(fila, medio)

    if audiovisuales:
        resultado = audiovisuales
    else:
        resultado, tanda = chequeos_post_infoad(fila, medio)

    if not coincidio_exacto and crear_emisiones:
        id_nuevo = crear_emision(fila, medio, tanda)
        if id_nuevo:
            resultado += ';audiovisual creado id %d' % id_nuevo
        else:
            resultado += '; no pude crear la emision...'

    resultado = resultado.encode('utf-8')
    for elem in resultado.split(';'):
        fila.append(elem)

    return fila


def crear_emision(fila, medio, tanda=""):
    ident, fecha, hora, marca, prod = get_from_fila(fila,
            [COL_ID_PROD_DTV, COL_FECHA, COL_HORA, COL_MARCA, COL_PRODUCTO])
    if ident:
        producto = Producto.objects.filter(codigo_directv__contains=ident)
    elif marca and prod:
        producto = Producto.objects.filter(nombre=prod)
    else:
        producto = None
    if producto:
        producto = producto[0]
        # busco otra emision del producto para tomar su duración
        otras_em = Emision.objects.filter(producto=producto, duracion__gt=0)
        duracion = 0
        for em in otras_em:
            duracion = em.duracion
            if duracion > 0:
                break

        fecha = datetime.strptime(fecha, formato_fecha)
        hora = datetime.strptime(hora, formato_hora)
        fecha = date(fecha.year, fecha.month, fecha.day)
        hora = time(hora.hour, hora.minute, hora.second)

        if True:
            nueva_emision = AudioVisual.objects.create(
                confirmado=False,
                score=-1,
                fecha=fecha,
                hora=hora,
                producto=producto,
                duracion=duracion)
            nueva_emision.set_horario(medio)
            nueva_emision.calcular_tarifa()

            if tanda:
                nueva_emision.origen = tanda
                #_, fecha, _, hora = tanda.split(".")[0].split("-")
                #fecha = datetime.strptime(fecha, "%d%m%y")
                #hora = datetime.strptime(hora, "%H%M%S")
            else:
                nueva_emision.set_origen()

            if ident:
                nueva_emision.codigo_directv = ident

            if not nueva_emision.duracion:
                d = nueva_emision.producto.obtener_duracion_de_nombre()
                if d:
                    nueva_emision.duracion = d

            nueva_emision.save()
            print "Emision creada! ID:", nueva_emision.id
            return nueva_emision.id

    return False


def obtener_periodo(archivo):
    min_fecha = None
    max_fecha = None
    reader = csv_reader(open(archivo + "2"), delimiter=';')

    print "Obteniendo fecha inferior y fecha superior"
    titulo = True
    for row in reader: # titulos
        if titulo:
            titulo = False
            continue

        try:
            fecha = datetime.strptime(get_from_fila(row, [COL_FECHA]), formato_fecha)
        except:
            print "problemas con", str(row)
            continue
        if not min_fecha or not max_fecha:
            min_fecha = fecha
            max_fecha = fecha
        else:
            if min_fecha > fecha:
                min_fecha = fecha
            if max_fecha < fecha:
                max_fecha = fecha

    return (min_fecha, max_fecha)


# =========================================
# función principal: generar_output_as_run
# =========================================

def generar_output_as_run(archivo, medio_id, inverso, crear_em_no_encontradas=False, debugging=False):
    # preparar el archivo
    if debugging:
        import ipdb
        ipdb.set_trace()
    cmd = "iconv -f ISO-8859-15 -t UTF-8 < '%s'> '%s'" % (archivo, archivo + "2")
    print cmd
    status_iconv, _ = commands.getstatusoutput(cmd)
    if status_iconv == 0:
        descubrir_formato_csv(archivo + '2')
        if inverso:
            response = _generar_output_as_run_inverso(archivo, medio_id, debugging)
        else:
            response = _generar_output_as_run(archivo, medio_id, debugging, crear_em_no_encontradas)
    else:
        response = _generar_response_error_iconv(debugging)

    if debugging:
        print "El archivo está listo"
    else:
        return response

def _generar_response_error_iconv(debugging):
    if debugging:
        response = open('/home/infoxel/Descargas/resultado.csv', 'w')
    else:
        # abrir filelike respuesta
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="tabla.csv"'

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response, delimiter=';', dialect="excel")
    writer.writerow(['Problemas al entender la codificacion del archivo'])
    return response

def _generar_output_as_run(archivo, medio_id, debugging, crear_em_no_encontradas):
    # buscamos mostrar cuánto de lo que hay en el csv está en bbdd infoad
    if debugging:
        response = open('/home/infoxel/Descargas/resultado.csv', 'w')
    else:
        # abrir filelike respuesta
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    reader = csv_reader(open(archivo + "2"), delimiter=';')

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response, delimiter=';', dialect="excel")

    detalles = detalles_del_formato()
    for d in detalles.split('\n'):
        writer.writerow(d.split(';'))

    formato_valido = aprobar_formato()
    if not formato_valido:
        writer.writerow(['El archivo no reúne los datos necesarios para empezar el control'])

    medio_ok, error_msg = aprobar_medio(medio_id)
    if not medio_ok:
        writer.writerow([error_msg])
    else:
        medio = Medio.objects.get(id=medio_id)
        # comienza el control
        titulo = True

        for index, row in enumerate(reader):
            if index % 500 == 0:
                print index
            if titulo:
                row.append("STATUS INFOAD")
                row.append("DETALLES")
                row = encode_utf8(row)
                writer.writerow(row)
                titulo = False
                continue

            # controlar la fila, con un try muy general, para que no se clave
            try:
                row = controlar_fila(row, medio, crear_em_no_encontradas)
            except Exception, e:
                row = encode_utf8(row)
                row.append("PROBLEMAS")
                row.append(e)

            # escribir el resultado
            writer.writerow(row)

    return response


def get_emision_mas_cercana(emisiones, fila):
    if len(emisiones) == 1:
        return emisiones[0]

    # calcular la hora de la emisión
    ident, fecha, hora = get_from_fila(fila, [COL_ID_PROD_DTV, COL_FECHA, COL_HORA])
    fecha = datetime.strptime(fecha, formato_fecha)
    try:
        # a veces la hora viene con décimas de segundo
        hora = hora.split('.')[0]
    except:
        pass

    hora = datetime.strptime(hora, formato_hora)
    hora = datetime(fecha.year, fecha.month, fecha.day, hora.hour, hora.minute, hora.second)
    distancia = None
    em_mas_cerc = None
    for em in emisiones:
        try:
            inf = min(hora, em.hora_real())
            sup = max(hora, em.hora_real())
        except:
            inf = min(hora, datetime(hora.year, hora.month, hora.day, em.hora_real().hour, em.hora_real().minute, em.hora_real().second))
            sup = max(hora, datetime(hora.year, hora.month, hora.day, em.hora_real().hour, em.hora_real().minute, em.hora_real().second))

        if not distancia or not em_mas_cerc:
            distancia = sup - inf
            em_mas_cerc = em
        else:
            if distancia > sup - inf:
                distancia = sup - inf
                em_mas_cerc = em
    return em_mas_cerc

def aprobar_medio(medio_id):
    """Informar si el medio es apto para empezar los controles"""
    try:
        m = Medio.objects.get(id=medio_id)
    except ObjectDoesNotExist:
        return False, 'No existe medio con id {}'.format(medio_id)
    else:
        if m.server is None:
            return False, 'Medio no tiene servidor de deteccion asignado'
        elif m.server_grabacion is None:
            return False, 'Medio no tiene servidor de grabacion asignado'
        elif not m.identificador_directv:
            return False, 'Medio no tiene identificador DTV'
        else:
            return True, ''

def aprobar_formato():
    # control de la FECHA
    valido = formato_fecha is not None and COL_FECHA in orden_columnas

    # control de la HORA
    valido = valido and (formato_hora is not None and COL_HORA in orden_columnas)

    # necesito IDENT o (MARCA y PRODUCTO)
    ident = COL_ID_PROD_DTV in orden_columnas
    marca = COL_MARCA in orden_columnas
    producto = COL_PRODUCTO in orden_columnas
    valido = valido and (ident or (marca and producto))

    return valido

def _generar_output_as_run_inverso(archivo, medio_id, debugging=False):
    # buscamos mostrar todas las emisiones que están infoad y no en el csv subido
    if debugging:
        response = open('/home/infoxel/Descargas/resultado.csv', 'w')
    else:
        # abrir filelike respuesta
        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename="tabla.csv"'
    reader = csv_reader(open(archivo + "2"), delimiter=';')

    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response, delimiter=';', dialect="excel")

    detalles = detalles_del_formato()
    for d in detalles.split('\n'):
        writer.writerow(d.split(';'))

    formato_valido = aprobar_formato()
    if not formato_valido:
        writer.writerow(['El archivo no reúne los datos necesarios para empezar el control'])

    medio_ok, error_msg = aprobar_medio(medio_id)
    if not medio_ok:
        writer.writerow([error_msg])
    else:
        medio = Medio.objects.get(id=medio_id)
        # obtener la totalidad de NUESTRAS emisiones dentro de este período
        min_fecha, max_fecha = obtener_periodo(archivo)
        writer.writerow(['Fecha inicial', min_fecha.strftime(formato_fecha), 'Fecha final', max_fecha.strftime(formato_fecha)])
        print "Filtrando desde %s hasta %s" % (min_fecha, max_fecha)
        emisiones = Emision.objects.filter(
                fecha__gte=min_fecha,
                fecha__lte=max_fecha,
                horario__medio__id=medio_id,
                producto__isnull=False).order_by('fecha', 'hora')
        # filtrar sólo las emisiones de producto con código DTV
        emisiones = emisiones.filter(producto__codigo_directv__isnull=False)
        emisiones = emisiones.filter(producto__codigo_directv__gt='')
        emisiones = list(emisiones)

        for index, row in enumerate(reader):
            if index % 500 == 0:
                print index
            if index == 0:
                # títulos
                continue
            row = encode_utf8(row)

            # tomar la emisión del archivo y eliminarla del query set
            try:
                em_row = get_emisiones_from_fila(row, medio)
            except Exception, e:
                em_row = None

            # a veces no viene ninguna...
            if not em_row:
                continue
            # a veces pueden venir dos o tres...
            em_mas_cercana = get_emision_mas_cercana(em_row, row)
            if em_mas_cercana in emisiones:
                # quizás ya la saqué...
                emisiones.remove(em_mas_cercana)

        # en este punto, las emisiones que quedaron son las que ellos no mandaron!

        # generar el output
        row = ['Emisiones que aparecen en INFOAD y no en el archivo subido']
        row = encode_utf8(row)
        writer.writerow(row)

        for em in emisiones:
            try:
                producto = em.producto.nombre
            except:
                producto = 'producto__nombre'
            try:
                if em.codigo_directv:
                    cdtv = em.codigo_directv
                else:
                    cdtv = em.producto.codigo_directv
            except:
                cdtv = 'producto__codigo_dtv'
            try:
                fecha = em.fecha.strftime(formato_fecha)
            except:
                fecha = 'fecha'
            try:
                hora = em.hora.strftime(formato_hora)
            except:
                hora = 'hora'

            row = [producto, cdtv, fecha, hora]
            row = encode_utf8(row)
            writer.writerow(row)

        writer.writerow(['FIN'])

    return response

