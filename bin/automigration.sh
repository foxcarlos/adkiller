#! /bin/bash
export LANG=en_US.UTF-8
export LOCALE=UTF-8

su - postgres -c pg_dump chile_base > /tmp/bkp_chile.sql
cp /tmp/bkp_chile.sql /home/infoad/projects/backup/chile/
rm /tmp/bkp_chile.sql

BASE_URL="http://www.megatime.cl/mmia/pautas"
BASE_DIR="/home/infoad/projects/migracion-chile"

echo "enter file name, followed by [ENTER] (ex.: '20140609 - 20140615.rar' ):"
read filename

filename=$(echo $filename | sed 's/ /\\ /g' )

#eval "wget $BASE_URL/$filename --output-document=$BASE_DIR/$filename"

eval "unrar x $BASE_DIR/$filename $BASE_DIR"

cd $BASE_DIR
python quitarespacios.py

ls $BASE_DIR/*.csv | while read file;
do
    echo "Migrando info del archivo:" $file
    python /home/infoad/projects/chile/manage.py migrar file "$file"
done

rm -rf $BASE_DIR/*.csv
rm -rf $BASE_DIR/*.csv2


exit

  if [[ ! `basename "$file"` =~ ^Metro.*|^Vía\ Pública* ]];
  then
    echo "Migrando info del archivo:" $file
    python /home/infoad/projects/chile/manage.py migrar file $file
  fi
